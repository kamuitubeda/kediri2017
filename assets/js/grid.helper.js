(function($) {
    if (typeof $.common === "undefined")
        $.common = {grid:{}};
    else
        $.common.grid = {};
        
    $.common.grid.alternateRows = function(el) {
        var rows = $(el).find(".jqgrow");
        for (var i = 0; i < rows.length; i += 2) {
            rows[i].attributes["class"].value += " ui-priority-secondary";
        }
    };
    
    $.common.grid.highlightCell = function(el, rowid, column, color) {
        $(el).jqGrid('setCell', rowid, column, '', {
            color:color
        });
    };
    
    $.common.grid.messageDialog = function (title, message, width, height) {
        width = typeof width !== 'undefined' ? width : 'auto';
        height = typeof height !== 'undefined' ? height : 'auto';
        var el = $("div#error");
        if (el.length > 0) {
            el.dialog({
                bgiframe: true,
                modal: true,
                width: width,
                height: height,
                title: title,
                close: function(event, ui) {
                    $(this).empty();
                    $(this).dialog("destroy")
                },
                open: function (event,ui) {
                    $(this).html(message);
                }
            });
        } else {
            alert(message);
        }
    };
    
    $.common.grid.showErrorResponse = function (response) {
        if (response.responseText != 'success') {
            $.common.grid.messageDialog('Response', response.responseText);
        }
        return [true,"",""];
    };
    
    $.common.grid.fixToolbar = function(g) {
        $('#' + g.navigation + '_center').attr('align', 'left');
        $('#' + g.navigation + '_left').appendTo('#t_' + g.grid);
        $('#t_' + g.grid).css("border-top", "0 none").css("border-left", "0 none").css("height", "25px");
    }
    
})(jQuery);