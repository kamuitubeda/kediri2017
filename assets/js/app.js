// autoresize map
jQuery(function($) {
	$(window).resize(function() {
		$("#map-canvas").css("height", jQuery(window).height()-30-77-20);
	});

	$(window).resize();
});


var markers = new Array();
var shownMarkers = new Array();
var polys = new Array();
var daftarSkpd = new Array();
var date = new Date();
var tahun = date.getFullYear()-1;
var location;


var mapOptions = {
	//center: { lat: -7.46369847, lng:112.43337572},
	center: { lat: -7.51852294, lng:112.55944669},
	zoom: 17
};


var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
var selectedMarker = null;
var selectedMarker2 = null;


// handler info_window pada saat klik informasi marker
var iwInfo = new google.maps.InfoWindow({
	maxWidth: 500
});
// handler info_window pada saat tambah marker
var iwAdd = new google.maps.InfoWindow({
	maxWidth: 500
});

function isInfoWindowOpened(iw) {
	return iw.getMap() !== null && typeof iw.getMap() !== "undefined";
}


function deleteMarker(kode) {
    jQuery.post(getAsetHapusUrl(), {kode: kode}, function(json) {
    	if (selectedMarker != null && json.success) {
    		selectedMarker.setMap(null);
    		selectedMarker = null;

    		if (selectedMarker2 != null) {
    			selectedMarker2.setMap(null);
    			selectedMarker2 = null;
    		}

    		if (isInfoWindowOpened(iwInfo)) {
    			iwInfo.close();
    		}
    	}
    }, "json");
}

function gantiData(uid){
	var table = jQuery("table#" + uid);

	var kode = table.find("input#kode").val();
	var old_kode = table.find("input#old_kode").val();
	var nama_asset = table.find("p#nama_asset").html();
	var kib = table.find("p#bidang").html();
	var kode_kib = table.find("input#kode_bidang").val();
	var skpd = table.find("p#nama_skpd").html();

	jQuery.post(getAsetUbahUrl(), {kode: kode, old_kode: old_kode, nama_asset: nama_asset, kib: kib, kode_kib: kode_kib}, function(json) {
    	if (json.success) {
    		if (selectedMarker != null) {
    			selectedMarker.kode = kode;
    		}
    		alert("Data telah diubah");
    	}
    	else{
    		alert("Data yang dimasukkan sama");
    	}
    }, "json");	
}


jQuery(function($) {
	$("[name='my-checkbox']").bootstrapSwitch();

	function infoCallback(iw, marker, marker2) {
		return function(e) {
			// cek info_window isopen
			if (isInfoWindowOpened(iw)) {
				iw.close();
			}

			selectedMarker = marker;
			selectedMarker2 = marker2;

			var uid = marker.uid;

			$.post(getAsetGetUrl(), {kode:marker.kode}, function(json) {
				var data = json.data;
				var imgs = json.images;

				var panelTab = null;
				var content = null;

				if(marker.kode_kib.substr(0,2) == '01'){
					panelTab = ['<li role="presentation" class="active"><a href="#info" aria-controls="info" role="tab" data-toggle="tab">Info</a></li>',
								'<li role="presentation"><a href="#foto" aria-controls="foto" role="tab" data-toggle="tab">Foto</a></li>']
				} else if(marker.tipe === "LINESTRING" || marker.kode_kib.substr(0,2) != '01'){
					panelTab = ['<li role="presentation" class="active"><a href="#info" aria-controls="info" role="tab" data-toggle="tab">Info</a></li>',
								'<li role="presentation"><a href="#foto" aria-controls="foto" role="tab" data-toggle="tab">Foto</a></li>']
				}


				var tmp = '';
				if (imgs.length > 0){
					for (var i=0; i<imgs.length; i++) {
						tmp += '<div class="col-xs-12"><img enc_id="' + imgs[i].id + '" src="' + getBaseUrl() + "assets/img/" + imgs[i].src + '" class="img-responsive img-thumbnail"></div>';
					}
				}


				if (marker.tipe === "POINT") {
					iw.open(map, marker);

					content = [
						'<div role="tabpanel">',
							'<ul class="nav nav-tabs" role="tablist">',
								panelTab,
							'</ul>',
							'<div class="tab-content">',
								'<div role="tabpanel" class="tab-pane active" id="info">',
									'<table class="hasil" style="height:300px; width:400px;" id=\'' + uid + '\'>', 
										'<input id="kode_bidang" type="hidden" value=' + data.kode_kib + ' />',
										'<input id="old_kode" type="hidden" value=' + data.kode + ' />',
										'<tr>',
											'<td style="width:25%;">Kode Aset:</td>',
											'<td style="width:75%;">',
												'<div class="ui-widget">',
													'<input style="padding-left:2px;" id="kode" size="25" value="' + data.kode + '" />',
												'</div>',
											'</td>',
										'</tr>',
										'<tr>',
											'<td style="width:25%;">Nama Aset:</td>',
											'<td style="width:75%;">',
												'<p style="padding-left:2px; margin-top: 7px;" id="nama_asset">' + data.nama_asset + '</p>',
											'</td>',
										'</tr>',
										'<tr>',
											'<td style="width:25%;">KIB:</td>',
											'<td style="width:75%;">',
												'<p style="padding-left:2px; margin-top: 7px;" id="bidang">' + data.kib + '</p>',
											'</td>',
										'</tr>',
										'<tr>',
											'<td style="width:25%;">Latitude:</td>',
											'<td style="width:75%;">',
												'<p style="padding-left:2px;" id="latFld">' + data.latitude + '</p>',
											'</td>',
										'</tr>',
										'<tr>',
											'<td style="width:25%;">Longitude:</td>',
											'<td style="width:75%;">',
												'<p style="padding-left:2px;" id="lngFld">' + data.longitude + '</p>',
											'</td>',
										'</tr>',
										'<tr>',
											'<td style="width:25%;">SKPD:</td>',
											'<td style="width:75%;">',
												'<p style="padding-left:2px;" id="skpdFld"> ' + data.nama_skpd + '</p>',
											'</td>',
										'</tr>',
										'<tr>',
											'<td colspan="2" style="text-align:center;">',
												'<input id="updateBtn" onclick="gantiData(\'' + uid + '\')" type="button" style="width:25%;" value="Update"/>&nbsp;&nbsp;',
												'<input id="deleteBtn" type="button" style="width:25%;" onclick="deleteMarker(\'' + data.kode + '\')" value="Delete"/>',
											'</td>',
										'</tr>',
									'</table>',
								'</div>',
								'<div role="tabpanel" class="tab-pane" id="foto">',
									'<div class="row">',
										'<div class="pull-left">',
											'<input id="fotoUploader" type="file" multiple />&nbsp;&nbsp;',
										'</div>',
									'</div>',
									'<div class="row">',
										'<div class="pull-left">',
											'<input class="form-control" id="bFotoUploader" type="button" value="Upload" />',
										'</div>',
									'</div>',
									'<br/>',
									'<div class="row foto2">',
										tmp,
									'</div>',
								'</div>',
							'</div>',
						'</div>'
					].join("");
				} else if (marker.tipe === "LINESTRING") {
					iw.setPosition(e.latLng);
					iw.open(map);

					content = [
						'<div id="info role="tabpanel">',
							'<ul class="nav nav-tabs" role="tablist">',
								panelTab,
							'</ul>',
							'<div class="tab-content">',
								'<div role="tabpanel" class="tab-pane active" id="info">',
									'<table class="hasil" style="height:300px; width:400px;" id=\'' + uid + '\'>', 
										'<input id="kode_bidang" type="hidden" value=' + data.kode_kib + ' />',
										'<input id="old_kode" type="hidden" value=' + data.kode + ' />',
										'<tr>',
											'<td style="width:25%;">Kode Aset:</td>',
											'<td style="width:75%;">',
												'<div class="ui-widget">',
													'<input style="padding-left:2px;" id="kode" size="25" value="' + data.kode + '" />',
												'</div>',
											'</td>',
										'</tr>',
										'<tr>',
											'<td style="width:25%;">Nama Aset:</td>',
											'<td style="width:75%;">',
												'<p style="padding-left:2px; margin-top: 7px;" id="nama_asset">' + data.nama_asset + '</p>',
											'</td>',
										'</tr>',
										'<tr>',
											'<td style="width:25%;">KIB:</td>',
											'<td style="width:75%;">',
												'<p style="padding-left:2px; margin-top: 7px;" id="bidang">' + data.kib + '</p>',
											'</td>',
										'</tr>',
										'<tr>',
											'<td style="width:25%;">Lintasan:</td>',
											'<td style="width:75%;">',
												'<p style="padding-left:2px;" id="linFld">-</p>',
											'</td>',
										'</tr>',
										'<tr>',
											'<td style="width:25%;">SKPD:</td>',
											'<td style="width:75%;">',
												'<p style="padding-left:2px;" id="skpdFld"> ' + data.nama_skpd + '</p>',
											'</td>',
										'</tr>',
										'<tr>',
											'<td colspan="2" style="text-align:center;">',
												'<input id="updateBtn" onclick="gantiData(\'' + uid + '\')" type="button" style="width:25%;" value="Update"/>&nbsp;&nbsp;',
												'<input id="deleteBtn" type="button" style="width:25%;" onclick="deleteMarker(\'' + data.kode + '\')" value="Delete"/>',
											'</td>',
										'</tr>',
									'</table>',
								'</div>',
								'<div role="tabpanel" class="tab-pane" id="foto">',
									'<div class="row">',
										'<div class="pull-left">',
											'<input id="fotoUploader" type="file" multiple />&nbsp;&nbsp;',
										'</div>',
									'</div>',
									'<div class="row">',
										'<div class="pull-left">',
											'<input class="form-control" id="bFotoUploader" type="button" value="Upload" />',
										'</div>',
									'</div>',
									'<br/>',
									'<div class="row foto2">',
										tmp,
									'</div>',
								'</div>',
							'</div>',
						'</div>'
					].join("");
				}

				iw.setContent(content);

				google.maps.event.addListener(iw, 'domready', function() {
					$("input#kode").autocomplete({
	    				minLength: 1,
	    				source: getAsetCariUrl(),
	    				select: function(e, u) {
	    					$(this).val(u.item.id);
	    					var table = $(this).closest("table");
	    					table.find("p#nama_asset").html(u.item.NAMA_BARANG);
	    					table.find("p#bidang").html(u.item.BIDANG);
	    					return false;
	    				}
	  				});

	  				if (marker.tipe === "LINESTRING") {
	  					var tmp = _.map(marker.getPath().getArray(), function(marker) { return {lat:marker.lat(), lng:marker.lng()} });
						$("table#" + uid).find("p[id=linFld]").html("<pre><code>" + library.json.prettyPrint(tmp) + "</code></pre>");
	  				}


					$("#bFotoUploader").unbind().click(function() {
						var files = $("#fotoUploader").get(0).files;
						var success = [];
						var status = false;

						if (files.length > 0) {
							for (var i=0; i<files.length; i++) {
								var fd = new FormData();
								fd.append("asset_id", $("#old_kode").val());
								fd.append("file", files[i]);

								success.push($.ajax({
									url: getUploadFotoUrl(),
									type: "POST",
									data: fd,
									processData: false,
									contentType: false,
									success: function(res) {
										var json = $.parseJSON(res);
										status = json.status;
										if (json.status === "success") {
											$(".foto2").append('<div class="col-xs-12"><img enc_id="' + json.id + '" src="' + getBaseUrl() + 'assets/img/' + json.src + '" class="img-responsive img-thumbnail"></div>');
										} else {
											alert("Foto yang Anda upload gagal!");
										}
									}
								}));

								$.when.apply(null, success).done(function() {
									if (status === "success") {
										$("#fotoUploader").replaceWith(jQuery("#fotoUploader").val("").clone(true));
									}
								});
							}
						} else {
							alert("Tidak ada foto yang dipilih");
						}
					});
				});
			}, "json");
		};
	}

	function skpdCallback(info_window, skpd) {
		return function() {
			selectedMarker = skpd;
			info_window.open(map, skpd);

			$("body").mask("Loading");

			var tahunbaru = $('#tahun-filter option:selected').val();
			if(tahunbaru != ""){
				tahun = tahunbaru;
			}

			$.post(getSKPDUrl(), {kode:skpd.kode, tahun:tahun}, function(json) {
				var data = json.data;
				var awal = json.skpd_awal;
				var mutasi = json.skpd_mutasi;
				var susut = json.skpd_susut;
				var asetSkpd = Array();

				var content = [
					'<div class="skpd" style="min-width:600px; max-height:300px;">',
						'<table style="width:100%">',
							'<tr><td style="width:25%;">Kode SKPD:</td><td style="width:75%;"><div style="width:250px;" class="ui-widget"><input class="form-control" style="padding-left:2px;" id="kode" size="25" value="' + data.kode + '" /></div></td></tr>',
							'<tr><td style="width:25%;">Nama Unit:</td><td style="width:75%;"><p style="padding-left:2px; margin-top: 7px;" id="nama_asset" />' + "KANTOR " + data.nama + '</td></tr>',
							'<tr><td style="width:25%;">Latitude:</td> <td style="width:75%;"><p style="padding-left:2px;" id="latFld"/> ' + data.longitude + '</td> </tr>',
							'<tr><td style="width:25%;">Longitude:</td> <td style="width:75%;"><p style="padding-left:2px;" id="lngFld"/> ' + data.latitude + '</td> </tr>',
							'<tr><td style="width:25%;">Tahun:</td> <td style="width:75%;"><p style="padding-left:2px;" id="thnFld"/> ' + tahun + '</td> </tr>',
						'</table>',
						'<br/>',
						'<table style="min-width:800px;" class="aset table table-striped table-bordered">',
							'<thead>',
								'<tr>',
									'<th style="min-width:150px" rowspan="2">Jenis Aset:</th>',
									'<th colspan="2" style="text-align:center;">Saldo Awal</th>',
									'<th colspan="2" style="text-align:center;">Mutasi</th>',
									'<th style="text-align:center;">Penyusutan</th>',
									'<th colspan="2" style="text-align:center;">Saldo Akhir</th>',
								'</tr>',
								'<tr>',
									'<th style="min-width:50px; text-align:center;">Jumlah Aset</th>',
									'<th style="min-width:150px; text-align:center;">Total Nilai Aset</th>',
									'<th style="min-width:50px; text-align:center;">Jumlah Aset</th>',
									'<th style="min-width:150px; text-align:center;">Total Nilai Aset</th>',
									'<th style="min-width:150px; text-align:center;">Total Nilai Aset</th>',
									'<th style="min-width:50px; text-align:center;">Jumlah Aset</th>',
									'<th style="min-width:150px; text-align:center;">Total Nilai Aset</th>',
								'</tr>',
							'</thead>',
							'<tbody>',
								'<tr><td>' + awal[0].URAIAN + '</td><td style="text-align:right;">' + Number(awal[0].JUMLAH) + '</td><td style="text-align:right;">' + accounting.formatMoney(Number(awal[0].TOTAL), "Rp. ", 2, ".", ",") + '</td><td style="text-align:right;">' + Number(mutasi[0].JUMLAH) + '</td><td style="text-align:right;">' + accounting.formatMoney(Number(mutasi[0].TOTAL), "Rp. ", 2, ".", ",") + '</td><td style="text-align:right;">' + accounting.formatMoney(Number(susut[0].TOTAL), "Rp. ", 2, ".", ",") + '</td><td style="text-align:right;">' + (Number(awal[0].JUMLAH) + Number(mutasi[0].JUMLAH)) + '</td><td style="text-align:right;">' + accounting.formatMoney(Number(awal[0].TOTAL) + Number(mutasi[0].TOTAL) - Number(susut[0].TOTAL), "Rp. ", 2, ".", ",") + '</td></tr>',
								'<tr><td>' + awal[1].URAIAN + '</td><td style="text-align:right;">' + Number(awal[1].JUMLAH) + '</td><td style="text-align:right;">' + accounting.formatMoney(Number(awal[1].TOTAL), "Rp. ", 2, ".", ",") + '</td><td style="text-align:right;">' + Number(mutasi[1].JUMLAH) + '</td><td style="text-align:right;">' + accounting.formatMoney(Number(mutasi[1].TOTAL), "Rp. ", 2, ".", ",") + '</td><td style="text-align:right;">' + accounting.formatMoney(Number(susut[1].TOTAL), "Rp. ", 2, ".", ",") + '</td><td style="text-align:right;">' + (Number(awal[1].JUMLAH) + Number(mutasi[1].JUMLAH)) + '</td><td style="text-align:right;">' + accounting.formatMoney(Number(awal[1].TOTAL) + Number(mutasi[1].TOTAL) - Number(susut[1].TOTAL), "Rp. ", 2, ".", ",") + '</td></tr>',
								'<tr><td>' + awal[2].URAIAN + '</td><td style="text-align:right;">' + Number(awal[2].JUMLAH) + '</td><td style="text-align:right;">' + accounting.formatMoney(Number(awal[2].TOTAL), "Rp. ", 2, ".", ",") + '</td><td style="text-align:right;">' + Number(mutasi[2].JUMLAH) + '</td><td style="text-align:right;">' + accounting.formatMoney(Number(mutasi[2].TOTAL), "Rp. ", 2, ".", ",") + '</td><td style="text-align:right;">' + accounting.formatMoney(Number(susut[2].TOTAL), "Rp. ", 2, ".", ",") + '</td><td style="text-align:right;">' + (Number(awal[2].JUMLAH) + Number(mutasi[2].JUMLAH)) + '</td><td style="text-align:right;">' + accounting.formatMoney(Number(awal[2].TOTAL) + Number(mutasi[2].TOTAL) - Number(susut[2].TOTAL), "Rp. ", 2, ".", ",") + '</td></tr>',
								'<tr><td>' + awal[3].URAIAN + '</td><td style="text-align:right;">' + Number(awal[3].JUMLAH) + '</td><td style="text-align:right;">' + accounting.formatMoney(Number(awal[3].TOTAL), "Rp. ", 2, ".", ",") + '</td><td style="text-align:right;">' + Number(mutasi[3].JUMLAH) + '</td><td style="text-align:right;">' + accounting.formatMoney(Number(mutasi[3].TOTAL), "Rp. ", 2, ".", ",") + '</td><td style="text-align:right;">' + accounting.formatMoney(Number(susut[3].TOTAL), "Rp. ", 2, ".", ",") + '</td><td style="text-align:right;">' + (Number(awal[3].JUMLAH) + Number(mutasi[3].JUMLAH)) + '</td><td style="text-align:right;">' + accounting.formatMoney(Number(awal[3].TOTAL) + Number(mutasi[3].TOTAL) - Number(susut[3].TOTAL), "Rp. ", 2, ".", ",") + '</td></tr>',
								'<tr><td>' + awal[4].URAIAN + '</td><td style="text-align:right;">' + Number(awal[4].JUMLAH) + '</td><td style="text-align:right;">' + accounting.formatMoney(Number(awal[4].TOTAL), "Rp. ", 2, ".", ",") + '</td><td style="text-align:right;">' + Number(mutasi[4].JUMLAH) + '</td><td style="text-align:right;">' + accounting.formatMoney(Number(mutasi[4].TOTAL), "Rp. ", 2, ".", ",") + '</td><td style="text-align:right;">' + accounting.formatMoney(Number(susut[4].TOTAL), "Rp. ", 2, ".", ",") + '</td><td style="text-align:right;">' + (Number(awal[4].JUMLAH) + Number(mutasi[4].JUMLAH)) + '</td><td style="text-align:right;">' + accounting.formatMoney(Number(awal[4].TOTAL) + Number(mutasi[4].TOTAL) - Number(susut[4].TOTAL), "Rp. ", 2, ".", ",") + '</td></tr>',
								'<tr><td>' + awal[5].URAIAN + '</td><td style="text-align:right;">' + Number(awal[5].JUMLAH) + '</td><td style="text-align:right;">' + accounting.formatMoney(Number(awal[5].TOTAL), "Rp. ", 2, ".", ",") + '</td><td style="text-align:right;">' + Number(mutasi[5].JUMLAH) + '</td><td style="text-align:right;">' + accounting.formatMoney(Number(mutasi[5].TOTAL), "Rp. ", 2, ".", ",") + '</td><td style="text-align:right;">' + accounting.formatMoney(Number(susut[5].TOTAL), "Rp. ", 2, ".", ",") + '</td><td style="text-align:right;">' + (Number(awal[5].JUMLAH) + Number(mutasi[5].JUMLAH)) + '</td><td style="text-align:right;">' + accounting.formatMoney(Number(awal[5].TOTAL) + Number(mutasi[5].TOTAL) - Number(susut[5].TOTAL), "Rp. ", 2, ".", ",") + '</td></tr>',
								'<tr><td>' + awal[6].URAIAN + '</td><td style="text-align:right;">' + Number(awal[6].JUMLAH) + '</td><td style="text-align:right;">' + accounting.formatMoney(Number(awal[6].TOTAL), "Rp. ", 2, ".", ",") + '</td><td style="text-align:right;">' + Number(mutasi[6].JUMLAH) + '</td><td style="text-align:right;">' + accounting.formatMoney(Number(mutasi[6].TOTAL), "Rp. ", 2, ".", ",") + '</td><td style="text-align:right;">' + accounting.formatMoney(Number(susut[6].TOTAL), "Rp. ", 2, ".", ",") + '</td><td style="text-align:right;">' + (Number(awal[6].JUMLAH) + Number(mutasi[6].JUMLAH)) + '</td><td style="text-align:right;">' + accounting.formatMoney(Number(awal[6].TOTAL) + Number(mutasi[6].TOTAL) - Number(susut[6].TOTAL), "Rp. ", 2, ".", ",") + '</td></tr>',
								'<tr><td>' + awal[7].URAIAN + '</td><td style="text-align:right;">' + Number(awal[7].JUMLAH) + '</td><td style="text-align:right;">' + accounting.formatMoney(Number(awal[7].TOTAL), "Rp. ", 2, ".", ",") + '</td><td style="text-align:right;">' + Number(mutasi[7].JUMLAH) + '</td><td style="text-align:right;">' + accounting.formatMoney(Number(mutasi[7].TOTAL), "Rp. ", 2, ".", ",") + '</td><td style="text-align:right;">' + accounting.formatMoney(Number(susut[7].TOTAL), "Rp. ", 2, ".", ",") + '</td><td style="text-align:right;">' + (Number(awal[7].JUMLAH) + Number(mutasi[7].JUMLAH)) + '</td><td style="text-align:right;">' + accounting.formatMoney(Number(awal[7].TOTAL) + Number(mutasi[7].TOTAL) - Number(susut[7].TOTAL), "Rp. ", 2, ".", ",") + '</td></tr>',
								'<tr><td>' + awal[8].URAIAN + '</td><td style="text-align:right;">' + Number(awal[8].JUMLAH) + '</td><td style="text-align:right;">' + accounting.formatMoney(Number(awal[8].TOTAL), "Rp. ", 2, ".", ",") + '</td><td style="text-align:right;">' + Number(mutasi[8].JUMLAH) + '</td><td style="text-align:right;">' + accounting.formatMoney(Number(mutasi[8].TOTAL), "Rp. ", 2, ".", ",") + '</td><td style="text-align:right;">' + accounting.formatMoney(Number(susut[8].TOTAL), "Rp. ", 2, ".", ",") + '</td><td style="text-align:right;">' + (Number(awal[8].JUMLAH) + Number(mutasi[8].JUMLAH)) + '</td><td style="text-align:right;">' + accounting.formatMoney(Number(awal[8].TOTAL) + Number(mutasi[8].TOTAL) - Number(susut[8].TOTAL), "Rp. ", 2, ".", ",") + '</td></tr>',
							'</tbody>',
						'</table>',
					'</div>'
				].join("");

				info_window.setContent(content);

				google.maps.event.addListener(info_window, 'domready', function() {
					$(".skpd").closest("div").css("height", "300px");

					$("input#kode").autocomplete({
	    				minLength: 1,
	    				source: getAsetCariUrl(),
	    				select: function(e, u) {
	    					$(this).val(u.item.id);
	    					var table = $(this).closest("table");
	    					table.find("p#nama_asset").html(u.item.NAMA_BARANG);
	    					table.find("p#bidang").html(u.item.BIDANG);
	    					return false;
	    				}
	  				});
				});
				$("body").unmask();
			}, "json");
		};
	}


	function loadAset(callback) {
		var url = getAsetTampilkanUrl();

		$.get(url, {}, function(json){
			//  data berupa tipe point
			var data = json.markers;
			var iconBase = getBaseUrl() + 'assets/img/marker/';

			// load marker berupa point
			for(var i=0;i<data.length;i++) {
				var uid = guid();
				var marker = null;
				var icon = data[i].kode_kib.substr(0, 2);

				markers[i] = { kode: data[i].kode, nama_asset: data[i].nama_asset, latitude: data[i].latitude, longitude: data[i].longitude, kib: data[i].kib, kode_kib: data[i].kode_kib, skpd:data[i].skpd, nama_skpd:data[i].nama_skpd};
				markers[i].marker = marker = new google.maps.Marker({ map: map, icon: iconBase + icon +'.png'});

				//hanya menampilkan tanah, bangunan, dan jalan/jembatan.
				if(data[i].kode_kib.substr(0,2) == '01' || data[i].kode_kib.substr(0,2) == '03' || data[i].kode_kib.substr(0,2) == '04') {
					marker.kode_kib = data[i].kode_kib;
					marker.uid = uid;
					marker.kode = String(data[i].kode);
					marker.setPosition(new google.maps.LatLng(data[i].latitude, data[i].longitude));
					marker.tipe = 'POINT';
					google.maps.event.addListener(marker,'click', infoCallback(iwInfo, marker, null));
				}
			}


			// load marker berupa polyline
			var data2 = json.polys;
			for (var i=0; i<data2.length; i++) {
				var uid = guid();
				var poly = null, poly2 = null;

				polys[i] = { 
					kode: data2[i].kode, 
					nama_asset: data2[i].nama_asset,
					kib: data2[i].kib, 
					kode_kib: data2[i].kode_kib, 
					skpd:data2[i].skpd, 
					nama_skpd:data2[i].nama_skpd,
					path: data2[i].path
				};

				var polyCoordinates = [];
				for (var j=0; j<data2[i].path.length; j++){
					var point = new google.maps.LatLng(data2[i].path[j].lat, data2[i].path[j].lng);
					polyCoordinates.push(point);
				}

				var polyOptions = {
					path: polyCoordinates,
					strokeColor: '#000000',
					strokeOpacity: 1.0,
					strokeWeight: 5
				};

				var polyOptions2 = {
					path: polyCoordinates,
					strokeColor: '#FFFFFF',
					strokeOpacity: 1.0,
					strokeWeight: 9
				};

				polys[i].poly2 = poly2 = new google.maps.Polyline(polyOptions2);
				poly2.setMap(map);
				polys[i].poly = poly = new google.maps.Polyline(polyOptions);
				poly.setMap(map);

				poly.uid = uid;
				poly.tipe = 'LINESTRING';
				poly.kode = String(data2[i].kode);
				poly.kode_kib = data2[i].kode_kib;
				
				google.maps.event.addListener(poly,'click', infoCallback(iwInfo, poly, poly2));
			}

			callback();
		}, "json");

	}

	function loadSkpd() {
		var url = getAsetTampilkanSKPDUrl();

		$.get(url, {}, function(json){ 
			var data = json.result;
			var iconBase = getBaseUrl() + "assets/img/marker/08.png";

			for(var i=0;i<data.length;i++) {
				var marker = null;

				daftarSkpd[i] = { skpd: data[i].kode, nama_skpd: data[i].nama, latitude: data[i].latitude, longitude: data[i].longitude, aktif: data[i].aktif};
				daftarSkpd[i].marker = marker = new google.maps.Marker({ map: map, icon: iconBase});

				var info_window = new google.maps.InfoWindow({
					maxWidth: 700,
					maxHeight: 300
				});
				var uid = guid();

				marker.kode = String(data[i].kode);
				marker.setPosition(new google.maps.LatLng(data[i].latitude, data[i].longitude))

				google.maps.event.addListener(marker,'click', skpdCallback(info_window, marker));
			}
		}, "json");
	}


	var switchOnChanged = function(event, state) {
		google.maps.event.clearListeners(map, 'click');
		google.maps.event.clearListeners(map, 'rightclick');

		if ($(this).is(":checked")) {
			google.maps.event.addListener(map, 'click', function(e) {
				addMarker(map, e);
			});

			//input menggunakan latitude dan longitude
			google.maps.event.addListener(map, "rightclick", function(e){
				$("#dialog").dialog("open");
			});
		} else {
			var poly = null, poly2 = null;
			drawPolyline(map, function(e) { poly = e }, function(e) { poly2 = e });

			var isSaved = false;
			var uid = guid();

			google.maps.event.addListener(map, "rightclick", function(e){
				var path = poly.getPath();
				var pathSize = path.getLength();
				var infoLoc = new google.maps.LatLng(path.getAt(pathSize - 1).lat(), path.getAt(pathSize - 1).lng());

				iwAdd.setPosition(infoLoc);
				iwAdd.setContent(
					[
						'<div id="polydiv" style="width:400px; max-height:320px;">',
							'<table class="hasil" style="height:300px; width:400px;" id=\'' + uid + '\'>', 
								'<tr><td style="width:25%;">Kode Aset:</td><td style="width:75%;"><input id="kode_bidang" type="hidden" /><div class="ui-widget"><input style="padding-left:2px;" id="kode" size="25" /></div></td></tr>',
								'<tr><td style="width:25%;">Nama Aset:</td><td style="width:75%;"><p style="padding-left:2px; margin-top: 7px;" id="nama_asset">-</p></td></tr>',
								'<tr><td style="width:25%;">KIB:</td><td style="width:75%;"><p style="padding-left:2px; margin-top: 7px;" id="bidang">-</p></td></tr>',
								'<tr><td style="width:25%;">Nomor Unit:</td> <td style="width:75%;"><p style="padding-left:2px;" id="skpd"/>-</p></td> </tr>',
								'<tr><td style="width:25%;">Nama Unit:</td> <td style="width:75%;"><p style="padding-left:2px;" id="nama_skpd"/>-</p></td> </tr>',
								'<tr><td style="width:25%;">Lintasan:</td> <td style="width:75%;"><p style="padding-left:2px;" id="linFld">-</p></td> </tr>',
								'<tr><td colspan="2" style="text-align:center;"><input id="simpanBtn" type="button" value="Save & Close"/></td></tr>',
							'</table>',
						'</div>'
					].join("")
				);
				iwAdd.open(map);
			});

			google.maps.event.addListener(iwAdd, 'domready', function() {
				var path = poly.getPath();

				$("input#kode").autocomplete({
    				minLength: 1,
    				source: getAsetCariUrl(),
    				select: function(e, u) {
    					$(this).val(u.item.id);

    					var table = $(this).closest("table");
    					table.find("p#nama_asset").html(u.item.NAMA_BARANG + (u.item.MERK_ALAMAT != null ? ", " + u.item.MERK_ALAMAT : "") + (u.item.TIPE != null ? ", " + u.item.TIPE : ""));
    					table.find("input#kode_bidang").val(u.item.KODE_BIDANG);
    					table.find("p#bidang").html(u.item.BIDANG);
    					table.find("p#skpd").html(u.item.SKPD);
    					table.find("p#nama_skpd").html(u.item.NAMA_SKPD);

    					return false;
    				}
  				});

			
				$("#polydiv").parent().css("overflow", "visible");
				var tmp = _.map(path.getArray(), function(marker) { return {lat:marker.lat(), lng:marker.lng()} });
				$("table#" + uid).find("p[id=linFld]").html("<pre><code>" + library.json.prettyPrint(tmp) + "</code></pre>");


				$("table#" + uid).find("input[id=simpanBtn]").unbind().click(function() {
					var table = $(this).closest("table");
					var kode = table.find("input#kode").val();
					var nama_asset = table.find("p#nama_asset").html();
					var kib = table.find("p#bidang").html();
					var kode_kib = table.find("input#kode_bidang").val();
					var skpd = table.find("p#skpd").html();
					var nama_skpd = table.find("p#nama_skpd").html();
					var url = getSimpanPolylinesUrl();

					if(kode == '') {
						alert("Tolong masukkan kode aset");
					} else {
						$.post(url, {kode:kode, nama_asset: nama_asset, kib:kib, kode_kib:kode_kib, path:tmp, skpd:skpd, nama_skpd:nama_skpd}, function(r) {
							polys.push({ kode: kode, nama_asset: nama_asset, kib: kib, kode_kib: kode_kib, path: tmp, skpd:skpd, nama_skpd: nama_skpd, poly: poly, poly2: poly2});

							isSaved = true;
							iwAdd.close();

							var uid = guid();
							poly.uid = uid;
							poly.tipe = 'LINESTRING';
							poly.kode = kode;
							poly.kode_kib = kode_kib;

							google.maps.event.addListener(poly,'click', infoCallback(iwInfo, poly, poly2));


							// reset
							drawPolyline(map, function(e) { poly = e }, function(e) { poly2 = e });
						}, "json");
					}
				});
			});

			google.maps.event.addListener(iwAdd, 'closeclick', function() {
				poly.setMap(null);
				poly2.setMap(null);

				// reset
				drawPolyline(map, function(e) { poly = e }, function(e) { poly2 = e });
			});
		}
	}

	function initialize() {
		$('input[name="my-checkbox"]').on('switchChange.bootstrapSwitch', switchOnChanged);
		$('input[name="my-checkbox"]').bootstrapSwitch('state', true);

		//menampilkan list SKPD
	  	var url = getListSKPDUrl();
		$.get(url, {}, function(json){ 
			var listSkpd = json.result;
			
			$.each(listSkpd, function (key, value) {
    			$("#skpd-filter").append($('<option></option>').val(value.NOMOR_UNIT).html(value.NAMA_UNIT));
			});
		}, "json");

		//menampilkan list tahun
		for (var j=0; j<99; j++){
			$("#tahun-filter").append($('<option></option>').val(2000+j).html(2000+j));
		}
	}

	// filter
	function doFilter() {
		var filters = _.map($("input[name=filter]:checked"), function(x) {
			return $(x).attr("kib");
		});

		var filteredMarkers = null;

		var filterSkpd = $('#skpd-filter option:selected').val();
		if (filterSkpd === "") {
			filteredMarkers = markers;
		} else {
			filteredMarkers = _.filter(markers, function(x){ return x.skpd.substr(0, 11) ===  filterSkpd; });
		}

		_.each(markers, function(x) { x.marker.setMap(null); });
		
		shownMarkers = _.filter(filteredMarkers, function(x) { return !_.isEmpty(_.filter(filters, function(y) { return x.kode_kib.substr(0, 2) == y }))  });
		
		_.each(filteredMarkers, function(x) { if (_.find(filters, function(y) { return x.kode_kib.substr(0, 2) === y }) !== undefined) { if (x.marker.getMap() == null) x.marker.setMap(map); } else x.marker.setMap(null); })

		if (filterSkpd === "") {
			_.each(daftarSkpd, function(x) { x.marker.setMap(map); });
		} else {
			//if(_.each(filteredMarkers, function(x) { if (_.find(filters, function(y) { return x.kode_kib.substr(0, 2) === y }) !== undefined) { if (x.marker.getMap() == null) x.marker.setMap(map); } else x.marker.setMap(null); })
			_.each(daftarSkpd, function(x) { if (filterSkpd === x.skpd.substr(0, 11)) { if (x.marker.getMap() == null) x.marker.setMap(map); } else x.marker.setMap(null)});
		}

		// override autocomplete
		$("input#pencarian").autocomplete({
			minLength: 1,
			source: _.map(shownMarkers, function(x) { return { id:x.kode, label: x.nama_asset, marker: x.marker } }),
			select: function(e, u){
				$(this).val(u.item.label);
				map.panTo(u.item.marker.getPosition());
				map.setZoom(15);

				return false;
			}
	  	});
	}

	function filterSkpd(){return;
		var filterSkpd = $('#skpd-filter option:selected').val();
		//shownMarkers = _.filter(markers, function(x){ return x.skpd.substr(0, 11) ===  filterSkpd; });
		_.each(markers, function(x) { if (filterSkpd === x.skpd.substr(0, 11)) { if (x.marker.getMap() == null) x.marker.setMap(map); } else x.marker.setMap(null)});
		_.each(daftarSkpd, function(x) { if (filterSkpd === x.skpd.substr(0, 11)) { if (x.marker.getMap() == null) x.marker.setMap(map); } else x.marker.setMap(null)});
	}

	function addMarker(map, e) {
		var uid = guid();

		var marker = new google.maps.Marker({ map: map });
		var isSaved = false;

		marker.uid = uid;
		if (e != null) {
			marker.setPosition(e.latLng);
		} else {
			var lat = $("#lat").val();
			var lng = $("#long").val();

			marker.setPosition(new google.maps.LatLng(lat, lng));
		}

		iwAdd.setContent('<div style="width:400px; max-height:320px;">' + 
			'<table class="hasil" style="height:300px; width:400px;" id=\'' + uid + '\'>' + '<input id="kode_bidang" type="hidden" />' +
			'<tr><td style="width:25%;">Kode Aset:</td><td style="width:75%;"><div class="ui-widget"><input style="padding-left:2px;" id="kode" size="25" /></div></td></tr>' + 
			'<tr><td style="width:25%;">Nama Aset:</td><td style="width:75%;"><p style="padding-left:2px; margin-top: 7px;" id="nama_asset">-</p></td></tr>' +
			'<tr><td style="width:25%;">KIB:</td><td style="width:75%;"><p style="padding-left:2px; margin-top: 7px;" id="bidang">-</p></td></tr>' +
			'<tr><td style="width:25%;">Nomor Unit:</td> <td style="width:75%;"><p style="padding-left:2px;" id="skpd"/>-</p></td> </tr>' +
			'<tr><td style="width:25%;">Nama Unit:</td> <td style="width:75%;"><p style="padding-left:2px;" id="nama_skpd"/>-</p></td> </tr>' +
			'</select> </td></tr>' +
			'<tr><td style="width:25%;">Latitude:</td> <td style="width:75%;"><p style="padding-left:2px;" id="latFld">-</p></td> </tr>' +
			'<tr><td style="width:25%;">Longitude:</td> <td style="width:75%;"><p style="padding-left:2px;" id="lngFld"/>-</p></td> </tr>' +
			'<tr><td colspan="2" style="text-align:center;"><input id="simpanBtn" type="button" value="Save & Close"/></td></tr>' +
			'</div>');

		iwAdd.open(map,marker);

		google.maps.event.addListener(iwAdd, 'domready', function() {
			$("input#kode").autocomplete({
				minLength: 1,
				source: getAsetCariUrl(),
				select: function(e, u) {
					$(this).val(u.item.id);
					var table = $(this).closest("table");
					table.find("p#nama_asset").html(u.item.NAMA_BARANG + (u.item.MERK_ALAMAT != null ? ", " + u.item.MERK_ALAMAT : "") + (u.item.TIPE != null ? ", " + u.item.TIPE : ""));
					table.find("input#kode_bidang").val(u.item.KODE_BIDANG);
					table.find("p#bidang").html(u.item.BIDANG);
					table.find("p#skpd").html(u.item.SKPD);
					table.find("p#nama_skpd").html(u.item.NAMA_SKPD);
					return false;
				}
				});

			if (e != null) {
				$("table#" + uid).find("p[id=latFld]").html(e.latLng.lat());
				$("table#" + uid).find("p[id=lngFld]").html(e.latLng.lng());
			} else {
				var lat = $("#lat").val();
				var lng = $("#long").val();

				$("table#" + uid).find("p[id=latFld]").html(lat);
				$("table#" + uid).find("p[id=lngFld]").html(lng);
			}

			$("table#" + uid).unbind().find("input[id=simpanBtn]").click(function() {
				var table = $(this).closest("table");
				var kode = table.find("input#kode").val();
				var nama_asset = table.find("p#nama_asset").html();
				var kib = table.find("p#bidang").html();
				var kode_kib = table.find("input#kode_bidang").val();
				var skpd = table.find("p#skpd").html();
				var nama_skpd = table.find("p#nama_skpd").html();
				var iconBase = getBaseUrl() + 'assets/img/marker/';
				var icon = kode_kib.substr(0, 2);

				var latLng = marker.getPosition();
				var url = getAsetSimpanUrl();

				marker.kode = kode;

				if(kode == ''){
					alert("Tolong masukkan kode aset");
				}
				else{
					$.post(url, {kode:kode, nama_asset: nama_asset, kib:kib, kode_kib:kode_kib, latitude: latLng.lat(), longitude: latLng.lng(), skpd:skpd, nama_skpd:nama_skpd}, function(r) {
						isSaved = true;

						markers.push({ kode: kode, nama_asset: nama_asset, latitude: latLng.lat(), longitude: latLng.lng(), kib: kib, kode_kib: kode_kib, skpd:skpd, nama_skpd:nama_skpd, marker: marker});
						
						marker.kode_kib = kode_kib;
						marker.setIcon(iconBase + icon + '.png')
						marker.uid = uid;
						marker.kode = kode;
						marker.setPosition(new google.maps.LatLng(latLng.lat(), latLng.lng()));
						marker.tipe = 'POINT';

						google.maps.event.addListener(marker,'click', infoCallback(iwInfo, marker));
					}, "json");
				}
			});
		});


		google.maps.event.addListener(iwAdd, 'closeclick', function() {
			if(!isSaved){
				marker.setMap(null);
			}

			iwAdd.close();
		});
	}


	function drawPolyline(map, cbPoly, cbPoly2) {
		var polyCoordinates = [];
		var poly = poly2 = null;

		google.maps.event.clearListeners(map, 'click');

		// Add a listener for the click event
		google.maps.event.addListener(map, 'click', function(e) {
			if (poly != null) { poly.setMap(null); }
			if (poly2 != null) { poly2.setMap(null); }

			polyCoordinates.push(e.latLng);

			var polyOptions = {
				path: polyCoordinates,
				strokeColor: '#000000',
				strokeOpacity: 1.0,
				strokeWeight: 5
			};

			var polyOptions2 = {
				path: polyCoordinates,
				strokeColor: '#FFFFFF',
				strokeOpacity: 1.0,
				strokeWeight: 9
			};

			poly2 = new google.maps.Polyline(polyOptions2);
			poly2.setMap(map);

			poly = new google.maps.Polyline(polyOptions);
			poly.setMap(map);

			cbPoly2(poly2);
			cbPoly(poly);
		});
	}


	google.maps.event.addDomListener(window, 'load', initialize);

	// checkbox filter
	_.each($("input[name=filter]"), function(x) {
		$(x).change(doFilter);
	});

	// register onchange selectbox
	$("select#skpd-filter").change(function() {
		doFilter();
	});


	// setup dialog
	var dialog = $( "#dialog" ).dialog({
		autoOpen: false,
		height: 180,
		width: 280,
		modal: true,
		open: function() {
			$("#lat").val("");
			$("#long").val("");
		},
		buttons: {
			Tambahkan: function() {
				dialog.dialog( "close" );
				addMarker(map, null);
			},
			Batal: function() {
				dialog.dialog( "close" );
			}
		}
	});


	loadAset(doFilter);
	loadSkpd();
})