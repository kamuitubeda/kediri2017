jQuery(document).ready(function($) {
	$("body").attr("data-menu-position", "closed");

	var ul = $('#sidebar > ul');	
	var ul2 = $('#sidebar li.open ul');

	var initialized = false;

	var jPM = $.jPanelMenu({
	    menu: '#sidebar',
	    trigger: '#menu-trigger'
	});

	$(window).resize(function() {
		if($(window).width() > 480 && $(window).width() < 769) {	
			ul2.css({'display':'none'});
			ul.css({'display':'block'});
		}
		
		if($(window).width() <= 480) {
			if(!$('html').hasClass('jPanelMenu')) {
				jPM.on();
			}

			ul.css({'display':'none'});
			ul2.css({'display':'block'});

			if($(window).scrollTop() > 35) {
				$('body').addClass('fixed');
			} 
			$(window).scroll(function() {
				if($(window).scrollTop() > 35) {
					$('body').addClass('fixed');
				} else {
					$('body').removeClass('fixed');
				}
			});
		} else {
			jPM.off();
		}

		if($(window).width() > 768) {
			ul.css({'display':'block'});
			ul2.css({'display':'block'});
			$('#user-nav > ul').css({width:'auto',margin:'0'});
		}
	});
	
	if($(window).width() <= 480) {
		if($(window).scrollTop() > 35) {
			$('body').addClass('fixed');
		} 

		$(window).scroll(function() {
			if($(window).scrollTop() > 35) {
				$('body').addClass('fixed');
			} else {
				$('body').removeClass('fixed');
			}
		});

		jPM.on();
	}

	if($(window).width() > 480) {
		ul.css({'display':'block'});
		jPM.off();
	}

	if($(window).width() > 480 && $(window).width() < 769) {
		ul2.css({'display':'none'});
	}

	$(document).on('click', '.submenu > a',function(e){
		e.preventDefault();

		var submenu = $(this).siblings('ul');
		var li = $(this).parents('li');

		if($(window).width() > 480) {
			var submenus = $('#sidebar li.submenu ul');
			var submenus_parents = $('#sidebar li.submenu');
		} else {
			var submenus = $('#jPanelMenu-menu li.submenu ul');
			var submenus_parents = $('#jPanelMenu-menu li.submenu');
		}
		
		if(li.hasClass('open')) {
			if(($(window).width() > 768) || ($(window).width() <= 480)) {
				submenu.slideUp();
			} else {
				submenu.fadeOut(250);
			}

			li.removeClass('open');
		} else {
			if(($(window).width() > 768) || ($(window).width() <= 480)) {
				submenus.slideUp();			
				submenu.slideDown();
			} else {
				submenus.fadeOut(250);			
				submenu.fadeIn(250);
			}

			submenus_parents.removeClass('open');		
			li.addClass('open');	
		}
	});
});