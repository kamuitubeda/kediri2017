!function() {
var a = {
    delimiters: {
        thousands: '.',
        decimal: ','
    },
    abbreviations: {
        thousand: 'rb',
        million: 'jt',
        billion: 'm',
        trillion: 't'
    },
    ordinal: function(number) {
        return "";
    },
    currency: {
        symbol: 'Rp'
    }
};
},

function() {
    var a = {
        delimiters: {
            thousands: ".",
            decimal: "," 
        },
        abbreviations: {
            thousand: "rb",
            million: "jt",
            billion: "m",
            trillion: "t" },
        ordinal: function(a) {
            return ""
        },
        currency: { symbol: "Rp" }
    };
    "undefined" != typeof module && module.exports && (module.exports = a),
    "undefined" != typeof window && this.numeral && this.numeral.language && this.numeral.language("id-ID", a)
} ()