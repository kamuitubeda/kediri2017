-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 20, 2018 at 05:03 AM
-- Server version: 5.6.11
-- PHP Version: 5.5.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `kediri_2017`
--

-- --------------------------------------------------------

--
-- Table structure for table `secusergroup`
--

CREATE TABLE IF NOT EXISTS `secusergroup` (
  `uid` varchar(50) NOT NULL COMMENT 'User ID pengguna',
  `gid` int(11) NOT NULL COMMENT 'ID group',
  PRIMARY KEY (`uid`,`gid`),
  KEY `fk_secman_4` (`gid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `secusergroup`
--

INSERT INTO `secusergroup` (`uid`, `gid`) VALUES
('admin', 3),
('adminaset', 3),
('adminjhon', 3),
('adminkus', 3),
('admin', 7),
('adminaset', 7),
('adminjhon', 7),
('adminkus', 7),
('admin', 8),
('adminaset', 8),
('adminjhon', 8),
('adminkus', 8),
('arsip', 8),
('badas', 8),
('bakesbang', 8),
('banyakan', 8),
('bapemas', 8),
('bappeda', 8),
('bkd', 8),
('bkp3', 8),
('bpbd', 8),
('bpkad', 8),
('bppkb', 8),
('bptsp', 8),
('dinkes', 8),
('dinkop', 8),
('dinsos', 8),
('disbudpar', 8),
('disdukcapil', 8),
('dishub', 8),
('dishut', 8),
('diskominfo', 8),
('disnakan', 8),
('disnakertrans', 8),
('dispenda', 8),
('disperta', 8),
('dispora', 8),
('dkp', 8),
('dppe', 8),
('dppka', 8),
('dpu', 8),
('gampengrejo', 8),
('grogol', 8),
('gurah', 8),
('inspektorat', 8),
('kandangan', 8),
('kandat', 8),
('kayenkidul', 8),
('kepung', 8),
('klh', 8),
('kpm', 8),
('kppt', 8),
('kras', 8),
('kunjang', 8),
('lurahpare', 8),
('mojo', 8),
('ngadiluwih', 8),
('ngancar', 8),
('ngasem', 8),
('pagu', 8),
('papar', 8),
('pare', 8),
('plemahan', 8),
('plosoklaten', 8),
('puncu', 8),
('purwoasri', 8),
('ringinrejo', 8),
('rsud', 8),
('satpol', 8),
('sekda', 8),
('sekwan', 8),
('semen', 8),
('tarokan', 8),
('wates', 8),
('bpk1', 9),
('bpk2', 9),
('bpk3', 9);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
