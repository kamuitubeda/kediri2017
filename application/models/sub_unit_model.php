<?php
class Sub_unit_model extends MY_Model {
	
	public function __construct() {
		parent::__construct();

		$this->set_table_name('subunit');
        $this->set_pk(array('nomorsubunit'));
	}

	public function get_sub_unit($nomorsubunit){
		$q = $this->db->query("
			SELECT 
				*
			FROM subunit s
			WHERE 
				s.nomorsubunit = ?
			", array(
				$nomorsubunit
			));

		return $q->result_array();
	}

	public function get_kd_urusan($nomorsubunit) {
		$q = $this->db->query("
			SELECT 
				kdurusan
			FROM subunit s
			WHERE 
				s.nomorsubunit = ?
			", array(
				$nomorsubunit
			));

		return $q->row_array();
	}

	public function get_kd_bidang($nomorsubunit) {
		$q = $this->db->query("
			SELECT 
				kdbidang
			FROM subunit s
			WHERE 
				s.nomorsubunit = ?
			", array(
				$nomorsubunit
			));

		return $q->row_array();
	}

	public function get_kd_unit($nomorsubunit) {
		$q = $this->db->query("
			SELECT 
				kdunit
			FROM subunit s
			WHERE 
				s.nomorsubunit = ?
			", array(
				$nomorsubunit
			));

		return $q->row_array();
	}

	public function get_kd_sub_unit($nomorsubunit) {
		$q = $this->db->query("
			SELECT 
				kdsubunit
			FROM subunit s
			WHERE 
				s.nomorsubunit = ?
			", array(
				$nomorsubunit
			));

		return $q->row_array();
	}
}