<?php
class Pembayaran_model extends CI_Model {

	private $db;

	public function __construct() {
		parent::__construct();

		$this->db = $this->load->database("simda", TRUE);
	}

	public function find_spp_by_no_kontrak($search) {
		return $this->db->query("
			SELECT
				*
			FROM Ta_SPP_Kontrak
			WHERE
				UPPER(No_Kontrak) = UPPER(?)
		", array(
			$search
		))->result_array();
	}

	public function find_spp_on_uraian_spm($search){
		return $this->db->query("
			SELECT
				*
			FROM Ta_SPM
			WHERE
				UPPER(Uraian) like UPPER(?)
		", array(
				"%%" . $search . "%"
		))->result_array();
	}

	public function find_spm_by_no_spp($search) {
		return $this->db->query("
			SELECT
				*
			FROM Ta_SPM
			WHERE
				UPPER(No_SPP) = UPPER(?)
		", array(
			$search
		))->result_array();
	}

	public function find_spm_by_input($search) {
		return $this->db->query("
			SELECT
				*
			FROM Ta_SPM
			WHERE
				UPPER(No_SPM) = UPPER(?)
		", array(
			$search
		))->row_array();
	}

	public function find_spm_rinc_by_no_spm($search) {
		return $this->db->query("
			SELECT
				*
			FROM Ta_SPM_Rinc
			WHERE
				UPPER(No_SPM) = UPPER(?)
		", array(
			$search
		))->result_array();
	}

	public function find_sp2d_by_no_spm($search) {
		return $this->db->query("
			SELECT
				t.Tahun, t.No_SP2D, t.NO_SPM, CONVERT(VARCHAR(10), t.Tgl_SP2D, 111) as Tgl_SP2D, t.Kd_Bank, t.No_BKU, t.Nm_Penandatangan, t.Nip_Penandatangan, t.Jbt_Penandatangan, t.Keterangan
			FROM Ta_SP2D t
			WHERE
				UPPER(t.No_SPM) = UPPER(?)
		", array(
			$search
		))->result_array();
	}

	public function get_nilai_spm($search) {
		return $this->db->query("
			SELECT
				*
			FROM Ta_SPM_Rinc
			WHERE
				UPPER(No_SPM) = UPPER(?)
			", array(
				$search
		))->result_array();
	}

	public function get_uraian_belanja($rek1, $rek2, $rek3, $rek4, $rek5) {
		return $this->db->query("
			SELECT
				Nm_Rek_5
			FROM Ref_Rek_5
			WHERE
				UPPER(Kd_Rek_1) = (?)
				AND UPPER(Kd_Rek_2) = UPPER(?)
				AND UPPER(Kd_Rek_3) = UPPER(?)
				AND UPPER(Kd_Rek_4) = UPPER(?)
				AND UPPER(Kd_Rek_5) = UPPER(?)
			", array(
				$rek1, 
				$rek2, 
				$rek3, 
				$rek4, 
				$rek5
		))->row_array();
	}
}