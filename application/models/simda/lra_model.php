<?php
class Lra_model extends CI_Model {

	private $db;

	public function __construct() {
		parent::__construct();

		$this->db = $this->load->database("simda", TRUE);
	}

	public function get_lra($urusan, $bidang, $unit) {
		return $this->db->query("{CALL RptLRA('2014', ?, ?, ?, NULL, '12/31/2014') }", array(
			$urusan,
			$bidang,
			$unit
		))->result_array();
	}

	public function get_all_unit() {
		return $this->db->query("SELECT * FROM Ref_Unit")->result_array();
	}
}