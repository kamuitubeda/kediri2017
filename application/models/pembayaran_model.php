<?php
class Pembayaran_model extends MY_Model {
	public function __construct() {
		parent::__construct();

		$this->set_table_name('pembayaran');
		$this->set_pk(array('pbid'));
	}

	public function clear($jpid) {
		$this->db->query("DELETE FROM pembayaran WHERE jpid = ?", $jpid);
		// $this->db->delete('pembayaran', array('jpid' => $jpid));
		// return $this->db->affected_rows() > 0;
	}
}