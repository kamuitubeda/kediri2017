<?php
class Kib_pengadaan_model extends MY_Model {
	
	public function __construct() {
		parent::__construct();

		$this->set_table_name('kib');
        $this->set_pk(array('kbid'));
	}

	public function find_all_aset($jpid){
		$q = $this->db->query("
			SELECT 
				*
			FROM kib k
			WHERE 
				k.jpid = ?
			", array(
				$jpid
			));

		return $q->result_array();
	}

	public function find_all_aset_tetap($jpid) {
		$q = $this->db->query("
			SELECT
				*
			FROM kib k
			WHERE
				k.kbjenisaset = 'A' 
				AND k.kbkoderekneraca NOT LIKE '1.3.6%'
				AND k.jpid = ?
		", array(
			$jpid
		));

		return $q->result_array();
	}

	public function find_all_penambahan_nilai($jpid) {
		$q = $this->db->query("
			SELECT
				*
			FROM kib k
			WHERE
				k.kbjenisaset = 'R' 
				AND k.kbkoderekneraca NOT LIKE '1.3.6%'
				AND k.jpid = ?
		", array(
			$jpid
		));

		return $q->result_array();
	}

	public function find_all_kdp($jpid) {
		$q = $this->db->query("
			SELECT
				*
			FROM kib k
			WHERE
				k.kbkoderekneraca LIKE '1.3.6%'
				AND k.jpid = ?
		", array(
			$jpid
		));

		return $q->result_array();
	}

	public function find_all_jasa($jpid) {
		$q = $this->db->query("
			SELECT
				*
			FROM kib k
			WHERE
				k.kbjenisaset = 'J' 
				AND k.jpid = ?
		", array(
			$jpid
		));

		return $q->result_array();
	}

	public function find_all_pakai_habis($jpid) {
		$q = $this->db->query("
			SELECT
				*
			FROM kib k
			WHERE
				k.kbjenisaset = 'H' 
				AND k.jpid = ?
		", array(
			$jpid
		));

		return $q->result_array();
	}

	public function find_all_bantuan($jpid) {
		$q = $this->db->query("
			SELECT
				*
			FROM kib k
			WHERE
				k.kbjenisaset = 'B' 
				AND k.jpid = ?
		", array(
			$jpid
		));

		return $q->result_array();
	}

}