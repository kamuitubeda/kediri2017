<?php
class Subsub_kelompok_model extends CI_Model{

	private $db;

	public function __construct() {
		parent::__construct();

		$this->db = $this->load->database("simbada", TRUE);
	}

	public function find_all() {
		return $this->db->get("SUBSUB_KELOMPOK")->result_array();
	}

	public function find_by_kode_sub_kel_at($kode_sub_kel_at, $filter = '', $offset = 0) {
		$per_page = 10;
		$limit = "FIRST $per_page ";
		if ($offset > 0) {
			$limit .= "SKIP " . ($per_page * 10) . " ";
		}

		return $this->db->query("
			SELECT $limit
				ssk.*,
				sk.DESKRIPSI as NAMA_SUB_KELOMPOK,
				k.DESKRIPSI as NAMA_KELOMPOK,
				b.DESKRIPSI as NAMA_BIDANG,
				g.GOLONGAN as NAMA_GOLONGAN
			FROM SUBSUB_KELOMPOK ssk
			JOIN SUB_KELOMPOK sk
				ON sk.KODE_SUB_KELOMPOK = ssk.KODE_SUB_KELOMPOK
			JOIN KELOMPOK k
				ON k.KODE_KELOMPOK = sk.KODE_KELOMPOK
			JOIN BIDANG b
				ON b.KODE_BIDANG = k.KODE_BIDANG
			JOIN GOLONGAN g
				ON g.KODE_GOLONGAN = b.KODE_GOLONGAN
			WHERE 
				ssk.KODE_SUB_KEL_AT LIKE ?
				AND (
					ssk.KODE_SUB_SUB_KELOMPOK LIKE ?
					OR UPPER(ssk.DESKRIPSI) LIKE UPPER(?)
				)
		", array(
			$kode_sub_kel_at . "%", 
			$filter . '%',
			$filter . '%'
		))->result_array();
	}

	public function find_by_range_kode_sub_kel_at($kode_sub_kel_at_awal, $kode_sub_kel_at_akhir, $filter = '', $offset = 0) {
		$per_page = 10;
		$limit = "FIRST $per_page ";
		if ($offset > 0) {
			$limit .= "SKIP " . ($per_page * 10) . " ";
		}

		return $this->db->query("
			SELECT $limit
				ssk.*,
				sk.DESKRIPSI as NAMA_SUB_KELOMPOK,
				k.DESKRIPSI as NAMA_KELOMPOK,
				b.DESKRIPSI as NAMA_BIDANG,
				g.GOLONGAN as NAMA_GOLONGAN
			FROM SUBSUB_KELOMPOK ssk
			JOIN SUB_KELOMPOK sk
				ON sk.KODE_SUB_KELOMPOK = ssk.KODE_SUB_KELOMPOK
			JOIN KELOMPOK k
				ON k.KODE_KELOMPOK = sk.KODE_KELOMPOK
			JOIN BIDANG b
				ON b.KODE_BIDANG = k.KODE_BIDANG
			JOIN GOLONGAN g
				ON g.KODE_GOLONGAN = b.KODE_GOLONGAN
			WHERE 
				LEFT(ssk.KODE_SUB_KEL_AT, 8) BETWEEN ? AND ?
				AND (
					ssk.KODE_SUB_SUB_KELOMPOK LIKE ?
					OR UPPER(ssk.DESKRIPSI) LIKE UPPER(?)
				)
		", array(
			$kode_sub_kel_at_awal, 
			$kode_sub_kel_at_akhir, 
			$filter . '%',
			$filter . '%'
		))->result_array();
	}
	
}