<?php
class Golongan_model extends CI_Model{

	private $db;

	public function __construct() {
		$this->db = $this->load->database("simbada", TRUE);
	}

	public function find_all() {
		return $this->db->get("GOLONGAN")->result_array();
	}

}