<?php
class Kamus_spk_model extends CI_Model{

	private $db;

	public function __construct() {
		parent::__construct();

		$this->db = $this->load->database("simbada", TRUE);
	}

	public function get_list($tahun, $nomor_lokasi, $filter, $limit = 0, $offset = 0) {
		$pg = "";
		$andTahun = "";

		if ($limit > 0) $pg .= "FIRST $limit";
		if ($offset > 0) $pg .= "SKIP $offset";

		if ($tahun > 0) {
			$andTahun = "AND TAHUN_SPJ = $tahun";
		}

		return $this->db->query("
			SELECT $pg
				*
			FROM KAMUS_SPK
			WHERE
				(
					LOWER(REKANAN) LIKE LOWER(?)
					OR LOWER(NO_SPK_SP_DOKUMEN) LIKE LOWER(?)
					OR LOWER(DESKRIPSI_SPK_DOKUMEN) LIKE LOWER(?)
				)
				AND LOWER(NOMOR_SUB_UNIT) LIKE LOWER(?)
				$andTahun
			ORDER BY TGL_SPK_SP_DOKUMEN DESC
		", array(
			"%".$filter."%",
			"%".$filter."%",
			"%".$filter."%",
			$nomor_lokasi."%"
		))->result_array();
	}

	public function count($tahun, $nomor_lokasi, $filter) {
		$andTahun = "";
		if ($tahun > 0) {
			$andTahun = "AND TAHUN_SPJ = $tahun";
		}

		$tmp = $this->db->query("
			SELECT
				COUNT(*) as c
			FROM KAMUS_SPK
			WHERE
				(
					LOWER(REKANAN) LIKE LOWER(?)
					OR LOWER(NO_SPK_SP_DOKUMEN) LIKE LOWER(?)
					OR LOWER(DESKRIPSI_SPK_DOKUMEN) LIKE LOWER(?)
				)
				AND LOWER(NOMOR_SUB_UNIT) LIKE LOWER(?)
				$andTahun
		", array(
			"%".$filter."%",
			"%".$filter."%",
			"%".$filter."%",
			$nomor_lokasi."%"
		))->row_array();

		return $tmp["C"];
	}

	public function read($id_kontrak) {
		return $this->db->query("SELECT * FROM KAMUS_SPK WHERE ID_KONTRAK = ?", array($id_kontrak))->row_array();
	}

	public function delete($id_kontrak) {
		return $this->db->query("DELETE FROM KAMUS_SPK WHERE ID_KONTRAK = ?", array($id_kontrak));
	}

	public function find_spk($search){
		return $this->db->query("
			SELECT FIRST 10
			*
			FROM KAMUS_SPK
			WHERE
				(
					LOWER(REKANAN) LIKE LOWER(?)
					OR LOWER(NO_SPK_SP_DOKUMEN) LIKE LOWER(?)
					OR LOWER(DESKRIPSI_SPK_DOKUMEN) LIKE LOWER(?)
				)
			ORDER BY TGL_SPK_SP_DOKUMEN DESC
		", array(
			"%".$search."%",
			"%".$search."%",
			"%".$search."%",
		))->result_array();
	}

	public function create($id_kontrak, $nomor_sub_unit, $id_kegiatan, $no_spk_sp_dokumen, $tgl_spk_sp_dokumen, $deskripsi_spk_dokumen, $nilai_spk, $tahun_spj, $rekanan, $alamat_rekanan, $termin, $estimasi_termin, $addendum, $no_add, $tgl_add, $uraian_add, $nilai_add, $tahun_add, $jml_termin_add) {
		//var_dump($id_kontrak, $nomor_sub_unit, $id_kegiatan, $no_spk_sp_dokumen, $tgl_spk_sp_dokumen, $deskripsi_spk_dokumen, $nilai_spk, $tahun_spj, $rekanan, $alamat_rekanan, $termin, $estimasi_termin, $addendum, $no_add, $tgl_add, $uraian_add, $nilai_add, $tahun_add, $jml_termin_add);
		return $this->db->query("
			INSERT INTO KAMUS_SPK (
				ID_KONTRAK,
				NOMOR_SUB_UNIT, 
				ID_KEGIATAN, 
				NO_SPK_SP_DOKUMEN,
				TGL_SPK_SP_DOKUMEN,
				DESKRIPSI_SPK_DOKUMEN,
				NILAI_SPK,
				TAHUN_SPJ,
				REKANAN,
				ALAMAT_REKANAN,
				TERMIN,
				ESTIMASI_TERMIN,
				ADDENDUM,
				NO_ADD,
				TGL_ADD,
				URAIAN_ADD,
				NILAI_ADD,
				TAHUN_ADD,
				JML_TERMIN_ADD
			) VALUES ( 
				?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?
			)
		", array(
			$id_kontrak,
			$nomor_sub_unit, 
			$id_kegiatan, 
			$no_spk_sp_dokumen,
			$tgl_spk_sp_dokumen,
			$deskripsi_spk_dokumen,
			$nilai_spk,
			$tahun_spj,
			$rekanan,
			$alamat_rekanan,
			$termin,
			$estimasi_termin,
			$addendum,
			$no_add,
			$tgl_add,
			$uraian_add,
			$nilai_add,
			$tahun_add,
			$jml_termin_add
		));
	}

	public function update($id_kontrak, $nomor_sub_unit, $id_kegiatan, $no_spk_sp_dokumen, $tgl_spk_sp_dokumen, $deskripsi_spk_dokumen, $nilai_spk, $tahun_spj, $rekanan, $alamat_rekanan, $termin, $estimasi_termin, $addendum, $no_add, $tgl_add, $uraian_add, $nilai_add, $tahun_add, $jml_termin_add) {
		return $this->db->query("
			UPDATE KAMUS_SPK SET
				NOMOR_SUB_UNIT = ?,
				ID_KEGIATAN = ?,
				NO_SPK_SP_DOKUMEN = ?,
				TGL_SPK_SP_DOKUMEN = ?,
				DESKRIPSI_SPK_DOKUMEN = ?,
				NILAI_SPK = ?,
				TAHUN_SPJ = ?,
				REKANAN = ?,
				ALAMAT_REKANAN = ?,
				TERMIN = ?,
				ESTIMASI_TERMIN = ?,
				ADDENDUM = ?,
				NO_ADD = ?,
				TGL_ADD = ?,
				URAIAN_ADD = ?,
				NILAI_ADD = ?,
				TAHUN_ADD = ?,
				JML_TERMIN_ADD = ?
			WHERE
				ID_KONTRAK = ?
		", array(
			$nomor_sub_unit, 
			$id_kegiatan, 
			$no_spk_sp_dokumen,
			$tgl_spk_sp_dokumen,
			$deskripsi_spk_dokumen,
			$nilai_spk,
			$tahun_spj,
			$rekanan,
			$alamat_rekanan,
			$termin,
			$estimasi_termin,
			$addendum,
			$no_add,
			$tgl_add,
			$uraian_add,
			$nilai_add,
			$tahun_add,
			$jml_termin_add,
			$id_kontrak
		));
	}

}