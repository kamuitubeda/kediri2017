<?php
class Kamus_lokasi_model extends CI_Model{

	private $db;

	public function __construct() {
		parent::__construct();

		$this->db = $this->load->database("simbada", TRUE);
	}

	public function find_all(){
		return $this->db->query("SELECT * FROM KAMUS_LOKASI")->result_array();
	}

	public function find_all_unit() {
		return $this->db->query("SELECT DISTINCT NOMOR_SUB_UNIT FROM KAMUS_LOKASI")->result_array();
	}

	public function find_by_nomor_lokasi($nomor_lokasi) {
		return $this->db->query("SELECT * FROM KAMUS_LOKASI WHERE NOMOR_LOKASI = ?", array($nomor_lokasi))->row_array();
	}
	
}