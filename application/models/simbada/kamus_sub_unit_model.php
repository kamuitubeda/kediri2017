<?php
class Kamus_sub_unit_model extends CI_Model{

	private $db;

	public function __construct() {
		parent::__construct();

		$this->db = $this->load->database("simbada", TRUE);
	}

	public function find_all(){
		return $this->db->query("SELECT * FROM KAMUS_SUB_UNIT")->result_array();
	}

	public function find_all_unit() {
		return $this->db->query("SELECT DISTINCT NOMOR_SUB_UNIT FROM KAMUS_SUB_UNIT")->result_array();
	}

	public function find_by_nomor_lokasi($nomor_lokasi) {
		return $this->db->query("SELECT * FROM KAMUS_SUB_UNIT WHERE NOMOR_LOKASI = ?", array($nomor_lokasi))->row_array();
	}
	
	public function get_sub_unit_name($nomor_sub_unit) {
		return $this->db->query("SELECT NAMA_SUB_UNIT FROM KAMUS_SUB_UNIT WHERE NOMOR_SUB_UNIT = ?", array($nomor_sub_unit))->row_array();
	}
}