<?php
class Masa_manfaat_model extends MY_Model {
	
	public function __construct() {
        parent::__construct();
        
        $this->set_table_name('masa_manfaat');
        $this->set_pk(array('kelompok'));
    }

    public function get_mm_by_kelompok($kelompok) {
		return $this->db->query("
			SELECT * FROM masa_manfaat WHERE kelompok = ?
		", array($kelompok))->row_array();
	}

}