<?php
class Rekap_penyusutan_model extends MY_Model {
	
	public function __construct() {
        parent::__construct();
        
        $this->set_table_name('rekap_penyusutan');
        $this->set_pk(array('no'));
    }

    public function get_all() {
		return $this->db->query("
			SELECT * FROM rekap_penyusutan
			ORDER BY nomor_unit
		", array())->result_array();
	}

	public function get_rekap($kib, $jenis_aset, $kode_kepemilikan) {
		return $this->db->query("
			SELECT * FROM rekap_penyusutan
			WHERE bidang_barang = ?
			AND jenis_aset = ?
			AND kode_kepemilikan = ?
			ORDER BY nomor_unit
		", array($kib, $jenis_aset, $kode_kepemilikan))->result_array();
	}

	public function get_all_by_unit($nomor_unit) {
		return $this->db->query("
			SELECT * FROM rekap_penyusutan WHERE nomor_unit like ?
		", array($nomor_unit))->result_array();
	}

	public function cek($no) {
		return $this->db->query("
			SELECT * 
			FROM rekap_penyusutan 
			WHERE no = ? 
		", array($no))->result_array();
	}

	public function insert_data($data) {
		$this->db->insert('rekap_penyusutan', $data);
		return $this->db->affected_rows() > 0;
	}

	public function update_data($data) {
		$this->db->where('nomor_unit', $data["nomor_unit"]);
		$this->db->where('tahun', $data["tahun"]);
		$this->db->where('bidang_barang', $data["bidang_barang"]);
		$this->db->where('jenis_aset', $data["jenis_aset"]);
		$this->db->where('kode_kepemilikan', $data["kode_kepemilikan"]);

		$this->db->update('rekap_penyusutan', $data);
		return $this->db->affected_rows() > 0;
	}
}