<?php
class Skpd_user_model extends MY_Model {
	
	public function __construct() {
        parent::__construct();
        
        $this->set_table_name('skpd_user');
        $this->set_pk(array('skpd_user_id'));
    }

	public function get_skpd_user($user) {
		$q = $this->db->query("
			SELECT 
				*
			FROM skpd_user s
			WHERE 
				s.uid like ?
			ORDER BY s.nomor_skpd
			", array(
				"%" . $user . "%"
			));

		return $q->result_array();
	}

	public function get_all_skpd() {
		$this->db->select("*");
		$this->db->from("kamus_unit");
		$this->db->order_by("nomor_unit");
        return $this->db->get()->result_array();
	}

	public function update_skpd_user($user, $skpd) {
		$param = array();
		
		if(!empty($skpd)){
			foreach ($skpd as $value) {
				array_push($param, array('uid' => $user, 'nomor_skpd' => $value));
			}
		}

		$this->db->delete('skpd_user', array('uid' => $user));
		if(!empty($param)){
			$this->db->insert_batch('skpd_user', $param);
		}
	}
}