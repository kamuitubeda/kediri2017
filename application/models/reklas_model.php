<?php
class Reklas_model extends MY_Model {
	
	public function __construct() {
        parent::__construct();
        
        $this->set_table_name('reklas');
        $this->set_pk(array('no'));
    }

    public function get_all() {
		return $this->db->query("
			SELECT * FROM reklas
		", array())->result_array();
	}

	public function get_all_by_sub_unit($nomor_sub_unit) {
		return $this->db->query("
			SELECT * FROM reklas WHERE nomor_sub_unit = ?
		", array($nomor_sub_unit))->result_array();
	}

	public function get_sub_unit() {
		return $this->db->query("
			SELECT DISTINCT nomor_sub_unit, nama_sub_unit FROM reklas
		", array())->result_array();
	}
}