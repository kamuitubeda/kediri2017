<?php
class Temp_penambahan_nilai_model extends MY_Model {
	
	public function __construct() {
		parent::__construct();

		$this->set_table_name('temp_penambahan_nilai');
        $this->set_pk(array('pnid'));
	}

	public function delete($pnid) {
        return $this->db->query("DELETE FROM temp_penambahan_nilai WHERE LOWER(pnid) = ?", array($pnid));
    }

	public function get_all_aset_tambah() {
		$query = $this->db->get('temp_penambahan_nilai');
		return $query->result_array();
	}

	public function get_all_aset_tambah_nama_kosong() {
		$this->db->select('*');
		$this->db->from('temp_penambahan_nilai');
		$this->db->where('namapn', NULL);

		$query = $this->db->get();
		return $query->result_array();
	}

	public function get_aset_tambah($aset_induk) {
		return $this->db->query("
			SELECT 
				* 
			FROM temp_penambahan_nilai 
			WHERE 
				LOWER(asetindukid) = LOWER(?)
			ORDER BY
				tahunpn ASC
		", array($aset_induk))->result_array();
	}

	public function tambahkan($register, $nama_pengadaan, $tahun_pengadaan, $nilai_pengadaan, $lokasi_pengadaan, $subunit_pengadaan, $register_induk, $kib_renov, $sub_kel_at_renov, $tahun_induk, $nama_induk, $kib_induk, $sub_kel_at_induk) {
		$data = array(
			'pnid' => $register,
			'namapn' => $nama_pengadaan,
			'tahunpn' => $tahun_pengadaan,
			'nilaipn' => $nilai_pengadaan,
			'lokasipn' => $lokasi_pengadaan,
			'subunitpn' => $subunit_pengadaan,
			'asetindukid' => $register_induk,
			'bidangpn' => $kib_renov,
			'kodepn' => $sub_kel_at_renov,
			'namainduk' => $nama_induk,
			'tahuninduk' => $tahun_induk,
			'bidanginduk' => $kib_induk,
			'kodeinduk' => $sub_kel_at_induk
		);

		$this->db->insert('temp_penambahan_nilai', $data);
		return $this->db->affected_rows() > 0;
	}

	public function get_list_by_nomor_unit($nomor_unit) {
		return $this->db->query("
			SELECT 
				*
			FROM temp_penambahan_nilai
			WHERE
				LOWER(lokasipn) = LOWER(?)
		", array($nomor_unit))->result_array();
	}

	public function get_list_by_nomor_sub_unit($nomor_sub_unit) {
		return $this->db->query("
			SELECT 
				*
			FROM temp_penambahan_nilai
			WHERE
				LOWER(subunitpn) = LOWER(?)
		", array($nomor_sub_unit))->result_array();
	}

	public function update_data_renov($no_register_renov, $nama_renov, $tahun_renov, $nilai_renov, $lokasi_renov, $subunit_renov, $sub_kel_at_renov, $kib_renov, $nama_induk, $tahun_induk, $sub_kel_at_induk, $kib_induk) {
		$data = array(
			'namapn' => $nama_renov,
			'tahunpn' => $tahun_renov,
			'nilaipn' => $nilai_renov,
			'lokasipn' => $lokasi_renov,
			'subunitpn' => $subunit_renov,
			'kodepn' => $sub_kel_at_renov,
			'bidangpn' => $kib_renov,
			'namainduk' => $nama_induk,
			'tahuninduk' => $tahun_induk,
			'kodeinduk' => $sub_kel_at_induk,
			'bidanginduk' => $kib_induk
		);

		$this->db->where('pnid', $no_register_renov);
		$this->db->update('temp_penambahan_nilai', $data);
		return $this->db->affected_rows() > 0;
	}
}