<?php
class Aset_model extends CI_Model{
	public function __construct()
	{
		$this->load->database();
	}

	public function eksis($kode){
		$this->db->select('kode, nama_asset');
		$this->db->where('kode', $kode);
		$query = $this->db->get('asset');

		return $query->num_rows() > 0;
	}

	public function read($kode) {
		$this->db->select('kode, nama_asset, kib, kode_kib, X(geom) as latitude, Y(geom) as longitude, skpd, nama_skpd');
		$this->db->where('kode', $kode);
		$query = $this->db->get('asset');

		return $query->row_array();
	}

	public function read_skpd($kode) {
		$this->db->where('kode', $kode);
		$query = $this->db->get('skpd');

		return $query->row_array();
	}

	public function find_by($search){
		return $this->db->query("
			SELECT * 
			FROM asset
			WHERE 
				(
					UPPER(kode) LIKE UPPER(?) 
					OR UPPER(nama_asset) LIKE UPPER(?)
					OR UPPER(kib) LIKE UPPER(?)
					OR UPPER(kode_kib) LIKE UPPER(?)
					OR UPPER(latitude) LIKE UPPER(?)
					OR UPPER(longitude) LIKE UPPER(?)
					OR UPPER(skpd) LIKE UPPER(?)
				)
			LIMIT 5
		", array(
			"%" . $search . "%",
			"%" . $search . "%",
			"%" . $search . "%",
			"%" . $search . "%",
			"%" . $search . "%",
			"%" . $search . "%",
			"%" . $search . "%",
		))->result_array();
	}

	public function get_aset($type = 'POINT')
	{
		if ($type === 'POINT') {
			$this->db->select('kode, nama_asset, kib, kode_kib, skpd, nama_skpd, X(geom) as latitude, Y(geom) as longitude');
			$this->db->where('NumPoints(geom) IS NULL');
		} else if ($type === 'LINESTRING') {
			$this->db->select('kode, nama_asset, kib, kode_kib, skpd, nama_skpd, AsText(geom) as wkt');
			$this->db->where('NumPoints(geom) IS NOT NULL');
		}

		$query = $this->db->get('asset');
		return $query->result_array();
	}

	public function set_aset($kode, $nama_asset, $kib, $kode_kib, $skpd, $nama_skpd, $path)
	{
		$data = array(
			'kode' => $kode,
			'nama_asset' => $nama_asset,
			'kib' => $kib,
			'kode_kib' => $kode_kib,
			// 'latitude' => (double)$latitude,
			// 'longitude' => (double)$longitude,
			'skpd' => $skpd,
			'nama_skpd' => $nama_skpd,
		);

		$tmp = join(", ", __::map($path, function($e) { return $e["lat"] . " " . $e["lng"]; }));
		$c = count($path);

		if ($c === 1) {
			$this->db->set('geom', 'GeomFromText(\'POINT(' . $tmp . ')\')', FALSE);
		} else if ($c > 1) {
			$this->db->set('geom', 'GeomFromText(\'LineString(' . $tmp . ')\')', FALSE);
		}

		$this->db->insert('asset', $data);

		return $this->db->affected_rows() > 0;
	}

	public function row_delete($kode)
	{
		$this->db->delete('asset', array('kode' => $kode));
		return $this->db->affected_rows() > 0;
	}

	public function update_data($old_kode, $kode, $nama_asset, $kib, $kode_kib)
	{
		$data = array(
			'kode' => $kode,
			'nama_asset' => $nama_asset, 
			'kib' => $kib,
			'kode_kib' => $kode_kib
		);

		$this->db->where('kode', $old_kode);
		$this->db->update('asset', $data);

		return $this->db->affected_rows() > 0;
	}

	public function set_skpd($kode, $nama, $latitude, $longitude, $aktif)
	{
		$data = array(
			'kode' => $kode,
			'nama' => $nama,
			'latitude' => (double)$latitude,
			'longitude' => (double)$longitude,
			'aktif' => $aktif
		);

		return $this->db->insert('skpd', $data);
	}

	public function get_skpd()
	{
		$query = $this->db->get('skpd');
		return $query->result_array();
	}

}