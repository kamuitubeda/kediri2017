<?php
class User_organisasi_model extends MY_Model {
	
	public function __construct() {
        parent::__construct();
        
        $this->set_table_name('secorg');
        $this->set_pk(array('oid'));
    }

	public function get_skpd_organisasi($oid) {
		$q = $this->db->query("
			SELECT 
				*
			FROM secorg s
			WHERE 
				s.oid = ?
			", array(
				$oid
			));

		return $q->row_array();
	}
}