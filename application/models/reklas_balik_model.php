<?php
class Reklas_balik_model extends MY_Model {
	
	public function __construct() {
        parent::__construct();
        
        $this->set_table_name('reklas_balik');
        $this->set_pk(array('no'));
    }

    public function get_all() {
		return $this->db->query("
			SELECT * FROM reklas_balik
		", array())->result_array();
	}

	public function get_all_by_sub_unit($nomor_sub_unit) {
		return $this->db->query("
			SELECT * FROM reklas_balik WHERE nomor_sub_unit = ?
		", array($nomor_sub_unit))->result_array();
	}

	public function get_sub_unit() {
		return $this->db->query("
			SELECT DISTINCT nomor_sub_unit, nama_sub_unit FROM reklas_balik
		", array())->result_array();
	}
}