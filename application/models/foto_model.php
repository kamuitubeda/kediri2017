<?php
class Foto_model extends MY_Model {
	
	public function __construct() {
        parent::__construct();
        
        $this->set_table_name('foto_asset');
        $this->set_pk(array('id'));
    }

    public function get_list_by_asset_id($asset_id) {
    	$this->db->where(array('asset_id' => $asset_id,));
    	return parent::get_list(array(), 0, 0, array());
    }

}