<?php
class Jurnal_pengadaan_model extends MY_Model {

	public function __construct() {
        parent::__construct();
        
        $this->set_table_name('jurnal_pengadaan');
        $this->set_pk(array('jpid'));
    }

    public function get_list($nomor_lokasi, $filter, $limit = 0, $offset = 0) {
        $pg = "";
        $post = "";

        if ($limit > 0) $pg .= "LIMIT $limit";
        if ($offset > 0) $pg .= " OFFSET $offset";

        // if($posted === 1){
        //     $post = "POSTED IS NOT NULL";
        // } else if($posted === 0){
        //     $post = "POSTED IS NULL";
        // }

        return $this->db->query("
            SELECT
                *
            FROM jurnal_pengadaan
            WHERE
                (
                    LOWER(id_kontrak) LIKE LOWER(?)
                    OR LOWER(jpkodekegiatan) LIKE LOWER(?)
                    OR LOWER(jpnomorlokasi) LIKE LOWER(?)
                )
            AND $pg
        ", array(
            "%".$filter."%",
            "%".$filter."%",
            $nomor_lokasi."%"
        ))->result_array();
    }

    public function get_list_by_nomor_lokasi($nomor_lokasi, $posted, $filter, $limit = 0, $offset = 0) {
        $pg = "";
        $post = "";

        if ($limit > 0) $pg .= "LIMIT $limit";
        if ($offset > 0) $pg .= " OFFSET $offset";

        if($posted === 1){
            $post = "POSTED IS NOT NULL";
        } else if($posted === 0){
            $post = "POSTED IS NULL";
        }

        return $this->db->query("
            SELECT
                *
            FROM jurnal_pengadaan
            WHERE
                (
                    LOWER(id_kontrak) LIKE LOWER(?)
                    OR LOWER(jpkodekegiatan) LIKE LOWER(?)
                    OR LOWER(jpnomorlokasi) LIKE LOWER(?)
                )
            AND $post
            $pg
        ", array(
            "%".$filter."%",
            "%".$filter."%",
            $nomor_lokasi."%"
        ))->result_array();
    }

    // public function get_list_by_nomor_lokasi($nomor_lokasi, $posted, $filter = array(), $limit = 0, $offset = 0, $sort = array()) {
    // 	if ($nomor_lokasi) {
    // 		$this->db->where(array(
	   //  		"jpnomorlokasi" => $nomor_lokasi,
    //             $posted === 0 ? "posted IS NULL" : "posted IS NOT NULL"
	   //  	));
    // 	}

    //     $filter = array($posted === 0 ? "posted IS NULL" : "posted IS NOT NULL");
    // 	return parent::get_list($filter, $limit, $offset, $sort);
    //     //get_list($nomor_lokasi, $filter, $limit, $offset);
    // }

    public function count_by_nomor_lokasi($nomor_lokasi, $posted, $filter) {
    	if ($nomor_lokasi) {
    		$this->db->where(array(
	    		"jpnomorlokasi" => $nomor_lokasi,
                $posted === 0 ? "posted IS NULL" : "posted IS NOT NULL"
	    	));
    	}
        $filter = array($posted === 0 ? "posted IS NULL" : "posted IS NOT NULL");
    	return parent::count();
    }

    public function delete($jpid) {
        return $this->db->query("DELETE FROM jurnal_pengadaan WHERE JPID = ?", array($jpid));
    }

    public function count_row($nomor_lokasi, $filter) {
        $tmp = $this->db->query("
            SELECT
                COUNT(*) as c
            FROM jurnal_pengadaan
            WHERE
                (
                    LOWER(jpkodekegiatan) LIKE LOWER(?)
                    OR LOWER(id_kontrak) LIKE LOWER(?)
                    OR LOWER(jpnobaterima) LIKE LOWER(?)
                )
                AND LOWER(jpnomorlokasi) LIKE LOWER(?)
        ", array(
            "%".$filter."%",
            "%".$filter."%",
            "%".$filter."%",
            $nomor_lokasi."%"
        ))->row_array();
    }

    public function get_list_by_filter($nomor_lokasi, $filter, $limit = 0, $offset = 0) {
        $pg = "";

        if ($limit > 0) $pg .= "LIMIT $limit";
        if ($offset > 0) $pg .= "SKIP $offset";

        return $this->db->query("
            SELECT
                *
            FROM jurnal_pengadaan
            WHERE
                (
                    LOWER(jpkodekegiatan) LIKE LOWER(?)
                    OR LOWER(id_kontrak) LIKE LOWER(?)
                    OR LOWER(jpnobaterima) LIKE LOWER(?)
                )
                AND LOWER(jpnomorlokasi) LIKE LOWER(?)
            ORDER BY JPTGL DESC
            $pg
        ", array(
            "%".$filter."%",
            "%".$filter."%",
            "%".$filter."%",
            $nomor_lokasi."%"
        ))->result_array();
    }
}