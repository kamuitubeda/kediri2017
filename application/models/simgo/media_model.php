<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Media_Model extends MY_Model {
	
	public function __construct() {
		parent::__construct();
		
		$this->set_table_name('media');
		$this->set_pk(array('mdid'));
	}

    public function create($o) {
        $uid = 'admin';//$this->session->userdata('uid');

        unset($o['created']);
        unset($o['createdby']);
        unset($o['modified']);
        unset($o['modifiedby']);

        $this->db->set('created', 'NOW()', FALSE);
        $this->db->set('createdby', $uid);
        $this->db->set('modified', 'NOW()', FALSE);
        $this->db->set('modifiedby', $uid);

        return parent::create($o);
    }

    public function update($o) {
        $uid = 'admin';//$this->session->userdata('uid');

        unset($o['modified']);
        unset($o['modifiedby']);
        $this->db->set('modified', 'NOW()', FALSE);
        $this->db->set('modifiedby', $uid);

        return parent::update($o);
    }

    public function delete($o, $purge = false) {
        if (!$purge) {
            $uid = 'admin';//$this->session->userdata('uid');

            unset($o['deleted']);
            unset($o['deletedby']);
            $this->db->set('deleted', 'NOW()', FALSE);
            $this->db->set('deletedby', $uid);

            return parent::update($o);
        }

        return parent::delete($o);
    }

    public function where_with_bracket($filter = array()) {
        // build where
        $tmp = "(";
        $i=0;
        $c = count($filter);
        foreach ($filter as $k => $v) {
            $operand = preg_match('/(like|<|<=|>|>=)$/', $k) ? " " : " = ";
            $tmp .= $k . $operand . $this->db->escape($v);
            $tmp .= ($i < $c - 1) ? " OR " : "";
            $i++;
        }
        $tmp .= ")";

        $this->db->where($tmp);
    }

    public function get_media_path($mdid){
        $this->db->select("media.mdnama, media.mdpath, CONCAT( media.mdid, media.mdext ) AS medianame");
        $this->db->from("media");
        $this->db->where(array("media.mdid"=>$mdid));
        return $this->db->get()->result_array();
    }

}