<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Gudang_Model extends MY_Model {

	public function __construct() {
		parent::__construct();
		
		$this->set_table_name('gudang');
		$this->set_pk(array('guno'));
	}
    
    public function simpan_gudang($guno, $alamat, $nomor_unit){
        $this->db->where("guno", $guno);
        $this->db->update("gudang", array(
            "gualamat" => $alamat,
        ));
        
        $this->db->delete('gudang_kamus_unit', array('guno' => $guno));
        foreach ($nomor_unit as $key => $value) {
            if($value!=""){
                $this->db->insert("gudang_kamus_unit", array(
                    "guno" => $guno,
                    "nomor_unit" => $value
                ));
            }
        }
    }
    
    public function tambah_gudang($guno, $alamat, $nomor_unit){
        $this->db->insert("gudang", array(
            "guno" => $guno,
            "gualamat" => $alamat
        ));
        
        foreach ($nomor_unit as $key => $value) {
            if($value!=""){
                $this->db->insert("gudang_kamus_unit", array(
                    "guno" => $guno,
                    "nomor_unit" => $value
                ));
            }
        }
    }
    
    public function get_gudang($guno){
        $this->db->select(array("gudang.guno, gudang.gualamat, group_concat(kamus_unit.nomor_unit separator ',') as nomor_unit, group_concat(kamus_unit.nama_unit separator ', ') as nama_unit"), false);
        $this->db->from("gudang");
        $this->db->join("gudang_kamus_unit", "gudang.guno = gudang_kamus_unit.guno", "LEFT");
        $this->db->join("kamus_unit", "kamus_unit.nomor_unit = gudang_kamus_unit.nomor_unit", "LEFT");
        $this->db->where(array("gudang.guno" => $guno));
        $this->db->group_by("gudang.guno");
        
        return $this->db->get()->result_array();
    }
    
    public function get_gudang_detail($limit=0, $offset=0){
        $this->db->select(array("gudang.guno, gudang.gualamat, group_concat(kamus_unit.nomor_unit separator ',') as nomor_unit, group_concat(kamus_unit.nama_unit separator ', ') as nama_unit"), false);
        $this->db->from("gudang");
        $this->db->join("gudang_kamus_unit", "gudang.guno = gudang_kamus_unit.guno", "LEFT");
        $this->db->join("kamus_unit", "kamus_unit.nomor_unit = gudang_kamus_unit.nomor_unit", "LEFT");
        $this->db->group_by("gudang.guno");
        $this->db->order_by("gudang.guno ASC");
        
        return $this->db->get()->result_array();
        
    }

	public function search_gudangs_by_skpdid($skpdid, $term){
		// SELECT * FROM gudang_kamus_unit
		// LEFT JOIN gudang ON gudang.guno = gudang_kamus_unit.guno
		// WHERE gudang_kamus_unit.nomor_unit = '13.02.06.01'
		// AND gudang.gualamat LIKE '%%';
		$this->db->from("gudang");
		$this->db->join("gudang_kamus_unit", "gudang.guno = gudang_kamus_unit.guno", "LEFT");
		$this->db->where(array("gudang_kamus_unit.nomor_unit" => $skpdid, "gudang.gualamat LIKE '%".$term."%'"));
		return $this->db->get()->result_array();
	}
}