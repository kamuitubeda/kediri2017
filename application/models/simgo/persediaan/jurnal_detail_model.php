<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Jurnal_Detail_Model extends MY_Model {

    public function __construct() {
        parent::__construct();

        $this->set_table_name('jurnal_detail');
        $this->set_pk(array('judid', 'nomor_unit'));
    }

    public function get_exist_goods_procurement($skpd, $kode_barang, $tahun_periode_laporan, $bulan_periode_laporan = 1) {
        $query = $this->db->query("
            SELECT 
                NULL AS jutgl,
                NULL AS juno,
                NULL AS judqty,
                NULL AS judhargasat,
                NULL AS gettercounter
            FROM DUAL WHERE
                (@stock:=(SELECT 
                        SUM(judqty)
                    FROM
                        jurnal_detail
                            LEFT JOIN
                        jurnal ON jurnal.juno = jurnal_detail.juno AND jurnal.nomor_unit = jurnal_detail.nomor_unit
                            LEFT JOIN
                        kamus_barang ON kamus_barang.kbid = jurnal_detail.kbid
                    WHERE
                        jurnal.nomor_unit = '".$skpd."'
                            AND kamus_barang.kbkode = '".$kode_barang."'
                            AND jurnal_detail.deleted IS NULL
                            AND YEAR(jurnal.jutgl) <= ".$tahun_periode_laporan."
                            AND MONTH(jurnal.jutgl) < ".$bulan_periode_laporan."
                    GROUP BY jurnal_detail.kbid)
                )
            UNION SELECT 
                jutgl,
                juno,
                judqty,
                judhargasat,
                (@stock:=@stock - judqty) AS stockjurnaluncount
            FROM
                (SELECT 
                    jurnal.jutgl, jurnal.juno, judqty, judhargasat
                FROM
                    jurnal_detail
                LEFT JOIN jurnal ON jurnal.juno = jurnal_detail.juno and jurnal.nomor_unit = jurnal_detail.nomor_unit
                LEFT JOIN kamus_barang ON kamus_barang.kbid = jurnal_detail.kbid
                WHERE
                    jurnal.nomor_unit = '".$skpd."'
                        AND kamus_barang.kbkode = '".$kode_barang."'
                        AND jurnal_detail.deleted IS NULL
                        AND jurnal.tipe = '0'
                        AND YEAR(jurnal.jutgl) <= ".$tahun_periode_laporan."
                        AND MONTH(jurnal.jutgl) < ".$bulan_periode_laporan."
                ORDER BY jutgl DESC , juno DESC) AS selected
            WHERE
                @stock > 0 
        ");
        // var_dump($query);die;
        $result = $query->result_array();
        array_shift($result);
        $result = array_reverse($result);
        if(!empty($result)){
            $result[0]['judqty'] = floatval($result[0]['judqty']) + floatval($result[0]['gettercounter']);
        }
        return $result;
    }
    
    public function get_this_month_procurement($skpd, $kode_barang, $tahun, $bulan){
        $this->db->select("jurnal.jutgl, jurnal.juno, judqty, judhargasat, CONCAT('0') AS seadanya");
        $this->db->from("jurnal_detail");
        $this->db->join("jurnal", "jurnal.juno = jurnal_detail.juno AND jurnal.nomor_unit = jurnal_detail.nomor_unit", "LEFT");
        $this->db->join("kamus_barang", "kamus_barang.kbid = jurnal_detail.kbid", "LEFT");
        $this->db->where(array(
            "jurnal.nomor_unit" => $skpd,
            "jurnal.tipe" => "0",
            "kamus_barang.kbkode" => $kode_barang,
            "YEAR(jurnal.jutgl)" => $tahun,
            "MONTH(jurnal.jutgl)" => $bulan,
            "jurnal.deleted is null",
            "jurnal_detail.deleted is null",
        ));
        $this->db->order_by("jurnal.jutgl, jurnal.juno");
        
        return $this->db->get()->result_array();
    }
}