<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Barang_Model extends MY_Model {

	public function __construct() {
		parent::__construct();
		
		$this->set_table_name('kamus_barang');
		$this->set_pk(array('kbid'));
	}
    
    public function count_available_item($skpdid, $tgl_jurnal, $filter=''){
        $this->db->select("sum(jurnal_detail.judqty) as stok");
        
        $this->db->from('jurnal_detail');
        
		$this->db->join('jurnal', 'jurnal.juno = jurnal_detail.juno AND jurnal.nomor_unit = jurnal_detail.nomor_unit', 'LEFT');
		$this->db->join('kamus_barang', 'jurnal_detail.kbid = kamus_barang.kbid', 'LEFT');
		
        $filter = array_merge($filter, array(
            'jurnal.nomor_unit' => $skpdid,
            'jurnal.jutgl <= "'.$tgl_jurnal . '"',
            // 'jurnal_detail.guno' => $gudang,
            'jurnal.deleted IS NULL',
            'jurnal_detail.deleted IS NULL'
        ));
        
        $this->db->where($filter);
        
        $this->db->group_by("jurnal_detail.kbid");
        
        return $this->db->get()->num_rows();
    }
    
    public function get_available_item($skpdid, $tgl_jurnal, $filter='', $limit=0, $offset=0){
        $this->db->select("kamus_barang.kbid, kamus_barang.kbkode, kamus_barang.kbnama, kamus_barang.kbunit, kamus_barang.kbspesifikasi, sum(jurnal_detail.judqty) as stok");
        
        $this->db->from('jurnal_detail');
        
		$this->db->join('jurnal', 'jurnal.juno = jurnal_detail.juno AND jurnal.nomor_unit = jurnal_detail.nomor_unit', 'LEFT');
		$this->db->join('kamus_barang', 'jurnal_detail.kbid = kamus_barang.kbid', 'LEFT');
		
        $filter = array_merge($filter, array(
            'jurnal.nomor_unit' => $skpdid,
            'jurnal.jutgl <= "'.$tgl_jurnal . '"',
            // 'jurnal_detail.guno' => $gudang,
            'jurnal.deleted IS NULL',
            'jurnal_detail.deleted IS NULL'
        ));
        
        $this->db->where($filter);
        
        $this->db->group_by("kamus_barang.kbkode");

        if ($limit > 0) {
            $this->db->limit($limit, $offset);
        }
        
        return $this->db->get()->result_array();
    }
}