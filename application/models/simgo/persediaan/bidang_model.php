<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bidang_Model extends MY_Model {

	public function __construct() {
		parent::__construct();
		
		$this->set_table_name('bidang');
		$this->set_pk(array('bdid'));
	}
}