<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mediapool_Model extends MY_Model {
	
	public function __construct() {
		parent::__construct();
		
		$this->set_table_name('mediapool');
		$this->set_pk(array('mpid'));
	}

    public function create($o) {
        $uid = 'admin';//$this->session->userdata('uid');

        return parent::create($o);
    }

    public function update($o) {
        $uid = 'admin';//$this->session->userdata('uid');

        return parent::update($o);
    }

    public function delete($o, $purge = false) {
        if (!$purge) {
            $uid = 'admin';//$this->session->userdata('uid');

            return parent::update($o);
        }

        return parent::delete($o);
    }

    public function where_with_bracket($filter = array()) {
        // build where
        $tmp = "(";
        $i=0;
        $c = count($filter);
        foreach ($filter as $k => $v) {
            $operand = preg_match('/(like|<|<=|>|>=)$/', $k) ? " " : " = ";
            $tmp .= $k . $operand . $this->db->escape($v);
            $tmp .= ($i < $c - 1) ? " OR " : "";
            $i++;
        }
        $tmp .= ")";

        $this->db->where($tmp);
    }

    public function create_mediapool_media_link($mpid,$mdid)
    {
        $data = array(
                'mpid' => $mpid,
                'mdid' => $mdid
            );
        $this->db->insert('mediapool_media', $data);
    }

    public function update_mediapool_newfile_notification($mpid, $mpnewfile)
    {
        $data = array(
                'mpnewfile' => $mpnewfile
            );
        $this->db->update('mediapool', $data, array('mpid' => $mpid));
    }

    public function update_mediapool_newfile_notification_data($mpid, $mpnewfile, $mpnewestmedia)
    {
        $data = array(
                'mpnewfile' => $mpnewfile,
                'mpnewestmedia' => $mpnewestmedia
            );
        $this->db->update('mediapool', $data, array('mpid' => $mpid));
    }

    public function get_mpno($mpid){
        $this->db->select("mediapool.mpno");
        $this->db->from("mediapool");
        $this->db->where(array("mediapool.mpid"=>$mpid));
        return $this->db->get()->result_array();
    }

    public function get_mediapoolkategori($mpid){
        $this->db->select("mediapoolkategori.mpktid, mediapoolkategori.mpktname");
        $this->db->from("mediapool");
        $this->db->join("mediapoolkategori", "mediapool.mpktid = mediapoolkategori.mpktid", "LEFT");
        $this->db->where("mpid",$mpid);

        return $this->db->get()->result_array();
    }

    public function get_latestmedia($mpid){
        $this->db->select("mediapool.mpnewestmedia");
        $this->db->from("mediapool");
        $this->db->where(array("mediapool.mpid"=>$mpid));
        return $this->db->get()->result_array();
    }

    public function get_all_mediapool($mpktid, $skpd='', $limit='0', $offset='0'){
        $this->db->select("mediapool.mpid, mediapool.mpno, mediapool.mpnewfile, mediapool.skpdid");

        $this->db->from("mediapool");

        $this->db->where("mediapool.mpktid", $mpktid);
        $this->db->where_in("mediapool.skpdid", $skpd);

        $this->db->limit($limit, $offset);

        $this->db->order_by("mediapool.mpid", "DESC");

        return $this->db->get()->result_array();
    }

    public function count_all_mediapool($mpktid, $skpd=array())
    {
        $this->db->select("*");
        $this->db->from("mediapool");

        $this->db->where("mediapool.mpktid", $mpktid);
        $this->db->where_in("mediapool.skpdid", $skpd);

        return $this->db->count_all_results();
    }

    public function get_medias_by_mpid($mpid, $mpktid='1'){
        $this->db->select("mediapool.mpno, media.mdnama, media.created as uploaded, mediakategori.mdktnama, media.mdid");

        $this->db->from("mediapool");

        $this->db->join("mediapool_media","mediapool.mpid = mediapool_media.mpid","LEFT");
        $this->db->join("media","mediapool_media.mdid = media.mdid","LEFT");
        $this->db->join("media_mediakategori","media.mdid = media_mediakategori.mdid","LEFT");
        $this->db->join("mediakategori","media_mediakategori.mdktid = mediakategori.mdktid","LEFT");

        $this->db->where(array("mediapool.mpid"=>$mpid, "mediakategori.mpktid"=>$mpktid));

        $this->db->order_by('uploaded','ASC');

        return $this->db->get()->result_array();
    }

    public function create_mediapool($mpno, $skpdid, $mpktid) {
        $data = array(
            'mpno' => $mpno,
            'skpdid' => $skpdid,
            'mpktid' => $mpktid,
            'mpnewfile' => '0',
            'mpnewestmedia' => ''
        );

        return $this->db->insert('mediapool', $data);
    }

    public function edit_mediapool_mpno($mpid, $mpno) {
        $data = array(
            'mpno' => $mpno
        );
        $this->db->update('mediapool', $data, array('mpid' => $mpid));
    }
    
    public function get_list_by_skpd($skpdid, $filter, $limit = 0, $offset = 0) {
        $pg = "";

        if ($limit > 0) $pg .= "LIMIT $limit";
        if ($offset > 0) $pg .= " OFFSET $offset";

        return $this->db->query("
            SELECT
                *
            FROM mediapool
            WHERE
                (
                    LOWER(mpno) LIKE LOWER(?)
                    OR LOWER(skpdid) LIKE LOWER(?)
                )
            $pg
        ", array(
            "%".$filter."%",
            $skpdid."%"
        ))->result_array();
    }

    public function count_by_skpd($skpdid) {
        if ($skpdid) {
            $this->db->where(array(
                "skpdid" => $skpdid
            ));
        }
        return parent::count();
    }

    public function count_row($skpdid, $filter) {
        $tmp = $this->db->query("
            SELECT
                COUNT(*) as c
            FROM mediapool
            WHERE
                (
                    LOWER(mpno) LIKE LOWER(?)
                )
                AND LOWER(skpdid) LIKE LOWER(?)
        ", array(
            "%".$filter."%",
            $skpdid."%"
        ))->row_array();
    }

    public function get_list_by_filter($skpdid, $filter, $limit = 0, $offset = 0) {
        $pg = "";

        if ($limit > 0) $pg .= "LIMIT $limit";
        if ($offset > 0) $pg .= "SKIP $offset";

        return $this->db->query("
            SELECT
                *
            FROM mediapool
            WHERE
                (
                    LOWER(mpno) LIKE LOWER(?)
                    or LOWER(skpdid) LIKE LOWER(?)
                )
                AND LOWER(skpdid) LIKE LOWER(?)
            $pg
        ", array(
            "%".$filter."%",
            "%".$filter."%",
            $skpdid."%"
        ))->result_array();
    }
}