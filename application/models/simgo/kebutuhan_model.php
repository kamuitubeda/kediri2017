<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kebutuhan_Model extends MY_Model {
	
	public function __construct() {
		parent::__construct();
		
		$this->set_table_name('kebutuhan');
		$this->set_pk(array('idkebutuhan'));
	}

    public function create($o) {
        $uid = 'admin';//$this->session->userdata('uid');

        return parent::create($o);
    }

    public function update($o) {
        $uid = 'admin';//$this->session->userdata('uid');

        return parent::update($o);
    }

    public function delete($o, $purge = false) {
        if (!$purge) {
            $uid = 'admin';//$this->session->userdata('uid');

            return parent::update($o);
        }

        return parent::delete($o);
    }
}