<?php
class Masa_manfaat_tambahan_model extends MY_Model {
	
	public function __construct() {
        parent::__construct();
        
        $this->set_table_name('masa_manfaat_tambahan');
        $this->set_pk(array('idmmt'));
    }

    public function get_mmt($kelompok, $rule) {
		return $this->db->query("
			SELECT mmt FROM masa_manfaat_tambahan WHERE kelompok = ? AND presentasemmt = ?
		", array($kelompok, $rule))->row_array();
	}

}