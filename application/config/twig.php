<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
 
$config['template_dir'] = array(
	APPPATH.'views', 
	APPPATH.'views'.DIRECTORY_SEPARATOR.'_templates', 
	APPPATH.'modules'.DIRECTORY_SEPARATOR.'secman'.DIRECTORY_SEPARATOR.'views'
);

$config['cache_dir'] = APPPATH.'cache/twig';