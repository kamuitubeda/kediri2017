<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


// halaman bypass
$config['default_bypass_pages'] = array(
	// don't change it from here
	'#^welcome#',
	'#^admin#',
	'#^secman/authorization/denied#',
	'#^secman/authentication#',
	// until here
	// "#^spk/#",
	// "#^kegiatan/#",
	'#^pengadaan#',
	'#^kegiatan/import_simda#',
	'#^spk/get_list_kegiatan2#',
	'#^spk/get_list_kegiatan_by_nomor_lokasi#',
	'#^aset/get_all_lokasi#'
);

$config["appname"] = "Aplikasi Penyusutan";
$config["appcode"] = "Aplikasi Penyusutan";


$config["admin_home"] = "welcome/dashboard";

$config["use_recaptcha"] = FALSE;

$config['public_key'] = '';
$config['private_key'] = '';

// Set Recaptcha options
// Reference at https://developers.google.com/recaptcha/docs/customization
$config['recaptcha_options'] = array(
'theme'=>'white', // red/white/blackglass/clean
'lang' => 'en' // en/nl/fl/de/pt/ru/es/tr
// 'custom_translations' - Use this to specify custom translations of reCAPTCHA strings.
// 'custom_theme_widget' - When using custom theming, this is a div element which contains the widget. See the custom theming section for how to use this.
// 'tabindex' - Sets a tabindex for the reCAPTCHA text box. If other elements in the form use a tabindex, this should be set so that navigation is easier for the user
);