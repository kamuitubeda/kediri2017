<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

interface GenericDao {

    public function create($o);

    public function read($id);

    public function update($o);

    public function delete($o);

    public function get_list($filter, $limit, $offset, $sort);

    public function count($filter);
}

/**
 * MY_Model.php
 * Encoding: UTF-8
 * Created on June 29, 2012
 * @author Christian Wijaya (christianwijaya.net@gmail.com)
 */
class MY_Model extends CI_Model implements GenericDao {

    protected $table, $pk;

    public function __construct() {
        parent::__construct();
    }

    public function set_table_name($table) {
        $this->table = $table;
    }

    public function get_table_name() {
        return $this->table;
    }

    public function set_pk($pk) {
        $this->pk = $pk;
    }

    public function get_pk() {
        return $this->pk;
    }

    public function create($o) {
        $this->db->insert($this->table, $o);

        return $this->db->insert_id();
    }

    public function read($id) {
        $filter = array();
        for ($i = 0; $i < count($this->pk); $i++) {
            $filter[$this->pk[$i]] = $id[$this->pk[$i]];
        }

        $this->db->from($this->table);
        $this->db->where($filter);

        return $this->db->get()->row_array();
    }

    public function update($o) {
        $filter = array();
        for ($i = 0; $i < count($this->pk); $i++) {
            $filter[$this->pk[$i]] = $o[$this->pk[$i]];
        }

        $this->db->where($filter);
        $this->db->update($this->table, $o);

        return $this->db->affected_rows();
    }

    public function delete($o) {
        for ($i = 0; $i < count($this->pk); $i++) {
            if (!array_key_exists($this->pk[$i], $o)) {
                return FALSE;
            }
        }

        $filter = array();
        for ($i = 0; $i < count($this->pk); $i++) {
            $filter[$this->pk[$i]] = $o[$this->pk[$i]];
        }

        $this->db->delete($this->table, $filter);

        return $this->db->affected_rows();
    }

    public function get_list($filter = array(), $limit = 0, $offset = 0, $sort = array(), $debug = 0) {
        $this->db->from($this->table);

        if (!empty($filter)) {
            $this->db->where($filter);
        }

        if (!empty($sort)) {
            foreach ($sort as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }

        if ($limit > 0) {
            $this->db->limit($limit, $offset);
        }

        if ($debug) {
            print_r($this->db->_compile_select());
        }

        return $this->db->get()->result_array();
    }

    public function count($filter = array(), $debug = 0) {
        $this->db->from($this->table);

        if (!empty($filter)) {
            $this->db->where($filter);
        }

        if ($debug) {
            print_r($this->db->_compile_select());
        }

        return $this->db->count_all_results();
    }

}

?>
