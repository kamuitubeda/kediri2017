<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * MY_RecordBase.php
 * Encoding: UTF-8
 * Created on July 01, 2012
 * @author Christian Wijaya (christianwijaya.net@gmail.com)
 */
class MY_RecordBase extends MY_Model {

    public function __construct() {
        parent::__construct();
    }

    public function create($o) {
        $uid = $this->session->userdata('uid');

        unset($o['created']);
        unset($o['createdby']);
        unset($o['modified']);
        unset($o['modifiedby']);

        $this->db->set('created', 'NOW()', FALSE);
        $this->db->set('createdby', $uid);
        $this->db->set('modified', 'NOW()', FALSE);
        $this->db->set('modifiedby', $uid);

        return parent::create($o);
    }

    public function update($o) {
        $uid = $this->session->userdata('uid');

        unset($o['modified']);
        unset($o['modifiedby']);
        $this->db->set('modified', 'NOW()', FALSE);
        $this->db->set('modifiedby', $uid);

        return parent::update($o);
    }

    public function delete($o) {
        $uid = $this->session->userdata('uid');

        unset($o['deleted']);
        unset($o['deletedby']);
        $this->db->set('deleted', 'NOW()', FALSE);
        $this->db->set('deletedby', $uid);

        return parent::update($o);
    }

    public function where_with_bracket($filter = array()) {
        // build where
        $tmp = "(";
        $i=0;
        $c = count($filter);
        foreach ($filter as $k => $v) {
            $operand = preg_match('/(like|<|<=|>|>=)$/', $k) ? " " : " = ";
            $tmp .= $k . $operand . $this->db->escape($v);
            $tmp .= ($i < $c - 1) ? " OR " : "";
            $i++;
        }
        $tmp .= ")";

        $this->db->where($tmp);
    }

}

?>
