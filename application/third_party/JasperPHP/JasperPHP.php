<?php

/** JasperPHP root directory */
if (!defined('JASPERPHP_ROOT')) {
    define('JASPERPHP_ROOT', APPPATH."third_party/");
    require(JASPERPHP_ROOT . 'JasperPHP/Autoloader.php');
}

if (!defined('JASPERSTARTER_PATH')) {
    define('JASPERSTARTER_PATH', "jasperStarter/bin/jasperstarter");
}
 
class JasperPHP
{
    protected $executable = JASPERSTARTER_PATH;
    protected $the_command;
    protected $output_file;
    protected $windows = false;
    protected $formats = array('pdf', 'rtf', 'xls', 'xlsx', 'docx', 'odt', 'ods', 'pptx', 'csv', 'html', 'xhtml', 'xml', 'jrprint');

    protected $dbdriver;
    protected $hostname;
    protected $database;
    protected $username;
    protected $password;


    function __construct()
    {
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN')
           $this->windows = true;

        if ($this->windows) {
            $this->executable = str_replace("/", "\\", $this->executable);
            $this->executable .= ".exe";
        }

        $ci =& get_instance();

        $this->dbdriver = $ci->db->dbdriver;
        $this->hostname = $ci->db->hostname;
        $this->database = $ci->db->database;
        $this->username = $ci->db->username;
        $this->password = $ci->db->password;

        $this->output_file = new stdClass;
    }

    public static function __callStatic($method, $parameters)
    {
        // Create a new instance of the called class, in this case it is Post
        $model = get_called_class();

        // Call the requested method on the newly created object
        return call_user_func_array(array(new $model, $method), $parameters);
    }

    public function compile($input_file)
    {
        if(is_null($input_file) || empty($input_file))
            throw new \Exception("No input file", 1);
            
        $command = $this->executable;
        
        $command .= " cp ";

        $command .= $input_file;

        $tmp = pathinfo($input_file);

        $command .= " -o " . $tmp["dirname"];

        $this->the_command      = $command;

        return $this;
    }

    public function process(
        $input_file, 
        $parameters = array(),
        $connection = array(), 
        $format = array("pdf")
    ) {
    	if (!array_key_exists("type", $connection)) {
    		die("type is empty");
    	}

        if ($connection["type"] == "db") {
            $tmp = array(
                "driver" => $this->dbdriver, 
                "host" => $this->hostname, 
                "database" => $this->database, 
                "username" => $this->username, 
                "password" => $this->password
            );


            $connection = array_merge($connection, $tmp);
        } else if ($connection["type"] == "csv") {
        	if (!array_key_exists("source", $connection)) {
        		die("source is empty");
        	}
        }

        if(is_null($input_file) || empty($input_file))
            throw new \Exception("No input file", 1);

        if( is_array($format) )
        {
            foreach ($format as $key) 
            {
                if( !in_array($key, $this->formats))
                    throw new \Exception("Invalid format!", 1);
            }
        } else {
            if( !in_array($format, $this->formats))
                    throw new \Exception("Invalid format!", 1);
        }        

        $command = $this->executable;
        
        $command .= " pr ";

        $command .= $input_file;


        $this->output_file->tmp_name = tempnam(sys_get_temp_dir(), '');
        $command .= " -o " . ' "' . $this->output_file->tmp_name . '"';
        

        $tmp = pathinfo($this->output_file->tmp_name);
        $this->output_file->tmp_dir = $tmp["dirname"];


        if( is_array($format) ) {
            $command .= " -f " . join(" ", $format);
            $this->output_file->format = $format;
        }
        else {
            $command .= " -f " . $format;
            $this->output_file->format = array($format);
        }

        // Resources dir
        $input = pathinfo($input_file);
        $command .= " -r " . $input["dirname"];

        if( count($parameters) > 0 )
        {
            $command .= " -P";
            foreach ($parameters as $key => $value) 
            {
                $command .= " " . $key . "=\"" . $value . "\"";
            }
        }    

        if($connection["type"] == "db")
        {
            $command .= " -t " . $connection['driver'];
            $command .= " -u " . $connection['username'];
            
            if( isset($connection['password']) && !empty($connection['password']) )
                $command .= " -p " . $connection['password'];
            
            $command .= " -H " . $connection['host'];
            $command .= " -n " . $connection['database'] . "?zeroDateTimeBehavior=convertToNull";
        } else if ($connection["type"] == "csv") {
        	$command .= " -t " . $connection['type'];
        	$command .= ' --data-file "' . $connection["source"] . '"';
        	$command .= " --csv-columns " . $connection["columns"];
        	$command .= " --csv-first-row --csv-record-del=\"\\n\" --csv-field-del=\"" . $connection["delimiter"]. "\"";
        }
        
        $this->the_command      = $command;
        // var_dump($command);  

        return $this;
    }

    public function output()
    {
        if (count($this->output_file->format) > 1) {
            $zipname = tempnam(sys_get_temp_dir(), '');

            $zip = new ZipArchive;
            $zip->open($zipname, ZipArchive::CREATE);

            foreach ($this->output_file->format as $key => $value) {
                $tmp = $this->output_file->tmp_name . "." . $value;
                $tmp2 = pathinfo($tmp);
                $zip->addFile($tmp, $tmp2["basename"]);
            }

            $zip->close();

            foreach ($this->output_file->format as $key => $value) {
                $tmp = $this->output_file->tmp_name . "." . $value;
            }

            readfile($zipname);
            unlink($zipname);
        } else if (count($this->output_file->format) == 1) {
            $tmp = $this->output_file->tmp_name . "." . $this->output_file->format[0];
            $info = pathinfo($tmp);

            $filename = $info["dirname"] . DIRECTORY_SEPARATOR . $info["basename"];

            readfile($filename);
            unlink($filename);
        }
    }

    public function execute($debug = 1)
    {
        $output     = array();
        $return_var = 0;

        if ($debug) {
        	var_dump($this->the_command);
        }
 
        exec($this->the_command, $output, $return_var);

        if($return_var != 0) {
            throw new \Exception("There was and error executing the report! Time to check the logs!", 1);
	    }

        return $this;
    }
}