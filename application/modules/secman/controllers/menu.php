<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * menu.php
 * Encoding: UTF-8
 * Created on Mar 24, 2012
 * @author Christian Wijaya (christianwijaya.net@gmail.com)
 */
class Menu extends MX_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->twig->display("menu.html", array(
            "title" => "Pengaturan Menu",
            "csrf" => $this->security->get_csrf_hash(),
            "js" => array(
                "jquery-ui-1.10.3.min.js",
                "common.js",
                "i18n/grid.locale-en.js",
                "jquery.jqGrid.min.js",
                "grid.helper.js"
            ),
            "css" => array(
                "ui.jqgrid.css",
                "devexpress-like/jquery-ui.css",
                "appbase-v2.css"
            )
        ));
    }

    public function get_data() {
        $page = (integer) $this->input->post('page', TRUE);
        $limit = (integer) $this->input->post('rows', TRUE);
        $sidx = $this->input->post('sidx', TRUE);
        $sord = $this->input->post('sord', TRUE);
        $searchOn = $this->input->post('_search', TRUE);
        $filters = $this->input->post('filters', TRUE);

        if (!$sidx)
            $sidx = "mid";
        if (!$sord)
            $sord = "asc";
        if (!$page)
            $page = 0;
        if (!$limit)
            $limit = 10;

        $wh = "";
        if ($searchOn == "true") {
            $searchstr = $filters;
            $wh = construct_where($searchstr);
        }

        $count = $this->menu_model->count_menus($wh);

        if ($count > 0) {
            $total_pages = ceil($count / $limit);
        } else {
            $total_pages = 0;
        }

        if ($page > $total_pages)
            $page = $total_pages;
        $start = $limit * $page - $limit; // do not put $limit*($page - 1)
        if ($start < 0)
            $start = 0;

        $rows = $this->menu_model->get_menus($wh, $limit, $start, array($sidx => $sord));

        $r = new stdClass();

        $r->page = $page;
        $r->total = $total_pages;
        $r->records = $count;
        $i = 0;

        foreach ($rows as $row) {
            $r->rows[$i]['id'] = $row['mid'];
            $r->rows[$i]['cell'] = array(
                $row['mid'],
                $row['mname'],
                $row['mshow'],
                $row['mseq']
            );
            $i++;
        }

        echo json_encode($r);
    }

    public function get_subdata($mid = 0) {
        $page = (integer) $this->input->post('page', TRUE);
        $limit = (integer) $this->input->post('rows', TRUE);
        $sidx = $this->input->post('sidx', TRUE);
        $sord = $this->input->post('sord', TRUE);
        $searchOn = $this->input->post('_search', TRUE);
        $filters = $this->input->post('filters', TRUE);

        if (!$sidx)
            $sidx = "mid";
        if (!$sord)
            $sord = "asc";
        if (!$page)
            $page = 0;
        if (!$limit)
            $limit = 10;

        $wh = "";
        if ($searchOn == "true") {
            $searchstr = $filters;
            $wh = construct_where($searchstr);
        }

        $count = $this->menu_model->count_submenus($mid, $wh);

        if ($count > 0) {
            $total_pages = ceil($count / $limit);
        } else {
            $total_pages = 0;
        }

        if ($page > $total_pages)
            $page = $total_pages;
        $start = $limit * $page - $limit; // do not put $limit*($page - 1)
        if ($start < 0)
            $start = 0;

        $rows = $this->menu_model->get_submenus($mid, $wh, $limit, $start, array($sidx => $sord));

        $r = new stdClass();

        $r->page = $page;
        $r->total = $total_pages;
        $r->records = $count;
        $i = 0;

        foreach ($rows as $row) {
            $r->rows[$i]['id'] = $row['mid'];
            $r->rows[$i]['cell'] = array(
                $row['mid'],
                $row['mname'],
                $row['muri'],
                $row['mshow'],
                $row['mpass'],
                $row['mseq'],
                $row['menable']
            );
            $i++;
        }

        echo json_encode($r);
    }

    public function get_services() {
        $page = (integer) $this->input->post('page', TRUE);
        $limit = (integer) $this->input->post('rows', TRUE);
        $sidx = $this->input->post('sidx', TRUE);
        $sord = $this->input->post('sord', TRUE);
        $searchOn = $this->input->post('_search', TRUE);
        $filters = $this->input->post('filters', TRUE);

        if (!$sidx)
            $sidx = "mid";
        if (!$sord)
            $sord = "asc";
        if (!$page)
            $page = 0;
        if (!$limit)
            $limit = 10;

        $wh = "";
        if ($searchOn == "true") {
            $searchstr = $filters;
            $wh = construct_where($searchstr);
        }

        $count = $this->menu_model->count_services($wh);

        if ($count > 0) {
            $total_pages = ceil($count / $limit);
        } else {
            $total_pages = 0;
        }

        if ($page > $total_pages)
            $page = $total_pages;
        $start = $limit * $page - $limit; // do not put $limit*($page - 1)
        if ($start < 0)
            $start = 0;

        $rows = $this->menu_model->get_services($wh, $limit, $start, array($sidx => $sord));

        $r = new stdClass();

        $r->page = $page;
        $r->total = $total_pages;
        $r->records = $count;
        $i = 0;

        foreach ($rows as $row) {
            $r->rows[$i]['id'] = $row['mid'];
            $r->rows[$i]['cell'] = array(
                $row['mid'],
                $row['muri'],
                $row['mdesc'],
                $row['mpass'],
                $row['menable']
            );
            $i++;
        }

        echo json_encode($r);
    }

    public function post($parentid = NULL) {
        $this->load->library('form_validation');
        
        $oper = $this->input->post('oper', TRUE);
        
        if ($oper == 'add' || $oper == 'edit') {
            $this->form_validation->set_rules('mname', '', 'max_length[50]');
            if ($parentid !== NULL) {
                $this->form_validation->set_rules('muri', '', 'required|max_length[200]');
            }
            
            if ($this->form_validation->run() == FALSE) {
                echo 'error';
                return;
            }
        }

        $id = $this->input->post('id', TRUE);
        $desc = $this->input->post('mdesc', TRUE);
        $enable = (int) $this->input->post('menable', TRUE);
        $name = $this->input->post('mname');
        $seq = (int) $this->input->post('mseq', TRUE);
        $uri = $this->input->post('muri', TRUE);
        $show = (int) $this->input->post('mshow', TRUE);
        $pass = (int) $this->input->post('mpass', TRUE);
        $type = (int) $this->input->post('mtype', TRUE);

        $id = ($id == '_empty') ? NULL : $id;
        $desc = $desc ? $desc : NULL;
        $name = $name ? $name : NULL;
        $uri = $uri ? $uri : NULL;

        $data = array(
            'mid' => $id,
            'mparentid' => $parentid,
            'mdesc' => $desc,
            'menable' => $enable,
            'mname' => $name,
            'mseq' => $seq,
            'muri' => $uri,
            'mshow' => $show,
            'mpass' => $pass,
            'mtype' => $type
        );
        
        $result = "error";

        if ($oper) {
            if ($oper == 'add') {
                $this->menu_model->create($data);
                $result = "success";
            } else if ($oper == 'edit') {
                $result = $this->menu_model->update($data) > 0 ? "success" : "error";
            } else if ($oper == 'del') {
                $o = $this->menu_model->get_menu($id);
                if (!empty($o)) $result = $this->menu_model->delete($o) > 0 ? "success" : "error";
            }
        }
        
        echo $result;
    }

    public function select() {
        $rows = $this->menu_model->get_menus();

        $result = array();

        $i = 0;
        foreach ($rows as $row) {
            $result[$i]['mid'] = safe_html($row['mid']);
            $result[$i]['mname'] = safe_html($row['mname']);
            $i++;
        }

        echo json_encode($result);
    }

    public function select_available($gid = 0, $type = 0) {
        echo "<select>";
        echo "<option value=''></option>";

        if ($type > 0) {
            echo "<optgroup label='" . $this->lang->line('def_service') . "'>";
            $services = $this->menu_model->get_remain_services($gid);
            foreach ($services as $service) {
                echo "<option value='" . $service['mid'] . "'>" . safe_html($service['mname'] . " (" . safe_html($service['muri']) . ")") . "</option>";
            }
            echo "</optgroup>";
        } else {
            $menus = $this->menu_model->get_menus();
            foreach ($menus as $menu) {
                echo "<optgroup label='" . $menu['mname'] . "'>";

                $submenus = $this->menu_model->get_remain_submenus($menu['mid'], $gid);
                foreach ($submenus as $submenu) {
                    echo "<option value='" . $submenu['mid'] . "'>" . safe_html($submenu['mname'] . " (" . safe_html($submenu['muri']) . ")") . "</option>";
                }

                echo "</optgroup>";
            }
        }

        echo "</select>";
    }

    public function moveto($mid = 0) {
        $nmparentid = (integer) $this->input->post('nmparentid', TRUE);

        if ($mid && $nmparentid) {
            $result = 0;
            $menu = $this->menu_model->get_menu($mid);
            if (!empty($menu)) {
                $menu['mparentid'] = $nmparentid;
                $result = $this->menu_model->update($menu);
            }

            echo json_encode(array(
                "success" => $result > 0 ? true : false,
                "message" => $result > 0 ? 'Data berhasil diupdate' : 'Gagal diupdate!'
            ));
        } else {
            echo json_encode(array(
                "success" => false,
                "message" => 'Gagal diupdate!'
            ));
        }
    }

}

?>
