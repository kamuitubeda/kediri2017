<?php

/**
 * authentication.php
 * Encoding: UTF-8
 * Created on Aug 28, 2012
 * @author Christian Wijaya (christianwijaya.net@gmail.com)
 */
class Authentication extends MX_Controller {

	public function __construct() {
		parent::__construct();
	}

	public function index() {
		if ($this->session->userdata('uid') !== FALSE) {
			redirect($this->config->item("admin_home"));
		}
		
		$this->security_handler->switch_language($this->input->get("lang", TRUE));

		if ($this->config->item("use_recaptcha")) {
			$this->load->library("recaptcha");
		}

		$this->twig->display("signin.html", array(
			"title" => "Login Form",
			"message" => $this->session->flashdata('message'),
			"js" => array("jquery.validate.min.js"),
			"recaptcha" => $this->config->item("use_recaptcha") === TRUE ? $this->recaptcha->recaptcha_get_html() : ""
		));
	}

	public function login() {
		if ($this->session->userdata('uid') !== FALSE) {
			redirect(site_url($this->config->item("admin_home")));
		}

		if ($this->config->item("use_recaptcha")) {
			$this->load->library("recaptcha");
			$this->recaptcha->recaptcha_check_answer();

			if (!$this->recaptcha->getIsValid()) {
				$this->session->set_flashdata('message', "<b>Anda belum mengisikan kode reCaptcha!</b>");
				redirect('secman/authentication/index');
			}
		}


		$this->load->library('form_validation');

		// rules
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_message('required', $this->lang->line('req_message'));
		$this->form_validation->set_error_delimiters("<label class='error'>", "</label>");

		if ($this->form_validation->run() === FALSE) {
			$this->session->set_flashdata('message', validation_errors());
			redirect('secman/authentication/index');
		}

		$uid = $this->input->post('username', TRUE);
		$pass = $this->input->post('password', TRUE);

		sleep(3);

		if ($this->user_model->check_login($uid, $pass) > 0) {
			$this->session->set_userdata('uid', $uid);
			
			// Menyimpan aktivitas
			$this->activity_model->create(array('uid' => $uid, 'acttype' => 1, 'actmessage' => $uid . ' is logged'));
			
			redirect(site_url("welcome/dashboard"));
		}

		$this->session->set_flashdata('message', $this->lang->line('err_unauthenticated'));
		redirect('secman/authentication/index');
	}

	public function logout() {
		$this->session->unset_userdata('uid');
		redirect("welcome/index");
	}

}

// END Authentication class

/* End of file Authentication.php */
/* Location: ./application/controllers/authentication.php */