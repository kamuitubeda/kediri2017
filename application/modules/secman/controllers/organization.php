<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Organization.php
 * Encoding: UTF-8
 * Created on Mar 25, 2012
 * @author Christian Wijaya (christianwijaya.net@gmail.com)
 */
class Organization extends MX_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->twig->display("instansi.html", array(
            "title" => "Pengaturan Instansi",
            "csrf" => $this->security->get_csrf_hash(),
            "js" => array(
                "jquery-ui-1.10.3.min.js",
                "common.js",
                "i18n/grid.locale-en.js",
                "jquery.jqGrid.min.js",
                "grid.helper.js"
            ),
            "css" => array(
                "ui.jqgrid.css",
                "devexpress-like/jquery-ui.css",
                "appbase-v2.css"
            )
        ));
    }

    public function get_data() {
        $rows = $this->organization_model->get_tree_adjecent();

        $r = new stdClass();

        $r->page = 1;
        $r->total = 1;
        $r->records = count($rows);
        $i = 0;

        foreach ($rows as $row) {
            $r->rows[$i]['id'] = $row['oid'];
            $r->rows[$i]['cell'] = array(
                $row['oid'],
                $row['oparentid'],
                $row['oparentname'],
                $row['oname'],
                $row['onomorlokasi'],
                $row['oext'],
                $row['oenable'],
                $row['olevel'],
                $row['oparentid'],
                $row['isleaf'] ? true : false,
                false
            );
            $i++;
        }

        echo json_encode($r);
    }

    public function select($id = 0) {
        if ($id) {
            $ignores = array();
            $o = $this->organization_model->get_organization($id);
            if ($o) {
                $descents = $this->organization_model->get_organizations(array(
                    'secorg.opath %' => $o['opath'] . '%',
                    'secorg.oext' => 0,
                    'secorg.oenable' => 1));
                for ($i = 0; $i < count($descents); $i++) {
                    $item = $descents[$i];
                    array_push($ignores, $item['oid']);
                }
            }
            $organizations = $this->organization_model->get_organizations(array(
                'secorg.oid <>' => $ignores,
                'secorg.oext' => 0,
                'secorg.oenable' => 1));
        } else {
            $organizations = $this->organization_model->get_organizations(array(
                'secorg.oext' => 0,
                'secorg.oenable' => 1));
        }

        echo "<select>";
        echo "<option value=''></option>";
        foreach ($organizations as $organization) {
            echo "<option value='" . $organization['oid'] . "'>" . safe_html($organization['oname']) . "</option>";
        }
        echo "</select>";
    }

    public function post() {
        $oper = $this->input->post('oper', TRUE);
        $id = $this->input->post('id', TRUE);
        $parentid = (int) $this->input->post('oparentid', TRUE);
        $name = $this->input->post('oname', TRUE);
        $nomor_lokasi = $this->input->post('onomorlokasi', TRUE);
        $ext = (int) $this->input->post('oext', TRUE);
        $enable = (int) $this->input->post('oenable', TRUE);

        $id = ($id == '_empty') ? NULL : $id;
        $name = $name ? $name : NULL;
        $parentid = $parentid ? $parentid : NULL;

        $data = array(
            'oid' => $id,
            'oparentid' => $parentid,
            'oname' => $name,
            'onomorlokasi' => $nomor_lokasi,
            'oext' => $ext,
            'oenable' => $enable
        );

        $result = "error";

        if ($oper) {
            if ($oper == 'add') {
                $this->organization_model->create($data);
                $result = "success";
            } else if ($oper == 'edit') {
                $result = $this->organization_model->update($data) > 0 ? "success" : "error";
            } else if ($oper == 'del') {
                $o = $this->organization_model->get_organization($id);
                if (!empty($o))
                    $result = $this->organization_model->delete($o) > 0 ? "success" : "error";
            }
        }

        echo $result;
    }

}

?>
