<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * User.php
 * Encoding: UTF-8
 * Created on Mar 25, 2012
 * @author Christian Wijaya (christianwijaya.net@gmail.com)
 */
class User extends MX_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->twig->display("pengguna.html", array(
            "title" => "Pengaturan Pengguna",
            "csrf" => $this->security->get_csrf_hash(),
            "js" => array(
                "jquery-ui-1.10.3.min.js",
                "common.js",
                "i18n/grid.locale-en.js",
                "jquery.jqGrid.min.js",
                "grid.helper.js"
            ),
            "css" => array(
                "ui.jqgrid.css",
                "devexpress-like/jquery-ui.css",
                "appbase-v2.css"
            )
        ));
    }

    public function change_password() {
        $this->twig->display("ubah_password.html", array(
            "title" => "Ubah Password",
            "message" => $this->session->flashdata('message'),
            "success" => $this->session->flashdata('success'),
            "js" => array(
                "jquery.validate.min.js"
            ),
        ));
    }

    public function profile() {
        $uid = $this->session->userdata('uid');

        $user = $this->user_model->read(array("uid" => $uid));

        // menghilangkan informasi password
        unset($user["upass"]);

        $grup = $this->user_model->get_usergroups($uid);

        $this->twig->display("profil.html", array(
            "title" => "Profil Anda",
            "user" => $user,
            "groups" => $grup
        ));
    }

    public function change_password_action() {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('old_pass', 'Password Lama', 'required');
        $this->form_validation->set_rules('new_pass', 'Password Baru', 'required');
        $this->form_validation->set_rules('konfirmasi_new_pass', 'Konfirmasi Password Baru', 'required');

        $this->form_validation->set_message('required', $this->lang->line('req_message'));
        $this->form_validation->set_error_delimiters("<label class='error'>", "</label>");

        if ($this->form_validation->run() === FALSE) {
            $this->session->set_flashdata('message', validation_errors());
            redirect('secman/user/change_password');
        }

        $old_pass = $this->input->post('old_pass', TRUE);
        $new_pass = $this->input->post('new_pass', TRUE);
        $konfirmasi_new_pass = $this->input->post('konfirmasi_new_pass', TRUE);

        if ($new_pass !== $konfirmasi_new_pass) {
            $this->session->set_flashdata('message', "Password Baru dan Konfirmasi Password Baru tidak sama.");
            redirect('secman/user/change_password');
        }

        $uid = $this->session->userdata('uid');
        $user = __::first($this->user_model->get_list(array(
            "uid" => $uid,
            "upass" => do_hash($old_pass, 'md5')
        )));
        
        if (!empty($user)) {
            $user["upass"] = do_hash($new_pass, 'md5');
            $this->user_model->update($user);

            $this->session->set_flashdata('success', "Password Anda berhasil diubah");
            redirect('secman/user/change_password');
        } else {
            $this->session->set_flashdata('message', "Password Lama Anda salah");
            redirect('secman/user/change_password');
        }
    }

    public function get_data() {
        $page = (integer) $this->input->post('page', TRUE);
        $limit = (integer) $this->input->post('rows', TRUE);
        $sidx = $this->input->post('sidx', TRUE);
        $sord = $this->input->post('sord', TRUE);
        $searchOn = $this->input->post('_search', TRUE);
        $filters = $this->input->post('filters', TRUE);

        $wh = "";
        if ($searchOn == "true") {
            $searchstr = $filters;
            $wh = construct_where($searchstr);
        }

        $count = $this->user_model->count_users($wh);

        if ($count > 0) {
            $total_pages = ceil($count / $limit);
        } else {
            $total_pages = 0;
        }

        if ($page > $total_pages)
            $page = $total_pages;
        $start = $limit * $page - $limit; // do not put $limit*($page - 1)
        if ($start < 0)
            $start = 0;

        $users = $this->user_model->get_users($wh, $limit, $start, array($sidx => $sord));

        $r = new stdClass();

        $r->page = $page;
        $r->total = $total_pages;
        $r->records = $count;
        $i = 0;

        foreach ($users as $user) {
            $r->rows[$i]['id'] = $user['uid'];
            $r->rows[$i]['cell'] = array(
                $user['uid'],
                $user['uname'],
                $user['oid'],
                $user['oname'],
                "*****",
                $user['uemail'],
                $user['uban'],
                $user['uenable'],
                $user['oenable']
            );
            $i++;
        }

        echo json_encode($r);
    }

    public function get_subdata($uid = 0) {
        $page = (int) $this->input->post('page', TRUE);
        $limit = (int) $this->input->post('rows', TRUE);
        $sidx = $this->input->post('sidx', TRUE);
        $sord = $this->input->post('sord', TRUE);
        $searchOn = $this->input->post('_search', TRUE);
        $filters = $this->input->post('filters', TRUE);

        $wh = "";
        if ($searchOn == "true") {
            $searchstr = $filters;
            $wh = construct_where($searchstr);
        }

        $count = $this->user_model->count_usergroups($uid, $wh);

        if ($count > 0) {
            $total_pages = ceil($count / $limit);
        } else {
            $total_pages = 0;
        }

        if ($page > $total_pages)
            $page = $total_pages;
        $start = $limit * $page - $limit; // do not put $limit*($page - 1)
        if ($start < 0)
            $start = 0;

        $groups = $this->user_model->get_usergroups($uid, $wh, $limit, $start, array($sidx => $sord));

        $r = new stdClass();

        $r->page = $page;
        $r->total = $total_pages;
        $r->records = $count;
        $i = 0;

        foreach ($groups as $group) {
            $r->rows[$i]['id'] = $group['gid'];
            $r->rows[$i]['cell'] = array(
                $group['gid'],
                $group['gname'],
                $group['genable']
            );
            $i++;
        }

        echo json_encode($r);
    }

    public function post() {
        $this->load->library('form_validation');

        $oper = $this->input->post('oper', TRUE);

        if ($oper == 'add' || $oper == 'edit') {
            $this->form_validation->set_rules('id', '', 'required|max_length[50]');
            $this->form_validation->set_rules('uname', '', 'required|max_length[200]');
            $this->form_validation->set_rules('oid', '', 'required');
            $this->form_validation->set_rules('upass', '', 'required');
            $this->form_validation->set_rules('uemail', '', 'required|valid_email');

            if ($this->form_validation->run() == FALSE) {
                echo "error";
                return;
            }
        }

        $id = $this->input->post('id', TRUE);
        $uname = $this->input->post('uname', TRUE);
        $oid = (int) $this->input->post('oid', TRUE);
        $upass = $this->input->post('upass', TRUE);
        $uemail = $this->input->post('uemail', TRUE);
        $uban = (int) $this->input->post('uban', TRUE);
        $uenable = (int) $this->input->post('uenable', TRUE);

        $id = ($id == '_empty') ? NULL : $id;
        $uname = $uname ? $uname : NULL;

        $data = array(
            'uid' => $id,
            'oid' => $oid,
            'uname' => $uname,
            'uemail' => $uemail,
            'uban' => $uban,
            'uenable' => $uenable
        );

        $result = "error";

        if ($upass != "*****") {
            $data['upass'] = do_hash($upass, 'md5');
        }

        if ($oper) {
            if ($oper == 'add') {
                $this->user_model->create($data);
                $result = "success";
            } else if ($oper == 'edit') {
                $result = $this->user_model->update($data) > 0 ? "success" : "error";
            } else if ($oper == 'del') {
                $o = $this->user_model->get_user($id);
                if (!empty($o))
                    $result = $this->user_model->delete($o) > 0 ? "success" : "error";
            }
        }

        echo $result;
    }

    public function subpost($uid = 0) {
        $oper = $this->input->post('oper', TRUE);

        $id = (int) $this->input->post('id', TRUE);

        $result = "error";

        if ($oper == 'add') {
            $result = $this->user_model->add_group($uid, $id) > 0 ? "success" : "error";
        } else if ($oper == 'del') {
            $result = $this->user_model->remove_group($uid, $id) > 0 ? "success" : "error";
        }

        echo $result;
    }

}

?>
