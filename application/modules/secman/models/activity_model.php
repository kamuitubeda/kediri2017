<?php

/**
 * activity_model.php
 * Encoding: UTF-8
 * Created on Sep 8, 2012
 * @author Christian Wijaya (christianwijaya.net@gmail.com)
 */
class Activity_Model extends MY_Model {
    public function __construct() {
        parent::__construct();
        
        $this->set_table_name('secact');
        $this->set_pk(array('actid'));
    }
    
    public function create($o) {
        $o['actdt'] = date ("Y-m-d H:m:s");        
        return parent::create($o);
    }
    
    public function update($o) {
        throw new Exception('Activity Model only used for inserting');
    }
    
    public function delete($o) {
        throw new Exception('Activity Model only used for inserting');
    }
}
