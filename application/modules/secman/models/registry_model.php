<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * registry_model.php
 * Encoding: UTF-8
 * Created on May 18, 2012
 * @author Christian Wijaya (christianwijaya.net@gmail.com)
 */
class Registry_Model extends MY_RecordBase {

    public function __construct() {
        parent::__construct();

        $this->set_table_name('registry');
        $this->set_pk(array('ssname', 'rgkey'));
    }

    /**
     * Mendapatkan jumlah daftar registry berdasarkan subsistem.
     * @param type $ssname
     * @param type $filter
     * @return type int
     */
    public function count_registries($ssname, $filter = array()) {
        $this->db->where(array('ssname' => $ssname));
        return parent::count($filter);
    }

    /**
     * Mendapatkan daftar registry berdasarkan subsistem.
     * @param type $ssname
     * @param type $filter
     * @param type $limit
     * @param type $offset
     * @param type $sort
     * @return type array
     */
    public function get_registries($ssname, $filter = array(), $limit = 0, $offset = 0, $sort = array('rgkey' => 'asc')) {
        $this->db->where(array('ssname' => $ssname));
        return parent::get_list($filter, $limit, $offset, $sort);
    }

    /**
     * Mendapatkan registry berdasarkan subsistem.
     * @param type $ssname
     * @param type $rgkey
     * @return type array
     */
    public function get_registry($ssname, $rgkey) {
        return parent::read(array('ssname' => $ssname, 'rgkey' => $rgkey));
    }

}