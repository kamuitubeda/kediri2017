<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * subsystem_model.php
 * Encoding: UTF-8
 * Created on Sep 5, 2012
 * @author Christian Wijaya (christianwijaya.net@gmail.com)
 */
class Subsystem_Model extends MY_RecordBase {
    public function __construct() {
        parent::__construct();
        
        $this->set_table_name('subsysem');
        $this->set_pk(array('ssname'));
    }
    
    /**
     * Mendapatkan jumlah daftar subsistem.
     * @param type $filter
     * @return type int
     */
    public function count_subsystems($filter = array()) {
        return parent::count($filter);
    }
    
    /**
     * Mendapatkan daftar subsistem.
     * @param type $filter
     * @param type $limit
     * @param type $offset
     * @param type $sort
     * @return type array
     */
    public function get_subsystems($filter = array(), $limit = 0, $offset = 0, $sort = array('ssname' => 'asc')) {
        return parent::get_list($filter, $limit, $offset, $sort);
    }
    
    /**
     * Mendapatkan subsistem.
     * @param type $id
     * @return type array
     */
    public function get_subsystem($id) {
        return parent::read(array('ssname' => $id));
    }
}
