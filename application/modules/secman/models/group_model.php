<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * group_model.php
 * Encoding: UTF-8
 * Created on May 18, 2012
 * @author Christian Wijaya (christianwijaya.net@gmail.com)
 */
class Group_model extends MY_RecordBase {

    public function __construct() {
        parent::__construct();

        $this->set_table_name('secgroup');
        $this->set_pk(array('gid'));
    }

    /**
     * Menambahkan akses menu ke grup.
     * @param type $gid
     * @param type $mid
     * @return type int
     */
    public function add_access($gid, $mid) {
        $this->db->insert('secmenugroup', array('gid' => $gid, 'mid' => $mid));
        return $this->db->affected_rows();
    }

    /**
     * Menghapus akses menu ke grup.
     * @param type $gid
     * @param type $mid
     * @return type int
     */
    public function remove_access($gid, $mid) {
        $this->db->where(array('gid' => $gid, 'mid' => $mid));
        $this->db->delete('secmenugroup');

        return $this->db->affected_rows();
    }

    /**
     * Mendapatkan jumlah menu terkait pada grup.
     * @param type $gid
     * @param type $filter
     * @return type int
     */
    public function count_menugroups($gid, $filter = array()) {
        $this->db->join('secmenugroup', 'secmenugroup.gid = secgroup.gid');
        $this->db->join('secmenu', 'secmenu.mid = secmenugroup.mid');
        $this->db->where(array('secmenugroup.gid' => $gid));

        return parent::count($filter);
    }

    /**
     * Mendapatkan menu terkait pada grup.
     * @param type $gid
     * @param type $filter
     * @param type $limit
     * @param type $offset
     * @param type $sort
     * @return type array
     */
    public function get_menugroups($gid, $filter = array(), $limit = 0, $offset = 0, $sort = array('mname' => 'asc')) {
        $this->db->join('secmenugroup', 'secmenugroup.gid = secgroup.gid');
        $this->db->join('secmenu', 'secmenu.mid = secmenugroup.mid');
        $this->db->where(array('secmenugroup.gid' => $gid));

        return parent::get_list($filter, $limit, $offset, $sort);
    }

    /**
     * Mendapatkan jumlah pada daftar grup.
     * @param type $filter
     * @return type int
     */
    public function count_groups($filter = array()) {
        return parent::count($filter);
    }

    /**
     * Mendapatkan daftar grup.
     * @param type $filter
     * @param type $limit
     * @param type $offset
     * @param type $sort
     * @return type array
     */
    public function get_groups($filter = array(), $limit = 0, $offset = 0, $sort = array('gname' => 'asc')) {
        $this->db->join('secorg', 'secorg.oid = secgroup.oid');

        return parent::get_list($filter, $limit, $offset, $sort);
    }

    /**
     * Mendapatkan sisa grup terkait yang mungkin ditambahkan ke grup.
     * @param type $uid
     * @param type $filter
     * @return type array
     */
    public function get_remain_groups($uid, $filter = array(), $limit = 0, $offset = 0, $sort = array('gname' => 'asc')) {
        $arr = $this->user_model->get_usergroups($uid);
        $ids = array();

        foreach ($arr as $it) {
            $ids[] = $it['gid'];
        }

        if (count($ids) > 0)
            $this->db->where_not_in('gid', $ids);

        return parent::get_list($filter, $limit, $offset, $sort);
    }

    /**
     * Mendapatkan grup.
     * @param type $gid
     * @return type array
     */
    public function get_group($gid) {
        return parent::read(array('gid' => $gid));
    }

}
