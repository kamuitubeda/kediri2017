<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * menu_model.php
 * Encoding: UTF-8
 * Created on May 18, 2012
 * @author Christian Wijaya (christianwijaya.net@gmail.com)
 */
class Menu_model extends MY_RecordBase {

    public function __construct() {
        parent::__construct();

        $this->set_table_name('secmenu');
        $this->set_pk(array('mid'));
    }

    /**
     * Cek uri bagian dari uri yang di bypass atau tidak.
     * @param type $muri
     * @return type int
     */
    public function check_bypass($muri) {
        return parent::count(array('mpass' => 1, 'muri LIKE' => $muri . "%", 'menable' => 1));
    }

    /**
     * Cek akses pengguna sesuai kewenangannya.
     * @param type $uid ID pengguna
     * @param type $muri url
     * @return type int
     */
    public function check_access($uid, $muri) {
        $this->db->join('secmenugroup', 'secmenugroup.mid = secmenu.mid');
        $this->db->join('secgroup', 'secgroup.gid = secmenugroup.gid AND secgroup.genable = 1');
        $this->db->join('secusergroup', 'secusergroup.gid = secgroup.gid');
        $this->db->join('secuser', 'secuser.uid = secusergroup.uid');
        $this->db->select('secmenu.mname, secmenu.muri');

        return parent::count(array('secmenu.menable' => 1, 'secuser.uid' => $uid, "LOWER('" . $muri . "') LIKE LOWER(CONCAT(secmenu.muri, '%'))"));
    }

    /**
     * Mendapatkan jumlah daftar menu.
     * @param type $filter
     * @return type int
     */
    public function count_menus($filter = array()) {
        $this->db->where(array('mtype' => 0, 'mparentid' => NULL));
        return parent::count($filter);
    }

    /**
     * Mendapatkan daftar menu.
     * @param type $filter
     * @param type $limit
     * @param type $offset
     * @param type $sort
     * @return type array
     */
    public function get_menus($filter = array(), $limit = 0, $offset = 0, $sort = array('mname' => 'asc')) {
        $this->db->where(array('mtype' => 0, 'mparentid' => NULL));
        return parent::get_list($filter, $limit, $offset, $sort);
    }

    /**
     * Mendapatkan jumlah daftar submenu.
     * @param type $mid
     * @param type $filter
     * @return type int
     */
    public function count_submenus($mid, $filter = array()) {
        $this->db->where(array('mtype' => 0, 'mparentid' => $mid));
        return parent::count($filter);
    }

    /**
     * Mendapatkan daftar submenu.
     * @param type $mid
     * @param type $filter
     * @param type $limit
     * @param type $offset
     * @param type $sort
     * @return type array
     */
    public function get_submenus($mid, $filter = array(), $limit = 0, $offset = 0, $sort = array('mid' => 'asc')) {
        $this->db->where(array('mtype' => 0, 'mparentid' => $mid));
        return parent::get_list($filter, $limit, $offset, $sort);
    }

    /**
     * Mendapatkan jumlah daftar service.
     * @param type $filter
     * @return type int
     */
    public function count_services($filter = array()) {
        $this->db->where(array('mtype' => 1));
        return parent::count($filter);
    }

    /**
     * Mendapatkan daftar service.
     * @param type $filter
     * @param type $limit
     * @param type $offset
     * @param type $sort
     * @return type int
     */
    public function get_services($filter = array(), $limit = 0, $offset = 0, $sort = array('muri' => 'asc')) {
        $this->db->where(array('mtype' => 1));
        return parent::get_list($filter, $limit, $offset, $sort);
    }

    /**
     * Mendapatkan sisa menu terkait yang mungkin ditambahkan ke menu.
     * @param type $mid
     * @param type $gid
     * @param type $limit
     * @param type $offset
     * @param type $sort
     * @return type array
     */
    public function get_remain_submenus($mid, $gid, $limit = 0, $offset = 0, $sort = array('mname' => 'asc')) {
        $arr = $this->group_model->get_menugroups($gid, array('mtype' => 0));
        $ids = array();

        foreach ($arr as $it) {
            $ids[] = $it['mid'];
        }

        if (count($ids) > 0)
            $this->db->where_not_in('mid', $ids);

        return parent::get_list(array('mparentid' => $mid), $limit, $offset, $sort);
    }

    /**
     * Mendapatkan sisa servis terkait yang mungkin ditambahkan ke servis.
     * @param type $gid
     * @param type $limit
     * @param type $offset
     * @param type $sort
     * @return type array
     */
    public function get_remain_services($gid, $limit = 0, $offset = 0, $sort = array('mname' => 'asc')) {
        $arr = $this->group_model->get_menugroups($gid, array('mtype' => 1));
        $ids = array();

        foreach ($arr as $it) {
            $ids[] = $it['mid'];
        }

        if (count($ids) > 0)
            $this->db->where_not_in('mid', $ids);

        return parent::get_list(array('mtype' => 1), $limit, $offset, $sort);
    }

    /**
     * Mendapatkan menu.
     * @param type $mid
     * @return type array
     */
    public function get_menu($mid) {
        return parent::read(array('mid' => $mid));
    }

}
