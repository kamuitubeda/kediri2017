<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

// def => definition
// err => error
// tip => tips
// req => required
// sta => status

$lang['err_login'] = "ID pengguna atau kata kunci salah";
$lang['err_currentPassword'] = "Kata kunci sekarang salah!";
$lang['err_unauthorized'] = "Anda tidak memiliki akses.";
$lang['err_unauthenticated'] = "<b>Username</b> atau <b>Password</b> salah.";
$lang['err_undeleted'] = "Data tidak dapat dihapus.";
$lang['tip_username'] = "ID pengguna";
$lang['tip_password'] = "Kata kunci";
$lang['tip_login'] = "Klik disini untuk login ke sistem";
$lang['req_message'] = "Inputkan <b>%1s</b>";
$lang['req_password'] = "Inputkan kata kunci";
$lang['req_currentPassword'] = "Inputkan kata kunci sekarang";
$lang['req_newPassword'] = "Inputkan kata kunci yang baru";
$lang['req_confirmNewPassword'] = "Konfirmasi kata kunci yang baru";
$lang['eqt_newPassword'] = "Inputkan kata kunci sama seperti diatas";
$lang['sta_changedPassword'] = "Kata kunci telah berubah";
$lang['sta_success'] = "Data telah berubah";
$lang['sta_failure'] = "Data tidak ada yang berubah";

$lang["signin_heading"] = "Silahkan login";
$lang["signin_button"] = "LOGIN";

$lang['def_menu'] = "Menu";
$lang['def_service'] = "Servis";

?>
