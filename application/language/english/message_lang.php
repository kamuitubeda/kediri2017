<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

// def => definition
// err => error
// tip => tips
// req => required
// sta => status

$lang['err_login'] = "The user name or password is incorrect";
$lang['err_currentPassword'] = "Invalid the current password!";
$lang['err_unauthorized'] = "Unauthorized access.";
$lang['err_unauthenticated'] = "<b>Username</b> or <b>Password</b> is incorrect.";
$lang['err_undeleted'] = "Failed to delete the data";
$lang['tip_username'] = "Enter username";
$lang['tip_password'] = "Enter password";
$lang['tip_login'] = "Click here to login";
$lang['req_message'] = "<b>%1s</b> is required";
$lang['req_password'] = "Please enter the password";
$lang['req_currentPassword'] = "Please enter the current password";
$lang['req_newPassword'] = "Please enter the new password";
$lang['req_confirmNewPassword'] = "Please enter the confirm new password";
$lang['eqt_newPassword'] = "Please enter the same password as above";
$lang['sta_changedPassword'] = "The password has been changed";
$lang['sta_success'] = "Data has been changed";
$lang['sta_failure'] = "Failed to change the data";

$lang["signin_heading"] = "Please sign in";
$lang["signin_button"] = "SIGN IN";

$lang['unauthorized'] = "Unauthorized access.";
$lang['undeleted'] = "Failed to delete the data";
?>
