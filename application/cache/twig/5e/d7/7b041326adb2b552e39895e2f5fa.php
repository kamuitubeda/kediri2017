<?php

/* simgo/persediaan/modal/modal_kelola_jurnal.html */
class __TwigTemplate_5ed77b041326adb2b552e39895e2f5fa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"modal fade\" id=\"kelola-jurnal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
\t<div class=\"modal-dialog modal-md\">
\t\t<div class=\"modal-content\">
\t\t\t<div class=\"modal-header\">
\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
\t\t\t\t<h4 class=\"modal-title\" id=\"myModalLabel\">Pengelolaan Jurnal</h4>
\t\t\t</div>
\t\t\t<div class=\"modal-body\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-12\">
                            <div id=\"newno text-center\" class=\"row\">
\t\t\t\t\t\t\t\t<h3>Apakah anda yakin untuk menghapus jurnal ini?</h3>
                                <!--<div class=\"form-group\">
                                    <label class=\"control-label\">Nomor Baru<strong>*</strong></label>
                                    <input type=\"text\" class=\"form-control searchable\" id=\"nomor-baru\" name=\"nomor-baru\" placeholder=\"Nomor\" data-rule-required=\"true\">
                                </div>-->
                            </div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"modal-footer\">
\t\t\t\t<button type=\"button\" id = \"batal\" name=\"batal\" class=\"btn btn-default\" data-dismiss=\"modal\">Batal</button>
\t\t\t\t<button type=\"button\" id = \"hapus-jurnal\" name=\"hapus-jurnal\" class=\"btn btn-danger\">Hapus</button>
\t\t\t\t<!--<button type=\"button\" id = \"ubah-juno\" name=\"ubah-juno\" class=\"btn btn-primary\">Ubah</button>-->
\t\t\t</div>
\t\t</div>
\t</div>
</div>";
    }

    public function getTemplateName()
    {
        return "simgo/persediaan/modal/modal_kelola_jurnal.html";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,  427 => 321,  413 => 310,  407 => 307,  394 => 297,  377 => 283,  371 => 280,  362 => 274,  353 => 268,  252 => 172,  229 => 154,  202 => 130,  195 => 129,  171 => 110,  164 => 109,  112 => 62,  108 => 61,  95 => 51,  77 => 36,  47 => 9,  43 => 8,  37 => 4,  35 => 3,  33 => 2,  30 => 1,);
    }
}
