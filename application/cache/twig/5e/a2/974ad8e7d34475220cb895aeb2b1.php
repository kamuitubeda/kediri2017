<?php

/* pengadaan/form.html */
class __TwigTemplate_5ea2974ad8e7d34475220cb895aeb2b1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("_templates/adminpage.html");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'scripts' => array($this, 'block_scripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_templates/adminpage.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "
";
        // line 5
        $this->env->loadTemplate("pengadaan/fragments/pilihan.html")->display($context);
        // line 6
        $this->env->loadTemplate("pengadaan/fragments/1a.html")->display($context);
        // line 7
        $this->env->loadTemplate("pengadaan/fragments/1b.html")->display($context);
        // line 8
        $this->env->loadTemplate("pengadaan/fragments/1c.html")->display($context);
        // line 9
        $this->env->loadTemplate("pengadaan/fragments/1d.html")->display($context);
        // line 10
        $this->env->loadTemplate("pengadaan/fragments/1e.html")->display($context);
        // line 11
        $this->env->loadTemplate("pengadaan/fragments/1f.html")->display($context);
        // line 12
        $this->env->loadTemplate("pengadaan/fragments/1g.html")->display($context);
        // line 13
        $this->env->loadTemplate("pengadaan/fragments/1h.html")->display($context);
        // line 14
        $this->env->loadTemplate("pengadaan/fragments/1i.html")->display($context);
        // line 15
        $this->env->loadTemplate("pengadaan/fragments/1j.html")->display($context);
        // line 16
        $this->env->loadTemplate("pengadaan/fragments/2a.html")->display($context);
        // line 17
        $this->env->loadTemplate("pengadaan/fragments/2b.html")->display($context);
        // line 18
        $this->env->loadTemplate("pengadaan/fragments/2c.html")->display($context);
        // line 19
        $this->env->loadTemplate("pengadaan/fragments/3a.html")->display($context);
        // line 20
        $this->env->loadTemplate("pengadaan/fragments/3b.html")->display($context);
        // line 21
        $this->env->loadTemplate("pengadaan/fragments/3c.html")->display($context);
        // line 22
        echo "
\t<div class=\"row\">
\t\t<div id=\"error\" class=\"col-xs-8 alert alert-danger\" style=\"margin-left:10px;";
        // line 24
        if (((isset($context["validation_errors"]) ? $context["validation_errors"] : null) == false)) {
            echo "display:none;";
        }
        echo "\">
\t\t\t";
        // line 25
        if (((isset($context["validation_errors"]) ? $context["validation_errors"] : null) != false)) {
            // line 26
            echo "\t\t\t\t";
            echo (isset($context["validation_errors"]) ? $context["validation_errors"] : null);
            echo "
\t\t\t";
        }
        // line 28
        echo "\t\t</div>
\t</div>

";
        // line 31
        if ((twig_length_filter($this->env, (isset($context["id"]) ? $context["id"] : null)) <= 1)) {
            // line 32
            echo form_open("pengadaan/submit", array("id" => "frmMain"));
            echo "
";
        } else {
            // line 34
            echo form_open(("pengadaan/submit/" . (isset($context["id"]) ? $context["id"] : null)), array("id" => "frmMain"));
            echo "
";
        }
        // line 36
        echo "\t<div class=\"row\">
\t\t<div class=\"col-sm-12\" style=\"padding-left:0px; padding-right:0px;\">
\t\t\t<!-- <div class=\"row\">
\t\t\t\t<div class=\"col-xs-2\">
\t\t\t\t\t<label>Jenis Pengadaan</label>
\t\t\t\t\t<select type=\"text\" name=\"jenispengadaan\" class=\"form-control\">
\t\t\t\t\t\t<option value=\"\">Jenis Pengadaan</option>
\t\t\t\t\t\t<option value=\"1\">Baru</option>
\t\t\t\t\t\t<option value=\"2\">Tambah Nilai</option>
\t\t\t\t\t</select>
\t\t\t\t</div>
\t\t\t</div> -->
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-xs-5\">
\t\t\t\t\t<label>SKPD</label>
\t\t\t\t\t<select type=\"text\" name=\"jpnomorlokasi\" class=\"form-control\">
\t\t\t\t\t\t<option value=\"\">Pilih SKPD</option>
\t\t\t\t\t</select>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-xs-2\">
\t\t\t\t\t<label class=\"control-label\">Tgl.Jurnal</label>
\t\t\t\t\t<div data-date-format=\"dd-mm-yyyy\" data-date=\"";
        // line 59
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "jptgl"), "html", null, true);
        echo "\" datepicker class=\"input-group input-append date\">
\t\t\t\t\t\t<input name=\"jptgl\" readonly=\"\" class=\"form-control\" value=\"";
        // line 60
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "jptgl"), "html", null, true);
        echo "\"></input>
\t\t\t\t\t\t<span class=\"input-group-addon add-on\"><i class=\"icon-calendar\"></i></span>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"clearfix\"></div>
\t\t\t<div class=\"spacer\"></div>
\t\t\t<div class=\"row\">
\t\t\t\t<ul id=\"wp\" class=\"nav nav-tabs\">
\t\t\t\t\t<li class=\"active\"><a href=\"#berita-acara\" data-toggle=\"tab\">Berita Acara</a></li>
\t\t\t\t\t<li><a href=\"#spk\" data-toggle=\"tab\"";
        // line 70
        if (((isset($context["isSaved"]) ? $context["isSaved"] : null) == 0)) {
            echo "disabled=\"\"";
        }
        echo ">SPK</a></li>
\t\t\t\t\t<li><a href=\"#rincian\" data-toggle=\"tab\" ";
        // line 71
        if (((isset($context["isSaved"]) ? $context["isSaved"] : null) == 0)) {
            echo "disabled=\"\"";
        }
        echo ">Rincian</a></li>
\t\t\t\t\t<li><a href=\"#pembayaran\" data-toggle=\"tab\" ";
        // line 72
        if (((isset($context["isSaved"]) ? $context["isSaved"] : null) == 0)) {
            echo "disabled=\"\"";
        }
        echo ">Pembayaran</a></li>
\t\t\t\t</ul>
\t\t\t\t<div class=\"tab-content\">
\t\t\t\t\t<div class=\"tab-pane fade in active\" id=\"berita-acara\">
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-xs-5\">
\t\t\t\t\t\t\t\t<label class=\"control-label\">No.BA PPHP</label>
\t\t\t\t\t\t\t\t<input class=\"form-control\" name=\"jpnobaperiksa\" value=\"";
        // line 79
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "jpnobaperiksa"), "html", null, true);
        echo "\"/>\t\t\t\t\t\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-2\">
\t\t\t\t\t\t\t\t<label class=\"control-label\">Tgl.BA PPHP</label>
\t\t\t\t\t\t\t\t<div data-date-format=\"dd-mm-yyyy\" data-date=\"";
        // line 83
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "jptglbaperiksa"), "html", null, true);
        echo "\" datepicker class=\"input-group input-append date\">
\t\t\t\t\t\t\t\t\t<input name=\"jptglbaperiksa\" readonly=\"\" class=\"form-control\" value=\"";
        // line 84
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "jptglbaperiksa"), "html", null, true);
        echo "\"/>
\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon add-on\"><i class=\"icon-calendar\"></i></span>
\t\t\t\t\t\t\t\t</div>\t\t\t\t\t\t\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-xs-5\">
\t\t\t\t\t\t\t\t<label class=\"control-label\">No.BA Penerimaan</label>
\t\t\t\t\t\t\t\t<input class=\"form-control\" name=\"jpnobaterima\" value=\"";
        // line 92
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "jpnobaterima"), "html", null, true);
        echo "\"/>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-2\">
\t\t\t\t\t\t\t\t<label class=\"control-label\">Tgl.BA Penerimaan</label>
\t\t\t\t\t\t\t\t<div data-date-format=\"dd-mm-yyyy\" data-date=\"";
        // line 96
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "jptglbaterima"), "html", null, true);
        echo "\" datepicker class=\"input-group input-append date\">
\t\t\t\t\t\t\t\t\t<input name=\"jptglbaterima\" readonly=\"\" class=\"form-control\" value=\"";
        // line 97
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "jptglbaterima"), "html", null, true);
        echo "\"/>
\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon add-on\"><i class=\"icon-calendar\"></i></span>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"tab-pane fade in\" id=\"spk\">
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-xs-4\">
\t\t\t\t\t\t\t\t<label class=\"control-label\">No.SPK/SP/Kontrak</label>
\t\t\t\t\t\t\t\t<input name=\"id_kontrak\" type=\"hidden\" />
\t\t\t\t\t\t\t\t<select class=\"form-control\" name=\"no_spk_sp_dokumen\" dropdown-width=\"600\"></select>\t\t\t\t\t\t\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-2\">
\t\t\t\t\t\t\t\t<label class=\"control-label\">Tanggal SPK/SP/Kontrak</label>
\t\t\t\t\t\t\t\t<div data-date-format=\"dd-mm-yyyy\" data-date=\"\" datepicker class=\"input-group input-append date\">
\t\t\t\t\t\t\t\t\t<input name=\"tgl_spk_sp_dokumen\" readonly=\"\" class=\"form-control\" value=\"\"></input>
\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon add-on\"><i class=\"icon-calendar\"></i></span>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t\t\t\t<label class=\"control-label\">Nilai SPK/SP/Kontrak</label>
\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\">Rp.</span>
\t\t\t\t\t\t\t\t\t<input class=\"form-control\" style=\"text-align:right;\" name=\"nilai_spk\" value=\"\" readonly=\"\" />
\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\">,00</span>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row\">\t\t\t
\t\t\t\t\t\t\t<div class=\"col-xs-7\">
\t\t\t\t\t\t\t\t<label>Uraian Pekerjaan</label>
\t\t\t\t\t\t\t\t<textarea class=\"form-control\" rows=\"4\" name=\"deskripsi_spk_dokumen\" readonly=\"\"></textarea>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row\">\t\t\t
\t\t\t\t\t\t\t<div class=\"col-xs-5\">
\t\t\t\t\t\t\t\t<label>Nama Rekanan</label>
\t\t\t\t\t\t\t\t<input class=\"form-control\" name=\"rekanan\" value=\"\" readonly=\"\" />
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-xs-6\">
\t\t\t\t\t\t\t\t<label>Kegiatan</label>
\t\t\t\t\t\t\t\t<select name=\"kode_kegiatan\" class=\"form-control\" disabled></select>
\t\t\t\t\t\t\t</div>\t\t\t\t\t\t
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"tab-pane fade in\" id=\"rincian\">
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<ul class=\"nav nav-tabs\">
\t\t\t\t\t\t\t\t<li class=\"active\"><a href=\"#rincian1\" data-toggle=\"tab\">Aset Tetap</a></li>
\t\t\t\t\t\t\t\t<li><a href=\"#rincian2\" data-toggle=\"tab\">Penambahan Nilai</a></li>
\t\t\t\t\t\t\t\t<li><a href=\"#rincian3\" data-toggle=\"tab\">KDP</a></li>
\t\t\t\t\t\t\t\t<li><a href=\"#rincian4\" data-toggle=\"tab\">Jasa</a></li>
\t\t\t\t\t\t\t\t<li><a href=\"#rincian5\" data-toggle=\"tab\">Pakai Habis</a></li>
\t\t\t\t\t\t\t\t<li><a href=\"#rincian6\" data-toggle=\"tab\">Bantuan</a></li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-xs-6\">
\t\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#pilihan\"><span><i class=\"icon-plus\"></i></span> Tambah</button>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"tab-content\">
\t\t\t\t\t\t\t<div class=\"tab-pane fade in active\" id=\"rincian1\">
\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t<div class=\"col-md-11\">
\t\t\t\t\t\t\t\t\t\t<div class=\"table-responsive\" style=\"overflow:auto;\">
\t\t\t\t\t\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t\t\t\t\t\t<table class=\"table table-striped table-hover\" id=\"daftar-rincian1\" style=\"width:2000px;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th rowspan=\"2\"></th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th rowspan=\"2\">No.Status Guna</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th rowspan=\"2\">Kd.Barang</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th rowspan=\"2\">Kode Neraca</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th rowspan=\"2\">Nama Barang</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th rowspan=\"2\">Merk / Alamat / Jenis</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th colspan=\"2\">Ruangan</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th rowspan=\"2\">Qty</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th rowspan=\"2\">H.Tot+Pajak</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th rowspan=\"2\">Tipe</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th rowspan=\"2\">Keterangan</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th rowspan=\"2\">Nopol</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th rowspan=\"2\">Thn.Pengadaan</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"center\">Kode</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th class=\"center\">Nama</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"tab-pane fade in\" id=\"rincian2\">
\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t<div class=\"col-md-11\">
\t\t\t\t\t\t\t\t\t\t<div class=\"table-responsive\" style=\"overflow:auto;\">
\t\t\t\t\t\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t\t\t\t\t\t<table class=\"table table-striped table-hover\" id=\"daftar-rincian2\" style=\"width:2000px;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th rowspan=\"2\"></th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th rowspan=\"2\">No.Regs Induk</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th rowspan=\"2\">Kd.Barang</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th rowspan=\"2\">Kode Neraca</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th rowspan=\"2\">Nama Barang</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th rowspan=\"2\">Merk / Alamat / Jenis</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th colspan=\"2\" class=\"center\">Ruangan</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th rowspan=\"2\">Qty</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th rowspan=\"2\">H.Tot+Pajak</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th rowspan=\"2\">Tipe</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th rowspan=\"2\">Keterangan</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th rowspan=\"2\">Nopol</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th rowspan=\"2\">Thn.Pengadaan</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th >Kode</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th >Nama</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"tab-pane fade in\" id=\"rincian3\">
\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t<div class=\"col-md-11\">
\t\t\t\t\t\t\t\t\t\t<div class=\"table-responsive\" style=\"overflow:auto;\">
\t\t\t\t\t\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t\t\t\t\t\t<table class=\"table table-striped table-hover\" id=\"daftar-rincian3\" style=\"width:2000px;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th></th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th>No.Regs Induk</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th>Kd.Barang</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th>Kode Neraca</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th>Nama Barang</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th>Merk / Alamat / Jenis</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th>Nilai KDP</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th>Nilai Mnrt Kontrak</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th>Tipe</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th>Qty</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th>Keterangan</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th>Nopol</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th>Thn.Pengadaan</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"tab-pane fade in\" id=\"rincian4\">
\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t<div class=\"col-md-11\">
\t\t\t\t\t\t\t\t\t\t<div class=\"table-responsive\" style=\"overflow:auto;\">
\t\t\t\t\t\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t\t\t\t\t\t<table class=\"table table-striped table-hover\" id=\"daftar-rincian4\" style=\"width:2000px;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th></th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th>Uraian</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th>Merk / Alamat / Jenis</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th>Tipe</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th>Qty</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th>H.Tot+Pajak</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th>Keterangan</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"tab-pane fade in\" id=\"rincian5\">
\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t<div class=\"col-md-11\">
\t\t\t\t\t\t\t\t\t\t<div class=\"table-responsive\" style=\"overflow:auto;\">
\t\t\t\t\t\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t\t\t\t\t\t<table class=\"table table-striped table-hover\" id=\"daftar-rincian5\" style=\"width:2000px;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th></th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th>Uraian</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th>Merk / Alamat / Jenis</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th>Tipe</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th>Qty</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th>H.Tot+Pajak</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th>Keterangan</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"tab-pane fade in\" id=\"rincian6\">
\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t<div class=\"col-md-11\">
\t\t\t\t\t\t\t\t\t\t<div class=\"table-responsive\" style=\"overflow:auto;\">
\t\t\t\t\t\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t\t\t\t\t\t<table class=\"table table-striped table-hover\" id=\"daftar-rincian6\" style=\"width:2000px;\">
\t\t\t\t\t\t\t\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th></th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th>Uraian</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th>Merk / Alamat / Jenis</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th>Tipe</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th>Qty</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th>H.Tot+Pajak</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t<th>Keterangan</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"tab-pane fade in\" id=\"pembayaran\">
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-xs-2\">
\t\t\t\t\t\t\t\t<button id=\"sync\" type=\"button\" class=\"btn btn-primary\"><span><i class=\"icon-refresh\"></i></span> Checking</button>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-6\">
\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"no_spm_input\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-md-11\">
\t\t\t\t\t\t\t\t<div class=\"table-responsive\" style=\"overflow:auto;\">
\t\t\t\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t\t\t\t<table class=\"table table-striped table-hover\" id=\"rincian-pembayaran\" style=\"width:2000px;\">
\t\t\t\t\t\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t\t\t\t<th>No.SP2D</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t<th>No.SPM</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t<th>Nilai SPM/SPMU</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t<th>Kode Rek</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t<th>Uraian Belanja</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t<th>Termin Ke</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t<th>Tahun</th>
\t\t\t\t\t\t\t\t\t\t\t\t\t<th>Keterangan</th>
\t\t\t\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-xs-2\">
\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t<button id=\"submit\" class=\"btn btn-success\" type=\"button\">Simpan</button>
\t\t\t\t\t</div>
\t\t\t\t</div>\t
\t\t\t</div>\t
\t\t</div>\t\t
\t</div>
";
        // line 392
        echo form_close();
        echo "
";
    }

    // line 396
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 397
        echo "\t";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "

\t<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 399
        echo twig_escape_filter($this->env, base_url("assets/css/datepicker3.css"), "html", null, true);
        echo "\" />
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 400
        echo twig_escape_filter($this->env, base_url("assets/css/devexpress-like/jquery-ui.css"), "html", null, true);
        echo "\" />
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 401
        echo twig_escape_filter($this->env, base_url("assets/css/select2.css"), "html", null, true);
        echo "\" />
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 402
        echo twig_escape_filter($this->env, base_url("assets/css/typeahead.css"), "html", null, true);
        echo "\" />
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 403
        echo twig_escape_filter($this->env, base_url("assets/css/jquery.loadmask.css"), "html", null, true);
        echo "\" />

\t<style type=\"text/css\">
\t\t#sync{
\t\t\tmargin-top: 10px !important;
\t\t}
\t\t.control-label{
\t\t\tpadding-top: 8px !important;
\t\t}

\t\tselect.form-control {
\t\t\theight: 48px !important;
\t\t}

\t\tinput.form-control {
\t\t\theight: 34px !important;
\t\t}

\t\ttextarea.form-control {
\t\t\theight:auto !important;
\t\t}

\t\t.row {
\t\t\tpadding:10px 0px;
\t\t}

\t\t.spacer {
\t\t    margin-top: 20px;
\t\t}

\t\t.modal {
\t\t\toverflow-y: hidden;
\t\t}

\t\t.ui-autocomplete{
\t\t\tbackground: none repeat scroll 0 0 white;
\t\t}

\t\t.input-group-addon{
\t\t\tborder-radius: 0 !important;
\t\t}

\t\t.input-group .form-control{
\t\t\tz-index: 0; !important;
\t\t}

\t\t.modal-body {
\t\t\toverflow-y:auto; 
\t\t\theight:350px;
\t\t}

\t\thr {
\t\t\tborder-color: #ccc;
\t\t}

\t\tul[id*=no_spk_sp_dokumen]:parent {
\t\t\twidth: 800px !important;
\t\t}

\t\t.select2-selection__rendered {
\t\t\theight: 28px;
\t\t}

\t\ttr > th:first-child {
\t\t\twidth: 75px;
\t\t}
\t</style>
";
    }

    // line 473
    public function block_scripts($context, array $blocks = array())
    {
        // line 474
        echo "\t";
        $this->displayParentBlock("scripts", $context, $blocks);
        echo "

\t<script type=\"text/javaScript\" src=\"";
        // line 476
        echo twig_escape_filter($this->env, base_url("assets/js/bootstrap-datepicker.js"), "html", null, true);
        echo "\"></script>
\t<script type=\"text/javaScript\" src=\"";
        // line 477
        echo twig_escape_filter($this->env, base_url("assets/js/accounting.min.js"), "html", null, true);
        echo "\"></script>
\t<script type=\"text/javaScript\" src=\"";
        // line 478
        echo twig_escape_filter($this->env, base_url("assets/js/jquery.validate.min.js"), "html", null, true);
        echo "\"></script>
\t<script type=\"text/javaScript\" src=\"";
        // line 479
        echo twig_escape_filter($this->env, base_url("assets/js/select2.js"), "html", null, true);
        echo "\"></script>
\t<script type=\"text/javaScript\" src=\"";
        // line 480
        echo twig_escape_filter($this->env, base_url("assets/js/typeahead.js"), "html", null, true);
        echo "\"></script>
\t<script type=\"text/javaScript\" src=\"";
        // line 481
        echo twig_escape_filter($this->env, base_url("assets/js/jquery.loadmask.min.js"), "html", null, true);
        echo "\"></script>
\t<script type=\"text/javaScript\" src=\"";
        // line 482
        echo twig_escape_filter($this->env, base_url("assets/js/common.js"), "html", null, true);
        echo "\"></script>
\t<script type=\"text/javaScript\" src=\"";
        // line 483
        echo twig_escape_filter($this->env, base_url("assets/js/base64.js"), "html", null, true);
        echo "\"></script>

\t<script type=\"text/javascript\">
\t\tvar guid = (function() {
\t\t\tfunction s4() {
\t\t\t  \treturn Math.floor((1 + Math.random()) * 0x10000)
\t\t\t\t\t.toString(16)
\t\t\t\t\t.substring(1);
\t\t\t}
\t\t\treturn function() {
\t\t\t  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
\t\t\t\t\t s4() + '-' + s4() + s4() + s4();
\t\t\t};
\t\t  })();
\t</script>

\t<script type=\"text/javascript\">
\t\tjQuery(function(\$) {
\t\t\tvar pembayaran = []; // storage fetch get_pembayaran
\t\t\tvar header = false;

\t\t\tif (!String.prototype.format) {
\t\t\t\tString.prototype.format = function() {
\t\t\t\t\tvar args = arguments;
\t\t\t\t\treturn this.replace(/{(\\d+)}/g, function(match, number) { 
\t\t\t\t\t\treturn typeof args[number] != 'undefined' ? args[number] : match;
\t\t\t\t\t});
\t\t\t\t};
\t\t\t}

\t\t\tif (!String.prototype.startsWith) {
\t\t\t\tString.prototype.startsWith = function(prefix) {
\t\t\t\t    return this.indexOf(prefix) === 0;
\t\t\t\t}
\t\t\t}



\t\t\t// configure setting accounting.js
\t\t\taccounting.settings = {
\t\t\t\tnumber: {
\t\t\t\t\tprecision : 0,  // default precision on numbers is 0
\t\t\t\t\tthousand: \",\",
\t\t\t\t\tdecimal : \".\"
\t\t\t\t}
\t\t\t}

\t\t\tvar n = function(v, p) {
\t\t\t\treturn accounting.formatNumber(v, p);
\t\t\t}

\t\t\tvar u = function(v) {
\t\t\t\treturn accounting.unformat(v);
\t\t\t}



\t\t\t\$(\"select[name=jenispengadaan]\").select2();
\t\t\t\$(\"*[datepicker]\").datepicker();



\t\t\t/* ==================== tabs ==================== */
\t\t\t\$(\"#wp.nav-tabs a[data-toggle=tab]\").on(\"click\", function(e) {
\t\t\t\tif (\$(e.currentTarget).attr(\"href\") == \"#spk\") {
\t\t\t\t\tif (\$(\"form#frmMain\").valid()) {
\t\t\t\t\t\t\$(e.currentTarget).removeAttr(\"disabled\");
\t\t\t\t\t}
\t\t\t\t}

\t\t\t\tif (\$(this).attr(\"disabled\") !== undefined) {
\t\t\t\t\talert(\"Simpan data terlebih dahulu agar dapat mengisi spk atau rincian atau pembayaran\");
\t\t\t\t\te.preventDefault();
\t\t\t\t\treturn false;
\t\t\t\t}
\t\t\t}).on('shown.bs.tab', function (e) {
\t\t\t\tvar jptgl = \$(\"input[name=jptgl]\");

\t\t\t\tif (\$(e.currentTarget).attr(\"href\") == \"#spk\") {
\t\t\t\t\t//populate SPK
\t\t\t\t\treload_kegiatan(function() {
\t\t\t\t\t\t\$(\"select[name=no_spk_sp_dokumen]\").select2({
\t\t\t\t\t\t\tajax: {
\t\t\t\t\t\t\t\turl: '";
        // line 566
        echo twig_escape_filter($this->env, site_url("spk/find_range_by_term"), "html", null, true);
        echo "',
\t\t\t\t\t\t\t\ttype: \"POST\",
\t\t\t\t\t\t\t\tdataType: \"json\",
\t\t\t\t\t\t\t\tdata: function(params) {
\t\t\t\t\t\t\t\t\treturn {
\t\t\t\t\t\t\t\t\t\ttahun: jptgl.val().split(\"-\").pop(),
\t\t\t\t\t\t\t\t\t\tnomor_lokasi: \$(\"select[name=jpnomorlokasi]\").val(),
\t\t\t\t\t\t\t\t\t\tterm: params.term
\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t},
\t\t\t\t\t\t\t\tprocessResults: function(json) {
\t\t\t\t\t\t\t\t\treturn {
\t\t\t\t\t\t\t\t\t\tresults: _.map(json.data, function(i) {
\t\t\t\t\t\t\t\t\t\t\treturn {
\t\t\t\t\t\t\t\t\t\t\t\tid: i.ID_KONTRAK,
\t\t\t\t\t\t\t\t\t\t\t\tdata: i
\t\t\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t\t\t})
\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t},
\t\t\t\t\t\t\ttemplateResult: function(i) {
\t\t\t\t\t\t\t\tif (i.loading) return i.text
\t\t\t\t\t\t\t\treturn \"<div class='row'><div class='col-xs-6'>\" + i.data.NO_SPK_SP_DOKUMEN + \"</div><div class='col-xs-6'>\" + i.data.REKANAN + \"</div></div>\";
\t\t\t\t\t\t\t},
\t\t\t\t\t\t\ttemplateSelection: function(i) {
\t\t\t\t\t\t\t\tvar nomor_lokasi = \$(\"select[name=jpnomorlokasi]\").val();
\t\t\t\t\t\t\t\tvar nomor_sub_unit = nomor_lokasi.substring(0, nomor_lokasi.length - 3);

\t\t\t\t\t\t\t\tif (i.data != undefined) {
\t\t\t\t\t\t\t\t\t\$(\"input[name=rekanan]\").val(i.data.REKANAN);
\t\t\t\t\t\t\t\t\t\$(\"input[name=nilai_spk]\").val(n(i.data.NILAI_SPK));
\t\t\t\t\t\t\t\t\t\$(\"input[name=tgl_spk_sp_dokumen]\").val(i.data.TGL_SPK_SP_DOKUMEN);
\t\t\t\t\t\t\t\t\t\$(\"textarea[name=deskripsi_spk_dokumen]\").text(i.data.DESKRIPSI_SPK_DOKUMEN);
\t\t\t\t\t\t\t\t\t\$(\"select[name=kode_kegiatan]\").val(i.data.ID_KEGIATAN.substring(nomor_sub_unit.length, i.data.ID_KEGIATAN.length-4)).trigger(\"change\");

\t\t\t\t\t\t\t\t\tvar thn = i.data.TGL_SPK_SP_DOKUMEN.split(\"-\").shift();

\t\t\t\t\t\t\t\t\t\$(\"input[name=id_kontrak][type=hidden]\").val(i.data.NO_SPK_SP_DOKUMEN + thn + nomor_sub_unit);

\t\t\t\t\t\t\t\t\tvar opt = \$(i.element);
\t\t\t\t\t\t\t\t\topt.attr(\"rekanan\", i.data.REKANAN);
\t\t\t\t\t\t\t\t\topt.attr(\"nilai_spk\", i.data.NILAI_SPK);
\t\t\t\t\t\t\t\t\topt.attr(\"tgl_spk_sp_dokumen\", i.data.TGL_SPK_SP_DOKUMEN);
\t\t\t\t\t\t\t\t\topt.attr(\"deskripsi_spk_dokumen\", i.data.DESKRIPSI_SPK_DOKUMEN);
\t\t\t\t\t\t\t\t\topt.attr(\"no_spk_sp_dokumen\", i.data.NO_SPK_SP_DOKUMEN + thn + nomor_lokasi);
\t\t\t\t\t\t\t\t\topt.attr(\"id_kegiatan\", i.data.ID_KEGIATAN);

\t\t\t\t\t\t\t\t\treturn i.data.NO_SPK_SP_DOKUMEN;
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tvar el = \$(i.element);

\t\t\t\t\t\t\t\t\t\$(\"input[name=rekanan]\").val(el.attr(\"rekanan\"));
\t\t\t\t\t\t\t\t\t\$(\"input[name=nilai_spk]\").val(n(el.attr(\"nilai_spk\")));
\t\t\t\t\t\t\t\t\t\$(\"input[name=tgl_spk_sp_dokumen]\").val(el.attr(\"tgl_spk_sp_dokumen\"));
\t\t\t\t\t\t\t\t\t\$(\"textarea[name=deskripsi_spk_dokumen]\").text(el.attr(\"deskripsi_spk_dokumen\"));

\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\tvar id_kegiatan = el.attr(\"id_kegiatan\");
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t\t\$(\"select[name=kode_kegiatan]\").val(id_kegiatan.substring(nomor_sub_unit.length, id_kegiatan.length-4)).trigger(\"change\");

\t\t\t\t\t\t\t\t\tvar thn = el.attr(\"tgl_spk_sp_dokumen\").split(\"-\").shift();

\t\t\t\t\t\t\t\t\t\$(\"input[name=id_kontrak][type=hidden]\").val(el.attr(\"no_spk_sp_dokumen\") + thn + nomor_sub_unit);

\t\t\t\t\t\t\t\t\treturn el.attr(\"no_spk_sp_dokumen\");
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t});
\t\t\t\t\t});
\t\t\t\t}
\t\t\t});



\t\t\t//menampilkan list SKPD
\t\t\t\$.get(\"";
        // line 643
        echo twig_escape_filter($this->env, site_url("aset/get_all_lokasi"), "html", null, true);
        echo "\", {}, function(json){ 
\t\t\t\tvar listSkpd = json.result;
\t\t\t\t
\t\t\t\t\$.each(listSkpd, function (key, value) {
\t\t\t\t\t\$(\"select[name=jpnomorlokasi]\").append(\$('<option></option>').val(value.NOMOR_LOKASI).html(value.LOKASI));
\t\t\t\t});

\t\t\t\t\$(\"select[name=jpnomorlokasi]\").val(\"";
        // line 650
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "jpnomorlokasi"), "html", null, true);
        echo "\");
\t\t\t\t\$(\"select[name=jpnomorlokasi]\").select2();
\t\t\t\treload_kegiatan();
\t\t\t}, \"json\");



\t\t\t// event onchange select skpd dan tahun kegiatan
\t\t\t\$(\"select[name=jpnomorlokasi]\").change(function() {
\t\t\t\treload_kegiatan();
\t\t\t});



\t\t\tvar reload_kegiatan = function(callback) {
\t\t\t\tvar jpnomorlokasi = \$(\"select[name=jpnomorlokasi]\").val();
\t\t\t\tvar jptgl = \$(\"input[name=jptgl]\").val();

\t\t\t\t\$.post(\"";
        // line 668
        echo twig_escape_filter($this->env, site_url("spk/get_list_kegiatan_by_nomor_lokasi"), "html", null, true);
        echo "\", {
\t\t\t\t\tnomor_lokasi: jpnomorlokasi,
\t\t\t\t\ttahun_spj: jptgl.split(\"-\").pop()
\t\t\t\t}, function(json){ 
\t\t\t\t\tvar data = json.data;
\t\t\t\t\tvar el = \$(\"select[name=kode_kegiatan]\");

\t\t\t\t\tel.empty();
\t\t\t\t\tel.append(\"<option value=''>Pilih Kegiatan</option>\");
\t\t\t\t\tfor (var i=0; i<data.length; i++) {
\t\t\t\t\t\tvar k = data[i];
\t\t\t\t\t\tel.append(\"<option value='{0}'>{1}</option>\".format(k.KODE_KEGIATAN, k.NAMA_KEGIATAN));
\t\t\t\t\t}

\t\t\t\t\tel.select2();

\t\t\t\t\tif (callback !== undefined) {
\t\t\t\t\t\tcallback();
\t\t\t\t\t}
\t\t\t\t}, \"json\");
\t\t\t}



\t\t\tfunction formatSubSubKel (d) {\t\t\t\t
\t\t\t\tvar el = \$(d.element);
\t\t\t\tvar r = [];
\t\t\t\t
\t\t\t\tif (\$(\".select2-results__options li\").find(\"[header]\").length <= 0 && !header) {
\t\t\t\t\tr.push([
\t\t\t\t\t\t'<div header class=\"row\" style=\"width:100%; font-weight:bold; color:#000000;\">',
\t\t\t\t\t\t\t'<div class=\"col-xs-1\">',
\t\t\t\t\t\t\t\t'KODE',
\t\t\t\t\t\t\t'</div>',
\t\t\t\t\t\t\t'<div class=\"col-xs-2\">',
\t\t\t\t\t\t\t\t'SUB SUB KEL',
\t\t\t\t\t\t\t'</div>',
\t\t\t\t\t\t\t'<div class=\"col-xs-2\">',
\t\t\t\t\t\t\t\t'SUB KEL',
\t\t\t\t\t\t\t'</div>',
\t\t\t\t\t\t\t'<div class=\"col-xs-2\">',
\t\t\t\t\t\t\t\t'KELOMPOK',
\t\t\t\t\t\t\t'</div>',
\t\t\t\t\t\t\t'<div class=\"col-xs-1\">',
\t\t\t\t\t\t\t\t'BIDANG',
\t\t\t\t\t\t\t'</div>',
\t\t\t\t\t\t\t'<div class=\"col-xs-1\">',
\t\t\t\t\t\t\t\t'GOLONGAN',
\t\t\t\t\t\t\t'</div>',
\t\t\t\t\t\t'</div>',
\t\t\t\t\t].join(\"\"));
\t\t\t\t\theader = true;
\t\t\t\t}

\t\t\t\tr.push([
\t\t\t\t\t'<div class=\"row\" style=\"width:100%;\">',
\t\t\t\t\t\t'<div class=\"col-xs-1\">',
\t\t\t\t\t\t\td.KODE_SUB_SUB_KELOMPOK,
\t\t\t\t\t\t'</div>',
\t\t\t\t\t\t'<div class=\"col-xs-2\">',
\t\t\t\t\t\t\td.DESKRIPSI,
\t\t\t\t\t\t'</div>',
\t\t\t\t\t\t'<div class=\"col-xs-2\">',
\t\t\t\t\t\t\td.NAMA_SUB_KELOMPOK,
\t\t\t\t\t\t'</div>',
\t\t\t\t\t\t'<div class=\"col-xs-2\">',
\t\t\t\t\t\t\td.NAMA_KELOMPOK,
\t\t\t\t\t\t'</div>',
\t\t\t\t\t\t'<div class=\"col-xs-1\">',
\t\t\t\t\t\t\td.NAMA_BIDANG,
\t\t\t\t\t\t'</div>',
\t\t\t\t\t\t'<div class=\"col-xs-1\">',
\t\t\t\t\t\t\td.NAMA_GOLONGAN,
\t\t\t\t\t\t'</div>',
\t\t\t\t\t'</div>',
\t\t\t\t].join(\"\"));

\t\t\t\treturn r;
\t\t\t};

\t\t\tfunction formatSelection(e) {
\t\t\t\tvar root = \$(e.element).closest(\".modal-body\");
\t\t\t\t
\t\t\t\tif (e.KODE_SUB_KEL_AT !== undefined)
\t\t\t\t\troot.find(\"input[name=kbkoderekneraca]\").val(e.KODE_SUB_KEL_AT);
\t\t\t\tif (e.DESKRIPSI !== undefined)
\t\t\t\t\troot.find(\"input[name=kbnamabarang]\").val(e.DESKRIPSI);

\t\t\t\treturn e.KODE_SUB_SUB_KELOMPOK || e.text;
\t\t\t}

\t\t\t\$(\"#pilihan\").on('show.bs.modal', function (e) {
\t\t\t\t\$(\"#pilihan\").find(\"input[name=optradio]\").prop('checked', false);
\t\t\t})



\t\t\t// logic pilihan
\t\t\t\$(\".tambah-mutasi\").click(function() {
\t\t\t\tfunction cbPopulateKodeSubSubKel(tipe, callback) {
\t\t\t\t\tvar root = \$(\"#\" + tipe);
\t\t\t\t\tvar kbkodesubsubkel = root.find(\"select[name=kbkodesubsubkel]\");

\t\t\t\t\tkbkodesubsubkel.unbind().select2({
\t\t\t\t\t\tajax: {
\t\t\t\t\t\t\turl: \"";
        // line 773
        echo twig_escape_filter($this->env, site_url("pengadaan/autocomplete_sub_sub_kel"), "html", null, true);
        echo "\",
\t\t\t\t\t\t\tdataType: 'json',
\t\t\t\t\t\t\tdelay: 250,
\t\t\t\t\t\t\tdata: function (params) {
\t\t\t\t\t\t\t\treturn {
\t\t\t\t\t\t\t\t\ttipe:tipe,
\t\t\t\t\t\t\t\t\tq:params.term,
\t\t\t\t\t\t\t\t\tpage: params.page
\t\t\t\t\t\t\t\t};
\t\t\t\t\t\t\t},
\t\t\t\t\t\t\tprocessResults: function (response, page) {
\t\t\t\t\t\t\t\treturn {
\t\t\t\t\t\t\t\t\tresults: response.data
\t\t\t\t\t\t\t\t};
\t\t\t\t\t\t\t}
\t\t\t\t\t\t},
\t\t\t\t\t\tescapeMarkup: function (markup) { return markup; },
\t\t\t\t\t\tminimumInputLength: 1,
\t\t\t\t\t\ttemplateResult: formatSubSubKel, // omitted for brevity, see the source of this page
\t\t\t\t\t\ttemplateSelection: formatSelection
\t\t\t\t\t});
\t\t\t\t\t\t
\t\t\t\t\tcallback();
\t\t\t\t}

\t\t\t\tfunction clearInput(el) {
\t\t\t\t\tel.find(\"input, select, textarea\").val(\"\");
\t\t\t\t\tel.find(\"input[type=checkbox]\").prop(\"checked\", false);

\t\t\t\t\tel.find(\"input[name='kbjumlah'], input[name='kbhargasatuan'], input[name='kbhargatotal'], input[name='kbhargatotalpajak']\").val(u(0));

\t\t\t\t\tel.find(\".modal-body\").animate({scrollTop:0});
\t\t\t\t}

\t\t\t\tfunction applyInputEvents(root) {
\t\t\t\t\troot.find(\"input[name=kbjumlah], input[name=kbhargasatuan], input[name=kbhargatotal]\").unbind(\"keyup\").on(\"keyup\", function() {
\t\t\t\t\t\tvar kbjumlah = root.find(\"input[name=kbjumlah]\");
\t\t\t\t\t\tvar kbhargasatuan = root.find(\"input[name=kbhargasatuan]\");
\t\t\t\t\t\tvar kbhargatotal = root.find(\"input[name=kbhargatotal]\");

\t\t\t\t\t\tkbhargatotal.val(n(u(kbjumlah.val()) * u(kbhargasatuan.val())));
\t\t\t\t\t});

\t\t\t\t\troot.find(\"input[name=kbjumlah], input[name=kbhargasatuan], input[name=kbhargatotal], input[name=kbhargatotalpajak]\").unbind(\"blur\").on(\"blur\", function() {
\t\t\t\t\t\t\$(this).val(n(u(\$(this).val())));
\t\t\t\t\t}).unbind(\"focus\").on(\"focus\", function() {
\t\t\t\t\t\t\$(this).val(u(\$(this).val()));
\t\t\t\t\t});
\t\t\t\t}


\t\t\t\tvar kode_kib = \$('input[name=\"optradio\"]:checked').val();

\t\t\t\tif (kode_kib >= 1 && kode_kib <= 16) {
\t\t\t\t\t\$(\"#pilihan\").modal('hide');
\t\t\t\t}

\t\t\t\tfunction populateAset(tipe, callback) {
\t\t\t\t\tvar root = \$(\"#\" + tipe);

\t\t\t\t\t\$(\"select[name=jenis_aset]\").change(function() {
\t\t\t\t\t\tvar jenis_aset = root.find(\"select[name=jenis_aset]\").val();
\t\t\t\t\t\tvar nama_aset = root.find(\"select[name=nama_aset]\").val();
\t\t\t\t\t\tvar nomor_sub_unit = (\$(\"select[name=jpnomorlokasi]\").val()).substr(0, 14);

\t\t\t\t\t\tvar a = \$(\"select[name=nama_aset]\");

\t\t\t\t\t\t\$.post(\"";
        // line 840
        echo twig_escape_filter($this->env, site_url("aset/get_aset"), "html", null, true);
        echo "\", {jenis_kib: jenis_aset, nomor_sub_unit: nomor_sub_unit}, function(r) {
\t\t\t\t\t\t\tvar data = r.data;
\t\t\t\t\t\t\ta.empty();
\t\t\t\t\t\t\tfor (var i = 0; i < data.length; i++) {
\t\t\t\t\t\t\t\tvar b = data[i];
\t\t\t\t\t\t\t\ta.append(\"<option value='\" + b.ID + \"'>\" + b.KETERANGAN + \"</option>\");
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}, \"json\");

\t\t\t\t\t});

\t\t\t\t\tcallback();
\t\t\t\t}
\t\t\t\t
\t\t\t\tswitch (kode_kib|0) {
\t\t\t\t\tcase 1:
\t\t\t\t\t\tsetTimeout(function() {
\t\t\t\t\t\t\tvar root = \$(\"#1a\");
\t\t\t\t\t\t\tvar content = root.find(\".modal-content\");

\t\t\t\t\t\t\troot.removeData(\"kbid\");
\t\t\t\t\t\t\troot.unbind().on('shown.bs.modal', function () {
\t\t\t\t\t\t\t\tcontent.mask(\"Loading...\");

\t\t\t\t\t\t\t\tclearInput(root);

\t\t\t\t\t\t\t\tapplyInputEvents(root);

\t\t\t\t\t\t\t\tcbPopulateKodeSubSubKel(\"1a\", function() {
\t\t\t\t\t\t\t\t\tcontent.unmask();
\t\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t})

\t\t\t\t\t\t\t\$('#1a').modal('show');
\t\t\t\t\t\t}, 1000);
\t\t\t\t\t\tbreak;
\t\t\t\t\tcase 2:
\t\t\t\t\t\tsetTimeout(function() {
\t\t\t\t\t\t\tvar root = \$(\"#1b\");
\t\t\t\t\t\t\tvar content = root.find(\".modal-content\");

\t\t\t\t\t\t\troot.removeData(\"kbid\");
\t\t\t\t\t\t\troot.unbind().on('shown.bs.modal', function () {
\t\t\t\t\t\t\t\tcontent.mask(\"Loading...\");

\t\t\t\t\t\t\t\tclearInput(root);

\t\t\t\t\t\t\t\tapplyInputEvents(root);

\t\t\t\t\t\t\t\tcbPopulateKodeSubSubKel(\"1b\", function() {
\t\t\t\t\t\t\t\t\tcontent.unmask();
\t\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t})

\t\t\t\t\t\t\t\$('#1b').modal('show');
\t\t\t\t\t\t}, 1000);
\t\t\t\t\t\tbreak;
\t\t\t\t\tcase 3:
\t\t\t\t\t\tsetTimeout(function() {
\t\t\t\t\t\t\tvar root = \$(\"#1c\");
\t\t\t\t\t\t\tvar content = root.find(\".modal-content\");

\t\t\t\t\t\t\troot.removeData(\"kbid\");
\t\t\t\t\t\t\troot.unbind().on('shown.bs.modal', function () {
\t\t\t\t\t\t\t\tcontent.mask(\"Loading...\");

\t\t\t\t\t\t\t\tclearInput(root);

\t\t\t\t\t\t\t\tapplyInputEvents(root);

\t\t\t\t\t\t\t\tcbPopulateKodeSubSubKel(\"1c\", function() {
\t\t\t\t\t\t\t\t\tcontent.unmask();
\t\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t})

\t\t\t\t\t\t\t\$('#1c').modal('show');
\t\t\t\t\t\t}, 1000);
\t\t\t\t\t\tbreak;
\t\t\t\t\tcase 4:
\t\t\t\t\t\tsetTimeout(function() {
\t\t\t\t\t\t\tvar root = \$(\"#1d\");
\t\t\t\t\t\t\tvar content = root.find(\".modal-content\");

\t\t\t\t\t\t\troot.removeData(\"kbid\");
\t\t\t\t\t\t\troot.unbind().on('shown.bs.modal', function () {
\t\t\t\t\t\t\t\tcontent.mask(\"Loading...\");

\t\t\t\t\t\t\t\tclearInput(root);

\t\t\t\t\t\t\t\tapplyInputEvents(root);

\t\t\t\t\t\t\t\tcbPopulateKodeSubSubKel(\"1d\", function() {
\t\t\t\t\t\t\t\t\tcontent.unmask();
\t\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t})

\t\t\t\t\t\t\t\$('#1d').modal('show');
\t\t\t\t\t\t}, 1000);
\t\t\t\t\t\tbreak;
\t\t\t\t\tcase 5:
\t\t\t\t\t\tsetTimeout(function() {
\t\t\t\t\t\t\tvar root = \$(\"#1e\");
\t\t\t\t\t\t\tvar content = root.find(\".modal-content\");

\t\t\t\t\t\t\troot.removeData(\"kbid\");
\t\t\t\t\t\t\troot.unbind().on('shown.bs.modal', function () {
\t\t\t\t\t\t\t\tcontent.mask(\"Loading...\");

\t\t\t\t\t\t\t\tclearInput(root);

\t\t\t\t\t\t\t\tapplyInputEvents(root);

\t\t\t\t\t\t\t\tcbPopulateKodeSubSubKel(\"1e\", function() {
\t\t\t\t\t\t\t\t\tcontent.unmask();
\t\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t})

\t\t\t\t\t\t\t\$('#1e').modal('show');
\t\t\t\t\t\t}, 1000);
\t\t\t\t\t\tbreak;
\t\t\t\t\tcase 6:
\t\t\t\t\t\tsetTimeout(function() {
\t\t\t\t\t\t\tvar root = \$(\"#1f\");
\t\t\t\t\t\t\tvar content = root.find(\".modal-content\");

\t\t\t\t\t\t\troot.removeData(\"kbid\");
\t\t\t\t\t\t\troot.unbind().on('shown.bs.modal', function () {
\t\t\t\t\t\t\t\tcontent.mask(\"Loading...\");

\t\t\t\t\t\t\t\tclearInput(root);

\t\t\t\t\t\t\t\tapplyInputEvents(root);

\t\t\t\t\t\t\t\tcbPopulateKodeSubSubKel(\"1f\", function(el) {
\t\t\t\t\t\t\t\t\tcontent.unmask();
\t\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t})

\t\t\t\t\t\t\t\$('#1f').modal('show');
\t\t\t\t\t\t}, 1000);
\t\t\t\t\t\tbreak;
\t\t\t\t\tcase 7:
\t\t\t\t\t\tsetTimeout(function() {
\t\t\t\t\t\t\tvar root = \$(\"#1g\");
\t\t\t\t\t\t\tvar content = root.find(\".modal-content\");

\t\t\t\t\t\t\troot.removeData(\"kbid\");
\t\t\t\t\t\t\troot.unbind().on('shown.bs.modal', function () {
\t\t\t\t\t\t\t\tcontent.mask(\"Loading...\");

\t\t\t\t\t\t\t\tclearInput(root);

\t\t\t\t\t\t\t\tapplyInputEvents(root);

\t\t\t\t\t\t\t\tcbPopulateKodeSubSubKel(\"1g\", function() {
\t\t\t\t\t\t\t\t\tcontent.unmask();
\t\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t})

\t\t\t\t\t\t\t\$('#1g').modal('show');
\t\t\t\t\t\t}, 1000);
\t\t\t\t\t\tbreak;
\t\t\t\t\tcase 8:
\t\t\t\t\t\tsetTimeout(function() {
\t\t\t\t\t\t\tvar root = \$(\"#1h\");
\t\t\t\t\t\t\tvar content = root.find(\".modal-content\");

\t\t\t\t\t\t\troot.removeData(\"kbid\");
\t\t\t\t\t\t\troot.unbind().on('shown.bs.modal', function () {
\t\t\t\t\t\t\t\tcontent.mask(\"Loading...\");

\t\t\t\t\t\t\t\tclearInput(root);

\t\t\t\t\t\t\t\tapplyInputEvents(root);

\t\t\t\t\t\t\t\tcbPopulateKodeSubSubKel(\"1h\", function() {
\t\t\t\t\t\t\t\t\tcontent.unmask();
\t\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t})

\t\t\t\t\t\t\t\$('#1h').modal('show');
\t\t\t\t\t\t}, 1000);
\t\t\t\t\t\tbreak;
\t\t\t\t\tcase 9:
\t\t\t\t\t\tsetTimeout(function() {
\t\t\t\t\t\t\tvar root = \$(\"#1i\");
\t\t\t\t\t\t\tvar content = root.find(\".modal-content\");

\t\t\t\t\t\t\troot.removeData(\"kbid\");
\t\t\t\t\t\t\troot.unbind().on('shown.bs.modal', function () {
\t\t\t\t\t\t\t\tcontent.mask(\"Loading...\");

\t\t\t\t\t\t\t\tclearInput(root);

\t\t\t\t\t\t\t\tapplyInputEvents(root);

\t\t\t\t\t\t\t\tcbPopulateKodeSubSubKel(\"1i\", function() {
\t\t\t\t\t\t\t\t\tcontent.unmask();
\t\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t})

\t\t\t\t\t\t\t\$('#1i').modal('show');
\t\t\t\t\t\t}, 1000);
\t\t\t\t\t\tbreak;
\t\t\t\t\tcase 10:
\t\t\t\t\t\tsetTimeout(function() {
\t\t\t\t\t\t\tvar root = \$(\"#1j\");
\t\t\t\t\t\t\tvar content = root.find(\".modal-content\");

\t\t\t\t\t\t\troot.removeData(\"kbid\");
\t\t\t\t\t\t\troot.unbind().on('shown.bs.modal', function () {
\t\t\t\t\t\t\t\tcontent.mask(\"Loading...\");

\t\t\t\t\t\t\t\tclearInput(root);

\t\t\t\t\t\t\t\tapplyInputEvents(root);

\t\t\t\t\t\t\t\tcbPopulateKodeSubSubKel(\"1j\", function() {
\t\t\t\t\t\t\t\t\tcontent.unmask();
\t\t\t\t\t\t\t\t});

\t\t\t\t\t\t\t\tpopulateAset(\"1j\", function () {
\t\t\t\t\t\t\t\t\tcontent.unmask();
\t\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t})

\t\t\t\t\t\t\t\$('#1j').modal('show');
\t\t\t\t\t\t}, 1000);
\t\t\t\t\t\tbreak;
\t\t\t\t\tcase 11:
\t\t\t\t\t\tsetTimeout(function() {
\t\t\t\t\t\t\t\$('#2a').modal('show');
\t\t\t\t\t\t}, 1000);
\t\t\t\t\t\tbreak;
\t\t\t\t\tcase 12:
\t\t\t\t\t\tsetTimeout(function() {
\t\t\t\t\t\t\t\$('#2b').modal('show');
\t\t\t\t\t\t}, 1000);
\t\t\t\t\t\tbreak;
\t\t\t\t\tcase 13:
\t\t\t\t\t\tsetTimeout(function() {
\t\t\t\t\t\t\t\$('#2c').modal('show');
\t\t\t\t\t\t}, 1000);
\t\t\t\t\t\tbreak;
\t\t\t\t\tcase 14:
\t\t\t\t\t\tsetTimeout(function() {
\t\t\t\t\t\t\t\$('#3a').modal('show');
\t\t\t\t\t\t}, 1000);
\t\t\t\t\t\tbreak;
\t\t\t\t\tcase 15:
\t\t\t\t\t\tsetTimeout(function() {
\t\t\t\t\t\t\t\$('#3b').modal('show');
\t\t\t\t\t\t}, 1000);
\t\t\t\t\t\tbreak;
\t\t\t\t\tcase 16:
\t\t\t\t\t\tsetTimeout(function() {
\t\t\t\t\t\t\t\$('#3c').modal('show');
\t\t\t\t\t\t}, 1000);
\t\t\t\t\t\tbreak;
\t\t\t\t\tdefault:
\t\t\t\t\t\talert(\"Pilih salah satu jenis aset di atas!\");
\t\t\t\t}\t
\t\t\t});


\t\t\t/* ==================== validation ==================== */
\t\t\t\$(\"form#frmMain\").validate({
\t\t\t\trules: {
\t\t\t\t\tjpnomorlokasi: {
\t\t\t\t\t\trequired: true
\t\t\t\t\t},
\t\t\t\t\tjptgl: {
\t\t\t\t\t\trequired: true
\t\t\t\t\t}
\t\t\t\t},
\t\t\t\tmessages: {
\t\t\t\t\tjpnomorlokasi: {
\t\t\t\t\t\trequired: \"SKPD harus dipilih!\"
\t\t\t\t\t},
\t\t\t\t\tjptgl: {
\t\t\t\t\t\trequired: \"Tgl.Jurnal harus dipilih!\"
\t\t\t\t\t}
\t\t\t\t},
\t\t\t\terrorLabelContainer: \"#error\",
\t\t\t\tignore: []
\t\t\t});


\t\t\tvar get_rincian1 = function() {
\t\t\t\t\$(\"#rincian1\").mask(\"Loading...\");
\t\t\t\t\$.get(\"";
        // line 1130
        echo twig_escape_filter($this->env, site_url(("pengadaan/get_rincian_1/" . (isset($context["id"]) ? $context["id"] : null))), "html", null, true);
        echo "\", {}, function(response) {
\t\t\t\t\tvar data = response.data;
\t\t\t\t\tvar tbody = \$(\"#rincian1\").find(\"tbody\");
\t\t\t\t\t
\t\t\t\t\ttbody.empty();
\t\t\t\t\tfor (var i = 0; i < data.length; i++) {
\t\t\t\t\t\tvar d = data[i];
\t\t\t\t\t\ttbody.append(
\t\t\t\t\t\t\t[
\t\t\t\t\t\t\t\t\"<tr>\",
\t\t\t\t\t\t\t\t\t\"<td>\",
\t\t\t\t\t\t\t\t\t\t\"<a edit kbid=\" + d.kbid + \" kbkoderekneraca=\" + d.kbkoderekneraca + \" title='Edit' style='padding: 0 10px;'>\",
\t\t\t\t\t\t\t\t\t\t\t\"<span class='glyphicon glyphicon-pencil'></span>\",
\t\t\t\t\t\t\t\t\t\t\"</a>\",
\t\t\t\t\t\t\t\t\t\t\"&nbsp;\",
\t\t\t\t\t\t\t\t\t\t\"<a delete kbid=\" + d.kbid + \" title='Hapus' style='padding: 0 10px;'>\",
\t\t\t\t\t\t\t\t\t\t\t\"<span class='glyphicon glyphicon-trash'></span>\",
\t\t\t\t\t\t\t\t\t\t\"</a>\",
\t\t\t\t\t\t\t\t\t\"</td>\",
\t\t\t\t\t\t\t\t\t\"<td>\" + d.kbstatusguna + \"</td>\",
\t\t\t\t\t\t\t\t\t\"<td>\" + d.kbkodesubsubkel + \"</td>\",
\t\t\t\t\t\t\t\t\t\"<td>\" + d.kbkoderekneraca + \"</td>\",
\t\t\t\t\t\t\t\t\t\"<td>\" + d.kbnamabarang + \"</td>\",
\t\t\t\t\t\t\t\t\t\"<td>\" + d.kbalamat + \"</td>\",
\t\t\t\t\t\t\t\t\t\"<td>\" + d.kbkoderuangan + \"</td>\",
\t\t\t\t\t\t\t\t\t\"<td></td>\",
\t\t\t\t\t\t\t\t\t\"<td>\" + d.kbjumlah + \"</td>\",
\t\t\t\t\t\t\t\t\t\"<td>\" + d.kbhargatotalpajak + \"</td>\",
\t\t\t\t\t\t\t\t\t\"<td>\" + d.kbtipe + \"</td>\",
\t\t\t\t\t\t\t\t\t\"<td>\" + d.kbketerangan + \"</td>\",
\t\t\t\t\t\t\t\t\t\"<td>\" + d.kbnopolisi + \"</td>\",
\t\t\t\t\t\t\t\t\t\"<td></td>\",
\t\t\t\t\t\t\t\t\"</tr>\"
\t\t\t\t\t\t\t].join(\"\")
\t\t\t\t\t\t);
\t\t\t\t\t}

\t\t\t\t\t\$(\"#rincian1\").unmask();

\t\t\t\t\ttbody.find(\"a[edit]\").click(function() {
\t\t\t\t\t\tvar kbid = \$(this).attr(\"kbid\");
\t\t\t\t\t\tvar kbkoderekneraca = \$(this).attr(\"kbkoderekneraca\");
\t\t\t\t\t\tvar kbjenisaset = \$(this).attr(\"kbjenisaset\");

\t\t\t\t\t\tfunction cbLoadDetail(tipe, callback) {
\t\t\t\t\t\t\tvar root = \$(\"#\" + tipe);

\t\t\t\t\t\t\troot.data(\"kbid\", kbid);

\t\t\t\t\t\t\t\$.get(\"";
        // line 1179
        echo twig_escape_filter($this->env, site_url("pengadaan/get_detail_by_id"), "html", null, true);
        echo "\", {kbid:kbid}, function(response) {
\t\t\t\t\t\t\t\tvar data = response.data;

\t\t\t\t\t\t\t\t// clean input
\t\t\t\t\t\t\t\troot.find(\"input, select, textarea\").val(\"\");
\t\t\t\t\t\t\t\troot.find(\"input[type=checkbox]\").prop(\"checked\", false);

\t\t\t\t\t\t\t\troot.find(\".modal-body\").animate({scrollTop:0});


\t\t\t\t\t\t\t\t// set data after populate autocomplete loaded
\t\t\t\t\t\t\t\tfor (var prop in data) {
\t\t\t\t\t\t\t\t\t_.each(root.find(\"input[name=\" + prop + \"], select[name=\" + prop + \"], textarea[name=\" + prop + \"]\"), function(e) {
\t\t\t\t\t\t\t\t\t\tswitch(\$(e).prop('tagName')) {
\t\t\t\t\t\t\t\t\t\t\tcase \"INPUT\":
\t\t\t\t\t\t\t\t\t\t\tcase \"TEXTAREA\":
\t\t\t\t\t\t\t\t\t\t\t\t\$(e).val(data[prop]);
\t\t\t\t\t\t\t\t\t\t\t\tbreak;
\t\t\t\t\t\t\t\t\t\t\tcase \"SELECT\":
\t\t\t\t\t\t\t\t\t\t\t\t\$(e).append(\"<option value='\" + data[prop] + \"'>\" + data[prop] + \"</option>\").val(data[prop]);
\t\t\t\t\t\t\t\t\t\t\t\tbreak;
\t\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t\t}

\t\t\t\t\t\t\t\t

\t\t\t\t\t\t\t\tif (data.kbpajak === \"1\") {
\t\t\t\t\t\t\t\t\troot.find(\"input[type=checkbox][name=kbpajak]\").prop(\"checked\", true);
\t\t\t\t\t\t\t\t}

\t\t\t\t\t\t\t\tcallback(data);
\t\t\t\t\t\t\t}, \"json\");
\t\t\t\t\t\t}

\t\t\t\t\t\tfunction cbPopulateKodeSubSubKel(tipe, callback) {
\t\t\t\t\t\t\tvar root = \$(\"#\" + tipe);
\t\t\t\t\t\t\tvar kbkodesubsubkel = root.find(\"select[name=kbkodesubsubkel]\");

\t\t\t\t\t\t\tkbkodesubsubkel.unbind().select2({
\t\t\t\t\t\t\t\tajax: {
\t\t\t\t\t\t\t\t\turl: \"";
        // line 1220
        echo twig_escape_filter($this->env, site_url("pengadaan/autocomplete_sub_sub_kel"), "html", null, true);
        echo "\",
\t\t\t\t\t\t\t\t\tdataType: 'json',
\t\t\t\t\t\t\t\t\tdelay: 250,
\t\t\t\t\t\t\t\t\tdata: function (params) {
\t\t\t\t\t\t\t\t\t\treturn {
\t\t\t\t\t\t\t\t\t\t\ttipe:tipe,
\t\t\t\t\t\t\t\t\t\t\tq:params.term,
\t\t\t\t\t\t\t\t\t\t\tpage: params.page
\t\t\t\t\t\t\t\t\t\t};
\t\t\t\t\t\t\t\t\t},
\t\t\t\t\t\t\t\t\tprocessResults: function (response, page) {
\t\t\t\t\t\t\t\t\t\treturn {
\t\t\t\t\t\t\t\t\t\t\tresults: response.data
\t\t\t\t\t\t\t\t\t\t};
\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t},
\t\t\t\t\t\t\t\tescapeMarkup: function (markup) { return markup; },
\t\t\t\t\t\t\t\tminimumInputLength: 1,
\t\t\t\t\t\t\t\ttemplateResult: formatSubSubKel, // omitted for brevity, see the source of this page
\t\t\t\t\t\t\t\ttemplateSelection: formatSelection
\t\t\t\t\t\t\t});

\t\t\t\t\t\t\tcallback();
\t\t\t\t\t\t}


\t\t\t\t\t\tif (kbkoderekneraca.startsWith(\"1.3.1\") && kbjenisaset == \"A\") {
\t\t\t\t\t\t\tvar root = \$(\"#1a\");
\t\t\t\t\t\t\tvar content = root.find(\".modal-content\");
\t\t\t\t\t\t\tvar kbkodesubsubkel = root.find(\"select[name=kbkodesubsubkel]\");

\t\t\t\t\t\t\troot.unbind().on('shown.bs.modal', function () { 
\t\t\t\t\t\t\t\tcontent.mask(\"Loading...\");

\t\t\t\t\t\t\t\tvar tipe = \"1a\";

\t\t\t\t\t\t\t\tcbLoadDetail(tipe, function(data) {
\t\t\t\t\t\t\t\t\tcbPopulateKodeSubSubKel(tipe, function() {
\t\t\t\t\t\t\t\t\t\tcontent.unmask();
\t\t\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t\t})
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t}).modal('show');
\t\t\t\t\t\t} else if (kbkoderekneraca.startsWith(\"1.3.2\") && kbjenisaset == \"A\") {
\t\t\t\t\t\t\tvar tmp = kbkoderekneraca.split(\".\");
\t\t\t\t\t\t\tif (tmp.length >= 3) {
\t\t\t\t\t\t\t\tvar c = tmp[3]|0;
\t\t\t\t\t\t\t\tif (c >= 1 && c <= 6) {
\t\t\t\t\t\t\t\t\tvar root = \$(\"#1c\");
\t\t\t\t\t\t\t\t\tvar content = root.find(\".modal-content\");
\t\t\t\t\t\t\t\t\tvar kbkodesubsubkel = root.find(\"select[name=kbkodesubsubkel]\");

\t\t\t\t\t\t\t\t\troot.unbind().on('shown.bs.modal', function () { 
\t\t\t\t\t\t\t\t\t\tcontent.mask(\"Loading...\");

\t\t\t\t\t\t\t\t\t\tvar tipe = \"1c\";

\t\t\t\t\t\t\t\t\t\tcbLoadDetail(tipe, function(data) {
\t\t\t\t\t\t\t\t\t\t\tcbPopulateKodeSubSubKel(tipe, function() {
\t\t\t\t\t\t\t\t\t\t\t\tcontent.unmask();
\t\t\t\t\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t\t\t}).modal('show');
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\tvar root = \$(\"#1b\");
\t\t\t\t\t\t\t\t\tvar content = root.find(\".modal-content\");
\t\t\t\t\t\t\t\t\tvar kbkodesubsubkel = root.find(\"select[name=kbkodesubsubkel]\");

\t\t\t\t\t\t\t\t\troot.unbind().on('shown.bs.modal', function () { 
\t\t\t\t\t\t\t\t\t\tcontent.mask(\"Loading...\");

\t\t\t\t\t\t\t\t\t\tvar tipe = \"1b\";

\t\t\t\t\t\t\t\t\t\tcbLoadDetail(tipe, function(data) {
\t\t\t\t\t\t\t\t\t\t\tcbPopulateKodeSubSubKel(tipe, function() {
\t\t\t\t\t\t\t\t\t\t\t\tcontent.unmask();
\t\t\t\t\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t\t\t}).modal('show');
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t}
\t\t\t\t\t\t} else if (kbkoderekneraca.startsWith(\"1.3.3\") && kbjenisaset == \"A\") {
\t\t\t\t\t\t\tvar root = \$(\"#1d\");
\t\t\t\t\t\t\tvar content = root.find(\".modal-content\");
\t\t\t\t\t\t\tvar kbkodesubsubkel = root.find(\"select[name=kbkodesubsubkel]\");

\t\t\t\t\t\t\troot.unbind().on('shown.bs.modal', function () { 
\t\t\t\t\t\t\t\tcontent.mask(\"Loading...\");

\t\t\t\t\t\t\t\tvar tipe = \"1d\";

\t\t\t\t\t\t\t\tcbLoadDetail(tipe, function(data) {
\t\t\t\t\t\t\t\t\tcbPopulateKodeSubSubKel(tipe, function() {
\t\t\t\t\t\t\t\t\t\tcontent.unmask();
\t\t\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t}).modal('show');
\t\t\t\t\t\t} else if (kbkoderekneraca.startsWith(\"1.3.4\") && kbjenisaset == \"A\") {
\t\t\t\t\t\t\tvar root = \$(\"#1e\");
\t\t\t\t\t\t\tvar content = root.find(\".modal-content\");
\t\t\t\t\t\t\tvar kbkodesubsubkel = root.find(\"select[name=kbkodesubsubkel]\");

\t\t\t\t\t\t\troot.unbind().on('shown.bs.modal', function () { 
\t\t\t\t\t\t\t\tcontent.mask(\"Loading...\");

\t\t\t\t\t\t\t\tvar tipe = \"1e\";

\t\t\t\t\t\t\t\tcbLoadDetail(tipe, function(data) {
\t\t\t\t\t\t\t\t\tcbPopulateKodeSubSubKel(tipe, function() {
\t\t\t\t\t\t\t\t\t\tcontent.unmask();
\t\t\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t}).modal('show');
\t\t\t\t\t\t} else if (kbkoderekneraca.startsWith(\"1.3.5.01\") && kbjenisaset == \"A\") {
\t\t\t\t\t\t\tvar root = \$(\"#1f\");
\t\t\t\t\t\t\tvar content = root.find(\".modal-content\");
\t\t\t\t\t\t\tvar kbkodesubsubkel = root.find(\"select[name=kbkodesubsubkel]\");

\t\t\t\t\t\t\troot.unbind().on('shown.bs.modal', function () { 
\t\t\t\t\t\t\t\tcontent.mask(\"Loading...\");

\t\t\t\t\t\t\t\tvar tipe = \"1f\";
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\tcbLoadDetail(tipe, function(data) {
\t\t\t\t\t\t\t\t\tcbPopulateKodeSubSubKel(tipe, function() {
\t\t\t\t\t\t\t\t\t\tcontent.unmask();
\t\t\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t}).modal('show');
\t\t\t\t\t\t} else if (kbkoderekneraca.startsWith(\"1.3.5.02\") && kbjenisaset == \"A\") {
\t\t\t\t\t\t\tvar root = \$(\"#1g\");
\t\t\t\t\t\t\tvar content = root.find(\".modal-content\");
\t\t\t\t\t\t\tvar kbkodesubsubkel = root.find(\"select[name=kbkodesubsubkel]\");

\t\t\t\t\t\t\troot.unbind().on('shown.bs.modal', function () { 
\t\t\t\t\t\t\t\tcontent.mask(\"Loading...\");

\t\t\t\t\t\t\t\tvar tipe = \"1g\";

\t\t\t\t\t\t\t\tcbLoadDetail(tipe, function(data) {
\t\t\t\t\t\t\t\t\tcbPopulateKodeSubSubKel(tipe, function() {
\t\t\t\t\t\t\t\t\t\tcontent.unmask();
\t\t\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t}).modal('show');
\t\t\t\t\t\t} else if (kbkoderekneraca.startsWith(\"1.3.5.03\") && kbjenisaset == \"A\") {
\t\t\t\t\t\t\tvar root = \$(\"#1h\");
\t\t\t\t\t\t\tvar content = root.find(\".modal-content\");
\t\t\t\t\t\t\tvar kbkodesubsubkel = root.find(\"select[name=kbkodesubsubkel]\");

\t\t\t\t\t\t\troot.unbind().on('shown.bs.modal', function () { 
\t\t\t\t\t\t\t\tcontent.mask(\"Loading...\");

\t\t\t\t\t\t\t\tvar tipe = \"1h\";

\t\t\t\t\t\t\t\tcbLoadDetail(tipe, function(data) {
\t\t\t\t\t\t\t\t\tcbPopulateKodeSubSubKel(tipe, function() {
\t\t\t\t\t\t\t\t\t\tcontent.unmask();
\t\t\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t}).modal('show');
\t\t\t\t\t\t} else if (kbkoderekneraca.startsWith(\"1.5.4\") && kbjenisaset == \"A\") {
\t\t\t\t\t\t\tvar root = \$(\"#1i\");
\t\t\t\t\t\t\tvar content = root.find(\".modal-content\");
\t\t\t\t\t\t\tvar kbkodesubsubkel = root.find(\"select[name=kbkodesubsubkel]\");

\t\t\t\t\t\t\troot.unbind().on('shown.bs.modal', function () { 
\t\t\t\t\t\t\t\tcontent.mask(\"Loading...\");

\t\t\t\t\t\t\t\tvar tipe = \"1i\";

\t\t\t\t\t\t\t\tcbLoadDetail(tipe, function(data) {
\t\t\t\t\t\t\t\t\tcbPopulateKodeSubSubKel(tipe, function() {
\t\t\t\t\t\t\t\t\t\tcontent.unmask();
\t\t\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t}).modal('show');
\t\t\t\t\t\t} else if (kbkoderekneraca.startsWith(\"1.3\") && kbjenisaset == \"R\") {
\t\t\t\t\t\t\tvar root = \$(\"#1j\");
\t\t\t\t\t\t\tvar content = root.find(\".modal-content\");
\t\t\t\t\t\t\tvar kbkodesubsubkel = root.find(\"select[name=kbkodesubsubkel]\");

\t\t\t\t\t\t\troot.unbind().on('shown.bs.modal', function () { 
\t\t\t\t\t\t\t\tcontent.mask(\"Loading...\");

\t\t\t\t\t\t\t\tvar tipe = \"1j\";

\t\t\t\t\t\t\t\tcbLoadDetail(tipe, function(data) {
\t\t\t\t\t\t\t\t\tcbPopulateKodeSubSubKel(tipe, function() {
\t\t\t\t\t\t\t\t\t\tcontent.unmask();
\t\t\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t}).modal('show');
\t\t\t\t\t\t} else if ( kbjenisaset == \"J\") {
\t\t\t\t\t\t\tvar root = \$(\"#2a\");
\t\t\t\t\t\t\tvar content = root.find(\".modal-content\");

\t\t\t\t\t\t\troot.unbind().on('shown.bs.modal', function () { 
\t\t\t\t\t\t\t\tcontent.mask(\"Loading...\");

\t\t\t\t\t\t\t\tvar tipe = \"2a\";

\t\t\t\t\t\t\t}).modal('show');
\t\t\t\t\t\t} else if ( kbjenisaset == \"H\") {
\t\t\t\t\t\t\tvar root = \$(\"#2b\");
\t\t\t\t\t\t\tvar content = root.find(\".modal-content\");

\t\t\t\t\t\t\troot.unbind().on('shown.bs.modal', function () { 
\t\t\t\t\t\t\t\tcontent.mask(\"Loading...\");

\t\t\t\t\t\t\t\tvar tipe = \"2b\";

\t\t\t\t\t\t\t}).modal('show');
\t\t\t\t\t\t} else if ( kbjenisaset == \"B\") {
\t\t\t\t\t\t\tvar root = \$(\"#2c\");
\t\t\t\t\t\t\tvar content = root.find(\".modal-content\");

\t\t\t\t\t\t\troot.unbind().on('shown.bs.modal', function () { 
\t\t\t\t\t\t\t\tcontent.mask(\"Loading...\");

\t\t\t\t\t\t\t\tvar tipe = \"2c\";

\t\t\t\t\t\t\t}).modal('show');
\t\t\t\t\t\t}
\t\t\t\t\t});

\t\t\t\t\ttbody.find(\"a[delete]\").click(function(e) {
\t\t\t\t\t\tif (!confirm(\"Apakah Anda yakin?\")) {
\t\t\t\t\t\t\te.preventDefault();
\t\t\t\t\t\t\treturn;
\t\t\t\t\t\t}

\t\t\t\t\t\tvar kbid = \$(this).attr(\"kbid\");

\t\t\t\t\t\t\$.post(\"";
        // line 1454
        echo twig_escape_filter($this->env, site_url("pengadaan/delete_by_kbid"), "html", null, true);
        echo "\", {kbid:kbid}, function(response){ 
\t\t\t\t\t\t\tvar success = response.success > 0 ? true : false;
\t\t\t\t\t\t\tif (success) {
\t\t\t\t\t\t\t\tget_rincian1();
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\talert(\"Data gagal dihapus\");
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}, \"json\");
\t\t\t\t\t});
\t\t\t\t}, \"json\");
\t\t\t}

\t\t\tvar get_rincian2 = function() {
\t\t\t\t\$(\"#rincian2\").mask(\"Loading...\");
\t\t\t\t\$.get(\"";
        // line 1468
        echo twig_escape_filter($this->env, site_url(("pengadaan/get_rincian_2/" . (isset($context["id"]) ? $context["id"] : null))), "html", null, true);
        echo "\", {}, function(response) {
\t\t\t\t\tvar data = response.data;
\t\t\t\t\tvar tbody = \$(\"#rincian2\").find(\"tbody\");
\t\t\t\t\t
\t\t\t\t\ttbody.empty();
\t\t\t\t\tfor (var i = 0; i < data.length; i++) {
\t\t\t\t\t\tvar d = data[i];
\t\t\t\t\t\ttbody.append(
\t\t\t\t\t\t\t[
\t\t\t\t\t\t\t\t\"<tr>\",
\t\t\t\t\t\t\t\t\t\"<td>\",
\t\t\t\t\t\t\t\t\t\t\"<a edit kbid=\" + d.kbid + \" kbkoderekneraca=\" + d.kbkoderekneraca + \" title='Edit' style='padding: 0 10px;'>\",
\t\t\t\t\t\t\t\t\t\t\t\"<span class='glyphicon glyphicon-pencil'></span>\",
\t\t\t\t\t\t\t\t\t\t\"</a>\",
\t\t\t\t\t\t\t\t\t\t\"&nbsp;\",
\t\t\t\t\t\t\t\t\t\t\"<a delete kbid=\" + d.kbid + \" title='Hapus' style='padding: 0 10px;'>\",
\t\t\t\t\t\t\t\t\t\t\t\"<span class='glyphicon glyphicon-trash'></span>\",
\t\t\t\t\t\t\t\t\t\t\"</a>\",
\t\t\t\t\t\t\t\t\t\"</td>\",
\t\t\t\t\t\t\t\t\t\"<td>\" + d.kbstatusguna + \"</td>\",
\t\t\t\t\t\t\t\t\t\"<td>\" + d.kbkodesubsubkel + \"</td>\",
\t\t\t\t\t\t\t\t\t\"<td>\" + d.kbkoderekneraca + \"</td>\",
\t\t\t\t\t\t\t\t\t\"<td>\" + d.kbnamabarang + \"</td>\",
\t\t\t\t\t\t\t\t\t\"<td>\" + d.kbalamat + \"</td>\",
\t\t\t\t\t\t\t\t\t\"<td>\" + d.kbkoderuangan + \"</td>\",
\t\t\t\t\t\t\t\t\t\"<td></td>\",
\t\t\t\t\t\t\t\t\t\"<td>\" + d.kbjumlah + \"</td>\",
\t\t\t\t\t\t\t\t\t\"<td>\" + d.kbhargatotalpajak + \"</td>\",
\t\t\t\t\t\t\t\t\t\"<td>\" + d.kbtipe + \"</td>\",
\t\t\t\t\t\t\t\t\t\"<td>\" + d.kbketerangan + \"</td>\",
\t\t\t\t\t\t\t\t\t\"<td>\" + d.kbnopolisi + \"</td>\",
\t\t\t\t\t\t\t\t\t\"<td></td>\",
\t\t\t\t\t\t\t\t\"</tr>\"
\t\t\t\t\t\t\t].join(\"\")
\t\t\t\t\t\t);
\t\t\t\t\t}

\t\t\t\t\t\$(\"#rincian2\").unmask();
\t\t\t\t}, \"json\");
\t\t\t}

\t\t\tvar get_rincian3 = function() {
\t\t\t\t\$(\"#rincian3\").mask(\"Loading...\");
\t\t\t\t\$.get(\"";
        // line 1511
        echo twig_escape_filter($this->env, site_url(("pengadaan/get_rincian_3/" . (isset($context["id"]) ? $context["id"] : null))), "html", null, true);
        echo "\", {}, function(response) {
\t\t\t\t\tvar data = response.data;
\t\t\t\t\tvar tbody = \$(\"#rincian3\").find(\"tbody\");
\t\t\t\t\t
\t\t\t\t\ttbody.empty();
\t\t\t\t\tfor (var i = 0; i < data.length; i++) {
\t\t\t\t\t\tvar d = data[i];
\t\t\t\t\t\ttbody.append(
\t\t\t\t\t\t\t[
\t\t\t\t\t\t\t\t\"<tr>\",
\t\t\t\t\t\t\t\t\t\"<td>\",
\t\t\t\t\t\t\t\t\t\t\"<a edit kbid=\" + d.kbid + \" kbkoderekneraca=\" + d.kbkoderekneraca + \" title='Edit' style='padding: 0 10px;'>\",
\t\t\t\t\t\t\t\t\t\t\t\"<span class='glyphicon glyphicon-pencil'></span>\",
\t\t\t\t\t\t\t\t\t\t\"</a>\",
\t\t\t\t\t\t\t\t\t\t\"&nbsp;\",
\t\t\t\t\t\t\t\t\t\t\"<a delete kbid=\" + d.kbid + \" title='Hapus' style='padding: 0 10px;'>\",
\t\t\t\t\t\t\t\t\t\t\t\"<span class='glyphicon glyphicon-trash'></span>\",
\t\t\t\t\t\t\t\t\t\t\"</a>\",
\t\t\t\t\t\t\t\t\t\"</td>\",
\t\t\t\t\t\t\t\t\t\"<td>\" + d.kbstatusguna + \"</td>\",
\t\t\t\t\t\t\t\t\t\"<td>\" + d.kbkodesubsubkel + \"</td>\",
\t\t\t\t\t\t\t\t\t\"<td>\" + d.kbkoderekneraca + \"</td>\",
\t\t\t\t\t\t\t\t\t\"<td>\" + d.kbnamabarang + \"</td>\",
\t\t\t\t\t\t\t\t\t\"<td>\" + d.kbalamat + \"</td>\",
\t\t\t\t\t\t\t\t\t\"<td>\" + d.kbhargatotalpajak + \"</td>\",
\t\t\t\t\t\t\t\t\t\"<td>\" + d.kbnilaitotalkontrak + \"</td>\",
\t\t\t\t\t\t\t\t\t\"<td>\" + d.kbtipe + \"</td>\",
\t\t\t\t\t\t\t\t\t\"<td>\" + d.kbjumlah + \"</td>\",
\t\t\t\t\t\t\t\t\t\"<td>\" + d.kbketerangan + \"</td>\",
\t\t\t\t\t\t\t\t\t\"<td>\" + d.kbnopolisi + \"</td>\",
\t\t\t\t\t\t\t\t\t\"<td>\" + d.kbtahun + \"</td>\",
\t\t\t\t\t\t\t\t\t\"<td></td>\",
\t\t\t\t\t\t\t\t\"</tr>\"
\t\t\t\t\t\t\t].join(\"\")
\t\t\t\t\t\t);
\t\t\t\t\t}

\t\t\t\t\t\$(\"#rincian3\").unmask();
\t\t\t\t}, \"json\");
\t\t\t}

\t\t\tvar get_rincian4 = function() {
\t\t\t\t\$(\"#rincian4\").mask(\"Loading...\");
\t\t\t\t\$.get(\"";
        // line 1554
        echo twig_escape_filter($this->env, site_url(("pengadaan/get_rincian_4/" . (isset($context["id"]) ? $context["id"] : null))), "html", null, true);
        echo "\", {}, function(response) {
\t\t\t\t\tvar data = response.data;
\t\t\t\t\tvar tbody = \$(\"#rincian4\").find(\"tbody\");
\t\t\t\t\t
\t\t\t\t\ttbody.empty();
\t\t\t\t\tfor (var i = 0; i < data.length; i++) {
\t\t\t\t\t\tvar d = data[i];
\t\t\t\t\t\ttbody.append(
\t\t\t\t\t\t\t[
\t\t\t\t\t\t\t\t\"<tr>\",
\t\t\t\t\t\t\t\t\t\"<td>\",
\t\t\t\t\t\t\t\t\t\t\"<a edit kbid=\" + d.kbid + \" kbnamabarang=\" + d.kbnamabarang + \" title='Edit' style='padding: 0 10px;'>\",
\t\t\t\t\t\t\t\t\t\t\t\"<span class='glyphicon glyphicon-pencil'></span>\",
\t\t\t\t\t\t\t\t\t\t\"</a>\",
\t\t\t\t\t\t\t\t\t\t\"&nbsp;\",
\t\t\t\t\t\t\t\t\t\t\"<a delete kbid=\" + d.kbid + \" title='Hapus' style='padding: 0 10px;'>\",
\t\t\t\t\t\t\t\t\t\t\t\"<span class='glyphicon glyphicon-trash'></span>\",
\t\t\t\t\t\t\t\t\t\t\"</a>\",
\t\t\t\t\t\t\t\t\t\"</td>\",
\t\t\t\t\t\t\t\t\t\"<td>\" + d.kbnamabarang + \"</td>\",
\t\t\t\t\t\t\t\t\t\"<td>\" + d.kbalamat + \"</td>\",
\t\t\t\t\t\t\t\t\t\"<td>\" + d.kbtipe + \"</td>\",
\t\t\t\t\t\t\t\t\t\"<td>\" + d.kbjumlah + \"</td>\",
\t\t\t\t\t\t\t\t\t\"<td>\" + d.kbhargatotalpajak + \"</td>\",
\t\t\t\t\t\t\t\t\t\"<td>\" + d.kbketerangan + \"</td>\",
\t\t\t\t\t\t\t\t\t\"<td></td>\",
\t\t\t\t\t\t\t\t\"</tr>\"
\t\t\t\t\t\t\t].join(\"\")
\t\t\t\t\t\t);
\t\t\t\t\t}

\t\t\t\t\t\$(\"#rincian4\").unmask();
\t\t\t\t}, \"json\");
\t\t\t}

\t\t\tvar get_rincian5 = function() {
\t\t\t\t\$(\"#rincian5\").mask(\"Loading...\");
\t\t\t\t\$.get(\"";
        // line 1591
        echo twig_escape_filter($this->env, site_url(("pengadaan/get_rincian_5/" . (isset($context["id"]) ? $context["id"] : null))), "html", null, true);
        echo "\", {}, function(response) {
\t\t\t\t\tvar data = response.data;
\t\t\t\t\tvar tbody = \$(\"#rincian5\").find(\"tbody\");
\t\t\t\t\t
\t\t\t\t\ttbody.empty();
\t\t\t\t\tfor (var i = 0; i < data.length; i++) {
\t\t\t\t\t\tvar d = data[i];
\t\t\t\t\t\ttbody.append(
\t\t\t\t\t\t\t[
\t\t\t\t\t\t\t\t\"<tr>\",
\t\t\t\t\t\t\t\t\t\"<td>\",
\t\t\t\t\t\t\t\t\t\t\"<a edit kbid=\" + d.kbid + \" kbkoderekneraca=\" + d.kbkoderekneraca + \" title='Edit' style='padding: 0 10px;'>\",
\t\t\t\t\t\t\t\t\t\t\t\"<span class='glyphicon glyphicon-pencil'></span>\",
\t\t\t\t\t\t\t\t\t\t\"</a>\",
\t\t\t\t\t\t\t\t\t\t\"&nbsp;\",
\t\t\t\t\t\t\t\t\t\t\"<a delete kbid=\" + d.kbid + \" title='Hapus' style='padding: 0 10px;'>\",
\t\t\t\t\t\t\t\t\t\t\t\"<span class='glyphicon glyphicon-trash'></span>\",
\t\t\t\t\t\t\t\t\t\t\"</a>\",
\t\t\t\t\t\t\t\t\t\"</td>\",
\t\t\t\t\t\t\t\t\t\"<td>\" + d.kbnamabarang + \"</td>\",
\t\t\t\t\t\t\t\t\t\"<td>\" + d.kbalamat + \"</td>\",
\t\t\t\t\t\t\t\t\t\"<td>\" + d.kbtipe + \"</td>\",
\t\t\t\t\t\t\t\t\t\"<td>\" + d.kbjumlah + \"</td>\",
\t\t\t\t\t\t\t\t\t\"<td>\" + d.kbhargatotalpajak + \"</td>\",
\t\t\t\t\t\t\t\t\t\"<td>\" + d.kbketerangan + \"</td>\",
\t\t\t\t\t\t\t\t\t\"<td></td>\",
\t\t\t\t\t\t\t\t\"</tr>\"
\t\t\t\t\t\t\t].join(\"\")
\t\t\t\t\t\t);
\t\t\t\t\t}

\t\t\t\t\t\$(\"#rincian5\").unmask();
\t\t\t\t}, \"json\");
\t\t\t}

\t\t\tvar get_rincian6 = function() {
\t\t\t\t\$(\"#rincian6\").mask(\"Loading...\");
\t\t\t\t\$.get(\"";
        // line 1628
        echo twig_escape_filter($this->env, site_url(("pengadaan/get_rincian_6/" . (isset($context["id"]) ? $context["id"] : null))), "html", null, true);
        echo "\", {}, function(response) {
\t\t\t\t\tvar data = response.data;
\t\t\t\t\tvar tbody = \$(\"#rincian6\").find(\"tbody\");
\t\t\t\t\t
\t\t\t\t\ttbody.empty();
\t\t\t\t\tfor (var i = 0; i < data.length; i++) {
\t\t\t\t\t\tvar d = data[i];
\t\t\t\t\t\ttbody.append(
\t\t\t\t\t\t\t[
\t\t\t\t\t\t\t\t\"<tr>\",
\t\t\t\t\t\t\t\t\t\"<td>\",
\t\t\t\t\t\t\t\t\t\t\"<a edit kbid=\" + d.kbid + \" kbkoderekneraca=\" + d.kbkoderekneraca + \" title='Edit' style='padding: 0 10px;'>\",
\t\t\t\t\t\t\t\t\t\t\t\"<span class='glyphicon glyphicon-pencil'></span>\",
\t\t\t\t\t\t\t\t\t\t\"</a>\",
\t\t\t\t\t\t\t\t\t\t\"&nbsp;\",
\t\t\t\t\t\t\t\t\t\t\"<a delete kbid=\" + d.kbid + \" title='Hapus' style='padding: 0 10px;'>\",
\t\t\t\t\t\t\t\t\t\t\t\"<span class='glyphicon glyphicon-trash'></span>\",
\t\t\t\t\t\t\t\t\t\t\"</a>\",
\t\t\t\t\t\t\t\t\t\"</td>\",
\t\t\t\t\t\t\t\t\t\"<td>\" + d.kbnamabarang + \"</td>\",
\t\t\t\t\t\t\t\t\t\"<td>\" + d.kbalamat + \"</td>\",
\t\t\t\t\t\t\t\t\t\"<td>\" + d.kbtipe + \"</td>\",
\t\t\t\t\t\t\t\t\t\"<td>\" + d.kbjumlah + \"</td>\",
\t\t\t\t\t\t\t\t\t\"<td>\" + d.kbhargatotalpajak + \"</td>\",
\t\t\t\t\t\t\t\t\t\"<td>\" + d.kbketerangan + \"</td>\",
\t\t\t\t\t\t\t\t\t\"<td></td>\",
\t\t\t\t\t\t\t\t\"</tr>\"
\t\t\t\t\t\t\t].join(\"\")
\t\t\t\t\t\t);
\t\t\t\t\t}

\t\t\t\t\t\$(\"#rincian6\").unmask();
\t\t\t\t}, \"json\");
\t\t\t}

\t\t\tvar get_pembayaran = function() {
\t\t\t\t\$(\"#pembayaran\").mask(\"Loading...\");

\t\t\t\t\$.get(\"";
        // line 1666
        echo twig_escape_filter($this->env, site_url(("pengadaan/get_pembayaran/" . (isset($context["id"]) ? $context["id"] : null))), "html", null, true);
        echo "\", {}, function(response) {
\t\t\t\t\tpembayaran = response.data;
\t\t\t\t\t
\t\t\t\t\tvar tbody = \$(\"#pembayaran\").find(\"tbody\");
\t\t\t\t\ttbody.empty();

\t\t\t\t\tfor (var i = 0; i < pembayaran.length; i++) {
\t\t\t\t\t\tvar d = pembayaran[i];
\t\t\t\t\t\ttbody.append(
\t\t\t\t\t\t\t[
\t\t\t\t\t\t\t\td.pbnospp == \"\" ? \"<tr pbnospp='\" + guid() + \"' generate>\" : \"<tr pbnospp='\" + Base64.encode(d.pbnospp) + \"'>\",
\t\t\t\t\t\t\t\t\t\"<td pbnosp2d pbtglsp2d='\" + Base64.encode(d.pbtglsp2d) + \"'>\" + d.pbnosp2d + \"</td>\",
\t\t\t\t\t\t\t\t\t\"<td pbnospm>\" + d.pbnospm + \"</td>\",
\t\t\t\t\t\t\t\t\t\"<td pbnilaispm style='text-align: right; '>\" + d.pbnilaispm + \"</td>\",
\t\t\t\t\t\t\t\t\t\"<td pbkoderek13>\" + d.pbkoderek13 + \"</td>\",
\t\t\t\t\t\t\t\t\t\"<td pburaianspm>\" + d.pburaianspm + \"</td>\",
\t\t\t\t\t\t\t\t\t\"<td pbtermin>\" + d.pbtermin + \"</td>\",
\t\t\t\t\t\t\t\t\t\"<td pbthnspm>\" + d.pbthnspm + \"</td>\",
\t\t\t\t\t\t\t\t\t\"<td pbketerangan>\" + d.pbketerangan + \"</td>\",
\t\t\t\t\t\t\t\t\"</tr>\"
\t\t\t\t\t\t\t].join(\"\")
\t\t\t\t\t\t);
\t\t\t\t\t}

\t\t\t\t\t\$(\"#pembayaran\").unmask();
\t\t\t\t}, \"json\");
\t\t\t}



\t\t\t\$(\"#sync\").click(function() {
\t\t\t\t\$(window).scrollTop(0);
\t\t\t\t\$(\"body\").mask(\"Checking...\");

\t\t\t\tvar tbody = \$(\"#pembayaran\").find(\"tbody\");
\t\t\t\ttbody.empty();

\t\t\t\tvar no_spk_sp_dokumen = \$(\"select[name='no_spk_sp_dokumen'] option:selected\").attr(\"no_spk_sp_dokumen\");
\t\t\t\tvar no_spm_input = \$(\"input[name=no_spm_input]\").val();

\t\t\t\tvar check_sp2d_by_spp = function(data_spp) {
\t\t\t\t\tfor (var i = 0; i < data_spp.length; i++) {
\t\t\t\t\t\tvar isExist = false;
\t\t\t\t\t\tfor (var j = 0; j < pembayaran.length; j++) {
\t\t\t\t\t\t\tif (pembayaran[j].pbnospp == data_spp[i].No_SPP) {
\t\t\t\t\t\t\t\tisExist = true;
\t\t\t\t\t\t\t\t//break;
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}

\t\t\t\t\t\t// get satu-satu disini
\t\t\t\t\t\tvar dspp = data_spp[i];
\t\t\t\t\t\tvar no_spp = dspp.No_SPP;

\t\t\t\t\t\tif (!isExist) {
\t\t\t\t\t\t\ttbody.append(
\t\t\t\t\t\t\t\t[
\t\t\t\t\t\t\t\t\t\"<tr pbnospp='\" + Base64.encode(no_spp) + \"'>\",
\t\t\t\t\t\t\t\t\t\t\"<td pbnosp2d></td>\",
\t\t\t\t\t\t\t\t\t\t\"<td pbnospm></td>\",
\t\t\t\t\t\t\t\t\t\t\"<td pbnilaispm style='text-align: right; '></td>\",
\t\t\t\t\t\t\t\t\t\t\"<td pbkoderek13></td>\",
\t\t\t\t\t\t\t\t\t\t\"<td pburaianspm></td>\",
\t\t\t\t\t\t\t\t\t\t\"<td pbtermin></td>\",
\t\t\t\t\t\t\t\t\t\t\"<td pbthnspm></td>\",
\t\t\t\t\t\t\t\t\t\t\"<td pbketerangan></td>\",
\t\t\t\t\t\t\t\t\t\"</tr>\"
\t\t\t\t\t\t\t\t].join(\"\")
\t\t\t\t\t\t\t);
\t\t\t\t\t\t}


\t\t\t\t\t\t\$.get(\"";
        // line 1738
        echo twig_escape_filter($this->env, site_url("pengadaan/pembayaran_spm"), "html", null, true);
        echo "\", {no_spp:no_spp }, function(response_spm){
\t\t\t\t\t\t\tvar data_spm = response_spm.data;
\t\t\t\t\t\t\tvar no_spp = response_spm.no_spp;
\t\t\t\t\t\t\tvar no_spm = [];
\t\t\t\t\t\t\tvar uraian_spm = [];

\t\t\t\t\t\t\tfor (var i = 0; i < data_spm.length; i++) {
\t\t\t\t\t\t\t\tvar dspm = data_spm[i];
\t\t\t\t\t\t\t\tno_spm.push(dspm.No_SPM);
\t\t\t\t\t\t\t\turaian_spm.push(dspm.Uraian);
\t\t\t\t\t\t\t}

\t\t\t\t\t\t\t\$(\"tr[pbnospp=\" + Base64.encode(no_spp) + \"]\").find(\"td[pbnospm]\").html(no_spm.join(\", \"));
\t\t\t\t\t\t\t\$(\"tr[pbnospp=\" + Base64.encode(no_spp) + \"]\").find(\"td[pburaianspm]\").html(uraian_spm.join(\", \"));

\t\t\t\t\t\t\tfor (var i = 0; i < data_spm.length; i++) {
\t\t\t\t\t\t\t\tvar dspm = data_spm[i];
\t\t\t\t\t\t\t\tvar no_spm = dspm.No_SPM;


\t\t\t\t\t\t\t\t//get nilai SPM + Kode Rekening
\t\t\t\t\t\t\t\t\$.get(\"";
        // line 1759
        echo twig_escape_filter($this->env, site_url("pengadaan/pembayaran_spm_rinc"), "html", null, true);
        echo "\", {no_spm:no_spm }, function(response_spm_rinc){
\t\t\t\t\t\t\t\t\tvar data_spm_rinc = response_spm_rinc.data;
\t\t\t\t\t\t\t\t\tvar nilai_spm = [];
\t\t\t\t\t\t\t\t\tvar kode_rek = [];
\t\t\t\t\t\t\t\t\tvar kd_rek_1 = '';
\t\t\t\t\t\t\t\t\tvar kd_rek_2 = '';
\t\t\t\t\t\t\t\t\tvar kd_rek_3 = '';
\t\t\t\t\t\t\t\t\tvar kd_rek_4 = '';
\t\t\t\t\t\t\t\t\tvar kd_rek_5 = '';
\t\t\t\t\t\t\t\t\tvar uraian_belanja = '';

\t\t\t\t\t\t\t\t\tfor (var i = 0; i < data_spm_rinc.length; i++) {
\t\t\t\t\t\t\t\t\t\tvar dspm_rinc = data_spm_rinc[i];
\t\t\t\t\t\t\t\t\t\tnilai_spm.push(dspm_rinc.Nilai);
\t\t\t\t\t\t\t\t\t\tkd_rek_1 = dspm_rinc.Kd_Rek_1;
\t\t\t\t\t\t\t\t\t\tkd_rek_2 = dspm_rinc.Kd_Rek_2;
\t\t\t\t\t\t\t\t\t\tkd_rek_3 = dspm_rinc.Kd_Rek_3;
\t\t\t\t\t\t\t\t\t\tkd_rek_4 = dspm_rinc.Kd_Rek_4;
\t\t\t\t\t\t\t\t\t\tkd_rek_5 = dspm_rinc.Kd_Rek_5;

\t\t\t\t\t\t\t\t\t\tkode_rek = [kd_rek_1, kd_rek_2, kd_rek_3, kd_rek_4, kd_rek_5].join('');

\t\t\t\t\t\t\t\t\t\t\$.get(\"";
        // line 1781
        echo twig_escape_filter($this->env, site_url("pengadaan/get_uraian_belanja"), "html", null, true);
        echo "\", {kd_rek_1:kd_rek_1, kd_rek_2:kd_rek_2, kd_rek_3:kd_rek_3, kd_rek_4:kd_rek_4, kd_rek_5:kd_rek_5}, function(response_uraian_belanja){
\t\t\t\t\t\t\t\t\t\t\t\turaian_belanja = response_uraian_belanja.data;
\t\t\t\t\t\t\t\t\t\t}, \"json\");
\t\t\t\t\t\t\t\t\t}

\t\t\t\t\t\t\t\t\t\$(\"tr[pbnospp=\" + Base64.encode(no_spp) + \"]\").find(\"td[pbnilaispm]\").html(nilai_spm.join(\", \"));
\t\t\t\t\t\t\t\t\t\$(\"tr[pbnospp=\" + Base64.encode(no_spp) + \"]\").find(\"td[pbkoderek13]\").html(kode_rek);
\t\t\t\t\t\t\t\t\t\$(\"tr[pbnospp=\" + Base64.encode(no_spp) + \"]\").find(\"td[pburaianspm]\").html(uraian_belanja);
\t\t\t\t\t\t\t\t}, \"json\");


\t\t\t\t\t\t\t\t// get sp2d
\t\t\t\t\t\t\t\t\$.get(\"";
        // line 1793
        echo twig_escape_filter($this->env, site_url("pengadaan/pembayaran_sp2d"), "html", null, true);
        echo "\", {no_spm:no_spm }, function(response_sp2d){
\t\t\t\t\t\t\t\t\tvar data_sp2d = response_sp2d.data;
\t\t\t\t\t\t\t\t\tvar no_sp2d = [];
\t\t\t\t\t\t\t\t\tvar ket_sp2d = [];

\t\t\t\t\t\t\t\t\tfor (var i = 0; i < data_sp2d.length; i++) {
\t\t\t\t\t\t\t\t\t\tvar dsp2d = data_sp2d[i];
\t\t\t\t\t\t\t\t\t\tno_sp2d.push(dsp2d.No_SP2D);
\t\t\t\t\t\t\t\t\t\tket_sp2d.push(dsp2d.Keterangan);
\t\t\t\t\t\t\t\t\t}

\t\t\t\t\t\t\t\t\t\$(\"tr[pbnospp=\" + Base64.encode(no_spp) + \"]\").find(\"td[pbnosp2d]\").html(no_sp2d.join(\", \"));
\t\t\t\t\t\t\t\t\t\$(\"tr[pbnospp=\" + Base64.encode(no_spp) + \"]\").find(\"td[pbketerangan]\").html(ket_sp2d.join(\", \"));

\t\t\t\t\t\t\t\t\t\$(\"tr[pbnospp=\" + Base64.encode(no_spp) + \"]\").find(\"td[pbnosp2d]\").attr(\"pbtglsp2d\", Base64.encode(data_sp2d.Tgl_SP2D));
\t\t\t\t\t\t\t\t}, \"json\");\t\t\t
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}, \"json\");
\t\t\t\t\t}
\t\t\t\t}

\t\t\t\tvar check_sp2d_by_spm = function(dspm_input) {
\t\t\t\t\tvar tbody = \$(\"#pembayaran\").find(\"tbody\");
\t\t\t\t\tvar no_spp = guid();

\t\t\t\t\t// get sp2d
\t\t\t\t\t\$.get(\"";
        // line 1819
        echo twig_escape_filter($this->env, site_url("pengadaan/pembayaran_sp2d"), "html", null, true);
        echo "\", {no_spm:dspm_input.No_SPM}, function(response_sp2d){
\t\t\t\t\t\tvar data_sp2d = response_sp2d.data;

\t\t\t\t\t\tfor (var i = 0; i < data_sp2d.length; i++) {
\t\t\t\t\t\t\tvar dsp2d = data_sp2d[i];

\t\t\t\t\t\t\ttbody.append(
\t\t\t\t\t\t\t\t[
\t\t\t\t\t\t\t\t\t\"<tr pbnospp='\" + Base64.encode(no_spp) + \"' generate>\",
\t\t\t\t\t\t\t\t\t\t\"<td pbnosp2d pbtglsp2d=\" + Base64.encode(dsp2d.Tgl_SP2D) + \">\" + dsp2d.No_SP2D + \"</td>\",
\t\t\t\t\t\t\t\t\t\t\"<td pbnospm>\" + dspm_input.No_SPM + \"</td>\",
\t\t\t\t\t\t\t\t\t\t\"<td pbnilaispm style='text-align: right; '></td>\",
\t\t\t\t\t\t\t\t\t\t\"<td pbkoderek13></td>\",
\t\t\t\t\t\t\t\t\t\t\"<td pburaianspm></td>\",
\t\t\t\t\t\t\t\t\t\t\"<td pbtermin></td>\",
\t\t\t\t\t\t\t\t\t\t\"<td pbthnspm>\" + dsp2d.Tgl_SP2D.split(\"/\")[0] + \"</td>\",
\t\t\t\t\t\t\t\t\t\t\"<td pbketerangan>\" + dspm_input.Uraian + \"</td>\",
\t\t\t\t\t\t\t\t\t\"</tr>\"
\t\t\t\t\t\t\t\t].join(\"\")
\t\t\t\t\t\t\t);
\t\t\t\t\t\t}

\t\t\t\t\t\t//get nilai SPM + Kode Rekening
\t\t\t\t\t\t\$.get(\"";
        // line 1842
        echo twig_escape_filter($this->env, site_url("pengadaan/pembayaran_spm_rinc"), "html", null, true);
        echo "\", {no_spm:dspm_input.No_SPM}, function(response_spm_rinc){
\t\t\t\t\t\t\tvar data_spm_rinc = response_spm_rinc.data;
\t\t\t\t\t\t\tvar nilai_spm = [];
\t\t\t\t\t\t\tvar kode_rek = [];
\t\t\t\t\t\t\tvar kd_rek_1 = '';
\t\t\t\t\t\t\tvar kd_rek_2 = '';
\t\t\t\t\t\t\tvar kd_rek_3 = '';
\t\t\t\t\t\t\tvar kd_rek_4 = '';
\t\t\t\t\t\t\tvar kd_rek_5 = '';
\t\t\t\t\t\t\tvar uraian_belanja = '';

\t\t\t\t\t\t\tfor (var i = 0; i < data_spm_rinc.length; i++) {
\t\t\t\t\t\t\t\tvar dspm_rinc = data_spm_rinc[i];
\t\t\t\t\t\t\t\tnilai_spm.push(dspm_rinc.Nilai);
\t\t\t\t\t\t\t\tkd_rek_1 = dspm_rinc.Kd_Rek_1;
\t\t\t\t\t\t\t\tkd_rek_2 = dspm_rinc.Kd_Rek_2;
\t\t\t\t\t\t\t\tkd_rek_3 = dspm_rinc.Kd_Rek_3;
\t\t\t\t\t\t\t\tkd_rek_4 = dspm_rinc.Kd_Rek_4;
\t\t\t\t\t\t\t\tkd_rek_5 = dspm_rinc.Kd_Rek_5;

\t\t\t\t\t\t\t\tkode_rek = [kd_rek_1, kd_rek_2, kd_rek_3, kd_rek_4, kd_rek_5].join('');

\t\t\t\t\t\t\t\t\$.get(\"";
        // line 1864
        echo twig_escape_filter($this->env, site_url("pengadaan/get_uraian_belanja"), "html", null, true);
        echo "\", {kd_rek_1:kd_rek_1, kd_rek_2:kd_rek_2, kd_rek_3:kd_rek_3, kd_rek_4:kd_rek_4, kd_rek_5:kd_rek_5}, function(response_uraian_belanja){
\t\t\t\t\t\t\t\t\t\turaian_belanja = response_uraian_belanja.data.Nm_Rek_5;
\t\t\t\t\t\t\t\t}, \"json\");

\t\t\t\t\t\t\t\tbreak;
\t\t\t\t\t\t\t}

\t\t\t\t\t\t\t// get ke controller yg mengembalikan uraian dari input kode_rek di tabel Ref_rek_5
\t\t\t\t\t\t\t// kerjakan
\t\t\t\t\t\t\t\$(\"tr[pbnospp=\" + Base64.encode(no_spp) + \"]\").find(\"td[pburaianspm]\").html(uraian_belanja);
\t\t\t\t\t\t\t\$(\"tr[pbnospp=\" + Base64.encode(no_spp) + \"]\").find(\"td[pbnilaispm]\").html(nilai_spm.join(\", \"));
\t\t\t\t\t\t\t\$(\"tr[pbnospp=\" + Base64.encode(no_spp) + \"]\").find(\"td[pbkoderek13]\").html(kode_rek);
\t\t\t\t\t\t}, \"json\");
\t\t\t\t\t}, \"json\");
\t\t\t\t}

\t\t\t\tfor (var i = 0; i < pembayaran.length; i++) {
\t\t\t\t\tvar d = pembayaran[i];
\t\t\t\t\ttbody.append(
\t\t\t\t\t\t[
\t\t\t\t\t\t\td.pbnospp == \"\" ? \"<tr pbnospp='\" + guid() + \"' generate>\" : \"<tr pbnospp='\" + Base64.encode(d.pbnospp) + \"'>\",
\t\t\t\t\t\t\t\t\"<td pbnosp2d pbtglsp2d='\" + Base64.encode(d.pbtglsp2d) + \"'>\" + d.pbnosp2d + \"</td>\",
\t\t\t\t\t\t\t\t\"<td pbnospm>\" + d.pbnospm + \"</td>\",
\t\t\t\t\t\t\t\t\"<td pbnilaispm style='text-align: right; '>\" + d.pbnilaispm + \"</td>\",
\t\t\t\t\t\t\t\t\"<td pbkoderek13>\" + d.pbkoderek13 + \"</td>\",
\t\t\t\t\t\t\t\t\"<td pburaianspm>\" + d.pburaianspm + \"</td>\",
\t\t\t\t\t\t\t\t\"<td pbtermin>\" + d.pbtermin + \"</td>\",
\t\t\t\t\t\t\t\t\"<td pbthnspm>\" + d.pbthnspm + \"</td>\",
\t\t\t\t\t\t\t\t\"<td pbketerangan>\" + d.pbketerangan + \"</td>\",
\t\t\t\t\t\t\t\"</tr>\"
\t\t\t\t\t\t].join(\"\")
\t\t\t\t\t);
\t\t\t\t}

\t\t\t\tif(no_spm_input === null || no_spm_input === ''){
\t\t\t\t\t\$.get(\"";
        // line 1899
        echo twig_escape_filter($this->env, site_url("pengadaan/pembayaran_spp"), "html", null, true);
        echo "\", {no_spk_sp_dokumen:no_spk_sp_dokumen }, function(response_spp) {
\t\t\t\t\t\tvar data_spp = response_spp.data;

\t\t\t\t\t\tif(data_spp.length === 0){
\t\t\t\t\t\t\t\$.get(\"";
        // line 1903
        echo twig_escape_filter($this->env, site_url("pengadaan/pembayaran_spp_uraian"), "html", null, true);
        echo "\", {no_spk_sp_dokumen:no_spk_sp_dokumen }, function(response_spp_uraian) {
\t\t\t\t\t\t\t\tdata_spp = response_spp_uraian.data;
\t\t\t\t\t\t\t\tcheck_sp2d_by_spp(data_spp);\t\t\t\t\t\t\t
\t\t\t\t\t\t\t}, \"json\");
\t\t\t\t\t\t} else {
\t\t\t\t\t\t\tdata_spp = data_spp;
\t\t\t\t\t\t\tcheck_sp2d_by_spp(data_spp);
\t\t\t\t\t\t}\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t\t\$(\"body\").unmask();
\t\t\t\t\t}, \"json\");
\t\t\t\t} else {
\t\t\t\t\t\$.get(\"";
        // line 1915
        echo twig_escape_filter($this->env, site_url("pengadaan/pembayaran_spm_input"), "html", null, true);
        echo "\", {no_spm_input:no_spm_input }, function(response_spm_input) {
\t\t\t\t\t\tcheck_sp2d_by_spm(response_spm_input.data);
\t\t\t\t\t\t
\t\t\t\t\t\t\$(\"body\").unmask();
\t\t\t\t\t}, \"json\");
\t\t\t\t}
\t\t\t});

\t\t\t\$(\"#submit\").on(\"click\", function(e) {
\t\t\t\tvar frm = \$(\"#frmMain\");
\t\t\t\t
\t\t\t\tvar jpnomorlokasi = frm.find(\"select[name=jpnomorlokasi]\").val();
\t\t\t\tvar jptgl = frm.find(\"input[name=jptgl]\").val();
\t\t\t\tvar jpnobaterima = frm.find(\"input[name=jpnobaterima]\").val();
\t\t\t\tvar jptglbaterima = frm.find(\"input[name=jptglbaterima]\").val();
\t\t\t\tvar jpnobaperiksa = frm.find(\"input[name=jpnobaperiksa]\").val();
\t\t\t\tvar jptglbaperiksa = frm.find(\"input[name=jptglbaperiksa]\").val();
\t\t\t\tvar no_spk_sp_dokumen = frm.find(\"select[name=no_spk_sp_dokumen]\").val();
\t\t\t\tvar tgl_spk_sp_dokumen = frm.find(\"input[name=tgl_spk_sp_dokumen]\").val();
\t\t\t\tvar kode_kegiatan = frm.find(\"select[name=kode_kegiatan]\").val();
\t\t\t\tvar id_kontrak = frm.find(\"input[name=id_kontrak]\").val();


\t\t\t\tvar frmData = new FormData();
\t\t\t\tfrmData.append(\"jpnomorlokasi\", jpnomorlokasi);
\t\t\t\tfrmData.append(\"jptgl\", jptgl);
\t\t\t\tfrmData.append(\"jpnobaterima\", jpnobaterima);
\t\t\t\tfrmData.append(\"jptglbaterima\", jptglbaterima);
\t\t\t\tfrmData.append(\"jpnobaperiksa\", jpnobaperiksa);
\t\t\t\tfrmData.append(\"jptglbaperiksa\", jptglbaperiksa);
\t\t\t\tfrmData.append(\"no_spk_sp_dokumen\", no_spk_sp_dokumen);
\t\t\t\tfrmData.append(\"tgl_spk_sp_dokumen\", tgl_spk_sp_dokumen);
\t\t\t\tfrmData.append(\"kode_kegiatan\", kode_kegiatan);
\t\t\t\tfrmData.append(\"id_kontrak\", id_kontrak);
\t\t\t\tfrmData.append(\"is_ajax\", 1);

\t\t\t\t";
        // line 1951
        if ((twig_length_filter($this->env, (isset($context["id"]) ? $context["id"] : null)) <= 1)) {
            // line 1952
            echo "\t\t\t\tvar url = \"";
            echo twig_escape_filter($this->env, site_url("pengadaan/submit"), "html", null, true);
            echo "\";
\t\t\t\t";
        } else {
            // line 1954
            echo "\t\t\t\tvar url = \"";
            echo twig_escape_filter($this->env, site_url(("pengadaan/submit/" . (isset($context["id"]) ? $context["id"] : null))), "html", null, true);
            echo "\";
\t\t\t\t";
        }
        // line 1956
        echo "
\t\t\t\t\$.ajax({
\t\t\t\t\turl: url,
\t\t\t\t\ttype: 'POST',
\t\t\t\t\tdata:  frmData,
\t\t\t\t\tdataType: \"json\",
\t\t\t\t\tmimeType: \"multipart/form-data\",
\t\t\t\t\tcontentType: false,
\t\t\t\t\tcache: false,
\t\t\t\t\tprocessData: false,
\t\t\t\t\tsuccess: function(response) {
\t\t\t\t\t\tvar id = response.id
\t\t\t\t\t\tif (response.success == 1) {
\t\t\t\t\t\t\tvar root = \$(\"#rincian-pembayaran\");

\t\t\t\t\t\t\tvar deferreds = [];

\t\t\t\t\t\t\ttrs = root.find(\"tbody > tr\");
\t\t\t\t\t\t\tif (trs.length > 0) {
\t\t\t\t\t\t\t\tfor (var i=0; i < trs.length; i++) {
\t\t\t\t\t\t\t\t\tvar tr = \$(trs[i]);

\t\t\t\t\t\t\t\t\tdeferreds.push(
\t\t\t\t\t\t\t\t\t\t\$.post(\"";
        // line 1979
        echo twig_escape_filter($this->env, site_url("pengadaan/submit_pembayaran"), "html", null, true);
        echo "\", {
\t\t\t\t\t\t\t\t\t\t\tpbthnspm: tr.find(\"td[pbthnspm]\").text(),
\t\t\t\t\t\t\t\t\t\t\tpbkoderek13: tr.find(\"td[pbkoderek13]\").text(),
\t\t\t\t\t\t\t\t\t\t\tpburaianspm: tr.find(\"td[pburaianspm]\").text(),
\t\t\t\t\t\t\t\t\t\t\tpbnosp2d: tr.find(\"td[pbnosp2d]\").text(),
\t\t\t\t\t\t\t\t\t\t\tpbtglsp2d: Base64.decode(tr.find(\"td[pbnosp2d]\").attr(\"pbtglsp2d\")),
\t\t\t\t\t\t\t\t\t\t\tpbtermin: tr.find(\"td[pbtermin]\").text(),
\t\t\t\t\t\t\t\t\t\t\tpbnospm: tr.find(\"td[pbnospm]\").text(),
\t\t\t\t\t\t\t\t\t\t\tpbtglspm: tr.find(\"td[pbtglspm]\").text(),
\t\t\t\t\t\t\t\t\t\t\tpbnospp: Base64.decode(tr.attr(\"pbnospp\")),
\t\t\t\t\t\t\t\t\t\t\tpbtglspp: tr.find(\"td[pbtglspp]\").text(),
\t\t\t\t\t\t\t\t\t\t\tpbketerangan: tr.find(\"td[pbketerangan]\").text(),
\t\t\t\t\t\t\t\t\t\t\tpbnilaispm: tr.find(\"td[pbnilaispm]\").text(),
\t\t\t\t\t\t\t\t\t\t\tjpid: ";
        // line 1992
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : null), "html", null, true);
        echo "
\t\t\t\t\t\t\t\t\t\t}, function(response) {}, \"json\")
\t\t\t\t\t\t\t\t\t);
\t\t\t\t\t\t\t\t}

\t\t\t\t\t\t\t\t\$.when.apply(null, deferreds).done(function() {
\t\t\t\t\t\t            window.location.href = \"";
        // line 1998
        echo twig_escape_filter($this->env, site_url("pengadaan/form"), "html", null, true);
        echo "/\" + id;
\t\t\t\t\t\t        });
\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\twindow.location.href = \"";
        // line 2001
        echo twig_escape_filter($this->env, site_url("pengadaan/form"), "html", null, true);
        echo "/\" + id;
\t\t\t\t\t\t\t}
\t\t\t\t\t\t} else {
\t\t\t\t\t\t\talert(\"Data tidak dapat disimpan!\");
\t\t\t\t\t\t}
\t\t\t\t\t} 
\t\t\t\t});

\t\t\t\te.preventDefault();
\t\t\t});


\t\t\t\$.common.CallbackManager.create({name: 'get_rincian1', callback: \$.Callbacks()});
\t\t\t\$.common.CallbackManager.getByName('get_rincian1').add(function() {
\t            get_rincian1();
\t        });

\t\t\t\$.common.CallbackManager.create({name: 'get_rincian2', callback: \$.Callbacks()});
\t\t\t\$.common.CallbackManager.getByName('get_rincian2').add(function() {
\t            get_rincian2();
\t        });

\t        \$.common.CallbackManager.create({name: 'get_rincian3', callback: \$.Callbacks()});
\t\t\t\$.common.CallbackManager.getByName('get_rincian3').add(function() {
\t            get_rincian3();
\t        });

\t        \$.common.CallbackManager.create({name: 'get_rincian4', callback: \$.Callbacks()});
\t\t\t\$.common.CallbackManager.getByName('get_rincian4').add(function() {
\t            get_rincian4();
\t        });

\t        \$.common.CallbackManager.create({name: 'get_rincian5', callback: \$.Callbacks()});
\t\t\t\$.common.CallbackManager.getByName('get_rincian5').add(function() {
\t            get_rincian5();
\t        });

\t        \$.common.CallbackManager.create({name: 'get_rincian6', callback: \$.Callbacks()});
\t\t\t\$.common.CallbackManager.getByName('get_rincian6').add(function() {
\t            get_rincian6();
\t        });

\t\t\tget_rincian1();
\t\t\tget_rincian2();
\t\t\tget_rincian3();
\t\t\tget_rincian4();
\t\t\tget_rincian5();
\t\t\tget_rincian6();
\t\t\tget_pembayaran();


\t\t\tvar el = \$(\"select[name=no_spk_sp_dokumen]\");
\t\t\t";
        // line 2053
        if ((twig_length_filter($this->env, (isset($context["id"]) ? $context["id"] : null)) > 1)) {
            // line 2054
            echo "\t\t\t\tel.empty().append(\"<option no_spk_sp_dokumen='";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "spk"), "NO_SPK_SP_DOKUMEN"), "html", null, true);
            echo "' value='";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "spk"), "ID_KONTRAK"), "html", null, true);
            echo "' rekanan='";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "spk"), "REKANAN"), "html", null, true);
            echo "' nilai_spk='";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "spk"), "NILAI_SPK"), "html", null, true);
            echo "' tgl_spk_sp_dokumen='";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "spk"), "TGL_SPK_SP_DOKUMEN"), "html", null, true);
            echo "' deskripsi_spk_dokumen='";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "spk"), "DESKRIPSI_SPK_DOKUMEN"), "html", null, true);
            echo "' id_kegiatan='";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "spk"), "ID_KEGIATAN"), "html", null, true);
            echo "'>";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "spk"), "NO_SPK_SP_DOKUMEN"), "html", null, true);
            echo "</option>\");
\t\t\t";
        }
        // line 2056
        echo "\t\t});
\t</script>
";
    }

    public function getTemplateName()
    {
        return "pengadaan/form.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  2338 => 2056,  2318 => 2054,  2316 => 2053,  2261 => 2001,  2255 => 1998,  2246 => 1992,  2230 => 1979,  2205 => 1956,  2199 => 1954,  2193 => 1952,  2191 => 1951,  2152 => 1915,  2137 => 1903,  2130 => 1899,  2092 => 1864,  2067 => 1842,  2041 => 1819,  2012 => 1793,  1997 => 1781,  1972 => 1759,  1948 => 1738,  1873 => 1666,  1832 => 1628,  1792 => 1591,  1752 => 1554,  1706 => 1511,  1660 => 1468,  1643 => 1454,  1406 => 1220,  1362 => 1179,  1310 => 1130,  1017 => 840,  947 => 773,  839 => 668,  818 => 650,  808 => 643,  728 => 566,  642 => 483,  638 => 482,  634 => 481,  630 => 480,  626 => 479,  622 => 478,  618 => 477,  614 => 476,  608 => 474,  605 => 473,  533 => 403,  529 => 402,  525 => 401,  521 => 400,  517 => 399,  511 => 397,  508 => 396,  502 => 392,  204 => 97,  200 => 96,  193 => 92,  182 => 84,  178 => 83,  171 => 79,  159 => 72,  153 => 71,  147 => 70,  134 => 60,  130 => 59,  105 => 36,  100 => 34,  95 => 32,  93 => 31,  88 => 28,  82 => 26,  80 => 25,  74 => 24,  70 => 22,  68 => 21,  66 => 20,  64 => 19,  62 => 18,  60 => 17,  58 => 16,  56 => 15,  54 => 14,  52 => 13,  50 => 12,  48 => 11,  46 => 10,  44 => 9,  42 => 8,  40 => 7,  38 => 6,  36 => 5,  33 => 4,  30 => 3,);
    }
}
