<?php

/* simgo/edit_mediapool.html */
class __TwigTemplate_6c95dbae02bbf28f9b53242cde1366b0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"modal fade\" id=\"edit-mediapool\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
\t<div class=\"modal-dialog modal-md\">
\t\t<div class=\"modal-content\">
\t\t\t<div class=\"modal-header\">
\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
\t\t\t\t<h4 class=\"modal-title\" id=\"myModalLabel\">Ubah Nomor ";
        // line 6
        echo twig_escape_filter($this->env, (isset($context["kategori"]) ? $context["kategori"] : null), "html", null, true);
        echo "</h4>
\t\t\t</div>
\t\t\t<div class=\"modal-body\">
\t\t\t\t<form role=\"form\">
\t\t\t\t\t<label class=\"control-label\">";
        // line 10
        if (((isset($context["kategori"]) ? $context["kategori"] : null) == "SPK")) {
            echo "No. SPK";
        } elseif (((isset($context["kategori"]) ? $context["kategori"] : null) == "Kegiatan")) {
            echo "Kode Rekening Kegiatan";
        }
        echo "</label>
\t\t\t\t\t<input type=\"text\" class=\"form-control input-lg\" name=\"no-mediapool\" value=\"";
        // line 11
        echo twig_escape_filter($this->env, (isset($context["mpno"]) ? $context["mpno"] : null), "html", null, true);
        echo "\">
\t\t\t\t</form>
\t\t\t</div>
\t\t\t<div class=\"modal-footer\">
\t\t\t\t<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Batal</button>
\t\t\t\t<button type=\"button\" name=\"simpan\" class=\"btn btn-primary\">Ubah</button>
\t\t\t</div>
\t\t</div>
\t</div>
</div>";
    }

    public function getTemplateName()
    {
        return "simgo/edit_mediapool.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  41 => 11,  26 => 6,  19 => 1,  249 => 120,  240 => 114,  229 => 106,  226 => 105,  199 => 81,  196 => 80,  180 => 67,  173 => 62,  166 => 60,  160 => 58,  154 => 56,  152 => 55,  145 => 54,  141 => 53,  127 => 41,  121 => 39,  119 => 38,  115 => 37,  112 => 36,  104 => 33,  100 => 31,  88 => 29,  86 => 28,  82 => 26,  78 => 25,  58 => 15,  52 => 13,  46 => 11,  44 => 10,  38 => 6,  36 => 5,  33 => 10,  30 => 3,);
    }
}
