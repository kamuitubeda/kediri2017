<?php

/* aset/penyusutan.html */
class __TwigTemplate_1440d713a0664f5ec039a0b915ec71f3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("_templates/adminpage.html");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'scripts' => array($this, 'block_scripts'),
            'stylesheets' => array($this, 'block_stylesheets'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_templates/adminpage.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "
<div class=\"row\">
\t<div class=\"col-xs-4\">
\t\t<div class=\"col-xs-12\">
\t\t\t<label>SKPD</label>
\t\t\t<select type=\"text\" name=\"nomor_unit\" class=\"form-control\">
\t\t\t\t<option value=\"\">Pilih SKPD</option>
\t\t\t</select>
\t\t</div>
\t\t<div class=\"col-xs-12\" name=\"sub-unit\" style=\"display:none\">
\t\t\t<label>SUB UNIT</label>
\t\t\t<select type=\"text\" name=\"nomor_sub_unit\" class=\"form-control\">
\t\t\t\t<option value=\"\">Pilih SUB UNIT</option>
\t\t\t</select>
\t\t</div>
\t</div>
\t<div class=\"col-xs-8\">
\t\t<div class=\"col-xs-6\">
\t\t\t<label>JENIS KIB</label>
\t\t\t<select type=\"text\" name=\"jenis_kib\" class=\"form-control\">
\t\t\t\t<option value=\"\">Pilih KIB</option>
\t\t\t\t<option value=\"B\">KIB B (Peralatan dan Kendaraan)</option>
\t\t\t\t<option value=\"C\">KIB C (Gedung dan Bangunan)</option>
\t\t\t\t<option value=\"D\">KIB D (Jalan dan Jembatan)</option>
\t\t\t\t<option value=\"E\">KIB E (Buku, Hewan, Tanaman, Barang Kebudayaan)</option>
\t\t\t\t<option value=\"G\">KIB G (Aset Tak Berwujud)</option>
\t\t\t</select>
\t\t</div>
\t\t<div class=\"col-xs-6\">
\t\t\t<label>JENIS ASET</label>
\t\t\t<select type=\"text\" name=\"jenis_aset\" class=\"form-control\">
\t\t\t\t<option value=\"\">Semuanya</option>
\t\t\t\t<option value=\"1.3\">Aset Tetap (1.3*)</option>
\t\t\t\t<option value=\"1.5\">Aset Lainnya (1.5*)</option>
\t\t\t</select>
\t\t</div>
\t\t<div class=\"col-xs-6\">
\t\t\t<label>KODE KEPEMILIKAN</label>
\t\t\t<select type=\"text\" name=\"kode_kepemilikan\" class=\"form-control\">
\t\t\t\t<option value=\"12\">12 (Pemerintah Kabupaten/Kota)</option>
\t\t\t\t<option value=\"98\">98 (Ekstrakom)</option>
\t\t\t</select>
\t\t</div>
\t\t<div class=\"col-xs-3\">
\t\t\t<label>KAPITAL</label>
\t\t\t<input type=\"checkbox\" name=\"kapital-checkbox\" data-on-text=\"Ya\" data-off-text=\"Tidak\" />
\t\t</div>
\t\t<div class=\"col-xs-3\" name=\"button-susut-atas\">
\t\t\t<button id=\"btnHitungSusut\" type=\"button\" class=\"btn btn-primary\" style=\"padding-left: 1; padding-right: 1;\"><span class=\"glyphicon glyphicon-ok-circle\"></span> Hitung Penyusutan</button>
\t\t</div>\t
\t</div>
\t<div class=\"row\" name=\"hasil_susut\" style=\"display:none\">
\t\t<div class=\"col-xs-12\">
\t\t\t<div class=\"col-xs-12\">
\t\t\t\t<div class=\"row\">\t
\t\t\t\t\t<div class=\"pull-left\">
\t\t\t\t\t\t<button id=\"btnPrintHitungSusut\" type=\"button\" style=\"height:30px;\" class=\"btn btn-primary\"><span class=\"glyphicon glyphicon-print\"></span> Print</button>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"table-responsive\">
\t\t\t\t\t<table class=\"table table-hover table_susut\">
\t\t\t\t\t\t<thead class=\"table-bordered\">
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<th class=\"table-bordered\" style=\"text-align:center\">Nama Unit</th>
\t\t\t\t\t\t\t<th class=\"table-bordered\" style=\"text-align:center\">No Register</th>
\t\t\t\t\t\t\t<th class=\"table-bordered\" style=\"text-align:center\">Nama Barang</th>
\t\t\t\t\t\t\t<th class=\"table-bordered\" style=\"text-align:center\">Merk Alamat</th>
\t\t\t\t\t\t\t<th class=\"table-bordered\" style=\"text-align:center\">Tahun Perolehan</th>
\t\t\t\t\t\t\t<th class=\"table-bordered\" style=\"text-align:center\">Masa Manfaat</th>
\t\t\t\t\t\t\t<th class=\"table-bordered\" style=\"text-align:center\">Masa Terpakai</th>
\t\t\t\t\t\t\t<th class=\"table-bordered\" style=\"text-align:center\">Sisa MM</th>
\t\t\t\t\t\t\t<th class=\"table-bordered\" style=\"text-align:center\">Nilai Perolehan</th>
\t\t\t\t\t\t\t<th class=\"table-bordered\" style=\"text-align:center\">Akumulasi Penyusutan Per 31 Desember 2016</th>
\t\t\t\t\t\t\t<th class=\"table-bordered\" style=\"text-align:center\">Beban Penyusutan Tahun 2017</th>
\t\t\t\t\t\t\t<th class=\"table-bordered\" style=\"text-align:center\">Nilai Buku</th>
\t\t\t\t\t\t</tr>
\t\t\t\t\t\t</thead>
\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t</tbody>
\t\t\t\t\t</table>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
</div>
";
    }

    // line 92
    public function block_scripts($context, array $blocks = array())
    {
        // line 93
        $this->displayParentBlock("scripts", $context, $blocks);
        echo "
<script type=\"text/javaScript\" src=\"";
        // line 94
        echo twig_escape_filter($this->env, base_url("assets/js/jquery.loadmask.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 95
        echo twig_escape_filter($this->env, base_url("assets/js/select2.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 96
        echo twig_escape_filter($this->env, base_url("assets/js/accounting.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 97
        echo twig_escape_filter($this->env, base_url("assets/js/bootstrap-switch.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 98
        echo twig_escape_filter($this->env, base_url("assets/js/xlsx.core.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 99
        echo twig_escape_filter($this->env, base_url("assets/js/Blob.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 100
        echo twig_escape_filter($this->env, base_url("assets/js/FileSaver.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 101
        echo twig_escape_filter($this->env, base_url("assets/js/tableexport.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\">
    jQuery(document).ready(function(\$) {
    \t\$.fn.bootstrapSwitch.defaults.size = 'normal';
    \t\$('input[name=\"kapital-checkbox\"]').bootstrapSwitch();
    \t// configure setting accounting.js
\t\taccounting.settings = {
\t\t\tnumber: {
\t\t\t\tprecision : 0,  // default precision on numbers is 0
\t\t\t\tthousand: \".\",
\t\t\t\tdecimal : \",\"
\t\t\t}
\t\t}

\t\tvar n = function(v, p) {
\t\t\treturn accounting.formatNumber(v, p);
\t\t}

\t\tvar u = function(v) {
\t\t\treturn accounting.unformat(v);
\t\t}

    \t//menampilkan list SKPD
\t\t\$.get(\"";
        // line 124
        echo twig_escape_filter($this->env, site_url("aset/list_skpd"), "html", null, true);
        echo "\", {}, function(json){ 
\t\t\tvar listSkpd = json.result;
\t\t\t
\t\t\t\$.each(listSkpd, function (key, value) {
\t\t\t\t\$(\"select[name=nomor_unit]\").append(\$('<option></option>').val(value.NOMOR_UNIT).html(value.NAMA_UNIT));
\t\t\t});

\t\t\t//\$(\"select[name=nomor_unit]\").val(\"";
        // line 131
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "NOMOR_UNIT"), "html", null, true);
        echo "\");
\t\t\t\$(\"select[name=nomor_unit]\").select2();

\t\t\t// //reload_skpd();
\t\t\t// var user = \$(\"#user\").text();

\t\t\t// if (user != 'Administrator') {
\t\t\t// \t\$.post(\"";
        // line 138
        echo twig_escape_filter($this->env, site_url("aset/get_skpd_user"), "html", null, true);
        echo "\", {user:user}, function(json){ 
\t\t\t// \t\tvar skpd_user = json.data;
\t\t\t// \t\tvar oid = skpd_user.oid;
\t\t\t// \t\t//console.log(skpd_user);

\t\t\t// \t\t\$.post(\"";
        // line 143
        echo twig_escape_filter($this->env, site_url("aset/get_skpd_organisasi"), "html", null, true);
        echo "\", {oid:oid}, function(json){ 
\t\t\t// \t\t\tvar skpd_organisasi = json.data;
\t\t\t// \t\t\tvar skpd_name = skpd_organisasi.oname;
\t\t\t\t\t\t
\t\t\t// \t\t\t\$.post(\"";
        // line 147
        echo twig_escape_filter($this->env, site_url("aset/get_skpd_val"), "html", null, true);
        echo "\", {skpd_name:skpd_name}, function(json){ 
\t\t\t// \t\t\t\tvar skpd_val = json.data;
\t\t\t
\t\t\t// \t\t\t\t//untuk mengubah data select 2 jangan lupa tambahkan ini >> .trigger('change');
\t\t\t// \t\t\t\t\$('select[name=\"nomor_unit\"]').val(skpd_val.NOMOR_UNIT).trigger('change');
\t\t\t// \t\t\t\t\$('select[name=\"nomor_unit\"]').prop( \"disabled\", true );           
\t\t\t\t\t\t\t
\t\t\t// \t\t\t}, \"json\");
\t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t// \t\t}, \"json\");
\t\t\t// \t}, \"json\");
\t\t\t// }

\t\t}, \"json\");

\t\t//load sub unit (khusus dinas pendidikan dan dinas kesehatan)
\t\t\$(\"select[name=nomor_unit]\").change(function() {
\t\t\tvar nomor_unit = \$(\"select[name=nomor_unit]\").val();

\t\t\tif(nomor_unit === \"13.10.08.01\" || nomor_unit === \"13.10.07.01\" || nomor_unit === \"13.10.04.01\") {
\t\t\t\tload_sub_unit();
\t\t\t\t\$('div[name=\"sub-unit\"]').show();
\t\t\t} else {
\t\t\t\t\$(\"select[name=nomor_sub_unit]\").val(\"\");
\t\t\t\t\$('div[name=\"sub-unit\"]').hide();
\t\t\t}
\t\t});

\t\tvar load_sub_unit = function(callback) {
\t\t\t\$(\"body\").mask(\"Processing...\");
\t\t\tvar nomor_unit = \$(\"select[name=nomor_unit]\").val();

\t\t\t\$.post(\"";
        // line 180
        echo twig_escape_filter($this->env, site_url("aset/get_list_sub_unit"), "html", null, true);
        echo "\", {
\t\t\t\tnomor_unit: nomor_unit
\t\t\t}, function(json){ 
\t\t\t\tvar data = json.data;
\t\t\t\tvar el = \$(\"select[name=nomor_sub_unit]\");

\t\t\t\tel.empty();
\t\t\t\tel.append(\"<option value=''>Pilih Sub Unit</option>\");
\t\t\t\tfor (var i=0; i<data.length; i++) {
\t\t\t\t\tvar k = data[i];
\t\t\t\t\tel.append(\$(\"<option></option>\").attr(\"value\", k.NOMOR_SUB_UNIT).html(k.NOMOR_SUB_UNIT + \" | \" + k.NAMA_SUB_UNIT));
\t\t\t\t\t// el.append(\"<option value='{0}'>{1}</option>\".format(k.NO_REGISTER, k.NAMA_BARANG));
\t\t\t\t}

\t\t\t\tel.select2();
\t\t\t\t\$(\"body\").unmask();

\t\t\t\tif (callback !== undefined) {
\t\t\t\t\tcallback();
\t\t\t\t}
\t\t\t}, \"json\");
\t\t}
\t\t
\t\t\$(\"#btnHitungSusut\").click(function() {
    \t\t\$('tbody').empty();
    \t\t\$(\"body\").mask(\"Processing...\");
    \t\t\$('caption').empty();

    \t\tvar nomor_unit = \$(\"select[name=nomor_unit]\").val();
    \t\tvar nomor_sub_unit = \$(\"select[name=nomor_sub_unit]\").val();
    \t\tvar nama_unit = \$(\"select[name=nomor_unit] option:selected\").text();
    \t\tvar nama_sub_unit = \$(\"select[name=nomor_sub_unit] option:selected\").text();
    \t\tvar kib = \$('select[name=\"jenis_kib\"]').val();
    \t\tvar jenis_aset = \$(\"select[name=jenis_aset]\").val();
    \t\tvar kode_kepemilikan = \$(\"select[name=kode_kepemilikan]\").val();
    \t\t\$('div[name=\"hasil_susut\"]').show();

    \t\tvar kapital = \$('input[name=\"kapital-checkbox\"]').is(\":checked\");

    \t\t\$.post(\"";
        // line 219
        echo twig_escape_filter($this->env, site_url("aset/get_susut_kib"), "html", null, true);
        echo "\", {nomor_unit:nomor_unit, nomor_sub_unit:nomor_sub_unit, kib:kib, jenis_aset:jenis_aset, kode_kepemilikan:kode_kepemilikan}, function(json) {

    \t\t\tif(kapital) {
    \t\t\t\tvar aset = json.aset_kapitalisasi; 
\t\t\t\t\tvar total_aset = json.total_aset_kapitalisasi;
    \t\t\t} else {
    \t\t\t\tvar aset = json.aset;
    \t\t\t\tvar total_aset = json.total_aset;
    \t\t\t}

    \t\t\tif(nomor_sub_unit !== '') {
    \t\t\t\tnama_unit = nama_sub_unit;
    \t\t\t}
\t\t\t\t
\t\t\t\tvar html = '';
\t\t\t\tvar tables = \$(\"table\");

\t\t\t\t\$.each(aset, function(key, value) {
\t\t\t\t\tvar value = aset[key];
\t\t\t\t\tvar mm_sisa = parseInt(value.MASA_MANFAAT) - parseInt(value.MASA_TERPAKAI);

\t\t\t\t\tif(mm_sisa < 0) {
\t\t\t\t\t\tmm_sisa = 0;
\t\t\t\t\t}

\t\t\t\t\tif(value.STATUS_ASET === 1) {
\t\t\t\t\t\thtml += '<tr style=\"font-weight:bold;\"><td>'
\t\t\t\t\t\t+ nama_unit
\t\t\t\t\t\t+ '</td><td>'
\t\t\t\t\t\t+ value.NO_REGISTER
\t\t\t\t\t\t+ '</td><td>'
\t\t\t\t\t\t+ value.NAMA_BARANG
\t\t\t\t\t\t+ '</td><td>'
\t\t\t\t\t\t+ value.MERK_ALAMAT
\t\t\t\t\t\t+ '</td><td>'
\t\t\t\t\t\t+ value.TAHUN_PENGADAAN
\t\t\t\t\t\t+ '</td><td>'
\t\t\t\t\t\t+ value.MASA_MANFAAT
\t\t\t\t\t\t+ '</td><td>'
\t\t\t\t\t\t+ value.MASA_TERPAKAI
\t\t\t\t\t\t+ '</td><td>'
\t\t\t\t\t\t+ mm_sisa
\t\t\t\t\t\t+ '</td><td align=\"right\">'
\t\t\t\t\t\t+ value.NILAI_PEROLEHAN
\t\t\t\t\t\t+ '</td><td align=\"right\">'
\t\t\t\t\t\t+ value.AKUMULASI_PENYUSUTAN
\t\t\t\t\t\t+ '</td><td align=\"right\">'
\t\t\t\t\t\t+ value.BEBAN
\t\t\t\t\t\t+ '</td><td align=\"right\">'
\t\t\t\t\t\t+ value.NILAI_BUKU
\t\t\t\t\t\t+ '</td></tr>';
\t\t\t\t\t} else {
\t\t\t\t\t\thtml += '<tr><td>'
\t\t\t\t\t\t+ nama_unit
\t\t\t\t\t\t+ '</td><td>'
\t\t\t\t\t\t+ value.NO_REGISTER
\t\t\t\t\t\t+ '</td><td>'
\t\t\t\t\t\t+ value.NAMA_BARANG
\t\t\t\t\t\t+ '</td><td>'
\t\t\t\t\t\t+ value.MERK_ALAMAT
\t\t\t\t\t\t+ '</td><td>'
\t\t\t\t\t\t+ value.TAHUN_PENGADAAN
\t\t\t\t\t\t+ '</td><td>'
\t\t\t\t\t\t+ value.MASA_MANFAAT
\t\t\t\t\t\t+ '</td><td>'
\t\t\t\t\t\t+ value.MASA_TERPAKAI
\t\t\t\t\t\t+ '</td><td>'
\t\t\t\t\t\t+ mm_sisa
\t\t\t\t\t\t+ '</td><td align=\"right\">'
\t\t\t\t\t\t+ value.NILAI_PEROLEHAN
\t\t\t\t\t\t+ '</td><td align=\"right\">'
\t\t\t\t\t\t+ value.AKUMULASI_PENYUSUTAN
\t\t\t\t\t\t+ '</td><td align=\"right\">'
\t\t\t\t\t\t+ value.BEBAN
\t\t\t\t\t\t+ '</td><td align=\"right\">'
\t\t\t\t\t\t+ value.NILAI_BUKU
\t\t\t\t\t\t+ '</td></tr>';
\t\t\t\t\t}
\t\t\t\t});

\t\t\t\thtml += '<tr><td>'
\t\t\t\t\t+ '<strong>' + \"TOTAL\" + '</strong>'
\t\t\t\t\t+ '</td><td>'
\t\t\t\t\t+ ''
\t\t\t\t\t+ '</td><td>'
\t\t\t\t\t+ ''
\t\t\t\t\t+ '</td><td>'
\t\t\t\t\t+ ''
\t\t\t\t\t+ '</td><td>'
\t\t\t\t\t+ ''
\t\t\t\t\t+ '</td><td>'
\t\t\t\t\t+ ''
\t\t\t\t\t+ '</td><td>'
\t\t\t\t\t+ ''
\t\t\t\t\t+ '</td><td>'
\t\t\t\t\t+ ''
\t\t\t\t\t+ '</td><td align=\"right\">'
\t\t\t\t\t+ '<strong>' + total_aset.TOTAL_NILAI_PEROLEHAN + '</strong>'
\t\t\t\t\t+ '</td><td align=\"right\">'
\t\t\t\t\t+ '<strong>' + total_aset.TOTAL_AKUMULASI_PENYUSUTAN + '</strong>'
\t\t\t\t\t+ '</td><td align=\"right\">'
\t\t\t\t\t+ '<strong>' + total_aset.TOTAL_BEBAN + '</strong>'
\t\t\t\t\t+ '</td><td align=\"right\">'
\t\t\t\t\t+ '<strong>' + total_aset.TOTAL_NILAI_BUKU + '</strong>'
\t\t\t\t\t+ '</td></tr>';

\t\t\t\t\$('.table_susut > tbody').append(html);
\t\t\t\t\$(\"body\").unmask();

\t\t\t\ttables.tableExport({
\t\t\t\t    bootstrap: true,
\t\t\t\t    format: [\"xlsx\"]
\t\t\t\t});
\t\t\t}, \"json\");

    \t});

\t\t\$(\"#btnPrintHitungSusut64\").click(function() {
\t\t\tvar nomor_unit = \$(\"select[name=nomor_unit]\").val();
    \t\tvar kib = \$('select[name=\"jenis_kib\"]').val();
            
            window.open( \"";
        // line 340
        echo twig_escape_filter($this->env, site_url("aset/print_penyusutan"), "html", null, true);
        echo "/\"+ nomor_unit + \"/\" + kib, '_blank');
\t\t});

\t\t\$(\"#btnPrintHitungSusut\").click(function() {
\t\t\tvar nomor_unit = \$(\"select[name=nomor_unit]\").val();
\t\t\tvar nomor_sub_unit = \$(\"select[name=nomor_sub_unit]\").val();
    \t\tvar kib = \$('select[name=\"jenis_kib\"]').val();
    \t\tvar kapital = \$('input[name=\"kapital-checkbox\"]').is(\":checked\");

    \t\tif(nomor_sub_unit === \"\") {
            \twindow.open( \"";
        // line 350
        echo twig_escape_filter($this->env, site_url("aset/print_penyusutan_per_item"), "html", null, true);
        echo "/\"+ nomor_unit + \"/\" + kapital + \"/\" + kib, '_blank');
    \t\t} else {
    \t\t\twindow.open( \"";
        // line 352
        echo twig_escape_filter($this->env, site_url("aset/print_penyusutan_per_item"), "html", null, true);
        echo "/\"+ nomor_sub_unit + \"/\" + kapital + \"/\" + kib, '_blank');
    \t\t}
            
\t\t});

\t\t\$(\"#btnPrintHitungSusut2\").click(function() {
\t\t\tvar nomor_unit = \$(\"select[name=nomor_unit]\").val();
    \t\tvar kib = \$('select[name=\"jenis_kib\"]').val();
    \t\tvar kapital = \$('input[name=\"kapital-checkbox\"]').is(\":checked\");
    \t\tvar rule = \"\";

    \t\tif(kapital) {
    \t\t\trule = \"yes\";
    \t\t} else {
    \t\t\trule = \"no\";
    \t\t}
            
            window.open( \"";
        // line 369
        echo twig_escape_filter($this->env, site_url("aset/print_penyusutan_per_item"), "html", null, true);
        echo "/\"+ nomor_unit + \"/\" + kib + \"/\" + rule, '_blank');
\t\t});
\t});
</script>
";
    }

    // line 376
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 377
        echo "\t";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 378
        echo twig_escape_filter($this->env, base_url("assets/css/devexpress-like/jquery-ui.css"), "html", null, true);
        echo "\" />
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 379
        echo twig_escape_filter($this->env, base_url("assets/css/select2.min.css"), "html", null, true);
        echo "\" />
\t<link href=\"";
        // line 380
        echo twig_escape_filter($this->env, base_url("assets/css/jquery.loadmask.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
\t<link href=\"";
        // line 381
        echo twig_escape_filter($this->env, base_url("assets/css/bootstrap-switch.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 382
        echo twig_escape_filter($this->env, base_url("assets/css/tableexport.css"), "html", null, true);
        echo "\" />\t

\t<style type=\"text/css\">
\t\t.control-label{
\t\t\tpadding-top: 8px !important;
\t\t}

\t\tselect.form-control {
\t\t\theight: 48px !important;
\t\t}

\t\tinput.form-control {
\t\t\theight: 34px !important;
\t\t}

\t\ttextarea.form-control {
\t\t\theight:auto !important;
\t\t}

\t\t.row {
\t\t\tpadding:10px 0px;
\t\t}

\t\t.spacer {
\t\t    margin-top: 20px;
\t\t}

\t\t.ui-autocomplete{
\t\t\tbackground: none repeat scroll 0 0 white;
\t\t}

\t\t.input-group-addon{
\t\t\tborder-radius: 0 !important;
\t\t}

\t\t.input-group .form-control{
\t\t\tz-index: 0; !important;
\t\t}

\t\t.select2-selection__rendered {
\t\t\theight: 28px;
\t\t}
\t\t.table > thead > tr > th {
\t\t\tvertical-align: middle;
\t\t}
\t\t#btnHitungSusut {
\t\t\tmargin-top: 22px;
\t\t\theight: 30px;
\t\t}
\t\t.btn-toolbar {
\t\t\ttext-align: left !Important;
\t\t}
\t</style>
";
    }

    public function getTemplateName()
    {
        return "aset/penyusutan.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  491 => 382,  487 => 381,  483 => 380,  479 => 379,  475 => 378,  470 => 377,  467 => 376,  458 => 369,  438 => 352,  433 => 350,  420 => 340,  296 => 219,  254 => 180,  218 => 147,  211 => 143,  203 => 138,  193 => 131,  183 => 124,  157 => 101,  153 => 100,  149 => 99,  145 => 98,  141 => 97,  137 => 96,  133 => 95,  129 => 94,  125 => 93,  122 => 92,  33 => 4,  30 => 3,);
    }
}
