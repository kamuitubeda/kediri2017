<?php

/* aset/modal_kendaraan.html */
class __TwigTemplate_625dd8647d0ac877b6d678db8c0be4e2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"modal fade\" id=\"tambah-kendaraan\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
\t<div class=\"modal-dialog modal-md\">
\t\t<div class=\"modal-content\">
\t\t\t<div class=\"modal-header\">
\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
\t\t\t\t<h4 class=\"modal-title\" id=\"myModalLabel\">Tambahkan Pengguna Kendaraan</h4>
\t\t\t</div>
\t\t\t<div class=\"modal-body\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t<form role=\"form\" id=\"tambah-aset-renov-form\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label class=\"control-label\">Kendaraan<strong>*</strong></label>
\t\t\t\t\t\t\t\t<select type=\"text\" class=\"form-control\" id=\"id-aset-kendaraan\" name=\"id-aset-kendaraan\" data-rule-required=\"true\">
\t\t\t\t\t\t\t\t\t<option value=\"\">Pilih Kendaraan</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label class=\"control-label\">Nama Pengguna<strong>*</strong></label>
\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"pengguna\" name=\"pengguna\" data-rule-required=\"true\"/>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</form>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"modal-footer\">
\t\t\t\t<button type=\"button\" name=\"batal\" class=\"btn btn-default\" data-dismiss=\"modal\">Batal</button>
\t\t\t\t<button type=\"button\" name=\"simpan\" class=\"btn btn-primary\">Tambah</button>
\t\t\t</div>
\t\t</div>
\t</div>
</div>";
    }

    public function getTemplateName()
    {
        return "aset/modal_kendaraan.html";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,  413 => 303,  409 => 302,  405 => 301,  401 => 300,  397 => 299,  392 => 298,  389 => 297,  376 => 286,  363 => 276,  333 => 249,  300 => 219,  255 => 177,  218 => 143,  182 => 110,  175 => 106,  168 => 102,  158 => 95,  148 => 88,  123 => 66,  119 => 65,  115 => 64,  111 => 63,  107 => 62,  103 => 61,  99 => 60,  95 => 59,  91 => 58,  88 => 57,  35 => 5,  33 => 4,  30 => 3,);
    }
}
