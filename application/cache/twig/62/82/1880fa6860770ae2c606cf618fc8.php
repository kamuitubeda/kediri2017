<?php

/* pengadaan/fragments/1e.html */
class __TwigTemplate_62821880fa6860770ae2c606cf618fc8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"modal fade\" id=\"1e\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
\t<div class=\"modal-dialog modal-lg\">
\t\t<div class=\"modal-content\">\t\t\t\t
\t\t\t<div class=\"modal-header\">
\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
\t\t\t\t<h4 class=\"modal-title\" id=\"myModalLabel\">Input Mutasi Tambah - Jalan, Jembatan, Jaringan, dan Instalasi</h4>
\t\t\t</div>
\t\t\t<div class=\"modal-body\">
\t\t\t\t<div id=\"error1e\" class=\"error\" style=\"display:none;\"></div>
\t\t\t\t";
        // line 10
        echo form_open("#");
        echo "
\t\t\t\t<ul class=\"nav nav-tabs\" role=\"tablist\">
\t\t\t\t\t<li role=\"presentation\" class=\"active\"><a href=\"#1e_p1\" aria-controls=\"P.1\" role=\"tab\" data-toggle=\"tab\">P.1</a></li>
\t\t\t\t\t<li role=\"presentation\"><a href=\"#1e_p2\" aria-controls=\"P.2\" role=\"tab\" data-toggle=\"tab\">P.2</a></li>
\t\t\t\t</ul>
\t\t\t\t<div class=\"tab-content\">
\t\t\t\t\t<div class=\"tab-pane fade in active\" id=\"1e_p1\">
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t\t\t\t<label class=\"control-label pull-right\">No. Status Penggunaan</label>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-5\">
\t\t\t\t\t\t\t\t<input class=\"form-control\" name=\"kbstatusguna\"></input>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t\t\t\t<label class=\"control-label pull-right\">Kode Sub-Sub Kel</label>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t\t\t\t<select class=\"form-control\" name=\"kbkodesubsubkel\" type=\"text\" dropdown-width=\"800\"></select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t\t\t\t<label class=\"control-label pull-right\">Kode Rek.Neraca</label>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t\t\t\t<input class=\"form-control\" name=\"kbkoderekneraca\"></input>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t\t\t\t<label class=\"control-label pull-right\">Nama Barang</label>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-6\">
\t\t\t\t\t\t\t\t<input class=\"form-control\" name=\"kbnamabarang\"></input>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t\t\t\t<label class=\"control-label pull-right\">Alamat</label>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-6\">
\t\t\t\t\t\t\t\t<input class=\"form-control\" name=\"kbalamat\"></input>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t\t\t\t<label class=\"control-label pull-right\">Tipe</label>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-6\">
\t\t\t\t\t\t\t\t<input class=\"form-control\" name=\"kbtipe\"></input>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t\t\t\t<label class=\"control-label pull-right\">Jumlah</label>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-2\">
\t\t\t\t\t\t\t\t<input class=\"form-control\" name=\"kbjumlah\"></input>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-1\">
\t\t\t\t\t\t\t\t<label class=\"control-label pull-right\">Satuan</label>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-2\">
\t\t\t\t\t\t\t\t<input class=\"form-control\" name=\"kbsatuan\"></input>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>

\t\t\t\t\t\t<hr />

\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t\t\t\t<label class=\"control-label pull-right\">Harga Satuan</label>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-4\">
\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\">Rp.</span>
\t\t\t\t\t\t\t\t\t<input class=\"form-control\" name=\"kbhargasatuan\" style=\"text-align:right;\" />
\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\">,00</span>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t\t\t\t<label class=\"control-label pull-right\">Harga Total</label>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-4\">
\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\">Rp.</span>
\t\t\t\t\t\t\t\t\t<input class=\"form-control\" name=\"kbhargatotal\" style=\"text-align:right;\" readonly />
\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\">.00</span>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-2\">
\t\t\t\t\t\t\t\t<label class=\"control-label\">
\t\t\t\t\t\t\t\t\t<input type=\"checkbox\" name=\"kbpajak\" value=\"\"> Pajak
\t\t\t\t\t\t\t\t</label>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t\t\t\t<label class=\"control-label pull-right\">Harga Total + Pajak</label>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-4\">
\t\t\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\">Rp.</span>
\t\t\t\t\t\t\t\t\t<input class=\"form-control\" name=\"kbhargatotalpajak\" style=\"text-align:right;\" />
\t\t\t\t\t\t\t\t\t<span class=\"input-group-addon\">,00</span>
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t\t\t\t<label class=\"control-label pull-right\">Rencana Alokasi</label>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-6\">
\t\t\t\t\t\t\t\t<input class=\"form-control\" name=\"kbrencanaalokasi\"></input>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t\t\t\t<label class=\"control-label pull-right\">Keterangan</label>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-6\">
\t\t\t\t\t\t\t\t<input class=\"form-control\" name=\"kbketerangan\"></input>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"tab-pane fade in\" id=\"1e_p2\">
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t\t\t\t<label class=\"control-label pull-right\">Jenis Bahan</label>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-4\">
\t\t\t\t\t\t\t\t<select class=\"form-control\" name=\"kbbahan\">
\t\t\t\t\t\t\t\t\t<option></option>
\t\t\t\t\t\t\t\t\t<option value=\"Aspal\">Aspal</option>
\t\t\t\t\t\t\t\t\t<option value=\"Bailey\">Bailey</option>
\t\t\t\t\t\t\t\t\t<option value=\"Beton\">Beton</option>
\t\t\t\t\t\t\t\t\t<option value=\"Kayu\">Kayu</option>
\t\t\t\t\t\t\t\t\t<option value=\"Pasangan Batu\">Pasangan Batu</option>
\t\t\t\t\t\t\t\t\t<option value=\"Besi\">Besi</option>
\t\t\t\t\t\t\t\t\t<option value=\"Baja\">Baja</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t\t\t\t<label class=\"control-label pull-right\">Jenis Konstruksi</label>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-4\">
\t\t\t\t\t\t\t\t<select class=\"form-control\" name=\"kbjeniskonstruksi\">
\t\t\t\t\t\t\t\t\t<option></option>
\t\t\t\t\t\t\t\t\t<option value=\"Permanen\">Permanen</option>
\t\t\t\t\t\t\t\t\t<option value=\"Semi Permanen\">Semi Permanen</option>
\t\t\t\t\t\t\t\t\t<option value=\"Darurat\">Darurat</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t\t\t\t<label class=\"control-label pull-right\">Nama Ruas</label>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-4\">
\t\t\t\t\t\t\t\t<input class=\"form-control\" name=\"kbnamaruas\"></input>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t\t\t\t<label class=\"control-label pull-right\">Pangkal Ruas</label>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-4\">
\t\t\t\t\t\t\t\t<input class=\"form-control\" name=\"kbpangkalruas\"></input>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t\t\t\t<label class=\"control-label pull-right\">Ujung Ruas</label>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-4\">
\t\t\t\t\t\t\t\t<input class=\"form-control\" name=\"kbujungruas\"></input>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t\t\t\t<label class=\"control-label pull-right\">Panjang</label>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-1\">
\t\t\t\t\t\t\t\t<input class=\"form-control\" name=\"kbpanjang\"></input>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-1\" style=\"padding-top:10px;\">
\t\t\t\t\t\t\t\tM
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t\t\t\t<label class=\"control-label pull-right\">Lebar</label>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-1\">
\t\t\t\t\t\t\t\t<input class=\"form-control\" name=\"kblebar\"></input>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-1\" style=\"padding-top:10px;\">
\t\t\t\t\t\t\t\tM
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t\t\t\t<label class=\"control-label pull-right\">Luas</label>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-1\">
\t\t\t\t\t\t\t\t<input class=\"form-control\" name=\"kbluasbangunan\"></input>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"col-xs-1\" style=\"padding-top:10px;\">
\t\t\t\t\t\t\t\tM2
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t";
        // line 230
        echo form_close();
        echo "
\t\t\t</div>
\t\t\t<div class=\"modal-footer\">
\t\t\t\t<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Tutup</button>
\t\t\t\t<button type=\"button\" id=\"btn1e\" class=\"btn btn-primary\">Simpan</button>
\t\t\t</div>
\t\t</div>
\t</div>
</div>

<script type=\"text/javascript\">
\tjQuery(function(\$) {
\t\tvar root = \$(\"#1e\");

\t\troot.find(\"button[id=btn1e]\").click(function() {
\t\t\tif (!form.valid()) {
\t\t\t\troot.find(\".modal-body\").animate({scrollTop:0});
\t\t\t\treturn;
\t\t\t}

\t\t\tvar kbid = root.data(\"kbid\");

\t\t\t\$.post(\"";
        // line 252
        echo twig_escape_filter($this->env, site_url("pengadaan/submit_rincian_1e"), "html", null, true);
        echo "\", {
\t\t\t\tkbstatusguna: root.find(\"input[name=kbstatusguna]\").val(),
\t\t\t\tkbkodesubsubkel: root.find(\"select[name=kbkodesubsubkel]\").val(),
\t\t\t\tkbkoderekneraca: root.find(\"input[name=kbkoderekneraca]\").val(),
\t\t\t\tkbnamabarang: root.find(\"input[name=kbnamabarang]\").val(),
\t\t\t\tkbalamat: root.find(\"input[name=kbalamat]\").val(),
\t\t\t\tkbtipe: root.find(\"input[name=kbtipe]\").val(),
\t\t\t\tkbjumlah: root.find(\"input[name=kbjumlah]\").val(),
\t\t\t\tkbsatuan: root.find(\"input[name=kbsatuan]\").val(),
\t\t\t\tkbhargasatuan: root.find(\"input[name=kbhargasatuan]\").val(),
\t\t\t\tkbhargatotal: root.find(\"input[name=kbhargatotal]\").val(),
\t\t\t\tkbpajak: root.find(\"input[name=kbpajak]\").is(\":checked\") ? 1 : 0,
\t\t\t\tkbhargatotalpajak: root.find(\"input[name=kbhargatotalpajak]\").val(),
\t\t\t\tkbrencanaalokasi: root.find(\"input[name=kbrencanaalokasi]\").val(),
\t\t\t\tkbketerangan: root.find(\"input[name=kbketerangan]\").val(),
\t\t\t\tkbbahan: root.find(\"select[name=kbbahan]\").val(),
\t\t\t\tkbjeniskonstruksi: root.find(\"select[name=kbjeniskonstruksi]\").val(),
\t\t\t\tkbnamaruas: root.find(\"input[name=kbnamaruas]\").val(),
\t\t\t\tkbpangkalruas: root.find(\"input[name=kbpangkalruas]\").val(),
\t\t\t\tkbujungruas: root.find(\"input[name=kbujungruas]\").val(),
\t\t\t\tkbpanjang: root.find(\"select[name=kbpanjang]\").val(),
\t\t\t\tkblebar: root.find(\"select[name=kblebarbangunan]\").val(),
\t\t\t\tkbluasbangunan: root.find(\"select[name=kbluasbangunan]\").val(),
\t\t\t\tjpid: ";
        // line 275
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : null), "html", null, true);
        echo ",
\t\t\t\tkbid: kbid
\t\t\t}, function(response) {
\t\t\t\troot.modal('hide');

\t\t\t\tvar get_rincian1 = \$.common.CallbackManager.getByName(\"get_rincian1\");
\t\t\t\tif (get_rincian1) {
\t\t\t\t\tget_rincian1.fire();
\t\t\t\t}
\t\t\t}, \"json\");
\t\t});


\t\t/* ==================== validation ==================== */
\t\tvar form = root.find(\"form\");
\t\tform.validate({
\t\t\trules: {
\t\t\t\tkbkodesubsubkel: {
\t\t\t\t\trequired: true
\t\t\t\t},
\t\t\t\tkbbahan: {
\t\t\t\t\trequired: true
\t\t\t\t},
\t\t\t\tkbjeniskonstruksi: {
\t\t\t\t\trequired: true
\t\t\t\t}
\t\t\t},
\t\t\tmessages: {
\t\t\t\tkbkodesubsubkel: {
\t\t\t\t\trequired: \"Kode Sub-Sub Kel harus diisi!\"
\t\t\t\t},
\t\t\t\tkbbahan: {
\t\t\t\t\trequired: \"Jenis bahan harus diisi!\"
\t\t\t\t},
\t\t\t\tkbjeniskonstruksi: {
\t\t\t\t\trequired: \"Jenis konstruksi harus diisi!\"
\t\t\t\t}
\t\t\t},
\t\t\terrorLabelContainer: \"#error1e\",
\t\t\tignore: []
\t\t});
\t});
</script>";
    }

    public function getTemplateName()
    {
        return "pengadaan/fragments/1e.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  304 => 275,  278 => 252,  253 => 230,  286 => 257,  261 => 235,  236 => 213,  19 => 1,  2297 => 2018,  2277 => 2016,  2275 => 2015,  2220 => 1963,  2214 => 1960,  2205 => 1954,  2189 => 1941,  2164 => 1918,  2158 => 1916,  2152 => 1914,  2150 => 1913,  2111 => 1877,  2096 => 1865,  2089 => 1861,  2051 => 1826,  2026 => 1804,  2000 => 1781,  1971 => 1755,  1956 => 1743,  1931 => 1721,  1907 => 1700,  1832 => 1628,  1791 => 1590,  1751 => 1553,  1711 => 1516,  1665 => 1473,  1619 => 1430,  1602 => 1416,  1365 => 1182,  1321 => 1141,  1269 => 1092,  937 => 763,  829 => 658,  808 => 640,  798 => 633,  718 => 556,  632 => 473,  628 => 472,  624 => 471,  620 => 470,  616 => 469,  612 => 468,  608 => 467,  604 => 466,  598 => 464,  595 => 463,  523 => 393,  519 => 392,  515 => 391,  511 => 390,  507 => 389,  501 => 387,  498 => 386,  492 => 382,  194 => 87,  190 => 86,  183 => 82,  172 => 74,  168 => 73,  161 => 69,  149 => 62,  143 => 61,  137 => 60,  124 => 50,  120 => 49,  105 => 36,  100 => 34,  95 => 32,  93 => 31,  88 => 28,  82 => 26,  80 => 25,  74 => 24,  70 => 22,  68 => 21,  66 => 20,  64 => 19,  62 => 18,  60 => 17,  58 => 16,  56 => 15,  54 => 14,  52 => 13,  50 => 12,  48 => 11,  46 => 10,  44 => 9,  42 => 8,  40 => 7,  38 => 6,  36 => 5,  33 => 4,  30 => 10,);
    }
}
