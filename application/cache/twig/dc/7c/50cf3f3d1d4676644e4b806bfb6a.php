<?php

/* simgo/manage_user_modal.html */
class __TwigTemplate_dc7c50cf3f3d1d4676644e4b806bfb6a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"modal fade\" id=\"edit-user\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
\t<div class=\"modal-dialog modal-md\">
\t\t<div class=\"modal-content\">
\t\t\t<div class=\"modal-header\">
\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
\t\t\t\t<h4 class=\"modal-title\" id=\"myModalLabel\">Ubah Hak Akses Pengguna</h4>
\t\t\t</div>
\t\t\t<div class=\"modal-body\">
\t\t\t\t<form role=\"form\" id=\"form-user-skpd\">
\t\t\t\t\t<h2 id=\"nama\">Nama Petugas</h2><br/>
\t\t\t\t\t<input id=\"uid\" value=\"\" class=\"invisible\">
\t\t\t\t\t";
        // line 12
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["allskpd"]) ? $context["allskpd"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["skpd"]) {
            // line 13
            echo "\t\t\t\t\t\t<div class=\"checkbox\">
\t\t\t\t\t\t  <label><input type=\"checkbox\" class=\"unit-checkbox\" value=\"\" name=\"";
            // line 14
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["skpd"]) ? $context["skpd"] : null), "nomor_unit"), "html", null, true);
            echo "\" id=\"";
            echo twig_escape_filter($this->env, strtr($this->getAttribute((isset($context["skpd"]) ? $context["skpd"] : null), "nomor_unit"), array("." => "")), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["skpd"]) ? $context["skpd"] : null), "nama_unit"), "html", null, true);
            echo "</label>
\t\t\t\t\t\t</div>
\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['skpd'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 17
        echo "\t\t\t\t</form>
\t\t\t</div>
\t\t\t<div class=\"modal-footer\">
\t\t\t\t<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Batal</button>
\t\t\t\t<button type=\"button\" name=\"simpan\" class=\"btn btn-primary\">Perbarui</button>
\t\t\t</div>
\t\t</div>
\t</div>
</div>";
    }

    public function getTemplateName()
    {
        return "simgo/manage_user_modal.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  52 => 17,  39 => 14,  32 => 12,  19 => 1,  184 => 108,  163 => 90,  136 => 67,  133 => 66,  118 => 55,  115 => 54,  106 => 48,  100 => 44,  89 => 39,  81 => 34,  73 => 29,  63 => 24,  58 => 21,  54 => 20,  38 => 6,  36 => 13,  33 => 4,  30 => 3,);
    }
}
