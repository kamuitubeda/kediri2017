<?php

/* aset/pencarian_aset.html */
class __TwigTemplate_b0da2d7bd4fc1864ddfd54dcc01fa66d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("_templates/adminpage.html");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'scripts' => array($this, 'block_scripts'),
            'stylesheets' => array($this, 'block_stylesheets'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_templates/adminpage.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"container\">
\t<div class=\"col-xs-12\">
\t\t<div class=\"col-md-3 pull-left\">
\t\t\t<select type=\"text\" name=\"nomor_unit\" class=\"form-control\">
\t\t\t\t<option value=\"\">Semua SKPD</option>
\t\t\t</select>
\t\t</div>
\t</div>
\t<div class=\"clearfix\"></div>
\t<div class=\"spacer\"></div>
\t<div class=\"row\">
\t\t<div class=\"col-xs-12\">
\t\t\t<table id=\"datatable\" class=\"table table-striped table-hover\">
\t\t\t\t<thead>
\t\t\t\t\t<tr>
\t\t\t\t\t\t<th class=\"center\">NOMOR REGISTER</th>
\t\t\t\t\t\t<th class=\"center\">NAMA BARANG</th>
\t\t\t\t\t\t<th class=\"center\">MERK ALAMAT</th>
\t\t\t\t\t\t<th class=\"center\">TAHUN PENGADAAN</th>
\t\t\t\t\t\t<th class=\"center\">NOPOL</th>
\t\t\t\t\t\t<th class=\"center\">NO RANGKA</th>
\t\t\t\t\t\t<th class=\"center\">NO MESIN</th>
\t\t\t\t\t</tr>
\t\t\t\t</thead>
\t\t\t\t<tbody>
\t\t\t\t</tbody>
\t\t\t</table>
\t\t</div>
\t</div>
</div>
";
    }

    // line 37
    public function block_scripts($context, array $blocks = array())
    {
        // line 38
        $this->displayParentBlock("scripts", $context, $blocks);
        echo "
<script type=\"text/javaScript\" src=\"";
        // line 39
        echo twig_escape_filter($this->env, base_url("assets/js/jquery.loadmask.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 40
        echo twig_escape_filter($this->env, base_url("assets/js/accounting.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 41
        echo twig_escape_filter($this->env, base_url("assets/js/select2.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 42
        echo twig_escape_filter($this->env, base_url("assets/js/base64.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 43
        echo twig_escape_filter($this->env, base_url("assets/js/jquery.dataTables.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 44
        echo twig_escape_filter($this->env, base_url("assets/js/dataTables.bootstrap.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\">
\tvar table;
\tvar save_method;
jQuery(document).ready(function(\$) {
\tif (!String.prototype.format) {
\t\tString.prototype.format = function() {
\t\t\tvar args = arguments;
\t\t\treturn this.replace(/{(\\d+)}/g, function(match, number) { 
\t\t\t\treturn typeof args[number] != 'undefined' ? args[number] : match;
\t\t\t});
\t\t};
\t}

\tvar delay = (function(){
\t\tvar timer = 0;
\t\t\treturn function(callback, ms){
\t\t\tclearTimeout (timer);
\t\t\ttimer = setTimeout(callback, ms);
\t\t};
\t})();


\t// populasi selectbox skpd
\t\$.get(\"";
        // line 68
        echo twig_escape_filter($this->env, site_url("aset/list_skpd"), "html", null, true);
        echo "\", {}, function(json){ 
\t\tvar listSkpd = json.result;
\t\t
\t\t\$.each(listSkpd, function (key, value) {
\t\t\t\$(\"select[name=nomor_unit]\").append(\$('<option></option>').val(value.NOMOR_UNIT).html(value.NAMA_UNIT));
\t\t});

\t\t\$(\"select[name=nomor_unit]\").select2();
\t}, \"json\");

\tfunction cari_data() {
\t\tvar nomor_lokasi = \$(\"select[name=nomor_unit]\").val();
\t\ttable = \$('#datatable').DataTable({
\t\t\t\"scrollX\": false,
\t        \"processing\": true, //Feature control the processing indicator.
\t        \"serverSide\": true, //Feature control DataTables' server-side processing mode.
\t        \"order\": [], //Initial no order.
\t        // Load data for the table's content from an Ajax source
\t        \"ajax\": {
\t            \"url\": '";
        // line 87
        echo twig_escape_filter($this->env, site_url("aset/pencarian"), "html", null, true);
        echo "',
\t            \"type\": \"POST\",
\t            \"data\": {nomor_lokasi:nomor_lokasi},
\t            \"dataType\": \"json\"
\t        },
\t        //Set column definition initialisation properties.
\t        \"columns\": [
\t            {\"data\": \"NO_REGISTER\",width:100},
\t            {\"data\": \"NAMA_BARANG\",width:300},
\t            {\"data\": \"MERK_ALAMAT\",width:200},
\t            {\"data\": \"TAHUN_PENGADAAN\",width:50},
\t            {\"data\": \"NOPOL\",width:75},
\t            {\"data\": \"NO_RANGKA_SERI\",width:75},
\t            {\"data\": \"NO_MESIN\",width:75},
\t        ],
\t    });
\t}

\t\$(\"select[name=nomor_unit]\").change(function() {
\t\tvar table = \$('#datatable').DataTable();
\t\ttable.destroy();
\t\tcari_data();
\t});
});
</script>
";
    }

    // line 115
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 116
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 117
        echo twig_escape_filter($this->env, base_url("assets/css/select2.min.css"), "html", null, true);
        echo "\" />
<link href=\"";
        // line 118
        echo twig_escape_filter($this->env, base_url("assets/css/jquery.loadmask.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">\t
<link href=\"";
        // line 119
        echo twig_escape_filter($this->env, base_url("assets/css/jquery.dataTables.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">\t

<style type=\"text/css\">
\t.control-label{
\t\tpadding-top: 8px !important;
\t}

\t*[class^='col-'] {
\t\tpadding-left: 0px;
\t\tpadding-right: 0px;
\t}

\tselect.form-control {
\t\theight: 48px !important;
\t}

\tinput.form-control {
\t\theight: 34px !important;
\t}

\ttextarea.form-control {
\t\theight:auto !important;
\t}

\t.spacer {
\t    margin-top: 20px;
\t}
\t.select2-selection__rendered {
\t\theight: 28px;
\t}
</style>
";
    }

    public function getTemplateName()
    {
        return "aset/pencarian_aset.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  188 => 119,  184 => 118,  180 => 117,  176 => 116,  173 => 115,  143 => 87,  121 => 68,  94 => 44,  90 => 43,  86 => 42,  82 => 41,  78 => 40,  74 => 39,  70 => 38,  67 => 37,  33 => 4,  30 => 3,);
    }
}
