<?php

/* dashboard/LRA.html */
class __TwigTemplate_67ad74b28c88cf40dab3fea6d5fd08bc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("_templates/adminpage.html");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'scripts' => array($this, 'block_scripts'),
            'stylesheets' => array($this, 'block_stylesheets'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_templates/adminpage.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "
<div class=\"row\">
\t<div class=\"col-xs-12\">
\t\t<div id=\"lra-pegawai-chart\" class=\"col-xs-4\" style=\"padding-left:0px; padding-right:0px;\">
\t\t</div>
\t\t<div id=\"lra-barang-chart\" class=\"col-xs-4\" style=\"padding-left:0px; padding-right:0px;\">
\t\t</div>
\t\t<div id=\"lra-modal-chart\" class=\"col-xs-4\" style=\"padding-left:0px; padding-right:0px;\">
\t\t</div>
\t</div>
\t<div id=\"lra-pembanding-chart\" class=\"col-xs-12\" style=\"padding-left:0px; padding-right:0px; height:300vh;\">
\t</div>
\t<div id=\"realisasi-simbada-tanah-chart\" class=\"col-xs-12\" style=\"padding-left:0px; padding-right:0px; height:50vh;\">
\t</div>
\t<div id=\"realisasi-simbada-barang-chart\" class=\"col-xs-12\" style=\"padding-left:0px; padding-right:0px; height:50vh;\">
\t</div>
\t<div id=\"realisasi-simbada-gedung-chart\" class=\"col-xs-12\" style=\"padding-left:0px; padding-right:0px; height:50vh;\">
\t</div>
\t<div id=\"realisasi-simbada-jalan-chart\" class=\"col-xs-12\" style=\"padding-left:0px; padding-right:0px; height:50vh;\">
\t</div>
</div>
";
    }

    // line 28
    public function block_scripts($context, array $blocks = array())
    {
        // line 29
        $this->displayParentBlock("scripts", $context, $blocks);
        echo "
<script type=\"text/javaScript\" src=\"";
        // line 30
        echo twig_escape_filter($this->env, base_url("assets/js/jquery.loadmask.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 31
        echo twig_escape_filter($this->env, base_url("assets/js/select2.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 32
        echo twig_escape_filter($this->env, base_url("assets/js/highcharts.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\">
    jQuery(document).ready(function(\$) {

    \t\$.post(\"";
        // line 36
        echo twig_escape_filter($this->env, site_url("aset/get_realisasi_tanah"), "html", null, true);
        echo "\", {}, function(json){
    \t\tvar anggaran = json.anggaran;
    \t\tvar realisasi = json.realisasi;
    \t\tvar realisasi_simbada = json.realisasi_simbada;

    \t\tvar persen_anggaran = anggaran/anggaran*100;
    \t\tvar persen_realisasi = realisasi/anggaran*100;
    \t\tvar persen_realisasi_simbada = realisasi_simbada/anggaran*100;

    \t\t\$('#realisasi-simbada-tanah-chart').highcharts({
\t\t        chart: {
\t\t            type: 'column'
\t\t        },
\t\t        title: {
\t\t            text: 'Komparasi Realisasi Tanah'
\t\t        },
\t\t        xAxis: {
\t\t            categories: ['Dinas Pendidikan'],
\t\t            title: {
\t\t                text: null
\t\t            }
\t\t        },
\t\t        yAxis: {
\t\t            min: 0,
\t\t            title: {
\t\t                text: 'Jumlah',
\t\t                align: 'high'
\t\t            },
\t\t            labels: {
\t\t                overflow: 'justify'
\t\t            }
\t\t        },
\t\t        tooltip: {
\t\t            valueSuffix: ' persen'
\t\t        },
\t\t        plotOptions: {
\t\t            bar: {
\t\t                dataLabels: {
\t\t                    enabled: true
\t\t                }
\t\t            }
\t\t        },
\t\t        legend: {
\t\t            layout: 'vertical',
\t\t            align: 'right',
\t\t            verticalAlign: 'top',
\t\t            x: -40,
\t\t            y: 80,
\t\t            floating: true,
\t\t            borderWidth: 1,
\t\t            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
\t\t            shadow: true
\t\t        },
\t\t        credits: {
\t\t            enabled: false
\t\t        },
\t\t        series: [{
\t\t            name: 'Anggaran',
\t\t            data: [persen_anggaran]
\t\t        }, {
\t\t            name: 'Realisasi',
\t\t            data: [persen_realisasi]
\t\t        }, {
\t\t            name: 'Realisasi Simbada',
\t\t            data: [persen_realisasi_simbada]
\t\t        }]
\t\t    });
\t\t}, \"json\");

\t\t\$.post(\"";
        // line 105
        echo twig_escape_filter($this->env, site_url("aset/get_realisasi_barang"), "html", null, true);
        echo "\", {}, function(json){
    \t\tvar anggaran = json.anggaran;
    \t\tvar realisasi = json.realisasi;
    \t\tvar realisasi_simbada = json.realisasi_simbada;

    \t\tvar persen_anggaran = anggaran/anggaran*100;
    \t\tvar persen_realisasi = realisasi/anggaran*100;
    \t\tvar persen_realisasi_simbada = realisasi_simbada/anggaran*100;

    \t\t\$('#realisasi-simbada-barang-chart').highcharts({
\t\t        chart: {
\t\t            type: 'column'
\t\t        },
\t\t        title: {
\t\t            text: 'Komparasi Realisasi Peralatan dan Mesin'
\t\t        },
\t\t        xAxis: {
\t\t            categories: ['Dinas Pendidikan'],
\t\t            title: {
\t\t                text: null
\t\t            }
\t\t        },
\t\t        yAxis: {
\t\t            min: 0,
\t\t            title: {
\t\t                text: 'Jumlah',
\t\t                align: 'high'
\t\t            },
\t\t            labels: {
\t\t                overflow: 'justify'
\t\t            }
\t\t        },
\t\t        tooltip: {
\t\t            valueSuffix: ' persen'
\t\t        },
\t\t        plotOptions: {
\t\t            bar: {
\t\t                dataLabels: {
\t\t                    enabled: true
\t\t                }
\t\t            }
\t\t        },
\t\t        legend: {
\t\t            layout: 'vertical',
\t\t            align: 'right',
\t\t            verticalAlign: 'top',
\t\t            x: -40,
\t\t            y: 80,
\t\t            floating: true,
\t\t            borderWidth: 1,
\t\t            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
\t\t            shadow: true
\t\t        },
\t\t        credits: {
\t\t            enabled: false
\t\t        },
\t\t        series: [{
\t\t            name: 'Anggaran',
\t\t            data: [persen_anggaran]
\t\t        }, {
\t\t            name: 'Realisasi',
\t\t            data: [persen_realisasi]
\t\t        }, {
\t\t            name: 'Realisasi Simbada',
\t\t            data: [persen_realisasi_simbada]
\t\t        }]
\t\t    });
\t\t}, \"json\");

\t\t\$.post(\"";
        // line 174
        echo twig_escape_filter($this->env, site_url("aset/get_realisasi_gedung"), "html", null, true);
        echo "\", {}, function(json){
    \t\tvar anggaran = json.anggaran;
    \t\tvar realisasi = json.realisasi;
    \t\tvar realisasi_simbada = json.realisasi_simbada;

    \t\tvar persen_anggaran = anggaran/anggaran*100;
    \t\tvar persen_realisasi = realisasi/anggaran*100;
    \t\tvar persen_realisasi_simbada = realisasi_simbada/anggaran*100;

    \t\t\$('#realisasi-simbada-gedung-chart').highcharts({
\t\t        chart: {
\t\t            type: 'column'
\t\t        },
\t\t        title: {
\t\t            text: 'Komparasi Realisasi Gedung'
\t\t        },
\t\t        xAxis: {
\t\t            categories: ['Dinas Pendidikan'],
\t\t            title: {
\t\t                text: null
\t\t            }
\t\t        },
\t\t        yAxis: {
\t\t            min: 0,
\t\t            title: {
\t\t                text: 'Jumlah',
\t\t                align: 'high'
\t\t            },
\t\t            labels: {
\t\t                overflow: 'justify'
\t\t            }
\t\t        },
\t\t        tooltip: {
\t\t            valueSuffix: ' persen'
\t\t        },
\t\t        plotOptions: {
\t\t            bar: {
\t\t                dataLabels: {
\t\t                    enabled: true
\t\t                }
\t\t            }
\t\t        },
\t\t        legend: {
\t\t            layout: 'vertical',
\t\t            align: 'right',
\t\t            verticalAlign: 'top',
\t\t            x: -40,
\t\t            y: 80,
\t\t            floating: true,
\t\t            borderWidth: 1,
\t\t            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
\t\t            shadow: true
\t\t        },
\t\t        credits: {
\t\t            enabled: false
\t\t        },
\t\t        series: [{
\t\t            name: 'Anggaran',
\t\t            data: [persen_anggaran]
\t\t        }, {
\t\t            name: 'Realisasi',
\t\t            data: [persen_realisasi]
\t\t        }, {
\t\t            name: 'Realisasi Simbada',
\t\t            data: [persen_realisasi_simbada]
\t\t        }]
\t\t    });
\t\t}, \"json\");

\t\t\$.post(\"";
        // line 243
        echo twig_escape_filter($this->env, site_url("aset/get_realisasi_jalan"), "html", null, true);
        echo "\", {}, function(json){
    \t\tvar anggaran = json.anggaran;
    \t\tvar realisasi = json.realisasi;
    \t\tvar realisasi_simbada = json.realisasi_simbada;

    \t\tvar persen_anggaran = anggaran/anggaran*100;
    \t\tvar persen_realisasi = realisasi/anggaran*100;
    \t\tvar persen_realisasi_simbada = realisasi_simbada/anggaran*100;

    \t\t\$('#realisasi-simbada-jalan-chart').highcharts({
\t\t        chart: {
\t\t            type: 'column'
\t\t        },
\t\t        title: {
\t\t            text: 'Komparasi Realisasi Jalan'
\t\t        },
\t\t        xAxis: {
\t\t            categories: ['Dinas Pendidikan'],
\t\t            title: {
\t\t                text: null
\t\t            }
\t\t        },
\t\t        yAxis: {
\t\t            min: 0,
\t\t            title: {
\t\t                text: 'Jumlah',
\t\t                align: 'high'
\t\t            },
\t\t            labels: {
\t\t                overflow: 'justify'
\t\t            }
\t\t        },
\t\t        tooltip: {
\t\t            valueSuffix: ' persen'
\t\t        },
\t\t        plotOptions: {
\t\t            bar: {
\t\t                dataLabels: {
\t\t                    enabled: true
\t\t                }
\t\t            }
\t\t        },
\t\t        legend: {
\t\t            layout: 'vertical',
\t\t            align: 'right',
\t\t            verticalAlign: 'top',
\t\t            x: -40,
\t\t            y: 80,
\t\t            floating: true,
\t\t            borderWidth: 1,
\t\t            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
\t\t            shadow: true
\t\t        },
\t\t        credits: {
\t\t            enabled: false
\t\t        },
\t\t        series: [{
\t\t            name: 'Anggaran',
\t\t            data: [persen_anggaran]
\t\t        }, {
\t\t            name: 'Realisasi',
\t\t            data: [persen_realisasi]
\t\t        }, {
\t\t            name: 'Realisasi Simbada',
\t\t            data: [persen_realisasi_simbada]
\t\t        }]
\t\t    });
\t\t}, \"json\");

    \t\$('#lra-pegawai-chart').highcharts({
\t        chart: {
\t            plotBackgroundColor: null,
\t            plotBorderWidth: null,
\t            plotShadow: false,
\t            type: 'pie'
\t        },
\t        title: {
\t            text: 'Realisasi Anggaran Pegawai'
\t        },
\t        tooltip: {
\t            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
\t        },
\t        plotOptions: {
\t            pie: {
\t            \tsize: '60%',
\t                allowPointSelect: true,
\t                cursor: 'pointer',
\t                dataLabels: {
\t                    enabled: true,
\t                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
\t                    style: {
\t                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
\t                    }
\t                }
\t            }
\t        },
\t        series: [{
\t            name: 'Anggaran',
\t            colorByPoint: true,
\t            data: [{
\t                name: 'Sisa',
\t                y: 9.85
\t            }, {
\t                name: 'Realisasi',
\t                y: 90.15,
\t                sliced: true,
\t                selected: true
\t            }]
\t        }]
\t    });

\t\t\$('#lra-barang-chart').highcharts({
\t        chart: {
\t            plotBackgroundColor: null,
\t            plotBorderWidth: null,
\t            plotShadow: false,
\t            type: 'pie'
\t        },
\t        title: {
\t            text: 'Realisasi Anggaran Barang & Jasa'
\t        },
\t        tooltip: {
\t            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
\t        },
\t        plotOptions: {
\t            pie: {
\t            \tsize: '60%',
\t                allowPointSelect: true,
\t                cursor: 'pointer',
\t                dataLabels: {
\t                    enabled: true,
\t                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
\t                    style: {
\t                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
\t                    }
\t                }
\t            }
\t        },
\t        series: [{
\t            name: 'Anggaran',
\t            colorByPoint: true,
\t            data: [{
\t                name: 'Sisa',
\t                y: 36.15
\t            }, {
\t                name: 'Realisasi',
\t                y: 63.85,
\t                sliced: true,
\t                selected: true
\t            }]
\t        }]
\t    });

\t\t\$('#lra-modal-chart').highcharts({
\t        chart: {
\t            plotBackgroundColor: null,
\t            plotBorderWidth: null,
\t            plotShadow: false,
\t            type: 'pie'
\t        },
\t        title: {
\t            text: 'Realisasi Anggaran Modal'
\t        },
\t        tooltip: {
\t            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
\t        },
\t        plotOptions: {
\t            pie: {
\t            \tsize: '60%',
\t                allowPointSelect: true,
\t                cursor: 'pointer',
\t                dataLabels: {
\t                    enabled: true,
\t                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
\t                    style: {
\t                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
\t                    }
\t                }
\t            }
\t        },
\t        series: [{
\t            name: 'Anggaran',
\t            colorByPoint: true,
\t            data: [{
\t                name: 'Sisa',
\t                y: 40.15
\t            }, {
\t                name: 'Realisasi',
\t                y: 59.85,
\t                sliced: true,
\t                selected: true
\t            }]
\t        }]
\t    });



\t    \$('#lra-pembanding-chart').highcharts({
\t        chart: {
\t            type: 'bar'
\t        },
\t        title: {
\t            text: 'Perbandingan LRA antar SKPD'
\t        },
\t        xAxis: {
\t            categories: ['Dinas Pendidikan', 'Dinas Kesehatan', 'RSUD. Prof. Dr. Soekandar', 'RSUD RA. Basoeni', 'Dinas PU Pengairan', 'Dinas PU Bina Marga', 'Dinas PU Cipta Karya dan Tata Ruang', 'BAPPEDA', 'Dinas Perhubungan, Komunikasi, dan Informatika', 'Badan Lingkungan Hidup', 'Dinas Kependudukan dan Catatan Sipil', 'Badan Pemberdayaan Perempuan dan KB', 'Dinas Sosial', 'Dinas Tenaga Kerja dan Transmigrasi', 'Dinas Kooperasi dan UKM', 'Dinas Pemuda, Olahraga, Kebudayaan, dan Pariwisata', 'Badan Kesatuan Bangsa dan Politik', 'Kepala Daerah dan WKDh', 'DPRD', 'Sekretariat Daerah', 'Sekretariat DPRD', 'Badan Perijinan Terpadu dan Penanaman Modal', 'Badan Kepegawaian, Pendidikan dan Pelatihan', 'Inspektorat Kabupaten', 'Satuan Polisi Pamong Praja', 'Kecamatan Sooko', 'Kecamatan Trowulan', 'Kecamatan Puri', 'Kecamatan Bangsal', 'Kecamatan Gedeg', 'Kecamatan Kemlagi', 'Kecamatan Jetis', 'Kecamatan Dawarblandong', 'Kecamatan Mojosari', 'Kecamatan Pungging', 'Kecamatan Ngoro', 'Kecamatan Kutorejo', 'Kecamatan Dlanggu', 'Kecamatan Gondang', 'Kecamatan Jatirejo', 'Kecamatan Trawas', 'Kecamatan Pacet', 'Kecamatan Mojoanyar', 'Badan Pengelolaan Keuangan dan Aset', 'Badan Penanggulangan Bencana Daerah', 'Dinas Pendapatan ', 'Kantor Ketahanan Pangan', 'Badan Pemberdayaan Masyarakat', 'Kantor Perpustakaan, Arsip, dan Dokumentasi', 'Dinas Pertanian', 'Dinas Peternakan dan Perikanan', 'Dinas Kehutanan dan Perkebunan', 'Dinas Perindustrian dan Perdagangan'],
\t            title: {
\t                text: null
\t            }
\t        },
\t        yAxis: {
\t            min: 0,
\t            title: {
\t                text: 'Jumlah',
\t                align: 'high'
\t            },
\t            labels: {
\t                overflow: 'justify'
\t            }
\t        },
\t        tooltip: {
\t            valueSuffix: ' persen'
\t        },
\t        plotOptions: {
\t            bar: {
\t                dataLabels: {
\t                    enabled: true
\t                }
\t            }
\t        },
\t        legend: {
\t            layout: 'vertical',
\t            align: 'right',
\t            verticalAlign: 'top',
\t            x: -40,
\t            y: 80,
\t            floating: true,
\t            borderWidth: 1,
\t            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
\t            shadow: true
\t        },
\t        credits: {
\t            enabled: false
\t        },
\t        series: [{
\t            name: 'Alokasi',
\t            data: [100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100, 100]
\t        }, {
\t            name: 'Realisasi',
\t            data: [65, 67, 82, 78, 61, 45, 57, 63, 58, 74, 62, 67, 68, 75, 84, 62, 77, 82, 65, 72, 70, 77, 67, 79, 81, 82, 75, 65, 63, 80, 71, 77, 66, 87, 76, 69, 74, 73, 70, 80, 81, 79, 77, 73, 76, 72, 71, 72, 78, 69, 83, 71, 74]
\t        }]
\t    });
\t});
</script>
";
    }

    // line 500
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 501
        echo "\t";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 502
        echo twig_escape_filter($this->env, base_url("assets/css/devexpress-like/jquery-ui.css"), "html", null, true);
        echo "\" />
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 503
        echo twig_escape_filter($this->env, base_url("assets/css/select2.min.css"), "html", null, true);
        echo "\" />
\t<link href=\"";
        // line 504
        echo twig_escape_filter($this->env, base_url("assets/css/jquery.loadmask.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">\t\t

\t<style type=\"text/css\">
\t\t.control-label{
\t\t\tpadding-top: 8px !important;
\t\t}

\t\tselect.form-control {
\t\t\theight: 48px !important;
\t\t}

\t\tinput.form-control {
\t\t\theight: 34px !important;
\t\t}

\t\ttextarea.form-control {
\t\t\theight:auto !important;
\t\t}

\t\t.row {
\t\t\tpadding:10px 0px;
\t\t}

\t\t.spacer {
\t\t    margin-top: 20px;
\t\t}

\t\t.ui-autocomplete{
\t\t\tbackground: none repeat scroll 0 0 white;
\t\t}

\t\t.input-group-addon{
\t\t\tborder-radius: 0 !important;
\t\t}

\t\t.input-group .form-control{
\t\t\tz-index: 0; !important;
\t\t}

\t\t.select2-selection__rendered {
\t\t\theight: 28px;
\t\t}
\t</style>
";
    }

    public function getTemplateName()
    {
        return "dashboard/LRA.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  571 => 504,  567 => 503,  563 => 502,  558 => 501,  555 => 500,  296 => 243,  224 => 174,  152 => 105,  80 => 36,  73 => 32,  69 => 31,  65 => 30,  61 => 29,  58 => 28,  33 => 4,  30 => 3,);
    }
}
