<?php

/* pengadaan/fragments/pilihan.html */
class __TwigTemplate_f5e64c5c929674cf0b0310166087ddff extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"modal fade\" id=\"pilihan\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
\t<div class=\"modal-dialog modal-md\">
\t\t<div class=\"modal-content\">
\t\t\t<div class=\"modal-header\">
\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
\t\t\t\t<h4 class=\"modal-title\" id=\"myModalLabel\">Pilih Penerimaan Pengadaan</h4>
\t\t\t</div>
\t\t\t<div class=\"modal-body\">
\t\t\t\t<form role=\"form\">
\t\t\t\t\t<label class=\"control-label\">Aset Tetap</label>
\t\t\t\t\t<div class=\"radio\">
\t\t\t\t\t\t<label><input type=\"radio\" name=\"optradio\" value=\"1\">1. A. Tanah</label>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"radio\">
\t\t\t\t\t\t<label><input type=\"radio\" name=\"optradio\" value=\"2\">2. B. 1. Peralatan</label>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"radio\">
\t\t\t\t\t\t<label><input type=\"radio\" name=\"optradio\" value=\"3\">3. B. 2. Alat Angkutan</label>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"radio\">
\t\t\t\t\t\t<label><input type=\"radio\" name=\"optradio\" value=\"4\">4. C. Gedung</label>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"radio\">
\t\t\t\t\t\t<label><input type=\"radio\" name=\"optradio\" value=\"5\">5. D. Jalan, Jembatan, Jaringan, dan Instalasi</label>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"radio\">
\t\t\t\t\t\t<label><input type=\"radio\" name=\"optradio\" value=\"6\">6. E. 1. Buku dan Barang Perpustakaan</label>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"radio\">
\t\t\t\t\t\t<label><input type=\"radio\" name=\"optradio\" value=\"7\">7. E. 2. Barang Budaya</label>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"radio\">
\t\t\t\t\t\t<label><input type=\"radio\" name=\"optradio\" value=\"8\">8. E. 3. Hewan dan Tanaman</label>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"radio\">
\t\t\t\t\t\t<label><input type=\"radio\" name=\"optradio\" value=\"9\">9. G. Aset Tidak Berwujud</label>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"radio\">
\t\t\t\t\t\t<label><input type=\"radio\" name=\"optradio\" value=\"10\">Penambahan Nilai</label>
\t\t\t\t\t</div>
\t\t\t\t\t<label class=\"control-label\">Selain Aset Tetap</label>
\t\t\t\t\t<div class=\"radio\">
\t\t\t\t\t\t<label><input type=\"radio\" name=\"optradio\" value=\"11\">Jasa</label>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"radio\">
\t\t\t\t\t\t<label><input type=\"radio\" name=\"optradio\" value=\"12\">Pakai Habis</label>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"radio\">
\t\t\t\t\t\t<label><input type=\"radio\" name=\"optradio\" value=\"13\">Bantuan</label>
\t\t\t\t\t</div>
\t\t\t\t\t<label class=\"control-label\">Progress</label>
\t\t\t\t\t<div class=\"radio\">
\t\t\t\t\t\t<label><input type=\"radio\" name=\"optradio\" value=\"14\">Konstruksi Dalam Pengerjaan - C. Gedung</label>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"radio\">
\t\t\t\t\t\t<label><input type=\"radio\" name=\"optradio\" value=\"15\">Konstruksi Dalam Pengerjaan - D. Jalan, Jembatan, Jaringan, dan Instalasi</label>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"radio\">
\t\t\t\t\t\t<label><input type=\"radio\" name=\"optradio\" value=\"16\">Konstruksi Dalam Pengerjaan - Penambahan Nilai</label>
\t\t\t\t\t</div>
\t\t\t\t</form>
\t\t\t</div>
\t\t\t<div class=\"modal-footer\">
\t\t\t\t<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Tutup</button>
\t\t\t\t<a type=\"button\" class=\"btn btn-primary tambah-mutasi\" data-toggle=\"modal\">Pilih</a>
\t\t\t</div>
\t\t</div>
\t</div>
</div>";
    }

    public function getTemplateName()
    {
        return "pengadaan/fragments/pilihan.html";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
