<?php

/* aset/index.html */
class __TwigTemplate_5bc0a8ee8fde585e64f7d87881456f62 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("_templates/adminpage.html");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'scripts' => array($this, 'block_scripts'),
            'stylesheets' => array($this, 'block_stylesheets'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_templates/adminpage.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "\t<div class=\"row\">
\t\t<div class=\"col-xs-12\">
\t\t\t<div style=\"\"></div>
\t\t</div>
\t\t<div class=\"col-xs-12\">
\t\t\t<div style=\"margin-top: 10px; margin-left:10px; margin-right:10px; background:white; height: 60px; border: 1px solid rgba(1, 1, 1, 0.25);\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-xs-3\" toolbar style=\"padding: 12px 10px;\">
\t\t\t\t\t\t<select type=\"text\" name=\"skpd-filter\" id=\"skpd-filter\" class=\"form-control ui-widget\">
\t\t\t\t\t\t\t<option value=\"\">All SKPD</option>
\t\t\t\t\t\t</select>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-xs-2\" toolbar style=\"padding: 12px 10px;\">
\t\t\t\t\t\t<select type=\"text\" id=\"tahun-filter\" class=\"form-control ui-widget\">
\t\t\t\t\t\t\t  <option value=\"\">Tahun</option>
\t\t\t\t\t\t</select>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-xs-1\" toolbar style=\"padding: 12px 10px;\">

\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-xs-3\" toolbar style=\"padding: 12px 10px;\">
\t\t\t\t\t\t<input type=\"checkbox\" name=\"my-checkbox\" data-on-text=\"Titik\" data-off-text=\"Lintasan\" data-handle-width=\"60\" />
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-xs-3\" toolbar style=\"padding: 12px 10px;\">
\t\t\t\t\t\t<input type=\"text\" id=\"pencarian\" class=\"form-control ui-widget\" placeholder=\"Pencarian\" />
\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-xs-12\" style=\"padding:5px 10px 5px 10px; background:white; height: 35px; border: 1px solid rgba(1, 1, 1, 0.25);\">
\t\t\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t\t\t<div class=\"col-xs-3\" toolbar>
\t\t\t\t\t\t\t\t\t\t<input kib=\"01\" name=\"filter\" type=\"checkbox\" checked /> Tanah
\t\t\t\t\t\t\t\t\t</div>
<!-- \t\t\t\t\t\t\t\t\t<div class=\"col-xs-3\" toolbar>
\t\t\t\t\t\t\t\t\t\t<input kib=\"02\"name=\"filter\" type=\"checkbox\" checked /> Peralatan dan Mesin
\t\t\t\t\t\t\t\t\t</div> -->
\t\t\t\t\t\t\t\t\t<div class=\"col-xs-3\" toolbar>
\t\t\t\t\t\t\t\t\t\t<input kib=\"03\"name=\"filter\" type=\"checkbox\" checked /> Gedung dan Bangunan
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"col-xs-3\" toolbar>
\t\t\t\t\t\t\t\t\t\t<input kib=\"04\"name=\"filter\" type=\"checkbox\" checked /> Jalan, Irigasi, dan Jaringan
\t\t\t\t\t\t\t\t\t</div>
<!-- \t\t\t\t\t\t\t\t\t<div class=\"col-xs-3\" toolbar>
\t\t\t\t\t\t\t\t\t\t<input kib=\"05\"name=\"filter\" type=\"checkbox\" checked /> Aset Tetap Lainnya
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"col-xs-3\" toolbar>
\t\t\t\t\t\t\t\t\t\t<input kib=\"06\"name=\"filter\" type=\"checkbox\" checked /> Konstruksi Dalam Pengerjaan
\t\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t\t\t<div class=\"col-xs-3\" toolbar>
\t\t\t\t\t\t\t\t\t\t<input kib=\"07\"name=\"filter\" type=\"checkbox\" checked /> Aset Tidak Berwujud
\t\t\t\t\t\t\t\t\t</div> -->
\t\t\t\t\t\t\t\t\t<!-- <div class=\"col-xs-3\" toolbar>
\t\t\t\t\t\t\t\t\t\t<input kib=\"08\" name=\"filter\" type=\"checkbox\" checked /> Kantor
\t\t\t\t\t\t\t\t\t</div> -->
\t\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"col-xs-12\">
\t\t\t<div id=\"map-canvas\"></div>
\t\t</div>
\t\t<div id=\"dialog\" title=\"Silakan Masukkan Posisi Aset\">
\t\t\t<form action=\"\" method=\"post\">
\t\t\t<table>
\t\t\t<tr style=\"border-bottom: solid transparent 5px\">
\t\t\t\t<td><label>Latitude</label></td><td><label>:</label></td>
\t\t\t\t<td><input id=\"lat\" name=\"latitude\" type=\"text\"></td>
\t\t\t</tr>
\t\t\t<tr style=\"border-bottom: solid transparent 5px\">
\t\t\t\t<td><label>Longitude</label></td><td><label>:</label></td>
\t\t\t\t<td><input id=\"long\" name=\"longitude\" type=\"text\"></td>
\t\t\t</tr>
\t\t\t<tr style=\"border-bottom: solid transparent 5px\">
\t\t\t\t<input type=\"submit\" tabindex=\"-1\" style=\"position:absolute; top:-1000px\">
<!-- \t\t\t\t<td style=\"text-align:center;\" colspan=\"3\"><input id=\"submit\" type=\"submit\" value=\"Submit\"></td> -->
\t\t\t</tr>
\t\t\t</table>
\t\t\t</form>
\t\t</div>
\t</div>
";
    }

    // line 88
    public function block_scripts($context, array $blocks = array())
    {
        // line 89
        echo "\t";
        $this->displayParentBlock("scripts", $context, $blocks);
        echo "

\t<script type=\"text/javascript\" src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyAqNNR6XcUE9Z-KSdekIhar3e2Rs6k0fNo\"></script>
\t<script type=\"text/javaScript\" src=\"http://code.jquery.com/ui/1.11.2/jquery-ui.js\"></script>
\t<script type=\"text/javaScript\" src=\"";
        // line 93
        echo twig_escape_filter($this->env, base_url("assets/js/jquery.loadmask.min.js"), "html", null, true);
        echo "\"></script>
\t<script type=\"text/javaScript\" src=\"";
        // line 94
        echo twig_escape_filter($this->env, base_url("assets/js/accounting.min.js"), "html", null, true);
        echo "\"></script>
\t<script type=\"text/javaScript\" src=\"";
        // line 95
        echo twig_escape_filter($this->env, base_url("assets/js/bootstrap-switch.min.js"), "html", null, true);
        echo "\"></script>
\t<script type=\"text/javascript\">
\t\tvar library = {};

\t\tlibrary.json = {
\t\t\treplacer: function(match, pIndent, pKey, pVal, pEnd) {
\t\t\t\tvar key = '<span class=json-key>';
\t\t\t\tvar val = '<span class=json-value>';
\t\t\t\tvar str = '<span class=json-string>';
\t\t\t\tvar r = pIndent || '';
\t\t\t\tif (pKey) r = r + key + pKey.replace(/[\": ]/g, '') + '</span>: ';
\t\t\t\tif (pVal) r = r + (pVal[0] == '\"' ? str : val) + pVal + '</span>';
\t\t\t\treturn r + (pEnd || '');
\t\t\t},
\t\t\tprettyPrint: function(obj) {
\t\t\t\tvar jsonLine = /^( *)(\"[\\w]+\": )?(\"[^\"]*\"|[\\w.+-]*)?([,[{])?\$/mg;
\t\t\t\treturn JSON.stringify(obj, null, 3)
\t\t\t\t\t.replace(/&/g, '&amp;').replace(/\\\\\"/g, '&quot;')
\t\t\t\t\t.replace(/</g, '&lt;').replace(/>/g, '&gt;')
\t\t\t\t\t.replace(jsonLine, library.json.replacer);
\t\t\t}
\t\t};
\t</script>
\t<script type=\"text/javascript\">
\t  var guid = (function() {
\t\tfunction s4() {
\t\t  \treturn Math.floor((1 + Math.random()) * 0x10000)
\t\t\t\t.toString(16)
\t\t\t\t.substring(1);
\t\t}
\t\treturn function() {
\t\t  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
\t\t\t\t s4() + '-' + s4() + s4() + s4();
\t\t};
\t  })();
\t</script>
\t<script type=\"text/javascript\">
\t\tvar markers = new Array();
\t\tvar shownMarkers = new Array();
\t\tvar polys = new Array();
\t\tvar daftarSkpd = new Array();
\t\tvar date = new Date();
\t\tvar tahun = date.getFullYear()-2;
\t\tvar location;

\t\tvar mapOptions = {
\t\t\t//center: { lat: -7.46369847, lng:112.43337572},
\t\t\tcenter: { lat: -7.51852294, lng:112.55944669},
\t\t\tzoom: 17
\t\t};

\t\tvar map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
\t\tvar selectedMarker = null;


\t\tvar poly;


\t\tfunction deleteMarker(kode, marker) {
\t\t    jQuery.post(\"";
        // line 154
        echo twig_escape_filter($this->env, site_url("aset/hapus"), "html", null, true);
        echo "\", {kode: kode}, function(json) {
\t\t    \tif (selectedMarker != null && json.success) {
\t\t    \t\tselectedMarker.setMap(null);
\t\t    \t\t//markers.splice(index, selectedMarker);
\t\t    \t}
\t\t    }, \"json\");
\t    }

\t    function gantiData(uid){
\t    \tvar table = jQuery(\"table#\" + uid);

\t    \tvar kode = table.find(\"input#kode\").val();
\t    \tvar old_kode = table.find(\"input#old_kode\").val();
\t    \tvar nama_asset = table.find(\"p#nama_asset\").html();
\t\t\tvar kib = table.find(\"p#bidang\").html();
\t\t\tvar kode_kib = table.find(\"input#kode_bidang\").val();
\t\t\tvar skpd = table.find(\"p#nama_skpd\").html();

\t    \tjQuery.post(\"";
        // line 172
        echo twig_escape_filter($this->env, site_url("aset/ubah"), "html", null, true);
        echo "\", {kode: kode, old_kode: old_kode, nama_asset: nama_asset, kib: kib, kode_kib: kode_kib, skpd:skpd, nama_skpd:nama_skpd}, function(json) {
\t\t    \tif (json.success) {
\t\t    \t\tif (selectedMarker != null) {
\t\t    \t\t\tselectedMarker.kode = kode;
\t\t    \t\t}
\t\t    \t\talert(\"Data telah diubah\");
\t\t    \t}
\t\t    \telse{
\t\t    \t\talert(\"Data yang dimasukkan sama\");
\t\t    \t}
\t\t    }, \"json\");\t
\t    }

\t\tjQuery(function(\$) {
\t\t\t\$(\"[name='my-checkbox']\").bootstrapSwitch();

\t\t\tfunction infoCallback(info_window, marker) {
\t\t\t\treturn function(e) {
\t\t\t\t\tselectedMarker = marker;

\t\t\t\t\tvar uid = marker.uid;

\t\t\t\t\t\$.post(\"";
        // line 194
        echo twig_escape_filter($this->env, site_url("aset/get"), "html", null, true);
        echo "\", {kode:marker.kode}, function(json) {
\t\t\t\t\t\tvar data = json.data;
\t\t\t\t\t\tvar imgs = json.images;

\t\t\t\t\t\tvar tmp = '';
\t\t\t\t\t\tif (imgs.length > 0){
\t\t\t\t\t\t\tfor (var i=0; i<imgs.length; i++) {
\t\t\t\t\t\t\t\ttmp += '<div class=\"col-xs-12\"><img enc_id=\"' + imgs[i].id + '\" src=\"";
        // line 201
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "/' + imgs[i].src + '\" class=\"img-responsive img-thumbnail\"></div>';
\t\t\t\t\t\t\t}
\t\t\t\t\t\t}

\t\t\t\t\t\tvar panelTab = null;
\t\t\t\t\t\tvar content = null;

\t\t\t\t\t\t//console.log(marker.kode_kib);
\t\t\t\t\t\tif(marker.kode_kib.substr(0,2) == '01'){
\t\t\t\t\t\t\tpanelTab = ['<li role=\"presentation\" class=\"active\"><a href=\"#info\" aria-controls=\"info\" role=\"tab\" data-toggle=\"tab\">Info</a></li>',
\t\t\t\t\t\t\t\t\t\t'<li role=\"presentation\"><a href=\"#foto\" aria-controls=\"foto\" role=\"tab\" data-toggle=\"tab\">Foto</a></li>']
\t\t\t\t\t\t} else if(marker.tipe === \"LINESTRING\" || marker.kode_kib.substr(0,2) != '01'){
\t\t\t\t\t\t\tpanelTab = '<li role=\"presentation\" class=\"active\"><a href=\"#info\" aria-controls=\"info\" role=\"tab\" data-toggle=\"tab\">Info</a></li>'
\t\t\t\t\t\t}

\t\t\t\t\t\tif (marker.tipe === \"POINT\") {
\t\t\t\t\t\t\tinfo_window.open(map, marker);

\t\t\t\t\t\t\tcontent = [
\t\t\t\t\t\t\t\t'<div role=\"tabpanel\">',
\t\t\t\t\t\t\t\t\t'<ul class=\"nav nav-tabs\" role=\"tablist\">',
\t\t\t\t\t\t\t\t\t\tpanelTab,
\t\t\t\t\t\t\t\t\t'</ul>',
\t\t\t\t\t\t\t\t\t'<div class=\"tab-content\">',
\t\t\t\t\t\t\t\t\t\t'<div role=\"tabpanel\" class=\"tab-pane active\" id=\"info\">',
\t\t\t\t\t\t\t\t\t\t\t'<table class=\"hasil\" style=\"height:300px; width:400px;\" id=\\'' + uid + '\\'>', 
\t\t\t\t\t\t\t\t\t\t\t\t'<input id=\"kode_bidang\" type=\"hidden\" value=' + data.kode_kib + ' />',
\t\t\t\t\t\t\t\t\t\t\t\t'<input id=\"old_kode\" type=\"hidden\" value=' + data.kode + ' />',
\t\t\t\t\t\t\t\t\t\t\t\t'<tr>',
\t\t\t\t\t\t\t\t\t\t\t\t\t'<td style=\"width:25%;\">Kode Aset:</td>',
\t\t\t\t\t\t\t\t\t\t\t\t\t'<td style=\"width:75%;\">',
\t\t\t\t\t\t\t\t\t\t\t\t\t\t'<div class=\"ui-widget\">',
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t'<input style=\"padding-left:2px;\" id=\"kode\" size=\"25\" value=\"' + data.kode + '\" />',
\t\t\t\t\t\t\t\t\t\t\t\t\t\t'</div>',
\t\t\t\t\t\t\t\t\t\t\t\t\t'</td>',
\t\t\t\t\t\t\t\t\t\t\t\t'</tr>',
\t\t\t\t\t\t\t\t\t\t\t\t'<tr>',
\t\t\t\t\t\t\t\t\t\t\t\t\t'<td style=\"width:25%;\">Nama Aset:</td>',
\t\t\t\t\t\t\t\t\t\t\t\t\t'<td style=\"width:75%;\">',
\t\t\t\t\t\t\t\t\t\t\t\t\t\t'<p style=\"padding-left:2px; margin-top: 7px;\" id=\"nama_asset\">' + data.nama_asset + '</p>',
\t\t\t\t\t\t\t\t\t\t\t\t\t'</td>',
\t\t\t\t\t\t\t\t\t\t\t\t'</tr>',
\t\t\t\t\t\t\t\t\t\t\t\t'<tr>',
\t\t\t\t\t\t\t\t\t\t\t\t\t'<td style=\"width:25%;\">KIB:</td>',
\t\t\t\t\t\t\t\t\t\t\t\t\t'<td style=\"width:75%;\">',
\t\t\t\t\t\t\t\t\t\t\t\t\t\t'<p style=\"padding-left:2px; margin-top: 7px;\" id=\"bidang\">' + data.kib + '</p>',
\t\t\t\t\t\t\t\t\t\t\t\t\t'</td>',
\t\t\t\t\t\t\t\t\t\t\t\t'</tr>',
\t\t\t\t\t\t\t\t\t\t\t\t// '</select> </td></tr>' +
\t\t\t\t\t\t\t\t\t\t\t\t'<tr>',
\t\t\t\t\t\t\t\t\t\t\t\t\t'<td style=\"width:25%;\">Latitude:</td>',
\t\t\t\t\t\t\t\t\t\t\t\t\t'<td style=\"width:75%;\">',
\t\t\t\t\t\t\t\t\t\t\t\t\t\t'<p style=\"padding-left:2px;\" id=\"latFld\">' + data.latitude + '</p>',
\t\t\t\t\t\t\t\t\t\t\t\t\t'</td>',
\t\t\t\t\t\t\t\t\t\t\t\t'</tr>',
\t\t\t\t\t\t\t\t\t\t\t\t'<tr>',
\t\t\t\t\t\t\t\t\t\t\t\t\t'<td style=\"width:25%;\">Longitude:</td>',
\t\t\t\t\t\t\t\t\t\t\t\t\t'<td style=\"width:75%;\">',
\t\t\t\t\t\t\t\t\t\t\t\t\t\t'<p style=\"padding-left:2px;\" id=\"lngFld\">' + data.longitude + '</p>',
\t\t\t\t\t\t\t\t\t\t\t\t\t'</td>',
\t\t\t\t\t\t\t\t\t\t\t\t'</tr>',
\t\t\t\t\t\t\t\t\t\t\t\t'<tr>',
\t\t\t\t\t\t\t\t\t\t\t\t\t'<td style=\"width:25%;\">SKPD:</td>',
\t\t\t\t\t\t\t\t\t\t\t\t\t'<td style=\"width:75%;\">',
\t\t\t\t\t\t\t\t\t\t\t\t\t\t'<p style=\"padding-left:2px;\" id=\"skpdFld\"> ' + data.nama_skpd + '</p>',
\t\t\t\t\t\t\t\t\t\t\t\t\t'</td>',
\t\t\t\t\t\t\t\t\t\t\t\t'</tr>',
\t\t\t\t\t\t\t\t\t\t\t\t'<tr>',
\t\t\t\t\t\t\t\t\t\t\t\t\t'<td colspan=\"2\" style=\"text-align:center;\">',
\t\t\t\t\t\t\t\t\t\t\t\t\t\t'<input id=\"updateBtn\" onclick=\"gantiData(\\'' + uid + '\\')\" type=\"button\" style=\"width:25%;\" value=\"Update\"/>&nbsp;&nbsp;',
\t\t\t\t\t\t\t\t\t\t\t\t\t\t'<input id=\"deleteBtn\" type=\"button\" style=\"width:25%;\" onclick=\"deleteMarker(\\'' + data.kode + '\\')\" value=\"Delete\"/>',
\t\t\t\t\t\t\t\t\t\t\t\t\t'</td>',
\t\t\t\t\t\t\t\t\t\t\t\t'</tr>',
\t\t\t\t\t\t\t\t\t\t\t'</table>',
\t\t\t\t\t\t\t\t\t\t'</div>',
\t\t\t\t\t\t\t\t\t\t'<div role=\"tabpanel\" class=\"tab-pane\" id=\"foto\">',
\t\t\t\t\t\t\t\t\t\t\t'<div class=\"row\">',
\t\t\t\t\t\t\t\t\t\t\t\t'<div class=\"pull-left\">',
\t\t\t\t\t\t\t\t\t\t\t\t\t'<input id=\"fotoUploader\" type=\"file\" multiple />&nbsp;&nbsp;',
\t\t\t\t\t\t\t\t\t\t\t\t'</div>',
\t\t\t\t\t\t\t\t\t\t\t'</div>',
\t\t\t\t\t\t\t\t\t\t\t'<div class=\"row\">',
\t\t\t\t\t\t\t\t\t\t\t\t'<div class=\"pull-left\">',
\t\t\t\t\t\t\t\t\t\t\t\t\t'<input class=\"form-control\" id=\"bFotoUploader\" type=\"button\" value=\"Upload\" />',
\t\t\t\t\t\t\t\t\t\t\t\t'</div>',
\t\t\t\t\t\t\t\t\t\t\t'</div>',
\t\t\t\t\t\t\t\t\t\t\t'<br/>',
\t\t\t\t\t\t\t\t\t\t\t'<div class=\"row foto2\">',
\t\t\t\t\t\t\t\t\t\t\t\ttmp,
\t\t\t\t\t\t\t\t\t\t\t'</div>',
\t\t\t\t\t\t\t\t\t\t'</div>',
\t\t\t\t\t\t\t\t\t'</div>'
\t\t\t\t\t\t\t].join(\"\");
\t\t\t\t\t\t} else if (marker.tipe === \"LINESTRING\") {
\t\t\t\t\t\t\tinfo_window.setPosition(e.latLng);
\t\t\t\t\t\t\tinfo_window.open(map);

\t\t\t\t\t\t\tcontent = [
\t\t\t\t\t\t\t\t'<div role=\"tabpanel\">',
\t\t\t\t\t\t\t\t\t'<ul class=\"nav nav-tabs\" role=\"tablist\">',
\t\t\t\t\t\t\t\t\t\tpanelTab,
\t\t\t\t\t\t\t\t\t'</ul>',
\t\t\t\t\t\t\t\t\t'<div class=\"tab-content\">',
\t\t\t\t\t\t\t\t\t\t'<div role=\"tabpanel\" class=\"tab-pane active\" id=\"info\">',
\t\t\t\t\t\t\t\t\t\t\t'<table class=\"hasil\" style=\"height:300px; width:400px;\" id=\\'' + uid + '\\'>', 
\t\t\t\t\t\t\t\t\t\t\t\t'<input id=\"kode_bidang\" type=\"hidden\" value=' + data.kode_kib + ' />',
\t\t\t\t\t\t\t\t\t\t\t\t'<input id=\"old_kode\" type=\"hidden\" value=' + data.kode + ' />',
\t\t\t\t\t\t\t\t\t\t\t\t'<tr>',
\t\t\t\t\t\t\t\t\t\t\t\t\t'<td style=\"width:25%;\">Kode Aset:</td>',
\t\t\t\t\t\t\t\t\t\t\t\t\t'<td style=\"width:75%;\">',
\t\t\t\t\t\t\t\t\t\t\t\t\t\t'<div class=\"ui-widget\">',
\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t'<input style=\"padding-left:2px;\" id=\"kode\" size=\"25\" value=\"' + data.kode + '\" />',
\t\t\t\t\t\t\t\t\t\t\t\t\t\t'</div>',
\t\t\t\t\t\t\t\t\t\t\t\t\t'</td>',
\t\t\t\t\t\t\t\t\t\t\t\t'</tr>',
\t\t\t\t\t\t\t\t\t\t\t\t'<tr>',
\t\t\t\t\t\t\t\t\t\t\t\t\t'<td style=\"width:25%;\">Nama Aset:</td>',
\t\t\t\t\t\t\t\t\t\t\t\t\t'<td style=\"width:75%;\">',
\t\t\t\t\t\t\t\t\t\t\t\t\t\t'<p style=\"padding-left:2px; margin-top: 7px;\" id=\"nama_asset\">' + data.nama_asset + '</p>',
\t\t\t\t\t\t\t\t\t\t\t\t\t'</td>',
\t\t\t\t\t\t\t\t\t\t\t\t'</tr>',
\t\t\t\t\t\t\t\t\t\t\t\t'<tr>',
\t\t\t\t\t\t\t\t\t\t\t\t\t'<td style=\"width:25%;\">KIB:</td>',
\t\t\t\t\t\t\t\t\t\t\t\t\t'<td style=\"width:75%;\">',
\t\t\t\t\t\t\t\t\t\t\t\t\t\t'<p style=\"padding-left:2px; margin-top: 7px;\" id=\"bidang\">' + data.kib + '</p>',
\t\t\t\t\t\t\t\t\t\t\t\t\t'</td>',
\t\t\t\t\t\t\t\t\t\t\t\t'</tr>',
\t\t\t\t\t\t\t\t\t\t\t\t// '</select> </td></tr>' +
\t\t\t\t\t\t\t\t\t\t\t\t'<tr>',
\t\t\t\t\t\t\t\t\t\t\t\t\t'<td style=\"width:25%;\">Lintasan:</td>',
\t\t\t\t\t\t\t\t\t\t\t\t\t'<td style=\"width:75%;\">',
\t\t\t\t\t\t\t\t\t\t\t\t\t\t'<p style=\"padding-left:2px;\" id=\"linFld\">-</p>',
\t\t\t\t\t\t\t\t\t\t\t\t\t'</td>',
\t\t\t\t\t\t\t\t\t\t\t\t'</tr>',
\t\t\t\t\t\t\t\t\t\t\t\t'<tr>',
\t\t\t\t\t\t\t\t\t\t\t\t\t'<td style=\"width:25%;\">SKPD:</td>',
\t\t\t\t\t\t\t\t\t\t\t\t\t'<td style=\"width:75%;\">',
\t\t\t\t\t\t\t\t\t\t\t\t\t\t'<p style=\"padding-left:2px;\" id=\"skpdFld\"> ' + data.nama_skpd + '</p>',
\t\t\t\t\t\t\t\t\t\t\t\t\t'</td>',
\t\t\t\t\t\t\t\t\t\t\t\t'</tr>',
\t\t\t\t\t\t\t\t\t\t\t\t'<tr>',
\t\t\t\t\t\t\t\t\t\t\t\t\t'<td colspan=\"2\" style=\"text-align:center;\">',
\t\t\t\t\t\t\t\t\t\t\t\t\t\t'<input id=\"updateBtn\" onclick=\"gantiData(\\'' + uid + '\\')\" type=\"button\" style=\"width:25%;\" value=\"Update\"/>&nbsp;&nbsp;',
\t\t\t\t\t\t\t\t\t\t\t\t\t\t'<input id=\"deleteBtn\" type=\"button\" style=\"width:25%;\" onclick=\"deleteMarker(\\'' + data.kode + '\\')\" value=\"Delete\"/>',
\t\t\t\t\t\t\t\t\t\t\t\t\t'</td>',
\t\t\t\t\t\t\t\t\t\t\t\t'</tr>',
\t\t\t\t\t\t\t\t\t\t\t'</table>',
\t\t\t\t\t\t\t\t\t\t'</div>',
\t\t\t\t\t\t\t\t\t\t// '<div role=\"tabpanel\" class=\"tab-pane\" id=\"foto\">',
\t\t\t\t\t\t\t\t\t\t// \t'<div class=\"row\">',
\t\t\t\t\t\t\t\t\t\t// \t\t'<div class=\"pull-left\">',
\t\t\t\t\t\t\t\t\t\t// \t\t\t'<input id=\"fotoUploader\" type=\"file\" multiple />&nbsp;&nbsp;',
\t\t\t\t\t\t\t\t\t\t// \t\t'</div>',
\t\t\t\t\t\t\t\t\t\t// \t'</div>',
\t\t\t\t\t\t\t\t\t\t// \t'<div class=\"row\">',
\t\t\t\t\t\t\t\t\t\t// \t\t'<div class=\"pull-left\">',
\t\t\t\t\t\t\t\t\t\t// \t\t\t'<input class=\"form-control\" id=\"bFotoUploader\" type=\"button\" value=\"Upload\" />',
\t\t\t\t\t\t\t\t\t\t// \t\t'</div>',
\t\t\t\t\t\t\t\t\t\t// \t'</div>',
\t\t\t\t\t\t\t\t\t\t// \t'<br/>',
\t\t\t\t\t\t\t\t\t\t// \t'<div class=\"row foto2\">',
\t\t\t\t\t\t\t\t\t\t// \t\ttmp,
\t\t\t\t\t\t\t\t\t\t// \t'</div>',
\t\t\t\t\t\t\t\t\t\t// '</div>',
\t\t\t\t\t\t\t\t'</div>'
\t\t\t\t\t\t\t].join(\"\");
\t\t\t\t\t\t}

\t\t\t\t\t\t//info_window.setMap(null);

\t\t\t\t\t\tinfo_window.setContent(content);

\t\t\t\t\t\tgoogle.maps.event.addListener(info_window, 'domready', function() {
\t\t\t\t\t\t\t\$(\"input#kode\").autocomplete({
\t\t\t    \t\t\t\tminLength: 1,
\t\t\t    \t\t\t\tsource: \"";
        // line 376
        echo twig_escape_filter($this->env, site_url("aset/cari"), "html", null, true);
        echo "\",
\t\t\t    \t\t\t\tselect: function(e, u) {
\t\t\t    \t\t\t\t\t\$(this).val(u.item.id);
\t\t\t    \t\t\t\t\tvar table = \$(this).closest(\"table\");
\t\t\t    \t\t\t\t\ttable.find(\"p#nama_asset\").html(u.item.NAMA_BARANG);
\t\t\t    \t\t\t\t\ttable.find(\"p#bidang\").html(u.item.BIDANG);
\t\t\t    \t\t\t\t\treturn false;
\t\t\t    \t\t\t\t}
\t\t\t  \t\t\t\t});

\t\t\t  \t\t\t\tif (marker.tipe === \"LINESTRING\") {
\t\t\t  \t\t\t\t\tvar tmp = _.map(marker.getPath().getArray(), function(marker) { return {lat:marker.lat(), lng:marker.lng()} });
\t\t\t\t\t\t\t\t\$(\"table#\" + uid).find(\"p[id=linFld]\").html(\"<pre><code>\" + library.json.prettyPrint(tmp) + \"</code></pre>\");
\t\t\t  \t\t\t\t}


\t\t\t\t\t\t\t\$(\"#bFotoUploader\").unbind().click(function() {
\t\t\t\t\t\t\t\tvar files = \$(\"#fotoUploader\").get(0).files;
\t\t\t\t\t\t\t\tvar success = [];
\t\t\t\t\t\t\t\tvar status = false;

\t\t\t\t\t\t\t\tif (files.length > 0) {
\t\t\t\t\t\t\t\t\tfor (var i=0; i<files.length; i++) {
\t\t\t\t\t\t\t\t\t\tvar fd = new FormData();
\t\t\t\t\t\t\t\t\t\tfd.append(\"asset_id\", \$(\"#old_kode\").val());
\t\t\t\t\t\t\t\t\t\tfd.append(\"file\", files[i]);

\t\t\t\t\t\t\t\t\t\tsuccess.push(\$.ajax({
\t\t\t\t\t\t\t\t\t\t\turl: \"";
        // line 404
        echo twig_escape_filter($this->env, site_url("aset/upload_foto"), "html", null, true);
        echo "\",
\t\t\t\t\t\t\t\t\t\t\ttype: \"POST\",
\t\t\t\t\t\t\t\t\t\t\tdata: fd,
\t\t\t\t\t\t\t\t\t\t\tprocessData: false,
\t\t\t\t\t\t\t\t\t\t\tcontentType: false,
\t\t\t\t\t\t\t\t\t\t\tsuccess: function(res) {
\t\t\t\t\t\t\t\t\t\t\t\tvar json = \$.parseJSON(res);
\t\t\t\t\t\t\t\t\t\t\t\tstatus = json.status;
\t\t\t\t\t\t\t\t\t\t\t\tif (json.status === \"success\") {
\t\t\t\t\t\t\t\t\t\t\t\t\t\$(\".foto2\").append('<div class=\"col-xs-12\"><img enc_id=\"' + json.id + '\" src=\"";
        // line 413
        echo twig_escape_filter($this->env, base_url(), "html", null, true);
        echo "/' + json.src + '\" class=\"img-responsive img-thumbnail\"></div>');
\t\t\t\t\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\t\t\t\t\talert(\"Foto yang Anda upload gagal!\");
\t\t\t\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t\t\t}));

\t\t\t\t\t\t\t\t\t\t\$.when.apply(null, success).done(function() {
\t\t\t\t\t\t\t\t\t\t\tif (status === \"success\") {
\t\t\t\t\t\t\t\t\t\t\t\t\$(\"#fotoUploader\").replaceWith(jQuery(\"#fotoUploader\").val(\"\").clone(true));
\t\t\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t\t\t});
\t\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t\t} else {
\t\t\t\t\t\t\t\t\talert(\"Tidak ada foto yang dipilih\");
\t\t\t\t\t\t\t\t}
\t\t\t\t\t\t\t});
\t\t\t\t\t\t});
\t\t\t\t\t}, \"json\");
\t\t\t\t};
\t\t\t}

\t\t\tfunction skpdCallback(info_window, skpd) {
\t\t\t\treturn function() {
\t\t\t\t\tselectedMarker = skpd;
\t\t\t\t\tinfo_window.open(map, skpd);

\t\t\t\t\t\$(\"body\").mask(\"Loading\");

\t\t\t\t\tvar tahunbaru = \$('#tahun-filter option:selected').val();
\t\t\t\t\tif(tahunbaru != \"\"){
\t\t\t\t\t\ttahun = tahunbaru;
\t\t\t\t\t}

\t\t\t\t\t\$.post(\"";
        // line 447
        echo twig_escape_filter($this->env, site_url("aset/get_skpd"), "html", null, true);
        echo "\", {kode:skpd.kode, tahun:tahun}, function(json) {
\t\t\t\t\t\tvar data = json.data;
\t\t\t\t\t\tvar skpd = json.skpd;
\t\t\t\t\t\tvar asetSkpd = Array();

\t\t\t\t\t\tvar content = [
\t\t\t\t\t\t\t'<div style=\"min-width:600px; max-height:300px;\">',
\t\t\t\t\t\t\t'<table style=\"width:100%\">',
\t\t\t\t\t\t\t\t'<tr><td style=\"width:25%;\">Kode SKPD:</td><td style=\"width:75%;\"><div class=\"ui-widget\"><input style=\"padding-left:2px;\" id=\"kode\" size=\"25\" value=\"' + data.kode + '\" /></div></td></tr>',
\t\t\t\t\t\t\t\t'<tr><td style=\"width:25%;\">Nama Unit:</td><td style=\"width:75%;\"><p style=\"padding-left:2px; margin-top: 7px;\" id=\"nama_asset\" />' + \"KANTOR \" + data.nama + '</td></tr>',
\t\t\t\t\t\t\t\t'<tr><td style=\"width:25%;\">Latitude:</td> <td style=\"width:75%;\"><p style=\"padding-left:2px;\" id=\"latFld\"/> ' + data.longitude + '</td> </tr>',
\t\t\t\t\t\t\t\t'<tr><td style=\"width:25%;\">Longitude:</td> <td style=\"width:75%;\"><p style=\"padding-left:2px;\" id=\"lngFld\"/> ' + data.latitude + '</td> </tr>',
\t\t\t\t\t\t\t\t'<tr><td style=\"width:25%;\">Tahun:</td> <td style=\"width:75%;\"><p style=\"padding-left:2px;\" id=\"thnFld\"/> ' + tahun + '</td> </tr>',
\t\t\t\t\t\t\t'</table>',
\t\t\t\t\t\t\t'<br/>',
\t\t\t\t\t\t\t'<table style=\"min-width:800px;\" class=\"aset table table-striped table-bordered\">',
\t\t\t\t\t\t\t\t'<thead>',
\t\t\t\t\t\t\t\t\t'<tr>',
\t\t\t\t\t\t\t\t\t\t'<th style=\"min-width:150px\" rowspan=\"2\">Jenis Aset:</th>',
\t\t\t\t\t\t\t\t\t\t'<th colspan=\"2\">Saldo Awal</th><th colspan=\"2\">Mutasi</th><th colspan=\"2\">Penyusutan</th><th colspan=\"2\">Saldo Akhir</th>',
\t\t\t\t\t\t\t\t\t'</tr>',
\t\t\t\t\t\t\t\t\t'<tr>',
\t\t\t\t\t\t\t\t\t\t'<th style=\"min-width:90px\">Jumlah Aset</th>',
\t\t\t\t\t\t\t\t\t\t'<th style=\"min-width:150px\">Total Nilai Aset</th>',
\t\t\t\t\t\t\t\t\t\t'<th style=\"min-width:90px\">Jumlah Aset</th>',
\t\t\t\t\t\t\t\t\t\t'<th style=\"min-width:150px\">Total Nilai Aset</th>',
\t\t\t\t\t\t\t\t\t\t'<th style=\"min-width:90px\">Jumlah Aset</th>',
\t\t\t\t\t\t\t\t\t\t'<th style=\"min-width:150px\">Total Nilai Aset</th>',
\t\t\t\t\t\t\t\t\t\t'<th style=\"min-width:90px\">Jumlah Aset</th>',
\t\t\t\t\t\t\t\t\t\t'<th style=\"min-width:150px\">Total Nilai Aset</th>',
\t\t\t\t\t\t\t\t\t'</tr>',
\t\t\t\t\t\t\t\t'</thead>',
\t\t\t\t\t\t\t\t'<tbody>',
\t\t\t\t\t\t\t\t\t'<tr><td>' + skpd[0].URAIAN + '</td><td>' + skpd[0].JUMLAH + '</td><td>' + accounting.formatMoney(skpd[0].HARGA_TOTAL, \"Rp. \", 2, \".\", \",\") + '</td><td></td><td></td><td></td><td></td><td></td><td></td></tr>',
\t\t\t\t\t\t\t\t\t'<tr><td>' + skpd[1].URAIAN + '</td><td>' + skpd[1].JUMLAH + '</td><td>' + accounting.formatMoney(skpd[1].HARGA_TOTAL, \"Rp. \", 2, \".\", \",\") + '</td><td></td><td></td><td></td><td></td><td></td><td></td></tr>',
\t\t\t\t\t\t\t\t\t'<tr><td>' + skpd[2].URAIAN + '</td><td>' + skpd[2].JUMLAH + '</td><td>' + accounting.formatMoney(skpd[2].HARGA_TOTAL, \"Rp. \", 2, \".\", \",\") + '</td><td></td><td></td><td></td><td></td><td></td><td></td></tr>',
\t\t\t\t\t\t\t\t\t'<tr><td>' + skpd[3].URAIAN + '</td><td>' + skpd[3].JUMLAH + '</td><td>' + accounting.formatMoney(skpd[3].HARGA_TOTAL, \"Rp. \", 2, \".\", \",\") + '</td><td></td><td></td><td></td><td></td><td></td><td></td></tr>',
\t\t\t\t\t\t\t\t\t'<tr><td>' + skpd[4].URAIAN + '</td><td>' + skpd[4].JUMLAH + '</td><td>' + accounting.formatMoney(skpd[4].HARGA_TOTAL, \"Rp. \", 2, \".\", \",\") + '</td><td></td><td></td><td></td><td></td><td></td><td></td></tr>',
\t\t\t\t\t\t\t\t\t'<tr><td>' + skpd[5].URAIAN + '</td><td>' + skpd[5].JUMLAH + '</td><td>' + accounting.formatMoney(skpd[5].HARGA_TOTAL, \"Rp. \", 2, \".\", \",\") + '</td><td></td><td></td><td></td><td></td><td></td><td></td></tr>',
\t\t\t\t\t\t\t\t\t'<tr><td>' + skpd[6].URAIAN + '</td><td>' + skpd[6].JUMLAH + '</td><td>' + accounting.formatMoney(skpd[6].HARGA_TOTAL, \"Rp. \", 2, \".\", \",\") + '</td><td></td><td></td><td></td><td></td><td></td><td></td></tr>',
\t\t\t\t\t\t\t\t\t'<tr><td>' + skpd[7].URAIAN + '</td><td>' + skpd[7].JUMLAH + '</td><td>' + accounting.formatMoney(skpd[7].HARGA_TOTAL, \"Rp. \", 2, \".\", \",\") + '</td><td></td><td></td><td></td><td></td><td></td><td></td></tr>',
\t\t\t\t\t\t\t\t\t'<tr><td>' + skpd[8].URAIAN + '</td><td>' + skpd[8].JUMLAH + '</td><td>' + accounting.formatMoney(skpd[8].HARGA_TOTAL, \"Rp. \", 2, \".\", \",\") + '</td><td></td><td></td><td></td><td></td><td></td><td></td></tr>',
\t\t\t\t\t\t\t\t'</tbody>',
\t\t\t\t\t\t\t'</table>',
\t\t\t\t\t\t\t'</div>'
\t\t\t\t\t\t].join(\"\");

\t\t\t\t\t\tinfo_window.setContent(content);

\t\t\t\t\t\tgoogle.maps.event.addListener(info_window, 'domready', function() {
\t\t\t\t\t\t\t\$(\"input#kode\").autocomplete({
\t\t\t    \t\t\t\tminLength: 1,
\t\t\t    \t\t\t\tsource: \"";
        // line 499
        echo twig_escape_filter($this->env, site_url("aset/cari"), "html", null, true);
        echo "\",
\t\t\t    \t\t\t\tselect: function(e, u) {
\t\t\t    \t\t\t\t\t\$(this).val(u.item.id);
\t\t\t    \t\t\t\t\tvar table = \$(this).closest(\"table\");
\t\t\t    \t\t\t\t\ttable.find(\"p#nama_asset\").html(u.item.NAMA_BARANG);
\t\t\t    \t\t\t\t\ttable.find(\"p#bidang\").html(u.item.BIDANG);
\t\t\t    \t\t\t\t\treturn false;
\t\t\t    \t\t\t\t}
\t\t\t  \t\t\t\t});
\t\t\t\t\t\t});
\t\t\t\t\t\t\$(\"body\").unmask();
\t\t\t\t\t}, \"json\");
\t\t\t\t};
\t\t\t}


\t\t\tfunction loadAset(callback) {
\t\t\t\tvar url = \"";
        // line 516
        echo twig_escape_filter($this->env, site_url("aset/tampilkan"), "html", null, true);
        echo "\";

\t\t\t\t\$.get(url, {}, function(json){ 
\t\t\t\t\tvar data = json.markers;
\t\t\t\t\tvar iconBase = \"http://localhost/pantau/assets/img/marker/\";

\t\t\t\t\tfor(var i=0;i<data.length;i++) {
\t\t\t\t\t\tvar marker = null;
\t\t\t\t\t\tvar icon = data[i].kode_kib.substr(0, 2);
\t\t\t\t\t\tmarkers[i] = { kode: data[i].kode, nama_asset: data[i].nama_asset, latitude: data[i].latitude, longitude: data[i].longitude, kib: data[i].kib, kode_kib: data[i].kode_kib, skpd:data[i].skpd, nama_skpd:data[i].nama_skpd};
\t\t\t\t\t\tmarkers[i].marker = marker = new google.maps.Marker({ map: map, icon: iconBase + icon +'.png'});

\t\t\t\t\t\tvar info_window = new google.maps.InfoWindow({
\t\t\t\t\t\t\tmaxWidth: 500
\t\t\t\t\t\t});
\t\t\t\t\t\tvar uid = guid();

\t\t\t\t\t\t//hanya menampilkan tanah, bangunan, dan jalan/jembatan.
\t\t\t\t\t\tif(data[i].kode_kib.substr(0,2) == '01' || data[i].kode_kib.substr(0,2) == '03' || data[i].kode_kib.substr(0,2) == '04')
\t\t\t\t\t\t{
\t\t\t\t\t\t\tmarker.kode_kib = data[i].kode_kib;
\t\t\t\t\t\t\tmarker.uid = uid;
\t\t\t\t\t\t\tmarker.kode = String(data[i].kode);
\t\t\t\t\t\t\tmarker.setPosition(new google.maps.LatLng(data[i].latitude, data[i].longitude));
\t\t\t\t\t\t\tmarker.tipe = 'POINT';
\t\t\t\t\t\t\tgoogle.maps.event.addListener(marker,'click', infoCallback(info_window, marker));
\t\t\t\t\t\t}
\t\t\t\t\t}


\t\t\t\t\tvar data2 = json.polys;
\t\t\t\t\tfor (var i=0; i<data2.length; i++) {
\t\t\t\t\t\tvar poly = null;
\t\t\t\t\t\tpolys[i] = { 
\t\t\t\t\t\t\tkode: data2[i].kode, 
\t\t\t\t\t\t\tnama_asset: data2[i].nama_asset,
\t\t\t\t\t\t\tkib: data2[i].kib, 
\t\t\t\t\t\t\tkode_kib: data2[i].kode_kib, 
\t\t\t\t\t\t\tskpd:data2[i].skpd, 
\t\t\t\t\t\t\tnama_skpd:data2[i].nama_skpd,
\t\t\t\t\t\t\tpath: data2[i].path
\t\t\t\t\t\t};

\t\t\t\t\t\t//menambahkan lintasan
\t\t\t\t\t\tvar polyCoordinates = [];
\t\t\t\t\t\tfor (var j=0; j<data2[i].path.length; j++){
\t\t\t\t\t\t\tvar point = new google.maps.LatLng(data2[i].path[j].lat, data2[i].path[j].lng);
\t\t\t\t\t\t\tpolyCoordinates.push(point);
\t\t\t\t\t\t}

\t\t\t\t\t\tvar polyOptions = {
\t\t\t\t\t\t\tpath: polyCoordinates,
\t\t\t\t\t\t\tstrokeColor: '#000000',
\t\t\t\t\t\t\tstrokeOpacity: 1.0,
\t\t\t\t\t\t\tstrokeWeight: 3
\t\t\t\t\t\t};

\t\t\t\t\t\tpolys[i].poly = poly = new google.maps.Polyline(polyOptions); 
\t\t\t\t\t\tpoly.setMap(map);

\t\t\t\t\t\tvar info_window = new google.maps.InfoWindow({
\t\t\t\t\t\t\tmaxWidth: 500
\t\t\t\t\t\t});

\t\t\t\t\t\tinfo_window.position = polys[i].path[polys[i].path.length-1];

\t\t\t\t\t\tvar uid = guid();
\t\t\t\t\t\tpoly.uid = uid;
\t\t\t\t\t\tpoly.tipe = 'LINESTRING';
\t\t\t\t\t\tpoly.kode = String(data2[i].kode);
\t\t\t\t\t\tpoly.kode_kib = data2[i].kode_kib;
\t\t\t\t\t\t
\t\t\t\t\t\tgoogle.maps.event.addListener(poly,'click', infoCallback(info_window, poly));
\t\t\t\t\t\t//new google.maps.Marker({ map: map, icon: iconBase + icon +'.png'});
\t\t\t\t\t}

\t\t\t\t\tcallback();
\t\t\t\t}, \"json\");

\t\t\t}

\t\t\tfunction loadSkpd() {
\t\t\t\tvar url = \"";
        // line 598
        echo twig_escape_filter($this->env, site_url("aset/tampilkan_skpd"), "html", null, true);
        echo "\";

\t\t\t\t\$.get(url, {}, function(json){ 
\t\t\t\t\tvar data = json.result;
\t\t\t\t\tvar iconBase = \"http://localhost/pantau/assets/img/marker/08.png\";

\t\t\t\t\tfor(var i=0;i<data.length;i++) {
\t\t\t\t\t\tvar marker = null;
//\t\t\t\t\t\tvar icon = data[i].kode_kib.substr(0, 2);
\t\t\t\t\t\tdaftarSkpd[i] = { skpd: data[i].kode, nama_skpd: data[i].nama, latitude: data[i].latitude, longitude: data[i].longitude, aktif: data[i].aktif};
\t\t\t\t\t\tdaftarSkpd[i].marker = marker = new google.maps.Marker({ map: map, icon: iconBase});

\t\t\t\t\t\tvar info_window = new google.maps.InfoWindow({
\t\t\t\t\t\t\tmaxWidth: 700,
\t\t\t\t\t\t\tmaxHeight: 300
\t\t\t\t\t\t});
\t\t\t\t\t\tvar uid = guid();

\t\t\t\t\t\t//marker.uid = uid;
\t\t\t\t\t\tmarker.kode = String(data[i].kode);
\t\t\t\t\t\tmarker.setPosition(new google.maps.LatLng(data[i].latitude, data[i].longitude))

\t\t\t\t\t\tgoogle.maps.event.addListener(marker,'click', skpdCallback(info_window, marker));
\t\t\t\t\t}

//\t\t\t\t\tcallback();
\t\t\t\t}, \"json\");
\t\t\t}


\t\t\tvar switchOnChanged = function(event, state) {
\t\t\t\tgoogle.maps.event.clearListeners(map, 'click');
\t\t\t\tgoogle.maps.event.clearListeners(map, 'rightclick');

\t\t\t\tif (\$(this).is(\":checked\")) {
\t\t\t\t\tgoogle.maps.event.addListener(map, 'click', function(e) {
\t\t\t\t\t\taddMarker(map, e);
\t\t\t\t\t});

\t\t\t\t\t//input menggunakan latitude dan longitude
\t\t\t\t\tgoogle.maps.event.addListener(map, \"rightclick\", function(e){
\t\t\t\t\t\t\$(\"#dialog\").dialog(\"open\");
\t\t\t\t\t});
\t\t\t\t} else {
\t\t\t\t\taddPolylines(map);

\t\t\t\t\tvar isSaved = false;
\t\t\t\t\tvar info_window = new google.maps.InfoWindow({
\t\t\t\t\t\tmaxWidth: 500
\t\t\t\t\t});
\t\t\t\t\tvar uid = guid();

\t\t\t\t\tgoogle.maps.event.addListener(map, \"rightclick\", function(e){
\t\t\t\t\t\tvar path = poly.getPath();
\t\t\t\t\t\tvar paths = _.map(poly.getPath().getArray(), function(marker) { return {lat:marker.lat(), lng:marker.lng()}; });
\t\t\t\t\t\tvar pathSize = path.getLength();
\t\t\t\t\t\tvar infoLoc = new google.maps.LatLng(path.getAt(pathSize - 1).lat(), path.getAt(pathSize - 1).lng());

\t\t\t\t\t\tinfo_window.setPosition(infoLoc);
\t\t\t\t\t\tinfo_window.setContent('<div id=\"polydiv\" style=\"width:400px; max-height:320px;\">' + 
\t\t\t\t\t\t'<table class=\"hasil\" style=\"height:300px; width:400px;\" id=\\'' + uid + '\\'>' + '<input id=\"kode_bidang\" type=\"hidden\" />' +
\t\t\t\t\t\t'<tr><td style=\"width:25%;\">Kode Aset:</td><td style=\"width:75%;\"><div class=\"ui-widget\"><input style=\"padding-left:2px;\" id=\"kode\" size=\"25\" /></div></td></tr>' + 
\t\t\t\t\t\t'<tr><td style=\"width:25%;\">Nama Aset:</td><td style=\"width:75%;\"><p style=\"padding-left:2px; margin-top: 7px;\" id=\"nama_asset\">-</p></td></tr>' +
\t\t\t\t\t\t'<tr><td style=\"width:25%;\">KIB:</td><td style=\"width:75%;\"><p style=\"padding-left:2px; margin-top: 7px;\" id=\"bidang\">-</p></td></tr>' +
\t\t\t\t\t\t'<tr><td style=\"width:25%;\">Nomor Unit:</td> <td style=\"width:75%;\"><p style=\"padding-left:2px;\" id=\"skpd\"/>-</p></td> </tr>' +
\t\t\t\t\t\t'<tr><td style=\"width:25%;\">Nama Unit:</td> <td style=\"width:75%;\"><p style=\"padding-left:2px;\" id=\"nama_skpd\"/>-</p></td> </tr>' +
\t\t\t\t\t\t'</select> </td></tr>' +
\t\t\t\t\t\t'<tr><td style=\"width:25%;\">Lintasan:</td> <td style=\"width:75%;\"><p style=\"padding-left:2px;\" id=\"linFld\">-</p></td> </tr>' +
\t\t\t\t\t\t'<tr><td colspan=\"2\" style=\"text-align:center;\"><input id=\"simpanBtn\" type=\"button\" value=\"Save & Close\"/></td></tr>' +
\t\t\t\t\t\t'</div>');
\t\t\t\t\t\tinfo_window.open(map);
\t\t\t\t\t});

\t\t\t\t\tgoogle.maps.event.addListener(info_window, 'domready', function() {
\t\t\t\t\t\t\$(\"input#kode\").autocomplete({
\t\t    \t\t\t\tminLength: 1,
\t\t    \t\t\t\tsource: \"";
        // line 674
        echo twig_escape_filter($this->env, site_url("aset/cari"), "html", null, true);
        echo "\",
\t\t    \t\t\t\tselect: function(e, u) {
\t\t    \t\t\t\t\t\$(this).val(u.item.id);
\t\t    \t\t\t\t\tvar table = \$(this).closest(\"table\");
\t\t    \t\t\t\t\ttable.find(\"p#nama_asset\").html(u.item.NAMA_BARANG + (u.item.MERK_ALAMAT != null ? \", \" + u.item.MERK_ALAMAT : \"\") + (u.item.TIPE != null ? \", \" + u.item.TIPE : \"\"));
\t\t    \t\t\t\t\ttable.find(\"input#kode_bidang\").val(u.item.KODE_BIDANG);
\t\t    \t\t\t\t\ttable.find(\"p#bidang\").html(u.item.BIDANG);
\t\t    \t\t\t\t\ttable.find(\"p#skpd\").html(u.item.SKPD);
\t\t    \t\t\t\t\ttable.find(\"p#nama_skpd\").html(u.item.NAMA_SKPD);
\t\t    \t\t\t\t\treturn false;
\t\t    \t\t\t\t}
\t\t  \t\t\t\t});

\t\t\t\t\t
\t\t\t\t\t\t\$(\"#polydiv\").parent().css(\"overflow\", \"visible\");
\t\t\t\t\t\tvar tmp = _.map(poly.getPath().getArray(), function(marker) { return {lat:marker.lat(), lng:marker.lng()} });
\t\t\t\t\t\t\$(\"table#\" + uid).find(\"p[id=linFld]\").html(\"<pre><code>\" + library.json.prettyPrint(tmp) + \"</code></pre>\");


\t\t\t\t\t\t\$(\"table#\" + uid).find(\"input[id=simpanBtn]\").click(function() {
\t\t\t\t\t\t\tvar table = \$(this).closest(\"table\");
\t\t\t\t\t\t\tvar kode = table.find(\"input#kode\").val();
\t\t\t\t\t\t\tvar nama_asset = table.find(\"p#nama_asset\").html();
\t\t\t\t\t\t\tvar kib = table.find(\"p#bidang\").html();
\t\t\t\t\t\t\tvar kode_kib = table.find(\"input#kode_bidang\").val();
\t\t\t\t\t\t\tvar skpd = table.find(\"p#skpd\").html();
\t\t\t\t\t\t\tvar nama_skpd = table.find(\"p#nama_skpd\").html();
\t\t\t\t\t\t\t// var iconBase = \"http://localhost/pantau/assets/img/marker/\";
\t\t\t\t\t\t\t// var icon = kode_kib.substr(0, 2);

\t\t\t\t\t\t\tvar url = \"";
        // line 704
        echo twig_escape_filter($this->env, site_url("aset/simpan_polylines"), "html", null, true);
        echo "\";

\t\t\t\t\t\t\tif(kode == ''){
\t\t\t\t\t\t\t\talert(\"Tolong masukkan kode aset\");
\t\t\t\t\t\t\t}
\t\t\t\t\t\t\telse{
\t\t\t\t\t\t\t\t\$.post(url, {kode:kode, nama_asset: nama_asset, kib:kib, kode_kib:kode_kib, path:tmp, skpd:skpd, nama_skpd:nama_skpd}, function(r) {
\t\t\t\t\t\t\t\t\tpolys.push({ kode: kode, nama_asset: nama_asset, kib: kib, kode_kib: kode_kib, path: tmp, skpd:skpd, nama_skpd: nama_skpd, poly: poly});
\t\t\t\t\t\t\t\t\tgoogle.maps.event.addListener(marker,'click', infoCallback(info_window, marker));
\t\t\t\t\t\t\t\t\t// google.maps.event.addListener(poly, 'click', function(event) {
\t\t\t\t\t\t\t\t\t// \tinfowindow.content = \"sukses\";
\t\t\t\t\t\t\t\t\t// \tinfowindow.position = event.latLng;
\t\t\t\t\t\t\t\t\t// \tinfowindow.open(map);
\t\t\t\t\t\t\t\t\t// });
\t\t\t\t\t\t\t\t\t/*markers.push({ kode: kode, nama_asset: nama_asset, latitude: latLng.lat(), longitude: latLng.lng(), kib: kib, kode_kib: kode_kib, skpd:skpd, nama_skpd:nama_skpd, marker: marker,});
\t\t\t\t\t\t\t\t\tmarker.setIcon(iconBase + icon + '.png')
\t\t\t\t\t\t\t\t\tgoogle.maps.event.addListener(marker,'click', infoCallback(info_window, marker));*/
\t\t\t\t\t\t\t\t}, \"json\");
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\tisSaved = true;
\t\t\t\t\t\t\t\tinfo_window.close();
\t\t\t\t\t\t\t\taddPolylines(map);
\t\t\t\t\t\t\t\t\$(\"#simpanBtn\").unbind();
\t\t\t\t\t\t\t}
\t\t\t\t\t\t});
\t\t\t\t\t});

\t\t\t\t\tgoogle.maps.event.addListener(info_window, 'closeclick', function() {
\t\t\t\t\t\tif(!isSaved){
\t\t\t\t\t\t\tpoly.setMap(null);
\t\t\t\t\t\t\taddPolylines(map);
\t\t\t\t\t\t\treturn false;
\t\t\t\t\t\t}
\t\t\t\t\t\telse{
\t\t\t\t\t\t\tinfo_window.close();\t\t\t\t\t\t\t
\t\t\t\t\t\t}
\t\t\t\t\t});
\t\t\t\t}
\t\t\t}

\t\t\tfunction initialize() {
\t\t\t\t\$('input[name=\"my-checkbox\"]').on('switchChange.bootstrapSwitch', switchOnChanged);
\t\t\t\t\$('input[name=\"my-checkbox\"]').bootstrapSwitch('state', true);

\t\t\t\t//menampilkan list SKPD
\t\t\t  \tvar url = \"";
        // line 749
        echo twig_escape_filter($this->env, site_url("aset/list_skpd"), "html", null, true);
        echo "\";
\t\t\t\t\$.get(url, {}, function(json){ 
\t\t\t\t\tvar listSkpd = json.result;
\t\t\t\t\t
\t\t\t\t\t\$.each(listSkpd, function (key, value) {
            \t\t\t\$(\"#skpd-filter\").append(\$('<option></option>').val(value.NOMOR_UNIT).html(value.NAMA_UNIT));
        \t\t\t});
\t\t\t\t}, \"json\");

\t\t\t\t//menampilkan list tahun
\t\t\t\tfor (var j=0; j<25; j++){
\t\t\t\t\t\$(\"#tahun-filter\").append(\$('<option></option>').val(2000+j).html(2000+j));
\t\t\t\t}
\t\t\t}

\t\t\tgoogle.maps.event.addDomListener(window, 'load', initialize);

\t\t\t// filter
\t\t\tfunction doFilter() {
\t\t\t\tvar filters = _.map(\$(\"input[name=filter]:checked\"), function(x) {
\t\t\t\t\treturn \$(x).attr(\"kib\");
\t\t\t\t});

\t\t\t\tvar filteredMarkers = null;

\t\t\t\tvar filterSkpd = \$('#skpd-filter option:selected').val();
\t\t\t\tif (filterSkpd === \"\") {
\t\t\t\t\tfilteredMarkers = markers;
\t\t\t\t} else {
\t\t\t\t\tfilteredMarkers = _.filter(markers, function(x){ return x.skpd.substr(0, 11) ===  filterSkpd; });
\t\t\t\t}

\t\t\t\t_.each(markers, function(x) { x.marker.setMap(null); });
\t\t\t\t
\t\t\t\tshownMarkers = _.filter(filteredMarkers, function(x) { return !_.isEmpty(_.filter(filters, function(y) { return x.kode_kib.substr(0, 2) == y }))  });
\t\t\t\t
\t\t\t\t_.each(filteredMarkers, function(x) { 
\t\t\t\t\tif (_.find(filters, function(y) { return x.kode_kib.substr(0, 2) === y }) !== undefined) { 
\t\t\t\t\t\tif (x.marker.getMap() == null) 
\t\t\t\t\t\t\tx.marker.setMap(map); 
\t\t\t\t\t\t} 
\t\t\t\t\telse 
\t\t\t\t\t\tx.marker.setMap(null); 
\t\t\t\t});

\t\t\t\tif (filterSkpd === \"\") {
\t\t\t\t\t_.each(daftarSkpd, function(x) { x.marker.setMap(map); });
\t\t\t\t} else {
\t\t\t\t\t//if(_.each(filteredMarkers, function(x) { if (_.find(filters, function(y) { return x.kode_kib.substr(0, 2) === y }) !== undefined) { if (x.marker.getMap() == null) x.marker.setMap(map); } else x.marker.setMap(null); })
\t\t\t\t\t_.each(daftarSkpd, function(x) { if (filterSkpd === x.skpd.substr(0, 11)) { if (x.marker.getMap() == null) x.marker.setMap(map); } else x.marker.setMap(null)});
\t\t\t\t}

\t\t\t\t// override autocomplete
\t\t\t\t\$(\"input#pencarian\").autocomplete({
\t\t\t\t\tminLength: 1,
\t\t\t\t\tsource: _.map(shownMarkers, function(x) { return { id:x.kode, label: x.nama_asset, marker: x.marker } }),
\t\t\t\t\tselect: function(e, u){
\t\t\t\t\t\t\$(this).val(u.item.label);
\t\t\t\t\t\tmap.panTo(u.item.marker.getPosition());
\t\t\t\t\t\tmap.setZoom(15);

\t\t\t\t\t\treturn false;
\t\t\t\t\t}
\t\t\t  \t});
\t\t\t}

\t\t\tfunction filterSkpd(){return;
\t\t\t\tvar filterSkpd = \$('#skpd-filter option:selected').val();
\t\t\t\t//shownMarkers = _.filter(markers, function(x){ return x.skpd.substr(0, 11) ===  filterSkpd; });
\t\t\t\t_.each(markers, function(x) { if (filterSkpd === x.skpd.substr(0, 11)) { if (x.marker.getMap() == null) x.marker.setMap(map); } else x.marker.setMap(null)});
\t\t\t\t_.each(daftarSkpd, function(x) { if (filterSkpd === x.skpd.substr(0, 11)) { if (x.marker.getMap() == null) x.marker.setMap(map); } else x.marker.setMap(null)});
\t\t\t}

\t\t\t_.each(\$(\"input[name=filter]\"), function(x) {
\t\t\t\t\$(x).change(doFilter);
\t\t\t});

\t\t\t// register onchange selectbox
\t\t\t\$(\"select#skpd-filter\").change(function() {
        \t\tdoFilter();
    \t\t});
\t\t\t
\t\t\tloadAset(doFilter);
\t\t\tloadSkpd();

\t\t\tfunction addMarker(map, e) {
\t\t\t\tif ( !marker ) {
\t\t\t\t\tvar uid = guid();
\t\t\t\t\tvar info_window = new google.maps.InfoWindow({
\t\t\t\t\t\tmaxWidth: 800
\t\t\t\t\t});

\t\t\t\t\tvar marker = new google.maps.Marker({ map: map });
\t\t\t\t\tvar isSaved = false;

\t\t\t\t\tmarker.uid = uid;
\t\t\t\t\tif (e != null) {
\t\t\t\t\t\tmarker.setPosition(e.latLng);
\t\t\t\t\t} else {
\t\t\t\t\t\tvar lat = \$(\"#lat\").val();
\t\t\t\t\t\tvar lng = \$(\"#long\").val();

\t\t\t\t\t\tmarker.setPosition(new google.maps.LatLng(lat, lng));
\t\t\t\t\t}

\t\t\t\t\tinfo_window.setContent('<div style=\"width:400px; max-height:320px;\">' + 
\t\t\t\t\t\t'<table class=\"hasil\" style=\"height:300px; width:400px;\" id=\\'' + uid + '\\'>' + '<input id=\"kode_bidang\" type=\"hidden\" />' +
\t\t\t\t\t\t'<tr><td style=\"width:25%;\">Kode Aset:</td><td style=\"width:75%;\"><div class=\"ui-widget\"><input style=\"padding-left:2px;\" id=\"kode\" size=\"25\" /></div></td></tr>' + 
\t\t\t\t\t\t'<tr><td style=\"width:25%;\">Nama Aset:</td><td style=\"width:75%;\"><p style=\"padding-left:2px; margin-top: 7px;\" id=\"nama_asset\">-</p></td></tr>' +
\t\t\t\t\t\t'<tr><td style=\"width:25%;\">KIB:</td><td style=\"width:75%;\"><p style=\"padding-left:2px; margin-top: 7px;\" id=\"bidang\">-</p></td></tr>' +
\t\t\t\t\t\t'<tr><td style=\"width:25%;\">Nomor Unit:</td> <td style=\"width:75%;\"><p style=\"padding-left:2px;\" id=\"skpd\"/>-</p></td> </tr>' +
\t\t\t\t\t\t'<tr><td style=\"width:25%;\">Nama Unit:</td> <td style=\"width:75%;\"><p style=\"padding-left:2px;\" id=\"nama_skpd\"/>-</p></td> </tr>' +
\t\t\t\t\t\t'</select> </td></tr>' +
\t\t\t\t\t\t'<tr><td style=\"width:25%;\">Latitude:</td> <td style=\"width:75%;\"><p style=\"padding-left:2px;\" id=\"latFld\">-</p></td> </tr>' +
\t\t\t\t\t\t'<tr><td style=\"width:25%;\">Longitude:</td> <td style=\"width:75%;\"><p style=\"padding-left:2px;\" id=\"lngFld\"/>-</p></td> </tr>' +
\t\t\t\t\t\t'<tr><td colspan=\"2\" style=\"text-align:center;\"><input id=\"simpanBtn\" type=\"button\" value=\"Save & Close\"/></td></tr>' +
\t\t\t\t\t\t'</div>');

\t\t\t\t\tinfo_window.open(map,marker);

\t\t\t\t\tgoogle.maps.event.addListener(info_window, 'domready', function() {
\t\t\t\t\t\t\$(\"input#kode\").autocomplete({
\t\t    \t\t\t\tminLength: 1,
\t\t    \t\t\t\tsource: \"";
        // line 872
        echo twig_escape_filter($this->env, site_url("aset/cari"), "html", null, true);
        echo "\",
\t\t    \t\t\t\tselect: function(e, u) {
\t\t    \t\t\t\t\t\$(this).val(u.item.id);
\t\t    \t\t\t\t\tvar table = \$(this).closest(\"table\");
\t\t    \t\t\t\t\ttable.find(\"p#nama_asset\").html(u.item.NAMA_BARANG + (u.item.MERK_ALAMAT != null ? \", \" + u.item.MERK_ALAMAT : \"\") + (u.item.TIPE != null ? \", \" + u.item.TIPE : \"\"));
\t\t    \t\t\t\t\ttable.find(\"input#kode_bidang\").val(u.item.KODE_BIDANG);
\t\t    \t\t\t\t\ttable.find(\"p#bidang\").html(u.item.BIDANG);
\t\t    \t\t\t\t\ttable.find(\"p#skpd\").html(u.item.SKPD);
\t\t    \t\t\t\t\ttable.find(\"p#nama_skpd\").html(u.item.NAMA_SKPD);
\t\t    \t\t\t\t\treturn false;
\t\t    \t\t\t\t}
\t\t  \t\t\t\t});

\t\t\t\t\t\tif (e != null) {
\t\t\t\t\t\t\t\$(\"table#\" + uid).find(\"p[id=latFld]\").html(e.latLng.lat());
\t\t\t\t\t\t\t\$(\"table#\" + uid).find(\"p[id=lngFld]\").html(e.latLng.lng());
\t\t\t\t\t\t} else {
\t\t\t\t\t\t\tvar lat = \$(\"#lat\").val();
\t\t\t\t\t\t\tvar lng = \$(\"#long\").val();

\t\t\t\t\t\t\t\$(\"table#\" + uid).find(\"p[id=latFld]\").html(lat);
\t\t\t\t\t\t\t\$(\"table#\" + uid).find(\"p[id=lngFld]\").html(lng);
\t\t\t\t\t\t}

\t\t\t\t\t\t\$(\"table#\" + uid).find(\"input[id=simpanBtn]\").click(function() {
\t\t\t\t\t\t\tvar table = \$(this).closest(\"table\");
\t\t\t\t\t\t\tvar kode = table.find(\"input#kode\").val();
\t\t\t\t\t\t\tvar nama_asset = table.find(\"p#nama_asset\").html();
\t\t\t\t\t\t\tvar kib = table.find(\"p#bidang\").html();
\t\t\t\t\t\t\tvar kode_kib = table.find(\"input#kode_bidang\").val();
\t\t\t\t\t\t\tvar skpd = table.find(\"p#skpd\").html();
\t\t\t\t\t\t\tvar nama_skpd = table.find(\"p#nama_skpd\").html();
\t\t\t\t\t\t\tvar iconBase = \"http://localhost/pantau/assets/img/marker/\";
\t\t\t\t\t\t\tvar icon = kode_kib.substr(0, 2);

\t\t\t\t\t\t\tvar latLng = marker.getPosition();
\t\t\t\t\t\t\tvar url = \"";
        // line 908
        echo twig_escape_filter($this->env, site_url("aset/simpan"), "html", null, true);
        echo "\";

\t\t\t\t\t\t\tmarker.kode = kode;

\t\t\t\t\t\t\tif(kode == ''){
\t\t\t\t\t\t\t\talert(\"Tolong masukkan kode aset\");
\t\t\t\t\t\t\t}
\t\t\t\t\t\t\telse{
\t\t\t\t\t\t\t\t\$.post(url, {kode:kode, nama_asset: nama_asset, kib:kib, kode_kib:kode_kib, latitude: latLng.lat(), longitude: latLng.lng(), skpd:skpd, nama_skpd:nama_skpd}, function(r) {
\t\t\t\t\t\t\t\t\tmarkers.push({ kode: kode, nama_asset: nama_asset, latitude: latLng.lat(), longitude: latLng.lng(), kib: kib, kode_kib: kode_kib, skpd:skpd, nama_skpd:nama_skpd, marker: marker});
\t\t\t\t\t\t\t\t\tmarker.setIcon(iconBase + icon + '.png')
\t\t\t\t\t\t\t\t\tgoogle.maps.event.addListener(marker,'click', infoCallback(info_window, marker));
\t\t\t\t\t\t\t\t}, \"json\");
\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\tisSaved = true;
\t\t\t\t\t\t\t\tinfo_window.close();
\t\t\t\t\t\t\t\t\$(\"#simpanBtn\").unbind();
\t\t\t\t\t\t\t}
\t\t\t\t\t\t});
\t\t\t\t\t});


\t\t\t\t\tgoogle.maps.event.addListener(info_window, 'closeclick', function() {
\t\t\t\t\t\tif(!isSaved){
\t\t\t\t\t\t\tmarker.setMap(null);
\t\t\t\t\t\t\treturn false;
\t\t\t\t\t\t}
\t\t\t\t\t\telse{
\t\t\t\t\t\t\tinfo_window.close();\t\t\t\t\t\t\t\t
\t\t\t\t\t\t}
\t\t\t\t\t});
\t\t\t\t} else {
\t\t\t\t\tmarker.setPosition(e.latLng);
\t\t\t\t}
\t\t\t}
\t\t\t
\t\t\tvar dialog = \$( \"#dialog\" ).dialog({
\t\t\t\tautoOpen: false,
\t\t\t\theight: 180,
\t\t\t\twidth: 280,
\t\t\t\tmodal: true,
\t\t\t\topen: function() {
\t\t\t\t\t\$(\"#lat\").val(\"\");
\t\t\t\t\t\$(\"#long\").val(\"\");
\t\t\t\t},
\t\t\t\tbuttons: {
\t\t\t\t\tTambahkan: function() {
\t\t\t\t\t\tdialog.dialog( \"close\" );
\t\t\t\t\t\taddMarker(map, null);
\t\t\t\t\t},
\t\t\t\t\tBatal: function() {
\t\t\t\t\t\tdialog.dialog( \"close\" );
\t\t\t\t\t}
\t\t\t\t}
\t\t\t});

\t\t\tfunction newPolylines() {
\t\t\t\tvar lineSymbol = {
\t\t\t\t    path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW
\t\t\t\t}
\t\t\t\tvar polyOptions = {
\t\t\t\t    strokeColor: '#000000',
\t\t\t\t    strokeOpacity: 1.0,
\t\t\t\t    strokeWeight: 3
\t\t\t\t};

\t\t\t\treturn new google.maps.Polyline(polyOptions);
\t\t\t}

\t\t\tfunction addPolylines(map) {
\t\t\t\tpoly = newPolylines();
  \t\t\t\tpoly.setMap(map);

  \t\t\t\t// Add a listener for the click event
  \t\t\t\tgoogle.maps.event.addListener(map, 'click', addLatLng);
\t\t\t}

\t\t\tfunction addLatLng(event) {
\t\t\t\tvar path = poly.getPath();

\t\t\t\t// Because path is an MVCArray, we can simply append a new coordinate
\t\t\t\t// and it will automatically appear.
\t\t\t\tpath.push(event.latLng);

\t\t\t\t// Add a new marker at the new plotted point on the polyline.
\t\t\t\tvar marker = new google.maps.Marker({
\t\t\t\t\tposition: event.latLng,
\t\t\t\t\ttitle: '#' + path.getLength(),
\t\t\t\t\tmap: null
\t\t\t\t});
\t\t\t}

\t\t})
\t</script>
";
    }

    // line 1005
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 1006
        echo "\t";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "

\t<link href=\"";
        // line 1008
        echo twig_escape_filter($this->env, base_url("assets/css/bootstrap-switch.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
\t<link href=\"http://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css\" rel=\"stylesheet\">
\t<link href=\"";
        // line 1010
        echo twig_escape_filter($this->env, base_url("assets/css/jquery.loadmask.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
\t<style type=\"text/css\">
\t#map-canvas {
\t  margin-left: 10px;
\t  margin-right: 10px;
\t  width: :800px;
\t  height: 530px;
\t  border-left: 1px solid rgba(1, 1, 1, 0.25);
\t  border-right: 1px solid rgba(1, 1, 1, 0.25);
\t  border-bottom: 1px solid rgba(1, 1, 1, 0.25);
\t}
\t#pushstat {
\t  display:none;
\t}
\t.masukan, .hasil{
\t    font-size:14px;
\t}

\t.hasil input{
\t\tfont-size: 12px;
\t\tpadding: 5px;
\t}
\t.gm-style-iw {
\t\tpadding: 10px !important;
\t}

\t.ui-autocomplete { height: 200px; overflow-y: scroll; overflow-x: hidden; font-size: 12px;}

\t.ui-widget {
\t    font-family: inherit;
\t    font-size: 1em;
\t}

\ttable.aset tr th:nth-child(2), table.aset tr td:nth-child(2) {
\t\ttext-align: center;
\t} 
\ttable.aset tr th:nth-child(3), table.aset tr td:nth-child(3) {
\t\ttext-align: right;
\t}

\tdiv.infowindow {
\t    max-height:300px;
\t    overflow-y:auto;
\t}

\tdiv.tab-pane {
\t\tpadding: 10px 0px;
\t}

\tdiv.gm-style-iw > div {
\t\theight: 100% !important;
\t}

\tdiv.row.foto2 {
\t\t-moz-column-count: 3;
\t\tcolumn-count: 3;
\t\t-moz-column-gap: 0;
\t\tcolumn-gap:0;
\t}

\tdiv.row.foto2 > .col-xs-12 {
\t\tfloat: none !important;
\t}

\tpre {
\t\tbackground-color: ghostwhite;
\t\tborder: 1px solid silver;
\t\tpadding: 10px 20px;
\t\tmargin: 20px; 
\t}

\t</style>
";
    }

    public function getTemplateName()
    {
        return "aset/index.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1112 => 1010,  1107 => 1008,  1101 => 1006,  1098 => 1005,  999 => 908,  960 => 872,  834 => 749,  786 => 704,  753 => 674,  674 => 598,  589 => 516,  569 => 499,  514 => 447,  477 => 413,  465 => 404,  434 => 376,  256 => 201,  246 => 194,  221 => 172,  200 => 154,  138 => 95,  134 => 94,  130 => 93,  122 => 89,  119 => 88,  33 => 4,  30 => 3,);
    }
}
