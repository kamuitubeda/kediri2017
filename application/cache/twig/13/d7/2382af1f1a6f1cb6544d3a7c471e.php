<?php

/* simgo/persediaan/laporan/buku_pengambilan_barang.html */
class __TwigTemplate_13d72382af1f1a6f1cb6544d3a7c471e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("_templates/adminpage.html");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'scripts' => array($this, 'block_scripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_templates/adminpage.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 1
    public function block_content($context, array $blocks = array())
    {
        // line 2
        echo "<div class=\"container\">
    <div class=\"row\">
        <div class=\"col-md-12\">
            <ol class=\"breadcrumb\">
                <li><a href=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("persediaan"), "html", null, true);
        echo "\">Persediaan</a></li>
                <li class=\"active\">Buku Pengambilan Barang</li>
            </ol>
        </div>
    </div>
    <div class=\"row\">
        <div class=\"col-md-12\">
            <table class=\"table\" style=\"border-style:hidden;\">
                <tr>
                    <td class='col-md-6 text-left'><strong>Daerah/SKPD : ";
        // line 15
        echo twig_escape_filter($this->env, (isset($context["skpd"]) ? $context["skpd"] : null), "html", null, true);
        echo "</strong></td>
                    <td class='col-md-6 text-right'><strong>No. :</strong> .......................................................</td>
                </tr>
            </table>
        </div>
    </div>
    <div class=\"row\">
        <div class=\"col-md-12 text-center\">
            <h2>Bukti Pengambilan Barang Dari Gudang</h2></div>
    </div>
    <div class=\"row\">
        <div class=\"col-md-12\">
            <table class=\"table table-striped table-bordered\">
                <thead>
                    <tr>
                        <th class=\"col-md-1 text-center\" rowspan=\"2\">Nomor SPPB</th>
                        <th class=\"col-md-1 text-center\" rowspan=\"2\">Tanggal SPPB</th>
                        <th class=\"text-center\" rowspan=\"2\">Nama dan Spesifikasi Barang</th>
                        <th class=\"text-center\" rowspan=\"2\">Satuan</th>
                        <th class=\"text-center\" colspan=\"2\">Jumlah Barang</th>
                        <th class=\"col-md-2 text-center\" rowspan=\"2\">Jumlah Harga</th>
                    </tr>
                    <tr>
                        <th class=\"col-md-1 text-center\">(angka)</th>
                        <th class=\"col-md-1 text-center\">(huruf)</th>
                    </tr>
                    <tr>
                        ";
        // line 42
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 7));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 43
            echo "                        <th class=\"small-header text-center\"><small>";
            echo twig_escape_filter($this->env, (isset($context["i"]) ? $context["i"] : null), "html", null, true);
            echo "</small></th>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 45
        echo "                    </tr>
                </thead>
                <tbody id=\"data-body\">
                    <tr></tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class=\"row\">
        <div class=\"col-md-12\">
            <div class=\"btn-group pull-right\">
                <button id=\"kembali\" type=\"button\" class=\"btn btn-default\">Kembali</button>
                <button id=\"cetak\" type=\"button\" class=\"btn btn-primary\"><span class=\"glyphicon glyphicon-print\"></span> Cetak</button>
            </div>
        </div>
    </div>
</div>
";
    }

    // line 62
    public function block_stylesheets($context, array $blocks = array())
    {
        echo " ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
<link rel=\"stylesheet\" type=\"text/css\" media=\"print\" href=\"";
        // line 63
        echo twig_escape_filter($this->env, base_url("assets/css/bootstrap.min.css"), "html", null, true);
        echo "\">
<style type=\"text/css\">
    .small-header {
        font-weight: normal !important;
        padding: 2px !important;
    }
    
    .text-middle{
        vertical-align: middle !important;
    }
    
    @media print {
        body * {
            visibility: hidden;
        }
        #topbar,
        #sidebar,
        .breadcrumb,
        h1 {
            display: none;
        }
        .container,
        .container * {
            visibility: visible;
        }
        .sidebar-max {
            padding: 0px !important;
        }
        #content {
            position: absolute !important;
            left: 0px !important;
            top: 0px !important;
        }
    }
</style>
";
    }

    // line 98
    public function block_scripts($context, array $blocks = array())
    {
        echo " ";
        $this->displayParentBlock("scripts", $context, $blocks);
        echo "
<script src=\"";
        // line 99
        echo twig_escape_filter($this->env, site_url("assets/js/numeral/numeral.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 100
        echo twig_escape_filter($this->env, site_url("assets/js/numeral/languages.id.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\">

    numeral.language('id-ID');
    
    jQuery(function(\$){
        populateTable();
        
        function populateTable(){
            \$(\"#data-body\").empty();
            \$(\"#data-body\").append(\"<tr></tr><tr></tr>\");
            
            \$.post(\"";
        // line 112
        echo twig_escape_filter($this->env, site_url("persediaan/get_populasi_buku_pengambilan_barang"), "html", null, true);
        echo "\", {skpd:\"";
        echo twig_escape_filter($this->env, (isset($context["skpd"]) ? $context["skpd"] : null), "html", null, true);
        echo "\"}, function(json){
                json.forEach(function(value, index){
                    var nomor = value[\"juno\"];
                    var tglrec = value[\"jutgl\"];
                    if(value.detail!=null){
                        value.detail.forEach(function(value, index){
                            \$(\"#data-body\").append(
                                \"<tr>\"+
                                    \"<td class='text-center'>\"+((index == 0)? nomor : \"\")+\"</td>\"+
                                    \"<td class='text-center'>\"+((index == 0)? tglrec : \"\")+\"</td>\"+
                                    \"<td>\"+value['namabarang']+\"<br/><small>\"+value['spesifikasi']+\"</small></td>\"+
                                    \"<td>\"+value['judunit']+\"</td>\"+
                                    \"<td class='text-right'>\"+numeral(-value['judqty']).format(\"0,0\")+\"</td>\"+
                                    \"<td><small>\"+value['qty_huruf']+\"</small></td>\"+
                                    \"<td class='text-right'>\"+numeral(-value['judhargasat'] * value['judqty']).format(\"\$0,0.00\")+\"</td>\"+
                                \"</tr>\"
                            );
                        }, nomor, tglrec);
                    }
                    else
                    {
                        \$(\"#data-body\").append(
                            \"<tr>\"+
                                \"<td class='text-center'>\"+nomor+\"</td>\"+
                                \"<td class='text-center'>\"+tglrec+\"</td>\"+
                                \"<td class='text-center text-middle'>-</td>\"+
                                \"<td class='text-center text-middle'>-</td>\"+
                                \"<td class='text-center text-middle'>-</td>\"+
                                \"<td class='text-center text-middle'>-</td>\"+
                                \"<td class='text-center text-middle'>-</td>\"+
                            \"</tr>\"
                        );
                    }
                });
            }, \"json\");
        }
        
        \$(\"#cetak\").on(\"click\", function(){
            window.open(\"";
        // line 150
        echo twig_escape_filter($this->env, site_url("persediaan/print_buku_pengambilan_barang"), "html", null, true);
        echo "\");
        });
        \$(\"#kembali\").on(\"click\", function(){
            window.location.href = \"";
        // line 153
        echo twig_escape_filter($this->env, site_url("persediaan"), "html", null, true);
        echo "\";
        });
\t});

</script>
";
    }

    public function getTemplateName()
    {
        return "simgo/persediaan/laporan/buku_pengambilan_barang.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  237 => 153,  231 => 150,  188 => 112,  173 => 100,  169 => 99,  162 => 98,  122 => 63,  115 => 62,  94 => 45,  85 => 43,  81 => 42,  51 => 15,  39 => 6,  33 => 2,  30 => 1,);
    }
}
