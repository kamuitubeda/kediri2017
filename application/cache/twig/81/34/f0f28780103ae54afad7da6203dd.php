<?php

/* dashboard/realisasi.html */
class __TwigTemplate_8134f0f28780103ae54afad7da6203dd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("_templates/adminpage.html");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'scripts' => array($this, 'block_scripts'),
            'stylesheets' => array($this, 'block_stylesheets'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_templates/adminpage.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "
<div class=\"row\">
\t<div class=\"col-xs-12\">
\t\t<div class=\"col-xs-3\" style=\"padding-left:0px; padding-right:0px; \" >
\t\t</div>
\t\t<div id=\"realisasi-pegawai-chart\" class=\"col-xs-6\" style=\"padding-left:0px; padding-right:0px; height:50vh;\">
\t\t</div>
\t\t<div class=\"col-xs-3\" style=\"padding-left:0px; padding-right:0px;\">
\t\t</div>
\t</div>
\t<div class=\"col-xs-12\">
\t\t<div id=\"realisasi-barang-chart\" class=\"col-xs-6\" style=\"padding-left:0px; padding-right:0px; height:50vh;\">
\t\t</div>
\t\t<div id=\"realisasi-modal-chart\" class=\"col-xs-6\" style=\"padding-left:0px; padding-right:0px; height:50vh;\">
\t\t</div>
\t</div>
\t<div id=\"realisasi-pembanding-chart\" class=\"col-xs-12\" style=\"padding-left:0px; padding-right:0px; height:1000vh;\">
\t</div>
\t<div id=\"realisasi-simbada-tanah-chart\" class=\"col-xs-12\" style=\"padding-left:0px; padding-right:0px; height:50vh;\">
\t</div>
\t<div id=\"realisasi-simbada-mesin-chart\" class=\"col-xs-12\" style=\"padding-left:0px; padding-right:0px; height:50vh;\">
\t</div>
\t<div id=\"realisasi-simbada-gedung-chart\" class=\"col-xs-12\" style=\"padding-left:0px; padding-right:0px; height:50vh;\">
\t</div>
\t<div id=\"realisasi-simbada-jalan-chart\" class=\"col-xs-12\" style=\"padding-left:0px; padding-right:0px; height:50vh;\">
\t</div>
\t<div id=\"realisasi-simbada-lain-chart\" class=\"col-xs-12\" style=\"padding-left:0px; padding-right:0px; height:50vh;\">
\t</div>
\t<div id=\"saldo-current-chart\" class=\"col-xs-12\" style=\"padding-left:0px; padding-right:0px; height:50vh;\">
\t</div>
</div>
";
    }

    // line 38
    public function block_scripts($context, array $blocks = array())
    {
        // line 39
        $this->displayParentBlock("scripts", $context, $blocks);
        echo "
<script type=\"text/javaScript\" src=\"";
        // line 40
        echo twig_escape_filter($this->env, base_url("assets/js/jquery.loadmask.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 41
        echo twig_escape_filter($this->env, base_url("assets/js/select2.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 42
        echo twig_escape_filter($this->env, base_url("assets/js/highcharts.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 43
        echo twig_escape_filter($this->env, base_url("assets/js/exporting.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 44
        echo twig_escape_filter($this->env, base_url("assets/js/accounting.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\">
    jQuery(document).ready(function(\$) {
    \t// configure setting accounting.js
\t\taccounting.settings = {
\t\t\tnumber: {
\t\t\t\tprecision : 0,  // default precision on numbers is 0
\t\t\t\tthousand: \".\",
\t\t\t\tdecimal : \",\"
\t\t\t}
\t\t}

\t\tvar n = function(v, p) {
\t\t\treturn accounting.formatNumber(v, p);
\t\t}

\t\tvar u = function(v) {
\t\t\treturn accounting.unformat(v);
\t\t}

    \t\$(\"body\").mask(\"Processing...\");
    \t\$.post(\"";
        // line 65
        echo twig_escape_filter($this->env, site_url("aset/get_realisasi_pegawai"), "html", null, true);
        echo "\", {}, function(json){
    \t\tvar anggaran = json.anggaran;
    \t\tvar realisasi = json.realisasi;

    \t\tvar persen_anggaran = anggaran/anggaran*100;
    \t\tvar persen_realisasi = realisasi/anggaran*100;

    \t\t\$('#realisasi-pegawai-chart').highcharts({
\t\t        chart: {
\t\t            plotBackgroundColor: null,
\t\t            plotBorderWidth: null,
\t\t            plotShadow: false,
\t\t            type: 'pie'
\t\t        },
\t\t        title: {
\t\t            text: 'Realisasi Anggaran Pegawai'
\t\t        },
\t\t        tooltip: {
\t\t            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
\t\t        },
\t\t        plotOptions: {
\t\t            pie: {
\t\t            \tsize: '90%',
\t\t                allowPointSelect: true,
\t\t                cursor: 'pointer',
\t\t                dataLabels: {
\t\t                    enabled: true,
\t\t                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
\t\t                    style: {
\t\t                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
\t\t                    }
\t\t                }
\t\t            }
\t\t        },
\t\t        series: [{
\t\t            name: 'Anggaran',
\t\t            colorByPoint: true,
\t\t            data: [{
\t\t                name: 'Sisa',
\t\t                y: persen_anggaran-persen_realisasi
\t\t            }, {
\t\t                name: 'Realisasi',
\t\t                y: persen_realisasi,
\t\t                sliced: true,
\t\t                selected: true
\t\t            }]
\t\t        }]
\t\t    });
\t\t}, \"json\");

\t\t\$.post(\"";
        // line 115
        echo twig_escape_filter($this->env, site_url("aset/get_realisasi_barang"), "html", null, true);
        echo "\", {}, function(json){
    \t\tvar anggaran = json.anggaran;
    \t\tvar realisasi = json.realisasi;

    \t\tvar persen_anggaran = anggaran/anggaran*100;
    \t\tvar persen_realisasi = realisasi/anggaran*100;

    \t\t\$('#realisasi-barang-chart').highcharts({
\t\t        chart: {
\t\t            plotBackgroundColor: null,
\t\t            plotBorderWidth: null,
\t\t            plotShadow: false,
\t\t            type: 'pie'
\t\t        },
\t\t        title: {
\t\t            text: 'Realisasi Anggaran Barang & Jasa'
\t\t        },
\t\t        tooltip: {
\t\t            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
\t\t        },
\t\t        plotOptions: {
\t\t            pie: {
\t\t            \tsize: '90%',
\t\t                allowPointSelect: true,
\t\t                cursor: 'pointer',
\t\t                dataLabels: {
\t\t                    enabled: true,
\t\t                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
\t\t                    style: {
\t\t                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
\t\t                    }
\t\t                }
\t\t            }
\t\t        },
\t\t        series: [{
\t\t            name: 'Anggaran',
\t\t            colorByPoint: true,
\t\t            data: [{
\t\t                name: 'Sisa',
\t\t                y: persen_anggaran-persen_realisasi
\t\t            }, {
\t\t                name: 'Realisasi',
\t\t                y: persen_realisasi,
\t\t                sliced: true,
\t\t                selected: true
\t\t            }]
\t\t        }]
\t\t    });
\t\t}, \"json\");

\t\t\$.post(\"";
        // line 165
        echo twig_escape_filter($this->env, site_url("aset/get_realisasi_modal"), "html", null, true);
        echo "\", {}, function(json){
    \t\tvar anggaran = json.anggaran;
    \t\tvar realisasi = json.realisasi;

    \t\tvar persen_anggaran = anggaran/anggaran*100;
    \t\tvar persen_realisasi = realisasi/anggaran*100;

    \t\t\$('#realisasi-modal-chart').highcharts({
\t\t        chart: {
\t\t            plotBackgroundColor: null,
\t\t            plotBorderWidth: null,
\t\t            plotShadow: false,
\t\t            type: 'pie'
\t\t        },
\t\t        title: {
\t\t            text: 'Realisasi Anggaran Modal'
\t\t        },
\t\t        tooltip: {
\t\t            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
\t\t        },
\t\t        plotOptions: {
\t\t            pie: {
\t\t            \tsize: '90%',
\t\t                allowPointSelect: true,
\t\t                cursor: 'pointer',
\t\t                dataLabels: {
\t\t                    enabled: true,
\t\t                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
\t\t                    style: {
\t\t                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
\t\t                    }
\t\t                }
\t\t            }
\t\t        },
\t\t        series: [{
\t\t            name: 'Anggaran',
\t\t            colorByPoint: true,
\t\t            data: [{
\t\t                name: 'Sisa Anggaran',
\t\t                y: persen_anggaran-persen_realisasi
\t\t            }, {
\t\t                name: 'Realisasi',
\t\t                y: persen_realisasi,
\t\t                sliced: true,
\t\t                selected: true
\t\t            }]
\t\t        }]
\t\t    });
\t\t}, \"json\");

    \t\$.post(\"";
        // line 215
        echo twig_escape_filter($this->env, site_url("aset/get_realisasi_tanah"), "html", null, true);
        echo "\", {}, function(json){
    \t\tvar anggaran = json.anggaran;
    \t\tvar realisasi = json.realisasi;
    \t\tvar realisasi_simbada = json.realisasi_simbada;

    \t\tvar persen_anggaran = anggaran/anggaran*100;
    \t\tvar persen_realisasi = realisasi/anggaran*100;
    \t\tvar persen_realisasi_simbada = realisasi_simbada/anggaran*100;

    \t\t\$('#realisasi-simbada-tanah-chart').highcharts({
\t\t        chart: {
\t\t            type: 'column'
\t\t        },
\t\t        title: {
\t\t            text: 'Komparasi Realisasi Tanah'
\t\t        },
\t\t        xAxis: {
\t\t            categories: ['Dinas Pendidikan'],
\t\t            title: {
\t\t                text: null
\t\t            }
\t\t        },
\t\t        yAxis: {
\t\t            min: 0,
\t\t            title: {
\t\t                text: 'Jumlah',
\t\t                align: 'high'
\t\t            },
\t\t            labels: {
\t\t                overflow: 'justify'
\t\t            }
\t\t        },
\t\t        tooltip: {
\t\t            valueSuffix: ' persen'
\t\t        },
\t\t        plotOptions: {
\t\t            bar: {
\t\t                dataLabels: {
\t\t                    enabled: true
\t\t                }
\t\t            }
\t\t        },
\t\t        legend: {
\t\t            layout: 'vertical',
\t\t            align: 'right',
\t\t            verticalAlign: 'top',
\t\t            x: -40,
\t\t            y: 80,
\t\t            floating: true,
\t\t            borderWidth: 1,
\t\t            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
\t\t            shadow: true
\t\t        },
\t\t        credits: {
\t\t            enabled: false
\t\t        },
\t\t        series: [{
\t\t            name: 'Anggaran',
\t\t            data: [persen_anggaran]
\t\t        }, {
\t\t            name: 'Realisasi',
\t\t            data: [persen_realisasi]
\t\t        }, {
\t\t            name: 'Realisasi Simbada',
\t\t            data: [persen_realisasi_simbada]
\t\t        }]
\t\t    });
\t\t}, \"json\");

\t\t\$.post(\"";
        // line 284
        echo twig_escape_filter($this->env, site_url("aset/get_realisasi_mesin"), "html", null, true);
        echo "\", {}, function(json){
    \t\tvar anggaran = json.anggaran;
    \t\tvar realisasi = json.realisasi;
    \t\tvar realisasi_simbada = json.realisasi_simbada;

    \t\tvar persen_anggaran = anggaran/anggaran*100;
    \t\tvar persen_realisasi = realisasi/anggaran*100;
    \t\tvar persen_realisasi_simbada = realisasi_simbada/anggaran*100;

    \t\t\$('#realisasi-simbada-mesin-chart').highcharts({
\t\t        chart: {
\t\t            type: 'column'
\t\t        },
\t\t        title: {
\t\t            text: 'Komparasi Realisasi Peralatan dan Mesin'
\t\t        },
\t\t        xAxis: {
\t\t            categories: ['Dinas Pendidikan'],
\t\t            title: {
\t\t                text: null
\t\t            }
\t\t        },
\t\t        yAxis: {
\t\t            min: 0,
\t\t            title: {
\t\t                text: 'Jumlah',
\t\t                align: 'high'
\t\t            },
\t\t            labels: {
\t\t                overflow: 'justify'
\t\t            }
\t\t        },
\t\t        tooltip: {
\t\t            valueSuffix: ' persen'
\t\t        },
\t\t        plotOptions: {
\t\t            bar: {
\t\t                dataLabels: {
\t\t                    enabled: true
\t\t                }
\t\t            }
\t\t        },
\t\t        legend: {
\t\t            layout: 'vertical',
\t\t            align: 'right',
\t\t            verticalAlign: 'top',
\t\t            x: -40,
\t\t            y: 80,
\t\t            floating: true,
\t\t            borderWidth: 1,
\t\t            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
\t\t            shadow: true
\t\t        },
\t\t        credits: {
\t\t            enabled: false
\t\t        },
\t\t        series: [{
\t\t            name: 'Anggaran',
\t\t            data: [persen_anggaran]
\t\t        }, {
\t\t            name: 'Realisasi',
\t\t            data: [persen_realisasi]
\t\t        }, {
\t\t            name: 'Realisasi Simbada',
\t\t            data: [persen_realisasi_simbada]
\t\t        }]
\t\t    });
\t\t}, \"json\");

\t\t\$.post(\"";
        // line 353
        echo twig_escape_filter($this->env, site_url("aset/get_realisasi_gedung"), "html", null, true);
        echo "\", {}, function(json){
    \t\tvar anggaran = json.anggaran;
    \t\tvar realisasi = json.realisasi;
    \t\tvar realisasi_simbada = json.realisasi_simbada;

    \t\tvar persen_anggaran = anggaran/anggaran*100;
    \t\tvar persen_realisasi = realisasi/anggaran*100;
    \t\tvar persen_realisasi_simbada = realisasi_simbada/anggaran*100;

    \t\t\$('#realisasi-simbada-gedung-chart').highcharts({
\t\t        chart: {
\t\t            type: 'column'
\t\t        },
\t\t        title: {
\t\t            text: 'Komparasi Realisasi Gedung'
\t\t        },
\t\t        xAxis: {
\t\t            categories: ['Dinas Pendidikan'],
\t\t            title: {
\t\t                text: null
\t\t            }
\t\t        },
\t\t        yAxis: {
\t\t            min: 0,
\t\t            title: {
\t\t                text: 'Jumlah',
\t\t                align: 'high'
\t\t            },
\t\t            labels: {
\t\t                overflow: 'justify'
\t\t            }
\t\t        },
\t\t        tooltip: {
\t\t            valueSuffix: ' persen'
\t\t        },
\t\t        plotOptions: {
\t\t            bar: {
\t\t                dataLabels: {
\t\t                    enabled: true
\t\t                }
\t\t            }
\t\t        },
\t\t        legend: {
\t\t            layout: 'vertical',
\t\t            align: 'right',
\t\t            verticalAlign: 'top',
\t\t            x: -40,
\t\t            y: 80,
\t\t            floating: true,
\t\t            borderWidth: 1,
\t\t            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
\t\t            shadow: true
\t\t        },
\t\t        credits: {
\t\t            enabled: false
\t\t        },
\t\t        series: [{
\t\t            name: 'Anggaran',
\t\t            data: [persen_anggaran]
\t\t        }, {
\t\t            name: 'Realisasi',
\t\t            data: [persen_realisasi]
\t\t        }, {
\t\t            name: 'Realisasi Simbada',
\t\t            data: [persen_realisasi_simbada]
\t\t        }]
\t\t    });
\t\t}, \"json\");

\t\t\$.post(\"";
        // line 422
        echo twig_escape_filter($this->env, site_url("aset/get_realisasi_jalan"), "html", null, true);
        echo "\", {}, function(json){
    \t\tvar anggaran = json.anggaran;
    \t\tvar realisasi = json.realisasi;
    \t\tvar realisasi_simbada = json.realisasi_simbada;

    \t\tvar persen_anggaran = anggaran/anggaran*100;
    \t\tvar persen_realisasi = realisasi/anggaran*100;
    \t\tvar persen_realisasi_simbada = realisasi_simbada/anggaran*100;

    \t\t\$('#realisasi-simbada-jalan-chart').highcharts({
\t\t        chart: {
\t\t            type: 'column'
\t\t        },
\t\t        title: {
\t\t            text: 'Komparasi Realisasi Jalan'
\t\t        },
\t\t        xAxis: {
\t\t            categories: ['Dinas Pendidikan'],
\t\t            title: {
\t\t                text: null
\t\t            }
\t\t        },
\t\t        yAxis: {
\t\t            min: 0,
\t\t            title: {
\t\t                text: 'Jumlah',
\t\t                align: 'high'
\t\t            },
\t\t            labels: {
\t\t                overflow: 'justify'
\t\t            }
\t\t        },
\t\t        tooltip: {
\t\t            valueSuffix: ' persen'
\t\t        },
\t\t        plotOptions: {
\t\t            bar: {
\t\t                dataLabels: {
\t\t                    enabled: true
\t\t                }
\t\t            }
\t\t        },
\t\t        legend: {
\t\t            layout: 'vertical',
\t\t            align: 'right',
\t\t            verticalAlign: 'top',
\t\t            x: -40,
\t\t            y: 80,
\t\t            floating: true,
\t\t            borderWidth: 1,
\t\t            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
\t\t            shadow: true
\t\t        },
\t\t        credits: {
\t\t            enabled: false
\t\t        },
\t\t        series: [{
\t\t            name: 'Anggaran',
\t\t            data: [persen_anggaran]
\t\t        }, {
\t\t            name: 'Realisasi',
\t\t            data: [persen_realisasi]
\t\t        }, {
\t\t            name: 'Realisasi Simbada',
\t\t            data: [persen_realisasi_simbada]
\t\t        }]
\t\t    });
\t\t}, \"json\");

\t\t\$.post(\"";
        // line 491
        echo twig_escape_filter($this->env, site_url("aset/get_realisasi_lain"), "html", null, true);
        echo "\", {}, function(json){
    \t\tvar anggaran = json.anggaran;
    \t\tvar realisasi = json.realisasi;
    \t\tvar realisasi_simbada = json.realisasi_simbada;

    \t\tvar persen_anggaran = anggaran/anggaran*100;
    \t\tvar persen_realisasi = realisasi/anggaran*100;
    \t\tvar persen_realisasi_simbada = realisasi_simbada/anggaran*100;

    \t\t\$('#realisasi-simbada-lain-chart').highcharts({
\t\t        chart: {
\t\t            type: 'column'
\t\t        },
\t\t        title: {
\t\t            text: 'Komparasi Realisasi Aset Tetap Lainnya'
\t\t        },
\t\t        xAxis: {
\t\t            categories: ['Dinas Pendidikan'],
\t\t            title: {
\t\t                text: null
\t\t            }
\t\t        },
\t\t        yAxis: {
\t\t            min: 0,
\t\t            title: {
\t\t                text: 'Jumlah',
\t\t                align: 'high'
\t\t            },
\t\t            labels: {
\t\t                overflow: 'justify'
\t\t            }
\t\t        },
\t\t        tooltip: {
\t\t            valueSuffix: ' persen'
\t\t        },
\t\t        plotOptions: {
\t\t            bar: {
\t\t                dataLabels: {
\t\t                    enabled: true
\t\t                }
\t\t            }
\t\t        },
\t\t        legend: {
\t\t            layout: 'vertical',
\t\t            align: 'right',
\t\t            verticalAlign: 'top',
\t\t            x: -40,
\t\t            y: 80,
\t\t            floating: true,
\t\t            borderWidth: 1,
\t\t            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
\t\t            shadow: true
\t\t        },
\t\t        credits: {
\t\t            enabled: false
\t\t        },
\t\t        series: [{
\t\t            name: 'Anggaran',
\t\t            data: [persen_anggaran]
\t\t        }, {
\t\t            name: 'Realisasi',
\t\t            data: [persen_realisasi]
\t\t        }, {
\t\t            name: 'Realisasi Simbada',
\t\t            data: [persen_realisasi_simbada]
\t\t        }]
\t\t    });
\t\t}, \"json\");

\t\t\$.post(\"";
        // line 560
        echo twig_escape_filter($this->env, site_url("aset/get_realisasi_semua"), "html", null, true);
        echo "\", {}, function(json){
    \t\tvar data = json.data;
    \t\tvar persen_realisasi_modal = [];
    \t\tvar persen_realisasi_pegawai = [];
    \t\tvar persen_realisasi_barang = [];
    \t\tfor(var i=0; i<data.length; i++){
    \t\t\tif( data[i][0] != 0) {
    \t\t\t\tpersen_realisasi_modal[i] = Math.round(data[i][1]/data[i][0]*100 *1e2)/1e2;
    \t\t\t} else {
    \t\t\t\tpersen_realisasi_modal[i] = 0;
    \t\t\t}
\t\t\t\t
\t\t\t\tif( data[i][2] != 0) {
\t\t\t\t\tpersen_realisasi_pegawai[i] = Math.round(data[i][3]/data[i][2]*100 *1e2)/1e2;
\t\t\t\t} else {
\t\t\t\t\tpersen_realisasi_pegawai[i] = 0;
\t\t\t\t}
\t\t\t\t
\t\t\t\tif( data[i][4] != 0) {
\t\t\t\t\tpersen_realisasi_barang[i] = Math.round(data[i][5]/data[i][4]*100 *1e2)/1e2;
\t\t\t\t} else {
\t\t\t\t\tpersen_realisasi_barang[i] = 0;
\t\t\t\t}
        \t}

    \t\tvar anggaran_modal_dinas_pendidikan = Math.round(data[0][0]/1000000 *1e2)/1e2;
    \t\tvar realisasi_modal_dinas_pendidikan = Math.round(data[0][1]/1000000 *1e2)/1e2;
    \t\tvar anggaran_pegawai_dinas_pendidikan = Math.round(data[0][2]/1000000 *1e2)/1e2;
    \t\tvar realisasi_pegawai_dinas_pendidikan = Math.round(data[0][3]/1000000 *1e2)/1e2;
    \t\tvar anggaran_barang_dinas_pendidikan = Math.round(data[0][4]/1000000 *1e2)/1e2;
    \t\tvar realisasi_barang_dinas_pendidikan = Math.round(data[0][5]/1000000 *1e2)/1e2;

    \t\tvar anggaran_modal_dinas_kesehatan = Math.round(data[1][0]/1000000 *1e2)/1e2;
    \t\tvar realisasi_modal_dinas_kesehatan = Math.round(data[1][1]/1000000 *1e2)/1e2;
    \t\tvar anggaran_pegawai_dinas_kesehatan = Math.round(data[1][2]/1000000 *1e2)/1e2;
    \t\tvar realisasi_pegawai_dinas_kesehatan = Math.round(data[1][3]/1000000 *1e2)/1e2;
    \t\tvar anggaran_barang_dinas_kesehatan = Math.round(data[1][4]/1000000 *1e2)/1e2;
    \t\tvar realisasi_barang_dinas_kesehatan = Math.round(data[1][5]/1000000 *1e2)/1e2;
    \t\t
    \t\tvar anggaran_modal_rsud_soekandar = Math.round(data[2][0]/1000000 *1e2)/1e2;
    \t\tvar realisasi_modal_rsud_soekandar = Math.round(data[2][1]/1000000 *1e2)/1e2;
    \t\tvar anggaran_pegawai_rsud_soekandar = Math.round(data[2][2]/1000000 *1e2)/1e2;
    \t\tvar realisasi_pegawai_rsud_soekandar = Math.round(data[2][3]/1000000 *1e2)/1e2;
    \t\tvar anggaran_barang_rsud_soekandar = Math.round(data[2][4]/1000000 *1e2)/1e2;
    \t\tvar realisasi_barang_rsud_soekandar = Math.round(data[2][5]/1000000 *1e2)/1e2;

    \t\tvar anggaran_modal_rsud_basoeni = Math.round(data[3][0]/1000000 *1e2)/1e2;
    \t\tvar realisasi_modal_rsud_basoeni = Math.round(data[3][1]/1000000 *1e2)/1e2;
    \t\tvar anggaran_pegawai_rsud_basoeni = Math.round(data[3][2]/1000000 *1e2)/1e2;
    \t\tvar realisasi_pegawai_rsud_basoeni = Math.round(data[3][3]/1000000 *1e2)/1e2;
    \t\tvar anggaran_barang_rsud_basoeni = Math.round(data[3][4]/1000000 *1e2)/1e2;
    \t\tvar realisasi_barang_rsud_basoeni = Math.round(data[3][5]/1000000 *1e2)/1e2;

\t\t\tvar anggaran_modal_dinas_pu_pengairan = Math.round(data[4][0]/1000000 *1e2)/1e2;
    \t\tvar realisasi_modal_dinas_pu_pengairan = Math.round(data[4][1]/1000000 *1e2)/1e2;
    \t\tvar anggaran_pegawai_dinas_pu_pengairan = Math.round(data[4][2]/1000000 *1e2)/1e2;
    \t\tvar realisasi_pegawai_dinas_pu_pengairan = Math.round(data[4][3]/1000000 *1e2)/1e2;
    \t\tvar anggaran_barang_dinas_pu_pengairan = Math.round(data[4][4]/1000000 *1e2)/1e2;
    \t\tvar realisasi_barang_dinas_pu_pengairan = Math.round(data[4][5]/1000000 *1e2)/1e2;

\t\t\tvar anggaran_modal_dinas_pu_bina_marga = Math.round(data[5][0]/1000000 *1e2)/1e2; 
    \t\tvar realisasi_modal_dinas_pu_bina_marga = Math.round(data[5][1]/1000000 *1e2)/1e2;
    \t\tvar anggaran_pegawai_dinas_pu_bina_marga = Math.round(data[5][2]/1000000 *1e2)/1e2;
    \t\tvar realisasi_pegawai_dinas_pu_bina_marga = Math.round(data[5][3]/1000000 *1e2)/1e2;
    \t\tvar anggaran_barang_dinas_pu_bina_marga = Math.round(data[5][4]/1000000 *1e2)/1e2;
    \t\tvar realisasi_barang_dinas_pu_bina_marga = Math.round(data[5][5]/1000000 *1e2)/1e2;

\t\t\tvar anggaran_modal_dinas_pu_cipta_karya = Math.round(data[6][0]/1000000 *1e2)/1e2; 
    \t\tvar realisasi_modal_dinas_pu_cipta_karya = Math.round(data[6][1]/1000000 *1e2)/1e2;
    \t\tvar anggaran_pegawai_dinas_pu_cipta_karya = Math.round(data[6][2]/1000000 *1e2)/1e2;
    \t\tvar realisasi_pegawai_dinas_pu_cipta_karya = Math.round(data[6][3]/1000000 *1e2)/1e2;
    \t\tvar anggaran_barang_dinas_pu_cipta_karya = Math.round(data[6][4]/1000000 *1e2)/1e2;
    \t\tvar realisasi_barang_dinas_pu_cipta_karya = Math.round(data[6][5]/1000000 *1e2)/1e2;

\t\t\tvar anggaran_modal_bappeda = Math.round(data[7][0]/1000000 *1e2)/1e2; 
    \t\tvar realisasi_modal_bappeda = Math.round(data[7][1]/1000000 *1e2)/1e2;
    \t\tvar anggaran_pegawai_bappeda = Math.round(data[7][2]/1000000 *1e2)/1e2;
    \t\tvar realisasi_pegawai_bappeda = Math.round(data[7][3]/1000000 *1e2)/1e2;
    \t\tvar anggaran_barang_bappeda = Math.round(data[7][4]/1000000 *1e2)/1e2;
    \t\tvar realisasi_barang_bappeda = Math.round(data[7][5]/1000000 *1e2)/1e2;

\t\t\tvar anggaran_modal_dinas_perhubungan = Math.round(data[8][0]/1000000 *1e2)/1e2; 
    \t\tvar realisasi_modal_dinas_perhubungan = Math.round(data[8][1]/1000000 *1e2)/1e2;
    \t\tvar anggaran_pegawai_dinas_perhubungan = Math.round(data[8][2]/1000000 *1e2)/1e2;
    \t\tvar realisasi_pegawai_dinas_perhubungan = Math.round(data[8][3]/1000000 *1e2)/1e2;
    \t\tvar anggaran_barang_dinas_perhubungan = Math.round(data[8][4]/1000000 *1e2)/1e2;
    \t\tvar realisasi_barang_dinas_perhubungan = Math.round(data[8][5]/1000000 *1e2)/1e2;

\t\t\tvar anggaran_modal_badan_lingkungan_hidup = Math.round(data[9][0]/1000000 *1e2)/1e2; 
    \t\tvar realisasi_modal_badan_lingkungan_hidup = Math.round(data[9][1]/1000000 *1e2)/1e2;
    \t\tvar anggaran_pegawai_badan_lingkungan_hidup = Math.round(data[9][2]/1000000 *1e2)/1e2;
    \t\tvar realisasi_pegawai_badan_lingkungan_hidup = Math.round(data[9][3]/1000000 *1e2)/1e2;
    \t\tvar anggaran_barang_badan_lingkungan_hidup = Math.round(data[9][4]/1000000 *1e2)/1e2;
    \t\tvar realisasi_barang_badan_lingkungan_hidup = Math.round(data[9][5]/1000000 *1e2)/1e2;

    \t\tvar anggaran_modal_dinas_kependudukan = Math.round(data[10][0]/1000000 *1e2)/1e2; 
    \t\tvar realisasi_modal_dinas_kependudukan = Math.round(data[10][1]/1000000 *1e2)/1e2;
    \t\tvar anggaran_pegawai_dinas_kependudukan = Math.round(data[10][2]/1000000 *1e2)/1e2;
    \t\tvar realisasi_pegawai_dinas_kependudukan = Math.round(data[10][3]/1000000 *1e2)/1e2;
    \t\tvar anggaran_barang_dinas_kependudukan = Math.round(data[10][4]/1000000 *1e2)/1e2;
    \t\tvar realisasi_barang_dinas_kependudukan = Math.round(data[10][5]/1000000 *1e2)/1e2;

\t\t\tvar anggaran_modal_badan_pemberdayaan_perempuan = Math.round(data[11][0]/1000000 *1e2)/1e2;  
    \t\tvar realisasi_modal_badan_pemberdayaan_perempuan = Math.round(data[11][1]/1000000 *1e2)/1e2; 
    \t\tvar anggaran_pegawai_badan_pemberdayaan_perempuan = Math.round(data[11][2]/1000000 *1e2)/1e2; 
    \t\tvar realisasi_pegawai_badan_pemberdayaan_perempuan = Math.round(data[11][3]/1000000 *1e2)/1e2; 
    \t\tvar anggaran_barang_badan_pemberdayaan_perempuan = Math.round(data[11][4]/1000000 *1e2)/1e2; 
    \t\tvar realisasi_barang_badan_pemberdayaan_perempuan = Math.round(data[11][5]/1000000 *1e2)/1e2; 

    \t\tvar anggaran_modal_dinas_sosial = Math.round(data[12][0]/1000000 *1e2)/1e2; 
    \t\tvar realisasi_modal_dinas_sosial = Math.round(data[12][1]/1000000 *1e2)/1e2;
    \t\tvar anggaran_pegawai_dinas_sosial = Math.round(data[12][2]/1000000 *1e2)/1e2;
    \t\tvar realisasi_pegawai_dinas_sosial = Math.round(data[12][3]/1000000 *1e2)/1e2;
    \t\tvar anggaran_barang_dinas_sosial = Math.round(data[12][4]/1000000 *1e2)/1e2;
    \t\tvar realisasi_barang_dinas_sosial = Math.round(data[12][5]/1000000 *1e2)/1e2;

    \t\tvar anggaran_modal_dinas_tenaga_kerja = Math.round(data[13][0]/1000000 *1e2)/1e2; 
    \t\tvar realisasi_modal_dinas_tenaga_kerja = Math.round(data[13][1]/1000000 *1e2)/1e2;
    \t\tvar anggaran_pegawai_dinas_tenaga_kerja = Math.round(data[13][2]/1000000 *1e2)/1e2;
    \t\tvar realisasi_pegawai_dinas_tenaga_kerja = Math.round(data[13][3]/1000000 *1e2)/1e2;
    \t\tvar anggaran_barang_dinas_tenaga_kerja = Math.round(data[13][4]/1000000 *1e2)/1e2;
    \t\tvar realisasi_barang_dinas_tenaga_kerja = Math.round(data[13][5]/1000000 *1e2)/1e2;

    \t\tvar anggaran_modal_dinas_kooperasi = Math.round(data[14][0]/1000000 *1e2)/1e2; 
    \t\tvar realisasi_modal_dinas_kooperasi = Math.round(data[14][1]/1000000 *1e2)/1e2;
    \t\tvar anggaran_pegawai_dinas_kooperasi = Math.round(data[14][2]/1000000 *1e2)/1e2;
    \t\tvar realisasi_pegawai_dinas_kooperasi = Math.round(data[14][3]/1000000 *1e2)/1e2;
    \t\tvar anggaran_barang_dinas_kooperasi = Math.round(data[14][4]/1000000 *1e2)/1e2;
    \t\tvar realisasi_barang_dinas_kooperasi = Math.round(data[14][5]/1000000 *1e2)/1e2;

    \t\tvar anggaran_modal_dinas_pemuda = Math.round(data[15][0]/1000000 *1e2)/1e2; 
    \t\tvar realisasi_modal_dinas_pemuda = Math.round(data[15][1]/1000000 *1e2)/1e2;
    \t\tvar anggaran_pegawai_dinas_pemuda = Math.round(data[15][2]/1000000 *1e2)/1e2;
    \t\tvar realisasi_pegawai_dinas_pemuda = Math.round(data[15][3]/1000000 *1e2)/1e2;
    \t\tvar anggaran_barang_dinas_pemuda = Math.round(data[15][4]/1000000 *1e2)/1e2;
    \t\tvar realisasi_barang_dinas_pemuda = Math.round(data[15][5]/1000000 *1e2)/1e2;

    \t\tvar anggaran_modal_badan_kesatuan_bangsa = Math.round(data[16][0]/1000000 *1e2)/1e2; 
    \t\tvar realisasi_modal_badan_kesatuan_bangsa = Math.round(data[16][1]/1000000 *1e2)/1e2;
    \t\tvar anggaran_pegawai_badan_kesatuan_bangsa = Math.round(data[16][2]/1000000 *1e2)/1e2;
    \t\tvar realisasi_pegawai_badan_kesatuan_bangsa = Math.round(data[16][3]/1000000 *1e2)/1e2;
    \t\tvar anggaran_barang_badan_kesatuan_bangsa = Math.round(data[16][4]/1000000 *1e2)/1e2;
    \t\tvar realisasi_barang_badan_kesatuan_bangsa = Math.round(data[16][5]/1000000 *1e2)/1e2;

    \t\tvar anggaran_modal_kepala_daerah = Math.round(data[17][0]/1000000 *1e2)/1e2; 
    \t\tvar realisasi_modal_kepala_daerah = Math.round(data[17][1]/1000000 *1e2)/1e2;
    \t\tvar anggaran_pegawai_kepala_daerah = Math.round(data[17][2]/1000000 *1e2)/1e2;
    \t\tvar realisasi_pegawai_kepala_daerah = Math.round(data[17][3]/1000000 *1e2)/1e2;
    \t\tvar anggaran_barang_kepala_daerah = Math.round(data[17][4]/1000000 *1e2)/1e2;
    \t\tvar realisasi_barang_kepala_daerah = Math.round(data[17][5]/1000000 *1e2)/1e2;

    \t\tvar anggaran_modal_dprd = Math.round(data[18][0]/1000000 *1e2)/1e2; 
    \t\tvar realisasi_modal_dprd = Math.round(data[18][1]/1000000 *1e2)/1e2;
    \t\tvar anggaran_pegawai_dprd = Math.round(data[18][2]/1000000 *1e2)/1e2;
    \t\tvar realisasi_pegawai_dprd = Math.round(data[18][3]/1000000 *1e2)/1e2;
    \t\tvar anggaran_barang_dprd = Math.round(data[18][4]/1000000 *1e2)/1e2;
    \t\tvar realisasi_barang_dprd = Math.round(data[18][5]/1000000 *1e2)/1e2;

    \t\tvar anggaran_modal_sekretariat_daerah = Math.round(data[19][0]/1000000 *1e2)/1e2; 
    \t\tvar realisasi_modal_sekretariat_daerah = Math.round(data[19][1]/1000000 *1e2)/1e2;
    \t\tvar anggaran_pegawai_sekretariat_daerah = Math.round(data[19][2]/1000000 *1e2)/1e2;
    \t\tvar realisasi_pegawai_sekretariat_daerah = Math.round(data[19][3]/1000000 *1e2)/1e2;
    \t\tvar anggaran_barang_sekretariat_daerah = Math.round(data[19][4]/1000000 *1e2)/1e2;
    \t\tvar realisasi_barang_sekretariat_daerah = Math.round(data[19][5]/1000000 *1e2)/1e2;

    \t\tvar anggaran_modal_sekretariat_dprd = Math.round(data[20][0]/1000000 *1e2)/1e2; 
    \t\tvar realisasi_modal_sekretariat_dprd = Math.round(data[20][1]/1000000 *1e2)/1e2;
    \t\tvar anggaran_pegawai_sekretariat_dprd = Math.round(data[20][2]/1000000 *1e2)/1e2;
    \t\tvar realisasi_pegawai_sekretariat_dprd = Math.round(data[20][3]/1000000 *1e2)/1e2;
    \t\tvar anggaran_barang_sekretariat_dprd = Math.round(data[20][4]/1000000 *1e2)/1e2;
    \t\tvar realisasi_barang_sekretariat_dprd = Math.round(data[20][5]/1000000 *1e2)/1e2;

    \t\tvar anggaran_modal_badan_perijinan_terpadu = Math.round(data[21][0]/1000000 *1e2)/1e2; 
    \t\tvar realisasi_modal_badan_perijinan_terpadu = Math.round(data[21][1]/1000000 *1e2)/1e2;
    \t\tvar anggaran_pegawai_badan_perijinan_terpadu = Math.round(data[21][2]/1000000 *1e2)/1e2;
    \t\tvar realisasi_pegawai_badan_perijinan_terpadu = Math.round(data[21][3]/1000000 *1e2)/1e2;
    \t\tvar anggaran_barang_badan_perijinan_terpadu = Math.round(data[21][4]/1000000 *1e2)/1e2;
    \t\tvar realisasi_barang_badan_perijinan_terpadu = Math.round(data[21][5]/1000000 *1e2)/1e2;

    \t\tvar anggaran_modal_badan_kepegawaian = Math.round(data[22][0]/1000000 *1e2)/1e2; 
    \t\tvar realisasi_modal_badan_kepegawaian = Math.round(data[22][1]/1000000 *1e2)/1e2;
    \t\tvar anggaran_pegawai_badan_kepegawaian = Math.round(data[22][2]/1000000 *1e2)/1e2;
    \t\tvar realisasi_pegawai_badan_kepegawaian = Math.round(data[22][3]/1000000 *1e2)/1e2;
    \t\tvar anggaran_barang_badan_kepegawaian = Math.round(data[22][4]/1000000 *1e2)/1e2;
    \t\tvar realisasi_barang_badan_kepegawaian = Math.round(data[22][5]/1000000 *1e2)/1e2;

    \t\tvar anggaran_modal_inspektorat_kabupaten = Math.round(data[23][0]/1000000 *1e2)/1e2; 
    \t\tvar realisasi_modal_inspektorat_kabupaten = Math.round(data[23][1]/1000000 *1e2)/1e2;
    \t\tvar anggaran_pegawai_inspektorat_kabupaten = Math.round(data[23][2]/1000000 *1e2)/1e2;
    \t\tvar realisasi_pegawai_inspektorat_kabupaten = Math.round(data[23][3]/1000000 *1e2)/1e2;
    \t\tvar anggaran_barang_inspektorat_kabupaten = Math.round(data[23][4]/1000000 *1e2)/1e2;
    \t\tvar realisasi_barang_inspektorat_kabupaten = Math.round(data[23][5]/1000000 *1e2)/1e2;

    \t\tvar anggaran_modal_satpol_pp = Math.round(data[24][0]/1000000 *1e2)/1e2; 
    \t\tvar realisasi_modal_satpol_pp = Math.round(data[24][1]/1000000 *1e2)/1e2;
    \t\tvar anggaran_pegawai_satpol_pp = Math.round(data[24][2]/1000000 *1e2)/1e2;
    \t\tvar realisasi_pegawai_satpol_pp = Math.round(data[24][3]/1000000 *1e2)/1e2;
    \t\tvar anggaran_barang_satpol_pp = Math.round(data[24][4]/1000000 *1e2)/1e2;
    \t\tvar realisasi_barang_satpol_pp = Math.round(data[24][5]/1000000 *1e2)/1e2;

    \t\tvar anggaran_modal_kec_sooko = Math.round(data[25][0]/1000000 *1e2)/1e2; 
    \t\tvar realisasi_modal_kec_sooko = Math.round(data[25][1]/1000000 *1e2)/1e2;
    \t\tvar anggaran_pegawai_kec_sooko = Math.round(data[25][2]/1000000 *1e2)/1e2;
    \t\tvar realisasi_pegawai_kec_sooko = Math.round(data[25][3]/1000000 *1e2)/1e2;
    \t\tvar anggaran_barang_kec_sooko = Math.round(data[25][4]/1000000 *1e2)/1e2;
    \t\tvar realisasi_barang_kec_sooko = Math.round(data[25][5]/1000000 *1e2)/1e2;

    \t\tvar anggaran_modal_kec_trowulan = Math.round(data[26][0]/1000000 *1e2)/1e2; 
    \t\tvar realisasi_modal_kec_trowulan = Math.round(data[26][1]/1000000 *1e2)/1e2;
    \t\tvar anggaran_pegawai_kec_trowulan = Math.round(data[26][2]/1000000 *1e2)/1e2;
    \t\tvar realisasi_pegawai_kec_trowulan = Math.round(data[26][3]/1000000 *1e2)/1e2;
    \t\tvar anggaran_barang_kec_trowulan = Math.round(data[26][4]/1000000 *1e2)/1e2;
    \t\tvar realisasi_barang_kec_trowulan = Math.round(data[26][5]/1000000 *1e2)/1e2;

    \t\tvar anggaran_modal_kec_puri = Math.round(data[27][0]/1000000 *1e2)/1e2; 
    \t\tvar realisasi_modal_kec_puri = Math.round(data[27][1]/1000000 *1e2)/1e2;
    \t\tvar anggaran_pegawai_kec_puri = Math.round(data[27][2]/1000000 *1e2)/1e2;
    \t\tvar realisasi_pegawai_kec_puri = Math.round(data[27][3]/1000000 *1e2)/1e2;
    \t\tvar anggaran_barang_kec_puri = Math.round(data[27][4]/1000000 *1e2)/1e2;
    \t\tvar realisasi_barang_kec_puri = Math.round(data[27][5]/1000000 *1e2)/1e2;

    \t\tvar anggaran_modal_kec_bangsal = Math.round(data[28][0]/1000000 *1e2)/1e2; 
    \t\tvar realisasi_modal_kec_bangsal = Math.round(data[28][1]/1000000 *1e2)/1e2;
    \t\tvar anggaran_pegawai_kec_bangsal = Math.round(data[28][2]/1000000 *1e2)/1e2;
    \t\tvar realisasi_pegawai_kec_bangsal = Math.round(data[28][3]/1000000 *1e2)/1e2;
    \t\tvar anggaran_barang_kec_bangsal = Math.round(data[28][4]/1000000 *1e2)/1e2;
    \t\tvar realisasi_barang_kec_bangsal = Math.round(data[28][5]/1000000 *1e2)/1e2;

    \t\tvar anggaran_modal_kec_gedeg = Math.round(data[29][0]/1000000 *1e2)/1e2; 
    \t\tvar realisasi_modal_kec_gedeg = Math.round(data[29][1]/1000000 *1e2)/1e2;
    \t\tvar anggaran_pegawai_kec_gedeg = Math.round(data[29][2]/1000000 *1e2)/1e2;
    \t\tvar realisasi_pegawai_kec_gedeg = Math.round(data[29][3]/1000000 *1e2)/1e2;
    \t\tvar anggaran_barang_kec_gedeg = Math.round(data[29][4]/1000000 *1e2)/1e2;
    \t\tvar realisasi_barang_kec_gedeg = Math.round(data[29][5]/1000000 *1e2)/1e2;

    \t\tvar anggaran_modal_kec_kemlagi = Math.round(data[30][0]/1000000 *1e2)/1e2; 
    \t\tvar realisasi_modal_kec_kemlagi = Math.round(data[30][1]/1000000 *1e2)/1e2;
    \t\tvar anggaran_pegawai_kec_kemlagi = Math.round(data[30][2]/1000000 *1e2)/1e2;
    \t\tvar realisasi_pegawai_kec_kemlagi = Math.round(data[30][3]/1000000 *1e2)/1e2;
    \t\tvar anggaran_barang_kec_kemlagi = Math.round(data[30][4]/1000000 *1e2)/1e2;
    \t\tvar realisasi_barang_kec_kemlagi = Math.round(data[30][5]/1000000 *1e2)/1e2;

    \t\tvar anggaran_modal_kec_jetis = Math.round(data[31][0]/1000000 *1e2)/1e2; 
    \t\tvar realisasi_modal_kec_jetis = Math.round(data[31][1]/1000000 *1e2)/1e2;
    \t\tvar anggaran_pegawai_kec_jetis = Math.round(data[31][2]/1000000 *1e2)/1e2;
    \t\tvar realisasi_pegawai_kec_jetis = Math.round(data[31][3]/1000000 *1e2)/1e2;
    \t\tvar anggaran_barang_kec_jetis = Math.round(data[31][4]/1000000 *1e2)/1e2;
    \t\tvar realisasi_barang_kec_jetis = Math.round(data[31][5]/1000000 *1e2)/1e2;

    \t\tvar anggaran_modal_kec_dawar = Math.round(data[32][0]/1000000 *1e2)/1e2; 
    \t\tvar realisasi_modal_kec_dawar = Math.round(data[32][1]/1000000 *1e2)/1e2;
    \t\tvar anggaran_pegawai_kec_dawar = Math.round(data[32][2]/1000000 *1e2)/1e2;
    \t\tvar realisasi_pegawai_kec_dawar = Math.round(data[32][3]/1000000 *1e2)/1e2;
    \t\tvar anggaran_barang_kec_dawar = Math.round(data[32][4]/1000000 *1e2)/1e2;
    \t\tvar realisasi_barang_kec_dawar = Math.round(data[32][5]/1000000 *1e2)/1e2;

    \t\tvar anggaran_modal_kec_mojosari = Math.round(data[33][0]/1000000 *1e2)/1e2; 
    \t\tvar realisasi_modal_kec_mojosari = Math.round(data[33][1]/1000000 *1e2)/1e2;
    \t\tvar anggaran_pegawai_kec_mojosari = Math.round(data[33][2]/1000000 *1e2)/1e2;
    \t\tvar realisasi_pegawai_kec_mojosari = Math.round(data[33][3]/1000000 *1e2)/1e2;
    \t\tvar anggaran_barang_kec_mojosari = Math.round(data[33][4]/1000000 *1e2)/1e2;
    \t\tvar realisasi_barang_kec_mojosari = Math.round(data[33][5]/1000000 *1e2)/1e2;

    \t\tvar anggaran_modal_kec_pungging = Math.round(data[34][0]/1000000 *1e2)/1e2; 
    \t\tvar realisasi_modal_kec_pungging = Math.round(data[34][1]/1000000 *1e2)/1e2;
    \t\tvar anggaran_pegawai_kec_pungging = Math.round(data[34][2]/1000000 *1e2)/1e2;
    \t\tvar realisasi_pegawai_kec_pungging = Math.round(data[34][3]/1000000 *1e2)/1e2;
    \t\tvar anggaran_barang_kec_pungging = Math.round(data[34][4]/1000000 *1e2)/1e2;
    \t\tvar realisasi_barang_kec_pungging = Math.round(data[34][5]/1000000 *1e2)/1e2;

    \t\tvar anggaran_modal_kec_ngoro = Math.round(data[35][0]/1000000 *1e2)/1e2; 
    \t\tvar realisasi_modal_kec_ngoro = Math.round(data[35][1]/1000000 *1e2)/1e2;
    \t\tvar anggaran_pegawai_kec_ngoro = Math.round(data[35][2]/1000000 *1e2)/1e2;
    \t\tvar realisasi_pegawai_kec_ngoro = Math.round(data[35][3]/1000000 *1e2)/1e2;
    \t\tvar anggaran_barang_kec_ngoro = Math.round(data[35][4]/1000000 *1e2)/1e2;
    \t\tvar realisasi_barang_kec_ngoro = Math.round(data[35][5]/1000000 *1e2)/1e2;

    \t\tvar anggaran_modal_kec_kutorejo = Math.round(data[36][0]/1000000 *1e2)/1e2; 
    \t\tvar realisasi_modal_kec_kutorejo = Math.round(data[36][1]/1000000 *1e2)/1e2;
    \t\tvar anggaran_pegawai_kec_kutorejo = Math.round(data[36][2]/1000000 *1e2)/1e2;
    \t\tvar realisasi_pegawai_kec_kutorejo = Math.round(data[36][3]/1000000 *1e2)/1e2;
    \t\tvar anggaran_barang_kec_kutorejo = Math.round(data[36][4]/1000000 *1e2)/1e2;
    \t\tvar realisasi_barang_kec_kutorejo = Math.round(data[36][5]/1000000 *1e2)/1e2;

    \t\tvar anggaran_modal_kec_dlanggu = Math.round(data[37][0]/1000000 *1e2)/1e2; 
    \t\tvar realisasi_modal_kec_dlanggu = Math.round(data[37][1]/1000000 *1e2)/1e2;
    \t\tvar anggaran_pegawai_kec_dlanggu = Math.round(data[37][2]/1000000 *1e2)/1e2;
    \t\tvar realisasi_pegawai_kec_dlanggu = Math.round(data[37][3]/1000000 *1e2)/1e2;
    \t\tvar anggaran_barang_kec_dlanggu = Math.round(data[37][4]/1000000 *1e2)/1e2;
    \t\tvar realisasi_barang_kec_dlanggu = Math.round(data[37][5]/1000000 *1e2)/1e2;

    \t\tvar anggaran_modal_kec_gondang = Math.round(data[38][0]/1000000 *1e2)/1e2; 
    \t\tvar realisasi_modal_kec_gondang = Math.round(data[38][1]/1000000 *1e2)/1e2;
    \t\tvar anggaran_pegawai_kec_gondang = Math.round(data[38][2]/1000000 *1e2)/1e2;
    \t\tvar realisasi_pegawai_kec_gondang = Math.round(data[38][3]/1000000 *1e2)/1e2;
    \t\tvar anggaran_barang_kec_gondang = Math.round(data[38][4]/1000000 *1e2)/1e2;
    \t\tvar realisasi_barang_kec_gondang = Math.round(data[38][5]/1000000 *1e2)/1e2;

    \t\tvar anggaran_modal_kec_jatirejo = Math.round(data[39][0]/1000000 *1e2)/1e2; 
    \t\tvar realisasi_modal_kec_jatirejo = Math.round(data[39][1]/1000000 *1e2)/1e2;
    \t\tvar anggaran_pegawai_kec_jatirejo = Math.round(data[39][2]/1000000 *1e2)/1e2;
    \t\tvar realisasi_pegawai_kec_jatirejo = Math.round(data[39][3]/1000000 *1e2)/1e2;
    \t\tvar anggaran_barang_kec_jatirejo = Math.round(data[39][4]/1000000 *1e2)/1e2;
    \t\tvar realisasi_barang_kec_jatirejo = Math.round(data[39][5]/1000000 *1e2)/1e2;

    \t\tvar anggaran_modal_kec_trawas = Math.round(data[40][0]/1000000 *1e2)/1e2; 
    \t\tvar realisasi_modal_kec_trawas = Math.round(data[40][1]/1000000 *1e2)/1e2;
    \t\tvar anggaran_pegawai_kec_trawas = Math.round(data[40][2]/1000000 *1e2)/1e2;
    \t\tvar realisasi_pegawai_kec_trawas = Math.round(data[40][3]/1000000 *1e2)/1e2;
    \t\tvar anggaran_barang_kec_trawas = Math.round(data[40][4]/1000000 *1e2)/1e2;
    \t\tvar realisasi_barang_kec_trawas = Math.round(data[40][5]/1000000 *1e2)/1e2;

    \t\tvar anggaran_modal_kec_pacet = Math.round(data[41][0]/1000000 *1e2)/1e2; 
    \t\tvar realisasi_modal_kec_pacet = Math.round(data[41][1]/1000000 *1e2)/1e2;
    \t\tvar anggaran_pegawai_kec_pacet = Math.round(data[41][2]/1000000 *1e2)/1e2;
    \t\tvar realisasi_pegawai_kec_pacet = Math.round(data[41][3]/1000000 *1e2)/1e2;
    \t\tvar anggaran_barang_kec_pacet = Math.round(data[41][4]/1000000 *1e2)/1e2;
    \t\tvar realisasi_barang_kec_pacet = Math.round(data[41][5]/1000000 *1e2)/1e2;

    \t\tvar anggaran_modal_kec_mojoanyar = Math.round(data[42][0]/1000000 *1e2)/1e2; 
    \t\tvar realisasi_modal_kec_mojoanyar = Math.round(data[42][1]/1000000 *1e2)/1e2;
    \t\tvar anggaran_pegawai_kec_mojoanyar = Math.round(data[42][2]/1000000 *1e2)/1e2;
    \t\tvar realisasi_pegawai_kec_mojoanyar = Math.round(data[42][3]/1000000 *1e2)/1e2;
    \t\tvar anggaran_barang_kec_mojoanyar = Math.round(data[42][4]/1000000 *1e2)/1e2;
    \t\tvar realisasi_barang_kec_mojoanyar = Math.round(data[42][5]/1000000 *1e2)/1e2;

    \t\tvar anggaran_modal_bpka = Math.round(data[43][0]/1000000 *1e2)/1e2; 
    \t\tvar realisasi_modal_bpka = Math.round(data[43][1]/1000000 *1e2)/1e2;
    \t\tvar anggaran_pegawai_bpka = Math.round(data[43][2]/1000000 *1e2)/1e2;
    \t\tvar realisasi_pegawai_bpka = Math.round(data[43][3]/1000000 *1e2)/1e2;
    \t\tvar anggaran_barang_bpka = Math.round(data[43][4]/1000000 *1e2)/1e2;
    \t\tvar realisasi_barang_bpka = Math.round(data[43][5]/1000000 *1e2)/1e2;

    \t\tvar anggaran_modal_bpbd = Math.round(data[44][0]/1000000 *1e2)/1e2; 
    \t\tvar realisasi_modal_bpbd = Math.round(data[44][1]/1000000 *1e2)/1e2;
    \t\tvar anggaran_pegawai_bpbd = Math.round(data[44][2]/1000000 *1e2)/1e2;
    \t\tvar realisasi_pegawai_bpbd = Math.round(data[44][3]/1000000 *1e2)/1e2;
    \t\tvar anggaran_barang_bpbd = Math.round(data[44][4]/1000000 *1e2)/1e2;
    \t\tvar realisasi_barang_bpbd = Math.round(data[44][5]/1000000 *1e2)/1e2;

    \t\tvar anggaran_modal_dinas_pendapatan = Math.round(data[45][0]/1000000 *1e2)/1e2; 
    \t\tvar realisasi_modal_dinas_pendapatan = Math.round(data[45][1]/1000000 *1e2)/1e2;
    \t\tvar anggaran_pegawai_dinas_pendapatan = Math.round(data[45][2]/1000000 *1e2)/1e2;
    \t\tvar realisasi_pegawai_dinas_pendapatan = Math.round(data[45][3]/1000000 *1e2)/1e2;
    \t\tvar anggaran_barang_dinas_pendapatan = Math.round(data[45][4]/1000000 *1e2)/1e2;
    \t\tvar realisasi_barang_dinas_pendapatan = Math.round(data[45][5]/1000000 *1e2)/1e2;

    \t\tvar anggaran_modal_kantor_ketahanan_pangan = Math.round(data[46][0]/1000000 *1e2)/1e2; 
    \t\tvar realisasi_modal_kantor_ketahanan_pangan = Math.round(data[46][1]/1000000 *1e2)/1e2;
    \t\tvar anggaran_pegawai_kantor_ketahanan_pangan = Math.round(data[46][2]/1000000 *1e2)/1e2;
    \t\tvar realisasi_pegawai_kantor_ketahanan_pangan = Math.round(data[46][3]/1000000 *1e2)/1e2;
    \t\tvar anggaran_barang_kantor_ketahanan_pangan = Math.round(data[46][4]/1000000 *1e2)/1e2;
    \t\tvar realisasi_barang_kantor_ketahanan_pangan = Math.round(data[46][5]/1000000 *1e2)/1e2;

    \t\tvar anggaran_modal_badan_pemberdayaan_masyarakat = Math.round(data[47][0]/1000000 *1e2)/1e2; 
    \t\tvar realisasi_modal_badan_pemberdayaan_masyarakat = Math.round(data[47][1]/1000000 *1e2)/1e2;
    \t\tvar anggaran_pegawai_badan_pemberdayaan_masyarakat = Math.round(data[47][2]/1000000 *1e2)/1e2;
    \t\tvar realisasi_pegawai_badan_pemberdayaan_masyarakat = Math.round(data[47][3]/1000000 *1e2)/1e2;
    \t\tvar anggaran_barang_badan_pemberdayaan_masyarakat = Math.round(data[47][4]/1000000 *1e2)/1e2;
    \t\tvar realisasi_barang_badan_pemberdayaan_masyarakat = Math.round(data[47][5]/1000000 *1e2)/1e2;

    \t\tvar anggaran_modal_kantor_perpustakaan = Math.round(data[48][0]/1000000 *1e2)/1e2; 
    \t\tvar realisasi_modal_kantor_perpustakaan = Math.round(data[48][1]/1000000 *1e2)/1e2;
    \t\tvar anggaran_pegawai_kantor_perpustakaan = Math.round(data[48][2]/1000000 *1e2)/1e2;
    \t\tvar realisasi_pegawai_kantor_perpustakaan = Math.round(data[48][3]/1000000 *1e2)/1e2;
    \t\tvar anggaran_barang_kantor_perpustakaan = Math.round(data[48][4]/1000000 *1e2)/1e2;
    \t\tvar realisasi_barang_kantor_perpustakaan = Math.round(data[48][5]/1000000 *1e2)/1e2;

    \t\tvar anggaran_modal_dinas_pertanian = Math.round(data[49][0]/1000000 *1e2)/1e2; 
    \t\tvar realisasi_modal_dinas_pertanian = Math.round(data[49][1]/1000000 *1e2)/1e2;
    \t\tvar anggaran_pegawai_dinas_pertanian = Math.round(data[49][2]/1000000 *1e2)/1e2;
    \t\tvar realisasi_pegawai_dinas_pertanian = Math.round(data[49][3]/1000000 *1e2)/1e2;
    \t\tvar anggaran_barang_dinas_pertanian = Math.round(data[49][4]/1000000 *1e2)/1e2;
    \t\tvar realisasi_barang_dinas_pertanian = Math.round(data[49][5]/1000000 *1e2)/1e2;

    \t\tvar anggaran_modal_dinas_peternakan = Math.round(data[50][0]/1000000 *1e2)/1e2; 
    \t\tvar realisasi_modal_dinas_peternakan = Math.round(data[50][1]/1000000 *1e2)/1e2;
    \t\tvar anggaran_pegawai_dinas_peternakan = Math.round(data[50][2]/1000000 *1e2)/1e2;
    \t\tvar realisasi_pegawai_dinas_peternakan = Math.round(data[50][3]/1000000 *1e2)/1e2;
    \t\tvar anggaran_barang_dinas_peternakan = Math.round(data[50][4]/1000000 *1e2)/1e2;
    \t\tvar realisasi_barang_dinas_peternakan = Math.round(data[50][5]/1000000 *1e2)/1e2;

    \t\tvar anggaran_modal_dinas_kehutanan = Math.round(data[51][0]/1000000 *1e2)/1e2; 
    \t\tvar realisasi_modal_dinas_kehutanan = Math.round(data[51][1]/1000000 *1e2)/1e2;
    \t\tvar anggaran_pegawai_dinas_kehutanan = Math.round(data[51][2]/1000000 *1e2)/1e2;
    \t\tvar realisasi_pegawai_dinas_kehutanan = Math.round(data[51][3]/1000000 *1e2)/1e2;
    \t\tvar anggaran_barang_dinas_kehutanan = Math.round(data[51][4]/1000000 *1e2)/1e2;
    \t\tvar realisasi_barang_dinas_kehutanan = Math.round(data[51][5]/1000000 *1e2)/1e2;

    \t\tvar anggaran_modal_dinas_perindustrian = Math.round(data[52][0]/1000000 *1e2)/1e2; 
    \t\tvar realisasi_modal_dinas_perindustrian = Math.round(data[52][1]/1000000 *1e2)/1e2;
    \t\tvar anggaran_pegawai_dinas_perindustrian = Math.round(data[52][2]/1000000 *1e2)/1e2;
    \t\tvar realisasi_pegawai_dinas_perindustrian = Math.round(data[52][3]/1000000 *1e2)/1e2;
    \t\tvar anggaran_barang_dinas_perindustrian = Math.round(data[52][4]/1000000 *1e2)/1e2;
    \t\tvar realisasi_barang_dinas_perindustrian = Math.round(data[52][5]/1000000 *1e2)/1e2;

    \t\t
    \t\t\$('#realisasi-pembanding-chart').highcharts({
\t\t        chart: {
\t\t            type: 'bar'
\t\t        },
\t\t        title: {
\t\t            text: 'Perbandingan LRA antar SKPD'
\t\t        },
\t\t        xAxis: {
\t\t            categories: ['Dinas Pendidikan', 'Dinas Kesehatan', 'RSUD. Prof. Dr. Soekandar', 'RSUD RA. Basoeni', 'Dinas PU Pengairan', 'Dinas PU Bina Marga', 'Dinas PU Cipta Karya dan Tata Ruang', 'BAPPEDA', 'Dinas Perhubungan, Komunikasi, dan Informatika', 'Badan Lingkungan Hidup', 'Dinas Kependudukan dan Catatan Sipil', 'Badan Pemberdayaan Perempuan dan KB', 'Dinas Sosial', 'Dinas Tenaga Kerja dan Transmigrasi', 'Dinas Kooperasi dan UKM', 'Dinas Pemuda, Olahraga, Kebudayaan, dan Pariwisata', 'Badan Kesatuan Bangsa dan Politik', 'Kepala Daerah dan WKDh', 'DPRD', 'Sekretariat Daerah', 'Sekretariat DPRD', 'Badan Perijinan Terpadu dan Penanaman Modal', 'Badan Kepegawaian, Pendidikan dan Pelatihan', 'Inspektorat Kabupaten', 'Satuan Polisi Pamong Praja', 'Kecamatan Sooko', 'Kecamatan Trowulan', 'Kecamatan Puri', 'Kecamatan Bangsal', 'Kecamatan Gedeg', 'Kecamatan Kemlagi', 'Kecamatan Jetis', 'Kecamatan Dawarblandong', 'Kecamatan Mojosari', 'Kecamatan Pungging', 'Kecamatan Ngoro', 'Kecamatan Kutorejo', 'Kecamatan Dlanggu', 'Kecamatan Gondang', 'Kecamatan Jatirejo', 'Kecamatan Trawas', 'Kecamatan Pacet', 'Kecamatan Mojoanyar', 'Badan Pengelolaan Keuangan dan Aset', 'Badan Penanggulangan Bencana Daerah', 'Dinas Pendapatan ', 'Kantor Ketahanan Pangan', 'Badan Pemberdayaan Masyarakat', 'Kantor Perpustakaan, Arsip, dan Dokumentasi', 'Dinas Pertanian', 'Dinas Peternakan dan Perikanan', 'Dinas Kehutanan dan Perkebunan', 'Dinas Perindustrian dan Perdagangan'],
\t\t            title: {
\t\t                text: null
\t\t            }
\t\t        },
\t\t        yAxis: {
\t\t            min: 0,
\t\t            title: {
\t\t                text: 'Jumlah',
\t\t                align: 'high'
\t\t            },
\t\t            labels: {
\t\t                overflow: 'justify'
\t\t            }, 
\t\t        },
\t\t        tooltip: {
\t\t            formatter: function () {
\t\t\t            return 'Nilai: <b>' + n(this.point.nilai) + ' Juta</b>';
\t\t\t        }
\t\t        },
\t\t        plotOptions: {
\t\t            bar: {
\t                    pointPadding: 0.1,
\t                    borderWidth: 0,
\t\t                dataLabels: {
\t\t                    enabled: true
\t\t                }
\t\t            }, series: {
\t\t                groupPadding: 0.05
\t\t            }
\t\t        },
\t\t        legend: {
\t\t            layout: 'vertical',
\t\t            align: 'right',
\t\t            verticalAlign: 'top',
\t\t            x: 0,
\t\t            y: 80,
\t\t            floating: false,
\t\t            borderWidth: 1,
\t\t            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
\t\t            shadow: true
\t\t        }, 
\t\t        exporting: {
\t\t                filename: 'Export chart',
\t\t                chartOptions:{
\t\t                    title:{
\t\t                        text:'Perbandingan Realisasi Antar SKPD'
\t\t                    }
\t\t                }, buttons: {
\t\t                    exportButton: {
\t\t                        menuItems: [{
\t\t                            text: 'Export Chart'
\t\t                        }, 
\t\t                        { 
\t\t                            text: 'Print Chart',
\t\t                            onclick: function () {
\t\t                            \tchart.print();
\t\t                            }
\t\t                        },
\t\t                    null,
\t\t                    null]
\t\t                }
\t\t            }
\t\t        },
\t\t        credits: {
\t\t            enabled: false
\t\t        },
\t\t        series: [{
\t\t            name: 'Realisasi Barang dan Jasa',
\t\t            data: [{y: persen_realisasi_barang[0], nilai: realisasi_barang_dinas_pendidikan}, {y: persen_realisasi_barang[1], nilai: realisasi_barang_dinas_kesehatan}, {y: persen_realisasi_barang[2], nilai: realisasi_barang_rsud_soekandar}, {y: persen_realisasi_barang[3], nilai: realisasi_barang_rsud_basoeni}, {y: persen_realisasi_barang[4], nilai: realisasi_barang_dinas_pu_pengairan}, {y: persen_realisasi_barang[5], nilai: realisasi_barang_dinas_pu_bina_marga}, {y: persen_realisasi_barang[6], nilai: realisasi_barang_dinas_pu_cipta_karya}, {y: persen_realisasi_barang[7], nilai: realisasi_barang_bappeda}, {y: persen_realisasi_barang[8], nilai: realisasi_barang_dinas_perhubungan}, {y: persen_realisasi_barang[9], nilai: realisasi_barang_badan_lingkungan_hidup}, {y: persen_realisasi_barang[10], nilai: realisasi_barang_dinas_kependudukan}, {y: persen_realisasi_barang[11], nilai: realisasi_barang_badan_pemberdayaan_perempuan}, {y: persen_realisasi_barang[12], nilai: realisasi_barang_dinas_sosial}, {y: persen_realisasi_barang[13], nilai: realisasi_barang_dinas_tenaga_kerja}, {y: persen_realisasi_barang[14], nilai: realisasi_barang_dinas_kooperasi}, {y: persen_realisasi_barang[15], nilai: realisasi_barang_dinas_pemuda}, {y: persen_realisasi_barang[16], nilai: realisasi_barang_badan_kesatuan_bangsa}, {y: persen_realisasi_barang[17], nilai: realisasi_barang_kepala_daerah}, {y: persen_realisasi_barang[18], nilai: realisasi_barang_dprd}, {y: persen_realisasi_barang[19], nilai: realisasi_barang_sekretariat_daerah}, {y: persen_realisasi_barang[20], nilai: realisasi_barang_sekretariat_dprd}, {y: persen_realisasi_barang[21], nilai: realisasi_barang_badan_perijinan_terpadu}, {y: persen_realisasi_barang[22], nilai: realisasi_barang_badan_kepegawaian}, {y: persen_realisasi_barang[23], nilai: realisasi_barang_inspektorat_kabupaten}, {y: persen_realisasi_barang[24], nilai: realisasi_barang_satpol_pp}, {y: persen_realisasi_barang[25], nilai: realisasi_barang_kec_sooko}, {y: persen_realisasi_barang[26], nilai: realisasi_barang_kec_trowulan}, {y: persen_realisasi_barang[27], nilai: realisasi_barang_kec_puri}, {y: persen_realisasi_barang[28], nilai: realisasi_barang_kec_bangsal}, {y: persen_realisasi_barang[29], nilai: realisasi_barang_kec_gedeg}, {y: persen_realisasi_barang[30], nilai: realisasi_barang_kec_kemlagi}, {y: persen_realisasi_barang[31], nilai: realisasi_barang_kec_jetis}, {y: persen_realisasi_barang[32], nilai: realisasi_barang_kec_dawar}, {y: persen_realisasi_barang[33], nilai: realisasi_barang_kec_mojosari}, {y: persen_realisasi_barang[34], nilai: realisasi_barang_kec_pungging}, {y: persen_realisasi_barang[35], nilai: realisasi_barang_kec_ngoro}, {y: persen_realisasi_barang[36], nilai: realisasi_barang_kec_kutorejo}, {y: persen_realisasi_barang[37], nilai: realisasi_barang_kec_dlanggu}, {y: persen_realisasi_barang[38], nilai: realisasi_barang_kec_gondang}, {y: persen_realisasi_barang[39], nilai: realisasi_barang_kec_jatirejo}, {y: persen_realisasi_barang[40], nilai: realisasi_barang_kec_trawas}, {y: persen_realisasi_barang[41], nilai: realisasi_barang_kec_pacet}, {y: persen_realisasi_barang[42], nilai: realisasi_barang_kec_mojoanyar}, {y: persen_realisasi_barang[43], nilai: realisasi_barang_bpka}, {y: persen_realisasi_barang[44], nilai: realisasi_barang_bpbd}, {y: persen_realisasi_barang[45], nilai: realisasi_barang_dinas_pendapatan}, {y: persen_realisasi_barang[46], nilai: realisasi_barang_kantor_ketahanan_pangan}, {y: persen_realisasi_barang[47], nilai: realisasi_barang_badan_pemberdayaan_masyarakat}, {y: persen_realisasi_barang[48], nilai: realisasi_barang_kantor_perpustakaan}, {y: persen_realisasi_barang[49], nilai: realisasi_barang_dinas_pertanian}, {y: persen_realisasi_barang[50], nilai: realisasi_barang_dinas_peternakan}, {y: persen_realisasi_barang[51], nilai: realisasi_barang_dinas_kehutanan}, {y: persen_realisasi_barang[52], nilai: realisasi_barang_dinas_perindustrian}],
\t\t            legendIndex:5
\t\t        }, {
\t\t            name: 'Anggaran Barang dan Jasa',
\t\t            data: [{y: 100, nilai: anggaran_barang_dinas_pendidikan}, {y: 100, nilai: anggaran_barang_dinas_kesehatan}, {y: 100, nilai: anggaran_barang_rsud_soekandar}, {y: 100, nilai: anggaran_barang_rsud_basoeni}, {y: 100, nilai: anggaran_barang_dinas_pu_pengairan}, {y: 100, nilai: anggaran_barang_dinas_pu_bina_marga}, {y: 100, nilai: anggaran_barang_dinas_pu_cipta_karya}, {y: 100, nilai: anggaran_barang_bappeda}, {y: 100, nilai: anggaran_barang_dinas_perhubungan}, {y: 100, nilai: anggaran_barang_badan_lingkungan_hidup}, {y: 100, nilai: anggaran_barang_dinas_kependudukan}, {y: 100, nilai: anggaran_barang_badan_pemberdayaan_perempuan}, {y: 100, nilai: anggaran_barang_dinas_sosial}, {y: 100, nilai: anggaran_barang_dinas_tenaga_kerja}, {y: 100, nilai: anggaran_barang_dinas_kooperasi}, {y: 100, nilai: anggaran_barang_dinas_pemuda}, {y: 100, nilai: anggaran_barang_badan_kesatuan_bangsa}, {y: 100, nilai: anggaran_barang_kepala_daerah}, {y: 100, nilai: anggaran_barang_dprd}, {y: 100, nilai: anggaran_barang_sekretariat_daerah}, {y: 100, nilai: anggaran_barang_sekretariat_dprd}, {y: 100, nilai: anggaran_barang_badan_perijinan_terpadu}, {y: 100, nilai: anggaran_barang_badan_kepegawaian}, {y: 100, nilai: anggaran_barang_inspektorat_kabupaten}, {y: 100, nilai: anggaran_barang_satpol_pp}, {y: 100, nilai: anggaran_barang_kec_sooko}, {y: 100, nilai: anggaran_barang_kec_trowulan}, {y: 100, nilai: anggaran_barang_kec_puri}, {y: 100, nilai: anggaran_barang_kec_bangsal}, {y: 100, nilai: anggaran_barang_kec_gedeg}, {y: 100, nilai: anggaran_barang_kec_kemlagi}, {y: 100, nilai: anggaran_barang_kec_jetis}, {y: 100, nilai: anggaran_barang_kec_dawar}, {y: 100, nilai: anggaran_barang_kec_mojosari}, {y: 100, nilai: anggaran_barang_kec_pungging}, {y: 100, nilai: anggaran_barang_kec_ngoro}, {y: 100, nilai: anggaran_barang_kec_kutorejo}, {y: 100, nilai: anggaran_barang_kec_dlanggu}, {y: 100, nilai: anggaran_barang_kec_gondang}, {y: 100, nilai: anggaran_barang_kec_jatirejo}, {y: 100, nilai: anggaran_barang_kec_trawas}, {y: 100, nilai: anggaran_barang_kec_pacet}, {y: 100, nilai: anggaran_barang_kec_mojoanyar}, {y: 100, nilai: anggaran_barang_bpka}, {y: 100, nilai: anggaran_barang_bpbd}, {y: 100, nilai: anggaran_barang_dinas_pendapatan}, {y: 100, nilai: anggaran_barang_kantor_ketahanan_pangan}, {y: 100, nilai: anggaran_barang_badan_pemberdayaan_masyarakat}, {y: 100, nilai: anggaran_barang_kantor_perpustakaan}, {y: 100, nilai: anggaran_barang_dinas_pertanian}, {y: 100, nilai: anggaran_barang_dinas_peternakan}, {y: 100, nilai: anggaran_barang_dinas_kehutanan}, {y: 100, nilai: anggaran_barang_dinas_perindustrian}],
\t\t            legendIndex:4
\t\t        }, {
\t\t            name: 'Realisasi Pegawai',
\t\t            data: [{y: persen_realisasi_pegawai[0], nilai: realisasi_pegawai_dinas_pendidikan}, {y: persen_realisasi_pegawai[1], nilai: realisasi_pegawai_dinas_kesehatan}, {y: persen_realisasi_pegawai[2], nilai: realisasi_pegawai_rsud_soekandar}, {y: persen_realisasi_pegawai[3], nilai: realisasi_pegawai_rsud_basoeni}, {y: persen_realisasi_pegawai[4], nilai: realisasi_pegawai_dinas_pu_pengairan}, {y: persen_realisasi_pegawai[5], nilai: realisasi_pegawai_dinas_pu_bina_marga}, {y: persen_realisasi_pegawai[6], nilai: realisasi_pegawai_dinas_pu_cipta_karya}, {y: persen_realisasi_pegawai[7], nilai: realisasi_pegawai_bappeda}, {y: persen_realisasi_pegawai[8], nilai: realisasi_pegawai_dinas_perhubungan}, {y: persen_realisasi_pegawai[9], nilai: realisasi_pegawai_badan_lingkungan_hidup}, {y: persen_realisasi_pegawai[10], nilai: realisasi_pegawai_dinas_kependudukan}, {y: persen_realisasi_pegawai[11], nilai: realisasi_pegawai_badan_pemberdayaan_perempuan}, {y: persen_realisasi_pegawai[12], nilai: realisasi_pegawai_dinas_sosial}, {y: persen_realisasi_pegawai[13], nilai: realisasi_pegawai_dinas_tenaga_kerja}, {y: persen_realisasi_pegawai[14], nilai: realisasi_pegawai_dinas_kooperasi}, {y: persen_realisasi_pegawai[15], nilai: realisasi_pegawai_dinas_pemuda}, {y: persen_realisasi_pegawai[16], nilai: realisasi_pegawai_badan_kesatuan_bangsa}, {y: persen_realisasi_pegawai[17], nilai: realisasi_pegawai_kepala_daerah}, {y: persen_realisasi_pegawai[18], nilai: realisasi_pegawai_dprd}, {y: persen_realisasi_pegawai[19], nilai: realisasi_pegawai_sekretariat_daerah}, {y: persen_realisasi_pegawai[20], nilai: realisasi_pegawai_sekretariat_dprd}, {y: persen_realisasi_pegawai[21], nilai: realisasi_pegawai_badan_perijinan_terpadu}, {y: persen_realisasi_pegawai[22], nilai: realisasi_pegawai_badan_kepegawaian}, {y: persen_realisasi_pegawai[23], nilai: realisasi_pegawai_inspektorat_kabupaten}, {y: persen_realisasi_pegawai[24], nilai: realisasi_pegawai_satpol_pp}, {y: persen_realisasi_pegawai[25], nilai: realisasi_pegawai_kec_sooko}, {y: persen_realisasi_pegawai[26], nilai: realisasi_pegawai_kec_trowulan}, {y: persen_realisasi_pegawai[27], nilai: realisasi_pegawai_kec_puri}, {y: persen_realisasi_pegawai[28], nilai: realisasi_pegawai_kec_bangsal}, {y: persen_realisasi_pegawai[29], nilai: realisasi_pegawai_kec_gedeg}, {y: persen_realisasi_pegawai[30], nilai: realisasi_pegawai_kec_kemlagi}, {y: persen_realisasi_pegawai[31], nilai: realisasi_pegawai_kec_jetis}, {y: persen_realisasi_pegawai[32], nilai: realisasi_pegawai_kec_dawar}, {y: persen_realisasi_pegawai[33], nilai: realisasi_pegawai_kec_mojosari}, {y: persen_realisasi_pegawai[34], nilai: realisasi_pegawai_kec_pungging}, {y: persen_realisasi_pegawai[35], nilai: realisasi_pegawai_kec_ngoro}, {y: persen_realisasi_pegawai[36], nilai: realisasi_pegawai_kec_kutorejo}, {y: persen_realisasi_pegawai[37], nilai: realisasi_pegawai_kec_dlanggu}, {y: persen_realisasi_pegawai[38], nilai: realisasi_pegawai_kec_gondang}, {y: persen_realisasi_pegawai[39], nilai: realisasi_pegawai_kec_jatirejo}, {y: persen_realisasi_pegawai[40], nilai: realisasi_pegawai_kec_trawas}, {y: persen_realisasi_pegawai[41], nilai: realisasi_pegawai_kec_pacet}, {y: persen_realisasi_pegawai[42], nilai: realisasi_pegawai_kec_mojoanyar}, {y: persen_realisasi_pegawai[43], nilai: realisasi_pegawai_bpka}, {y: persen_realisasi_pegawai[44], nilai: realisasi_pegawai_bpbd}, {y: persen_realisasi_pegawai[45], nilai: realisasi_pegawai_dinas_pendapatan}, {y: persen_realisasi_pegawai[46], nilai: realisasi_pegawai_kantor_ketahanan_pangan}, {y: persen_realisasi_pegawai[47], nilai: realisasi_pegawai_badan_pemberdayaan_masyarakat}, {y: persen_realisasi_pegawai[48], nilai: realisasi_pegawai_kantor_perpustakaan}, {y: persen_realisasi_pegawai[49], nilai: realisasi_pegawai_dinas_pertanian}, {y: persen_realisasi_pegawai[50], nilai: realisasi_pegawai_dinas_peternakan}, {y: persen_realisasi_pegawai[51], nilai: realisasi_pegawai_dinas_kehutanan}, {y: persen_realisasi_pegawai[52], nilai: realisasi_pegawai_dinas_perindustrian}],
\t\t            legendIndex:3
\t\t        }, {
\t\t            name: 'Anggaran Pegawai',
\t\t            data: [{y: 100, nilai: anggaran_pegawai_dinas_pendidikan}, {y: 100, nilai: anggaran_pegawai_dinas_kesehatan}, {y: 100, nilai: anggaran_pegawai_rsud_soekandar}, {y: 100, nilai: anggaran_pegawai_rsud_basoeni}, {y: 100, nilai: anggaran_pegawai_dinas_pu_pengairan}, {y: 100, nilai: anggaran_pegawai_dinas_pu_bina_marga}, {y: 100, nilai: anggaran_pegawai_dinas_pu_cipta_karya}, {y: 100, nilai: anggaran_pegawai_bappeda}, {y: 100, nilai: anggaran_pegawai_dinas_perhubungan}, {y: 100, nilai: anggaran_pegawai_badan_lingkungan_hidup}, {y: 100, nilai: anggaran_pegawai_dinas_kependudukan}, {y: 100, nilai: anggaran_pegawai_badan_pemberdayaan_perempuan}, {y: 100, nilai: anggaran_pegawai_dinas_sosial}, {y: 100, nilai: anggaran_pegawai_dinas_tenaga_kerja}, {y: 100, nilai: anggaran_pegawai_dinas_kooperasi}, {y: 100, nilai: anggaran_pegawai_dinas_pemuda}, {y: 100, nilai: anggaran_pegawai_badan_kesatuan_bangsa}, {y: 100, nilai: anggaran_pegawai_kepala_daerah}, {y: 100, nilai: anggaran_pegawai_dprd}, {y: 100, nilai: anggaran_pegawai_sekretariat_daerah}, {y: 100, nilai: anggaran_pegawai_sekretariat_dprd}, {y: 100, nilai: anggaran_pegawai_badan_perijinan_terpadu}, {y: 100, nilai: anggaran_pegawai_badan_kepegawaian}, {y: 100, nilai: anggaran_pegawai_inspektorat_kabupaten}, {y: 100, nilai: anggaran_pegawai_satpol_pp}, {y: 100, nilai: anggaran_pegawai_kec_sooko}, {y: 100, nilai: anggaran_pegawai_kec_trowulan}, {y: 100, nilai: anggaran_pegawai_kec_puri}, {y: 100, nilai: anggaran_pegawai_kec_bangsal}, {y: 100, nilai: anggaran_pegawai_kec_gedeg}, {y: 100, nilai: anggaran_pegawai_kec_kemlagi}, {y: 100, nilai: anggaran_pegawai_kec_jetis}, {y: 100, nilai: anggaran_pegawai_kec_dawar}, {y: 100, nilai: anggaran_pegawai_kec_mojosari}, {y: 100, nilai: anggaran_pegawai_kec_pungging}, {y: 100, nilai: anggaran_pegawai_kec_ngoro}, {y: 100, nilai: anggaran_pegawai_kec_kutorejo}, {y: 100, nilai: anggaran_pegawai_kec_dlanggu}, {y: 100, nilai: anggaran_pegawai_kec_gondang}, {y: 100, nilai: anggaran_pegawai_kec_jatirejo}, {y: 100, nilai: anggaran_pegawai_kec_trawas}, {y: 100, nilai: anggaran_pegawai_kec_pacet}, {y: 100, nilai: anggaran_pegawai_kec_mojoanyar}, {y: 100, nilai: anggaran_pegawai_bpka}, {y: 100, nilai: anggaran_pegawai_bpbd}, {y: 100, nilai: anggaran_pegawai_dinas_pendapatan}, {y: 100, nilai: anggaran_pegawai_kantor_ketahanan_pangan}, {y: 100, nilai: anggaran_pegawai_badan_pemberdayaan_masyarakat}, {y: 100, nilai: anggaran_pegawai_kantor_perpustakaan}, {y: 100, nilai: anggaran_pegawai_dinas_pertanian}, {y: 100, nilai: anggaran_pegawai_dinas_peternakan}, {y: 100, nilai: anggaran_pegawai_dinas_kehutanan}, {y: 100, nilai: anggaran_pegawai_dinas_perindustrian}],
\t\t            legendIndex:2
\t\t        }, {
\t\t            name: 'Realisasi Modal',
\t\t            data: [{y: persen_realisasi_modal[0], nilai: realisasi_modal_dinas_pendidikan}, {y: persen_realisasi_modal[1], nilai: realisasi_modal_dinas_kesehatan}, {y: persen_realisasi_modal[2], nilai: realisasi_modal_rsud_soekandar}, {y: persen_realisasi_modal[3], nilai: realisasi_modal_rsud_basoeni}, {y: persen_realisasi_modal[4], nilai: realisasi_modal_dinas_pu_pengairan}, {y: persen_realisasi_modal[5], nilai: realisasi_modal_dinas_pu_bina_marga}, {y: persen_realisasi_modal[6], nilai: realisasi_modal_dinas_pu_cipta_karya}, {y: persen_realisasi_modal[7], nilai: realisasi_modal_bappeda}, {y: persen_realisasi_modal[8], nilai: realisasi_modal_dinas_perhubungan}, {y: persen_realisasi_modal[9], nilai: realisasi_modal_badan_lingkungan_hidup}, {y: persen_realisasi_modal[10], nilai: realisasi_modal_dinas_kependudukan}, {y: persen_realisasi_modal[11], nilai: realisasi_modal_badan_pemberdayaan_perempuan}, {y: persen_realisasi_modal[12], nilai: realisasi_modal_dinas_sosial}, {y: persen_realisasi_modal[13], nilai: realisasi_modal_dinas_tenaga_kerja}, {y: persen_realisasi_modal[14], nilai: realisasi_modal_dinas_kooperasi}, {y: persen_realisasi_modal[15], nilai: realisasi_modal_dinas_pemuda}, {y: persen_realisasi_modal[16], nilai: realisasi_modal_badan_kesatuan_bangsa}, {y: persen_realisasi_modal[17], nilai: realisasi_modal_kepala_daerah}, {y: persen_realisasi_modal[18], nilai: realisasi_modal_dprd}, {y: persen_realisasi_modal[19], nilai: realisasi_modal_sekretariat_daerah}, {y: persen_realisasi_modal[20], nilai: realisasi_modal_sekretariat_dprd}, {y: persen_realisasi_modal[21], nilai: realisasi_modal_badan_perijinan_terpadu}, {y: persen_realisasi_modal[22], nilai: realisasi_modal_badan_kepegawaian}, {y: persen_realisasi_modal[23], nilai: realisasi_modal_inspektorat_kabupaten}, {y: persen_realisasi_modal[24], nilai: realisasi_modal_satpol_pp}, {y: persen_realisasi_modal[25], nilai: realisasi_modal_kec_sooko}, {y: persen_realisasi_modal[26], nilai: realisasi_modal_kec_trowulan}, {y: persen_realisasi_modal[27], nilai: realisasi_modal_kec_puri}, {y: persen_realisasi_modal[28], nilai: realisasi_modal_kec_bangsal}, {y: persen_realisasi_modal[29], nilai: realisasi_modal_kec_gedeg}, {y: persen_realisasi_modal[30], nilai: realisasi_modal_kec_kemlagi}, {y: persen_realisasi_modal[31], nilai: realisasi_modal_kec_jetis}, {y: persen_realisasi_modal[32], nilai: realisasi_modal_kec_dawar}, {y: persen_realisasi_modal[33], nilai: realisasi_modal_kec_mojosari}, {y: persen_realisasi_modal[34], nilai: realisasi_modal_kec_pungging}, {y: persen_realisasi_modal[35], nilai: realisasi_modal_kec_ngoro}, {y: persen_realisasi_modal[36], nilai: realisasi_modal_kec_kutorejo}, {y: persen_realisasi_modal[37], nilai: realisasi_modal_kec_dlanggu}, {y: persen_realisasi_modal[38], nilai: realisasi_modal_kec_gondang}, {y: persen_realisasi_modal[39], nilai: realisasi_modal_kec_jatirejo}, {y: persen_realisasi_modal[40], nilai: realisasi_modal_kec_trawas}, {y: persen_realisasi_modal[41], nilai: realisasi_modal_kec_pacet}, {y: persen_realisasi_modal[42], nilai: realisasi_modal_kec_mojoanyar}, {y: persen_realisasi_modal[43], nilai: realisasi_modal_bpka}, {y: persen_realisasi_modal[44], nilai: realisasi_modal_bpbd}, {y: persen_realisasi_modal[45], nilai: realisasi_modal_dinas_pendapatan}, {y: persen_realisasi_modal[46], nilai: realisasi_modal_kantor_ketahanan_pangan}, {y: persen_realisasi_modal[47], nilai: realisasi_modal_badan_pemberdayaan_masyarakat}, {y: persen_realisasi_modal[48], nilai: realisasi_modal_kantor_perpustakaan}, {y: persen_realisasi_modal[49], nilai: realisasi_modal_dinas_pertanian}, {y: persen_realisasi_modal[50], nilai: realisasi_modal_dinas_peternakan}, {y: persen_realisasi_modal[51], nilai: realisasi_modal_dinas_kehutanan}, {y: persen_realisasi_modal[52], nilai: realisasi_modal_dinas_perindustrian}],
\t\t            legendIndex:1
\t\t        }, {
\t\t            name: 'Anggaran Modal',
\t\t            data: [{y: 100, nilai: anggaran_modal_dinas_pendidikan}, {y: 100, nilai: anggaran_modal_dinas_kesehatan}, {y: 100, nilai: anggaran_modal_rsud_soekandar}, {y: 100, nilai: anggaran_modal_rsud_basoeni}, {y: 100, nilai: anggaran_modal_dinas_pu_pengairan}, {y: 100, nilai: anggaran_modal_dinas_pu_bina_marga}, {y: 100, nilai: anggaran_modal_dinas_pu_cipta_karya}, {y: 100, nilai: anggaran_modal_bappeda}, {y: 100, nilai: anggaran_modal_dinas_perhubungan}, {y: 100, nilai: anggaran_modal_badan_lingkungan_hidup}, {y: 100, nilai: anggaran_modal_dinas_kependudukan}, {y: 100, nilai: anggaran_modal_badan_pemberdayaan_perempuan}, {y: 100, nilai: anggaran_modal_dinas_sosial}, {y: 100, nilai: anggaran_modal_dinas_tenaga_kerja}, {y: 100, nilai: anggaran_modal_dinas_kooperasi}, {y: 100, nilai: anggaran_modal_dinas_pemuda}, {y: 100, nilai: anggaran_modal_badan_kesatuan_bangsa}, {y: 100, nilai: anggaran_modal_kepala_daerah}, {y: 100, nilai: anggaran_modal_dprd}, {y: 100, nilai: anggaran_modal_sekretariat_daerah}, {y: 100, nilai: anggaran_modal_sekretariat_dprd}, {y: 100, nilai: anggaran_modal_badan_perijinan_terpadu}, {y: 100, nilai: anggaran_modal_badan_kepegawaian}, {y: 100, nilai: anggaran_modal_inspektorat_kabupaten}, {y: 100, nilai: anggaran_modal_satpol_pp}, {y: 100, nilai: anggaran_modal_kec_sooko}, {y: 100, nilai: anggaran_modal_kec_trowulan}, {y: 100, nilai: anggaran_modal_kec_puri}, {y: 100, nilai: anggaran_modal_kec_bangsal}, {y: 100, nilai: anggaran_modal_kec_gedeg}, {y: 100, nilai: anggaran_modal_kec_kemlagi}, {y: 100, nilai: anggaran_modal_kec_jetis}, {y: 100, nilai: anggaran_modal_kec_dawar}, {y: 100, nilai: anggaran_modal_kec_mojosari}, {y: 100, nilai: anggaran_modal_kec_pungging}, {y: 100, nilai: anggaran_modal_kec_ngoro}, {y: 100, nilai: anggaran_modal_kec_kutorejo}, {y: 100, nilai: anggaran_modal_kec_dlanggu}, {y: 100, nilai: anggaran_modal_kec_gondang}, {y: 100, nilai: anggaran_modal_kec_jatirejo}, {y: 100, nilai: anggaran_modal_kec_trawas}, {y: 100, nilai: anggaran_modal_kec_pacet}, {y: 100, nilai: anggaran_modal_kec_mojoanyar}, {y: 100, nilai: anggaran_modal_bpka}, {y: 100, nilai: anggaran_modal_bpbd}, {y: 100, nilai: anggaran_modal_dinas_pendapatan}, {y: 100, nilai: anggaran_modal_kantor_ketahanan_pangan}, {y: 100, nilai: anggaran_modal_badan_pemberdayaan_masyarakat}, {y: 100, nilai: anggaran_modal_kantor_perpustakaan}, {y: 100, nilai: anggaran_modal_dinas_pertanian}, {y: 100, nilai: anggaran_modal_dinas_peternakan}, {y: 100, nilai: anggaran_modal_dinas_kehutanan}, {y: 100, nilai: anggaran_modal_dinas_perindustrian}],
\t\t            legendIndex:0
\t\t        }]
\t\t    });
\t\t\t
\t\t\t\$(\"body\").unmask();
\t\t}, \"json\");

\t\t\$.post(\"";
        // line 1062
        echo twig_escape_filter($this->env, site_url("aset/get_saldo_current"), "html", null, true);
        echo "\", {}, function(json){
    \t\tvar tanah = Math.round(json.tanah/1000000 * 1e2 )/1e2;
    \t\tvar mesin = Math.round(json.mesin/1000000 * 1e2)/1e2;
    \t\tvar gedung = Math.round(json.gedung/1000000 * 1e2)/1e2;
    \t\tvar jalan = Math.round(json.jalan/1000000 * 1e2)/1e2;
    \t\tvar buku = Math.round(json.buku/1000000 * 1e2)/1e2;
    \t\tvar kdp = Math.round(json.kdp/1000000 * 1e2)/1e2;
    \t\tvar software = Math.round(json.software/1000000 * 1e2)/1e2;

    \t\t\$('#saldo-current-chart').highcharts({
\t\t        chart: {
\t\t            type: 'column'
\t\t        },
\t\t        title: {
\t\t            text: 'Saldo Current'
\t\t        },
\t\t        xAxis: {
\t\t            categories: ['Dinas Pendidikan'],
\t\t            title: {
\t\t                text: null
\t\t            }
\t\t        },
\t\t        yAxis: {
\t\t            min: 0,
\t\t            title: {
\t\t                text: 'Jumlah',
\t\t                align: 'high'
\t\t            },
\t\t            labels: {
\t\t                overflow: 'justify'
\t\t            }
\t\t        },
\t\t        tooltip: {
\t\t            valueSuffix: ' juta'
\t\t        },
\t\t        plotOptions: {
\t\t            bar: {
\t\t                dataLabels: {
\t\t                    enabled: true
\t\t                }
\t\t            }
\t\t        },
\t\t        legend: {
\t\t            layout: 'vertical',
\t\t            align: 'right',
\t\t            verticalAlign: 'top',
\t\t            x: -40,
\t\t            y: 80,
\t\t            floating: true,
\t\t            borderWidth: 1,
\t\t            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
\t\t            shadow: true
\t\t        },
\t\t        credits: {
\t\t            enabled: false
\t\t        },
\t\t        series: [{
\t\t            name: 'Tanah',
\t\t            data: [tanah]
\t\t        }, {
\t\t            name: 'Peralatan dan Mesin',
\t\t            data: [mesin]
\t\t        }, {
\t\t            name: 'Gedung dan Bangunan',
\t\t            data: [gedung]
\t\t        }, {
\t\t            name: 'Jalan, Jembatan, Instalasi, dan Jaringan',
\t\t            data: [jalan]
\t\t        }, {
\t\t            name: 'Buku, Barang Bercorak Kebudayaan, Hewan, dan Tanaman',
\t\t            data: [buku]
\t\t        }, {
\t\t            name: 'Konstruksi Dalam Pengerjaan',
\t\t            data: [kdp]
\t\t        }, {
\t\t            name: 'Aset Tidak Berwujud',
\t\t            data: [software]
\t\t        }]
\t\t    });
\t\t}, \"json\");

\t});
</script>
";
    }

    // line 1148
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 1149
        echo "\t";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 1150
        echo twig_escape_filter($this->env, base_url("assets/css/devexpress-like/jquery-ui.css"), "html", null, true);
        echo "\" />
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 1151
        echo twig_escape_filter($this->env, base_url("assets/css/select2.min.css"), "html", null, true);
        echo "\" />
\t<link href=\"";
        // line 1152
        echo twig_escape_filter($this->env, base_url("assets/css/jquery.loadmask.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">\t\t

\t<style type=\"text/css\">
\t\t.control-label{
\t\t\tpadding-top: 8px !important;
\t\t}

\t\tselect.form-control {
\t\t\theight: 48px !important;
\t\t}

\t\tinput.form-control {
\t\t\theight: 34px !important;
\t\t}

\t\ttextarea.form-control {
\t\t\theight:auto !important;
\t\t}

\t\t.row {
\t\t\tpadding:10px 0px;
\t\t}

\t\t.spacer {
\t\t    margin-top: 20px;
\t\t}

\t\t.ui-autocomplete{
\t\t\tbackground: none repeat scroll 0 0 white;
\t\t}

\t\t.input-group-addon{
\t\t\tborder-radius: 0 !important;
\t\t}

\t\t.input-group .form-control{
\t\t\tz-index: 0; !important;
\t\t}

\t\t.select2-selection__rendered {
\t\t\theight: 28px;
\t\t}
\t</style>
";
    }

    public function getTemplateName()
    {
        return "dashboard/realisasi.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1243 => 1152,  1239 => 1151,  1235 => 1150,  1230 => 1149,  1227 => 1148,  1139 => 1062,  634 => 560,  562 => 491,  490 => 422,  418 => 353,  346 => 284,  274 => 215,  221 => 165,  168 => 115,  115 => 65,  91 => 44,  87 => 43,  83 => 42,  79 => 41,  75 => 40,  71 => 39,  68 => 38,  33 => 4,  30 => 3,);
    }
}
