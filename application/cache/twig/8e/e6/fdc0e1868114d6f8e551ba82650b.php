<?php

/* simgo/persediaan/modal/modal_tambah_barang_keluar.html */
class __TwigTemplate_8ee6fdc0e1868114d6f8e551ba82650b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"modal fade\" id=\"tambah-barang-keluar\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
\t<div class=\"modal-dialog modal-md\">
\t\t<div class=\"modal-content\">
\t\t\t<div class=\"modal-header\">
\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
\t\t\t\t<h4 class=\"modal-title\" id=\"myModalLabel\">Tambahkan Data Barang Keluar Baru</h4>
\t\t\t</div>
\t\t\t<div class=\"modal-body\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t<form role=\"form\" id=\"tambah-barang-keluar-form\">
\t\t\t\t\t\t\t<!--<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label class=\"control-label\">Gudang<strong>*</strong></label>
\t\t\t\t\t\t\t\t<select type=\"text\" class=\"form-control searchable\" id=\"gudang-select\" placeholder=\"Pilih Gudang\" style=\"width:100% !important;\" data-rule-required=\"true\">
\t\t\t\t\t\t\t\t\t<option></option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>-->
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label class=\"control-label\">Nama Barang<strong>*</strong></label>
\t\t\t\t\t\t\t\t<select type=\"text\" class=\"form-control searchable\" id=\"barang-select\" placeholder=\"Pilih Barang\" style=\"width:100% !important;\" data-rule-required=\"true\">
\t\t\t\t\t\t\t\t\t<option></option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t<input class=\"form-control\" type=\"text\" id=\"barang-input\" style=\"display:none\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label class=\"control-label\">Jumlah Barang<strong>*</strong></label>
\t\t\t\t\t\t\t\t<input type=\"number\" class=\"form-control\" id=\"jumlah-barang\" value=\"\" placeholder=\"Masukkan jumlah barang\" data-rule-required=\"true\">
\t\t\t\t\t\t\t</div> 
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label class=\"control-label\">Keterangan</label>
\t\t\t\t\t\t\t\t\t<textarea class=\"form-control\" style=\"width:calc(100% - 27px); height:auto !important; resize:none;\" rows=\"5\" id=\"keterangan-detail\" placeholder=\"Keterangan barang\"></textarea>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</form>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"modal-footer\">
\t\t\t\t<button type=\"button\" name=\"batal\" class=\"btn btn-default\" data-dismiss=\"modal\">Batal</button>
\t\t\t\t<button type=\"button\" name=\"simpan\" class=\"btn btn-primary\">Tambahkan</button>
\t\t\t</div>
\t\t</div>
\t</div>
</div>";
    }

    public function getTemplateName()
    {
        return "simgo/persediaan/modal/modal_tambah_barang_keluar.html";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,  663 => 538,  655 => 533,  632 => 513,  531 => 417,  498 => 389,  434 => 328,  424 => 321,  405 => 307,  294 => 199,  290 => 198,  283 => 197,  243 => 162,  239 => 161,  232 => 160,  148 => 81,  144 => 80,  135 => 74,  131 => 73,  120 => 65,  116 => 64,  101 => 52,  97 => 51,  76 => 33,  65 => 25,  61 => 24,  42 => 8,  35 => 3,  30 => 1,);
    }
}
