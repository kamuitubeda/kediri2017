<?php

/* simgo/persediaan/modal/modal_tambah_barang_masuk.html */
class __TwigTemplate_b8c567f20559ea26c098e58c8e9c8e7e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"modal fade\" id=\"tambah-barang-masuk\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
\t<div class=\"modal-dialog modal-md\">
\t\t<div class=\"modal-content\">
\t\t\t<div class=\"modal-header\">
\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
\t\t\t\t<h4 class=\"modal-title\" id=\"myModalLabel\">Tambahkan Barang Baru</h4>
\t\t\t</div>
\t\t\t<div class=\"modal-body\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t<form role=\"form\" id=\"tambah-barang-masuk-form\">
\t\t\t\t\t\t\t<!--<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label class=\"control-label\">Gudang<strong>*</strong></label>
\t\t\t\t\t\t\t\t<select type=\"text\" class=\"form-control searchable\" id=\"gudang-select\" placeholder=\"Pilih Gudang\" style=\"width:100% !important;\" data-rule-required=\"true\">
\t\t\t\t\t\t\t\t\t<option></option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>-->
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label class=\"control-label\">Nama Barang<strong>*</strong></label>
\t\t\t\t\t\t\t\t<select type=\"text\" class=\"form-control searchable\" id=\"barang-select\" placeholder=\"Pilih Barang\" style=\"width:100% !important;\" data-rule-required=\"true\">
\t\t\t\t\t\t\t\t\t<option></option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t\t<input class=\"form-control\" type=\"text\" id=\"barang-input\" style=\"display:none\">
\t\t\t\t\t\t\t</div> 
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label class=\"control-label\">Jumlah Barang<strong>*</strong></label>
\t\t\t\t\t\t\t\t<input type=\"number\" class=\"form-control\" id=\"jumlah-barang\" name=\"jumlah-barang\" value=\"\" placeholder=\"Masukkan jumlah barang\" data-rule-required=\"true\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label class=\"control-label\">Harga Satuan<strong>*</strong></label>
\t\t\t\t\t\t\t\t<input type=\"number\" class=\"form-control\" id=\"harga-satuan\" value=\"\" placeholder=\"Masukkan harga satuan\" data-rule-required=\"true\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<!--<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label class=\"control-label\">Harga Total<strong>*</strong></label>
\t\t\t\t\t\t\t\t<input type=\"number\" class=\"form-control\" id=\"harga-total\" name=\"harga-total\" value=\"\" placeholder=\"Masukkan harga total\" data-rule-required=\"true\">
\t\t\t\t\t\t\t</div>-->
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label class=\"control-label\">Tahun Buat<strong>*</strong></label>
\t\t\t\t\t\t\t\t<input type=\"number\" class=\"form-control\" id=\"tahun-buat\" name=\"tahun-buat\" value=\"\" placeholder=\"Masukkan tahun pembuatan\" data-rule-required=\"true\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label class=\"control-label\">Kadaluarsa<strong>*</strong></label>
\t\t\t\t\t\t\t\t<input type=\"number\" class=\"form-control\" id=\"kadaluarsa\" name=\"kadaluarsa\" value=\"\" placeholder=\"Masukkan tahun kadaluarsa\" data-rule-required=\"true\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label class=\"control-label\">Keterangan</label>
\t\t\t\t\t\t\t\t\t<textarea class=\"form-control\" style=\"width:calc(100% - 27px); height:auto !important; resize:none;\" rows=\"5\" id=\"keterangan-detail\" name=\"keterangan\" placeholder=\"Keterangan barang\"></textarea>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</form>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"modal-footer\">
\t\t\t\t<button type=\"button\" name=\"batal\" class=\"btn btn-default\" data-dismiss=\"modal\">Batal</button>
\t\t\t\t<button type=\"button\" name=\"simpan\" class=\"btn btn-primary\">Tambah</button>
\t\t\t</div>
\t\t</div>
\t</div>
</div>";
    }

    public function getTemplateName()
    {
        return "simgo/persediaan/modal/modal_tambah_barang_masuk.html";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,  586 => 475,  566 => 458,  472 => 369,  285 => 185,  266 => 171,  240 => 148,  229 => 140,  225 => 139,  218 => 138,  199 => 124,  195 => 123,  188 => 122,  127 => 66,  123 => 65,  108 => 53,  104 => 52,  83 => 34,  79 => 33,  66 => 23,  57 => 17,  53 => 16,  41 => 7,  35 => 3,  30 => 1,);
    }
}
