<?php

/* simgo/manage_user.html */
class __TwigTemplate_0b87341aa376685a1d67d07ebcf9831d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("_templates/adminpage.html");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'scripts' => array($this, 'block_scripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_templates/adminpage.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "
";
        // line 5
        $this->env->loadTemplate("simgo/manage_user_modal.html")->display($context);
        // line 6
        echo "
<div class=\"container\">
\t<div class=\"row\">

\t\t<table class=\"table table-striped table-bordered\" id=\"nameContainer\">
\t\t\t<thead>
\t\t\t\t<tr>
\t\t\t\t\t<th class=\"text-center\"></th>
\t\t\t\t\t<th class=\"text-center\">User ID</th>
\t\t\t\t\t<th class=\"text-center\">Nama Pengguna</th>
\t\t\t\t\t<th class=\"text-center\">Organisasi</th>
\t\t\t\t</tr>
\t\t\t</thead>
\t\t\t<tbody>
\t\t\t\t";
        // line 20
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["userlist"]) ? $context["userlist"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
            // line 21
            echo "\t\t\t\t<tr>
\t\t\t\t\t<td>
\t\t\t\t\t\t<div class=\"text-center\">
\t\t\t\t\t\t\t<a href=\"#\" data-toggle=\"modal\" data-target=\"#edit-user\" class=\"user\" value=\"";
            // line 24
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "uid"), "html", null, true);
            echo "\" name=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "uname"), "html", null, true);
            echo "\"><span class=\"glyphicon glyphicon-edit\"></span></a>
\t\t\t\t\t\t</div>
\t\t\t\t\t</td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<div class=\"\">
\t\t\t\t\t\t\t";
            // line 29
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "uid"), "html", null, true);
            echo "
\t\t\t\t\t\t</div>
\t\t\t\t\t</td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<div class=\"\">
\t\t\t\t\t\t\t";
            // line 34
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "uname"), "html", null, true);
            echo "
\t\t\t\t\t\t</div>
\t\t\t\t\t</td>
\t\t\t\t\t<td>
\t\t\t\t\t\t<div class=\"\">
\t\t\t\t\t\t\t";
            // line 39
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "oname"), "html", null, true);
            echo "
\t\t\t\t\t\t</div>
\t\t\t\t\t</td>
\t\t\t\t</tr>
\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 44
        echo "\t\t\t</tbody>
\t\t</table>
\t</div>
\t<div id=\"pagination\">
\t\t";
        // line 48
        echo (isset($context["pagination"]) ? $context["pagination"] : null);
        echo "
\t</div>
</div>

";
    }

    // line 54
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 55
        echo "\t";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "

\t<style type=\"text/css\">
\t\t.checkBackground{
\t\t    background-color: gray;
\t\t    color: white;
\t\t}
\t</style>

";
    }

    // line 66
    public function block_scripts($context, array $blocks = array())
    {
        // line 67
        echo "\t";
        $this->displayParentBlock("scripts", $context, $blocks);
        echo "

\t<script type=\"text/javascript\">

\t\tjQuery(function(\$) {
\t\t\t\$(\"input[type='checkbox']\").change(function(){
\t\t\t    if(\$(this).is(\":checked\")){
\t\t\t        \$(this).parent().parent().addClass(\"checkBackground\"); 
\t\t\t    }else{
\t\t\t        \$(this).parent().parent().removeClass(\"checkBackground\");  
\t\t\t    }
\t\t\t});

\t\t\t\$(\".user\").on('click', function(){

\t\t\t\t\$('.unit-checkbox').prop('checked', false);
\t\t\t\t\$('.unit-checkbox').parent().parent().removeClass(\"checkBackground\");

\t\t\t\tuid = \$(this).attr(\"value\");
\t\t\t\tname = \$(this).attr(\"name\");
\t\t\t\t\$(\"#uid\").val(uid);
\t\t\t\t\$(\"#nama\").html(name);

\t\t\t\t\$.post(\"";
        // line 90
        echo twig_escape_filter($this->env, site_url("media/list_skpd_by_uid"), "html", null, true);
        echo "\", {uid:uid}, function(json){
\t\t\t\t\t\$.each(json.data, function(key, value){
\t\t\t\t\t\tvar skpdid = \"#\" + value.nomor_unit.split('.').join('');
\t\t\t\t\t\t\$(skpdid).prop('checked', true);
\t\t\t\t\t\t\$(skpdid).parent().parent().addClass(\"checkBackground\");
\t\t\t\t\t});
\t\t\t\t}, \"json\" );
\t\t\t});

\t\t\t\$(\"button[name=simpan]\").on(\"click\", function(){
\t\t\t\tvar terpilih = [];
\t\t\t\tvar uid = \$(\"#uid\").val();
\t\t\t\t\$.each(\$('.unit-checkbox'), function(key,value){
\t\t\t\t\tif(\$(this).prop('checked') == true){
\t\t\t\t\t\tterpilih.push(\$(this).attr('name'));
\t\t\t\t\t}
\t\t\t\t});
\t\t\t\t
\t\t\t\t\$.post(\"";
        // line 108
        echo twig_escape_filter($this->env, site_url("media/update_user_skpd"), "html", null, true);
        echo "\", {uid:uid, skpd:terpilih}, function(json){
\t\t\t\t}, \"json\");
\t\t\t\t
\t\t\t\t\$(\"#edit-user\").modal(\"hide\");
\t\t\t});

\t\t\t\$('#edit-user').on('hide.bs.modal', function () {
\t\t\t\t\$(\"#edit-user\").scrollTop(0);
\t\t\t});
\t\t});

\t</script>

";
    }

    public function getTemplateName()
    {
        return "simgo/manage_user.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  184 => 108,  163 => 90,  136 => 67,  133 => 66,  118 => 55,  115 => 54,  106 => 48,  100 => 44,  89 => 39,  81 => 34,  73 => 29,  63 => 24,  58 => 21,  54 => 20,  38 => 6,  36 => 5,  33 => 4,  30 => 3,);
    }
}
