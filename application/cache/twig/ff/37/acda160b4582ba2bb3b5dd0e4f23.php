<?php

/* simgo/tambah_kegiatan.html */
class __TwigTemplate_ff37acda160b4582ba2bb3b5dd0e4f23 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"modal fade\" id=\"tambah-kegiatan\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
\t<div class=\"modal-dialog modal-md\">
\t\t<div class=\"modal-content\">
\t\t\t<div class=\"modal-header\">
\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
\t\t\t\t<h4 class=\"modal-title\" id=\"myModalLabel\">Tambahkan Kegiatan Baru</h4>
\t\t\t</div>
\t\t\t<div class=\"modal-body\">
\t\t\t\t<form role=\"form\">
\t\t\t\t\t<label class=\"control-label\">Kode Rekening Kegiatan</label>
\t\t\t\t\t<input type=\"text\" class=\"form-control input-lg\" name=\"no-kegiatan\" value=\"\">
\t\t\t\t</form>
\t\t\t</div>
\t\t\t<div class=\"modal-footer\">
\t\t\t\t<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Batal</button>
\t\t\t\t<button type=\"button\" name=\"simpan\" class=\"btn btn-primary\">Tambahkan</button>
\t\t\t</div>
\t\t</div>
\t</div>
</div>";
    }

    public function getTemplateName()
    {
        return "simgo/tambah_kegiatan.html";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,  231 => 156,  206 => 134,  199 => 130,  195 => 129,  187 => 124,  156 => 96,  144 => 88,  141 => 87,  114 => 63,  111 => 62,  103 => 57,  68 => 25,  59 => 18,  53 => 16,  47 => 12,  45 => 11,  38 => 6,  36 => 5,  33 => 4,  30 => 3,);
    }
}
