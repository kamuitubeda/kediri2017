<?php

/* aset/kantor2.html */
class __TwigTemplate_909cc0023b103e181b5472eb9850c68e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("_templates/adminpage.html");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'scripts' => array($this, 'block_scripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_templates/adminpage.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "<!doctype html>
<html lang=\"en\">
\t<head>
\t\t<title>Daftar Kantor SKPD</title>
\t</head>
\t<body>
\t\t<div class=\"modal fade\" id=\"tambah-kantor\">
\t\t\t<div class=\"modal-dialog modal-sm\">
\t\t\t\t<div class=\"modal-content\">
\t\t\t\t\t<div class=\"modal-header\">
\t\t\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
\t\t\t\t\t\t<h4 class=\"modal-title\">Tambahkan SKPD</h4>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"modal-body\">
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-xs-11\">
\t\t\t\t\t\t\t\t<label class=\"control-label\">Nomor Unit</label>
\t\t\t\t\t\t\t\t<input name=\"skpd\" id=\"skpd\" value=\"\" class=\"form-control\"></input>\t\t\t\t\t\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-xs-11\">
\t\t\t\t\t\t\t\t<label class=\"control-label\">Nama SKPD</label>
\t\t\t\t\t\t\t\t<p type=\"text\" name=\"nama_skpd\" id=\"nama_skpd\"></p>\t\t\t\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-xs-11\">
\t\t\t\t\t\t\t\t<label class=\"control-label\">Latitude</label>
\t\t\t\t\t\t\t\t<input name=\"latitude\" id=\"latitude\" value=\"\" class=\"form-control\"></input>\t\t\t\t\t\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t\t<div class=\"row\">
\t\t\t\t\t\t\t<div class=\"col-xs-11\">
\t\t\t\t\t\t\t\t<label class=\"control-label\">Longitude</label>
\t\t\t\t\t\t\t\t<input name=\"longitude\" id=\"longitude\" value=\"\" class=\"form-control\"></input>\t\t\t\t\t\t
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"modal-footer\">
\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Tutup</button>
\t\t\t\t\t\t<button type=\"button\" name=\"simpan\" class=\"btn btn-primary\">Simpan</button>
\t\t\t\t\t</div>
\t\t\t\t</div><!-- /.modal-content -->
\t\t\t</div><!-- /.modal-dialog -->
\t\t</div><!-- /.modal -->

\t\t<div class=\"row\">
\t\t\t<div class=\"col-md-11\">
\t\t\t\t<div class=\"table-responsive\" style=\"overflow:auto;\">
\t\t\t\t\t<h1 style=\"font-weight:bold;\">Daftar SKPD:</h1>
\t\t\t\t\t<table id=\"skpd-data\" class=\"table table-striped table-hover\" style=\"width:700px;\">
\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<th style=\"width:60%\">Nama SKPD</th>
\t\t\t\t\t\t\t\t<th style=\"width:20%\">Latitude</th>
\t\t\t\t\t\t\t\t<th style=\"width:20%\">Longitude</th>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t</thead>
\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t</tbody>
\t\t\t\t\t</table>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-xs-12\">
\t\t\t\t\t<button type=\"button\" class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#tambah-kantor\">Tambahkan SKPD</button>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</body>
</html>
";
    }

    // line 76
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 77
        echo "\t";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "

\t<link rel=\"stylesheet\" href=\"//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css\">

\t<style>
\t\tlabel, input { 
\t\t\tpadding: 10px 0 0 0;
\t\t}

\t\t.modal-dialog {
\t\t\tmin-width: 400px;
\t\t}
\t</style>

";
    }

    // line 93
    public function block_scripts($context, array $blocks = array())
    {
        // line 94
        echo "\t";
        $this->displayParentBlock("scripts", $context, $blocks);
        echo "

\t<script type=\"text/javascript\">
\t\tjQuery(function(\$) {

\t\t\t\$(\"input#skpd\").autocomplete({
\t\t\t\tappendTo: \"#tambah-kantor\",
\t\t\t\tminLength: 1,
\t\t\t\tsource: \"";
        // line 102
        echo twig_escape_filter($this->env, site_url("aset/cari_skpd"), "html", null, true);
        echo "\",
\t\t\t\tselect: function(e, u) {
\t\t\t\t\t\$(this).val(u.item.id);
\t\t\t\t\t\$(\"p[name=nama_skpd]\").html(u.item.NAMA_UNIT);
\t\t\t\t\treturn false;
\t\t\t\t}
\t\t\t});

\t\t\tvar url = \"";
        // line 110
        echo twig_escape_filter($this->env, site_url("aset/tampilkan_skpd"), "html", null, true);
        echo "\";

\t\t\t\$.get(url, {}, function(json){ 
\t\t\t\tvar data = json.result;
\t\t\t\t
\t\t\t\tfor(var i=0;i<data.length;i++) {
\t\t\t\t\t\$( \"#skpd-data tbody\" ).append( \"<tr>\" +
\t\t\t\t\t\"<td>\" + data[i].nama + \"</td>\" +
\t\t\t\t\t\"<td>\" + data[i].latitude + \"</td>\" +
\t\t\t\t\t\"<td>\" + data[i].longitude + \"</td>\" +
\t\t\t\t\t\"</tr>\" );
\t\t\t\t}
\t\t\t}, \"json\");

\t\t\tvar daftarSkpd = new Array();
\t\t\tvar dialog, form,
\t\t\tskpd = \$(\"#skpd\"),
\t\t\tnama_skpd = \$( \"#nama_skpd\" ),
\t\t\tlatitude = \$( \"#latitude\" ),
\t\t\tlongitude = \$( \"#longitude\" ),
\t\t\tallFields = \$( [] ).add( skpd ).add( latitude ).add( longitude ), tips = \$( \".validateTips\" );

\t\t\tfunction isNotNull(i, data){
\t\t\t\tif(data){
\t\t\t\t\treturn true;
\t\t\t\t}
\t\t\t\telse{
\t\t\t\t\treturn false;
\t\t\t\t}
\t\t\t}

\t\t\tfunction isNumber(i, data){
\t\t\t\tif(isNaN(data)){
\t\t\t\t\treturn false;
\t\t\t\t}else{
\t\t\t\t\treturn true;
\t\t\t\t}
\t\t\t}

\t\t\tfunction addSkpd() {
\t\t\t\tvar valid = true;

\t\t\t\tvalid = valid && isNotNull(skpd, skpd.val());
\t\t\t\tvalid = valid && isNotNull(nama_skpd, nama_skpd.text());
\t\t\t\tvalid = valid && isNotNull(latitude, latitude.val());
\t\t\t\tvalid = valid && isNotNull(longitude, longitude.val());
\t\t\t\tvalid = valid && isNumber(latitude, latitude.val());
\t\t\t\tvalid = valid && isNumber(longitude, longitude.val());

\t\t\t\tif (valid){
\t\t\t\t\tvar aktif = 1;
\t\t\t\t\tvar url = \"";
        // line 161
        echo twig_escape_filter($this->env, site_url("aset/simpan_skpd"), "html", null, true);
        echo "\";

\t\t\t\t\t\$.post(url, {kode:skpd.val(), nama:nama_skpd.text(), latitude:latitude.val(), longitude:longitude.val(), aktif:aktif}, function(r) {
\t\t\t\t\t}, \"json\");

\t\t\t\t\t\$( \"#skpd-data tbody\" ).append( \"<tr>\" +
\t\t\t\t\t\"<td>\" + nama_skpd.text() + \"</td>\" +
\t\t\t\t\t\"<td>\" + latitude.val() + \"</td>\" +
\t\t\t\t\t\"<td>\" + longitude.val() + \"</td>\" +
\t\t\t\t\t\"</tr>\" );
\t\t\t\t\t\$('#tambah-kantor').modal('hide');
\t\t\t\t}
\t\t\t\treturn valid;
\t\t\t}

\t\t\t\$(\"button[name=simpan]\").click(function(){
\t\t\t\taddSkpd();
\t\t\t}); 
\t
\t\t});
\t</script>

";
    }

    public function getTemplateName()
    {
        return "aset/kantor2.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  210 => 161,  156 => 110,  145 => 102,  133 => 94,  130 => 93,  110 => 77,  107 => 76,  33 => 4,  30 => 3,);
    }
}
