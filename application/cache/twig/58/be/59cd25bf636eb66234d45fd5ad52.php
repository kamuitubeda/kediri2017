<?php

/* simgo/persediaan/laporan/laporan_semester.html */
class __TwigTemplate_58be59cd25bf636eb66234d45fd5ad52 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("_templates/adminpage.html");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'scripts' => array($this, 'block_scripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_templates/adminpage.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 1
    public function block_content($context, array $blocks = array())
    {
        // line 2
        $this->env->loadTemplate("simgo/persediaan/modal/modal_persiapan_cetak.html")->display($context);
        // line 3
        echo "<div class=\"container\">
    <div class=\"row\">
        <div class=\"col-md-12\">
            <ol class=\"breadcrumb\">
                <li><a href=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("persediaan"), "html", null, true);
        echo "\">Persediaan</a></li>
                <li class=\"active\">Laporan Semester</li>
            </ol>
        </div>
    </div>
    <div class=\"row\" style=\"display:none\">
        <div class=\"col-md-12 text-center\">
            <h2>Laporan Semester tentang Penerimaan dan Pengeluaran Barang Pakai Habis</h2>
        </div>
    </div>
    <div class=\"row\">
        <div class=\"col-md-12 text-right\">
            <h4>
                    Semester 
                    <select class=\"select-box\" name=\"semester\" id=\"semester\">
                        <option value=\"1\">Pertama</option>
                        <option value=\"2\">Kedua</option>
                    </select>
                     Tahun 
                    <select class=\"select-box\" name=\"tahun\" id=\"tahun\">
                        ";
        // line 27
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["tahun"]) ? $context["tahun"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["val"]) {
            // line 28
            echo "                        <option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["val"]) ? $context["val"] : null), "tahun"), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["val"]) ? $context["val"] : null), "tahun"), "html", null, true);
            echo "</option>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['val'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 30
        echo "                    </select>
            </h4>
        </div>
    </div>
    <div class=\"row\">
        <div class=\"col-md-12\">
            <table>
                <tr>
                    <td>SKPD&nbsp;</td>
                    <td>: ";
        // line 39
        echo twig_escape_filter($this->env, (isset($context["oname"]) ? $context["oname"] : null), "html", null, true);
        echo "</td>
                </tr>
                <tr>
                    <td>Kabupaten&nbsp;</td>
                    <td>: Mojokerto</td>
                </tr>
                <tr>
                    <td>Provinsi&nbsp;</td>
                    <td>: Jawa Timur</td>
                </tr>
            </table>
        </div>
    </div>
    <br/>
    <div class=\"row\">
        <div id=\"table-container\" class=\"col-md-12\" style=\"overflow-x:auto\">
            <table class=\"table table-striped table-bordered\">
                <thead>
                    <tr>
                        <th class=\"text-center\" rowspan='2'>No.</th>
                        <th class=\"text-center\" rowspan='2'>Terima Tanggal</th>
                        <th class=\"text-center\" rowspan='2'>Dari</th>
                        <th class=\"text-center\" colspan='2'>Dokumen / Faktur</th>
                        <th class=\"text-center\" colspan='2'>Dasar Penerimaan</th>
                        <th class=\"text-center\" rowspan='2'>Banyaknya</th>
                        <th class=\"text-center\" rowspan='2'>Nama Barang</th>
                        <th class=\"text-center\" rowspan='2'>Harga Satuan</th>
                        <th class=\"text-center\" colspan='2'>Buku Penerimaan</th>
                        <th class=\"text-center\" rowspan='2'>Ket.</th>
                        <th class=\"text-center\" rowspan='2'>No. Urut</th>
                        <th class=\"text-center\" rowspan='2'>Terima Tanggal</th>
                        <th class=\"text-center\" colspan='2'>Nota Dinas Permintaan</th>
                        <th class=\"text-center\" rowspan='2'>Untuk</th>
                        <th class=\"text-center\" rowspan='2'>Banyaknya</th>
                        <th class=\"text-center\" rowspan='2'>Nama Barang</th>
                        <th class=\"text-center\" rowspan='2'>Harga Satuan</th>
                        <th class=\"text-center\" rowspan='2'>Jumlah Harga</th>
                        <th class=\"text-center\" rowspan='2'>Tgl. Penyerahan</th>
                        <th class=\"text-center\" rowspan='2'>Ket.</th>
                    </tr>
                    <tr>
                        <th class=\"text-center\">Nomor</th>
                        <th class=\"text-center\">Tanggal</th>
                        <th class=\"text-center\">Jenis / Surat</th>
                        <th class=\"text-center\">Nomor</th>
                        <th class=\"text-center\">Nomor</th>
                        <th class=\"text-center\">Tanggal</th>
                        <th class=\"text-center\">Nomor</th>
                        <th class=\"text-center\">Tanggal</th>
                    </tr>
                    <tr>
                        ";
        // line 90
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 24));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 91
            echo "                        <th class=\"small-header text-center\"><small>";
            echo twig_escape_filter($this->env, (isset($context["i"]) ? $context["i"] : null), "html", null, true);
            echo "</small></th>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 93
        echo "                    </tr>
                </thead>
                <tbody id=\"data-body\">
                    <tr></tr>
                </tbody>
            </table>
        </div>
    </div>
    <br/>
    <div class=\"row\">
        <div class=\"col-md-12\">
            <div class=\"btn-group pull-right\">
                <button id=\"kembali\" type=\"button\" class=\"btn btn-default\">Kembali</button>
                <button id=\"cetak\" type=\"button\" class=\"btn btn-primary\"><span class=\"glyphicon glyphicon-print\"></span> Cetak</button>
            </div>
        </div>
    </div>
</div>
";
    }

    // line 111
    public function block_stylesheets($context, array $blocks = array())
    {
        echo " ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
<link rel=\"stylesheet\" type=\"text/css\" media=\"print\" href=\"";
        // line 112
        echo twig_escape_filter($this->env, base_url("assets/css/bootstrap.min.css"), "html", null, true);
        echo "\">
<style type=\"text/css\">
    .small-header {
        font-weight: normal !important;
        padding: 2px !important;
    }
    
    @media print {
        body * {
            visibility: hidden;
        }
        #topbar,
        #sidebar,
        .breadcrumb,
        h1 {
            display: none;
        }
        .container,
        .container * {
            visibility: visible;
        }
        .sidebar-max {
            padding: 0px !important;
        }
        #content {
            position: absolute !important;
            left: 0px !important;
            top: 0px !important;
        }
        #table-container {
            overflow: visible !important;
        }
    }
</style>
";
    }

    // line 146
    public function block_scripts($context, array $blocks = array())
    {
        echo " ";
        $this->displayParentBlock("scripts", $context, $blocks);
        echo "
<script src=\"";
        // line 147
        echo twig_escape_filter($this->env, site_url("assets/js/numeral/numeral.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 148
        echo twig_escape_filter($this->env, site_url("assets/js/numeral/languages.id.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 149
        echo twig_escape_filter($this->env, base_url("assets/js/base64.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\">

    numeral.language('id-ID');
    
    jQuery(function(\$){
        populateTable();
        
        \$(\".select-box\").on(\"change\", function(){
            populateTable();
        });
        
        var today = \"";
        // line 161
        echo twig_escape_filter($this->env, (isset($context["today"]) ? $context["today"] : null), "html", null, true);
        echo "\";
        var bulan = today.split(\"-\")[0];
        var tahun = today.split(\"-\")[1];
        
        if (parseInt(bulan) <= 6)
        {
            \$(\"#semester\").val(\"1\");
        }
        else
        {
            \$(\"#semester\").val(\"2\");
        }
        
        if(\$('#tahun').has('option').length === 0) {
            \$(\"#tahun\").append(\"<option value='\"+tahun+\"' selected='selected'>\"+tahun+\"</option>\");
        }
        
        \$(\"#tahun\").val(\$(\"#tahun option:last\").val());
        
        function populateTable(){
            \$(\"#data-body\").empty();
            \$(\"#data-body\").append(\"<tr></tr><tr></tr>\");
            
            \$.post(\"";
        // line 184
        echo twig_escape_filter($this->env, site_url("persediaan/get_populasi_laporan_semester"), "html", null, true);
        echo "\", {skpd:\"";
        echo twig_escape_filter($this->env, (isset($context["skpd"]) ? $context["skpd"] : null), "html", null, true);
        echo "\", semester:\$(\"#semester\").val(), tahun:\$(\"#tahun\").val()}, function(json){
                var penerimaan = [];
                var jurnal_penerimaan_count = 0;
                var pengeluaran = [];
                var jurnal_pengeluaran_count = 0;
                
                json.forEach(function(ju,juin){
                    var jurnal = ju;
                    var nomor = juin+1;
                    ju.detail.forEach(function(jud, judin){
                        var row;
                        if(jurnal['tipe'] == 0)
                        {
                            row = \"\";
                            if(judin == 0)
                            {
                                // no.
                                row += \"<td>\"+(++jurnal_penerimaan_count)+\"</td>\";
                                // terima tanggal
                                row += \"<td>\"+jurnal[\"jutgl\"]+\"</td>\";
                                // dari
                                row += \"<td>\"+jurnal[\"spnama\"]+\"</td>\";
                                // nomor dokumen / faktur
                                row += \"<td>\"+jurnal[\"judokumenno\"]+\"</td>\";
                                // tanggal dokumen / faktur
                                row += \"<td>\"+jurnal[\"judokumentgl\"]+\"</td>\";
                                // jenis / surat (dasar penerimaan)
                                row += \"<td>\"+\"\"+\"</td>\";
                                // nomor
                                row += \"<td>\"+jurnal[\"judasarno\"]+\"</td>\";
                            }
                            else
                            {
                                row += \"<td></td><td></td><td></td><td></td><td></td><td></td><td></td>\";
                            }
                            // banyaknya
                            row += \"<td>\"+numeral(jud[\"judqty\"]).format(\"0,0.0\")+\" \"+jud[\"judunit\"].toLowerCase()+\"</td>\";
                            // nama barang
                            row += \"<td>\"+jud[\"namabarang\"]+\"</td>\";
                            // row += \"<td>\"+jud[\"namabarang\"]+\"<br/><small>\"+jud[\"spesifikasi\"]+\"</small></td>\";
                            // harga satuan
                            row += \"<td class='text-right'>\"+numeral(jud[\"judhargasat\"]).format(\"\$0.0,[00]\")+\"</td>\";
                            if(judin == 0)
                            {
                                // nomor buku penerimaan
                                row += \"<td>\"+jurnal[\"juno\"]+\"</td>\";
                                // tanggal buku penerimaan
                                row += \"<td>\"+jurnal[\"jutgl\"]+\"</td>\";
                                // keterangan
                                row += \"<td>\"+jurnal[\"keterangan\"]+\"</td>\";
                            }
                            else
                            {
                                row += \"<td></td><td></td><td></td>\";
                            }
                            penerimaan.push(row);
                        }
                        else if(jurnal['tipe'] == 1)
                        {
                            row = \"\";
                            if(judin == 0)
                            {
                                // no urut
                                row += \"<td>\"+(++jurnal_pengeluaran_count)+\"</td>\";
                                // diserahkan tanggal
                                row += \"<td>\"+jurnal[\"jutgl\"]+\"</td>\";
                                // nomor surat bon
                                row += \"<td>\"+jurnal[\"judasarno\"]+\"</td>\";
                                // tanggal surat bon
                                row += \"<td>\"+jurnal[\"judasartgl\"]+\"</td>\";
                                // untuk
                                row += \"<td>\"+jurnal[\"spnama\"]+\"</td>\";
                            }
                            else
                            {
                                row += \"<td></td><td></td><td></td><td></td><td></td>\";
                            }
                            // banyaknya
                            row += \"<td>\"+numeral(-jud[\"judqty\"]).format(\"0,0.0\")+\" \"+jud[\"judunit\"].toLowerCase()+\"</td>\";
                            // nama barang
                            row += \"<td>\"+jud[\"namabarang\"]+\"</td>\";
                            // row += \"<td>\"+jud[\"namabarang\"]+\"<br/><small>\"+jud[\"spesifikasi\"]+\"</small></td>\";
                            // harga satuan
                            row += \"<td class='text-right'>\"+numeral(jud[\"judhargasat\"]).format(\"\$0.0,[00]\")+\"</td>\";
                            // jumlah harga 
                            row += \"<td class='text-right'>\"+numeral(jud[\"judhargasat\"] * -jud[\"judqty\"]).format(\"\$0.0,[00]\")+\"</td>\";
                            if(judin == 0)
                            {
                                // tanggal penyerahan
                                row += \"<td>\"+jurnal[\"jutgl\"]+\"</td>\";
                                // ket
                                row += \"<td>\"+jurnal[\"keterangan\"]+\"</td>\";
                            }
                            else
                            {
                                row += \"<td></td><td></td>\";
                            }
                            pengeluaran.push(row);
                        }
                    }, nomor, jurnal);
                });
                
                // console.log(penerimaan);console.log(pengeluaran);
                
                var loopCount = 0;
                if(penerimaan.length > pengeluaran.length){loopCount = penerimaan.length;}
                else{loopCount = pengeluaran.length;}

                for (i=0; i<loopCount; i++) {
                    var table_row = \"<tr>\";
                    if(penerimaan.length > i) {
                        table_row += penerimaan[i];
                    }
                    else {
                        for (j = 0; j < 13; j++)
                        {
                            table_row+=\"<td></td>\";
                        }
                    }

                    if(pengeluaran.length > i) {
                        table_row += pengeluaran[i];
                    }
                    else {
                        for (j = 0; j < 11; j++)
                        {
                            table_row+=\"<td></td>\";
                        }
                    }
                    table_row += \"</tr>\";
                    \$(\"#data-body\").append(table_row);
                }
                
                if(false){
                    // json.forEach(function(ju,juin){
                    //     var jurnal = ju;
                    //     var nomor = juin+1;
                    //     ju.detail.forEach(function(jud, judin){
                    //         var table_row = \"<tr>\";
                    //         if(jurnal['tipe'] == 0)
                    //         {
                    //             if(judin == 0)
                    //             {
                    //                 // no.
                    //                 table_row += \"<td>\"+nomor+\"</td>\";
                    //                 // terima tanggal
                    //                 table_row += \"<td>\"+jurnal[\"jutgl\"]+\"</td>\";
                    //                 // dari
                    //                 table_row += \"<td>\"+jurnal[\"spnama\"]+\"</td>\";
                    //                 // nomor dokumen / faktur
                    //                 table_row += \"<td>\"+jurnal[\"judokumenno\"]+\"</td>\";
                    //                 // tanggal dokumen / faktur
                    //                 table_row += \"<td>\"+jurnal[\"judokumentgl\"]+\"</td>\";
                    //                 // jenis / surat (dasar penerimaan)
                    //                 table_row += \"<td>\"+\"\"+\"</td>\";
                    //                 // nomor
                    //                 table_row += \"<td>\"+jurnal[\"judasarno\"]+\"</td>\";
                    //             }
                    //             else
                    //             {
                    //                 table_row += \"<td></td><td></td><td></td><td></td><td></td><td></td><td></td>\";
                    //             }
                    //             // banyaknya
                    //             table_row += \"<td>\"+jud[\"judqty\"]+\" \"+jud[\"judunit\"]+\"</td>\";
                    //             // nama barang
                    //             table_row += \"<td>\"+jud[\"namabarang\"]+\"<br/><small>\"+jud[\"spesifikasi\"]+\"</small></td>\";
                    //             // harga satuan
                    //             table_row += \"<td>\"+\"Rp. xx.xxx,-\"+\"</td>\";
                    //             if(judin == 0)
                    //             {
                    //                 // nomor buku penerimaan
                    //                 table_row += \"<td>\"+jurnal[\"juno\"]+\"</td>\";
                    //                 // tanggal buku penerimaan
                    //                 table_row += \"<td>\"+jurnal[\"jutgl\"]+\"</td>\";
                    //                 // keterangan
                    //                 table_row += \"<td>\"+jurnal[\"keterangan\"]+\"</td>\";
                    //             }
                    //             else
                    //             {
                    //                 table_row += \"<td></td><td></td><td></td>\";
                    //             }
                    //             for (var i = 0; i < 11; i++)
                    //             {
                    //                 table_row+=\"<td></td>\";
                    //             }
                    //         }
                    //         else if(jurnal['tipe'] == 1)
                    //         {
                    //             for (var i = 0; i < 13; i++)
                    //             {
                    //                 table_row+=\"<td></td>\";
                    //             }
                    //             if(judin == 0)
                    //             {
                    //                 // no urut
                    //                 table_row += \"<td>\"+nomor+\"</td>\";
                    //                 // diserahkan tanggal
                    //                 table_row += \"<td>\"+jurnal[\"jutgl\"]+\"</td>\";
                    //                 // nomor surat bon
                    //                 table_row += \"<td>\"+jurnal[\"judasarno\"]+\"</td>\";
                    //                 // tanggal surat bon
                    //                 table_row += \"<td>\"+jurnal[\"judasartgl\"]+\"</td>\";
                    //                 // untuk
                    //                 table_row += \"<td>\"+jurnal[\"spnama\"]+\"</td>\";
                    //             }
                    //             else
                    //             {
                    //                 table_row += \"<td></td><td></td><td></td><td></td><td></td>\";
                    //             }
                    //             // banyaknya
                    //             table_row += \"<td>\"+(-jud[\"judqty\"])+\" \"+jud[\"judunit\"]+\"</td>\";
                    //             // nama barang
                    //             table_row += \"<td>\"+jud[\"namabarang\"]+\"<br/><small>\"+jud[\"spesifikasi\"]+\"</small></td>\";
                    //             // harga satuan
                    //             table_row += \"<td>\"+\"Rp. xxx.xxx,xx\"+\"</td>\";
                    //             // jumlah harga 
                    //             table_row += \"<td>\"+\"Rp. xxx.xxx,xx\"+\"</td>\";
                    //             if(judin == 0)
                    //             {
                    //                 // tanggal penyerahan
                    //                 table_row += \"<td>\"+jurnal[\"jutgl\"]+\"</td>\";
                    //                 // ket
                    //                 table_row += \"<td>\"+jurnal[\"keterangan\"]+\"</td>\";
                    //             }
                    //             else
                    //             {
                    //                 table_row += \"<td></td><td></td>\";
                    //             }
                    //         }
                    //         table_row += \"</tr>\";
                    //         \$(\"#data-body\").append(table_row);
                    //     }, nomor, jurnal);
                    // });
                }
            },\"json\");
        }
        
        \$(\"#lakukan-cetak\").on('click', function(){
            var namaunit = Base64.encode(\"";
        // line 422
        echo twig_escape_filter($this->env, (isset($context["oname"]) ? $context["oname"] : null), "html", null, true);
        echo "\");
            var skpd = Base64.encode(\"";
        // line 423
        echo twig_escape_filter($this->env, (isset($context["skpd"]) ? $context["skpd"] : null), "html", null, true);
        echo "\");
            var semester = Base64.encode(\$(\"#semester\").val());
            var tahun = Base64.encode(\$(\"#tahun\").val());
            var pengurusbarang = Base64.encode(\$(\"#nama-petugas\").val());
            var nippengurusbarang = Base64.encode(\$(\"#nip-petugas\").val());
            var atasanlangsung = Base64.encode(\$(\"#nama-atasan\").val());
            var nipatasanlangsung = Base64.encode(\$(\"#nip-atasan\").val());
            
            if( skpd === \"\") {skpd = \"-\"}
            if( semester === \"\") {semester = \"-\"}
            if( namaunit === \"\") {namaunit = \"-\"}
            if( pengurusbarang === \"\") {pengurusbarang = \"-\"}
            if( nippengurusbarang === \"\") {nippengurusbarang = \"-\"}
            if( atasanlangsung === \"\") {atasanlangsung = \"-\"}
            if( nipatasanlangsung === \"\") {nipatasanlangsung = \"-\"}
            
            window.open(\"";
        // line 439
        echo twig_escape_filter($this->env, site_url("persediaan/print_laporan_semester"), "html", null, true);
        echo "/\"+ namaunit + \"/\" + skpd + \"/\" + semester + \"/\" + tahun + \"/\" + pengurusbarang + \"/\" + nippengurusbarang + \"/\" + atasanlangsung + \"/\" + nipatasanlangsung, \"_blank\");
        });

        \$(\"#cetak\").on(\"click\", function(){
            \$(\"#pengambil\").hide();
            \$(\"#persiapan-cetak\").modal(\"show\");
        });
        
        \$(\"#kembali\").on(\"click\", function(){
            window.location.href = \"";
        // line 448
        echo twig_escape_filter($this->env, site_url("persediaan"), "html", null, true);
        echo "\";
        });
    });

</script>
";
    }

    public function getTemplateName()
    {
        return "simgo/persediaan/laporan/laporan_semester.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  559 => 448,  547 => 439,  528 => 423,  524 => 422,  281 => 184,  255 => 161,  240 => 149,  236 => 148,  232 => 147,  225 => 146,  186 => 112,  179 => 111,  157 => 93,  148 => 91,  144 => 90,  90 => 39,  79 => 30,  68 => 28,  64 => 27,  41 => 7,  35 => 3,  33 => 2,  30 => 1,);
    }
}
