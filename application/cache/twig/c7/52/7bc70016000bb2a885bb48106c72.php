<?php

/* simgo/persediaan/modal/modal_persiapan_cetak.html */
class __TwigTemplate_c7527bc70016000bb2a885bb48106c72 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"modal fade\" id=\"persiapan-cetak\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
\t<div class=\"modal-dialog modal-md\">
\t\t<div class=\"modal-content\">
\t\t\t<div class=\"modal-header\">
\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
\t\t\t\t<h4 class=\"modal-title\" id=\"myModalLabel\">Penandatangan Dokumen</h4>
\t\t\t</div>
\t\t\t<div class=\"modal-body\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t<form role=\"form\" id=\"persiapan-cetak-form\">
                            <div id=\"petugas\" class=\"row\">
                                <div class=\"form-group col-md-8\">
                                    <label class=\"control-label\">Petugas Penyimpan<strong>*</strong></label>
                                    <input type=\"text\" class=\"form-control searchable\" id=\"nama-petugas\" name=\"nama-petugas\" placeholder=\"Nama\" data-rule-required=\"true\">
                                </div>
                                <div class=\"form-group col-md-4\">
                                    <label class=\"control-label\">&nbsp;</label>
                                    <input type=\"text\" class=\"form-control searchable\" id=\"nip-petugas\" name=\"nip-petugas\" placeholder=\"NIP\" data-rule-required=\"true\">
                                </div>
                            </div>
                            
                            <div id=\"atasan\" class=\"row\">
                                <div class=\"form-group col-md-8\">
                                    <label class=\"control-label\">Atasan Langsung<strong>*</strong></label>
                                    <input type=\"text\" class=\"form-control searchable\" id=\"nama-atasan\" name=\"nama-atasan\" placeholder=\"Nama\" data-rule-required=\"true\">
                                </div>
                                <div class=\"form-group col-md-4\">
                                    <label class=\"control-label\">&nbsp;</label>
                                    <input type=\"text\" class=\"form-control searchable\" id=\"nip-atasan\" name=\"nip-atasan\" placeholder=\"NIP\" data-rule-required=\"true\">
                                </div>
                            </div>
                            
                            <div id=\"pengambil\" class=\"row\">
                                <div class=\"form-group col-md-8\">
                                    <label class=\"control-label\">Pengambil Barang<strong>*</strong></label>
                                    <input type=\"text\" class=\"form-control searchable\" id=\"nama-pengambil\" name=\"nama-pengambil\" placeholder=\"Nama\" data-rule-required=\"true\">
                                </div>
                                <div class=\"form-group col-md-4\">
                                    <label class=\"control-label\">&nbsp;</label>
                                    <input type=\"text\" class=\"form-control searchable\" id=\"nip-pengambil\" name=\"nip-pengambil\" placeholder=\"NIP\" data-rule-required=\"true\">
                                </div>
                            </div>
\t\t\t\t\t\t</form>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"modal-footer\">
\t\t\t\t<button type=\"button\" id = \"batal\" name=\"batal\" class=\"btn btn-default\" data-dismiss=\"modal\">Batal</button>
\t\t\t\t<button type=\"button\" id = \"lakukan-cetak\" name=\"lakukan-cetak\" class=\"btn btn-primary\">Cetak</button>
\t\t\t</div>
\t\t</div>
\t</div>
</div>";
    }

    public function getTemplateName()
    {
        return "simgo/persediaan/modal/modal_persiapan_cetak.html";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,  356 => 244,  343 => 234,  326 => 220,  321 => 218,  317 => 217,  306 => 209,  295 => 201,  272 => 181,  239 => 153,  212 => 129,  208 => 128,  201 => 127,  165 => 96,  161 => 95,  154 => 94,  132 => 76,  123 => 74,  119 => 73,  68 => 25,  55 => 15,  51 => 14,  41 => 7,  35 => 3,  33 => 2,  30 => 1,);
    }
}
