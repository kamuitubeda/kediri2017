<?php

/* simgo/persediaan/laporan/buku_barang_pakai_habis.html */
class __TwigTemplate_c7a869e011c7d72ceb9c11548921dc62 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("_templates/adminpage.html");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'scripts' => array($this, 'block_scripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_templates/adminpage.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 1
    public function block_content($context, array $blocks = array())
    {
        // line 2
        $this->env->loadTemplate("simgo/persediaan/modal/modal_persiapan_cetak.html")->display($context);
        // line 3
        echo "<div class=\"container\">
\t<div class=\"row\">
\t\t<div class=\"col-md-12\">
\t\t\t<ol class=\"breadcrumb\">
\t\t\t\t<li><a href=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("persediaan"), "html", null, true);
        echo "\">Persediaan</a></li>
\t\t\t\t<li class=\"active\">Buku Barang Pakai Habis</li>
\t\t\t</ol>
\t\t</div>
\t</div>
\t<div class=\"row\">
\t\t<div class=\"col-md-3 pull-right\">
\t\t\t<div data-date-format=\"mm-yyyy\" data-date=\"";
        // line 14
        echo twig_escape_filter($this->env, (isset($context["today"]) ? $context["today"] : null), "html", null, true);
        echo "\" datepicker class=\"input-group input-append date\">
\t\t\t\t<input id=\"tanggal\" name=\"tanggal\" readonly=\"\" class=\"form-control\" value=\"";
        // line 15
        echo twig_escape_filter($this->env, (isset($context["today"]) ? $context["today"] : null), "html", null, true);
        echo "\"></input>
\t\t\t\t<span class=\"input-group-addon add-on\"><i class=\"icon-calendar\"></i></span>
\t\t\t</div>
\t\t</div>
\t</div>
\t<div class=\"row\">
\t\t<div class=\"col-md-12\">
\t\t\t<table>
\t\t\t\t<tr>
\t\t\t\t\t<td>SKPD&nbsp;</td>
\t\t\t\t\t<td>: ";
        // line 25
        echo twig_escape_filter($this->env, (isset($context["oname"]) ? $context["oname"] : null), "html", null, true);
        echo "</td>
\t\t\t\t</tr>
\t\t\t\t<tr>
\t\t\t\t\t<td>KABUPATEN&nbsp;</td>
\t\t\t\t\t<td>: Mojokerto</td>
\t\t\t\t</tr>
\t\t\t\t<tr>
\t\t\t\t\t<td>PROVINSI&nbsp;</td>
\t\t\t\t\t<td>: Jawa Timur</td>
\t\t\t\t</tr>
\t\t\t</table>
\t\t</div>
\t</div>
\t<br/>
\t<div class=\"row\" style=\"display:none\">
\t\t<div class=\"col-md-12 text-center\">
\t\t\t<h2>Buku Barang Pakai Habis</h2>
\t\t</div>
\t</div>
\t<div class=\"row\">
\t\t<div class=\"col-md-12\">
\t\t\t<table class=\"table table-striped table-bordered\">
\t\t\t\t<thead>
\t\t\t\t\t<tr>
\t\t\t\t\t\t<th class=\"text-center\" rowspan=\"3\">No.</th>
\t\t\t\t\t\t<th class=\"text-center\" rowspan=\"3\">Jenis / Nama Barang</th>
\t\t\t\t\t\t<th class=\"text-center\" rowspan=\"3\">Merk / Ukuran</th>
\t\t\t\t\t\t<th class=\"text-center\" rowspan=\"3\">Tahun Pembuatan</th>
\t\t\t\t\t\t<th class=\"text-center\" colspan=\"5\">PENERIMAAN</th>
\t\t\t\t\t\t<th class=\"text-center\" colspan=\"4\">PENGELUARAN</th>
\t\t\t\t\t\t<th class=\"text-center\" rowspan=\"3\">Ket.</th>
\t\t\t\t\t\t<th class=\"text-center\" rowspan=\"3\"></th>
\t\t\t\t\t</tr>
\t\t\t\t\t<tr>
\t\t\t\t\t\t<th class=\"text-center\" rowspan=\"2\">Tanggal Diterima</th>
\t\t\t\t\t\t<th class=\"text-center\" rowspan=\"2\">Jumlah Satuan / Barang</th>
\t\t\t\t\t\t<th class=\"text-center\" rowspan=\"2\">Tgl / No. Kontrak / SP / SPK / Harga Satuan</th>
\t\t\t\t\t\t<th class=\"text-center\" colspan=\"2\">Berita Acara Pemeriksaan</th>
\t\t\t\t\t\t<th class=\"text-center\" rowspan=\"2\">Tanggal Dikeluarkan</th>
\t\t\t\t\t\t<th class=\"text-center\" rowspan=\"2\">Diserahkan Kepada</th>
\t\t\t\t\t\t<th class=\"text-center\" rowspan=\"2\">Jumlah Satuan / Barang</th>
\t\t\t\t\t\t<th class=\"text-center\" rowspan=\"2\">Tgl / No. Surat Penyerahan</th>
\t\t\t\t\t</tr>
\t\t\t\t\t<tr>
\t\t\t\t\t\t<th>Tanggal</th>
\t\t\t\t\t\t<th>Nomor</th>
\t\t\t\t\t</tr>
\t\t\t\t\t<tr>
\t\t\t\t\t\t";
        // line 73
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 14));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 74
            echo "\t\t\t\t\t\t<th class=\"small-header text-center\"><small>";
            echo twig_escape_filter($this->env, (isset($context["i"]) ? $context["i"] : null), "html", null, true);
            echo "</small></th>
\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 76
        echo "\t\t\t\t\t\t<th class=\"small-header text-center\"></th>
\t\t\t\t\t</tr>
\t\t\t\t</thead>
\t\t\t\t<tbody id=\"data-body\">
\t\t\t\t\t<tr></tr>
\t\t\t\t</tbody>
\t\t\t</table>
\t\t</div>
\t</div>
\t<div class=\"row\">
        <div class=\"col-md-12\">
            <div class=\"btn-group pull-right\">
                <button id=\"kembali\" type=\"button\" class=\"btn btn-default\">Kembali</button>
                <button id=\"cetak\" type=\"button\" class=\"btn btn-primary\"><span class=\"glyphicon glyphicon-print\"></span> Cetak</button>
            </div>
        </div>
    </div>
</div>
";
    }

    // line 94
    public function block_stylesheets($context, array $blocks = array())
    {
        echo " ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 95
        echo twig_escape_filter($this->env, base_url("assets/css/datepicker3.css"), "html", null, true);
        echo "\" />
<link rel=\"stylesheet\" type=\"text/css\" media=\"print\" href=\"";
        // line 96
        echo twig_escape_filter($this->env, base_url("assets/css/bootstrap.min.css"), "html", null, true);
        echo "\">
<style type=\"text/css\">
\t.small-header {
\t\tfont-weight: normal !important;
\t\tpadding: 2px !important;
\t}
\t
\t@media print {
\t\tbody * {
\t\t\tvisibility: hidden;
\t\t}
\t\t#topbar,
\t\t#sidebar,
\t\t.breadcrumb,
\t\th1 {
\t\t\tdisplay: none;
\t\t}
\t\t.container,
\t\t.container * {
\t\t\tvisibility: visible;
\t\t}
\t\t.sidebar-max {
\t\t\tpadding: 0px !important;
\t\t}
\t\t#content {
\t\t\tposition: absolute !important;
\t\t\tleft: 0px !important;
\t\t\ttop: 0px !important;
\t\t}
\t}
</style>
";
    }

    // line 127
    public function block_scripts($context, array $blocks = array())
    {
        echo " ";
        $this->displayParentBlock("scripts", $context, $blocks);
        echo "
<script type=\"text/javaScript\" src=\"";
        // line 128
        echo twig_escape_filter($this->env, base_url("assets/js/bootstrap-datepicker.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 129
        echo twig_escape_filter($this->env, base_url("assets/js/base64.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\">
\tjQuery(function(\$){
\t\t
        populateTable();
        var masuk = [];
        var keluar = [];

\t\t\$(\"*[datepicker]\").datepicker({
\t\t\tautoclose:true,
\t\t\torientation:\"bottom\",
\t\t\tformat: \"mm-yyyy\",
\t\t\tstartView: \"months\", 
\t\t\tminViewMode: \"months\"
\t\t});
        
        \$(\"#tanggal\").on(\"change\", function(){
            populateTable();
        });
        
        function populateTable(){
            \$(\"#data-body\").empty();
            \$(\"#data-body\").append(\"<tr></tr><tr></tr>\");
            
            \$.post(\"";
        // line 153
        echo twig_escape_filter($this->env, site_url("persediaan/get_populasi_buku_pakai_habis"), "html", null, true);
        echo "\", {skpd:\"";
        echo twig_escape_filter($this->env, (isset($context["skpd"]) ? $context["skpd"] : null), "html", null, true);
        echo "\", tanggal:\$(\"#tanggal\").val()}, function(json){
                json.forEach(function(value, index){
                \tif(value['tipe'] == \"1\") {
                \t\tkeluar.push(value);
                \t} else if (value['tipe'] == \"0\") {
                \t\tmasuk.push(value);
                \t}

                    jurnal = value;
                    nomor = index+1;
                    value.detail.forEach(function(value, index){
                    \tif (jurnal['tipe'] == \"1\") {
                    \t\t\$(\"#data-body\").append(
\t                            \"<tr>\"+
\t                                \"<td class='text-center'>\"+((index == 0)? nomor+\".\" : \"\")+\"</td>\"+
\t                                \"<td>\"+value['namabarang']+\"</td>\"+
\t                                \"<td>\"+value['spesifikasi']+\"</td>\"+
\t                                \"<td class='text-center'></td>\"+
\t                                \"<td class='text-center'></td>\"+
\t                                \"<td class='text-right'></td>\"+
\t                                \"<td></td>\"+
\t                                \"<td></td>\"+
\t                                \"<td></td>\"+
\t                                \"<td>\"+jurnal['jutgl']+\"</td>\"+
\t                                \"<td>\"+jurnal['spnama']+\"</td>\"+
\t                                \"<td>\"+(-value['judqty'])+\"</td>\"+
\t                                \"<td>\"+((index == 0)? jurnal['judasarno'] : \"\")+\"</td>\"+
\t                                \"<td>\"+jurnal['keterangan']+\"</td>\"+
\t\t\t\t\t\t\t\t\t\"<td>\"+((index == 0)? \"<a href='";
        // line 181
        echo twig_escape_filter($this->env, site_url("persediaan/barang_keluar"), "html", null, true);
        echo "/\"+Base64.encode(jurnal['juno'])+\"' target='_blank'>\"+\"<span class='glyphicon glyphicon-pencil'></span>\"+\"</a>\" : \"\")+\"</td>\"+
\t\t\t\t\t\t\t\t\"</tr>\"
\t                        );
                    \t} else if (jurnal['tipe'] == \"0\") {
                    \t\t\$(\"#data-body\").append(
\t                            \"<tr>\"+
\t                                \"<td class='text-center'>\"+((index == 0)? nomor+\".\" : \"\")+\"</td>\"+
\t                                \"<td>\"+value['namabarang']+\"</td>\"+
\t                                \"<td>\"+value['spesifikasi']+\"</td>\"+
\t                                \"<td class='text-center'>\"+value['judtahunbuat']+\"</td>\"+
\t                                \"<td class='text-center'>\"+((index == 0)? jurnal['judasartgl'] : \"\")+\"</td>\"+
\t                                \"<td class='text-right'>\"+(value['judqty'])+\"</td>\"+
\t                                \"<td>\"+((index == 0)? jurnal['judasarno'] : \"\")+\"</td>\"+
\t                                \"<td>\"+((index == 0)? jurnal['jubuktino'] : \"\")+\"</td>\"+
\t                                \"<td>\"+((index == 0)? jurnal['jubuktitgl'] : \"\")+\"</td>\"+
\t                                \"<td></td>\"+
\t                                \"<td></td>\"+
\t                                \"<td></td>\"+
\t                                \"<td></td>\"+
\t                                \"<td>\"+jurnal['keterangan']+\"</td>\"+
\t\t\t\t\t\t\t\t\t\"<td>\"+((index == 0)? \"<a href='";
        // line 201
        echo twig_escape_filter($this->env, site_url("persediaan/barang_masuk"), "html", null, true);
        echo "/\"+Base64.encode(jurnal['juno'])+\"' target='_blank'>\"+\"<span class='glyphicon glyphicon-pencil'></span>\"+\"</a>\" : \"\")+\"</td>\"+
\t                            \"</tr>\"
\t                        );
                    \t}
                    }, jurnal, nomor);
                });
\t\t\t\t\t
\t\t\t\t\$(\".masuk\").on(\"click\", function(){
\t\t\t\t\t\$.post(\"";
        // line 209
        echo twig_escape_filter($this->env, site_url("persediaan/barang_masuk"), "html", null, true);
        echo "\",{},function(result){
\t\t\t\t\t\t\$(document).html(result);
\t\t\t\t\t},\"html\");
\t\t\t\t});
            },\"json\");
        }
\t\t
        \$(\"#lakukan-cetak\").on('click', function(){
            // window.open(\"";
        // line 217
        echo twig_escape_filter($this->env, site_url("persediaan/print_buku_pakai_habis"), "html", null, true);
        echo "\");
            var skpd = Base64.encode(\"";
        // line 218
        echo twig_escape_filter($this->env, (isset($context["skpd"]) ? $context["skpd"] : null), "html", null, true);
        echo "\");
            var tanggal = Base64.encode(\$(\"#tanggal\").val());
      \t\tvar namaunit = Base64.encode(\"";
        // line 220
        echo twig_escape_filter($this->env, (isset($context["oname"]) ? $context["oname"] : null), "html", null, true);
        echo "\");
            var pengurusbarang = Base64.encode(\$(\"#nama-petugas\").val());
            var nippengurusbarang = Base64.encode(\$(\"#nip-petugas\").val());
            var atasanlangsung = Base64.encode(\$(\"#nama-atasan\").val());
            var nipatasanlangsung = Base64.encode(\$(\"#nip-atasan\").val());
            
            if( skpd === \"\") {skpd = \"-\"}
            if( tanggal === \"\") {tanggal = \"-\"}
            if( namaunit === \"\") {namaunit = \"-\"}
            if( pengurusbarang === \"\") {pengurusbarang = \"-\"}
            if( nippengurusbarang === \"\") {nippengurusbarang = \"-\"}
            if( atasanlangsung === \"\") {atasanlangsung = \"-\"}
            if( nipatasanlangsung === \"\") {nipatasanlangsung = \"-\"}
            
\t\t\twindow.open(\"";
        // line 234
        echo twig_escape_filter($this->env, site_url("persediaan/print_buku_pakai_habis"), "html", null, true);
        echo "/\"+ namaunit + \"/\" + skpd + \"/\" + tanggal + \"/\" + pengurusbarang + \"/\" + nippengurusbarang + \"/\" + atasanlangsung + \"/\" + nipatasanlangsung, \"_blank\");
\t\t\t//tambah organisasi, nama, jabatan, dan NIP dari pengurus gudang dan atasan langsung
        });

        \$(\"#cetak\").on(\"click\", function(){
            \$(\"#pengambil\").hide();
            \$(\"#persiapan-cetak\").modal(\"show\");
        });
\t\t
        \$(\"#kembali\").on(\"click\", function(){
            window.location.href = \"";
        // line 244
        echo twig_escape_filter($this->env, site_url("persediaan"), "html", null, true);
        echo "\";
        });
    });

</script>
";
    }

    public function getTemplateName()
    {
        return "simgo/persediaan/laporan/buku_barang_pakai_habis.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  356 => 244,  343 => 234,  326 => 220,  321 => 218,  317 => 217,  306 => 209,  295 => 201,  272 => 181,  239 => 153,  212 => 129,  208 => 128,  201 => 127,  165 => 96,  161 => 95,  154 => 94,  132 => 76,  123 => 74,  119 => 73,  68 => 25,  55 => 15,  51 => 14,  41 => 7,  35 => 3,  33 => 2,  30 => 1,);
    }
}
