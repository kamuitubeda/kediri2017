<?php

/* aset/kendaraan.html */
class __TwigTemplate_c855eb0baa534e4fe20b90a2d9d2b2fb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("_templates/adminpage.html");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'scripts' => array($this, 'block_scripts'),
            'stylesheets' => array($this, 'block_stylesheets'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_templates/adminpage.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        $this->env->loadTemplate("aset/modal_kendaraan.html")->display($context);
        // line 5
        echo "
<div class=\"row\">
\t<div class=\"col-xs-12\" style=\"padding-left:0px; padding-right:0px;\">
\t\t<div class=\"row\">
\t\t\t<div class=\"col-xs-4\">
\t\t\t\t<label>UNIT</label>
\t\t\t\t<select type=\"text\" name=\"nomor_unit\" class=\"form-control\">
\t\t\t\t\t<option value=\"\">Pilih Unit</option>
\t\t\t\t</select>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"row\" name=\"sub-unit\" style=\"display:none\">
\t\t\t<div class=\"col-xs-4\">
\t\t\t\t<label>SUB UNIT</label>
\t\t\t\t<select type=\"text\" name=\"nomor_sub_unit\" class=\"form-control\">
\t\t\t\t\t<option value=\"\">Pilih Sub Unit</option>
\t\t\t\t</select>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"row\" name=\"daftar_kendaraan\" style=\"display:none\">
\t\t\t<div class=\"col-xs-12\">
\t\t\t\t<div class=\"row\"  style=\"float:right;\">
\t\t\t\t\t\t<button id=\"btnKendaraan\" type=\"button\" style=\"height:30px;\" class=\"btn btn-primary\"><span class=\"glyphicon glyphicon-print\"></span></button>
\t\t\t\t\t\t<button id=\"btnTambahKendaraan\" type=\"button\" style=\"height:30px;\" class=\"btn btn-primary\"><span class=\"glyphicon glyphicon-plus\"></span></button>
\t\t\t\t</div>
\t\t\t\t<div class=\"table-responsive\">
\t\t\t\t\t<table class=\"table table-bordered table-hover table_kendaraan\">
\t\t\t\t\t\t<thead>
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<th style=\"text-align:center\">Nama Unit</th>
\t\t\t\t\t\t\t<th style=\"text-align:center\">ID Aset</th>
\t\t\t\t\t\t\t<th style=\"text-align:center\">Nama Barang</th>
\t\t\t\t\t\t\t<th style=\"text-align:center\">Merk Barang</th>
\t\t\t\t\t\t\t<th style=\"text-align:center\">Nopol</th>
\t\t\t\t\t\t\t<th style=\"text-align:center\">Pengguna</th>
\t\t\t\t\t\t\t<th></th>
\t\t\t\t\t\t</tr>
\t\t\t\t\t\t</thead>
\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t</tbody>
\t\t\t\t\t</table>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
</div>
";
    }

    // line 54
    public function block_scripts($context, array $blocks = array())
    {
        // line 55
        $this->displayParentBlock("scripts", $context, $blocks);
        echo "
<script type=\"text/javaScript\" src=\"";
        // line 56
        echo twig_escape_filter($this->env, base_url("assets/js/jquery.loadmask.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 57
        echo twig_escape_filter($this->env, base_url("assets/js/select2.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 58
        echo twig_escape_filter($this->env, base_url("assets/js/accounting.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 59
        echo twig_escape_filter($this->env, base_url("assets/js/bootstrap-switch.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 60
        echo twig_escape_filter($this->env, base_url("assets/js/xlsx.core.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 61
        echo twig_escape_filter($this->env, base_url("assets/js/Blob.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 62
        echo twig_escape_filter($this->env, base_url("assets/js/FileSaver.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 63
        echo twig_escape_filter($this->env, base_url("assets/js/tableexport.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\">
    jQuery(document).ready(function(\$) {
    \t\$('input[name=\"kapital-checkbox\"]').bootstrapSwitch();
    \t// configure setting accounting.js
\t\taccounting.settings = {
\t\t\tnumber: {
\t\t\t\tprecision : 0,  // default precision on numbers is 0
\t\t\t\tthousand: \".\",
\t\t\t\tdecimal : \",\"
\t\t\t}
\t\t}

\t\tvar n = function(v, p) {
\t\t\treturn accounting.formatNumber(v, p);
\t\t}

\t\tvar u = function(v) {
\t\t\treturn accounting.unformat(v);
\t\t}

    \t//menampilkan list SKPD
\t\t\$.get(\"";
        // line 85
        echo twig_escape_filter($this->env, site_url("aset/list_skpd"), "html", null, true);
        echo "\", {}, function(json){ 
\t\t\tvar listSkpd = json.result;
\t\t\t
\t\t\t\$.each(listSkpd, function (key, value) {
\t\t\t\t\$(\"select[name=nomor_unit]\").append(\$('<option></option>').val(value.NOMOR_UNIT).html(value.NAMA_UNIT));
\t\t\t});

\t\t\t//\$(\"select[name=nomor_unit]\").val(\"";
        // line 92
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "NOMOR_UNIT"), "html", null, true);
        echo "\");
\t\t\t\$(\"select[name=nomor_unit]\").select2();

\t\t\t//reload_skpd();
\t\t\t// var user = \$(\"#user\").text();

\t\t\t// if (user != 'Administrator') {
\t\t\t// \t\$.post(\"";
        // line 99
        echo twig_escape_filter($this->env, site_url("aset/get_skpd_user"), "html", null, true);
        echo "\", {user:user}, function(json){ 
\t\t\t// \t\tvar skpd_user = json.data;
\t\t\t// \t\tvar oid = skpd_user.oid;

\t\t\t// \t\t\$.post(\"";
        // line 103
        echo twig_escape_filter($this->env, site_url("aset/get_skpd_organisasi"), "html", null, true);
        echo "\", {oid:oid}, function(json){ 
\t\t\t// \t\t\tvar skpd_organisasi = json.data;
\t\t\t// \t\t\tvar skpd_name = skpd_organisasi.oname;
\t\t\t\t\t\t
\t\t\t// \t\t\t\$.post(\"";
        // line 107
        echo twig_escape_filter($this->env, site_url("aset/get_skpd_val"), "html", null, true);
        echo "\", {skpd_name:skpd_name}, function(json){ 
\t\t\t// \t\t\t\tvar skpd_val = json.data;
\t\t\t
\t\t\t// \t\t\t\t//untuk mengubah data select 2 jangan lupa tambahkan ini >> .trigger('change');
\t\t\t// \t\t\t\t\$('select[name=\"nomor_unit\"]').val(skpd_val.NOMOR_UNIT).trigger('change');
\t\t\t// \t\t\t\t\$('select[name=\"nomor_unit\"]').prop( \"disabled\", true );           
\t\t\t\t\t\t\t
\t\t\t// \t\t\t}, \"json\");
\t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t// \t\t}, \"json\");
\t\t\t// \t}, \"json\");
\t\t\t// }

\t\t}, \"json\");

\t\t//load sub unit (khusus dinas pendidikan dan dinas kesehatan)
\t\t\$(\"select[name=nomor_unit]\").change(function() {
\t\t\tvar nomor_unit = \$(\"select[name=nomor_unit]\").val();

\t\t\tif(nomor_unit === \"13.10.08.01\" || nomor_unit === \"13.10.07.01\" || nomor_unit === \"13.10.04.01\") {
\t\t\t\tload_sub_unit();
\t\t\t\t\$('div[name=\"sub-unit\"]').show();
\t\t\t} else {
\t\t\t\t\$(\"select[name=nomor_sub_unit]\").val(\"\");
\t\t\t\t\$('div[name=\"sub-unit\"]').hide();
\t\t\t\tload_aset_kendaraan();
\t\t\t}
\t\t});

\t\tvar load_sub_unit = function(callback) {
\t\t\t\$(\"body\").mask(\"Processing...\");
\t\t\tvar nomor_unit = \$(\"select[name=nomor_unit]\").val();

\t\t\t\$.post(\"";
        // line 141
        echo twig_escape_filter($this->env, site_url("aset/get_list_sub_unit"), "html", null, true);
        echo "\", {
\t\t\t\tnomor_unit: nomor_unit
\t\t\t}, function(json){ 
\t\t\t\tvar data = json.data;
\t\t\t\tvar el = \$(\"select[name=nomor_sub_unit]\");

\t\t\t\tel.empty();
\t\t\t\tel.append(\"<option value=''>Pilih Sub Unit</option>\");
\t\t\t\tfor (var i=0; i<data.length; i++) {
\t\t\t\t\tvar k = data[i];
\t\t\t\t\tel.append(\$(\"<option></option>\").attr(\"value\", k.NOMOR_SUB_UNIT).html(k.NOMOR_SUB_UNIT + \" | \" + k.NAMA_SUB_UNIT));
\t\t\t\t}

\t\t\t\tel.select2();

\t\t\t\tload_aset_kendaraan();
\t\t\t\t\$(\"body\").unmask();

\t\t\t\tif (callback !== undefined) {
\t\t\t\t\tcallback();
\t\t\t\t}
\t\t\t}, \"json\");
\t\t}

\t\t//menampilkan daftar aset kendaraan
\t\t\$(\"select[name=nomor_sub_unit]\").change(function() {
\t\t\tload_aset_kendaraan();
\t\t});
\t\t
\t\tfunction load_aset_kendaraan() {
    \t\t\$('tbody').empty();
    \t\t\$(\"body\").mask(\"Processing...\");
    \t\t\$('caption').empty();

    \t\tvar nomor_unit = \$(\"select[name=nomor_unit]\").val();
    \t\tvar nomor_sub_unit = \$(\"select[name=nomor_sub_unit]\").val();
    \t\tvar nama_unit = \$(\"select[name=nomor_unit] option:selected\").text();
    \t\tvar nama_sub_unit = \$(\"select[name=nomor_sub_unit] option:selected\").text();

    \t\t\$('div[name=\"daftar_kendaraan\"]').show();

    \t\t\$.post(\"";
        // line 182
        echo twig_escape_filter($this->env, site_url("aset/get_list_kendaraan_unit"), "html", null, true);
        echo "\", {nomor_unit:nomor_unit, nomor_sub_unit:nomor_sub_unit}, function(json) {
    \t\t\tvar data = json.data;

    \t\t\tif(nomor_sub_unit !== '') {
    \t\t\t\tnama_unit = nama_sub_unit;
    \t\t\t}
\t\t\t\t
\t\t\t\tvar tables = \$(\"table\");

\t\t\t\t\$.each(data, function(key, value) {
\t\t\t\t\tvar value = data[key];

\t\t\t\t\tvar html = [
\t\t\t\t\t\t'<tr id=\"row_'+key+'\">',
\t\t\t\t\t\t\t'<td>' + nama_unit + '</td>',
\t\t\t\t\t\t\t'<td>' + value.id_aset + '</td>',
\t\t\t\t\t\t\t'<td>' + value.nama_barang + '</td>',
\t\t\t\t\t\t\t'<td>' + value.merk_barang + '</td>',
\t\t\t\t\t\t\t'<td>' + value.nopol + '</td>',
\t\t\t\t\t\t\t'<td>' + value.pengguna + '</td>',
\t\t\t\t\t\t\t'<td width=\"15px;\" style=\"text-align:center;\">',
\t\t\t\t\t\t\t\t'<a id=\"hapus_' + key + '\" href=\"#\">',
\t\t\t\t\t\t\t\t\t'<span class=\"glyphicon glyphicon-trash\"></span>',
\t\t\t\t\t\t\t\t'</a>',
\t\t\t\t\t\t\t'</td>',
\t\t\t\t\t\t'</tr>'
\t\t\t\t\t].join(\"\\r\\n\");

\t\t\t\t\t\$('.table_kendaraan > tbody').append(html);

\t\t\t\t\t\$('#hapus_'+key).on(\"click\", function(e){
\t                    e.preventDefault();
\t                    \$.post(\"";
        // line 214
        echo twig_escape_filter($this->env, site_url("aset/hapus_aset_kendaraan"), "html", null, true);
        echo "\", {id_aset:value.id_aset}, function(json) {
\t                    \tload_aset_kendaraan();
\t                    }, \"json\");
\t                });
\t\t\t\t\t
\t\t\t\t});

\t\t\t\t\$(\"body\").unmask();

\t\t\t\ttables.tableExport({
\t\t\t\t    bootstrap: true,
\t\t\t\t    format: [\"xlsx\"]
\t\t\t\t});
\t\t\t}, \"json\");
    \t}

    \t\$(\"#btnTambahKendaraan\").on(\"click\", function() {
\t\t\tvar nomor_unit = \$(\"select[name=nomor_unit]\").val();
    \t\tvar nomor_sub_unit = \$(\"select[name=nomor_sub_unit]\").val();

\t\t\t\$.post(\"";
        // line 234
        echo twig_escape_filter($this->env, site_url("aset/get_list_kendaraan_by_nomor_unit"), "html", null, true);
        echo "\", {
\t\t\t\tnomor_unit: nomor_unit, nomor_sub_unit: nomor_sub_unit
\t\t\t}, function(json){ 
\t\t\t\tvar data = json.data;
\t\t\t\tvar el = \$(\"select[name=id-aset-kendaraan]\");

\t\t\t\tel.empty();
\t\t\t\tel.append(\"<option value=''>Pilih Kendaraan</option>\");
\t\t\t\tfor (var i=0; i<data.length; i++) {
\t\t\t\t\tvar k = data[i];
\t\t\t\t\tel.append(\$(\"<option></option>\").attr(\"value\", k.ID_ASET).html(k.NAMA_BARANG + \" \" + k.MERK_ALAMAT + \" - \" + k.NOPOL));
\t\t\t\t}

\t\t\t\tel.select2();
\t\t\t\t\$(\"body\").unmask();

\t\t\t\tif (callback !== undefined) {
\t\t\t\t\tcallback();
\t\t\t\t}
\t\t\t}, \"json\");

            \$(\"#tambah-kendaraan\").modal(\"show\");
        });

        \$(\"button[name=simpan]\").on(\"click\", function() {
        \tvar id_aset = \$(\"select[name=id-aset-kendaraan]\").val();
        \tvar pengguna = \$(\"input[name=pengguna]\").val();
        \tvar nama_unit = \$(\"select[name=nomor_unit] option:selected\").text();

    \t\t\$(\"body\").mask(\"Processing...\");
        \t\$.post(\"";
        // line 264
        echo twig_escape_filter($this->env, site_url("aset/get_aset_by_id_aset"), "html", null, true);
        echo "\", {id_aset:id_aset}, function(json) {
        \t\tvar aset = json.aset;
        \t\tvar html = '';

\t\t\t\t\$.each(aset, function(key, value) {
\t\t\t\t\tvar value = aset[key];

\t\t\t\t\tvar html2 = [
\t\t\t\t\t\t'<tr id=\"row_'+key+'\">',
\t\t\t\t\t\t\t'<td>' + nama_unit + '</td>',
\t\t\t\t\t\t\t'<td>' + value.ID_ASET + '</td>',
\t\t\t\t\t\t\t'<td>' + value.NAMA_BARANG + '</td>',
\t\t\t\t\t\t\t'<td>' + value.MERK_ALAMAT + '</td>',
\t\t\t\t\t\t\t'<td>' + value.NOPOL + '</td>',
\t\t\t\t\t\t\t'<td>' + pengguna + '</td>',
\t\t\t\t\t\t\t'<td width=\"15px;\" style=\"text-align:center;\">',
\t\t\t\t\t\t\t\t'<a id=\"hapus_' + key + '\" href=\"#\">',
\t\t\t\t\t\t\t\t\t'<span class=\"glyphicon glyphicon-trash\"></span>',
\t\t\t\t\t\t\t\t'</a>',
\t\t\t\t\t\t\t'</td>',
\t\t\t\t\t\t'</tr>'
\t\t\t\t\t].join(\"\\r\\n\");

\t\t\t\t\t\$('.table_kendaraan > tbody').append(html2);

\t\t\t\t\t\$('#hapus_'+key).on(\"click\", function(e){
\t                    e.preventDefault();
\t                    \$.post(\"";
        // line 291
        echo twig_escape_filter($this->env, site_url("aset/hapus_aset_kendaraan"), "html", null, true);
        echo "\", {id_aset:value.id_aset}, function(json) {
\t                    \tload_aset_kendaraan();
\t                    }, \"json\");
\t                });
\t\t\t\t});

\t\t\t\t\$(\"body\").unmask();
        \t}, \"json\");


        \t\$.post(\"";
        // line 301
        echo twig_escape_filter($this->env, site_url("aset/simpan_aset_kendaraan"), "html", null, true);
        echo "\", {id_aset:id_aset, pengguna:pengguna}, function(json) {
        \t}, \"json\");

        \t\$(\"#tambah-kendaraan\").modal(\"hide\");
        \t\$('select[name=id-aset-kendaraan]').val('').trigger('change');
        });


\t\t\$(\"#btnKendaraan\").click(function() {
\t\t\tvar nomor_unit = \$(\"select[name=nomor_unit]\").val();
\t\t\tvar nomor_sub_unit = \$(\"select[name=nomor_sub_unit]\").val();

\t\t\tif(nomor_sub_unit === \"\") {
    \t\t\twindow.open( \"";
        // line 314
        echo twig_escape_filter($this->env, site_url("aset/print_kendaraan"), "html", null, true);
        echo "/\"+ nomor_unit, '_blank');
\t\t\t} else {
    \t\t\twindow.open( \"";
        // line 316
        echo twig_escape_filter($this->env, site_url("aset/print_kendaraan"), "html", null, true);
        echo "/\"+ nomor_sub_unit, '_blank');
\t\t\t}

\t\t});
\t});
</script>
";
    }

    // line 325
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 326
        echo "\t";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 327
        echo twig_escape_filter($this->env, base_url("assets/css/devexpress-like/jquery-ui.css"), "html", null, true);
        echo "\" />
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 328
        echo twig_escape_filter($this->env, base_url("assets/css/select2.min.css"), "html", null, true);
        echo "\" />
\t<link href=\"";
        // line 329
        echo twig_escape_filter($this->env, base_url("assets/css/jquery.loadmask.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
\t<link href=\"";
        // line 330
        echo twig_escape_filter($this->env, base_url("assets/css/bootstrap-switch.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 331
        echo twig_escape_filter($this->env, base_url("assets/css/tableexport.css"), "html", null, true);
        echo "\" />\t

\t<style type=\"text/css\">
\t\t.control-label{
\t\t\tpadding-top: 8px !important;
\t\t}

\t\tselect.form-control {
\t\t\theight: 48px !important;
\t\t}

\t\tinput.form-control {
\t\t\theight: 34px !important;
\t\t}

\t\ttextarea.form-control {
\t\t\theight:auto !important;
\t\t}

\t\t.row {
\t\t\tpadding:10px 0px;
\t\t}

\t\t.spacer {
\t\t    margin-top: 20px;
\t\t}

\t\t.ui-autocomplete{
\t\t\tbackground: none repeat scroll 0 0 white;
\t\t}

\t\t.input-group-addon{
\t\t\tborder-radius: 0 !important;
\t\t}

\t\t.input-group .form-control{
\t\t\tz-index: 0; !important;
\t\t}

\t\t.select2-selection__rendered {
\t\t\theight: 28px;
\t\t}
\t\t.table > thead > tr > th {
\t\t\tvertical-align: middle;
\t\t}
\t\t#tampilkan {
\t\t\tmargin-top: 22px;
\t\t\theight: 30px;
\t\t}
\t\t.btn-toolbar {
\t\t\ttext-align: left !Important;
\t\t}
\t</style>
";
    }

    public function getTemplateName()
    {
        return "aset/kendaraan.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  450 => 331,  446 => 330,  442 => 329,  438 => 328,  434 => 327,  429 => 326,  426 => 325,  415 => 316,  410 => 314,  394 => 301,  381 => 291,  351 => 264,  318 => 234,  295 => 214,  260 => 182,  216 => 141,  179 => 107,  172 => 103,  165 => 99,  155 => 92,  145 => 85,  120 => 63,  116 => 62,  112 => 61,  108 => 60,  104 => 59,  100 => 58,  96 => 57,  92 => 56,  88 => 55,  85 => 54,  35 => 5,  33 => 4,  30 => 3,);
    }
}
