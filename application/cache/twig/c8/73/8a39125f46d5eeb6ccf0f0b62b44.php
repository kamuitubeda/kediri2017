<?php

/* aset/ekstrakom.html */
class __TwigTemplate_c8738a39125f46d5eeb6ccf0f0b62b44 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("_templates/adminpage.html");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'scripts' => array($this, 'block_scripts'),
            'stylesheets' => array($this, 'block_stylesheets'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_templates/adminpage.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "
<div class=\"row\">
\t<div class=\"col-xs-12\" style=\"padding-left:0px; padding-right:0px;\">
\t\t<div class=\"row\">
\t\t\t<div class=\"col-xs-12\">
\t\t\t\t<p class=\"bg-danger\" style=\"height:20px; padding:10px; font-weight:bold;\">Back up database dulu sebelum melakukan perubahan kepemilikan</p>
\t\t\t</div>
\t\t\t<div class=\"col-xs-12\">
\t\t\t\t<div class=\"col-xs-4\">
\t\t\t\t\t<label>SKPD</label>
\t\t\t\t\t<select type=\"text\" name=\"nomor_unit\" class=\"form-control\">
\t\t\t\t\t\t<option value=\"\">Pilih SKPD</option>
\t\t\t\t\t</select>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-xs-2\">
\t\t\t\t\t<button id=\"do_ekstrakom\" name=\"do_ekstrakom\" type=\"button\" class=\"btn btn-success\" style=\"float:left;\">Ubah Kepemilikan</button>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"row\" name=\"sub-unit\" style=\"display:none\">
\t\t\t<div class=\"col-xs-12\">
\t\t\t\t<div class=\"col-xs-5\">
\t\t\t\t\t<label>SUB UNIT</label>
\t\t\t\t\t<select type=\"text\" name=\"nomor_sub_unit\" class=\"form-control\">
\t\t\t\t\t\t<option value=\"\">Pilih SUB UNIT</option>
\t\t\t\t\t</select>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
</div>
";
    }

    // line 38
    public function block_scripts($context, array $blocks = array())
    {
        // line 39
        $this->displayParentBlock("scripts", $context, $blocks);
        echo "
<script type=\"text/javaScript\" src=\"";
        // line 40
        echo twig_escape_filter($this->env, base_url("assets/js/jquery.loadmask.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 41
        echo twig_escape_filter($this->env, base_url("assets/js/select2.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 42
        echo twig_escape_filter($this->env, base_url("assets/js/accounting.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 43
        echo twig_escape_filter($this->env, base_url("assets/js/jquery.validate.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\">
    jQuery(document).ready(function(\$) {
    \tvar content = \$(\"#content\");

    \t// configure setting accounting.js
\t\taccounting.settings = {
\t\t\tnumber: {
\t\t\t\tprecision : 0,  // default precision on numbers is 0
\t\t\t\tthousand: \".\",
\t\t\t\tdecimal : \",\"
\t\t\t}
\t\t}

\t\tvar n = function(v, p) {
\t\t\treturn accounting.formatNumber(v, p);
\t\t}

\t\tvar u = function(v) {
\t\t\treturn accounting.unformat(v);
\t\t}

\t\t//menampilkan list SKPD
\t\t\$.get(\"";
        // line 66
        echo twig_escape_filter($this->env, site_url("aset/list_skpd"), "html", null, true);
        echo "\", {}, function(json){ 
\t\t\tvar listSkpd = json.result;
\t\t\t
\t\t\t\$.each(listSkpd, function (key, value) {
\t\t\t\t\$(\"select[name=nomor_unit]\").append(\$('<option></option>').val(value.NOMOR_UNIT).html(value.NAMA_UNIT));
\t\t\t});

\t\t\t//\$(\"select[name=nomor_unit]\").val(\"";
        // line 73
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "NOMOR_UNIT"), "html", null, true);
        echo "\");
\t\t\t\$(\"select[name=nomor_unit]\").select2();

\t\t\t//reload_skpd();
\t\t\tvar user = \$(\"#user\").text();

\t\t\tif (user != 'Administrator') {
\t\t\t\t\$.post(\"";
        // line 80
        echo twig_escape_filter($this->env, site_url("aset/get_skpd_user"), "html", null, true);
        echo "\", {user:user}, function(json){ 
\t\t\t\t\tvar skpd_user = json.data;
\t\t\t\t\tvar oid = skpd_user.oid;
\t\t\t\t\t//console.log(skpd_user);

\t\t\t\t\t\$.post(\"";
        // line 85
        echo twig_escape_filter($this->env, site_url("aset/get_skpd_organisasi"), "html", null, true);
        echo "\", {oid:oid}, function(json){ 
\t\t\t\t\t\tvar skpd_organisasi = json.data;
\t\t\t\t\t\tvar skpd_name = skpd_organisasi.oname;
\t\t\t\t\t\t
\t\t\t\t\t\t\$.post(\"";
        // line 89
        echo twig_escape_filter($this->env, site_url("aset/get_skpd_val"), "html", null, true);
        echo "\", {skpd_name:skpd_name}, function(json){ 
\t\t\t\t\t\t\tvar skpd_val = json.data;
\t\t\t
\t\t\t\t\t\t\t//untuk mengubah data select 2 jangan lupa tambahkan ini >> .trigger('change');
\t\t\t\t\t\t\t\$('select[name=\"nomor_unit\"]').val(skpd_val.NOMOR_UNIT).trigger('change');
\t\t\t\t\t\t\t\$('select[name=\"nomor_unit\"]').prop( \"disabled\", true );           
\t\t\t\t\t\t\t
\t\t\t\t\t\t}, \"json\");
\t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t\t\t}, \"json\");
\t\t\t\t}, \"json\");
\t\t\t}

\t\t}, \"json\");

\t\t//load sub unit (khusus dinas pendidikan dan dinas kesehatan)
\t\t\$(\"select[name=nomor_unit]\").change(function() {
\t\t\tvar nomor_unit = \$(\"select[name=nomor_unit]\").val();

\t\t\tif(nomor_unit === \"13.10.08.01\" || nomor_unit === \"13.10.07.01\") {
\t\t\t\tload_sub_unit();
\t\t\t\t\$('div[name=\"sub-unit\"]').show();
\t\t\t} else {
\t\t\t\t\$(\"select[name=nomor_sub_unit]\").val(\"\");
\t\t\t\t\$('div[name=\"sub-unit\"]').hide();
\t\t\t}
\t\t});

\t\tvar load_sub_unit = function(callback) {
\t\t\t\$(\"body\").mask(\"Processing...\");
\t\t\tvar nomor_unit = \$(\"select[name=nomor_unit]\").val();

\t\t\t\$.post(\"";
        // line 122
        echo twig_escape_filter($this->env, site_url("aset/get_list_sub_unit"), "html", null, true);
        echo "\", {
\t\t\t\tnomor_unit: nomor_unit
\t\t\t}, function(json){ 
\t\t\t\tvar data = json.data;
\t\t\t\tvar el = \$(\"select[name=nomor_sub_unit]\");

\t\t\t\tel.empty();
\t\t\t\tel.append(\"<option value=''>Pilih Sub Unit</option>\");
\t\t\t\tfor (var i=0; i<data.length; i++) {
\t\t\t\t\tvar k = data[i];
\t\t\t\t\tel.append(\$(\"<option></option>\").attr(\"value\", k.NOMOR_SUB_UNIT).html(k.NOMOR_SUB_UNIT + \" | \" + k.NAMA_SUB_UNIT));
\t\t\t\t\t// el.append(\"<option value='{0}'>{1}</option>\".format(k.NO_REGISTER, k.NAMA_BARANG));
\t\t\t\t}

\t\t\t\tel.select2();
\t\t\t\t\$(\"body\").unmask();

\t\t\t\tif (callback !== undefined) {
\t\t\t\t\tcallback();
\t\t\t\t}
\t\t\t}, \"json\");
\t\t}

\t\t\$(\"button[name=do_ekstrakom]\").on(\"click\", function() {
\t\t\t\$(\"body\").mask(\"Silakan tunggu. Proses reklas membutuhkan waktu 2-5 menit.\");
\t\t\tvar nomor_unit = \$(\"select[name=nomor_unit]\").val();
\t\t\tvar nomor_sub_unit = \$(\"select[name=nomor_sub_unit]\").val();
\t\t\t\t\t\t
\t\t\t\$.post(\"";
        // line 150
        echo twig_escape_filter($this->env, site_url("aset/do_ekstrakom"), "html", null, true);
        echo "\", {nomor_unit:nomor_unit, nomor_sub_unit:nomor_sub_unit}, function(json){ 
\t\t\t\t\$(\"body\").unmask();
\t\t\t}, \"json\");
\t\t});

\t});
</script>
";
    }

    // line 160
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 161
        echo "\t";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 162
        echo twig_escape_filter($this->env, base_url("assets/css/devexpress-like/jquery-ui.css"), "html", null, true);
        echo "\" />
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 163
        echo twig_escape_filter($this->env, base_url("assets/css/select2.min.css"), "html", null, true);
        echo "\" />
\t<link href=\"";
        // line 164
        echo twig_escape_filter($this->env, base_url("assets/css/jquery.loadmask.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">\t\t

\t<style type=\"text/css\">
\t\t.control-label{
\t\t\tpadding-top: 8px !important;
\t\t}

\t\tselect.form-control {
\t\t\theight: 48px !important;
\t\t}

\t\tinput.form-control {
\t\t\theight: 34px !important;
\t\t}

\t\ttextarea.form-control {
\t\t\theight:auto !important;
\t\t}

\t\t.row {
\t\t\tpadding:10px 0px;
\t\t}

\t\t.spacer {
\t\t    margin-top: 20px;
\t\t}

\t\t.ui-autocomplete{
\t\t\tbackground: none repeat scroll 0 0 white;
\t\t}

\t\t.input-group-addon{
\t\t\tborder-radius: 0 !important;
\t\t}

\t\t.input-group .form-control{
\t\t\tz-index: 0; !important;
\t\t}

\t\t.select2-selection__rendered {
\t\t\theight: 28px;
\t\t}
\t\t.select2-container {
\t\t\twidth: 100% !important;
\t\t\tpadding: 0;
\t\t}
\t\t#btnTambahRenov {
\t\t\tmargin-top: 23px;
\t\t\theight: 30px;
\t\t}
\t\t#do_ekstrakom {
\t\t\tmargin-top: 23px;
\t\t\theight: 30px;
\t\t}
\t</style>
";
    }

    public function getTemplateName()
    {
        return "aset/ekstrakom.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  243 => 164,  239 => 163,  235 => 162,  230 => 161,  227 => 160,  215 => 150,  184 => 122,  148 => 89,  141 => 85,  133 => 80,  123 => 73,  113 => 66,  87 => 43,  83 => 42,  79 => 41,  75 => 40,  71 => 39,  68 => 38,  33 => 4,  30 => 3,);
    }
}
