<?php

/* simgo/persediaan/katalog_stok_opname.html */
class __TwigTemplate_c873c7e271726c3df91a5a3f06cd0f0b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("_templates/adminpage.html");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'scripts' => array($this, 'block_scripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_templates/adminpage.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 1
    public function block_content($context, array $blocks = array())
    {
        // line 2
        echo "<div class=\"container\">
    <div class=\"row\">
        <div class=\"col-md-12\">
            <ol class=\"breadcrumb\">
                <li><a href=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("persediaan"), "html", null, true);
        echo "\">Persediaan</a></li>
                <li class=\"active\">Daftar Stock Opname</li>
            </ol>
        </div>
    </div>
    <div class=\"row\">
        <div class=\"col-md-4 pull-right\">
            <div data-date-format=\"mm-yyyy\" data-date=\"";
        // line 13
        echo twig_escape_filter($this->env, (isset($context["today"]) ? $context["today"] : null), "html", null, true);
        echo "\" datepicker class=\"input-group input-append date\">
                <input id=\"tanggal\" name=\"tanggal\" readonly=\"\" class=\"form-control\" value=\"";
        // line 14
        echo twig_escape_filter($this->env, (isset($context["today"]) ? $context["today"] : null), "html", null, true);
        echo "\"></input>
                <span class=\"input-group-addon add-on\"><i class=\"icon-calendar\"></i></span>
            </div>
        </div>
    </div>
    <br/>
    <div class=\"row\">
        <div class=\"col-md-12\">
            <table class=\"table table-striped table-bordered\" id=\"tabel-daftar-stok-opname\">
                <thead>
                    <tr>
                        <td class=\"col-md-2\">Tanggal</td>
                        <td>Nomor</td>
                        <td class=\"col-md-1\"></td>
                    </tr>
                </thead>
                <tbody>
                    <tr></tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

";
    }

    // line 38
    public function block_stylesheets($context, array $blocks = array())
    {
        echo " ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 39
        echo twig_escape_filter($this->env, base_url("assets/css/datepicker3.css"), "html", null, true);
        echo "\" />

";
    }

    // line 41
    public function block_scripts($context, array $blocks = array())
    {
        echo " ";
        $this->displayParentBlock("scripts", $context, $blocks);
        echo "
<script type=\"text/javaScript\" src=\"";
        // line 42
        echo twig_escape_filter($this->env, base_url("assets/js/bootstrap-datepicker.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 43
        echo twig_escape_filter($this->env, base_url("assets/js/base64.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\">
    jQuery(function(\$){
        
        var data = [];
        
        showData();
        
        \$(\"*[datepicker]\").datepicker({
            autoclose:true,
            orientation:\"bottom\",
            format: \"mm-yyyy\",
            startView: \"months\", 
            minViewMode: \"months\"
        });
        
        function showData(){
            \$.post(\"";
        // line 60
        echo twig_escape_filter($this->env, site_url("persediaan/get_populasi_stok_opname"), "html", null, true);
        echo "\", {skpd:\"";
        echo twig_escape_filter($this->env, (isset($context["skpd"]) ? $context["skpd"] : null), "html", null, true);
        echo "\", tanggal:\$(\"#tanggal\").val()}, function(json){
                \$(\"#tabel-daftar-stok-opname > tbody\").empty();
                \$(\"#tabel-daftar-stok-opname > tbody\").append(\"<tr></tr><tr></tr>\");
                for(x in json){
                    \$(\"#tabel-daftar-stok-opname\").append(\"<tr>\"+
                        \"<td>\"+json[x].jutgl+\"</td>\"+
                        \"<td>\"+json[x].juno+\"</td>\"+
                        \"<td class='text-center'>\"+\"<a href='";
        // line 67
        echo twig_escape_filter($this->env, site_url("persediaan/stok_opname"), "html", null, true);
        echo "/\"+Base64.encode(json[x].juno)+\"'>\"+\"<span class='glyphicon glyphicon-search'></span>\"+\"</a>\"+\"</td>\"+
                    \"</tr>\");
                }
            } ,\"json\");
        }
        
        \$(\"#tanggal\").on(\"change\", function(){
            showData();
        });
    });
</script>

";
    }

    public function getTemplateName()
    {
        return "simgo/persediaan/katalog_stok_opname.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  139 => 67,  127 => 60,  107 => 43,  103 => 42,  96 => 41,  89 => 39,  82 => 38,  53 => 14,  49 => 13,  39 => 6,  33 => 2,  30 => 1,);
    }
}
