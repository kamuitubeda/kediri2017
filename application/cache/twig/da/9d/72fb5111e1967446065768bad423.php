<?php

/* aset/daftar_pengadaan.html */
class __TwigTemplate_da9d72fb5111e1967446065768bad423 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("_templates/adminpage.html");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'scripts' => array($this, 'block_scripts'),
            'stylesheets' => array($this, 'block_stylesheets'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_templates/adminpage.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"container\">
\t<ul id=\"data\" class=\"nav nav-tabs\">
\t\t<li class=\"active\"><a href=\"#draft\" data-toggle=\"tab\">Draft</a></li>
\t\t<li><a href=\"#posting\" data-toggle=\"tab\">Posting</a></li>
\t</ul>
\t<div class=\"tab-content\">
\t\t<div class=\"spacer\"></div>
\t\t<div class=\"tab-pane fade in active\" id=\"draft\">
\t\t\t<div class=\"col-xs-12\">
\t\t\t\t<div class=\"col-md-3 pull-left\">
\t\t\t\t\t<select type=\"text\" id=\"nomor_lokasi\" class=\"form-control\">
\t\t\t\t\t\t<option value=\"\">Semua SKPD</option>
\t\t\t\t\t</select>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-md-3 pull-right\">
\t\t\t\t\t<input name=\"cari\" type=\"text\" class=\"form-control\" placeholder=\"Pencarian\" />
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"clearfix\"></div>
\t\t\t<div class=\"spacer\"></div>
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-xs-12\">
\t\t\t\t\t<div class=\"table-responsive\" style=\"overflow:auto;\">
\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t<table class=\"table table-striped table-hover\">
\t\t\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<th></th>
\t\t\t\t\t\t\t\t\t\t<th class=\"center\">Kode Kegiatan</th>
\t\t\t\t\t\t\t\t\t\t<th class=\"center\">Kegiatan</th>
\t\t\t\t\t\t\t\t\t\t<th class=\"center\">Nama Rekanan/Asal</th>
\t\t\t\t\t\t\t\t\t\t<th class=\"center\">No.Jurnal</th>
\t\t\t\t\t\t\t\t\t\t<th class=\"center\">Tgl.Jurnal</th>
\t\t\t\t\t\t\t\t\t\t<th class=\"center\">No.SPK/SP/Kontrak</th>
\t\t\t\t\t\t\t\t\t\t<th class=\"center\">No.BA Penerimaan</th>
\t\t\t\t\t\t\t\t\t\t<th class=\"center\">Nilai SPK</th>
\t\t\t\t\t\t\t\t\t\t<th class=\"center\">Uraian SPK</th>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t\t<div id=\"pagination\">
\t\t\t\t\t\t";
        // line 51
        echo (isset($context["pagination"]) ? $context["pagination"] : null);
        echo "
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"tab-pane fade in\" id=\"posting\">
\t\t\t<div class=\"col-xs-12\">
\t\t\t\t<div class=\"col-md-3 pull-left\">
\t\t\t\t\t<select type=\"text\" id=\"nomor_lokasi2\" class=\"form-control\">
\t\t\t\t\t\t<option value=\"\">Semua SKPD</option>
\t\t\t\t\t</select>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-md-3 pull-right\">
\t\t\t\t\t<input name=\"cari2\" type=\"text\" class=\"form-control\" placeholder=\"Pencarian\" />
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"clearfix\"></div>
\t\t\t<div class=\"spacer\"></div>
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-xs-12\">
\t\t\t\t\t<div class=\"table-responsive\" style=\"overflow:auto;\">
\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t<table class=\"table table-striped table-hover\">
\t\t\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<th></th>
\t\t\t\t\t\t\t\t\t\t<th class=\"center\">Kode Kegiatan</th>
\t\t\t\t\t\t\t\t\t\t<th class=\"center\">Kegiatan</th>
\t\t\t\t\t\t\t\t\t\t<th class=\"center\">Nama Rekanan/Asal</th>
\t\t\t\t\t\t\t\t\t\t<th class=\"center\">No.Jurnal</th>
\t\t\t\t\t\t\t\t\t\t<th class=\"center\">Tgl.Jurnal</th>
\t\t\t\t\t\t\t\t\t\t<th class=\"center\">No.SPK/SP/Kontrak</th>
\t\t\t\t\t\t\t\t\t\t<th class=\"center\">No.BA Penerimaan</th>
\t\t\t\t\t\t\t\t\t\t<th class=\"center\">Nilai SPK</th>
\t\t\t\t\t\t\t\t\t\t<th class=\"center\">Uraian SPK</th>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t\t\t
\t\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t\t<div id=\"pagination\">
\t\t\t\t\t\t";
        // line 96
        echo (isset($context["pagination"]) ? $context["pagination"] : null);
        echo "
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
</div>
";
    }

    // line 106
    public function block_scripts($context, array $blocks = array())
    {
        // line 107
        $this->displayParentBlock("scripts", $context, $blocks);
        echo "
<script type=\"text/javaScript\" src=\"";
        // line 108
        echo twig_escape_filter($this->env, base_url("assets/js/accounting.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 109
        echo twig_escape_filter($this->env, base_url("assets/js/base64.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\">
jQuery(document).ready(function(\$) {
\tif (!String.prototype.format) {
\t\tString.prototype.format = function() {
\t\t\tvar args = arguments;
\t\t\treturn this.replace(/{(\\d+)}/g, function(match, number) { 
\t\t\t\treturn typeof args[number] != 'undefined' ? args[number] : match;
\t\t\t});
\t\t};
\t}

\tvar delay = (function(){
\t\tvar timer = 0;
\t\t\treturn function(callback, ms){
\t\t\tclearTimeout (timer);
\t\t\ttimer = setTimeout(callback, ms);
\t\t};
\t})();


\t// populasi selectbox skpd
\t\$.post(\"";
        // line 131
        echo twig_escape_filter($this->env, site_url("aset/list_sub_skpd"), "html", null, true);
        echo "\", {}, function(json){\t\t\t\t
\t\t\$.each(json.result, function (key, value) {
\t\t\t\$(\"#nomor_lokasi\").append(\$('<option></option>').val(value.NOMOR_SUB_UNIT).html(value.NAMA_SUB_UNIT));
\t\t\t\$(\"#nomor_lokasi2\").append(\$('<option></option>').val(value.NOMOR_SUB_UNIT).html(value.NAMA_SUB_UNIT));
\t\t});
\t}, \"json\");
});
</script>
";
    }

    // line 142
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 143
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "

<style type=\"text/css\">
\t.control-label{
\t\tpadding-top: 8px !important;
\t}

\t*[class^='col-'] {
\t\tpadding-left: 0px;
\t\tpadding-right: 0px;
\t}

\tselect.form-control {
\t\theight: 48px !important;
\t}

\tinput.form-control {
\t\theight: 34px !important;
\t}

\ttextarea.form-control {
\t\theight:auto !important;
\t}

\t.spacer {
\t    margin-top: 20px;
\t}
</style>
";
    }

    public function getTemplateName()
    {
        return "aset/daftar_pengadaan.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  194 => 143,  191 => 142,  178 => 131,  153 => 109,  149 => 108,  145 => 107,  142 => 106,  130 => 96,  82 => 51,  33 => 4,  30 => 3,);
    }
}
