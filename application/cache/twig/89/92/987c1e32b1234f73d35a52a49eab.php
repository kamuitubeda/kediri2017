<?php

/* simgo/persediaan/pengadaan_barang.html */
class __TwigTemplate_8992987c1e32b1234f73d35a52a49eab extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("_templates/adminpage.html");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'scripts' => array($this, 'block_scripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_templates/adminpage.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        $this->env->loadTemplate("simgo/persediaan/modal/modal_tambah_pengadaan.html")->display($context);
        // line 5
        echo "<div class=\"container\">
\t<div class=\"row\">
\t\t<div class=\"col-md-12\">
\t\t\t<ol class=\"breadcrumb\">
\t\t\t\t<li><a href=\"";
        // line 9
        echo twig_escape_filter($this->env, site_url("persediaan"), "html", null, true);
        echo "\">Persediaan</a></li>
\t\t\t\t<li class=\"active\">Pengadaan</li>
\t\t\t</ol>
\t\t</div>
\t</div>
\t<div class=\"row\">
\t\t<div class=\"col-md-12\">
\t\t\t<div class=\"form-group\">
\t\t\t\t<label for=\"nomor\">Nomor Dasar Pengadaan (SPK / Kuitansi / Lainnya)</label>
\t\t\t\t<select class=\"form-control\" id=\"nomor\" name=\"nomor\"><option></option></select>
\t\t\t</div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<label for=\"vendor\">Vendor</label>
\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"vendor\" name=\"vendor\" placeholder=\"Vendor\">
\t\t\t</div>
\t\t\t<div class=\"form-group\">
\t\t\t\t<label for=\"vendor\">Estimasi Pengiriman</label>
\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"estimasi\" name=\"estimasi\" placeholder=\"Estimasi Pengiriman\">
\t\t\t</div>
\t\t</div>
\t</div>
\t<div class=\"row\" name=\"barang_pakai_habis\" style=\"display:none\">
\t\t<div class=\"col-xs-12\">
\t\t\t<div class=\"table-responsive\">
\t\t\t<label>Daftar Barang Pakai Habis</label>
\t\t\t\t<table class=\"table table_pakai_habis\">
\t\t\t\t\t<thead>
\t\t\t\t\t<tr>
\t\t\t\t\t\t<th style=\"text-align:center\">Kode</th>
\t\t\t\t\t\t<th style=\"text-align:center\">Nama Barang</th>
\t\t\t\t\t\t<th style=\"text-align:center\">Merk</th>
\t\t\t\t\t\t<th style=\"text-align:center\">Jumlah Barang</th>
\t\t\t\t\t\t<th style=\"text-align:center\">Satuan</th>
\t\t\t\t\t\t<th style=\"text-align:center\">Harga Satuan</th>
\t\t\t\t\t\t<th style=\"text-align:center\">Harga Total</th>
\t\t\t\t\t\t<th style=\"text-align:center\">Keterangan</th>
\t\t\t\t\t</tr>
\t\t\t\t\t</thead>
\t\t\t\t\t<tbody>
\t\t\t\t\t</tbody>
\t\t\t\t</table>
\t\t\t</div>
\t\t</div>
\t</div>
\t<div class=\"row\">
\t\t<div class=\"col-md-12\">
\t\t\t<label for=\"data\">Daftar Barang</label>
\t\t\t<table class=\"table table-detail-pakai-habis table-striped table-bordered\" id=\"data\">
\t\t\t\t<thead>
\t\t\t\t\t<tr>
\t\t\t\t\t\t<th class=\"text-center\">Kode</th>
\t\t\t\t\t\t<th class=\"text-center\">Nama Barang</th>
\t\t\t\t\t\t<th class=\"text-center\">Qty</th>
\t\t\t\t\t\t<th class=\"text-center\">Satuan</th>
\t\t\t\t\t\t<th class=\"text-center\">Harga Satuan</th>
\t\t\t\t\t\t<th class=\"text-center\">Harga Total</th>
\t\t\t\t\t\t<th class=\"text-center\">Keterangan</th>
\t\t\t\t\t</tr>
\t\t\t\t</thead>
\t\t\t\t<tbody id=\"data-body\">
\t\t\t\t\t<tr>
\t\t\t\t\t\t<td class=\"text-center\">11.554.15.025</td>
\t\t\t\t\t\t<td class=\"text-left\">Kuda Lumping</td>
\t\t\t\t\t\t<td class=\"text-right\">150</td>
\t\t\t\t\t\t<td class=\"text-left\">lembar</td>
\t\t\t\t\t\t<td class=\"text-right\">125000</td>
\t\t\t\t\t\t<td class=\"text-right\">1875000</td>
\t\t\t\t\t\t<td class=\"text-left\">Diisi</td>
\t\t\t\t\t</tr>
\t\t\t\t\t<tr>
\t\t\t\t\t\t<td class=\"text-center\">11.554.15.005</td>
\t\t\t\t\t\t<td class=\"text-left\">Kuda Rumbia</td>
\t\t\t\t\t\t<td class=\"text-right\">2254</td>
\t\t\t\t\t\t<td class=\"text-left\">lembar</td>
\t\t\t\t\t\t<td class=\"text-right\">10000</td>
\t\t\t\t\t\t<td class=\"text-right\">22540000</td>
\t\t\t\t\t\t<td class=\"text-left\"></td>
\t\t\t\t\t</tr>
\t\t\t\t\t<tr>
\t\t\t\t\t\t<td class=\"text-center\">11.554.15.015</td>
\t\t\t\t\t\t<td class=\"text-left\">Kuda</td>
\t\t\t\t\t\t<td class=\"text-right\">75</td>
\t\t\t\t\t\t<td class=\"text-left\">ekor</td>
\t\t\t\t\t\t<td class=\"text-right\">50000000</td>
\t\t\t\t\t\t<td class=\"text-right\">3750000000</td>
\t\t\t\t\t\t<td class=\"text-left\"></td>
\t\t\t\t\t</tr>
\t\t\t\t</tbody>
\t\t\t\t<tbody>
\t\t\t\t\t<tr>
\t\t\t\t\t\t<td colspan=\"7\">
\t\t\t\t\t\t\t<button type=\"button\" class=\"btn btn-primary pull-right\" data-toggle=\"modal\" data-target=\"#tambah-pengadaan\"><span class=\"glyphicon glyphicon-plus\"></span> Tambah Barang</button></td>
\t\t\t\t\t</tr>
\t\t\t\t</tbody>
\t\t\t</table>
\t\t</div>
\t</div>
\t<div class=\"row\">
\t\t<div class=\"col-md-12\">
\t\t\t<div class=\"btn-group pull-right\">
\t\t\t\t<button type=\"button\" class=\"btn btn-default\">Batal</span>
\t\t\t\t</button>
\t\t\t\t<button type=\"button\" class=\"btn btn-primary\" id=\"simpan-pengadaan\" name=\"simpan-pengadaan\">
\t\t\t\t\t<span class=\"glyphicon glyphicon-floppy-disk\"></span> Simpan</span>
\t\t\t\t</button>
\t\t\t</div>
\t\t</div>
\t</div>
</div>
";
    }

    // line 120
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 121
        echo "\t";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 122
        echo twig_escape_filter($this->env, base_url("assets/css/select2.css"), "html", null, true);
        echo "\" />
";
    }

    // line 125
    public function block_scripts($context, array $blocks = array())
    {
        // line 126
        echo "\t";
        $this->displayParentBlock("scripts", $context, $blocks);
        echo "
\t<script type=\"text/javaScript\" src=\"";
        // line 127
        echo twig_escape_filter($this->env, base_url("assets/js/select2.min.js"), "html", null, true);
        echo "\"></script>
\t<script type=\"text/javaScript\">
\t\tjQuery( document ).ready(function(\$) {
\t\t\t\$(\"select[name=nomor]\").select2({
\t\t\t\tajax: {
\t\t\t\t\turl: \"";
        // line 132
        echo twig_escape_filter($this->env, site_url("persediaan/get_list_pakai_habis"), "html", null, true);
        echo "\",
\t\t\t\t\ttype: \"POST\",
\t\t\t\t\tdataType: \"json\",
\t\t\t\t\tdata: function(params) {
\t\t\t\t\t\treturn {
\t\t\t\t\t\t\tterm: params.term
\t\t\t\t\t\t}
\t\t\t\t\t}
\t\t\t\t}
\t\t\t});

\t\t\t\$(\"select[name=nomor]\").on(\"select2:select\", function () { 
\t\t\t\tvar spk = \$(\"select[name=nomor]\").find('option:selected').data().data.nomor;
\t\t\t\t\$.post(\"";
        // line 145
        echo twig_escape_filter($this->env, site_url("persediaan/get_vendor"), "html", null, true);
        echo "\", { spk:spk }, function(json){ 
\t\t\t\t\tvar data = json.data;
\t\t\t\t\t
\t\t\t\t\t\$(\"input[name=vendor]\").val(data.INSTANSI_YG_MENYERAHKAN);
\t\t\t\t\t\$(\"input[name=estimasi]\").val(data.TGL_BA_PENERIMAAN);

\t\t\t\t}, \"json\");

\t\t\t\t\$('div[name=\"barang_pakai_habis\"]').show();
\t\t\t\t\$('.table_pakai_habis > tbody').empty();
\t\t\t\t\$.post(\"";
        // line 155
        echo twig_escape_filter($this->env, site_url("persediaan/get_list_barang_pakai_habis"), "html", null, true);
        echo "\", { spk:spk }, function(json){ 
\t\t\t\t\tvar data = json.data;
\t\t\t\t\tvar html = '';
\t\t\t\t\t
\t\t\t\t\t\$.each(data, function(key, value) {
\t\t\t\t\t\tvar value = data[key];
\t\t\t\t\t\thtml += '<tr><td>'
\t\t\t\t\t\t+ value.NO_KEY
\t\t\t\t\t\t+ '</td><td>'
\t\t\t\t\t\t+ value.NAMA_BARANG
\t\t\t\t\t\t+ '</td><td>'
\t\t\t\t\t\t+ value.MERK_ALAMAT
\t\t\t\t\t\t+ '</td><td>'
\t\t\t\t\t\t+ value.JUMLAH_BARANG
\t\t\t\t\t\t+ '</td><td>'
\t\t\t\t\t\t+ value.SATUAN
\t\t\t\t\t\t+ '</td><td>'
\t\t\t\t\t\t+ value.HARGA_SATUAN
\t\t\t\t\t\t+ '</td><td>'
\t\t\t\t\t\t+ value.HARGA_TOTAL_PLUS_PAJAK
\t\t\t\t\t\t+ '</td><td>'
\t\t\t\t\t\t+ value.KETERANGAN
\t\t\t\t\t\t+ '</td><td>'
    \t\t\t\t});

\t\t\t\t\t\$('.table_pakai_habis > tbody').append(html);
\t\t\t\t}, \"json\");
\t\t\t});
\t\t\t

\t\t\t/* UNTUK MODAL */

\t\t\tvar data = [];
\t\t\t
\t\t\tfunction tambahBarang() {
\t\t\t\tvar kodeBarang\t\t= \$(\"#kode-barang\").val();
\t\t\t\tvar namaBarang \t\t= \$(\"#nama-barang\").val();
\t\t\t\tvar merk \t\t \t= \$(\"#merk\").val();
\t\t\t\tvar jumlahBarang \t= parseInt(\$(\"#jumlah-barang\").val(),10);
\t\t\t\tvar satuan \t\t \t= \$(\"#satuan\").val();
\t\t\t\tvar hargaSatuan \t= parseInt(\$(\"#harga-satuan\").val(),10);
\t\t\t\tvar hargaTotal \t\t= parseInt(\$(\"#harga-total\").val(),10);
\t\t\t\tvar keterangan \t\t= \$(\"#keterangan-detail\").val();

\t\t\t\tconsole.log();

\t\t\t\tdata.push({
\t\t\t\t\t\"kodeBarang\"\t:kodeBarang,
\t\t\t\t\t\"namaBarang\"\t:namaBarang,
\t\t\t\t\t\"merk\"\t\t\t:merk,
\t\t\t\t\t\"jumlahBarang\"\t:jumlahBarang,
\t\t\t\t\t\"satuan\"\t\t:satuan,
\t\t\t\t\t\"hargaSatuan\"\t:hargaSatuan,
\t\t\t\t\t\"hargaTotal\"\t:hargaTotal,
\t\t\t\t\t\"keterangan\"\t:keterangan
\t\t\t\t});
\t\t\t\trepopulateDataTable();
\t\t\t}

\t\t\tfunction kurangiBarang(index){
\t\t\t\tdata.splice(index,1);
\t\t\t\trepopulateDataTable();
\t\t\t}

\t\t\tfunction resetAddDataModal(){
\t\t\t\t\$(\"#tambah-pengadaan-form\").trigger(\"reset\");
\t\t\t\t\$(\".alert\").remove();
\t\t\t}

\t\t\tfunction repopulateDataTable(){
\t\t\t\t\$(\"#data-body\").empty();
\t\t\t\t\$(\"#data-body\").append(\"<tr></tr>\");
\t\t\t\tdata.forEach(function(value, index){
\t\t\t\t\t\$(\"#data-body\").append(
\t\t\t\t\t\t\"<tr>\"+
\t\t\t\t\t\t\t\"<td class='text-left'>\"+value['kodeBarang'] +\"</td>\"+
\t\t\t\t\t\t\t\"<td class='text-left'>\"+value['namaBarang']+\"</td>\"+
\t\t\t\t\t\t\t\"<td class='text-left'>\"+value['merk']+\"</td>\"+
\t\t\t\t\t\t\t\"<td class='text-right'>\"+value['jumlahBarang']+\"</td>\"+
\t\t\t\t\t\t\t\"<td class='text-left'>\"+value['satuan']+\"</td>\"+
\t\t\t\t\t\t\t\"<td class='text-right'>\"+value['hargaSatuan']+\"</td>\"+
\t\t\t\t\t\t\t\"<td class='text-right'>\"+value['hargaTotal']+\"</td>\"+
\t\t\t\t\t\t\t\"<td class='text-left'>\"+value['keterangan']+\"</td>\"+
\t\t\t\t\t\t\t\"<td class='text-center'><a href='' id='data_\" + index + \"' value='\" + index + \"'><span class='glyphicon glyphicon-trash'></span></a></td>\"+
\t\t\t\t\t\t\"</tr>\"
\t\t\t\t\t);

\t\t\t\t\t\$('#data_'+index).on(\"click\", function(e){
\t\t\t\t\t\te.preventDefault();
\t\t\t\t\t\tkurangiBarang(index);
\t\t\t\t\t});
\t\t\t\t});
\t\t\t}

\t\t\t\$(\"button[name=simpan]\").on(\"click\", function() {
\t\t\t\tif(\$(\"#kode-barang\").val() === \"\" || \$(\"#nama-barang\").val() === \"\" || \$(\"#merk\").val() === \"\" || \$(\"#jumlah-barang\").val() === \"\" || \$(\"#satuan\").val() === \"\" || \$(\"#harga-satuan\").val() === \"\" || \$(\"#harga-total\").val() === \"\")
\t\t\t\t{
\t\t\t\t\t\$(\"#tambah-pengadaan-form\").prepend(\"<div class='alert alert-danger'>Isian dengan tanda (*) harus diisi.</div>\");
\t\t\t\t\t\$(\"#tambah-pengadaan\").scrollTop(0);
\t\t\t\t}
\t\t\t\telse
\t\t\t\t{
\t\t\t\t\t\$.post(\"";
        // line 257
        echo twig_escape_filter($this->env, site_url("persediaan/submit_pengadaan"), "html", null, true);
        echo "\", { 
\t\t\t\t\t\tspk:spk 
\t\t\t\t\t}, function(json){ 
\t\t\t\t\t\tvar data = json.data;
\t\t\t\t\t\t\$(\"#tambah-pengadaan\").modal(\"hide\");
\t\t\t\t\t\ttambahBarang();
\t\t\t\t\t\tresetAddDataModal();
\t\t\t\t\t}, \"json\");

\t\t\t\t}
\t\t\t});

\t\t\t\$(\"button[name=batal]\").on(\"click\", function() {
\t\t\t\tresetAddDataModal();
\t\t\t});

\t\t\t\$('#tambah-pengadaan').on('hide.bs.modal', function () {
\t\t\t\t\$(\"#tambah-pengadaan\").scrollTop(0);
\t\t\t});

\t\t\t\$(\"#simpan-pengadaan\").on(\"click\", function(){
\t\t\t\t// prepare data
\t\t\t\tvar jurnal;

\t\t\t\tvar nomorJurnal\t\t\t\t= \$(\"#juno\").val();
\t\t\t\tvar tanggalJurnal \t\t\t= \$(\"#bktgl\").val();
\t\t\t\t// var bukti \t\t\t\t\t= \$(\"#bkpenerimaan\").val();
\t\t\t\tvar tanggalEstimasi \t\t= \$(\"#mttgl\").val();
\t\t\t\tvar tanggalRealisasi \t\t= \$(\"#srtgl\").val();
\t\t\t\tvar bidang \t\t\t\t\t= \$(\"#bidang-select\").find(\"option:selected\").val();
\t\t\t\tvar penanggungJawab \t\t= \$(\"#penerima\").val();
\t\t\t\tvar dokumenPenyerta \t\t= \$(\"#bon\").val();
\t\t\t\tvar tanggalDokumenPenyerta \t= \$(\"#bptgl\").val();
\t\t\t\tvar dasarHukum \t\t\t\t= \$(\"#dsno\").val();
\t\t\t\tvar tanggalDasarHukum \t\t= \$(\"#dstgl\").val();
\t\t\t\tvar skpd\t\t\t\t\t= \"";
        // line 292
        echo twig_escape_filter($this->env, (isset($context["skpd"]) ? $context["skpd"] : null), "html", null, true);
        echo "\";
\t\t\t\tvar keterangan \t\t\t\t= \$(\"#ket\").val();

\t\t\t\tjurnal = {
\t\t\t\t\t\"nomorJurnal\"\t\t\t\t: nomorJurnal,
\t\t\t\t\t\"tanggalJurnal\" \t\t\t: tanggalJurnal,
\t\t\t\t\t\"tanggalEstimasi\" \t\t\t: tanggalEstimasi,
\t\t\t\t\t\"tanggalRealisasi\" \t\t\t: tanggalRealisasi,
\t\t\t\t\t\"bidang\" \t\t\t\t\t: bidang,
\t\t\t\t\t\"penanggungJawab\" \t\t\t: penanggungJawab,
\t\t\t\t\t\"dokumenPenyerta\"\t\t\t: dokumenPenyerta,
\t\t\t\t\t\"tanggalDokumenPenyerta\" \t: tanggalDokumenPenyerta,
\t\t\t\t\t\"dasarHukum\" \t\t\t\t: dasarHukum,
\t\t\t\t\t\"tanggalDasarHukum\" \t\t: tanggalDasarHukum,
\t\t\t\t\t\"skpd\" \t\t\t\t\t\t: skpd,
\t\t\t\t\t\"keterangan\" \t\t\t\t: keterangan,
\t\t\t\t};

\t\t\t\t\$.post(\"";
        // line 310
        echo twig_escape_filter($this->env, site_url("persediaan/simpan_barang_keluar"), "html", null, true);
        echo "\",{ju:jurnal, jud:data},function(json){
\t\t\t\t\tconsole.log(json);
\t\t\t\t\tjson.forEach(function(item, index){
\t\t\t\t\t\tdata[index][\"judid\"] = item;
\t\t\t\t\t});
\t\t\t\t},\"json\");
\t\t\t});
\t\t});
\t</script>

";
    }

    public function getTemplateName()
    {
        return "simgo/persediaan/pengadaan_barang.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  378 => 310,  357 => 292,  319 => 257,  214 => 155,  201 => 145,  185 => 132,  177 => 127,  172 => 126,  169 => 125,  163 => 122,  158 => 121,  155 => 120,  41 => 9,  35 => 5,  33 => 4,  30 => 3,);
    }
}
