<?php

/* simgo/daftar_spk_admin.html */
class __TwigTemplate_de6c9c65924ec79fe36f02858405db79 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("_templates/adminpage.html");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'scripts' => array($this, 'block_scripts'),
            'stylesheets' => array($this, 'block_stylesheets'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_templates/adminpage.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"container\">
\t<div class=\"col-xs-12\">
\t\t<div class=\"col-md-3 pull-left\">
\t\t\t<select type=\"text\" id=\"skpdid\" class=\"form-control\">
\t\t\t\t<option value=\"\">Semua SKPD</option>
\t\t\t</select>
\t\t</div>
\t\t<div class=\"col-md-3 pull-right\">
\t\t\t<input name=\"cari\" type=\"text\" class=\"form-control\" placeholder=\"Pencarian\" />
\t\t</div>
\t</div>
\t<div class=\"clearfix\"></div>
\t<div class=\"spacer\"></div>
\t<div class=\"row\">
\t\t<div class=\"col-xs-12\">
\t\t\t<div class=\"table-responsive\" style=\"overflow:auto;\">
\t\t\t\t<div>
\t\t\t\t\t<table class=\"table table-striped table-hover\">
\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<th></th>
\t\t\t\t\t\t\t\t<th></th>
\t\t\t\t\t\t\t\t<th></th>
\t\t\t\t\t\t\t\t<th></th>
\t\t\t\t\t\t\t\t<th class=\"center\">Nomor Lokasi</th>
\t\t\t\t\t\t\t\t<th class=\"center\">No SPK/SP/Kontrak</th>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t</thead>
\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t";
        // line 33
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["daftar"]) ? $context["daftar"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["dt"]) {
            // line 34
            echo "\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t<td></td>
\t\t\t\t\t\t\t\t<td width=\"15px;\" style=\"text-align:center;\">
\t\t\t\t\t\t\t\t\t<a posted jpid=\"";
            // line 37
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["dt"]) ? $context["dt"] : null), "spkid"), "html", null, true);
            echo "\" href=\"#\" title=\"Posting\">
\t\t\t\t\t\t\t\t\t\t<span class=\"glyphicon glyphicon-open\"></span>
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t<td width=\"15px;\" style=\"text-align:center;\">
\t\t\t\t\t\t\t\t\t<a target=\"_blank\" href=\"";
            // line 42
            echo twig_escape_filter($this->env, site_url(("pengadaan/form/" . $this->getAttribute((isset($context["dt"]) ? $context["dt"] : null), "spkid"))), "html", null, true);
            echo "\" title=\"Edit\">
\t\t\t\t\t\t\t\t\t\t<span class=\"glyphicon glyphicon-pencil\"></span>
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t<td width=\"15px;\" style=\"text-align:center;\">
\t\t\t\t\t\t\t\t\t<a onclick='return  confirm(\"Anda yakin ingin menghapus data ini ?\")' href=\"";
            // line 47
            echo twig_escape_filter($this->env, site_url(("pengadaan/hapus/" . $this->getAttribute((isset($context["dt"]) ? $context["dt"] : null), "spkid"))), "html", null, true);
            echo "\" title=\"Hapus\">
\t\t\t\t\t\t\t\t\t\t<span class=\"glyphicon glyphicon-trash\"></span>
\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t<td>";
            // line 51
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["dt"]) ? $context["dt"] : null), "skpdid"), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t\t<td>";
            // line 52
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["dt"]) ? $context["dt"] : null), "mpno"), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['dt'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 55
        echo "\t\t\t\t\t\t</tbody>
\t\t\t\t\t</table>
\t\t\t\t</div>
\t\t\t</div>

\t\t\t<div id=\"pagination\">
\t\t\t\t";
        // line 61
        echo (isset($context["pagination"]) ? $context["pagination"] : null);
        echo "
\t\t\t</div>
</div>
";
    }

    // line 67
    public function block_scripts($context, array $blocks = array())
    {
        // line 68
        $this->displayParentBlock("scripts", $context, $blocks);
        echo "
<script type=\"text/javaScript\" src=\"";
        // line 69
        echo twig_escape_filter($this->env, base_url("assets/js/accounting.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 70
        echo twig_escape_filter($this->env, base_url("assets/js/base64.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\">
jQuery(document).ready(function(\$) {
\tif (!String.prototype.format) {
\t\tString.prototype.format = function() {
\t\t\tvar args = arguments;
\t\t\treturn this.replace(/{(\\d+)}/g, function(match, number) { 
\t\t\t\treturn typeof args[number] != 'undefined' ? args[number] : match;
\t\t\t});
\t\t};
\t}

\tvar delay = (function(){
\t\tvar timer = 0;
\t\t\treturn function(callback, ms){
\t\t\tclearTimeout (timer);
\t\t\ttimer = setTimeout(callback, ms);
\t\t};
\t})();


\t// populasi selectbox skpd
\t\$.post(\"";
        // line 92
        echo twig_escape_filter($this->env, site_url("aset/list_sub_skpd"), "html", null, true);
        echo "\", {}, function(json){\t\t\t\t
\t\t\$.each(json.result, function (key, value) {
\t\t\t\$(\"#skpdid\").append(\$('<option></option>').val(value.NOMOR_SUB_UNIT).html(value.NAMA_SUB_UNIT));
\t\t\t\$(\"#skpdid\").append(\$('<option></option>').val(value.NOMOR_SUB_UNIT).html(value.NAMA_SUB_UNIT));
\t\t});
\t}, \"json\");

\tfunction cari_data() {
\t\tvar filter = \$(\"input[name=cari]\").val();
\t\t//console.log(filter);
\t\tvar skpdid = \$(\"select[id=skpdid]\").val();

\t\tvar x = \$.ajax({
\t\t\turl: \"";
        // line 105
        echo twig_escape_filter($this->env, site_url("media/pencarian"), "html", null, true);
        echo "?filter={0}&skpdid={1}\".format(filter, skpdid),
\t\t\ttype: \"POST\",
\t\t\tdataType: \"json\"
\t\t});

\t\tx.done(function(json) {
\t\t\tvar tbody = \$(\"table > tbody\");
\t\t\ttbody.empty();
\t\t\t
\t\t\tvar data = json.data;
\t\t\tfor(var i=0; i<data.length; i++) {
\t\t\t\tvar t = data[i];
\t\t\t\t
\t\t\t\tvar c = [
\t\t\t\t\t'<tr>',
\t\t\t\t\t\t'<td></td>',
\t\t\t\t\t\t'<td width=\"15px;\" style=\"text-align:center;\">',
\t\t\t\t\t\t\t'<a posted jpid=\"'+ t.spkid +'\" href=\"#\" title=\"Posting\">',
\t\t\t\t\t\t\t\t'<span class=\"glyphicon glyphicon-open\"></span>',
\t\t\t\t\t\t\t'</a>',
\t\t\t\t\t\t'</td>',
\t\t\t\t\t\t'<td width=\"15px;\" style=\"text-align:center;\">',
\t\t\t\t\t\t\t'<a target=\"_blank\" href=\"";
        // line 127
        echo twig_escape_filter($this->env, site_url("pengadaan/form"), "html", null, true);
        echo "/' + t.spkid + '\" title=\"Edit\">',
\t\t\t\t\t\t\t\t'<span class=\"glyphicon glyphicon-pencil\"></span>',
\t\t\t\t\t\t\t'</a>',
\t\t\t\t\t\t'</td>',
\t\t\t\t\t\t'<td width=\"15px;\" style=\"text-align:center;\">',
\t\t\t\t\t\t\t'<a onclick=\"return confirm(\\'Anda yakin ingin menghapus data ini ?\\')\" href=\"";
        // line 132
        echo twig_escape_filter($this->env, site_url("pengadaan/hapus/"), "html", null, true);
        echo "/' + t.spkid + '\" title=\"Hapus\">',
\t\t\t\t\t\t\t\t'<span class=\"glyphicon glyphicon-trash\"></span>',
\t\t\t\t\t\t\t'</a>',
\t\t\t\t\t\t'</td>',
\t\t\t\t\t\t'<td>' + t.skpdid + '</td>',
\t\t\t\t\t\t'<td>' + t.mpno + '</td>',
\t\t\t\t\t'</tr>'
\t\t\t\t].join(\"\\r\\n\");

\t\t\t\ttbody.append(c);
\t\t\t}
\t\t\t
\t\t\t\$(\"div#pagination\").html(json.pagination);

\t\t});

\t\tx.fail(function() {
\t\t\treturn;
\t\t\t//window.location.replace(\"";
        // line 150
        echo twig_escape_filter($this->env, site_url("pengadaan/index"), "html", null, true);
        echo "/?filter={0}\".format(filter));
\t\t});
\t}


\t\$(\"input[name=cari]\").keyup(function(e) {
\t\t delay(function(){
\t\t \tcari_data();
\t    }, 500);
\t});


\t\$(\"select[id=skpdid]\").change(function() {
\t\tcari_data();
\t});

\t//posting ke simbada
\t\$(\"a[posted]\").on(\"click\", function() {
\t\tvar jpid = \$(this).attr(\"jpid\");

\t\t\$.post(\"";
        // line 170
        echo twig_escape_filter($this->env, site_url("pengadaan/post"), "html", null, true);
        echo "/\" + jpid, {}, function() {
\t\t\talert (\"Data berhasil diposting ke Simbada\")
\t\t}, \"json\");
\t});
});
</script>
";
    }

    // line 179
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 180
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "

<style type=\"text/css\">
\t.control-label{
\t\tpadding-top: 8px !important;
\t}

\t*[class^='col-'] {
\t\tpadding-left: 0px;
\t\tpadding-right: 0px;
\t}

\tselect.form-control {
\t\theight: 48px !important;
\t}

\tinput.form-control {
\t\theight: 34px !important;
\t}

\ttextarea.form-control {
\t\theight:auto !important;
\t}

\t.spacer {
\t    margin-top: 20px;
\t}
</style>
";
    }

    public function getTemplateName()
    {
        return "simgo/daftar_spk_admin.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  268 => 180,  265 => 179,  254 => 170,  231 => 150,  210 => 132,  202 => 127,  177 => 105,  161 => 92,  136 => 70,  132 => 69,  128 => 68,  125 => 67,  117 => 61,  109 => 55,  100 => 52,  96 => 51,  89 => 47,  81 => 42,  73 => 37,  68 => 34,  64 => 33,  33 => 4,  30 => 3,);
    }
}
