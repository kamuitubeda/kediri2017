<?php

/* simgo/unggah_spk.html */
class __TwigTemplate_a06354678cd1b6758a940bc56337c768 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("_templates/adminpage.html");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_templates/adminpage.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"container\">
\t<div class=\"row\">
\t\t<div class=\"col-md-12\">
\t\t\t";
        // line 7
        echo (isset($context["error"]) ? $context["error"] : null);
        echo "
\t\t\t<h2>";
        // line 8
        if (((isset($context["kategori"]) ? $context["kategori"] : null) == "SPK")) {
            echo "No. SPK";
        } elseif (((isset($context["kategori"]) ? $context["kategori"] : null) == "Kegiatan")) {
            echo "Kode Rekening Kegiatan";
        }
        echo " : <a href=\"";
        echo twig_escape_filter($this->env, site_url(("media/view_spk/" . (isset($context["mpid"]) ? $context["mpid"] : null)), (isset($context["mpno"]) ? $context["mpno"] : null)), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, (isset($context["mpno"]) ? $context["mpno"] : null), "html", null, true);
        echo "</a></h2>

\t\t\t<form action=\"";
        // line 10
        echo twig_escape_filter($this->env, site_url(("media/do_upload/" . (isset($context["mpid"]) ? $context["mpid"] : null))), "html", null, true);
        echo "\" method=\"post\" accept-charset=\"utf-8\" enctype=\"multipart/form-data\">
\t\t\t\t<input class=\"form-control\" name=\"spkid\" type=\"hidden\" value= ";
        // line 11
        echo twig_escape_filter($this->env, (isset($context["mpid"]) ? $context["mpid"] : null), "html", null, true);
        echo " />
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<label for=\"doctype\">Tipe dokumen:</label>
\t\t\t\t\t<select class=\"form-control\" id=\"doctype\" name=\"kategori\">
\t\t\t\t\t\t";
        // line 15
        if (((isset($context["kategori"]) ? $context["kategori"] : null) == "SPK")) {
            // line 16
            echo "\t\t\t\t\t\t\t<option ";
            if (((isset($context["dokumen"]) ? $context["dokumen"] : null) == "1")) {
                echo " selected ";
            }
            echo " >SPM</option>
\t\t\t\t\t\t\t<option ";
            // line 17
            if (((isset($context["dokumen"]) ? $context["dokumen"] : null) == "2")) {
                echo " selected ";
            }
            echo " >SP2D</option>
\t\t\t\t\t\t";
        } elseif (((isset($context["kategori"]) ? $context["kategori"] : null) == "Kegiatan")) {
            // line 19
            echo "\t\t\t\t\t\t\t<option ";
            if (((isset($context["dokumen"]) ? $context["dokumen"] : null) == "3")) {
                echo " selected ";
            }
            echo " >RKA</option>
\t\t\t\t\t\t\t<option ";
            // line 20
            if (((isset($context["dokumen"]) ? $context["dokumen"] : null) == "4")) {
                echo " selected ";
            }
            echo " >DPA</option>
\t\t\t\t\t\t";
        }
        // line 22
        echo "\t\t\t\t\t</select>
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<label>Pilih berkas:</label>
\t\t\t\t\t\t\t<input class=\"form-control\" type=\"file\" name=\"userfile\">
\t\t\t\t</div>
\t\t\t\t<button type=\"submit\" class=\"btn btn-default\">Unggah</button>
\t\t\t</form>
\t\t</div>
\t</div>
</div>
";
    }

    public function getTemplateName()
    {
        return "simgo/unggah_spk.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 22,  87 => 20,  80 => 19,  73 => 17,  66 => 16,  64 => 15,  57 => 11,  53 => 10,  40 => 8,  36 => 7,  31 => 4,  28 => 3,);
    }
}
