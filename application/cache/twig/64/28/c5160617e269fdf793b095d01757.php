<?php

/* _templates/smart_base.html */
class __TwigTemplate_6428c5160617e269fdf793b095d01757 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("bootstrap_base.html");

        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'content' => array($this, 'block_content'),
            'scripts' => array($this, 'block_scripts'),
            'stylesheets' => array($this, 'block_stylesheets'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "bootstrap_base.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_body($context, array $blocks = array())
    {
        // line 5
        echo "<div class=\"container-fluid\" style=\"width: 1366px;\">
\t<div class=\"row\">
\t\t<div class=\"col-xs-12\" topbar>
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-xs-6\" logo>
\t\t\t\t\t<i class=\"icon-cogs icon-2x\"></i>
\t\t\t\t\t<h3 style=\"display:inline; padding-left:5px;\">
\t\t\t\t\t\t<b>SIMBADA GO!</b>
\t\t\t\t\t</h3>
\t\t\t\t</div>
\t\t\t\t<div class=\"pull-right col-xs-1\" profil>
\t\t\t\t\t<ul>
\t\t\t\t\t\t<li>
\t\t\t\t\t\t\t<a data-toggle=\"dropdown\" href=\"#\">
\t\t\t\t\t\t\t\t<i class=\"icon-gear icon-2x\" ></i>
\t\t\t\t\t\t\t\t<span></span>
\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t<ul class=\"dropdown-menu\" style=\"margin-left:-100px; top:94%\">
\t\t\t\t\t\t\t\t<li><a href=\"http://localhost/pantau/secman/user/profile\">Profil</a></li>
\t\t\t\t\t\t\t\t<li class=\"divider\"></li>
\t\t\t\t\t\t\t\t<li><a href=\"http://localhost/pantau/secman/authentication/logout\">Logout</a></li>
\t\t\t\t\t\t\t</ul>
\t\t\t\t\t\t</li>
\t\t\t\t\t</ul>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
\t<div class=\"row\">
\t\t<div sidebar class=\"col-xs-2\">
\t\t\t<div class=\"row\" menu style=\"background:black;\">
\t\t\t\t<i class=\"icon-user icon-2x\"></i>
\t\t\t\t<span style=\"padding-left:10px; position: absolute; top:16px; font-weight: bold\">";
        // line 37
        echo twig_escape_filter($this->env, (isset($context["uname"]) ? $context["uname"] : null), "html", null, true);
        echo "<br/>";
        echo twig_escape_filter($this->env, (isset($context["oname"]) ? $context["oname"] : null), "html", null, true);
        echo "</span>
\t\t\t</div>
\t\t\t<div class=\"row menu\" menu>
\t\t\t\t<a href=\"";
        // line 40
        echo twig_escape_filter($this->env, site_url("aset/index"), "html", null, true);
        echo "\"
\t\t\t\t\t<i class=\"icon-map-marker\"></i>
\t\t\t\t\t<span style=\"\">Peta Aset</span>
\t\t\t\t</a>
\t\t\t</div>
\t\t\t<div class=\"row menu\" menu>
\t\t\t\t<a href=\"";
        // line 46
        echo twig_escape_filter($this->env, site_url("aset/kantor"), "html", null, true);
        echo "\"
\t\t\t\t\t<i class=\"icon-home\"></i>
\t\t\t\t\t<span style=\"\">Master Kantor</span>
\t\t\t\t</a>
\t\t\t</div>
\t\t\t<div class=\"row menu\" menu>
\t\t\t\t<a href=\"";
        // line 52
        echo twig_escape_filter($this->env, site_url("aset/daftar_pengadaan"), "html", null, true);
        echo "\"
\t\t\t\t\t<i class=\"icon-home\"></i>
\t\t\t\t\t<span style=\"\">Daftar Pengadaan</span>
\t\t\t\t</a>
\t\t\t</div>
\t\t\t<!-- <div class=\"row menu\" menu>
\t\t\t\t<a href=\"";
        // line 58
        echo twig_escape_filter($this->env, site_url("aset/penerimaan"), "html", null, true);
        echo "\"
\t\t\t\t\t<i class=\"icon-home\"></i>
\t\t\t\t\t<span style=\"\">Penerimaan</span>
\t\t\t\t</a>
\t\t\t</div> -->
\t\t\t<div class=\"row menu\" menu>
\t\t\t\t<a href=\"";
        // line 64
        echo twig_escape_filter($this->env, site_url("aset/kamus_spk"), "html", null, true);
        echo "\"
\t\t\t\t\t<i class=\"icon-home\"></i>
\t\t\t\t\t<span style=\"\">Kamus SPK</span>
\t\t\t\t</a>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"col-xs-10\">
\t\t\t";
        // line 71
        $this->displayBlock('content', $context, $blocks);
        // line 72
        echo "\t\t</div>
\t</div>
</div>
";
    }

    // line 71
    public function block_content($context, array $blocks = array())
    {
    }

    // line 78
    public function block_scripts($context, array $blocks = array())
    {
        // line 79
        echo "\t";
        $this->displayParentBlock("scripts", $context, $blocks);
        echo "

\t<script type=\"text/javascript\" src=\"";
        // line 81
        echo twig_escape_filter($this->env, base_url("assets/js/smart.js"), "html", null, true);
        echo "\"></script>
";
    }

    // line 85
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 86
        echo "\t";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "

\t<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 88
        echo twig_escape_filter($this->env, base_url("assets/css/smart.css"), "html", null, true);
        echo "\">
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 89
        echo twig_escape_filter($this->env, base_url("assets/css/font-awesome.min.css"), "html", null, true);
        echo "\">
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 90
        echo twig_escape_filter($this->env, base_url(("assets/css/" . "icomoon.css")), "html", null, true);
        echo "\">
";
    }

    public function getTemplateName()
    {
        return "_templates/smart_base.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  168 => 90,  164 => 89,  160 => 88,  154 => 86,  151 => 85,  145 => 81,  139 => 79,  136 => 78,  131 => 71,  124 => 72,  122 => 71,  112 => 64,  103 => 58,  94 => 52,  85 => 46,  76 => 40,  68 => 37,  34 => 5,  31 => 4,);
    }
}
