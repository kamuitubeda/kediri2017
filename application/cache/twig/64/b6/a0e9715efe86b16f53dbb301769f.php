<?php

/* aset/master_spk.html */
class __TwigTemplate_64b6a0e9715efe86b16f53dbb301769f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("_templates/adminpage.html");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'scripts' => array($this, 'block_scripts'),
            'stylesheets' => array($this, 'block_stylesheets'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_templates/adminpage.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "<div style=\"margin-top: 10px; margin-left:10px; margin-right:10px; background:white; border: 1px solid rgba(1, 1, 1, 0.25);\">
\t<div class=\"row\">
\t\t<form class=\"form-horizontal\" role=\"form\">
\t\t\t<div class=\"col-sm-6\">
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<label class=\"col-sm-4 control-label\"></label>
\t\t\t\t\t<label class=\"col-sm-4 control-label\"></label>
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<label class=\"col-sm-4 control-label\">Nama SKPD</label>
\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t<select type=\"text\" name=\"nama-skpd\" id=\"nama-skpd\" class=\"form-control\">
\t\t\t\t\t\t\t<option value=\"\">Pilih SKPD</option>
\t\t\t\t\t\t</select>
\t\t\t\t\t</div>
\t\t\t\t</div>\t
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<label class=\"col-sm-4 control-label\">Nama Kegiatan</label>
\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t<input class=\"form-control\" id=\"nama-kegiatan\">
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<label class=\"col-sm-4 control-label\">SPK/SP/Kontrak: </label>
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<label class=\"col-sm-4 control-label\">Nomor</label>
\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t<input class=\"form-control\" id=\"nomor-spk\">
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<label class=\"col-sm-4 control-label\">Tanggal</label>
\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t<input class=\"form-control\" id=\"tanggal-spk\">
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<label class=\"col-sm-4 control-label\">Uraian</label>
\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t<input class=\"form-control\" id=\"uraian-spk\">
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<label class=\"col-sm-4 control-label\">Nilai</label>
\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t<input class=\"form-control\" id=\"nilais-spk\">
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<label class=\"col-sm-4 control-label\">Penyedia Barang: </label>
\t\t\t\t</div>\t
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<label class=\"col-sm-4 control-label\">Nama</label>
\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t<input class=\"form-control\" id=\"nama-penyedia-barang\">
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<label class=\"col-sm-4 control-label\">Alamat</label>
\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t<input class=\"form-control\" id=\"alamat-penyedia-barang\">
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<label class=\"col-sm-4 control-label\"></label>
\t\t\t\t\t<label class=\"col-sm-4 control-label\"></label>
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<div class=\"col-sm-4\">
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-sm-3\">
\t\t\t\t\t\t<button type=\"submit\" class=\"btn btn-default btn-block\">Tambah</button>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-sm-3\">
\t\t\t\t\t\t<button type=\"submit\" class=\"btn btn-default btn-block\">Batal</button>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div><!-- 
\t\t\t<div class=\"col-sm-6\">
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<label class=\"col-sm-6 control-label\">Rincian Barang: </label>
\t\t\t\t</div>\t
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<label class=\"col-sm-6 control-label\">Kode Rekening Belanja</label>
\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t<input class=\"form-control\" id=\"kode-rekening-belanja-6\">
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<label class=\"col-sm-6 control-label\">Uraian</label>
\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t<input class=\"form-control\" id=\"uraian-rekening-belanja-6\">
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<label class=\"col-sm-6 control-label\">Kode Rekening Belanja</label>
\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t<input class=\"form-control\" id=\"kode-rekening-belanja-5\">
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<label class=\"col-sm-6 control-label\">Uraian</label>
\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t<input class=\"form-control\" id=\"uraian-rekening-belanja-5\">
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<label class=\"col-sm-6 control-label\">Nama Barang</label>
\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t<input class=\"form-control\" id=\"nama-barang\">
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<label class=\"col-sm-6 control-label\">Kode Barang (Permendagri 17)</label>
\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t<input class=\"form-control\" id=\"kode-barang-17-2\">
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<label class=\"col-sm-6 control-label\">Uraian</label>
\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t<input class=\"form-control\" id=\"uraian-kode-barang-17-2\">
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<label class=\"col-sm-6 control-label\">Kode Barang (Permendagri 17)</label>
\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t<input class=\"form-control\" id=\"kode-barang-17\">
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<label class=\"col-sm-6 control-label\">Uraian</label>
\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t<input class=\"form-control\" id=\"uraian-kode-barang-17\">
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<label class=\"col-sm-6 control-label\">Volume</label>
\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t<input class=\"form-control\" id=\"volume-barang\">
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<label class=\"col-sm-6 control-label\">Satuan</label>
\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t<input class=\"form-control\" id=\"satuan-barang\">
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<label class=\"col-sm-6 control-label\">Harga Satuan</label>
\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t<input class=\"form-control\" id=\"harga-satuan-barang\">
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<label class=\"col-sm-6 control-label\">Jumlah Harga</label>
\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t<input class=\"form-control\" id=\"jumlah-harga-barang\">
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<label class=\"col-sm-6 control-label\">Detail Spesifikasi: </label>
\t\t\t\t</div>\t
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<label class=\"col-sm-6 control-label\">Merk</label>
\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t<input class=\"form-control\" id=\"merk-barang\">
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<label class=\"col-sm-6 control-label\">Tipe</label>
\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t<input class=\"form-control\" id=\"tipe-barang\">
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<label class=\"col-sm-6 control-label\">Ukuran</label>
\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t<input class=\"form-control\" id=\"ukuran-barang\">
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<label class=\"col-sm-6 control-label\">Bahan</label>
\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t<input class=\"form-control\" id=\"bahan-barang\">
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t<label class=\"col-sm-6 control-label\">Warna</label>
\t\t\t\t\t<div class=\"col-sm-6\">
\t\t\t\t\t\t<input class=\"form-control\" id=\"warna-barang\">
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div> -->
\t\t</form>\t\t
\t</div>
</div>
";
    }

    // line 204
    public function block_scripts($context, array $blocks = array())
    {
        // line 205
        echo "\t";
        $this->displayParentBlock("scripts", $context, $blocks);
        echo "
\t
\t<script type=\"text/javaScript\" src=\"http://code.jquery.com/ui/1.11.2/jquery-ui.js\"></script>
\t<script type=\"text/javaScript\" src=\"";
        // line 208
        echo twig_escape_filter($this->env, base_url("assets/js/jquery.loadmask.min.js"), "html", null, true);
        echo "\"></script>
\t<script type=\"text/javaScript\" src=\"";
        // line 209
        echo twig_escape_filter($this->env, base_url("assets/js/accounting.min.js"), "html", null, true);
        echo "\"></script>
\t<script type=\"text/javaScript\" src=\"";
        // line 210
        echo twig_escape_filter($this->env, base_url("assets/js/bootstrap-switch.min.js"), "html", null, true);
        echo "\"></script>
\t<script type=\"text/javaScript\">
\t\tjQuery( document ).ready(function(\$) {
\t\t\t//menampilkan list SKPD
\t\t  \tvar url = \"";
        // line 214
        echo twig_escape_filter($this->env, site_url("aset/list_sub_skpd"), "html", null, true);
        echo "\";
\t\t\t\$.get(url, {}, function(json){ 
\t\t\t\tvar listSkpd = json.result;
\t\t\t\t
\t\t\t\t\$.each(listSkpd, function (key, value) {
\t\t\t\t\t\$(\"#nama-skpd\").append(\$('<option></option>').val(value.NOMOR_SUB_UNIT).html(value.NAMA_SUB_UNIT));
\t\t\t\t});
\t\t\t}, \"json\");
\t\t});
\t</script>
";
    }

    // line 226
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 227
        echo "\t";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "

\t<link href=\"";
        // line 229
        echo twig_escape_filter($this->env, base_url("assets/css/bootstrap-switch.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
\t<link href=\"http://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css\" rel=\"stylesheet\">
\t<link href=\"";
        // line 231
        echo twig_escape_filter($this->env, base_url("assets/css/jquery.loadmask.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
\t

\t<style type=\"text/css\">
\t\t.control-label{
\t\t\tpadding-top: 8px !important;
\t\t}
\t</style>

";
    }

    public function getTemplateName()
    {
        return "aset/master_spk.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  289 => 231,  284 => 229,  278 => 227,  275 => 226,  260 => 214,  253 => 210,  249 => 209,  245 => 208,  238 => 205,  235 => 204,  33 => 4,  30 => 3,);
    }
}
