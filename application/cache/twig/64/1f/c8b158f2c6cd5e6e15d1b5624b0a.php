<?php

/* pengadaan/daftar.html */
class __TwigTemplate_641fc8b158f2c6cd5e6e15d1b5624b0a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("_templates/adminpage.html");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'scripts' => array($this, 'block_scripts'),
            'stylesheets' => array($this, 'block_stylesheets'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_templates/adminpage.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"container\">
\t<ul id=\"data\" class=\"nav nav-tabs\">
\t\t<li class=\"active\"><a href=\"#draft\" data-toggle=\"tab\">Draft</a></li>
\t\t<li><a href=\"#posting\" data-toggle=\"tab\">Posting</a></li>
\t</ul>
\t<div class=\"tab-content\">
\t\t<div class=\"spacer\"></div>
\t\t<div class=\"tab-pane fade in active\" id=\"draft\">
\t\t\t<div class=\"col-xs-12\">
\t\t\t\t<div class=\"col-md-3 pull-left\">
\t\t\t\t\t<select type=\"text\" id=\"nomor_lokasi\" class=\"form-control\">
\t\t\t\t\t\t<option value=\"\">Semua SKPD</option>
\t\t\t\t\t</select>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-md-3 pull-right\">
\t\t\t\t\t<input name=\"cari\" type=\"text\" class=\"form-control\" placeholder=\"Pencarian\" />
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"clearfix\"></div>
\t\t\t<div class=\"spacer\"></div>
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-xs-12\">
\t\t\t\t\t<div class=\"table-responsive\" style=\"overflow:auto;\">
\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t<table class=\"table table-striped table-hover\">
\t\t\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<th></th>
\t\t\t\t\t\t\t\t\t\t<th></th>
\t\t\t\t\t\t\t\t\t\t<th></th>
\t\t\t\t\t\t\t\t\t\t<th></th>
\t\t\t\t\t\t\t\t\t\t<th class=\"center\">Lokasi</th>
\t\t\t\t\t\t\t\t\t\t<th class=\"center\">Tanggal Jurnal</th>
\t\t\t\t\t\t\t\t\t\t<th class=\"center\">No SPK/SP/Kontrak</th>
\t\t\t\t\t\t\t\t\t\t<th class=\"center\">Kode Kegiatan</th>
\t\t\t\t\t\t\t\t\t\t<th>No.BA Penerimaan</th>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t\t";
        // line 43
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["daftar"]) ? $context["daftar"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["dt"]) {
            // line 44
            echo "\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td></td>
\t\t\t\t\t\t\t\t\t\t<td width=\"15px;\" style=\"text-align:center;\">
\t\t\t\t\t\t\t\t\t\t\t<a posted jpid=\"";
            // line 47
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["dt"]) ? $context["dt"] : null), "jpid"), "html", null, true);
            echo "\" href=\"#\" title=\"Posting\">
\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"glyphicon glyphicon-open\"></span>
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t<td width=\"15px;\" style=\"text-align:center;\">
\t\t\t\t\t\t\t\t\t\t\t<a target=\"_blank\" href=\"";
            // line 52
            echo twig_escape_filter($this->env, site_url(("pengadaan/form/" . $this->getAttribute((isset($context["dt"]) ? $context["dt"] : null), "jpid"))), "html", null, true);
            echo "\" title=\"Edit\">
\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"glyphicon glyphicon-pencil\"></span>
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t<td width=\"15px;\" style=\"text-align:center;\">
\t\t\t\t\t\t\t\t\t\t\t<a onclick='return  confirm(\"Anda yakin ingin menghapus data ini ?\")' href=\"";
            // line 57
            echo twig_escape_filter($this->env, site_url(("pengadaan/hapus/" . $this->getAttribute((isset($context["dt"]) ? $context["dt"] : null), "jpid"))), "html", null, true);
            echo "\" title=\"Hapus\">
\t\t\t\t\t\t\t\t\t\t\t\t<span class=\"glyphicon glyphicon-trash\"></span>
\t\t\t\t\t\t\t\t\t\t\t</a>
\t\t\t\t\t\t\t\t\t\t</td>
\t\t\t\t\t\t\t\t\t\t<td>";
            // line 61
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["dt"]) ? $context["dt"] : null), "jpnomorlokasi"), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t\t\t\t<td>";
            // line 62
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["dt"]) ? $context["dt"] : null), "jptgl"), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t\t\t\t<td>";
            // line 63
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["dt"]) ? $context["dt"] : null), "id_kontrak"), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t\t\t\t<td>";
            // line 64
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["dt"]) ? $context["dt"] : null), "jpkodekegiatan"), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t\t\t\t<td>";
            // line 65
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["dt"]) ? $context["dt"] : null), "jpnobaterima"), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['dt'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 68
        echo "\t\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t\t<div id=\"pagination\">
\t\t\t\t\t\t";
        // line 74
        echo (isset($context["pagination"]) ? $context["pagination"] : null);
        echo "
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"tab-pane fade in\" id=\"posting\">
\t\t\t<div class=\"col-xs-12\">
\t\t\t\t<div class=\"col-md-3 pull-left\">
\t\t\t\t\t<select type=\"text\" id=\"nomor_lokasi2\" class=\"form-control\">
\t\t\t\t\t\t<option value=\"\">Semua SKPD</option>
\t\t\t\t\t</select>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-md-3 pull-right\">
\t\t\t\t\t<input name=\"cari2\" type=\"text\" class=\"form-control\" placeholder=\"Pencarian\" />
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"clearfix\"></div>
\t\t\t<div class=\"spacer\"></div>
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-xs-12\">
\t\t\t\t\t<div class=\"table-responsive\" style=\"overflow:auto;\">
\t\t\t\t\t\t<div>
\t\t\t\t\t\t\t<table class=\"table table-striped table-hover\">
\t\t\t\t\t\t\t\t<thead>
\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<th class=\"center\">Lokasi</th>
\t\t\t\t\t\t\t\t\t\t<th class=\"center\">Tanggal Jurnal</th>
\t\t\t\t\t\t\t\t\t\t<th class=\"center\">No SPK/SP/Kontrak</th>
\t\t\t\t\t\t\t\t\t\t<th class=\"center\">Kode Kegiatan</th>
\t\t\t\t\t\t\t\t\t\t<th>No.BA Penerimaan</th>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t\t";
        // line 107
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["daftar_posted"]) ? $context["daftar_posted"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["dt"]) {
            // line 108
            echo "\t\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t\t<td>";
            // line 109
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["dt"]) ? $context["dt"] : null), "jpnomorlokasi"), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t\t\t\t<td>";
            // line 110
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["dt"]) ? $context["dt"] : null), "jptgl"), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t\t\t\t<td>";
            // line 111
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["dt"]) ? $context["dt"] : null), "id_kontrak"), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t\t\t\t<td>";
            // line 112
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["dt"]) ? $context["dt"] : null), "jpkodekegiatan"), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t\t\t\t<td>";
            // line 113
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["dt"]) ? $context["dt"] : null), "jpnobaterima"), "html", null, true);
            echo "</td>
\t\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['dt'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 116
        echo "\t\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t\t</table>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>

\t\t\t\t\t<div id=\"pagination\">
\t\t\t\t\t\t";
        // line 122
        echo (isset($context["pagination"]) ? $context["pagination"] : null);
        echo "
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
</div>
";
    }

    // line 132
    public function block_scripts($context, array $blocks = array())
    {
        // line 133
        $this->displayParentBlock("scripts", $context, $blocks);
        echo "
<script type=\"text/javaScript\" src=\"";
        // line 134
        echo twig_escape_filter($this->env, base_url("assets/js/accounting.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 135
        echo twig_escape_filter($this->env, base_url("assets/js/base64.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\">
jQuery(document).ready(function(\$) {
\tif (!String.prototype.format) {
\t\tString.prototype.format = function() {
\t\t\tvar args = arguments;
\t\t\treturn this.replace(/{(\\d+)}/g, function(match, number) { 
\t\t\t\treturn typeof args[number] != 'undefined' ? args[number] : match;
\t\t\t});
\t\t};
\t}

\tvar delay = (function(){
\t\tvar timer = 0;
\t\t\treturn function(callback, ms){
\t\t\tclearTimeout (timer);
\t\t\ttimer = setTimeout(callback, ms);
\t\t};
\t})();


\t// populasi selectbox skpd
\t\$.post(\"";
        // line 157
        echo twig_escape_filter($this->env, site_url("aset/list_sub_skpd"), "html", null, true);
        echo "\", {}, function(json){\t\t\t\t
\t\t\$.each(json.result, function (key, value) {
\t\t\t\$(\"#nomor_lokasi\").append(\$('<option></option>').val(value.NOMOR_SUB_UNIT).html(value.NAMA_SUB_UNIT));
\t\t\t\$(\"#nomor_lokasi2\").append(\$('<option></option>').val(value.NOMOR_SUB_UNIT).html(value.NAMA_SUB_UNIT));
\t\t});
\t}, \"json\");

\tfunction cari_data() {
\t\tvar filter = \$(\"input[name=cari]\").val();
\t\t//console.log(filter);
\t\tvar nomor_lokasi = \$(\"select[id=nomor_lokasi]\").val();

\t\tvar x = \$.ajax({
\t\t\turl: \"";
        // line 170
        echo twig_escape_filter($this->env, site_url("pengadaan/pencarian"), "html", null, true);
        echo "?filter={0}&nomor_lokasi={1}\".format(filter, nomor_lokasi),
\t\t\ttype: \"POST\",
\t\t\tdataType: \"json\"
\t\t});

\t\tx.done(function(json) {
\t\t\tvar tbody = \$(\"table > tbody\");
\t\t\ttbody.empty();
\t\t\t
\t\t\tvar data = json.data;
\t\t\tfor(var i=0; i<data.length; i++) {
\t\t\t\tvar t = data[i];
\t\t\t\t
\t\t\t\tvar c = [
\t\t\t\t\t'<tr>',
\t\t\t\t\t\t'<td></td>',
\t\t\t\t\t\t'<td width=\"15px;\" style=\"text-align:center;\">',
\t\t\t\t\t\t\t'<a posted jpid=\"'+ t.jpid +'\" href=\"#\" title=\"Posting\">',
\t\t\t\t\t\t\t\t'<span class=\"glyphicon glyphicon-open\"></span>',
\t\t\t\t\t\t\t'</a>',
\t\t\t\t\t\t'</td>',
\t\t\t\t\t\t'<td width=\"15px;\" style=\"text-align:center;\">',
\t\t\t\t\t\t\t'<a target=\"_blank\" href=\"";
        // line 192
        echo twig_escape_filter($this->env, site_url("pengadaan/form"), "html", null, true);
        echo "/' + t.jpid + '\" title=\"Edit\">',
\t\t\t\t\t\t\t\t'<span class=\"glyphicon glyphicon-pencil\"></span>',
\t\t\t\t\t\t\t'</a>',
\t\t\t\t\t\t'</td>',
\t\t\t\t\t\t'<td width=\"15px;\" style=\"text-align:center;\">',
\t\t\t\t\t\t\t'<a onclick=\"return confirm(\\'Anda yakin ingin menghapus data ini ?\\')\" href=\"";
        // line 197
        echo twig_escape_filter($this->env, site_url("pengadaan/hapus/"), "html", null, true);
        echo "/' + t.jpid + '\" title=\"Hapus\">',
\t\t\t\t\t\t\t\t'<span class=\"glyphicon glyphicon-trash\"></span>',
\t\t\t\t\t\t\t'</a>',
\t\t\t\t\t\t'</td>',
\t\t\t\t\t\t'<td>' + t.jpnomorlokasi + '</td>',
\t\t\t\t\t\t'<td>' + t.jptgl + '</td>',
\t\t\t\t\t\t'<td>' + t.id_kontrak + '</td>',
\t\t\t\t\t\t'<td>' + t.jpkodekegiatan + '</td>',
\t\t\t\t\t\t'<td>' + t.jpnobaterima + '</td>',
\t\t\t\t\t'</tr>'
\t\t\t\t].join(\"\\r\\n\");

\t\t\t\ttbody.append(c);
\t\t\t}
\t\t\t
\t\t\t\$(\"div#pagination\").html(json.pagination);

\t\t});

\t\tx.fail(function() {
\t\t\treturn;
\t\t\t//window.location.replace(\"";
        // line 218
        echo twig_escape_filter($this->env, site_url("pengadaan/index"), "html", null, true);
        echo "/?filter={0}\".format(filter));
\t\t});
\t}


\t\$(\"input[name=cari]\").keyup(function(e) {
\t\t delay(function(){
\t\t \tcari_data();
\t    }, 500);
\t});


\t\$(\"select[id=nomor_lokasi]\").change(function() {
\t\tcari_data();
\t});

\t//posting ke simbada
\t\$(\"a[posted]\").on(\"click\", function() {
\t\tvar jpid = \$(this).attr(\"jpid\");

\t\t\$.post(\"";
        // line 238
        echo twig_escape_filter($this->env, site_url("pengadaan/post"), "html", null, true);
        echo "/\" + jpid, {}, function() {
\t\t\talert (\"Data berhasil diposting ke Simbada\")
\t\t}, \"json\");
\t});
});
</script>
";
    }

    // line 247
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 248
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "

<style type=\"text/css\">
\t.control-label{
\t\tpadding-top: 8px !important;
\t}

\t*[class^='col-'] {
\t\tpadding-left: 0px;
\t\tpadding-right: 0px;
\t}

\tselect.form-control {
\t\theight: 48px !important;
\t}

\tinput.form-control {
\t\theight: 34px !important;
\t}

\ttextarea.form-control {
\t\theight:auto !important;
\t}

\t.spacer {
\t    margin-top: 20px;
\t}
</style>
";
    }

    public function getTemplateName()
    {
        return "pengadaan/daftar.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  373 => 248,  370 => 247,  359 => 238,  336 => 218,  312 => 197,  304 => 192,  279 => 170,  263 => 157,  238 => 135,  234 => 134,  230 => 133,  227 => 132,  215 => 122,  207 => 116,  198 => 113,  194 => 112,  190 => 111,  186 => 110,  182 => 109,  179 => 108,  175 => 107,  139 => 74,  131 => 68,  122 => 65,  118 => 64,  114 => 63,  110 => 62,  106 => 61,  99 => 57,  91 => 52,  83 => 47,  78 => 44,  74 => 43,  33 => 4,  30 => 3,);
    }
}
