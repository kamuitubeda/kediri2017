<?php

/* aset/modal_tambah_nilai.html */
class __TwigTemplate_bec2bbca32d93ad9f85b81086872ae74 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"modal fade\" id=\"tambah-aset-renov\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
\t<div class=\"modal-dialog modal-md\">
\t\t<div class=\"modal-content\">
\t\t\t<div class=\"modal-header\">
\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
\t\t\t\t<h4 class=\"modal-title\" id=\"myModalLabel\">Tambahkan Aset Renovasi</h4>
\t\t\t</div>
\t\t\t<div class=\"modal-body\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t<form role=\"form\" id=\"tambah-aset-renov-form\">
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label class=\"control-label\">Aset Induk<strong>*</strong></label>
\t\t\t\t\t\t\t\t<input type=\"text\" class=\"form-control\" id=\"aset-induk-dummie\" name=\"aset-induk-dummie\" value=\"\" readonly=\"readonly\">
\t\t\t\t\t\t\t\t<input type=\"hidden\" id=\"aset-induk-id\" name=\"aset-induk-id\" value=\"\">
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"form-group\">
\t\t\t\t\t\t\t\t<label class=\"control-label\">Aset Renov<strong>*</strong></label>
\t\t\t\t\t\t\t\t<select type=\"text\" class=\"form-control\" id=\"aset-renov-id\" name=\"aset-renov-id\" data-rule-required=\"true\">
\t\t\t\t\t\t\t\t\t<option value=\"\">Pilih Aset Renov</option>
\t\t\t\t\t\t\t\t</select>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t\t<div class=\"form-group has-error\" style=\"display:none;\">
\t\t\t\t\t\t\t\t<label class=\"control-label\"><strong>Aset Induk dan Aset Renov Tidak Boleh Sama</strong></label>
\t\t\t\t\t\t\t</div>
\t\t\t\t\t\t</form>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"modal-footer\">
\t\t\t\t<button type=\"button\" name=\"batal\" class=\"btn btn-default\" data-dismiss=\"modal\">Batal</button>
\t\t\t\t<button type=\"button\" name=\"simpan\" class=\"btn btn-primary\">Tambah</button>
\t\t\t</div>
\t\t</div>
\t</div>
</div>";
    }

    public function getTemplateName()
    {
        return "aset/modal_tambah_nilai.html";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
