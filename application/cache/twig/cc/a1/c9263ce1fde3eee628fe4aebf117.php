<?php

/* aset/rekap_penyusutan.html */
class __TwigTemplate_cca1c9263ce1fde3eee628fe4aebf117 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("_templates/adminpage.html");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'scripts' => array($this, 'block_scripts'),
            'stylesheets' => array($this, 'block_stylesheets'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_templates/adminpage.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "
<div class=\"row\">
\t<div class=\"col-xs-12\" style=\"padding-left:0px; padding-right:0px;\">
\t\t<div class=\"row\" name=\"select_kib\">
\t\t\t<div class=\"col-xs-3\">
\t\t\t\t<label>JENIS KIB</label>
\t\t\t\t<select type=\"text\" name=\"jenis_kib\" class=\"form-control\">
\t\t\t\t\t<option value=\"B\">KIB B (Peralatan dan Kendaraan)</option>
\t\t\t\t\t<option value=\"C\">KIB C (Gedung dan Bangunan)</option>
\t\t\t\t\t<option value=\"D\">KIB D (Jalan dan Jembatan)</option>
\t\t\t\t\t<option value=\"E\">KIB E (Buku, Hewan, Tanaman, Barang Kebudayaan)</option>
\t\t\t\t\t<option value=\"G\">KIB G (Aset Tak Berwujud)</option>
\t\t\t\t</select>
\t\t\t</div>
\t\t\t<div class=\"col-xs-3\">
\t\t\t\t<label>JENIS ASET</label>
\t\t\t\t<select type=\"text\" name=\"jenis_aset\" class=\"form-control\">
\t\t\t\t\t<option value=\"1.3\">1.3 (Aset Tetap)</option>
\t\t\t\t\t<option value=\"1.5\">1.5 (Aset Lain-lain)</option>
\t\t\t\t</select>
\t\t\t</div>
\t\t\t<div class=\"col-xs-3\">
\t\t\t\t<label>KODE KEPEMILIKAN</label>
\t\t\t\t<select type=\"text\" name=\"kode_kepemilikan\" class=\"form-control\">
\t\t\t\t\t<option value=\"12\">12 (Pemerintah Kab/Kota)</option>
\t\t\t\t\t<option value=\"98\">98 (Ekstrakom)</option>
\t\t\t\t</select>
\t\t\t</div>
\t\t\t<div class=\"col-xs-2\" name=\"button-susut\">
\t\t\t\t<button id=\"btnHitungSusut\" type=\"button\" class=\"btn btn-primary\"><span class=\"glyphicon glyphicon-ok-circle\"></span> Hitung Penyusutan</button>
\t\t\t</div>\t
\t\t</div>
\t\t<div class=\"row\" name=\"hasil_susut\" style=\"display:none\">
\t\t\t<div class=\"col-xs-12\">
\t\t\t\t<div class=\"table-responsive\">
\t\t\t\t\t<table class=\"table table-hover table_susut\">
\t\t\t\t\t\t<thead class=\"table-bordered\">
\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t<th class=\"table-bordered\" style=\"text-align:center\">Nomor Unit</th>
\t\t\t\t\t\t\t<th class=\"table-bordered\" style=\"text-align:center\">Nama Unit</th>
\t\t\t\t\t\t\t<th class=\"table-bordered\" style=\"text-align:center\">Nilai Perolehan</th>
\t\t\t\t\t\t\t<th class=\"table-bordered\" style=\"text-align:center\">Akumulasi Penyusutan</th>
\t\t\t\t\t\t\t<th class=\"table-bordered\" style=\"text-align:center\">Beban</th>
\t\t\t\t\t\t\t<th class=\"table-bordered\" style=\"text-align:center\">Nilai Buku</th>
\t\t\t\t\t\t</tr>
\t\t\t\t\t\t</thead>
\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t</tbody>
\t\t\t\t\t</table>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
</div>
";
    }

    // line 60
    public function block_scripts($context, array $blocks = array())
    {
        // line 61
        $this->displayParentBlock("scripts", $context, $blocks);
        echo "
<script type=\"text/javaScript\" src=\"";
        // line 62
        echo twig_escape_filter($this->env, base_url("assets/js/jquery.loadmask.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 63
        echo twig_escape_filter($this->env, base_url("assets/js/select2.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 64
        echo twig_escape_filter($this->env, base_url("assets/js/accounting.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 65
        echo twig_escape_filter($this->env, base_url("assets/js/bootstrap-switch.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 66
        echo twig_escape_filter($this->env, base_url("assets/js/xlsx.core.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 67
        echo twig_escape_filter($this->env, base_url("assets/js/Blob.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 68
        echo twig_escape_filter($this->env, base_url("assets/js/FileSaver.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 69
        echo twig_escape_filter($this->env, base_url("assets/js/tableexport.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\">
    jQuery(document).ready(function(\$) {
    \t\$('input[name=\"kapital-checkbox\"]').bootstrapSwitch();
    \t// configure setting accounting.js
\t\taccounting.settings = {
\t\t\tnumber: {
\t\t\t\tprecision : 0,  // default precision on numbers is 0
\t\t\t\tthousand: \".\",
\t\t\t\tdecimal : \",\"
\t\t\t}
\t\t}

\t\tvar n = function(v, p) {
\t\t\treturn accounting.formatNumber(v, p);
\t\t}

\t\tvar u = function(v) {
\t\t\treturn accounting.unformat(v);
\t\t}
\t\t
\t\t\$(\"#btnHitungSusut\").click(function() {
    \t\t\$('tbody').empty();
    \t\t\$(\"body\").mask(\"Processing...\");
    \t\t\$('caption').empty();

    \t\tvar jenis_aset = \$(\"select[name=jenis_aset]\").val();
    \t\tvar kib = \$('select[name=\"jenis_kib\"]').val();
    \t\tvar kode_kepemilikan = \$(\"select[name=kode_kepemilikan]\").val();
    \t\t\$('div[name=\"hasil_susut\"]').show();

\t\t\t\$.post(\"";
        // line 100
        echo twig_escape_filter($this->env, site_url("aset/get_rekap_susut_per_skpd"), "html", null, true);
        echo "\", {kib:kib, jenis_aset:jenis_aset, kode_kepemilikan:kode_kepemilikan}, function(json) {
\t\t\t\tvar aset = json.aset;
\t\t\t\tvar total = json.total;
\t\t\t\t
\t\t\t\tvar html = '';
\t\t\t\tvar tables = \$(\"table\");

\t\t\t\t\$.each(aset, function(key, value) {
\t\t\t\t\tvar value = aset[key];

\t\t\t\t\thtml += '<tr><td>'
\t\t\t\t\t+ value.nomor_unit
\t\t\t\t\t+ '</td><td>'
\t\t\t\t\t+ value.nama_unit
\t\t\t\t\t+ '</td><td align=\"right\">'
\t\t\t\t\t+ value.nilai_perolehan
\t\t\t\t\t+ '</td><td align=\"right\">'
\t\t\t\t\t+ value.akumulasi_penyusutan
\t\t\t\t\t+ '</td><td align=\"right\">'
\t\t\t\t\t+ value.beban
\t\t\t\t\t+ '</td><td align=\"right\">'
\t\t\t\t\t+ value.nilai_buku
\t\t\t\t\t+ '</td></tr>';
\t\t\t\t});

\t\t\t\thtml += '<tr><td>'
\t\t\t\t\t+ '<strong>' + \"TOTAL\" + '</strong>'
\t\t\t\t\t+ '</td><td>'
\t\t\t\t\t+ ''
\t\t\t\t\t+ '</td><td align=\"right\">'
\t\t\t\t\t+ '<strong>' + total.total_nilai_perolehan + '</strong>'
\t\t\t\t\t+ '</td><td align=\"right\">'
\t\t\t\t\t+ '<strong>' + total.total_akumulasi_penyusutan + '</strong>'
\t\t\t\t\t+ '</td><td align=\"right\">'
\t\t\t\t\t+ '<strong>' + total.total_beban + '</strong>'
\t\t\t\t\t+ '</td><td align=\"right\">'
\t\t\t\t\t+ '<strong>' + total.total_nilai_buku + '</strong>'
\t\t\t\t\t+ '</td></tr>';

\t\t\t\t\$('.table_susut > tbody').append(html);
\t\t\t\t\$(\"body\").unmask();

\t\t\t\ttables.tableExport({
\t\t\t\t    bootstrap: true,
\t\t\t\t    format: [\"xlsx\"]
\t\t\t\t});
\t\t\t}, \"json\");
    \t});
\t});
</script>
";
    }

    // line 153
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 154
        echo "\t";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 155
        echo twig_escape_filter($this->env, base_url("assets/css/devexpress-like/jquery-ui.css"), "html", null, true);
        echo "\" />
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 156
        echo twig_escape_filter($this->env, base_url("assets/css/select2.min.css"), "html", null, true);
        echo "\" />
\t<link href=\"";
        // line 157
        echo twig_escape_filter($this->env, base_url("assets/css/jquery.loadmask.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
\t<link href=\"";
        // line 158
        echo twig_escape_filter($this->env, base_url("assets/css/bootstrap-switch.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 159
        echo twig_escape_filter($this->env, base_url("assets/css/tableexport.css"), "html", null, true);
        echo "\" />\t

\t<style type=\"text/css\">
\t\t.control-label{
\t\t\tpadding-top: 8px !important;
\t\t}

\t\tselect.form-control {
\t\t\theight: 48px !important;
\t\t}

\t\tinput.form-control {
\t\t\theight: 34px !important;
\t\t}

\t\ttextarea.form-control {
\t\t\theight:auto !important;
\t\t}

\t\t.row {
\t\t\tpadding:10px 0px;
\t\t}

\t\t.spacer {
\t\t    margin-top: 20px;
\t\t}

\t\t.ui-autocomplete{
\t\t\tbackground: none repeat scroll 0 0 white;
\t\t}

\t\t.input-group-addon{
\t\t\tborder-radius: 0 !important;
\t\t}

\t\t.input-group .form-control{
\t\t\tz-index: 0; !important;
\t\t}

\t\t.select2-selection__rendered {
\t\t\theight: 28px;
\t\t}
\t\t.table > thead > tr > th {
\t\t\tvertical-align: middle;
\t\t}
\t\t#btnHitungSusut {
\t\t\tmargin-top: 22px;
\t\t\theight: 30px;
\t\t}
\t\t.btn-toolbar {
\t\t\ttext-align: left !Important;
\t\t}
\t</style>
";
    }

    public function getTemplateName()
    {
        return "aset/rekap_penyusutan.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  239 => 159,  235 => 158,  231 => 157,  227 => 156,  223 => 155,  218 => 154,  215 => 153,  160 => 100,  126 => 69,  122 => 68,  118 => 67,  114 => 66,  110 => 65,  106 => 64,  102 => 63,  98 => 62,  94 => 61,  91 => 60,  33 => 3,  30 => 2,);
    }
}
