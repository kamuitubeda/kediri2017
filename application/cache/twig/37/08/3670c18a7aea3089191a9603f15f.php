<?php

/* simgo/persediaan/laporan/bukti_pengambilan_barang.html */
class __TwigTemplate_37083670c18a7aea3089191a9603f15f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("_templates/adminpage.html");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'scripts' => array($this, 'block_scripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_templates/adminpage.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 1
    public function block_content($context, array $blocks = array())
    {
        // line 2
        $this->env->loadTemplate("simgo/persediaan/modal/modal_persiapan_cetak.html")->display($context);
        // line 3
        echo "<div class=\"container\">
    <div class=\"row\">
        <div class=\"col-md-12\">
            <ol class=\"breadcrumb\">
                <li><a href=\"";
        // line 7
        echo twig_escape_filter($this->env, site_url("persediaan"), "html", null, true);
        echo "\">Persediaan</a></li>
                <li><a href=\"";
        // line 8
        echo twig_escape_filter($this->env, site_url("persediaan/katalog_bukti_pengambilan"), "html", null, true);
        echo "\">Daftar Buku Pengambilan Barang</a></li>
                <li class=\"active\">Buku Pengambilan Barang</li>
            </ol>
        </div>
    </div>
\t<div class=\"row\">
\t\t<!--<div class=\"col-md-3 pull-right\">
\t\t\t<div data-date-format=\"mm-yyyy\" data-date=\"";
        // line 15
        echo twig_escape_filter($this->env, (isset($context["today"]) ? $context["today"] : null), "html", null, true);
        echo "\" datepicker class=\"input-group input-append date\">
\t\t\t\t<input id=\"tanggal\" name=\"tanggal\" readonly=\"\" class=\"form-control\" value=\"";
        // line 16
        echo twig_escape_filter($this->env, (isset($context["today"]) ? $context["today"] : null), "html", null, true);
        echo "\"></input>
\t\t\t\t<span class=\"input-group-addon add-on\"><i class=\"icon-calendar\"></i></span>
\t\t\t</div>
\t\t</div>-->
\t</div>
    <div class=\"row\">
        <div class=\"col-md-12\">
            <table class=\"table\" style=\"border-style:hidden;\">
                <tr>
                    <td class='col-md-6 text-left'><strong>Daerah/SKPD : ";
        // line 25
        echo twig_escape_filter($this->env, (isset($context["oname"]) ? $context["oname"] : null), "html", null, true);
        echo "</strong></td>
                    <td class='col-md-6 text-right'><strong>No. : ";
        // line 26
        echo twig_escape_filter($this->env, (isset($context["juno"]) ? $context["juno"] : null), "html", null, true);
        echo "</strong></td>
                </tr>
            </table>
        </div>
    </div>
    <div class=\"row\">
        <div class=\"col-md-12 text-center\">
            <h2>Bukti Pengambilan Barang Dari Gudang</h2></div>
    </div>
    <div class=\"row\">
        <div class=\"col-md-12\">
            <table class=\"table table-striped table-bordered\">
                <thead>
                    <tr>
                        <th class=\"col-md-1 text-center\" rowspan=\"2\">Nomor SPPB</th>
                        <th class=\"col-md-1 text-center\" rowspan=\"2\">Tanggal SPPB</th>
                        <th class=\"text-center\" rowspan=\"2\">Nama dan Spesifikasi Barang</th>
                        <th class=\"text-center\" rowspan=\"2\">Satuan</th>
                        <th class=\"text-center\" colspan=\"2\">Jumlah Barang</th>
                        <th class=\"col-md-2 text-center\" rowspan=\"2\">Jumlah Harga</th>
                    </tr>
                    <tr>
                        <th class=\"col-md-1 text-center\">(angka)</th>
                        <th class=\"col-md-1 text-center\">(huruf)</th>
                    </tr>
                    <tr>
                        ";
        // line 52
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 7));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 53
            echo "                        <th class=\"small-header text-center\"><small>";
            echo twig_escape_filter($this->env, (isset($context["i"]) ? $context["i"] : null), "html", null, true);
            echo "</small></th>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 55
        echo "                    </tr>
                </thead>
                <tbody id=\"data-body\">
                    <!--<tr></tr>-->
                    ";
        // line 59
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["table_data"]) ? $context["table_data"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["x"]) {
            // line 60
            echo "                        ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["x"]) ? $context["x"] : null), "detail"));
            foreach ($context['_seq'] as $context["_key"] => $context["y"]) {
                // line 61
                echo "                            <tr>
                                <td>";
                // line 62
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["x"]) ? $context["x"] : null), "judasarno"), "html", null, true);
                echo "</td>
                                <td>";
                // line 63
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["x"]) ? $context["x"] : null), "judasartgl"), "html", null, true);
                echo "</td>
                                <td>";
                // line 64
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["y"]) ? $context["y"] : null), "namabarang"), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["y"]) ? $context["y"] : null), "spesifikasi"), "html", null, true);
                echo "</td>
                                <td>";
                // line 65
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["y"]) ? $context["y"] : null), "judunit"), "html", null, true);
                echo "</td>
                                <td>";
                // line 66
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["y"]) ? $context["y"] : null), "judqty"), "html", null, true);
                echo "</td>
                                <td>";
                // line 67
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["y"]) ? $context["y"] : null), "qty_huruf"), "html", null, true);
                echo "</td>
                                <td>";
                // line 68
                echo twig_escape_filter($this->env, ($this->getAttribute((isset($context["y"]) ? $context["y"] : null), "judhargasat") * $this->getAttribute((isset($context["y"]) ? $context["y"] : null), "judqty")), "html", null, true);
                echo "</td>
                            </tr>
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['y'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 71
            echo "                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['x'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 72
        echo "                </tbody>
            </table>
        </div>
    </div>
    <div class=\"row\">
        <div class=\"col-md-12\">
            <div class=\"btn-group pull-right\">
                <button id=\"kembali\" type=\"button\" class=\"btn btn-default\">Kembali</button>
                <button id=\"cetak\" type=\"button\" class=\"btn btn-primary\"><span class=\"glyphicon glyphicon-print\"></span> Cetak</button>
            </div>
        </div>
    </div>
</div>
";
    }

    // line 85
    public function block_stylesheets($context, array $blocks = array())
    {
        echo " ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 86
        echo twig_escape_filter($this->env, base_url("assets/css/datepicker3.css"), "html", null, true);
        echo "\" />
<link rel=\"stylesheet\" type=\"text/css\" media=\"print\" href=\"";
        // line 87
        echo twig_escape_filter($this->env, base_url("assets/css/bootstrap.min.css"), "html", null, true);
        echo "\">
<style type=\"text/css\">
    .small-header {
        font-weight: normal !important;
        padding: 2px !important;
    }
    
    .text-middle{
        vertical-align: middle !important;
    }
    
    @media print {
        body * {
            visibility: hidden;
        }
        #topbar,
        #sidebar,
        .breadcrumb,
        h1 {
            display: none;
        }
        .container,
        .container * {
            visibility: visible;
        }
        .sidebar-max {
            padding: 0px !important;
        }
        #content {
            position: absolute !important;
            left: 0px !important;
            top: 0px !important;
        }
    }
</style>
";
    }

    // line 122
    public function block_scripts($context, array $blocks = array())
    {
        echo " ";
        $this->displayParentBlock("scripts", $context, $blocks);
        echo "
<script type=\"text/javaScript\" src=\"";
        // line 123
        echo twig_escape_filter($this->env, base_url("assets/js/bootstrap-datepicker.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 124
        echo twig_escape_filter($this->env, site_url("assets/js/numeral/numeral.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 125
        echo twig_escape_filter($this->env, site_url("assets/js/numeral/languages.id.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 126
        echo twig_escape_filter($this->env, base_url("assets/js/base64.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\">

    numeral.language('id-ID');
    
    jQuery(function(\$){
        populateTable();

\t\t// \$(\"*[datepicker]\").datepicker({
\t\t// \tautoclose:true,
\t\t// \torientation:\"bottom\",
\t\t// \tformat: \"mm-yyyy\",
\t\t// \tstartView: \"months\", 
\t\t// \tminViewMode: \"months\"
\t\t// });
        
        // \$(\"#tanggal\").on(\"change\", function(){
        //     populateTable();
        // });
        
        function populateTable(){
            \$(\"#data-body\").empty();
            \$(\"#data-body\").append(\"<tr></tr><tr></tr>\");
            
            data = [];
            
            ";
        // line 152
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["table_data"]) ? $context["table_data"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["x"]) {
            // line 153
            echo "                data.push( {
                    judasarno : \"";
            // line 154
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["x"]) ? $context["x"] : null), "judasarno"), "html", null, true);
            echo "\",
                    judasartgl : \"";
            // line 155
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["x"]) ? $context["x"] : null), "judasartgl"), "html", null, true);
            echo "\",
                    detail : [],
                } );
                ";
            // line 158
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["x"]) ? $context["x"] : null), "detail"));
            foreach ($context['_seq'] as $context["_key"] => $context["y"]) {
                // line 159
                echo "                    data[data.length-1].detail.push( {
                        namabarang : \"";
                // line 160
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["y"]) ? $context["y"] : null), "namabarang"), "html", null, true);
                echo "\",
                        spesifikasi : \"";
                // line 161
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["y"]) ? $context["y"] : null), "spesifikasi"), "html", null, true);
                echo "\",
                        judunit : \"";
                // line 162
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["y"]) ? $context["y"] : null), "judunit"), "html", null, true);
                echo "\",
                        judqty : -parseFloat(\"";
                // line 163
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["y"]) ? $context["y"] : null), "judqty"), "html", null, true);
                echo "\"),
                        qty_huruf : \"";
                // line 164
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["y"]) ? $context["y"] : null), "qty_huruf"), "html", null, true);
                echo "\",
                        judhargasat : \"";
                // line 165
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["y"]) ? $context["y"] : null), "judhargasat"), "html", null, true);
                echo "\"
                    } );
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['y'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 168
            echo "            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['x'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 169
        echo "            
            for (x in data) {
                if(data[x].detail != null){
                    for (y in data[x].detail) {
                        var hargaTotal = data[x].detail[y].judhargasat * data[x].detail[y].judqty;
                        \$(\"#data-body\").append(
                            \"<tr>\"+
                                \"<td class='text-center'>\"+((y == 0)? data[x].judasarno : \"\")+\"</td>\"+
                                \"<td class='text-center'>\"+((y == 0)? data[x].judasartgl : \"\")+\"</td>\"+
                                \"<td>\"+data[x].detail[y].namabarang+\"<br/><small>\"+data[x].detail[y].spesifikasi+\"</small></td>\"+
                                \"<td>\"+data[x].detail[y].judunit+\"</td>\"+
                                \"<td class='text-right'>\"+numeral(data[x].detail[y].judqty).format(\"0,0.0\")+\"</td>\"+
                                \"<td><small>\"+data[x].detail[y].qty_huruf+\"</small></td>\"+
                                \"<td class='text-right'>\"+numeral(hargaTotal).format(\"\$0,0.00\")+\"</td>\"+
                            \"</tr>\"
                        );
                    }
                }
                else
                {
                    \$(\"#data-body\").append(
                        \"<tr>\"+
                            \"<td class='text-center'>\"+data[x].judasarno+\"</td>\"+
                            \"<td class='text-center'>\"+data[x].judasartgl+\"</td>\"+
                            \"<td class='text-center text-middle'>-</td>\"+
                            \"<td class='text-center text-middle'>-</td>\"+
                            \"<td class='text-center text-middle'>-</td>\"+
                            \"<td class='text-center text-middle'>-</td>\"+
                            \"<td class='text-center text-middle'>-</td>\"+
                        \"</tr>\"
                    );
                }
            }
            
            // data.forEach(function(value, index){
            //     var nomor = value[\"judasarno\"];
            //     var tglrec = value[\"judasartgl\"];
            //     if(value.detail!=null){
            //         value.detail.forEach(function(value, index){
            //             \$(\"#data-body\").append(
            //                 \"<tr>\"+
            //                     \"<td class='text-center'>\"+((index == 0)? nomor : \"\")+\"</td>\"+
            //                     \"<td class='text-center'>\"+((index == 0)? tglrec : \"\")+\"</td>\"+
            //                     \"<td>\"+value['namabarang']+\"<br/><small>\"+value['spesifikasi']+\"</small></td>\"+
            //                     \"<td>\"+value['judunit']+\"</td>\"+
            //                     \"<td class='text-right'>\"+numeral(-value['judqty']).format(\"0,0.0\")+\"</td>\"+
            //                     \"<td><small>\"+value['qty_huruf']+\"</small></td>\"+
            //                     \"<td class='text-right'>\"+numeral(-value['judhargasat'] * value['judqty']).format(\"\$0,0.00\")+\"</td>\"+
            //                 \"</tr>\"
            //             );
            //         }, nomor, tglrec);
            //     }
            //     else
            //     {
            //         \$(\"#data-body\").append(
            //             \"<tr>\"+
            //                 \"<td class='text-center'>\"+nomor+\"</td>\"+
            //                 \"<td class='text-center'>\"+tglrec+\"</td>\"+
            //                 \"<td class='text-center text-middle'>-</td>\"+
            //                 \"<td class='text-center text-middle'>-</td>\"+
            //                 \"<td class='text-center text-middle'>-</td>\"+
            //                 \"<td class='text-center text-middle'>-</td>\"+
            //                 \"<td class='text-center text-middle'>-</td>\"+
            //             \"</tr>\"
            //         );
            //     }
            // });
        }
        
        
        \$(\"#lakukan-cetak\").on('click', function(){
            var juno = Base64.encode(\"";
        // line 240
        echo twig_escape_filter($this->env, (isset($context["juno"]) ? $context["juno"] : null), "html", null, true);
        echo "\");
            var skpd = Base64.encode(\"";
        // line 241
        echo twig_escape_filter($this->env, (isset($context["skpd"]) ? $context["skpd"] : null), "html", null, true);
        echo "\");
            var tanggal = Base64.encode(\"04-2016\");
            var namaunit = Base64.encode(\"";
        // line 243
        echo twig_escape_filter($this->env, (isset($context["oname"]) ? $context["oname"] : null), "html", null, true);
        echo "\");
            var pengurusbarang = Base64.encode(\$(\"#nama-petugas\").val());
            var nippengurusbarang = Base64.encode(\$(\"#nip-petugas\").val());
            var atasanlangsung = Base64.encode(\$(\"#nama-atasan\").val());
            var nipatasanlangsung = Base64.encode(\$(\"#nip-atasan\").val());
            var penerima = Base64.encode(\$(\"#nama-pengambil\").val());
            var nippenerima = Base64.encode(\$(\"#nip-pengambil\").val());
            
            if(juno === \"\") {juno = \"-\"}
            if(skpd === \"\") {skpd = \"-\"}
            if(tanggal === \"\") {tanggal = \"-\"}
            if(namaunit === \"\") {namaunit = \"-\"}
            if(pengurusbarang === \"\") {pengurusbarang = \"-\"}
            if(nippengurusbarang === \"\") {nippengurusbarang = \"-\"}
            if(atasanlangsung === \"\") {atasanlangsung = \"-\"}
            if(nipatasanlangsung === \"\") {nipatasanlangsung = \"-\"}
            if(penerima === \"\") {penerima = \"-\"}
            if(nippenerima === \"\") {nippenerima = \"-\"}
            
            window.open(\"";
        // line 262
        echo twig_escape_filter($this->env, site_url("persediaan/print_bukti_pengambilan_barang"), "html", null, true);
        echo "/\"+ namaunit + \"/\" + skpd + \"/\" + juno + \"/\" + tanggal + \"/\" + pengurusbarang + \"/\" + nippengurusbarang + \"/\" + penerima + \"/\" + nippenerima + \"/\" + atasanlangsung + \"/\" + nipatasanlangsung, \"_blank\");
        });

        \$(\"#cetak\").on(\"click\", function(){
            \$(\"#persiapan-cetak\").modal('show');
        });
        \$(\"#kembali\").on(\"click\", function(){
            window.location.href = \"";
        // line 269
        echo twig_escape_filter($this->env, site_url("persediaan/katalog_bukti_pengambilan"), "html", null, true);
        echo "\";
        });
\t});

</script>
";
    }

    public function getTemplateName()
    {
        return "simgo/persediaan/laporan/bukti_pengambilan_barang.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  465 => 269,  455 => 262,  433 => 243,  428 => 241,  424 => 240,  351 => 169,  345 => 168,  336 => 165,  332 => 164,  328 => 163,  324 => 162,  320 => 161,  316 => 160,  313 => 159,  309 => 158,  303 => 155,  299 => 154,  296 => 153,  292 => 152,  263 => 126,  259 => 125,  255 => 124,  251 => 123,  244 => 122,  204 => 87,  200 => 86,  193 => 85,  176 => 72,  170 => 71,  161 => 68,  157 => 67,  153 => 66,  149 => 65,  143 => 64,  139 => 63,  135 => 62,  132 => 61,  127 => 60,  123 => 59,  117 => 55,  108 => 53,  104 => 52,  75 => 26,  71 => 25,  59 => 16,  55 => 15,  45 => 8,  41 => 7,  35 => 3,  33 => 2,  30 => 1,);
    }
}
