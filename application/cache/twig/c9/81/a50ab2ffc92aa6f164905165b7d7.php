<?php

/* denied.html */
class __TwigTemplate_c981a50ab2ffc92aa6f164905165b7d7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("_templates/bootstrap_base.html");

        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'scripts' => array($this, 'block_scripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_templates/bootstrap_base.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        // line 4
        echo "\t<nav style=\"margin-bottom: 0;\" role=\"navigation\"
\t\tclass=\"navbar navbar-default\">
\t\t<div class=\"container\" role=\"contentinfo\">
\t\t\t<img src=\"";
        // line 7
        echo twig_escape_filter($this->env, base_url("assets/img/"), "html", null, true);
        echo "\" class=\"logo\" />
\t\t\t<p class=\"lead\" style=\"font-weight: bold;\">";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["appname"]) ? $context["appname"] : null), "html", null, true);
        echo "</p>
\t\t</div>
\t</nav>
\t<div class=\"wrap\">
\t\t<div class=\"container\"
\t\t\tstyle=\"padding: 20px 0 20px 0; text-align: center\">
\t\t\t<p class=\"lead\">
\t\t\t<div>
\t\t\t\t<b style=\"font-size: 90px;\">DENIED</b>
\t\t\t</div>
\t\t\t<div style=\"font-size: 40px;\">Anda tidak memiliki akses ke URL yang Anda tuju</div>
\t\t\t<div style=\"padding: 10px; font-size: 16px\">
\t\t\t\t<a href=\"";
        // line 20
        echo twig_escape_filter($this->env, site_url(), "html", null, true);
        echo "\">[Halaman Utama]</a> | <a
\t\t\t\t\thref=\"javascript:back(-1)\">[Kembali]</a>
\t\t\t</div>
\t\t\t</p>
\t\t</div>
\t</div>
\t<footer class=\"bs-footer\" role=\"contentinfo\">
\t\t<div class=\"container\">
\t\t\t<p style=\"margin-top: 10px;\">";
        // line 28
        echo twig_escape_filter($this->env, (isset($context["appcode"]) ? $context["appcode"] : null), "html", null, true);
        echo " &copy; 2014 was developed by PT. Sarana Integrasi Informatika</p>
\t\t</div>
\t</footer>
";
    }

    // line 33
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 34
        echo "\t";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 35
        echo twig_escape_filter($this->env, base_url("assets/css/font-awesome.min.css"), "html", null, true);
        echo "\">
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 36
        echo twig_escape_filter($this->env, base_url("assets/css/icomoon.css"), "html", null, true);
        echo "\">
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 37
        echo twig_escape_filter($this->env, base_url("assets/css/madmin.css"), "html", null, true);
        echo "\">
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 38
        echo twig_escape_filter($this->env, base_url("assets/css/login.css"), "html", null, true);
        echo "\">
";
    }

    // line 42
    public function block_scripts($context, array $blocks = array())
    {
        // line 43
        echo "\t";
        $this->displayParentBlock("scripts", $context, $blocks);
        echo "
\t<script type=\"text/javascript\">
\t\tjQuery(document).ready(function(\$) {
\t\t\t
\t\t});
\t</script>
";
    }

    public function getTemplateName()
    {
        return "denied.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  105 => 43,  102 => 42,  96 => 38,  92 => 37,  88 => 36,  84 => 35,  79 => 34,  76 => 33,  68 => 28,  57 => 20,  42 => 8,  38 => 7,  33 => 4,  30 => 3,);
    }
}
