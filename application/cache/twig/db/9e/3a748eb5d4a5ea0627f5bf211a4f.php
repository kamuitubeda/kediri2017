<?php

/* ubah_password.html */
class __TwigTemplate_db9e3a748eb5d4a5ea0627f5bf211a4f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("_templates/adminpage.html");

        $this->blocks = array(
            'scripts' => array($this, 'block_scripts'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_templates/adminpage.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_scripts($context, array $blocks = array())
    {
        // line 4
        $this->displayParentBlock("scripts", $context, $blocks);
        echo "
<script type=\"text/javascript\">
\tjQuery(document).ready(function(\$) {
\t\t\$(\"form\").validate({
\t\t\trules: {
\t\t\t\told_pass: \"required\",
\t\t\t\tnew_pass: \"required\",
\t\t\t\tkonfirmasi_new_pass: {required: \"required\", equalTo: \"#new_pass\"}
\t\t\t},
\t\t\tmessages: {
\t\t\t\told_pass: \"";
        // line 14
        echo sprintf(lang("req_message", ""), "Password Lama");
        echo "\",
\t\t\t\tnew_pass: \"";
        // line 15
        echo sprintf(lang("req_message", ""), "Password Baru");
        echo "\",
\t\t\t\tkonfirmasi_new_pass: {
\t\t\t\t\trequired : \"";
        // line 17
        echo sprintf(lang("req_message", ""), "Konfirmasi Password Baru");
        echo "\",
\t\t\t\t\tequalTo: \"Password Baru dan Konfirmasi Password Baru tidak sama.\"
\t\t\t\t}
\t\t\t},
\t\t\terrorLabelContainer: \$(this).find(\"div.form-error\"),
\t\t\tinvalidHandler: function(event, validator) { 
\t\t\t\tevent.preventDefault();
\t\t\t\tvar errors = validator.numberOfInvalids();
\t\t\t\tif (errors) {
\t\t\t\t\t\$(\".form-message\").hide();
\t\t\t\t} else {
\t\t\t\t\t\$(\".form-message\").show();
\t\t\t\t}

\t\t\t\t\$(\".form-error\").show(\"fast\", function() {
\t\t\t\t\t\$(\"html, body\").animate({ scrollTop: \$(\".form-error\").offset().top-20 }, \"fast\");
\t\t\t\t});
\t\t\t},
\t\t\tfocusInvalid: false
\t\t});
\t});
</script>
";
    }

    // line 41
    public function block_content($context, array $blocks = array())
    {
        // line 42
        echo form_open("secman/user/change_password_action");
        echo "
<div class=\"row\">
\t<div class=\"col-md-4\">
\t\t<div class=\"form-error alert alert-danger\"></div>
\t\t";
        // line 46
        if (((isset($context["success"]) ? $context["success"] : null) != false)) {
            // line 47
            echo "\t\t<div class=\"form-message alert alert-success\">";
            echo (isset($context["success"]) ? $context["success"] : null);
            echo "</div>
\t\t";
        }
        // line 49
        echo "\t\t";
        if (((isset($context["message"]) ? $context["message"] : null) != false)) {
            // line 50
            echo "\t\t<div class=\"form-message alert alert-danger\">";
            echo (isset($context["message"]) ? $context["message"] : null);
            echo "</div>
\t\t";
        }
        // line 52
        echo "\t\t<div class=\"form-group\">
\t\t\t<label for=\"old_pass\">Password Lama</label>
\t\t\t<input type=\"password\" class=\"form-control\" name=\"old_pass\" placeholder=\"Inputkan password lama Anda\">
\t\t</div>
\t\t<div class=\"form-group\">
\t\t\t<label for=\"new_pass\">Password Baru</label>
\t\t\t<input type=\"password\" class=\"form-control\" name=\"new_pass\" id=\"new_pass\" placeholder=\"Inputkan password baru Anda\">
\t\t</div>
\t\t<div class=\"form-group\">
\t\t\t<label for=\"konfirmasi_new_pass\">Konfirmasi Password Baru</label>
\t\t\t<input type=\"password\" class=\"form-control\" name=\"konfirmasi_new_pass\" placeholder=\"Inputkan kembali password baru Anda\">
\t\t</div>
\t\t<button type=\"submit\" class=\"btn btn-default\">Submit</button>
\t</div>
</div>
";
        // line 67
        echo form_close();
        echo "
";
    }

    public function getTemplateName()
    {
        return "ubah_password.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  125 => 67,  108 => 52,  102 => 50,  99 => 49,  93 => 47,  91 => 46,  84 => 42,  81 => 41,  54 => 17,  49 => 15,  45 => 14,  32 => 4,  29 => 3,);
    }
}
