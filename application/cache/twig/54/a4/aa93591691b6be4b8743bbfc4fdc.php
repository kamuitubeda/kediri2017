<?php

/* profil.html */
class __TwigTemplate_54a4aa93591691b6be4b8743bbfc4fdc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("_templates/adminpage.html");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_templates/adminpage.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "
<ul class=\"nav nav-tabs\" data-tabs=\"tabs\">
\t<li class=\"active\"><a data-toggle=\"tab\" href=\"#umum\">Umum</a></li>
\t<li><a data-toggle=\"tab\" href=\"#grup\">Akses</a></li>
</ul>

<div class=\"tab-content\">
\t<div class=\"tab-pane active\" id=\"umum\">
\t\t<div class=\"container\" style=\"padding: 15px;\">
\t\t\t<div class=\"row\" style=\"padding-bottom: 15px;\">
\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t<label>User ID</label>
\t\t\t\t\t<p class=\"form-control-static\">";
        // line 16
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "uid"), "html", null, true);
        echo "</p>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"row\" style=\"padding-bottom: 15px;\">
\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t<label>Nama</label>
\t\t\t\t\t<p class=\"form-control-static\">";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "uname"), "html", null, true);
        echo "</p>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"row\" style=\"padding-bottom: 15px;\">
\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t<label>Email</label>
\t\t\t\t\t<p class=\"form-control-static\">";
        // line 28
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["user"]) ? $context["user"] : null), "uemail"), "html", null, true);
        echo "</p>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
\t<div class=\"tab-pane\" id=\"grup\">
\t\t<div class=\"container\" style=\"padding: 15px 0px 0px 15px;\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t<label>Grup</label>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t";
        // line 40
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["groups"]) ? $context["groups"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["group"]) {
            // line 41
            echo "\t\t\t\t<div class=\"row\" style=\"padding-left:10px;\">
\t\t\t\t\t<div class=\"col-md-6\">
\t\t\t\t\t\t<p class=\"form-control-static\">";
            // line 43
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["group"]) ? $context["group"] : null), "gname"), "html", null, true);
            echo "</p>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['group'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 47
        echo "\t\t</div>
\t</div>
</div>

";
    }

    public function getTemplateName()
    {
        return "profil.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  96 => 47,  86 => 43,  82 => 41,  78 => 40,  63 => 28,  54 => 22,  45 => 16,  31 => 4,  28 => 3,);
    }
}
