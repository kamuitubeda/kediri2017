<?php

/* simgo/persediaan/barang_keluar.html */
class __TwigTemplate_fdf2ac967d28552afb4b5232a8bdcc13 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("_templates/adminpage.html");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'scripts' => array($this, 'block_scripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_templates/adminpage.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 1
    public function block_content($context, array $blocks = array())
    {
        echo " ";
        $this->env->loadTemplate("simgo/persediaan/modal/modal_tambah_barang_keluar.html")->display($context);
        // line 2
        echo " ";
        $this->env->loadTemplate("simgo/persediaan/modal/modal_kelola_jurnal.html")->display($context);
        echo " ";
        $this->env->loadTemplate("simgo/persediaan/modal/modal_persiapan_cetak.html")->display($context);
        // line 3
        echo " ";
        $this->env->loadTemplate("simgo/persediaan/modal/modal_persiapan_cetak_sppb.html")->display($context);
        // line 4
        echo "

<div class=\"container\">
    <div class=\"row\">
        <div class=\"col-md-12\">
            <ol class=\"breadcrumb\">
                <li><a href=\"";
        // line 10
        echo twig_escape_filter($this->env, site_url("persediaan"), "html", null, true);
        echo "\">Persediaan</a></li>
                <li class=\"active\">Barang Keluar</li>
            </ol>
        </div>
    </div>
    <form role=\"form\">
        <div class=\"row\">
            <div class=\"col-md-12\" id=\"alert-placement\">
                <div class=\"alert alert-info\">
                    Data akan disimpan setelah tombol simpan di bagian bawah formulir di-klik.
                </div>
            </div>
        </div>
        <div class=\"row\">
            <div class=\"form-group col-md-4\">
                <label for=\"bktgl\">Tanggal</label>
                <div data-date-format=\"dd-mm-yyyy\" data-date=\"";
        // line 26
        echo twig_escape_filter($this->env, (isset($context["today"]) ? $context["today"] : null), "html", null, true);
        echo "\" datepicker class=\"input-group input-append date\">
                    <input id=\"bktgl\" name=\"bktgl\" readonly=\"\" class=\"form-control\" value=\"";
        // line 27
        echo twig_escape_filter($this->env, (isset($context["today"]) ? $context["today"] : null), "html", null, true);
        echo "\"></input>
                    <span class=\"input-group-addon add-on\"><i class=\"icon-calendar\"></i></span>
                </div>
            </div>
            <div class=\"form-group col-md-6 pull-right\">
                <label for=\"juno\">Nomor Bukti Pengambilan Barang *</label>
                <!--<div class=\"input-group\">-->
                <!--<div class=\"input-group-addon\">027/</div>-->
                <div class=\"input-group\">
                    <input type=\"text\" class=\"form-control\" id=\"juno\" placeholder=\"No. Bukti Pengambilan Barang\" value=\"";
        // line 36
        echo twig_escape_filter($this->env, (isset($context["juno"]) ? $context["juno"] : null), "html", null, true);
        echo "\">
                    <span id='edit-nomor' class=\"icon input-group-addon add-on\"><i class=\"icon-trash\"></i></span>
                </div>
                <!--<div class=\"input-group-addon\">/416-110/PPB/2016</div>-->
                <!--</div>-->
            </div>
        </div>
        <div class=\"row\">
            <div class=\"form-group col-md-12\">
                <label for=\"bidang\">Bidang</label>
                <input type=\"text\" class=\"form-control\" id=\"bidang\" placeholder=\"Bidang yang Meminta\">
            </div>
        </div>
        <div class=\"row\">
            <div class=\"form-group col-md-8\">
                <label for=\"nota_dinas\">Nota Permintaan *</label>
                <input type=\"text\" class=\"form-control\" id=\"nota_dinas\" placeholder=\"Nota Permintaan\">
            </div>
            <div class=\"form-group col-md-4\">
                <label for=\"bptgl\">Tanggal</label>
                <div data-date-format=\"dd-mm-yyyy\" data-date=\"";
        // line 56
        echo twig_escape_filter($this->env, (isset($context["today"]) ? $context["today"] : null), "html", null, true);
        echo "\" datepicker class=\"input-group input-append date\">
                    <input id=\"bptgl\" name=\"bptgl\" readonly=\"\" class=\"form-control\" value=\"";
        // line 57
        echo twig_escape_filter($this->env, (isset($context["today"]) ? $context["today"] : null), "html", null, true);
        echo "\"></input>
                    <span class=\"input-group-addon add-on\"><i class=\"icon-calendar\"></i></span>
                </div>
            </div>
        </div>
        <div class=\"row\">
            <div class=\"form-group col-md-8\">
                <label for=\"sppb\">Nomor SPPB *</label>
                <input type=\"text\" class=\"form-control\" id=\"sppb\" placeholder=\"No. SPPB\">
            </div>
            <div class=\"form-group col-md-4\">
                <label for=\"sppbtgl\">Tanggal</label>
                <div data-date-format=\"dd-mm-yyyy\" data-date=\"";
        // line 69
        echo twig_escape_filter($this->env, (isset($context["today"]) ? $context["today"] : null), "html", null, true);
        echo "\" datepicker class=\"input-group input-append date\">
                    <input id=\"sppbtgl\" name=\"sppbtgl\" readonly=\"\" class=\"form-control\" value=\"";
        // line 70
        echo twig_escape_filter($this->env, (isset($context["today"]) ? $context["today"] : null), "html", null, true);
        echo "\"></input>
                    <span class=\"input-group-addon add-on\"><i class=\"icon-calendar\"></i></span>
                </div>
            </div>
        </div>
        <div class=\"row\">
            <div class=\"form-group col-md-4\">
                <label for=\"nomor\">Permintaan Tanggal Penyerahan</label>
                <div data-date-format=\"dd-mm-yyyy\" data-date=\"";
        // line 78
        echo twig_escape_filter($this->env, (isset($context["today"]) ? $context["today"] : null), "html", null, true);
        echo "\" datepicker class=\"input-group input-append date\">
                    <input id=\"mttgl\" name=\"mttgl\" readonly=\"\" class=\"form-control\" value=\"";
        // line 79
        echo twig_escape_filter($this->env, (isset($context["today"]) ? $context["today"] : null), "html", null, true);
        echo "\"></input>
                    <span class=\"input-group-addon add-on\"><i class=\"icon-calendar\"></i></span>
                </div>
            </div>
            <div class=\"form-group col-md-4\">
                <label for=\"nomor\">Tanggal Diserahkan</label>
                <div data-date-format=\"dd-mm-yyyy\" data-date=\"";
        // line 85
        echo twig_escape_filter($this->env, (isset($context["today"]) ? $context["today"] : null), "html", null, true);
        echo "\" datepicker class=\"input-group input-append date\">
                    <input id=\"srtgl\" name=\"srtgl\" readonly=\"\" class=\"form-control\" value=\"";
        // line 86
        echo twig_escape_filter($this->env, (isset($context["today"]) ? $context["today"] : null), "html", null, true);
        echo "\"></input>
                    <span class=\"input-group-addon add-on\"><i class=\"icon-calendar\"></i></span>
                </div>
            </div>
        </div>
        <div class=\"row\">
            <div class=\"form-group col-md-12\">
                <label for=\"penerima\">Yang Menerima</label>
                <input type=\"text\" class=\"form-control\" id=\"penerima\" placeholder=\"Yang Menerima\">
            </div>
        </div>
        <div class=\"row\">
            <div class=\"form-group col-md-12\">
                <label for=\"untuk\">Untuk</label>
                <input type=\"text\" class=\"form-control\" id=\"untuk\" placeholder=\"Untuk dipergunakan pada...\">
            </div>
        </div>
        <div class=\"row\">
            <div class=\"col-md-12\">
                <label for=\"data\">Daftar Barang</label>
                <table class=\"table table-striped table-bordered\" id=\"data\">
                    <thead>
                        <tr>
                            <th class=\"text-center\">Kode</th>
                            <th class=\"text-center\">Nama Barang</th>
                            <th class=\"text-center\">Qty</th>
                            <th class=\"text-center\">Satuan</th>
                            <!--<th class=\"text-center\">Dari Gudang</th>-->
                            <th class=\"text-center\">Keterangan</th>
                            <th class=\"text-center\"></th>
                        </tr>
                    </thead>
                    <tbody id=\"data-body\">
                        <tr></tr>
                        <tr></tr>
                    </tbody>
                    <tbody>
                        <tr>
                            <td colspan=\"10\">
                                <button type=\"button\" class=\"btn btn-primary pull-right\" data-toggle=\"modal\" data-target=\"#tambah-barang-keluar\"><span class=\"glyphicon glyphicon-plus\"></span> Tambah Barang</button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class=\"row\">
            <div class=\"form-group col-md-12\">
                <label for=\"ket\">Keterangan</label>
                <textarea class=\"form-control\" style=\"width:calc(100% - 27px); height:auto !important; resize:none;\" rows=\"5\" id=\"ket\" placeholder=\"Keterangan Jurnal\"></textarea>
            </div>
        </div>
        <div class=\"row\">
            <div class=\"col-md-12\">
                <div class=\"btn-group pull-right\">
                    <button type=\"button\" class=\"btn btn-primary\" id=\"cetak-sppb\" disabled>
                        <span class=\"glyphicon glyphicon-print\"></span> Cetak SPPB
                    </button>
                    <button type=\"button\" class=\"btn btn-primary\" id=\"cetak-bukti-pengambilan\" disabled>
                        <span class=\"glyphicon glyphicon-print\"></span> Cetak Bukti Pengambilan Barang
                    </button>
                </div>
            </div>
        </div>
        <br/>
        <div class=\"row\">
            <div class=\"col-md-12\">
                <div class=\"btn-group pull-right\">
                    <button type=\"button\" class=\"btn btn-default\" id=\"form-batal\">
                        Batal
                    </button>
                    <button type=\"button\" class=\"btn btn-primary\" id=\"simpan-barang-keluar\">
                        <span class=\"glyphicon glyphicon-floppy-disk\"></span> Simpan
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>
";
    }

    // line 165
    public function block_stylesheets($context, array $blocks = array())
    {
        echo " ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 166
        echo twig_escape_filter($this->env, base_url("assets/css/datepicker3.css"), "html", null, true);
        echo "\" />
<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 167
        echo twig_escape_filter($this->env, base_url("assets/css/select2.css"), "html", null, true);
        echo "\" />
<style type=\"text/css\">
    .select2-selection__rendered {
        height: 28px;
    }
    
    .form-control {
        height: 37px !important;
    }
    
    .searchable {
        width: 100% !important;
    }
    
    .modal {
        text-align: center;
        padding: 0 !important;
    }
    
    .modal:before {
        content: '';
        display: inline-block;
        height: 100%;
        vertical-align: middle;
        margin-right: -4px;
        /* Adjusts for spacing */
    }
    
    .modal-dialog {
        display: inline-block;
        text-align: left;
        vertical-align: middle;
        margin-top: 60px;
    }
    
    .add-on {
        cursor: pointer;
    }
</style>
";
    }

    // line 206
    public function block_scripts($context, array $blocks = array())
    {
        echo " ";
        $this->displayParentBlock("scripts", $context, $blocks);
        echo "
<script type=\"text/javaScript\" src=\"";
        // line 207
        echo twig_escape_filter($this->env, base_url("assets/js/bootstrap-datepicker.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 208
        echo twig_escape_filter($this->env, base_url("assets/js/select2.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 209
        echo twig_escape_filter($this->env, base_url("assets/js/base64.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\">
    jQuery(function(\$){

        var data = [];

        \$(\"*[datepicker]\").datepicker({
            autoclose: true,
            format: \"yyyy-mm-dd\",
            orientation:\"bottom\"
        });
        
        if(\$(\"#juno\").val() != \"\"){ \$(\"#juno\").prop(\"disabled\", true); }

        // var gudang = [];
        // gudangSelect();

        barangSelect();
        
        populateForm();
        
        function suntingDataBarang(index){
            // var gudang \t\t\t= \$(\"#gudang-select\").find(\"option:selected\").attr(\"value\");
            var judid           = data[index].judid;
            var idBarang\t\t= data[index].idBarang;
            var kodeBarang\t\t= data[index].kodeBarang;
            var jumlahBarang \t= -parseFloat(\$(\"#jumlah-barang\").val(),10);
            var keterangan \t\t= \$(\"#keterangan-detail\").val();
            
            var namaBarang \t\t= data[index].namaBarang;
            var satuan \t\t\t= data[index].satuan;

            // console.log();

            data[index] = {
                \"judid\"\t\t\t:judid,
                // \"gudang\"\t\t:gudang,
                \"idBarang\"      :idBarang,
                \"kodeBarang\"\t:kodeBarang,
                \"jumlahBarang\"\t:jumlahBarang,
                \"keterangan\"\t:keterangan,
                \"namaBarang\"\t:namaBarang,
                \"satuan\"\t\t:satuan,
            };
            repopulateDataTable();
        }

        function tambahBarang(){
            // var gudang \t\t\t= \$(\"#gudang-select\").find(\"option:selected\").attr(\"value\");
            var idBarang\t\t= \$(\"#barang-select\").find(\"option:selected\").attr(\"value\");
            var kodeBarang      = \$(\"#barang-select\").find(\"option:selected\").data().data.kode;
            var jumlahBarang \t= -parseFloat(\$(\"#jumlah-barang\").val(),10);
            var keterangan \t\t= \$(\"#keterangan-detail\").val();
            
            var namaBarang \t\t= \$(\"#barang-select\").find(\"option:selected\").text();
            var satuan \t\t\t= \$(\"#barang-select\").find(\"option:selected\").data().data.satuan;

            // console.log();

            data.push({
                \"judid\"\t\t\t:-1,
                // \"gudang\"\t\t:gudang,
                \"idBarang\"      :idBarang,
                \"kodeBarang\"\t:kodeBarang,
                \"jumlahBarang\"\t:jumlahBarang,
                \"keterangan\"\t:keterangan,
                \"namaBarang\"\t:namaBarang,
                \"satuan\"\t\t:satuan,
            });
            repopulateDataTable();
        }

        function kurangiBarang(index){
            data.splice(index,1);
            repopulateDataTable();
        }

        function repopulateDataTable(){
            \$(\"#data-body\").empty();
            \$(\"#data-body\").append(\"<tr></tr><tr></tr>\");
            data.forEach(function(value, index){
                \$(\"#data-body\").append(
                    \"<tr>\"+
                        \"<td class='text-left'>\"+value['kodeBarang'] +\"</td>\"+
                        \"<td class='text-left'>\"+value['namaBarang']+\"</td>\"+
                        \"<td class='text-right'>\"+(-value['jumlahBarang'])+\"</td>\"+
                        \"<td class='text-left'>\"+value['satuan']+\"</td>\"+
                        // \"<td class='text-left'>\"+value['gudang']+\"</td>\"+
                        \"<td class='text-left'>\"+value['keterangan']+\"</td>\"+
                        \"<td class='text-center'>\"+
                            \"<a href='' id='edit_\" + index + \"' value='\" + index + \"'><span class='glyphicon glyphicon-pencil'></span></a>\"+
                            \" / \"+
                            \"<a href='' id='delete_\" + index + \"' value='\" + index + \"'><span class='glyphicon glyphicon-trash'></span></a>\"+
                        \"</td>\"+
                    \"</tr>\"
                );

                \$('#edit_'+index).on(\"click\", function(e){
                    e.preventDefault();
                    suntingBarang(index);
                });

                \$('#delete_'+index).on(\"click\", function(e){
                    e.preventDefault();
                    kurangiBarang(index);
                });
            });
        }

        // function gudangSelect(){
        //     \$(\"#gudang-select\").empty();
        //     \$(\"#gudang-select\").append(\"<option></option>\");
        //     \$.post(\"";
        // line 321
        echo twig_escape_filter($this->env, site_url("persediaan/get_daftar_gudang"), "html", null, true);
        echo "\", {nomor_unit:\"";
        echo twig_escape_filter($this->env, (isset($context["skpd"]) ? $context["skpd"] : null), "html", null, true);
        echo "\"}, function(json){
        //         \$(\"#gudang-select\").select2({
        //             placeholder : \"Pilih gudang...\",
        //             data : json.results,
        //         });
        //     }, \"json\");
        // }

        function barangSelect(){
            pagingLimit = 10;
            \$(\"#barang-select\").empty();\$(\"#barang-select\").append(\"<option></option>\");
            \$(\"#barang-select\").select2({
                placeholder : \"Pilih barang...\",
                ajax: {
                    url:\"";
        // line 335
        echo twig_escape_filter($this->env, site_url("persediaan/get_daftar_barang_ada_stok"), "html", null, true);
        echo "\",
                    type:\"post\",
                    dataType:\"json\",
                    delay:500,
                    data:function(params){
                        return{
                            tanggal_jurnal  : \$(\"#bktgl\").val(),
                            skpd            : \"";
        // line 342
        echo twig_escape_filter($this->env, (isset($context["skpd"]) ? $context["skpd"] : null), "html", null, true);
        echo "\",
                            //gudang: \$(\"#gudang-select\").find(\"option:selected\").attr(\"value\"),
                            limit           : pagingLimit,
                            barang          : params.term || \"\",
                            page            : params.page || 1
                        }
                    },
                    processResults: function (data, params) {
                        // parse the results into the format expected by Select2
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data, except to indicate that infinite
                        // scrolling can be used
                        params.page = params.page || 1;

                        return {
                            results     : data.results,
                            pagination  : {
                            more        : (params.page * pagingLimit) < data.total_count
                            }
                        };
                    },
                },
                
                escapeMarkup:function(markup){
                    return markup;
                },
                
                templateResult:function(repo){
                    if (repo.loading) return repo.text;
                    var markup = \"<table style='width:100%;'><tr>\" +
                        \"<td><div style='font-weight:bold;'>\"+ repo.text +\"</div>\";
                    if (repo.description) {
                        markup += \"<div><small>\" + repo.description + \"</small></div>\";
                    }
                    
                    markup+=\"</td><td style='text-align:right;'>\"+
                        \"<div style='text-align:right;'>\"+
                        repo.stok + \" \" + repo.satuan +
                        \"</div></td>\";
                    
                    markup+=\"</tr></table></div>\";
                    
                    return markup;
                },
                templateSelection:function(repo){
                    return repo.text || repo.description || repo.id;
                },
            });
        }
        
        function suntingBarang(index){
            \$(\"#barang-select\").select2(\"destroy\");
            \$(\"#barang-select\").hide();
                
            \$(\"button[name=simpan]\").text(\"Simpan\");
            
            \$(\"#barang-input\").val(data[index].namaBarang);
            \$(\"#barang-input\").show();
            \$(\"#barang-input\").prop(\"disabled\", true);
            \$(\"#barang-input\").attr(\"data-index\", index);
            
            \$.post(\"";
        // line 403
        echo twig_escape_filter($this->env, site_url("persediaan/get_stok_barang"), "html", null, true);
        echo "\", {skpd:\"";
        echo twig_escape_filter($this->env, (isset($context["skpd"]) ? $context["skpd"] : null), "html", null, true);
        echo "\", kbid:data[index].idBarang, tanggal_jurnal:\$(\"#bktgl\").val()}, function(json){
                var avail = parseFloat(json) + (-parseFloat(data[index].jumlahBarang));
                \$('#jumlah-barang').attr({
                    \"max\" : avail,
                    \"min\" : 0,
                });
            } ,\"json\");
            
            \$(\"#jumlah-barang\").val(-data[index].jumlahBarang);
            \$(\"#keteranga-detail\").val(data[index].keterangan);
            
            \$(\"#tambah-barang-keluar\").modal(\"show\");
        }

        function resetAddDataModal(){
            \$(\"#tambah-barang-keluar-form\").trigger(\"reset\");
            \$(\".alert\").remove();
            barangSelect();
                
            \$(\"button[name=simpan]\").text(\"Tambah\");
            
            \$(\"#barang-input\").val(\"\");
            \$(\"#barang-input\").hide();
            \$(\"#barang-input\").attr(\"data-index\", \"\");
            // gudangSelect();
        }

        function activatePrintButton(){
            //\$(\"#cetak-sppb\").prop(\"disabled\", false);
            \$(\"#cetak-sppb\").removeAttr(\"disabled\");
            \$(\"#cetak-bukti-pengambilan\").removeAttr(\"disabled\");
            //\$(\"#cetak-bukti-pengambilan\").prop(\"disabled\", false);

            \$(\"#cetak-sppb\").on('click', function(){
                \$(\"#persiapan-cetak-sppb\").modal('show');
            });
            
            \$(\"#lakukan-cetak-sppb\").on('click', function(){
                var namaunit = Base64.encode(\"";
        // line 441
        echo twig_escape_filter($this->env, (isset($context["oname"]) ? $context["oname"] : null), "html", null, true);
        echo "\");
                var alamatunit = Base64.encode(\$(\"#alamat-unit\").val());
                var skpd = Base64.encode(\"";
        // line 443
        echo twig_escape_filter($this->env, (isset($context["skpd"]) ? $context["skpd"] : null), "html", null, true);
        echo "\");
                var juno = Base64.encode(\$(\"#juno\").val());
                var tanggal_jurnal = Base64.encode(\$(\"#sppbtgl\").val());
                var jenis = Base64.encode(\"1\");
                var nomor_spbb = Base64.encode(\$(\"#sppb\").val());
                var dari = Base64.encode(\$(\"#dari\").val());
                var kepada = Base64.encode(\$(\"#kepada\").val());
                var alamat_kepada = Base64.encode(\$(\"#alamat-kepada\").val());
                var dasar_no = Base64.encode(\$(\"#nota_dinas\").val());
                var dasar_tgl = Base64.encode(\$(\"#bptgl\").val());
                var tanggal = Base64.encode(\$(\"#sppbtgl\").val());
                var pengurusbarang = Base64.encode(\$(\"#nama-pengurus\").val());
                var nippengurusbarang = Base64.encode(\$(\"#nip-pengurus\").val());
                
                console.log(tanggal_jurnal);
                
                if( namaunit === \"\" ) { namaunit = \"-\"; }
                if( alamatunit === \"\" ) { alamatunit = \"-\"; }
                if( skpd === \"\" ) { skpd = \"-\"; }
                if( juno === \"\" ) { juno = \"-\"; }
                if( tanggal_jurnal === \"\" ) { tanggal_jurnal = \"-\"; }
                if( jenis === \"\" ) { jenis = \"-\"; }
                if( nomor_spbb === \"\" ) { nomor_spbb = \"-\"; }
                if( dari === \"\" ) { dari = \"-\"; }
                if( kepada === \"\" ) { kepada = \"-\"; }
                if( alamat_kepada === \"\" ) { alamat_kepada = \"-\"; }
                if( dasar_no === \"\" ) { dasar_no = \"-\"; }
                if( dasar_tgl === \"\" ) { dasar_tgl = \"-\"; }
                if( tanggal === \"\" ) { tanggal = \"-\"; }
                if( pengurusbarang === \"\" ) { pengurusbarang = \"-\"; }
                if( nippengurusbarang === \"\" ) { nippengurusbarang = \"-\"; }
                
                window.open(\"";
        // line 475
        echo twig_escape_filter($this->env, site_url("persediaan/print_sppb"), "html", null, true);
        echo "/\"+ namaunit + \"/\" + alamatunit + \"/\"+ skpd + \"/\" + juno + \"/\" + tanggal_jurnal + \"/\" + jenis + \"/\" + nomor_spbb + \"/\" + dari + \"/\" + kepada + \"/\" + alamat_kepada + \"/\" + dasar_no + \"/\" + dasar_tgl + \"/\" + tanggal + \"/\" + pengurusbarang + \"/\" + nippengurusbarang, \"_blank\");
                //tambah organisasi, nama, jabatan, dan NIP dari pengurus gudang dan atasan langsung
            });
            
            \$(\"#cetak-bukti-pengambilan\").on(\"click\", function(){
                \$(\"#persiapan-cetak\").modal('show');
            });
            
            \$(\"#lakukan-cetak\").on('click', function(){
                var juno = Base64.encode(\$(\"#juno\").val());
                var skpd = Base64.encode(\"";
        // line 485
        echo twig_escape_filter($this->env, (isset($context["skpd"]) ? $context["skpd"] : null), "html", null, true);
        echo "\");
                var tanggal = Base64.encode(\$(\"#juno\").val());
                var namaunit = Base64.encode(\"";
        // line 487
        echo twig_escape_filter($this->env, (isset($context["oname"]) ? $context["oname"] : null), "html", null, true);
        echo "\");
                var pengurusbarang = Base64.encode(\$(\"#nama-petugas\").val());
                var nippengurusbarang = Base64.encode(\$(\"#nip-petugas\").val());
                var atasanlangsung = Base64.encode(\$(\"#nama-atasan\").val());
                var nipatasanlangsung = Base64.encode(\$(\"#nip-atasan\").val());
                var penerima = Base64.encode(\$(\"#nama-pengambil\").val());
                var nippenerima = Base64.encode(\$(\"#nip-pengambil\").val());
                
                if(juno === \"\") {juno = \"-\"}
                if(skpd === \"\") {skpd = \"-\"}
                if(tanggal === \"\") {tanggal = \"-\"}
                if(namaunit === \"\") {namaunit = \"-\"}
                if(pengurusbarang === \"\") {pengurusbarang = \"-\"}
                if(nippengurusbarang === \"\") {nippengurusbarang = \"-\"}
                if(atasanlangsung === \"\") {atasanlangsung = \"-\"}
                if(nipatasanlangsung === \"\") {nipatasanlangsung = \"-\"}
                if(penerima === \"\") {penerima = \"-\"}
                if(nippenerima === \"\") {nippenerima = \"-\"}
                
                window.open(\"";
        // line 506
        echo twig_escape_filter($this->env, site_url("persediaan/print_bukti_pengambilan_barang"), "html", null, true);
        echo "/\"+ namaunit + \"/\" + skpd + \"/\" + juno + \"/\" + tanggal + \"/\" + pengurusbarang + \"/\" + nippengurusbarang + \"/\" + penerima + \"/\" + nippenerima + \"/\" + atasanlangsung + \"/\" + nipatasanlangsung, \"_blank\");
            });
        }
        
        function populateForm(){
            \$.post(\"";
        // line 511
        echo twig_escape_filter($this->env, site_url("persediaan/juno_check"), "html", null, true);
        echo "\", {juno:\$(\"#juno\").val(), nomor_unit:\"";
        echo twig_escape_filter($this->env, (isset($context["skpd"]) ? $context["skpd"] : null), "html", null, true);
        echo "\", jenis:\"1\"}, function(json){
                // console.log(json);
                if(json.data != null){
                    \$(\"#bktgl\").val(json.data[0].jutgl);
                    \$(\"#mttgl\").val(json.data[0].juestimasitgl);
                    \$(\"#srtgl\").val(json.data[0].jutgl);
                    \$(\"#bidang\").val(json.data[0].spnama);
                    \$(\"#penerima\").val(json.data[0].sppenanggungjawab);
                    \$(\"#nota_dinas\").val(json.data[0].judokumenno);
                    \$(\"#bptgl\").val(json.data[0].judokumentgl);
                    \$(\"#sppb\").val(json.data[0].judasarno);
                    \$(\"#sppbtgl\").val(json.data[0].judasartgl);
                    \$(\"#untuk\").val(json.data[0].untuk);
                    \$(\"#ket\").val(json.data[0].keterangan);

                    data.splice(0, data.length);

                    json.data_table.forEach(function(item, index){

                        data.push({
                            \"judid\"\t\t\t:item.judid,
                            // \"gudang\"\t\t:item.guno,
                            \"idBarang\"      :item.kbid,
                            \"kodeBarang\"\t:item.kbkode,
                            \"jumlahBarang\"\t:parseFloat(item.judqty),
                            \"keterangan\"\t:item.keterangan,
                            \"namaBarang\"\t:item.namabarang,
                            \"satuan\"\t\t:item.judunit,
                        });
                    });

                    repopulateDataTable();
                    activatePrintButton();
                }
                else{
                    // temporary
                    data.splice(0,data.length);
                    repopulateDataTable();
                }
            },\"json\");
        }

        \$(\"button[name=simpan]\").on(\"click\", function(){
            if((\$(\"#barang-select\").prop(\"disabled\") == true && \$(\"#barang-select\").val() === \"\") || \$(\"#jumlah-barang\").val() === \"\" || \$(\"#harga-satuan\").val() === \"\" || \$(\"#tahun-buat\").val() === \"\")
            {
                \$(\"#tambah-barang-keluar-form\").prepend(\"<div class='alert alert-danger'>Isian dengan tanda (*) harus diisi.</div>\");
                \$(\"#tambah-barang-keluar\").scrollTop(0);
            }
            else
            {
                console.log(\$(\"#barang-input\").is(\":hidden\"));
                if(\$(\"#barang-input\").is(\":hidden\")){
                    tambahBarang();
                }
                else{
                    suntingDataBarang(\$(\"#barang-input\").attr(\"data-index\"));
                }
                \$(\"#tambah-barang-keluar\").modal(\"hide\");
                resetAddDataModal();
            }
        });

        \$(\"button[name=batal]\").on(\"click\", function(){
            resetAddDataModal();
        });

        \$('#tambah-barang-keluar').on('hide.bs.modal', function () {
            \$(\"#tambah-barang-keluar\").scrollTop(0);
            resetAddDataModal();
        });
        
        \$(\"#form-batal\").on(\"click\", function(){
            window.close();
        });

        \$(\"#simpan-barang-keluar\").on(\"click\", function(){
            if(\$(\"#juno\").val() === \"\" || \$(\"#nota_dinas\").val() === \"\")
            {
                \$(\"#alert-placement\").empty();
                \$(\"#alert-placement\").prepend(\"<div class='alert alert-danger'>Isian dengan tanda (*) harus diisi.</div>\");
                \$(window).scrollTop(0);
            }
            else
            {
                // prepare data
                var jurnal;

                var nomorJurnal\t\t\t\t= \$(\"#juno\").val();
                var tanggalJurnal \t\t\t= \$(\"#bktgl\").val();
                var tanggalEstimasi \t\t= \$(\"#mttgl\").val();
                var tanggalRealisasi \t\t= \$(\"#srtgl\").val();
                var bidang \t\t\t\t\t= \$(\"#bidang\").val();
                var penanggungJawab \t\t= \$(\"#penerima\").val();
                var dokumenPenyerta \t\t= \$(\"#nota_dinas\").val();
                var tanggalDokumenPenyerta \t= \$(\"#bptgl\").val();
                var dasarHukum \t\t\t\t= \$(\"#sppb\").val();
                var tanggalDasarHukum \t\t= \$(\"#sppbtgl\").val();
                var skpd\t\t\t\t\t= \"";
        // line 608
        echo twig_escape_filter($this->env, (isset($context["skpd"]) ? $context["skpd"] : null), "html", null, true);
        echo "\";
                var untuk \t\t\t\t\t= \$(\"#untuk\").val();
                var keterangan \t\t\t\t= \$(\"#ket\").val();

                jurnal = {
                    \"nomorJurnal\"\t\t\t\t: nomorJurnal,
                    \"tanggalJurnal\" \t\t\t: tanggalJurnal,
                    \"tanggalEstimasi\" \t\t\t: tanggalEstimasi,
                    \"tanggalRealisasi\" \t\t\t: tanggalRealisasi,
                    \"bidang\" \t\t\t\t\t: bidang,
                    \"penanggungJawab\" \t\t\t: penanggungJawab,
                    \"dokumenPenyerta\"\t\t\t: dokumenPenyerta,
                    \"tanggalDokumenPenyerta\" \t: tanggalDokumenPenyerta,
                    \"dasarHukum\" \t\t\t\t: dasarHukum,
                    \"tanggalDasarHukum\" \t\t: tanggalDasarHukum,
                    \"skpd\" \t\t\t\t\t\t: skpd,
                    \"untuk\"\t\t\t\t\t\t: untuk,
                    \"keterangan\" \t\t\t\t: keterangan,
                };

                \$.post(\"";
        // line 628
        echo twig_escape_filter($this->env, site_url("persediaan/simpan_barang_keluar"), "html", null, true);
        echo "\",{ju:jurnal, jud:data},function(json){
                    // console.log(json);
                    json.forEach(function(item, index){
                        data[index][\"judid\"] = item;
                    });
                    // window.location.href = \"";
        // line 633
        echo twig_escape_filter($this->env, site_url("persediaan/index"), "html", null, true);
        echo "\";
                    //window.close();
                    activatePrintButton();
                },\"json\");
            }
        });

        \$(\"#juno\").on(\"blur\", function(){
            populateForm();
        });
        
        \$('#barang-select').on('change', function(){
            \$('#jumlah-barang').attr({
                \"max\" : \$(this).find(\"option:selected\").data().data.stok,
                \"min\" : 0,
            });
            \$('#jumlah-barang').val(\"0\");
        });
        
        \$(\"#jumlah-barang\").on('blur', function(){
            if(parseFloat(\$(this).val()) > parseFloat(\$(this).attr(\"max\")))
            {
                \$(this).val(\$(this).attr(\"max\"));
            }
            else if(parseFloat(\$(this).val()) < parseFloat(\$(this).attr(\"min\")))
            {
                \$(this).val(\$(this).attr(\"min\"));
            }
        });

        \$('#tambah-barang-keluar').on('hide.bs.modal', function () {
            \$(\"#tambah-barang-keluar\").scrollTop(0);
        });
        
        \$(\"#edit-nomor\").on(\"click\", function(){
            \$(\"#nomor-baru\").val(\$(\"#juno\").val());
            \$(\"#kelola-jurnal\").modal(\"show\");
        });
        
        \$(\"#hapus-jurnal\").on(\"click\", function(){
            \$.post(\"";
        // line 673
        echo twig_escape_filter($this->env, site_url("persediaan/delete_jurnal"), "html", null, true);
        echo "\", {juno:\$(\"#juno\").val()}, function(json){
                console.log(json);
                \$(\"#kelola-jurnal\").modal(\"hide\");
                window.location.href = \"";
        // line 676
        echo twig_escape_filter($this->env, site_url("persediaan/buku_barang_pakai_habis"), "html", null, true);
        echo "\";
            }, \"json\");
        });
    });

</script>
";
    }

    public function getTemplateName()
    {
        return "simgo/persediaan/barang_keluar.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  835 => 676,  829 => 673,  786 => 633,  778 => 628,  755 => 608,  653 => 511,  645 => 506,  623 => 487,  618 => 485,  605 => 475,  570 => 443,  565 => 441,  522 => 403,  458 => 342,  448 => 335,  429 => 321,  314 => 209,  310 => 208,  306 => 207,  299 => 206,  255 => 167,  251 => 166,  244 => 165,  160 => 86,  156 => 85,  147 => 79,  143 => 78,  132 => 70,  128 => 69,  113 => 57,  109 => 56,  86 => 36,  74 => 27,  70 => 26,  51 => 10,  43 => 4,  40 => 3,  35 => 2,  30 => 1,);
    }
}
