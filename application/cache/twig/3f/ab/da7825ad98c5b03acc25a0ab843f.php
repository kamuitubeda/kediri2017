<?php

/* aset/reklas.html */
class __TwigTemplate_3fabda7825ad98c5b03acc25a0ab843f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("_templates/adminpage.html");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'scripts' => array($this, 'block_scripts'),
            'stylesheets' => array($this, 'block_stylesheets'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_templates/adminpage.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        $this->env->loadTemplate("aset/modal_tambah_nilai.html")->display($context);
        // line 5
        echo "
<div class=\"row\">
\t<div class=\"col-xs-12\" style=\"padding-left:0px; padding-right:0px;\">
\t\t<div class=\"row\">
\t\t\t<div class=\"col-xs-12\">
\t\t\t\t<p class=\"bg-danger\" style=\"height:20px; padding:10px; font-weight:bold;\">Back up database dulu sebelum melakukan reklas!</p>
\t\t\t</div>
\t\t\t<div class=\"col-xs-12\">
\t\t\t\t<div class=\"col-xs-4\">
\t\t\t\t\t<label>SKPD</label>
\t\t\t\t\t<select type=\"text\" name=\"nomor_sub_unit\" class=\"form-control\">
\t\t\t\t\t\t<option value=\"\">Pilih SKPD</option>
\t\t\t\t\t</select>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-xs-2\">
\t\t\t\t\t<button id=\"do_reklas\" name=\"do_reklas\" type=\"button\" class=\"btn btn-success\" style=\"float:left;\">Lakukan Reklas</button>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
</div>
";
    }

    // line 29
    public function block_scripts($context, array $blocks = array())
    {
        // line 30
        $this->displayParentBlock("scripts", $context, $blocks);
        echo "
<script type=\"text/javaScript\" src=\"";
        // line 31
        echo twig_escape_filter($this->env, base_url("assets/js/jquery.loadmask.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 32
        echo twig_escape_filter($this->env, base_url("assets/js/select2.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 33
        echo twig_escape_filter($this->env, base_url("assets/js/accounting.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 34
        echo twig_escape_filter($this->env, base_url("assets/js/jquery.validate.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\">
    jQuery(document).ready(function(\$) {
    \tvar content = \$(\"#content\");

    \t// configure setting accounting.js
\t\taccounting.settings = {
\t\t\tnumber: {
\t\t\t\tprecision : 0,  // default precision on numbers is 0
\t\t\t\tthousand: \".\",
\t\t\t\tdecimal : \",\"
\t\t\t}
\t\t}

\t\tvar n = function(v, p) {
\t\t\treturn accounting.formatNumber(v, p);
\t\t}

\t\tvar u = function(v) {
\t\t\treturn accounting.unformat(v);
\t\t}

\t\t//menampilkan daftar skpd
\t\t\$.get(\"";
        // line 57
        echo twig_escape_filter($this->env, site_url("aset/list_sub_unit_reklas"), "html", null, true);
        echo "\", {}, function(json){ 
\t\t\tvar listSkpd = json.data;
\t\t\t
\t\t\t\$.each(listSkpd, function (key, value) {
\t\t\t\t\$(\"select[name=nomor_sub_unit]\").append(\$('<option></option>').val(value.nomor_sub_unit).html(value.nama_sub_unit));
\t\t\t});

\t\t\t//\$(\"select[name=nomor_sub_unit]\").val(\"";
        // line 64
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "nomor_sub_unit"), "html", null, true);
        echo "\");
\t\t\t\$(\"select[name=nomor_sub_unit]\").select2();

\t\t}, \"json\");

\t\t\$(\"button[name=do_reklas]\").on(\"click\", function() {
\t\t\t\$(\"body\").mask(\"Silakan tunggu. Proses reklas membutuhkan waktu 2-5 menit.\");
\t\t\tvar nomor_sub_unit = \$(\"select[name=nomor_sub_unit]\").val();
\t\t\t\t\t\t
\t\t\t\$.post(\"";
        // line 73
        echo twig_escape_filter($this->env, site_url("aset/do_reklas"), "html", null, true);
        echo "\", {nomor_sub_unit:nomor_sub_unit}, function(json){ 
\t\t\t\t\$(\"body\").unmask();
\t\t\t}, \"json\");
\t\t});

\t});
</script>
";
    }

    // line 83
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 84
        echo "\t";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 85
        echo twig_escape_filter($this->env, base_url("assets/css/devexpress-like/jquery-ui.css"), "html", null, true);
        echo "\" />
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 86
        echo twig_escape_filter($this->env, base_url("assets/css/select2.min.css"), "html", null, true);
        echo "\" />
\t<link href=\"";
        // line 87
        echo twig_escape_filter($this->env, base_url("assets/css/jquery.loadmask.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">\t\t

\t<style type=\"text/css\">
\t\t.control-label{
\t\t\tpadding-top: 8px !important;
\t\t}

\t\tselect.form-control {
\t\t\theight: 48px !important;
\t\t}

\t\tinput.form-control {
\t\t\theight: 34px !important;
\t\t}

\t\ttextarea.form-control {
\t\t\theight:auto !important;
\t\t}

\t\t.row {
\t\t\tpadding:10px 0px;
\t\t}

\t\t.spacer {
\t\t    margin-top: 20px;
\t\t}

\t\t.ui-autocomplete{
\t\t\tbackground: none repeat scroll 0 0 white;
\t\t}

\t\t.input-group-addon{
\t\t\tborder-radius: 0 !important;
\t\t}

\t\t.input-group .form-control{
\t\t\tz-index: 0; !important;
\t\t}

\t\t.select2-selection__rendered {
\t\t\theight: 28px;
\t\t}
\t\t.select2-container {
\t\t\twidth: 100% !important;
\t\t\tpadding: 0;
\t\t}
\t\t#btnTambahRenov {
\t\t\tmargin-top: 23px;
\t\t\theight: 30px;
\t\t}
\t\t#do_reklas {
\t\t\tmargin-top: 23px;
\t\t\theight: 30px;
\t\t}
\t</style>
";
    }

    public function getTemplateName()
    {
        return "aset/reklas.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  155 => 87,  151 => 86,  147 => 85,  142 => 84,  139 => 83,  127 => 73,  115 => 64,  105 => 57,  79 => 34,  75 => 33,  71 => 32,  67 => 31,  63 => 30,  60 => 29,  35 => 5,  33 => 4,  30 => 3,);
    }
}
