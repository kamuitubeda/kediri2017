<?php

/* aset/penambahan_nilai.html */
class __TwigTemplate_485dcdfa8fee328527785b9ebaae7b72 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("_templates/adminpage.html");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'scripts' => array($this, 'block_scripts'),
            'stylesheets' => array($this, 'block_stylesheets'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_templates/adminpage.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        $this->env->loadTemplate("aset/modal_tambah_nilai.html")->display($context);
        // line 5
        echo "
<div class=\"row\">
\t<div class=\"col-xs-12\" style=\"padding-left:0px; padding-right:0px;\">
\t\t<div class=\"row\">
\t\t\t<div class=\"col-xs-12\">
\t\t\t\t<div class=\"col-xs-4\">
\t\t\t\t\t<label>SKPD</label>
\t\t\t\t\t<select type=\"text\" name=\"nomor_unit\" class=\"form-control\">
\t\t\t\t\t\t<option value=\"\">Pilih SKPD</option>
\t\t\t\t\t</select>
\t\t\t\t</div>
\t\t\t\t<div class=\"col-xs-8\">
\t\t\t\t\t<label>Aset</label>
\t\t\t\t\t<select type=\"text\" name=\"aset_induk\" class=\"form-control\">
\t\t\t\t\t\t<option value=\"\">Pilih Aset</option>
\t\t\t\t\t</select>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"row\" name=\"list_aset_tambah\" style=\"display: none;\">
\t\t\t<div class=\"col-xs-12\">
\t\t\t\t<div class=\"col-xs-12\">
\t\t\t\t\t<div class=\"table-responsive\">
\t\t\t\t\t<label>Daftar Renovasi</label>
\t\t\t\t\t\t<table class=\"table table_tambah_nilai\">
\t\t\t\t\t\t\t<thead class=\"table-bordered\">
\t\t\t\t\t\t\t\t<tr>
\t\t\t\t\t\t\t\t\t<th class=\"table-bordered\" style=\"text-align:center\">No Register Renovasi</th>
\t\t\t\t\t\t\t\t\t<th class=\"table-bordered\" style=\"text-align:center\">Detail Renovasi</th>
\t\t\t\t\t\t\t\t\t<th class=\"table-bordered\" style=\"text-align:center\">Tahun Renovasi</th>
\t\t\t\t\t\t\t\t\t<th class=\"table-bordered\" style=\"text-align:center\">Nilai Renovasi</th>
\t\t\t\t\t\t\t\t\t<th></th>
\t\t\t\t\t\t\t\t</tr>
\t\t\t\t\t\t\t</thead>
\t\t\t\t\t\t\t<tbody>
\t\t\t\t\t\t\t</tbody>
\t\t\t\t\t\t</table>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"row\"  style=\"float:right;\">
\t\t\t\t\t\t<button id=\"btnPrintTambahNilai\" type=\"button\" style=\"height:30px;\" class=\"btn btn-primary\"><span class=\"glyphicon glyphicon-print\"></span></button>
\t\t\t\t\t\t<button id=\"btnTambahRenov\" type=\"button\" style=\"height:30px;\" class=\"btn btn-primary\"><span class=\"glyphicon glyphicon-plus\"></span></button>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
</div>
";
    }

    // line 55
    public function block_scripts($context, array $blocks = array())
    {
        // line 56
        $this->displayParentBlock("scripts", $context, $blocks);
        echo "
<script type=\"text/javaScript\" src=\"";
        // line 57
        echo twig_escape_filter($this->env, base_url("assets/js/jquery.loadmask.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 58
        echo twig_escape_filter($this->env, base_url("assets/js/select2.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 59
        echo twig_escape_filter($this->env, base_url("assets/js/accounting.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 60
        echo twig_escape_filter($this->env, base_url("assets/js/jquery.validate.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\">
    jQuery(document).ready(function(\$) {
    \tvar content = \$(\"#content\");

    \t// configure setting accounting.js
\t\taccounting.settings = {
\t\t\tnumber: {
\t\t\t\tprecision : 0,  // default precision on numbers is 0
\t\t\t\tthousand: \".\",
\t\t\t\tdecimal : \",\"
\t\t\t}
\t\t}

\t\tvar n = function(v, p) {
\t\t\treturn accounting.formatNumber(v, p);
\t\t}

\t\tvar u = function(v) {
\t\t\treturn accounting.unformat(v);
\t\t}

\t\t//menampilkan daftar skpd
\t\t\$.get(\"";
        // line 83
        echo twig_escape_filter($this->env, site_url("aset/list_skpd"), "html", null, true);
        echo "\", {}, function(json){ 
\t\t\tvar listSkpd = json.result;
\t\t\t
\t\t\t\$.each(listSkpd, function (key, value) {
\t\t\t\t\$(\"select[name=nomor_unit]\").append(\$('<option></option>').val(value.NOMOR_UNIT).html(value.NAMA_UNIT));
\t\t\t});

\t\t\t//\$(\"select[name=nomor_unit]\").val(\"";
        // line 90
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "NOMOR_UNIT"), "html", null, true);
        echo "\");
\t\t\t\$(\"select[name=nomor_unit]\").select2();

\t\t\t//reload_skpd();
\t\t\t// var user = \$(\"#user\").text();

\t\t\t// if (user != 'Administrator') {
\t\t\t// \t\$.post(\"";
        // line 97
        echo twig_escape_filter($this->env, site_url("aset/get_skpd_user"), "html", null, true);
        echo "\", {user:user}, function(json){ 
\t\t\t// \t\tvar skpd_user = json.data;
\t\t\t// \t\tvar oid = skpd_user.oid;
\t\t\t// \t\t//console.log(skpd_user);

\t\t\t// \t\t\$.post(\"";
        // line 102
        echo twig_escape_filter($this->env, site_url("aset/get_skpd_organisasi"), "html", null, true);
        echo "\", {oid:oid}, function(json){ 
\t\t\t// \t\t\tvar skpd_organisasi = json.data;
\t\t\t// \t\t\tvar skpd_name = skpd_organisasi.oname;
\t\t\t\t\t\t
\t\t\t// \t\t\t\$.post(\"";
        // line 106
        echo twig_escape_filter($this->env, site_url("aset/get_skpd_val"), "html", null, true);
        echo "\", {skpd_name:skpd_name}, function(json){ 
\t\t\t// \t\t\t\tvar skpd_val = json.data;
\t\t\t
\t\t\t// \t\t\t\t//untuk mengubah data select 2 jangan lupa tambahkan ini >> .trigger('change');
\t\t\t// \t\t\t\t\$('select[name=\"nomor_unit\"]').val(skpd_val.NOMOR_UNIT).trigger('change');
\t\t\t// \t\t\t\t\$('select[name=\"nomor_unit\"]').prop( \"disabled\", true );           
\t\t\t\t\t\t\t
\t\t\t// \t\t\t}, \"json\");
\t\t\t\t\t\t
\t\t\t\t\t\t
\t\t\t// \t\t}, \"json\");
\t\t\t// \t}, \"json\");
\t\t\t// }

\t\t}, \"json\");

\t\t// event onchange select skpd
\t\t\$(\"select[name=nomor_unit]\").change(function() {
\t\t\treload_aset();
\t\t});

\t\tvar reload_aset = function(callback) {
\t\t\tcontent.mask(\"Loading...\");
\t\t\tvar nomor_unit = \$(\"select[name=nomor_unit]\").val();

\t\t\t\$.post(\"";
        // line 131
        echo twig_escape_filter($this->env, site_url("aset/get_list_aset_cd_by_nomor_unit"), "html", null, true);
        echo "\", {
\t\t\t\tnomor_unit: nomor_unit
\t\t\t}, function(json){ 
\t\t\t\tvar data = json.data;
\t\t\t\tvar el = \$(\"select[name=aset_induk]\");
\t\t\t\tvar el2 = \$(\"select[name=aset-renov-id]\");

\t\t\t\tel.empty();
\t\t\t\tel.append(\"<option value=''>Pilih Aset</option>\");
\t\t\t\tfor (var i=0; i<data.length; i++) {
\t\t\t\t\tvar k = data[i];
\t\t\t\t\tel.append(\$(\"<option></option>\").attr(\"value\", k.NO_REGISTER).html(k.NO_REGISTER + \" | \" + k.NAMA_BARANG));
\t\t\t\t\t// el.append(\"<option value='{0}'>{1}</option>\".format(k.NO_REGISTER, k.NAMA_BARANG));
\t\t\t\t}

\t\t\t\tel2.empty();
\t\t\t\tel2.append(\"<option value=''>Pilih Aset</option>\");
\t\t\t\tfor (var i=0; i<data.length; i++) {
\t\t\t\t\tvar k = data[i];
\t\t\t\t\tel2.append(\$(\"<option></option>\").attr(\"value\", k.NO_REGISTER).html(k.NO_REGISTER + \" | \" + k.NAMA_BARANG));
\t\t\t\t\t// el2.append(\"<option value='{0}'>{1}</option>\".format(k.NO_REGISTER, k.NAMA_BARANG));
\t\t\t\t}

\t\t\t\tel.select2();
\t\t\t\tel2.select2();

\t\t\t\tcontent.unmask();
\t\t\t\tif (callback !== undefined) {
\t\t\t\t\tcallback();
\t\t\t\t}
\t\t\t}, \"json\");
\t\t}

\t\t//menampilkan daftar aset renov dari aset induk
\t\t\$(\"select[name=aset_induk]\").change(function() {
\t\t\tload_aset_renov();
\t\t});

\t\tfunction load_aset_renov() {
\t\t\tvar aset_induk = \$(\"select[name=aset_induk]\").val();

\t\t\t\$('tbody').empty();
    \t\t\$(\"body\").mask(\"Processing...\");

    \t\t\$('div[name=\"list_aset_tambah\"]').show();

    \t\t\$.post(\"";
        // line 177
        echo twig_escape_filter($this->env, site_url("aset/list_aset_tambah"), "html", null, true);
        echo "\", {aset_induk:aset_induk}, function(json) {
\t\t\t\tvar aset_tambah = json.aset_tambah;
\t\t\t\tvar html = '';

\t\t\t\t\$.each(aset_tambah, function(key, value) {
\t\t\t\t\tvar value = aset_tambah[key];

\t\t\t\t\thtml += '<tr><td>'
\t\t\t\t\t+ value.pnid
\t\t\t\t\t+ '</td><td>'
\t\t\t\t\t+ value.namapn
\t\t\t\t\t+ '</td><td align=\"center\">'
\t\t\t\t\t+ value.tahunpn
\t\t\t\t\t+ '</td><td align=\"right\">'
\t\t\t\t\t+ value.nilaipn
\t\t\t\t\t+ '</td></tr>';

\t\t\t\t\tvar html2 = [
\t\t\t\t\t\t'<tr id=\"row_'+key+'\">',
\t\t\t\t\t\t\t'<td>' + value.pnid + '</td>',
\t\t\t\t\t\t\t'<td>' + value.namapn + '</td>',
\t\t\t\t\t\t\t'<td>' + value.tahunpn + '</td>',
\t\t\t\t\t\t\t'<td>' + value.nilaipn + '</td>',
\t\t\t\t\t\t\t'<td width=\"15px;\" style=\"text-align:center;\">',
\t\t\t\t\t\t\t\t'<a id=\"hapus_' + key + '\" href=\"#\">',
\t\t\t\t\t\t\t\t\t'<span class=\"glyphicon glyphicon-trash\"></span>',
\t\t\t\t\t\t\t\t'</a>',
\t\t\t\t\t\t\t'</td>',
\t\t\t\t\t\t'</tr>'
\t\t\t\t\t].join(\"\\r\\n\");

\t\t\t\t\t\$('.table_tambah_nilai > tbody').append(html2);

\t\t\t\t\t\$('#hapus_'+key).on(\"click\", function(e){
\t                    e.preventDefault();
\t                    \$.post(\"";
        // line 212
        echo twig_escape_filter($this->env, site_url("aset/hapus_aset_renov"), "html", null, true);
        echo "\", {no_register:value.pnid}, function(json) {
\t                    \tload_aset_renov();
\t                    }, \"json\");
\t                });
\t\t\t\t});

\t\t\t\t// \$('.table_tambah_nilai > tbody').append(html);
\t\t\t\t\$(\"body\").unmask();
\t\t\t}, \"json\");
\t\t}

\t\t\$(\"#btnTambahRenov\").on(\"click\", function() {
\t\t\tvar aset_induk = \$(\"select[name=aset_induk]\").val();
\t\t\tvar nama_aset_induk = \$(\"select[name=aset_induk] option:selected\").text();

\t\t\t\$(\"input[name=aset-induk-dummie]\").val(nama_aset_induk);
\t\t\t\$(\"input[name=aset-induk-id]\").val(aset_induk);

            \$(\"#tambah-aset-renov\").modal(\"show\");
        });

        \$(\"button[name=simpan]\").on(\"click\", function() {
        \tvar aset_induk = \$(\"select[name=aset_induk]\").val();
        \tvar aset_renov = \$(\"select[name=aset-renov-id]\").val();

        \tif (aset_renov == aset_induk) {
        \t\talert(\"Aset Induk dan Aset Renov Tidak Boleh Sama.\");
        \t} else {
        \t\t\$(\"body\").mask(\"Processing...\");
\t        \t\$.post(\"";
        // line 241
        echo twig_escape_filter($this->env, site_url("aset/get_aset_by_no_register"), "html", null, true);
        echo "\", {no_register:aset_renov}, function(json) {
\t        \t\tvar aset = json.aset;
\t        \t\tvar html = '';

\t\t\t\t\t\$.each(aset, function(key, value) {
\t\t\t\t\t\tvar value = aset[key];

\t\t\t\t\t\thtml += '<tr><td>'
\t\t\t\t\t\t+ value.NO_REGISTER
\t\t\t\t\t\t+ '</td><td>'
\t\t\t\t\t\t+ value.NAMA_BARANG
\t\t\t\t\t\t+ '</td><td align=\"center\">'
\t\t\t\t\t\t+ value.TAHUN_PENGADAAN
\t\t\t\t\t\t+ '</td><td align=\"right\">'
\t\t\t\t\t\t+ value.HARGA_TOTAL_PLUS_PAJAK
\t\t\t\t\t\t+ '</td></tr>';

\t\t\t\t\t\tvar html2 = [
\t\t\t\t\t\t\t'<tr id=\"row_'+key+'\">',
\t\t\t\t\t\t\t\t'<td>' + value.NO_REGISTER + '</td>',
\t\t\t\t\t\t\t\t'<td>' + value.NAMA_BARANG + '</td>',
\t\t\t\t\t\t\t\t'<td>' + value.TAHUN_PENGADAAN + '</td>',
\t\t\t\t\t\t\t\t'<td>' + value.HARGA_TOTAL_PLUS_PAJAK + '</td>',
\t\t\t\t\t\t\t\t'<td width=\"15px;\" style=\"text-align:center;\">',
\t\t\t\t\t\t\t\t\t'<a id=\"hapus_' + key + '\" href=\"#\">',
\t\t\t\t\t\t\t\t\t\t'<span class=\"glyphicon glyphicon-trash\"></span>',
\t\t\t\t\t\t\t\t\t'</a>',
\t\t\t\t\t\t\t\t'</td>',
\t\t\t\t\t\t\t'</tr>'
\t\t\t\t\t\t].join(\"\\r\\n\");

\t\t\t\t\t\t\$('.table_tambah_nilai > tbody').append(html2);

\t\t\t\t\t\t\$('#hapus_'+key).on(\"click\", function(e){
\t\t                    e.preventDefault();
\t\t                    \$.post(\"";
        // line 276
        echo twig_escape_filter($this->env, site_url("aset/hapus_aset_renov"), "html", null, true);
        echo "\", {no_register:value.NO_REGISTER}, function(json) {
\t\t                    \tload_aset_renov();
\t\t                    }, \"json\");
\t\t                });
\t\t\t\t\t});

\t\t\t\t\t//\$('.table_tambah_nilai > tbody').append(html);
\t\t\t\t\t\$(\"body\").unmask();
\t        \t}, \"json\");


\t        \t\$.post(\"";
        // line 287
        echo twig_escape_filter($this->env, site_url("aset/tambah_aset_renov"), "html", null, true);
        echo "\", {aset_induk:aset_induk, aset_renov:aset_renov}, function(json) {
\t        \t}, \"json\");

\t        \t\$(\"#tambah-aset-renov\").modal(\"hide\");
\t        \t\$('select[name=aset-renov-id]').val('').trigger('change');
        \t}
        });

\t\t\$(\"#btnPrintTambahNilai\").click(function() {
\t\t\tvar nomor_unit = \$(\"select[name=nomor_unit]\").val();
\t\t\t//var nomor_sub_unit = \$(\"select[name=nomor_sub_unit]\").val();
    \t\tvar no_register_induk = \$(\"select[name=aset_induk]\").val();
    \t\twindow.open( \"";
        // line 299
        echo twig_escape_filter($this->env, site_url("aset/print_penambahan_nilai"), "html", null, true);
        echo "/\"+ nomor_unit + \"/\" + no_register_induk, '_blank');
    \t\t// if(nomor_sub_unit === \"\") {
      //       \twindow.open( \"";
        // line 301
        echo twig_escape_filter($this->env, site_url("aset/print_penambahan_nilai"), "html", null, true);
        echo "/\"+ nomor_unit + \"/\" + no_register_induk, '_blank');
    \t\t// } else {
    \t\t// \twindow.open( \"";
        // line 303
        echo twig_escape_filter($this->env, site_url("aset/print_penambahan_nilai"), "html", null, true);
        echo "/\"+ nomor_sub_unit + \"/\" + no_register_induk, '_blank');
    \t\t// }
            
\t\t});
    \t//menampilkan list ASET INDUK
  //   \t\$.get(\"";
        // line 308
        echo twig_escape_filter($this->env, site_url("aset/list_aset_induk"), "html", null, true);
        echo "\", {}, function(json){ 
\t\t// \tvar listAsetInduk = json.result;
\t\t\t
\t\t// \t\$.each(listAsetInduk, function (key, value) {
\t\t// \t\t\$(\"select[name=nama_aset]\").append(\$('<option></option>').val(value.ID_ASET).html(value.NAMA_BARANG));
\t\t// \t});
\t\t// }, \"json\");
\t});
</script>
";
    }

    // line 320
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 321
        echo "\t";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 322
        echo twig_escape_filter($this->env, base_url("assets/css/devexpress-like/jquery-ui.css"), "html", null, true);
        echo "\" />
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 323
        echo twig_escape_filter($this->env, base_url("assets/css/select2.min.css"), "html", null, true);
        echo "\" />
\t<link href=\"";
        // line 324
        echo twig_escape_filter($this->env, base_url("assets/css/jquery.loadmask.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">\t\t

\t<style type=\"text/css\">
\t\t.control-label{
\t\t\tpadding-top: 8px !important;
\t\t}

\t\tselect.form-control {
\t\t\theight: 48px !important;
\t\t}

\t\tinput.form-control {
\t\t\theight: 34px !important;
\t\t}

\t\ttextarea.form-control {
\t\t\theight:auto !important;
\t\t}

\t\t.row {
\t\t\tpadding:10px 0px;
\t\t}

\t\t.spacer {
\t\t    margin-top: 20px;
\t\t}

\t\t.ui-autocomplete{
\t\t\tbackground: none repeat scroll 0 0 white;
\t\t}

\t\t.input-group-addon{
\t\t\tborder-radius: 0 !important;
\t\t}

\t\t.input-group .form-control{
\t\t\tz-index: 0; !important;
\t\t}

\t\t.select2-selection__rendered {
\t\t\theight: 28px;
\t\t}
\t\t.select2-container {
\t\t\twidth: 100% !important;
\t\t\tpadding: 0;
\t\t}
\t\t#btnTambahRenov {
\t\t\tmargin-top: 23px;
\t\t\theight: 30px;
\t\t}
\t\t#btnPrintTambahNilai {
\t\t\tmargin-top: 23px;
\t\t\theight: 30px;
\t\t}
\t</style>
";
    }

    public function getTemplateName()
    {
        return "aset/penambahan_nilai.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  428 => 324,  424 => 323,  420 => 322,  415 => 321,  412 => 320,  398 => 308,  390 => 303,  385 => 301,  380 => 299,  365 => 287,  351 => 276,  313 => 241,  281 => 212,  243 => 177,  194 => 131,  166 => 106,  159 => 102,  151 => 97,  141 => 90,  131 => 83,  105 => 60,  101 => 59,  97 => 58,  93 => 57,  89 => 56,  86 => 55,  35 => 5,  33 => 4,  30 => 3,);
    }
}
