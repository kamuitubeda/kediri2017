<?php

/* simgo/spk.html */
class __TwigTemplate_74fb8facd86f7f1952923128bb70f2d0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("_templates/adminpage.html");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'scripts' => array($this, 'block_scripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_templates/adminpage.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "
";
        // line 5
        $this->env->loadTemplate("simgo/edit_mediapool.html")->display($context);
        // line 6
        echo "
<div class=\"container\">
\t<div class=\"row\">
\t\t<div class=\"col-xs-12\">
\t\t\t";
        // line 10
        if (((isset($context["kategori"]) ? $context["kategori"] : null) == "SPK")) {
            // line 11
            echo "\t\t\t\t<a href=\"";
            echo twig_escape_filter($this->env, site_url("media/list_spk"), "html", null, true);
            echo "\">&lt;&lt; kembali</a>
\t\t\t";
        } elseif (((isset($context["kategori"]) ? $context["kategori"] : null) == "Kegiatan")) {
            // line 13
            echo "\t\t\t\t<a href=\"";
            echo twig_escape_filter($this->env, site_url("media/list_kegiatan"), "html", null, true);
            echo "\">&lt;&lt; kembali</a>
\t\t\t";
        }
        // line 15
        echo "\t\t\t<h2><span class=\"glyphicon glyphicon-list\"></span> <span name=\"nomor\">";
        if (((isset($context["kategori"]) ? $context["kategori"] : null) == "SPK")) {
            echo "No. SPK";
        } elseif (((isset($context["kategori"]) ? $context["kategori"] : null) == "Kegiatan")) {
            echo "Kode Rekening Kegiatan";
        }
        echo " : ";
        echo twig_escape_filter($this->env, (isset($context["mpno"]) ? $context["mpno"] : null), "html", null, true);
        echo "</span>
\t\t\t\t<button id=\"ubah\" type=\"button\" class=\"btn btn-link\" data-toggle=\"modal\" data-target=\"#edit-mediapool\"><span><i class=\"icon-edit\" style=\"font-size:24px;\"></i></span></button>
\t\t\t</h2>
\t\t</div>
\t</div>

\t<div class=\"row\" style=\"margin-bottom:12px\">
\t\t<div class=\"col-xs-12\">
\t\t\t<h3><span class=\"glyphicon glyphicon-folder-open\"></span> Dokumen</h3>
\t\t\t<ul class=\"list-group\" style=\"margin-bottom:6px\">
\t\t\t\t";
        // line 25
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["mpdokumen"]) ? $context["mpdokumen"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["spk"]) {
            // line 26
            echo "\t\t\t\t\t<li class=\"list-group-item\" style=\"padding-top:2px; padding-bottom:2px;\">
\t\t\t\t\t\t<small>
\t\t\t\t\t\t";
            // line 28
            if (($this->getAttribute((isset($context["spk"]) ? $context["spk"] : null), "uploaded") != null)) {
                // line 29
                echo "\t\t\t\t\t\t\t<span class=\"glyphicon glyphicon-file\"></span> <strong>";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["spk"]) ? $context["spk"] : null), "mdktnama"), "html", null, true);
                echo "</strong> (";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["spk"]) ? $context["spk"] : null), "uploaded"), "html", null, true);
                echo ") : <a href=\"";
                echo twig_escape_filter($this->env, site_url(((("media/do_download/" . (isset($context["mpid"]) ? $context["mpid"] : null)) . "/") . $this->getAttribute((isset($context["spk"]) ? $context["spk"] : null), "mdid"))), "html", null, true);
                echo "\" target=\"_blank\">";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["spk"]) ? $context["spk"] : null), "mdnama"), "html", null, true);
                echo "</a>
\t\t\t\t\t\t";
            } else {
                // line 31
                echo "\t\t\t\t\t\t\t-
\t\t\t\t\t\t";
            }
            // line 33
            echo "\t\t\t\t\t\t</small>
\t\t\t\t\t</li>
\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['spk'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 36
        echo "\t\t\t</ul>
\t\t\t<a href=\"";
        // line 37
        echo twig_escape_filter($this->env, site_url(("media/upload_spk/" . (isset($context["mpid"]) ? $context["mpid"] : null))), "html", null, true);
        echo "\" class=\"btn btn-default\"><span class=\"glyphicon glyphicon-upload\"></span> Unggah</a>
\t\t\t";
        // line 38
        if (((isset($context["latest_media"]) ? $context["latest_media"] : null) != "")) {
            // line 39
            echo "\t\t\t\t<a href=\"";
            echo twig_escape_filter($this->env, site_url(((("media/do_download/" . (isset($context["mpid"]) ? $context["mpid"] : null)) . "/") . (isset($context["latest_media"]) ? $context["latest_media"] : null))), "html", null, true);
            echo "\" class=\"btn btn-default\"><span class=\"glyphicon glyphicon-download\"></span> Unduh Berkas Terbaru</a>
\t\t\t";
        }
        // line 41
        echo "\t\t</div>
\t</div>

\t<div class=\"row\">
\t\t<div class=\"col-xs-12\">
\t\t\t<h3><span class=\"glyphicon glyphicon-comment\"></span> Komentar</h3>
\t\t</div>
\t</div>

\t<div class=\"row\">
\t\t<div class=\"col-xs-12\">
\t\t\t<ul class=\"list-group\" style=\"margin-bottom:10px\">
\t\t\t\t";
        // line 53
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["komentar"]) ? $context["komentar"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["k"]) {
            // line 54
            echo "\t\t\t\t<li class=\"list-group-item\"><span class=\"glyphicon glyphicon-user\" style=\"font-size:16px\"></span> <strong>";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["k"]) ? $context["k"] : null), "uid"), "html", null, true);
            echo "</strong> <small>(";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["k"]) ? $context["k"] : null), "created"), "html", null, true);
            echo ")</small><br/>
\t\t\t\t\t";
            // line 55
            if ((!$this->getAttribute((isset($context["k"]) ? $context["k"] : null), "raw"))) {
                // line 56
                echo "\t\t\t\t\t";
                echo twig_escape_filter($this->env, $this->getAttribute((isset($context["k"]) ? $context["k"] : null), "message"), "html", null, true);
                echo "
\t\t\t\t\t";
            } else {
                // line 58
                echo "\t\t\t\t\t";
                echo $this->getAttribute((isset($context["k"]) ? $context["k"] : null), "message");
                echo "
\t\t\t\t\t";
            }
            // line 60
            echo "\t\t\t\t</li>
\t\t\t\t";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['k'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 62
        echo "\t\t\t</ul>
\t\t</div>
\t</div>
\t<div class=\"row\">
\t\t<div class=\"col-xs-12\">
\t\t\t<form class=\"form-inline\" method=\"post\" action=\"";
        // line 67
        echo twig_escape_filter($this->env, site_url(("media/send_comment/" . (isset($context["mpid"]) ? $context["mpid"] : null))), "html", null, true);
        echo "#comment\" role=\"form\">
\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t<input type=\"text\" class=\"form-control\" name=\"comment\" id=\"comment\">
\t\t\t\t\t<span class=\"input-group-btn\">
\t\t\t\t\t\t<button type=\"submit\" class=\"btn btn-default\">Kirim</button>
\t\t\t\t\t</span>
\t\t\t\t</div>
\t\t\t</form>
\t\t</div>
\t</div>
</div>
";
    }

    // line 80
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 81
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "

<style type=\"text/css\">
.modal {
  text-align: center;
  padding: 0!important;
}

.modal:before {
  content: '';
  display: inline-block;
  height: 100%;
  vertical-align: middle;
  margin-right: -4px; /* Adjusts for spacing */
}

.modal-dialog {
  display: inline-block;
  text-align: left;
  vertical-align: middle;
}
</style>
";
    }

    // line 105
    public function block_scripts($context, array $blocks = array())
    {
        // line 106
        $this->displayParentBlock("scripts", $context, $blocks);
        echo "
<script type=\"text/javascript\">
\tjQuery(function(\$){
\t\tif(window.location.hash == \"comment\"){
\t\t\t\$(\"#comment\").focus();
\t\t}

\t\tfunction editMediapoolNomor() {
\t\t\tvar url = \"";
        // line 114
        echo twig_escape_filter($this->env, ((site_url("media/edit_mediapool/") . "/") . (isset($context["mpid"]) ? $context["mpid"] : null)), "html", null, true);
        echo "\";
\t\t\tconsole.log(url);
\t\t\tvar no_mediapool = \$(\"input[name=no-mediapool]\").val();

\t\t\t\$.post(url, {no_mediapool: no_mediapool}, function(r) {
\t\t\t\t\$(\"#edit-mediapool\").modal(\"hide\");
\t\t\t\t\$(\"span[name=nomor]\").html(\"";
        // line 120
        if (((isset($context["kategori"]) ? $context["kategori"] : null) == "SPK")) {
            echo "No.";
        } elseif (((isset($context["kategori"]) ? $context["kategori"] : null) == "Kegiatan")) {
            echo "Kode Rekening Kegiatan";
        }
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["kategori"]) ? $context["kategori"] : null), "html", null, true);
        echo " : \" + \$(\"input[name=no-mediapool]\").val());
\t\t\t}, \"json\");
\t\t}
\t\t//tambah mediapool
\t\t\$(\"button[name=simpan]\").click(function(){
\t\t\teditMediapoolNomor();
\t\t}); 
\t});
</script>
";
    }

    public function getTemplateName()
    {
        return "simgo/spk.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  249 => 120,  240 => 114,  229 => 106,  226 => 105,  199 => 81,  196 => 80,  180 => 67,  173 => 62,  166 => 60,  160 => 58,  154 => 56,  152 => 55,  145 => 54,  141 => 53,  127 => 41,  121 => 39,  119 => 38,  115 => 37,  112 => 36,  104 => 33,  100 => 31,  88 => 29,  86 => 28,  82 => 26,  78 => 25,  58 => 15,  52 => 13,  46 => 11,  44 => 10,  38 => 6,  36 => 5,  33 => 4,  30 => 3,);
    }
}
