<?php

/* aset/form_spk.html */
class __TwigTemplate_bd5bf4e86ac77a7ed58395c757e3edff extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("_templates/adminpage.html");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'scripts' => array($this, 'block_scripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_templates/adminpage.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "\t<div class=\"row\">
\t\t<div id=\"error\" class=\"col-xs-8 alert alert-danger\" style=\"margin-left:10px;";
        // line 5
        if (((isset($context["validation_errors"]) ? $context["validation_errors"] : null) == false)) {
            echo "display:none;";
        }
        echo "\">
\t\t\t";
        // line 6
        if (((isset($context["validation_errors"]) ? $context["validation_errors"] : null) != false)) {
            // line 7
            echo "\t\t\t\t";
            echo (isset($context["validation_errors"]) ? $context["validation_errors"] : null);
            echo "
\t\t\t";
        }
        // line 9
        echo "\t\t</div>
\t</div>
";
        // line 11
        if ((twig_length_filter($this->env, (isset($context["id"]) ? $context["id"] : null)) <= 1)) {
            // line 12
            echo form_open("spk/submit");
            echo "
";
        } else {
            // line 14
            echo form_open(("spk/submit/" . (isset($context["id"]) ? $context["id"] : null)));
            echo "
";
        }
        // line 16
        echo "\t<div class=\"row\">
\t\t<div class=\"col-xs-12\" style=\"padding-left:0px; padding-right:0px;\">
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-xs-5\">
\t\t\t\t\t<label>SKPD</label>
\t\t\t\t\t<select type=\"text\" name=\"nomor_sub_unit\" class=\"form-control\">
\t\t\t\t\t\t<option value=\"\">Pilih SKPD</option>
\t\t\t\t\t</select>
\t\t\t\t</div>\t
\t\t\t</div>
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-xs-2\">
\t\t\t\t\t<label>Tahun Kegiatan</label>
\t\t\t\t\t<select type=\"text\" name=\"tahun_spj\" class=\"form-control\">
\t\t\t\t\t\t<option value=\"\">Pilih Tahun</option>
\t\t\t\t\t</select>
\t\t\t\t</div>\t
\t\t\t</div>
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-xs-6\">
\t\t\t\t\t<label>Kegiatan</label>
\t\t\t\t\t<select name=\"kode_kegiatan\" class=\"form-control\"></select>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"clearfix\"></div>
\t\t<div class=\"spacer\"></div>\t
\t\t<ul class=\"nav nav-tabs\">
\t\t\t<li class=\"active\"><a href=\"#awal\" data-toggle=\"tab\">SPK/SP/Kontrak Awal</a></li>
\t\t\t<li><a href=\"#addendum\" data-toggle=\"tab\">Perubahan/Addendum</a></li>
\t\t</ul>
\t\t<div class=\"tab-content\">
\t\t\t<div class=\"tab-pane fade in active\" id=\"awal\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-xs-4\">
\t\t\t\t\t\t<label>No.SPK/SP/Kontrak</label>
\t\t\t\t\t\t<input class=\"form-control\" name=\"no_spk_sp_dokumen\" value=\"";
        // line 52
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "NO_SPK_SP_DOKUMEN"), "html", null, true);
        echo "\" />
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-xs-2\">
\t\t\t\t\t\t<label class=\"control-label\">Tanggal SPK/SP/Kontrak</label>
\t\t\t\t\t\t<div data-date-format=\"dd-mm-yyyy\" data-date=\"";
        // line 58
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "TGL_SPK_SP_DOKUMEN"), "html", null, true);
        echo "\" datepicker class=\"input-group input-append date\">
\t\t\t\t\t\t\t<input name=\"tgl_spk_sp_dokumen\" readonly=\"\" class=\"form-control\" value=\"";
        // line 59
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "TGL_SPK_SP_DOKUMEN"), "html", null, true);
        echo "\"></input>
\t\t\t\t\t\t\t<span class=\"input-group-addon add-on\"><i class=\"icon-calendar\"></i></span>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-xs-7\">
\t\t\t\t\t\t<label>Uraian Pekerjaan</label>
\t\t\t\t\t\t<textarea class=\"form-control\" rows=\"4\" name=\"deskripsi_spk_dokumen\">";
        // line 67
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "DESKRIPSI_SPK_DOKUMEN"), "html", null, true);
        echo "</textarea>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-xs-5\">
\t\t\t\t\t\t<label>Nama Rekanan</label>
\t\t\t\t\t\t<input class=\"form-control\" name=\"rekanan\" value=\"";
        // line 73
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "REKANAN"), "html", null, true);
        echo "\" />
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-xs-7\">
\t\t\t\t\t\t<label>Alamat Rekanan</label>
\t\t\t\t\t\t<textarea class=\"form-control\" rows=\"4\" name=\"alamat_rekanan\">";
        // line 79
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "ALAMAT_REKANAN"), "html", null, true);
        echo "</textarea>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t\t<label>Nilai SPK/SP/Kontrak</label>
\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t<span class=\"input-group-addon\">Rp.</span>
\t\t\t\t\t\t\t<input class=\"form-control\" style=\"text-align:right;\" name=\"nilai_spk\" value=\"";
        // line 87
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "NILAI_SPK"), 0, ",", "."), "html", null, true);
        echo "\" />
\t\t\t\t\t\t\t<span class=\"input-group-addon\">,00</span>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-xs-1\">
\t\t\t\t\t\t<label>Jumlah Termin</label>
\t\t\t\t\t\t<input class=\"form-control\" name=\"termin\" value=\"";
        // line 95
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "ESTIMASI_TERMIN"), "html", null, true);
        echo "\" />
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"tab-pane fade in\" id=\"addendum\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-xs-4\">
\t\t\t\t\t\t<label>SPK/SP/Kontrak</label>
\t\t\t\t\t\t<input class=\"form-control\" name=\"no_add\" value=\"";
        // line 103
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "NO_ADD"), "html", null, true);
        echo "\" />
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-xs-2\">
\t\t\t\t\t\t<label class=\"control-label\">Tanggal SPK/SP/Kontrak</label>
\t\t\t\t\t\t<div data-date-format=\"dd-mm-yyyy\" data-date=\"";
        // line 109
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "TGL_ADD"), "html", null, true);
        echo "\" datepicker class=\"input-group input-append date\">
\t\t\t\t\t\t\t<input name=\"tgl_add\" readonly=\"\" class=\"form-control\" value=\"";
        // line 110
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "TGL_ADD"), "html", null, true);
        echo "\"></input>
\t\t\t\t\t\t\t<span class=\"input-group-addon add-on\"><i class=\"icon-calendar\"></i></span>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-xs-7\">
\t\t\t\t\t\t<label>Uraian Pekerjaan</label>
\t\t\t\t\t\t<textarea class=\"form-control\" rows=\"4\" name=\"uraian_add\">";
        // line 118
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "URAIAN_ADD"), "html", null, true);
        echo "</textarea>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-xs-3\">
\t\t\t\t\t\t<label>Nilai SPK/SP/Kontrak</label>
\t\t\t\t\t\t<div class=\"input-group\">
\t\t\t\t\t\t\t<span class=\"input-group-addon\">Rp.</span>
\t\t\t\t\t\t\t<input class=\"form-control\" style=\"text-align:right;\" name=\"nilai_add\" value=\"";
        // line 126
        echo twig_escape_filter($this->env, twig_number_format_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "NILAI_ADD"), 0, ",", "."), "html", null, true);
        echo "\" />
\t\t\t\t\t\t\t<span class=\"input-group-addon\">,00</span>
\t\t\t\t\t\t</div>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-xs-1\">
\t\t\t\t\t\t<label>Jumlah Termin</label>
\t\t\t\t\t\t<input class=\"form-control\" name=\"jml_termin_add\" value=\"";
        // line 134
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "JML_TERMIN_ADD"), "html", null, true);
        echo "\">
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t\t<div class=\"clearfix\"></div>
\t\t<div class=\"spacer\"></div>
\t\t<div class=\"row\">
\t\t\t<div class=\"form-group\">
\t\t\t\t<div class=\"col-sm-offset-4 col-sm-10\">
\t\t\t\t\t<button class=\"btn btn-success\" type=\"submit\">Simpan</button>
\t\t\t\t\t<a href=\"";
        // line 145
        echo twig_escape_filter($this->env, site_url("spk/index"), "html", null, true);
        echo "\"><button class=\"btn btn-default\" type=\"button\">Kembali</button></a>
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t</div>
";
        // line 150
        echo form_close();
        echo "
";
    }

    // line 154
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 155
        echo "\t";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "

\t<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 157
        echo twig_escape_filter($this->env, base_url("assets/css/datepicker3.css"), "html", null, true);
        echo "\" />
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 158
        echo twig_escape_filter($this->env, base_url("assets/css/devexpress-like/jquery-ui.css"), "html", null, true);
        echo "\" />
\t<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 159
        echo twig_escape_filter($this->env, base_url("assets/css/select2.min.css"), "html", null, true);
        echo "\" />
\t

\t<style type=\"text/css\">
\t\t.control-label{
\t\t\tpadding-top: 8px !important;
\t\t}

\t\tselect.form-control {
\t\t\theight: 48px !important;
\t\t}

\t\tinput.form-control {
\t\t\theight: 34px !important;
\t\t}

\t\ttextarea.form-control {
\t\t\theight:auto !important;
\t\t}

\t\t.row {
\t\t\tpadding:10px 0px;
\t\t}

\t\t.spacer {
\t\t    margin-top: 20px;
\t\t}

\t\t.ui-autocomplete{
\t\t\tbackground: none repeat scroll 0 0 white;
\t\t}

\t\t.input-group-addon{
\t\t\tborder-radius: 0 !important;
\t\t}

\t\t.input-group .form-control{
\t\t\tz-index: 0; !important;
\t\t}
\t</style>

";
    }

    // line 203
    public function block_scripts($context, array $blocks = array())
    {
        // line 204
        echo "\t";
        $this->displayParentBlock("scripts", $context, $blocks);
        echo "
\t
\t<script type=\"text/javaScript\" src=\"";
        // line 206
        echo twig_escape_filter($this->env, base_url("assets/js/bootstrap-datepicker.js"), "html", null, true);
        echo "\"></script>
\t<script type=\"text/javaScript\" src=\"";
        // line 207
        echo twig_escape_filter($this->env, base_url("assets/js/accounting.min.js"), "html", null, true);
        echo "\"></script>
\t<script type=\"text/javaScript\" src=\"";
        // line 208
        echo twig_escape_filter($this->env, base_url("assets/js/jquery.validate.min.js"), "html", null, true);
        echo "\"></script>
\t<script type=\"text/javaScript\" src=\"";
        // line 209
        echo twig_escape_filter($this->env, base_url("assets/js/select2.min.js"), "html", null, true);
        echo "\"></script>
\t<script type=\"text/javaScript\">
\t\tjQuery( document ).ready(function(\$) {
\t\t\tif (!String.prototype.format) {
\t\t\t\tString.prototype.format = function() {
\t\t\t\t\tvar args = arguments;
\t\t\t\t\treturn this.replace(/{(\\d+)}/g, function(match, number) { 
\t\t\t\t\t\treturn typeof args[number] != 'undefined' ? args[number] : match;
\t\t\t\t\t});
\t\t\t\t};
\t\t\t}


\t\t\t// configure setting accounting.js
\t\t\taccounting.settings = {
\t\t\t\tnumber: {
\t\t\t\t\tprecision : 0,  // default precision on numbers is 0
\t\t\t\t\tthousand: \".\",
\t\t\t\t\tdecimal : \",\"
\t\t\t\t}
\t\t\t}


\t\t\t\$(\"*[datepicker]\").datepicker();
\t\t\t\$(\"select\").select2();


\t\t\t//menampilkan list SKPD
\t\t\t\$.get(\"";
        // line 237
        echo twig_escape_filter($this->env, site_url("aset/list_sub_skpd"), "html", null, true);
        echo "\", {}, function(json){ 
\t\t\t\tvar listSkpd = json.result;
\t\t\t\t
\t\t\t\t\$.each(listSkpd, function (key, value) {
\t\t\t\t\t\$(\"select[name=nomor_sub_unit]\").append(\$('<option></option>').val(value.NOMOR_SUB_UNIT).html(value.NAMA_SUB_UNIT));
\t\t\t\t});

\t\t\t\t\$(\"select[name=nomor_sub_unit]\").val(\"";
        // line 244
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "NOMOR_SUB_UNIT"), "html", null, true);
        echo "\");
\t\t\t\t\$(\"select[name=nomor_sub_unit]\").select2();

\t\t\t\treload_kegiatan();
\t\t\t}, \"json\");


\t\t\t\$(\"input[name=id_kegiatan]\").autocomplete({
\t\t\t\tminLength: 1,
\t\t\t\tsource: \"";
        // line 253
        echo twig_escape_filter($this->env, site_url("pengadaan/auto_kegiatan"), "html", null, true);
        echo "\",
\t\t\t\tselect: function(e, u) {
\t\t\t\t\t\$(this).val(u.item.id);
\t\t\t\t\tvar form = \$(this).closest(\"form\");
\t\t\t\t\tform.find(\"input[name=id_kegiatan]\").val(u.item.Ket_Kegiatan);
\t\t\t\t\tform.find(\"input[name=id_kegiatan]\").val(u.item.id);
\t\t\t\t\treturn false;
\t\t\t\t}
\t\t\t});


\t\t\t//menampilkan list tahun
\t\t\tfor (var j=0; j<50; j++){
\t\t\t\t\$(\"select[name=tahun_spj]\").append(\$('<option></option>').val(2010+j).html(2010+j));
\t\t\t}

\t\t\t\$(\"select[name=tahun_spj]\").val(\"";
        // line 269
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "TAHUN_SPJ"), "html", null, true);
        echo "\");
\t\t\t\$(\"select[name=tahun_spj]\").select2();



\t\t\t\$(\"select[name=nomor_sub_unit], select[name=tahun_spj]\").change(function() {
\t\t\t\treload_kegiatan();
\t\t\t});


\t\t\tvar reload_kegiatan = function() {
\t\t\t\tvar nomor_sub_unit = \$(\"select[name=nomor_sub_unit]\").val();
\t\t\t\tvar tahun_spj = \$(\"select[name=tahun_spj]\").val();

\t\t\t\t\$.post(\"";
        // line 283
        echo twig_escape_filter($this->env, site_url("spk/get_list_kegiatan"), "html", null, true);
        echo "\", {
\t\t\t\t\tnomor_sub_unit: nomor_sub_unit,
\t\t\t\t\ttahun_spj: tahun_spj
\t\t\t\t}, function(json){ 
\t\t\t\t\tvar data = json.data;
\t\t\t\t\tvar el = \$(\"select[name=kode_kegiatan]\");

\t\t\t\t\tel.empty();
\t\t\t\t\tel.append(\"<option value=''>Pilih Kegiatan</option>\");
\t\t\t\t\tfor (var i=0; i<data.length; i++) {
\t\t\t\t\t\tvar k = data[i];
\t\t\t\t\t\tel.append(\"<option value='{0}'>{1}</option>\".format(k.KODE_KEGIATAN, k.NAMA_KEGIATAN));
\t\t\t\t\t}

\t\t\t\t\tel.val(\"";
        // line 297
        echo twig_escape_filter($this->env, twig_slice($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "ID_KEGIATAN"), 0, (twig_length_filter($this->env, $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "ID_KEGIATAN")) - 4)), "html", null, true);
        echo "\");
\t\t\t\t\tel.select2();
\t\t\t\t}, \"json\");
\t\t\t}



\t\t\t// nilai spk
\t\t\t\$(\"input[name=nilai_spk],input[name=nilai_add]\").focus(function() {
\t\t\t\t\$(this).val(\$(this).val().replace(/\\./g, \"\"));
\t\t\t});

\t\t\t\$(\"input[name=nilai_spk],input[name=nilai_add]\").blur(function() {
\t\t\t\t\$(this).val(accounting.formatNumber(\$(this).val().replace(/\\D/g, \"\")));
\t\t\t});



\t\t\t/* ==================== validation ==================== */
\t\t\t\$(\"form\").validate({
\t\t\trules: {
\t\t\t\tnomor_sub_unit: {
\t\t\t\t\trequired: true
\t\t\t\t},
\t\t\t\ttahun_spj: {
\t\t\t\t\trequired: true
\t\t\t\t},
\t\t\t\tkode_kegiatan: {
\t\t\t\t\trequired: true
\t\t\t\t},
\t\t\t\tno_spk_sp_dokumen: {
\t\t\t\t\trequired: true
\t\t\t\t}
\t\t\t},
\t\t\tmessages: {
\t\t\t\tnomor_sub_unit: {
\t\t\t\t\trequired: \"SKPD harus dipilih!\"
\t\t\t\t},
\t\t\t\ttahun_spj: {
\t\t\t\t\trequired: \"Tahun Kegiatan harus dipilih!\"
\t\t\t\t},
\t\t\t\tkode_kegiatan: {
\t\t\t\t\trequired: \"Kegiatan harus dipilih!\"
\t\t\t\t},
\t\t\t\tno_spk_sp_dokumen: {
\t\t\t\t\trequired: \"No.SPK/SP/Kontrak harus diisi!\"
\t\t\t\t}
\t\t\t},
\t\t\terrorLabelContainer: \"#error\",
\t\t\tignore: []
\t\t});
\t\t});
\t</script>
";
    }

    public function getTemplateName()
    {
        return "aset/form_spk.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  443 => 297,  426 => 283,  409 => 269,  390 => 253,  378 => 244,  368 => 237,  337 => 209,  333 => 208,  329 => 207,  325 => 206,  319 => 204,  316 => 203,  270 => 159,  266 => 158,  262 => 157,  256 => 155,  253 => 154,  247 => 150,  239 => 145,  225 => 134,  214 => 126,  203 => 118,  192 => 110,  188 => 109,  179 => 103,  168 => 95,  157 => 87,  146 => 79,  137 => 73,  128 => 67,  117 => 59,  113 => 58,  104 => 52,  66 => 16,  61 => 14,  56 => 12,  54 => 11,  50 => 9,  44 => 7,  42 => 6,  36 => 5,  33 => 4,  30 => 3,);
    }
}
