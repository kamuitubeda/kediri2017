<?php

/* aset/hitung_susut.html */
class __TwigTemplate_bfd4b9d58d593705e2e9080559cc55df extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("_templates/adminpage.html");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'scripts' => array($this, 'block_scripts'),
            'stylesheets' => array($this, 'block_stylesheets'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_templates/adminpage.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"col-md-11\">
\t<div class=\"row\">
\t    <p class=\"lead\">Penyusutan aset dihitung hingga waktu ekonomis aset habis.<br /> Dapat dilakukan dengan menekan tombol \"Hitung Susut\".</p>
\t    <div class=\"spacer\"></div>

\t    <button id=\"btnHitungSusut\" type=\"button\" class=\"btn btn-success\">Hitung Susut</button>
\t</div>
</div>
";
    }

    // line 15
    public function block_scripts($context, array $blocks = array())
    {
        // line 16
        $this->displayParentBlock("scripts", $context, $blocks);
        echo "
<script type=\"text/javaScript\" src=\"";
        // line 17
        echo twig_escape_filter($this->env, base_url("assets/js/jquery.loadmask.min.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\">
    jQuery(document).ready(function(\$) {
    \t\$(\"#btnHitungSusut\").click(function() {
    \t\t\$(\"body\").mask(\"Processing...\");

    \t\t\$.post(\"";
        // line 23
        echo twig_escape_filter($this->env, site_url("aset/susutkan"), "html", null, true);
        echo "\", {}, function(json) {
\t\t\t\t\$(\"body\").unmask();
    \t\t}, \"json\");
    \t});
    });
</script>
";
    }

    // line 32
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 33
        echo "\t";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "

\t<link href=\"";
        // line 35
        echo twig_escape_filter($this->env, base_url("assets/css/jquery.loadmask.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
";
    }

    public function getTemplateName()
    {
        return "aset/hitung_susut.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  81 => 35,  75 => 33,  72 => 32,  61 => 23,  52 => 17,  48 => 16,  45 => 15,  33 => 4,  30 => 3,);
    }
}
