<?php

/* simgo/persediaan/modal/modal_persiapan_cetak_sppb.html */
class __TwigTemplate_1b3c1f3b0e5acca4b7e21f9de8619f86 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"modal fade\" id=\"persiapan-cetak-sppb\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
\t<div class=\"modal-dialog modal-md\">
\t\t<div class=\"modal-content\">
\t\t\t<div class=\"modal-header\">
\t\t\t\t<button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
\t\t\t\t<h4 class=\"modal-title\" id=\"myModalLabel\">Parameter Dokumen</h4>
\t\t\t</div>
\t\t\t<div class=\"modal-body\">
\t\t\t\t<div class=\"row\">
\t\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t\t<form role=\"form\" id=\"persiapan-cetak-form\">
                            <div class=\"row\">
                                <div class=\"form-group col-md-12\">
                                    <label class=\"control-label\">Alamat Unit<strong>*</strong></label>
                                    <input type=\"text\" class=\"form-control searchable\" id=\"alamat-unit\" name=\"alamat-unit\" placeholder=\"Alamat\" data-rule-required=\"true\">
                                </div>
                            </div>
                            <br/>
                            <div class=\"row\">
                                <div class=\"form-group col-md-12\">
                                    <label class=\"control-label\">Dari<strong>*</strong></label>
                                    <input type=\"text\" class=\"form-control searchable\" id=\"dari\" name=\"dari\" placeholder=\"Dari\" data-rule-required=\"true\">
                                </div>
                            </div>
                            <br/>
                            <div class=\"row\">
                                <div class=\"form-group col-md-12\">
                                    <label class=\"control-label\">Kepada<strong>*</strong></label>
                                    <input type=\"text\" class=\"form-control searchable\" id=\"kepada\" name=\"kepada\" placeholder=\"Bidang\" data-rule-required=\"true\">
                                </div>
                                <div class=\"form-group col-md-12\">
                                    <!--<label class=\"control-label\">&nbsp;</label>-->
                                    <input type=\"text\" class=\"form-control searchable\" id=\"alamat-kepada\" name=\"alamat-kepada\" placeholder=\"Alamat\" data-rule-required=\"true\">
                                </div>
                            </div>
                            <br/>
                            <div class=\"row\">
                                <div class=\"form-group col-md-12\">
                                    <label class=\"control-label\">Pengurus Barang<strong>*</strong></label>
                                    <input type=\"text\" class=\"form-control searchable\" id=\"nama-pengurus\" name=\"nama-pengurus\" placeholder=\"Nama\" data-rule-required=\"true\">
                                </div>
                                <div class=\"form-group col-md-12\">
                                    <!--<label class=\"control-label\">&nbsp;</label>-->
                                    <input type=\"text\" class=\"form-control searchable\" id=\"nip-pengurus\" name=\"nip-pengurus\" placeholder=\"NIP\" data-rule-required=\"true\">
                                </div>
                            </div>
\t\t\t\t\t\t</form>
\t\t\t\t\t</div>
\t\t\t\t</div>
\t\t\t</div>
\t\t\t<div class=\"modal-footer\">
\t\t\t\t<button type=\"button\" id = \"batal\" name=\"batal\" class=\"btn btn-default\" data-dismiss=\"modal\">Batal</button>
\t\t\t\t<button type=\"button\" id = \"lakukan-cetak-sppb\" name=\"lakukan-cetak-sppb\" class=\"btn btn-primary\">Cetak</button>
\t\t\t</div>
\t\t</div>
\t</div>
</div>";
    }

    public function getTemplateName()
    {
        return "simgo/persediaan/modal/modal_persiapan_cetak_sppb.html";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,  829 => 670,  823 => 667,  780 => 627,  772 => 622,  749 => 602,  647 => 505,  639 => 500,  617 => 481,  612 => 479,  599 => 469,  566 => 439,  561 => 437,  518 => 399,  454 => 338,  444 => 331,  425 => 317,  314 => 209,  310 => 208,  306 => 207,  299 => 206,  255 => 167,  251 => 166,  244 => 165,  160 => 86,  156 => 85,  147 => 79,  143 => 78,  132 => 70,  128 => 69,  113 => 57,  109 => 56,  86 => 36,  74 => 27,  70 => 26,  51 => 10,  43 => 4,  40 => 3,  35 => 2,  30 => 1,);
    }
}
