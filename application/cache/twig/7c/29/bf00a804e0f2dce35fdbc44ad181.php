<?php

/* simgo/unggah.html */
class __TwigTemplate_7c29bf00a804e0f2dce35fdbc44ad181 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("_templates/adminpage.html");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_templates/adminpage.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "<div class=\"container\">
\t<div class=\"row\">
\t\t<div class=\"col-md-12\">
\t\t\t<div class=\"page-header\">
\t\t\t\t<h1>Unggah
\t\t\t\t\t<br>
\t\t\t\t\t<small>SPM dan SPPD</small>
\t\t\t\t</h1>
\t\t\t</div>
\t\t</div>
\t</div>
\t";
        // line 15
        echo twig_escape_filter($this->env, (isset($context["error"]) ? $context["error"] : null), "html", null, true);
        echo "
\t<h2>No. SPK : <a href=\"";
        // line 16
        echo twig_escape_filter($this->env, site_url(("media/view_spk/" . (isset($context["spkid"]) ? $context["spkid"] : null)), (isset($context["spkno"]) ? $context["spkno"] : null)), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, (isset($context["spkno"]) ? $context["spkno"] : null), "html", null, true);
        echo "</a></h2>

\t<form action=\"";
        // line 18
        echo twig_escape_filter($this->env, site_url(("media/do_upload/" . (isset($context["spkid"]) ? $context["spkid"] : null))), "html", null, true);
        echo "\" method=\"post\" accept-charset=\"utf-8\" enctype=\"multipart/form-data\">
\t\t<input class=\"form-control\" name=\"spkid\" type=\"hidden\" value= ";
        // line 19
        echo twig_escape_filter($this->env, (isset($context["spkid"]) ? $context["spkid"] : null), "html", null, true);
        echo " />
\t\t<div class=\"form-group\">
\t\t\t<label for=\"doctype\">Tipe dokumen:</label>
\t\t\t<select class=\"form-control\" id=\"doctype\" name=\"kategori\">
\t\t\t\t<option ";
        // line 23
        if (((isset($context["dokumen"]) ? $context["dokumen"] : null) == "1")) {
            echo " selected ";
        }
        echo " >SPM</option>
\t\t\t\t<option ";
        // line 24
        if (((isset($context["dokumen"]) ? $context["dokumen"] : null) == "2")) {
            echo " selected ";
        }
        echo " >SPPD</option>
\t\t\t</select>
\t\t</div>
\t\t<div class=\"form-group\">
\t\t\t<label>Pilih berkas:</label>
\t\t\t<div class=\"row\">
\t\t\t\t<div class=\"col-md-12\">
\t\t\t\t\t<input class=\"form-control\" type=\"file\" name=\"userfile\">
\t\t\t\t</div>
\t\t\t</div>
\t\t</div>
\t\t<button type=\"submit\" class=\"btn btn-default\">Unggah</button>
\t</form>
</div>
";
    }

    public function getTemplateName()
    {
        return "simgo/unggah.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 24,  66 => 23,  59 => 19,  55 => 18,  48 => 16,  44 => 15,  31 => 4,  28 => 3,);
    }
}
