<?php

/* simgo/persediaan/laporan/katalog_bukti_pengambilan_barang.html */
class __TwigTemplate_6e0f965105863463adec52a74308552b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("_templates/adminpage.html");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'scripts' => array($this, 'block_scripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "_templates/adminpage.html";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 1
    public function block_content($context, array $blocks = array())
    {
        // line 2
        echo "<div class=\"container\">
    <div class=\"row\">
        <div class=\"col-md-12\">
            <ol class=\"breadcrumb\">
                <li><a href=\"";
        // line 6
        echo twig_escape_filter($this->env, site_url("persediaan"), "html", null, true);
        echo "\">Persediaan</a></li>
                <li class=\"active\">Buku Pengambilan Barang</li>
            </ol>
        </div>
    </div>
\t<div class=\"row\">
\t\t<div class=\"col-md-3 pull-right\">
\t\t\t<div data-date-format=\"mm-yyyy\" data-date=\"";
        // line 13
        echo twig_escape_filter($this->env, (isset($context["today"]) ? $context["today"] : null), "html", null, true);
        echo "\" datepicker class=\"input-group input-append date\">
\t\t\t\t<input id=\"tanggal\" name=\"tanggal\" readonly=\"\" class=\"form-control\" value=\"";
        // line 14
        echo twig_escape_filter($this->env, (isset($context["today"]) ? $context["today"] : null), "html", null, true);
        echo "\"></input>
\t\t\t\t<span class=\"input-group-addon add-on\"><i class=\"icon-calendar\"></i></span>
\t\t\t</div>
\t\t</div>
\t</div>
    <div class=\"row\">
        <div class=\"col-md-12\">
            <table class=\"table\" style=\"border-style:hidden;\">
                <tr>
                    <td class='col-md-6 text-left'><strong>Daerah/SKPD : ";
        // line 23
        echo twig_escape_filter($this->env, (isset($context["oname"]) ? $context["oname"] : null), "html", null, true);
        echo "</strong></td>
                    <!--<td class='col-md-6 text-right'><strong>No. :</strong> <input type=\"text\"></td>-->
                </tr>
            </table>
        </div>
    </div>
    <!--<div class=\"row\">
        <div class=\"col-md-12 text-center\">
            <h2>Katalog Bukti Pengambilan Barang</h2></div>
    </div>-->
    <div class=\"row\">
        <div class=\"col-md-12\">
            <table class=\"table table-striped table-bordered\">
                <thead>
                    <tr>
                        <th class=\"col-md-1 text-center\" rowspan=\"2\">Nomor Bukti Pengambilan</th>
                        <th class=\"col-md-1 text-center\" rowspan=\"2\">Nomor SPPB</th>
                        <th class=\"col-md-1 text-center\" rowspan=\"2\">Tanggal SPPB</th>
                        <th class=\"text-center\" rowspan=\"2\">Nama dan Spesifikasi Barang</th>
                        <th class=\"text-center\" rowspan=\"2\">Satuan</th>
                        <th class=\"text-center\" colspan=\"2\">Jumlah Barang</th>
                        <th class=\"col-md-2 text-center\" rowspan=\"2\">Jumlah Harga</th>
                        <th class=\"text-center\" rowspan=\"2\"></th>
                    </tr>
                    <tr>
                        <th class=\"col-md-1 text-center\">(angka)</th>
                        <th class=\"col-md-1 text-center\">(huruf)</th>
                    </tr>
                    <tr>
                        ";
        // line 52
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable(range(1, 9));
        foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
            // line 53
            echo "                        <th class=\"small-header text-center\"><small>";
            echo twig_escape_filter($this->env, (isset($context["i"]) ? $context["i"] : null), "html", null, true);
            echo "</small></th>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 55
        echo "                    </tr>
                </thead>
                <tbody id=\"data-body\">
                    <tr></tr>
                </tbody>
            </table>
        </div>
    </div>
    <!--<div class=\"row\">
        <div class=\"col-md-12\">
            <div class=\"btn-group pull-right\">
                <button id=\"kembali\" type=\"button\" class=\"btn btn-default\">Kembali</button>
                <button id=\"cetak\" type=\"button\" class=\"btn btn-primary\"><span class=\"glyphicon glyphicon-print\"></span> Cetak</button>
            </div>
        </div>
    </div>-->
</div>
";
    }

    // line 72
    public function block_stylesheets($context, array $blocks = array())
    {
        echo " ";
        $this->displayParentBlock("stylesheets", $context, $blocks);
        echo "
<link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 73
        echo twig_escape_filter($this->env, base_url("assets/css/datepicker3.css"), "html", null, true);
        echo "\" />
<link rel=\"stylesheet\" type=\"text/css\" media=\"print\" href=\"";
        // line 74
        echo twig_escape_filter($this->env, base_url("assets/css/bootstrap.min.css"), "html", null, true);
        echo "\">
<style type=\"text/css\">
    .small-header {
        font-weight: normal !important;
        padding: 2px !important;
    }
    
    .text-middle{
        vertical-align: middle !important;
    }
    
    @media print {
        body * {
            visibility: hidden;
        }
        #topbar,
        #sidebar,
        .breadcrumb,
        h1 {
            display: none;
        }
        .container,
        .container * {
            visibility: visible;
        }
        .sidebar-max {
            padding: 0px !important;
        }
        #content {
            position: absolute !important;
            left: 0px !important;
            top: 0px !important;
        }
    }
</style>
";
    }

    // line 109
    public function block_scripts($context, array $blocks = array())
    {
        echo " ";
        $this->displayParentBlock("scripts", $context, $blocks);
        echo "
<script type=\"text/javaScript\" src=\"";
        // line 110
        echo twig_escape_filter($this->env, base_url("assets/js/bootstrap-datepicker.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 111
        echo twig_escape_filter($this->env, site_url("assets/js/numeral/numeral.min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 112
        echo twig_escape_filter($this->env, site_url("assets/js/numeral/languages.id.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javaScript\" src=\"";
        // line 113
        echo twig_escape_filter($this->env, base_url("assets/js/base64.js"), "html", null, true);
        echo "\"></script>
<script type=\"text/javascript\">

    numeral.language('id-ID');
    
    jQuery(function(\$){
        populateTable();

\t\t\$(\"*[datepicker]\").datepicker({
\t\t\tautoclose:true,
\t\t\torientation:\"bottom\",
\t\t\tformat: \"mm-yyyy\",
\t\t\tstartView: \"months\", 
\t\t\tminViewMode: \"months\"
\t\t});
        
        \$(\"#tanggal\").on(\"change\", function(){
            populateTable();
        });
        
        function populateTable(){
            \$(\"#data-body\").empty();
            \$(\"#data-body\").append(\"<tr></tr><tr></tr>\");
            
            \$.post(\"";
        // line 137
        echo twig_escape_filter($this->env, site_url("persediaan/get_populasi_bukti_pengambilan_barang"), "html", null, true);
        echo "\", {skpd:\"";
        echo twig_escape_filter($this->env, (isset($context["skpd"]) ? $context["skpd"] : null), "html", null, true);
        echo "\", tanggal:\$(\"#tanggal\").val()}, function(json){
                json.forEach(function(value, index){
                    var nomor = value[\"juno\"];
                    var dasar = value[\"judasarno\"];
                    var tglrec = value[\"judasartgl\"];
                    if(value.detail!=null){
                        value.detail.forEach(function(value, index){
                            \$(\"#data-body\").append(
                                \"<tr>\"+
                                    \"<td class='text-center'>\"+((index == 0)? nomor : \"\")+\"</td>\"+
                                    \"<td class='text-center'>\"+((index == 0)? dasar : \"\")+\"</td>\"+
                                    \"<td class='text-center'>\"+((index == 0)? tglrec : \"\")+\"</td>\"+
                                    \"<td>\"+value['namabarang']+\"<br/><small>\"+value['spesifikasi']+\"</small></td>\"+
                                    \"<td>\"+value['judunit']+\"</td>\"+
                                    \"<td class='text-right'>\"+numeral(-value['judqty']).format(\"0,0.0\")+\"</td>\"+
                                    \"<td><small>\"+value['qty_huruf']+\"</small></td>\"+
                                    \"<td class='text-right'>\"+numeral(-value['judhargasat'] * value['judqty']).format(\"\$0,0.00\")+\"</td>\"+
                                    \"<td class='text-center'>\"+((index == 0)? \"<a href='";
        // line 154
        echo twig_escape_filter($this->env, site_url("persediaan/bukti_pengambilan"), "html", null, true);
        echo "/\"+Base64.encode(nomor)+\"'><span class='glyphicon glyphicon-search'></span></a>\" : \"\")+\"</td>\"+
                                \"</tr>\"
                            );
                        }, nomor, tglrec);
                    }
                    else
                    {
                        \$(\"#data-body\").append(
                            \"<tr>\"+
                                \"<td class='text-center'>\"+nomor+\"</td>\"+
                                \"<td class='text-center'>\"+tglrec+\"</td>\"+
                                \"<td class='text-center text-middle'>-</td>\"+
                                \"<td class='text-center text-middle'>-</td>\"+
                                \"<td class='text-center text-middle'>-</td>\"+
                                \"<td class='text-center text-middle'>-</td>\"+
                                \"<td class='text-center text-middle'>-</td>\"+
                            \"</tr>\"
                        );
                    }
                });
            }, \"json\");
        }
        
        \$(\"#cetak\").on(\"click\", function(){
            window.open(\"";
        // line 178
        echo twig_escape_filter($this->env, site_url("persediaan/print_bukti_pengambilan_barang"), "html", null, true);
        echo "\");
        });
        \$(\"#kembali\").on(\"click\", function(){
            window.location.href = \"";
        // line 181
        echo twig_escape_filter($this->env, site_url("persediaan"), "html", null, true);
        echo "\";
        });
\t});

</script>
";
    }

    public function getTemplateName()
    {
        return "simgo/persediaan/laporan/katalog_bukti_pengambilan_barang.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  283 => 181,  277 => 178,  250 => 154,  228 => 137,  201 => 113,  197 => 112,  193 => 111,  189 => 110,  182 => 109,  142 => 74,  138 => 73,  131 => 72,  110 => 55,  101 => 53,  97 => 52,  65 => 23,  53 => 14,  49 => 13,  39 => 6,  33 => 2,  30 => 1,);
    }
}
