<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		redirect(site_url("secman/authentication"));
	}

	public function dashboard() {
		$this->twig->display("aset/penyusutan.html", array(
			"title" => "Perhitungan Penyusutan Aset"
		));
	}

	public function test() {
		$this->load->library("jasper");

		if (!file_exists("reports/persediaan/barang_keluar2.jasper")) {
			$this->jasper->compile("reports/persediaan/barang_keluar2.jrxml")->execute();
		}

		//mime
		header('Content-Type: application/pdf');
		//nama file
		header('Content-Disposition: inline;filename="' . preg_replace("/[^A-Za-z0-9 ]/", '', $rb["rbnamakegiatan"]) . '.pdf"');
		//tanpa cache
		header('Cache-Control: max-age=0');

		if (PHP_OS === "Linux") {
			setlocale(LC_TIME, "id_ID");
		} else {
			setlocale(LC_TIME, "id");
		}

		$tmpfile = tempnam(sys_get_temp_dir(), 'Rpt') . ".csv";

		// generate csv
		$list = array(
			"value;id;text;satuan",
			"11.554.15.005;11.554.15.005;Kudaku Lari Ke mana;Lembar",
			"11.554.15.015;11.554.15.015;Kuda;Ekor",
			"11.554.15.025;11.554.15.025;Kuda Lumping;Lembar",
			"11.554.15.055;11.554.15.055;Kuda;Ekor",
			"11.554.15.075;11.554.15.075;Kuda Kudaan;Buah"
		);

		$file = fopen($tmpfile,"w");

		foreach ($list as $line) {
			fputcsv($file, explode(';', $line), "|");
		}

		fclose($file);


		$this->jasper->process(
			"reports/persediaan/barang_keluar2.jasper", 
			array(),
			array(
				"type" => "csv",
				"source" => $tmpfile,
				"columns" => "value,id,text,satuan",
				"delimiter" => "|"
			),
			array(
				"pdf"
			)
		)->execute()->output();

		unlink($tmpfile);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */