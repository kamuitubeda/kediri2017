<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pengadaan extends CI_Controller {

	private $per_page = 10;

	function __construct() {
		parent::__construct();
	}

	private function setup_pagination($config = array()) {
		$this->load->model("jurnal_pengadaan_model");
		$this->load->library('pagination');


		$filter = $this->input->get("filter", TRUE);
		$nomor_lokasi = $this->input->get("nomor_lokasi");


		$config['base_url'] = site_url("pengadaan/index?filter=" . $filter . "&nomor_lokasi=" . $nomor_lokasi);
		if (!array_key_exists("total_rows", $config)) {
			$config['total_rows'] = $this->jurnal_pengadaan_model->count_by_nomor_lokasi($nomor_lokasi, $filter);
		}
		$config['per_page'] = $this->per_page;

		$config['uri_segment'] = 4;
		$config['display_pages'] = TRUE;
		$config['page_query_string'] = TRUE;

		$config['first_link'] = '&laquo; First';
		$config['first_tag_open'] = '<li class="prev page">';
		$config['first_tag_close'] = '</li>';

		$config['last_link'] = 'Last &raquo;';
		$config['last_tag_open'] = '<li class="next page">';
		$config['last_tag_close'] = '</li>';

		$config['next_link'] = 'Next &rarr;';
		$config['next_tag_open'] = '<li class="next page">';
		$config['next_tag_close'] = '</li>';

		$config['prev_link'] = '&larr; Previous';
		$config['prev_tag_open'] = '<li class="prev page">';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><a href="">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li class="page">';
		$config['num_tag_close'] = '</li>';

		$config['full_tag_open'] = '<div style="text-align:center;"><ul class="pagination">';

		$config['full_tag_close'] = '</ul></div>';

		$this->pagination->initialize($config); 

		return $this->pagination->create_links();
	}

	public function index() {
		$filter = $this->input->get("filter", TRUE);
		$filter = $filter === FALSE ? "" : $filter;
		$nomor_lokasi = $this->input->get("nomor_lokasi", TRUE);
		$offset = (int)$this->input->get("per_page", TRUE);

		$this->load->model("jurnal_pengadaan_model");

		$cfg = array();
		$cfg["total_rows"] = $this->jurnal_pengadaan_model->count_by_nomor_lokasi($nomor_lokasi, 0, $filter);
		$daftar = array();
		// $daftar = $this->jurnal_pengadaan_model->get_list_by_nomor_lokasi($nomor_lokasi, 0, $filter, $this->per_page, $offset);
		// var_dump($daftar);
		//var_dump($cfg);

		$this->twig->display("pengadaan/daftar.html", array(
			"title" => "Daftar Pengadaan",
			"daftar" => $this->jurnal_pengadaan_model->get_list_by_nomor_lokasi($nomor_lokasi, 0, $filter, $this->per_page, $offset),
			"daftar_posted" => $this->jurnal_pengadaan_model->get_list_by_nomor_lokasi($nomor_lokasi, 1, $filter, $this->per_page, $offset),
			"offset" => $offset,
			"pagination" => $this->setup_pagination($cfg),
			"filter" => $filter
		));
	}

	public function pencarian() {
		$this->load->model("simbada/kib_model");

		$r = new stdClass;
		$r->count = 0;

		$nomor_lokasi = $this->input->get("nomor_lokasi");
		$filter = $this->input->get("filter");
		$filter = $filter === FALSE ? "" : $filter;
		//var_dump($filter);
		$offset = (int)$this->input->get("per_page", TRUE);


		$cfg = array();
		$cfg["total_rows"] = $this->jurnal_pengadaan_model->count_row($nomor_lokasi, $filter);


		$r->data = $this->jurnal_pengadaan_model->get_list_by_filter($nomor_lokasi, $filter, $this->per_page, $offset);
		$r->count = $this->jurnal_pengadaan_model->count_row($nomor_lokasi, $filter);
		$r->pagination = $this->setup_pagination($cfg);
		
		echo json_encode($r);
	}

	public function form($id = FALSE) {
		$this->load->model("jurnal_pengadaan_model");
		$this->load->model("simbada/kamus_spk_model", "kamus_spk_model");

		$isSaved = FALSE;

		if ($id !== FALSE) {
			$jp = $this->jurnal_pengadaan_model->read(array(
				"jpid" => $id
			));

			$jp["jptgl"] = date("d-m-Y", strtotime($jp["jptgl"]));
			$jp["jptglbaterima"] = date("d-m-Y", strtotime($jp["jptglbaterima"]));
			$jp["jptglbaperiksa"] = date("d-m-Y", strtotime($jp["jptglbaperiksa"]));

			$jp["spk"] = $this->kamus_spk_model->read($jp["id_kontrak"]);

			$isSaved = TRUE;

			// tambahkan untuk format tanggal jika ada
		} else {
			$jp = $this->session->flashdata('jp');
		}

		$this->twig->display("pengadaan/form.html", array(
			"title" => "Form Pengadaan",
			"validation_errors" => $this->session->flashdata('validation_errors'),
			"data" => $jp,
			"id" => $id === FALSE ? 0 : $id,
			"isSaved" => $isSaved
		));
	}

	public function submit($id = FALSE) {
		$this->load->model("jurnal_pengadaan_model");
		$this->load->model("kib_pengadaan_model");
		$this->load->model("simbada/kamus_spk_model", "kamus_spk_model");
		$this->load->model("pembayaran_model");

		$is_ajax = $this->input->post("is_ajax", TRUE);

		if ($is_ajax === FALSE) $is_ajax = 0;

		$jpnomorlokasi = $this->input->post("jpnomorlokasi", TRUE);
		$jptgl = $this->input->post("jptgl", TRUE);
		$jpnobaterima = $this->input->post("jpnobaterima", TRUE);
		$jptglbaterima = $this->input->post("jptglbaterima", TRUE);
		$jpnobaperiksa = $this->input->post("jpnobaperiksa", TRUE);
		$jptglbaperiksa = $this->input->post("jptglbaperiksa", TRUE);
		$no_spk_sp_dokumen = $this->input->post("no_spk_sp_dokumen", TRUE);
		$tgl_spk_sp_dokumen = $this->input->post("tgl_spk_sp_dokumen", TRUE);
		$jpkodekegiatan = $this->input->post("kode_kegiatan", TRUE);

		$id_kontrak = $this->input->post("id_kontrak", TRUE);

		if ($jptgl !== FALSE && $jptgl !== '') {
			$jptgl = date_create_from_format('d-m-Y', $jptgl);
			$tahun = date_format($jptgl, 'Y');
			$jptgl = date_format($jptgl, 'Y-m-d');
		} else {
			$jptgl = NULL;
		}

		if ($jptglbaterima !== FALSE && $jptglbaterima !== '') {
			$jptglbaterima = date_create_from_format('d-m-Y', $jptglbaterima);
			$tahunbaterima = date_format($jptglbaterima, 'Y');
			$jptglbaterima = date_format($jptglbaterima, 'Y-m-d');
		} else {
			$jptglbaterima = NULL;
		}

		if ($jptglbaperiksa !== FALSE && $jptglbaperiksa !== '') {
			$jptglbaperiksa = date_create_from_format('d-m-Y', $jptglbaperiksa);
			$tahunbaperiksa = date_format($jptglbaperiksa, 'Y');
			$jptglbaperiksa = date_format($jptglbaperiksa, 'Y-m-d');
		} else {
			$jptglbaperiksa = NULL;
		}


		$data = array(
			"jpnomorlokasi" => $jpnomorlokasi,
			"jptgl" => $jptgl,
			"jpnobaterima" => $jpnobaterima,
			"jptglbaterima" => $jptglbaterima,
			"jpnobaperiksa" => $jpnobaperiksa,
			"jptglbaperiksa" => $jptglbaperiksa,
			"id_kontrak" => $id_kontrak,
			"jpkodekegiatan" => $jpkodekegiatan
		);

		if($id === FALSE) {
			$spk = $this->kamus_spk_model->read($id_kontrak);
			if (!empty($spk)) {
				$id = $this->jurnal_pengadaan_model->create($data);
				if ($id > 0 || $pembayaran > 0) {
					$this->session->set_flashdata('validation_errors', "<b>Data telah disimpan!</b>");
				} else {
					$this->session->set_flashdata('jp', $data);
				}
			} else {
				$this->session->set_flashdata('jp', $data);
				$this->session->set_flashdata('validation_errors', "<b>No.SPK/SP/Kontrak tidak ditemukan!</b>");
			}
		} else {
			$jp = $this->jurnal_pengadaan_model->read(array("jpid" => $id));
			//$id_kontrak = $jp['id_kontrak'];
			$spk = $this->kamus_spk_model->read($id_kontrak); 

			if (!empty($jp) && !empty($spk)) {
				$jp = array_merge($jp, $data);

				$isUpdated = $this->jurnal_pengadaan_model->update($jp);

				if (!$isUpdated) {
					$this->session->set_flashdata('jp', $data);
					$this->session->set_flashdata('validation_errors', "<b>Data tidak dapat diperbarui!</b>");
				}
			}

			if (empty($spk)) {
				$this->session->set_flashdata('validation_errors', "<b>No.SPK/SP/Kontrak tidak ditemukan!</b>");
			}
		}


		//$this->load->model("pembayaran_model");
		$this->pembayaran_model->clear($id);

		if (!$is_ajax) {
			redirect("pengadaan/form/" . $id);
		} else {
			$r = new stdClass;
			$r->success = 1;
			$r->id = $id;

			echo json_encode($r);
		}
	}

	public function hapus($id = FALSE) {
		$this->load->model("jurnal_pengadaan_model");
		if ($id !== FALSE) {
			$this->jurnal_pengadaan_model->delete($id);
		}

		redirect("pengadaan/index");
	}

	public function autocomplete_sub_sub_kel() {
		$tipe = $this->input->get('tipe', TRUE);
		$q = $this->input->get('q', TRUE);
		$page = $this->input->get('page', TRUE);


		$this->load->model("simbada/subsub_kelompok_model", 'subsub_kelompok_model');


		$r = new stdClass;

		$kode_sub_kel_at = '';
		switch ($tipe) {
			case '1a':
				$kode_sub_kel_at = '1.3.1';
				break;
			case '1b':
				$kode_sub_kel_at = '1.3.2';
				break;
			case '1c':
				$kode_sub_kel_at = '1.3.2';
				break;
			case '1d' :
				$kode_sub_kel_at = '1.3.3';
				break;
			case '1e' :
				$kode_sub_kel_at = '1.3.4';
				break;
			case '1f':
				$kode_sub_kel_at = '1.3.5.01';
				break;
			case '1g':
				$kode_sub_kel_at = '1.3.5.02';
				break;
			case '1h':
				$kode_sub_kel_at = '1.3.5.03';
				break;
			case '1i' :
				$kode_sub_kel_at = '1.5.4';
				break;
			case '1j' :
				$kode_sub_kel_at = '1.3';
				break;

			// dst
		}

		

		if (!in_array($tipe, array("1b", "1c"))) {
			// fetch data dari model dari tabel sub_sub_kelompok dimana kode_sub_kel_at start with '1.3.1' utk tanah, dst
			$r->data = __::map($this->subsub_kelompok_model->find_by_kode_sub_kel_at($kode_sub_kel_at, $q, $page), function($e) {
				$e = array_merge($e, array("id" => $e["KODE_SUB_SUB_KELOMPOK"]));
				return $e;
			});
		} else if ($tipe == "1b") {
			$r->data = __::map($this->subsub_kelompok_model->find_by_range_kode_sub_kel_at('1.3.2.07', '1.3.2.20', $q, $page), function($e) {
				$e = array_merge($e, array("id" => $e["KODE_SUB_SUB_KELOMPOK"]));
				return $e;
			});
		} else if ($tipe == "1c") {
			$r->data = __::map($this->subsub_kelompok_model->find_by_range_kode_sub_kel_at('1.3.2.01', '1.3.2.6', $q, $page), function($e) {
				$e = array_merge($e, array("id" => $e["KODE_SUB_SUB_KELOMPOK"]));
				return $e;
			});
		}

		echo json_encode($r);
	}
	
	
	public function submit_rincian_1a() {
		$r = new stdClass;
		$r->status = 0;

		$this->load->model("kib_pengadaan_model");

		$kbstatusguna = $this->input->post("kbstatusguna", TRUE);
		$kbkodesubsubkel = $this->input->post("kbkodesubsubkel", TRUE);
		$kbkoderekneraca = $this->input->post("kbkoderekneraca", TRUE);
		$kbnamabarang = $this->input->post("kbnamabarang", TRUE);
		$kbalamat = $this->input->post("kbalamat", TRUE);
		$kbjumlah = $this->input->post("kbjumlah", TRUE);
		$kbsatuan = $this->input->post("kbsatuan", TRUE);
		$kbhargasatuan = $this->input->post("kbhargasatuan", TRUE);
		$kbhargatotal = $this->input->post("kbhargatotal", TRUE);
		$kbpajak = $this->input->post("kbpajak", TRUE);
		$kbhargatotalpajak = $this->input->post("kbhargatotalpajak", TRUE);
		$kbrencanaalokasi = $this->input->post("kbrencanaalokasi", TRUE);
		$kbpenggunaan = $this->input->post("kbpenggunaan", TRUE);
		$kbketerangan = $this->input->post("kbketerangan", TRUE);
		$kbstatustanah = $this->input->post("kbstatustanah", TRUE);
		$kbnosertifikat = $this->input->post("kbnosertifikat", TRUE);
		$kbtglsertifikat = $this->input->post("kbtglsertifikat", TRUE);
		$kbansertifikat = $this->input->post("kbansertifikat", TRUE);
		$kbpanjang = $this->input->post("kbpanjang", TRUE);
		$kblebar = $this->input->post("kblebar", TRUE);
		$kbluas = $this->input->post("kbluas", TRUE);
		$jpid = $this->input->post("jpid", TRUE);
		$kbid = $this->input->post("kbid", TRUE);

		if ($kbtglsertifikat !== FALSE && $kbtglsertifikat !== '') {
			$kbtglsertifikat = date_create_from_format('d-m-Y', $kbtglsertifikat);
			$kbtglsertifikat = date_format($kbtglsertifikat, 'Y-m-d');
		} else {
			$kbtglsertifikat = NULL;
		}

		if ($kbid === FALSE) {
			$isCreated = $this->kib_pengadaan_model->create(array(
				"kbstatusguna" => $kbstatusguna,
				"kbkodesubsubkel" => $kbkodesubsubkel,
				"kbkoderekneraca" => $kbkoderekneraca,
				"kbnamabarang" => $kbnamabarang,
				"kbalamat" => $kbalamat,
				"kbjumlah" => $kbjumlah,
				"kbsatuan" => $kbsatuan,
				"kbhargasatuan" => $kbhargasatuan,
				"kbhargatotal" => $kbhargatotal,
				"kbpajak" => $kbpajak,
				"kbhargatotalpajak" => $kbhargatotalpajak,
				"kbrencanaalokasi" => $kbrencanaalokasi,
				"kbpenggunaan" => $kbpenggunaan,
				"kbketerangan" => $kbketerangan,
				"kbstatustanah" => $kbstatustanah,
				"kbnosertifikat" => $kbnosertifikat,
				"kbtglsertifikat" => $kbtglsertifikat,
				"kbansertifikat" => $kbansertifikat,
				"kbpanjang" => $kbpanjang,
				"kblebar" => $kblebar,
				"kbluas" => $kbluas,
				"kbjenisaset" => 'A',
				"kbbidangbarang" => 'A',
				"jpid" => $jpid
			));

			if ($isCreated > 0) {
				$r->status = 1;
			}
		} else {
			$o = $this->kib_pengadaan_model->read(array("kbid" => $kbid));
			$o = array_merge($o, array(
				"kbstatusguna" => $kbstatusguna,
				"kbkodesubsubkel" => $kbkodesubsubkel,
				"kbkoderekneraca" => $kbkoderekneraca,
				"kbnamabarang" => $kbnamabarang,
				"kbalamat" => $kbalamat,
				"kbjumlah" => $kbjumlah,
				"kbsatuan" => $kbsatuan,
				"kbhargasatuan" => $kbhargasatuan,
				"kbhargatotal" => $kbhargatotal,
				"kbpajak" => $kbpajak,
				"kbhargatotalpajak" => $kbhargatotalpajak,
				"kbrencanaalokasi" => $kbrencanaalokasi,
				"kbpenggunaan" => $kbpenggunaan,
				"kbketerangan" => $kbketerangan,
				"kbstatustanah" => $kbstatustanah,
				"kbnosertifikat" => $kbnosertifikat,
				"kbtglsertifikat" => $kbtglsertifikat,
				"kbansertifikat" => $kbansertifikat,
				"kbpanjang" => $kbpanjang,
				"kblebar" => $kblebar,
				"kbluas" => $kbluas,
				"kbjenisaset" => 'A',
				"kbbidangbarang" => 'A',
				"jpid" => $jpid
			));

			$isUpdated = $this->kib_pengadaan_model->update($o);
			if ($isUpdated > 0) {
				$r->status = 1;
			}
		}

		echo json_encode($r);
	}

	public function submit_rincian_1b() {
		$r = new stdClass;
		$r->status = 0;

		$this->load->model("kib_pengadaan_model");

		$kbstatusguna = $this->input->post("kbstatusguna", TRUE);
		$kbkodesubsubkel = $this->input->post("kbkodesubsubkel", TRUE);
		$kbkoderekneraca = $this->input->post("kbkoderekneraca", TRUE);
		$kbkoderuangan = $this->input->post("kbkoderuangan", TRUE);
		$kbnamabarang = $this->input->post("kbnamabarang", TRUE);
		$kbalamat = $this->input->post("kbalamat", TRUE);
		$kbtipe = $this->input->post("kbtipe", TRUE);
		$kbjumlah = $this->input->post("kbjumlah", TRUE);
		$kbsatuan = $this->input->post("kbsatuan", TRUE);
		$kbhargasatuan = $this->input->post("kbhargasatuan", TRUE);
		$kbhargatotal = $this->input->post("kbhargatotal", TRUE);
		$kbpajak = $this->input->post("kbpajak", TRUE);
		$kbhargatotalpajak = $this->input->post("kbhargatotalpajak", TRUE);
		$kbrencanaalokasi = $this->input->post("kbrencanaalokasi", TRUE);
		$kbpenggunaan = $this->input->post("kbpenggunaan", TRUE);
		$kbketerangan = $this->input->post("kbketerangan", TRUE);
		$kbukuran = $this->input->post("kbukuran", TRUE);
		$kbwarna = $this->input->post("kbwarna", TRUE);
		$kbbahan = $this->input->post("kbbahan", TRUE);
		$jpid = $this->input->post("jpid", TRUE);
		$kbid = $this->input->post("kbid", TRUE);

		if($kbid === FALSE) {
			$isCreated = $this->kib_pengadaan_model->create(array(
				"kbstatusguna" => $kbstatusguna,
				"kbkodesubsubkel" => $kbkodesubsubkel,
				"kbkoderekneraca" => $kbkoderekneraca,
				"kbkoderuangan" => $kbkoderuangan,
				"kbnamabarang" => $kbnamabarang,
				"kbalamat" => $kbalamat,
				"kbtipe" => $kbtipe,
				"kbjumlah" => $kbjumlah,
				"kbsatuan" => $kbsatuan,
				"kbhargasatuan" => $kbhargasatuan,
				"kbhargatotal" => $kbhargatotal,
				"kbpajak" => $kbpajak,
				"kbhargatotalpajak" => $kbhargatotalpajak,
				"kbrencanaalokasi" => $kbrencanaalokasi,
				"kbpenggunaan" => $kbpenggunaan,
				"kbketerangan" => $kbketerangan,
				"kbukuran" => $kbukuran,
				"kbwarna" => $kbwarna,
				"kbbahan" => $kbbahan,
				"kbjenisaset" => 'A',
				"kbbidangbarang" => 'B',
				"jpid" => $jpid
			));

			if ($isCreated > 0) {
				$r->status = 1;
			}
		} else {
			$o = $this->kib_pengadaan_model->read(array("kbid" => $kbid));
			$o = array_merge($o, array(
				"kbstatusguna" => $kbstatusguna,
				"kbkodesubsubkel" => $kbkodesubsubkel,
				"kbkoderekneraca" => $kbkoderekneraca,
				"kbkoderuangan" => $kbkoderuangan,
				"kbnamabarang" => $kbnamabarang,
				"kbalamat" => $kbalamat,
				"kbtipe" => $kbtipe,
				"kbjumlah" => $kbjumlah,
				"kbsatuan" => $kbsatuan,
				"kbhargasatuan" => $kbhargasatuan,
				"kbhargatotal" => $kbhargatotal,
				"kbpajak" => $kbpajak,
				"kbhargatotalpajak" => $kbhargatotalpajak,
				"kbrencanaalokasi" => $kbrencanaalokasi,
				"kbpenggunaan" => $kbpenggunaan,
				"kbketerangan" => $kbketerangan,
				"kbukuran" => $kbukuran,
				"kbwarna" => $kbwarna,
				"kbbahan" => $kbbahan,
				"kbjenisaset" => 'A',
				"kbbidangbarang" => 'B',
				"jpid" => $jpid
			));

			$isUpdated = $this->kib_pengadaan_model->update($o);
			if ($isUpdated > 0) {
				$r->status = 1;
			}
		}
		
		echo json_encode($r);
	}

	public function submit_rincian_1c() {
		$r = new stdClass;
		$r->status = 0;

		$this->load->model("kib_pengadaan_model");

		$kbstatusguna = $this->input->post("kbstatusguna", TRUE);
		$kbkodesubsubkel = $this->input->post("kbkodesubsubkel", TRUE);
		$kbkoderekneraca = $this->input->post("kbkoderekneraca", TRUE);
		$kbnamabarang = $this->input->post("kbnamabarang", TRUE);
		$kbalamat = $this->input->post("kbalamat", TRUE);
		$kbtipe = $this->input->post("kbtipe", TRUE);
		$kbjumlah = $this->input->post("kbjumlah", TRUE);
		$kbsatuan = $this->input->post("kbsatuan", TRUE);
		$kbhargasatuan = $this->input->post("kbhargasatuan", TRUE);
		$kbhargatotal = $this->input->post("kbhargatotal", TRUE);
		$kbpajak = $this->input->post("kbpajak", TRUE);
		$kbhargatotalpajak = $this->input->post("kbhargatotalpajak", TRUE);
		$kbrencanaalokasi = $this->input->post("kbrencanaalokasi", TRUE);
		$kbketerangan = $this->input->post("kbketerangan", TRUE);
		$kbnobpkb = $this->input->post("kbnobpkb", TRUE);
		$kbnopolisi = $this->input->post("kbnopolisi", TRUE);
		$kbnorangka = $this->input->post("kbnorangka", TRUE);
		$kbnomesin = $this->input->post("kbnomesin", TRUE);
		$kbwarna = $this->input->post("kbwarna", TRUE);
		$kbkapasitasslinder = $this->input->post("kbkapasitasslinder", TRUE);
		$kbnostnk = $this->input->post("kbnostnk", TRUE);
		$kbtglstnk = $this->input->post("kbtglstnk", TRUE);
		$kbbahan = $this->input->post("kbbahan", TRUE);
		$kbthnbuat = $this->input->post("kbthnbuat", TRUE);
		$kbthnrakit = $this->input->post("kbthnrakit", TRUE);
		$jpid = $this->input->post("jpid", TRUE);
		$kbid = $this->input->post("kbid", TRUE);

		if ($kbtglstnk !== FALSE && $kbtglstnk !== '') {
			$kbtglstnk = date_create_from_format('d-m-Y', $kbtglstnk);
			$kbtglstnk = date_format($kbtglstnk, 'Y-m-d');
		} else {
			$kbtglstnk = NULL;
		}

		if($kbid === FALSE) {
			$isCreated = $this->kib_pengadaan_model->create(array(
				"kbstatusguna" => $kbstatusguna,
				"kbkodesubsubkel" => $kbkodesubsubkel,
				"kbkoderekneraca" => $kbkoderekneraca,
				"kbnamabarang" => $kbnamabarang,
				"kbalamat" => $kbalamat,
				"kbtipe" => $kbtipe,
				"kbjumlah" => $kbjumlah,
				"kbsatuan" => $kbsatuan,
				"kbhargasatuan" => $kbhargasatuan,
				"kbhargatotal" => $kbhargatotal,
				"kbpajak" => $kbpajak,
				"kbhargatotalpajak" => $kbhargatotalpajak,
				"kbrencanaalokasi" => $kbrencanaalokasi,
				"kbketerangan" => $kbketerangan,
				"kbnobpkb" => $kbnobpkb,
				"kbnopolisi" => $kbnopolisi,
				"kbnorangka" => $kbnorangka,
				"kbnomesin" => $kbnomesin,
				"kbwarna" => $kbwarna,
				"kbkapasitasslinder" => $kbkapasitasslinder,
				"kbnostnk" => $kbnostnk,
				"kbtglstnk" => $kbtglstnk,
				"kbbahan" => $kbbahan,
				"kbthnbuat" => $kbthnbuat,
				"kbthnrakit" => $kbthnrakit,
				"kbjenisaset" => 'A',
				"kbbidangbarang" => 'B',
				"jpid" => $jpid
			));

			if ($isCreated > 0) {
				$r->status = 1;
			}
		} else {
			$o = $this->kib_pengadaan_model->read(array("kbid" => $kbid));
			$o = array_merge($o, array(
				"kbstatusguna" => $kbstatusguna,
				"kbkodesubsubkel" => $kbkodesubsubkel,
				"kbkoderekneraca" => $kbkoderekneraca,
				"kbnamabarang" => $kbnamabarang,
				"kbalamat" => $kbalamat,
				"kbtipe" => $kbtipe,
				"kbjumlah" => $kbjumlah,
				"kbsatuan" => $kbsatuan,
				"kbhargasatuan" => $kbhargasatuan,
				"kbhargatotal" => $kbhargatotal,
				"kbpajak" => $kbpajak,
				"kbhargatotalpajak" => $kbhargatotalpajak,
				"kbrencanaalokasi" => $kbrencanaalokasi,
				"kbketerangan" => $kbketerangan,
				"kbnobpkb" => $kbnobpkb,
				"kbnopolisi" => $kbnopolisi,
				"kbnorangka" => $kbnorangka,
				"kbnomesin" => $kbnomesin,
				"kbwarna" => $kbwarna,
				"kbkapasitasslinder" => $kbkapasitasslinder,
				"kbnostnk" => $kbnostnk,
				"kbtglstnk" => $kbtglstnk,
				"kbbahan" => $kbbahan,
				"kbthnbuat" => $kbthnbuat,
				"kbthnrakit" => $kbthnrakit,
				"kbjenisaset" => 'A',
				"kbbidangbarang" => 'B',
				"jpid" => $jpid
			));

			$isUpdated = $this->kib_pengadaan_model->update($o);
			if ($isUpdated > 0) {
				$r->status = 1;
			}
		}

		echo json_encode($r);
	}

	public function submit_rincian_1d() {
		$r = new stdClass;
		$r->status = 0;

		$this->load->model("kib_pengadaan_model");

		$kbstatusguna = $this->input->post("kbstatusguna", TRUE);
		$kbkodesubsubkel = $this->input->post("kbkodesubsubkel", TRUE);
		$kbkoderekneraca = $this->input->post("kbkoderekneraca", TRUE);
		$kbnamabarang = $this->input->post("kbnamabarang", TRUE);
		$kbalamat = $this->input->post("kbalamat", TRUE);
		$kbtipe = $this->input->post("kbtipe", TRUE);
		$kbjumlah = $this->input->post("kbjumlah", TRUE);
		$kbsatuan = $this->input->post("kbsatuan", TRUE);
		$kbhargasatuan = $this->input->post("kbhargasatuan", TRUE);
		$kbhargatotal = $this->input->post("kbhargatotal", TRUE);
		$kbpajak = $this->input->post("kbpajak", TRUE);
		$kbhargatotalpajak = $this->input->post("kbhargatotalpajak", TRUE);
		$kbrencanaalokasi = $this->input->post("kbrencanaalokasi", TRUE);
		$kbketerangan = $this->input->post("kbketerangan", TRUE);
		$kbbahan = $this->input->post("kbbahan", TRUE);
		$kbjeniskonstruksi = $this->input->post("kbjeniskonstruksi", TRUE);
		$kbluas = $this->input->post("kbluas", TRUE);
		$kbluasbangunan = $this->input->post("kbluasbangunan", TRUE);
		$kbnoreginduktanah = $this->input->post("kbnoreginduktanah", TRUE);
		$kbstatustanah = $this->input->post("kbstatustanah", TRUE);
		$jpid = $this->input->post("jpid", TRUE);
		$kbid = $this->input->post("kbid", TRUE);

		if($kbid === FALSE) {
			$isCreated = $this->kib_pengadaan_model->create(array(
				"kbstatusguna" => $kbstatusguna,
				"kbkodesubsubkel" => $kbkodesubsubkel,
				"kbkoderekneraca" => $kbkoderekneraca,
				"kbnamabarang" => $kbnamabarang,
				"kbalamat" => $kbalamat,
				"kbtipe" => $kbtipe,
				"kbjumlah" => $kbjumlah,
				"kbsatuan" => $kbsatuan,
				"kbhargasatuan" => $kbhargasatuan,
				"kbhargatotal" => $kbhargatotal,
				"kbpajak" => $kbpajak,
				"kbhargatotalpajak" => $kbhargatotalpajak,
				"kbrencanaalokasi" => $kbrencanaalokasi,
				"kbketerangan" => $kbketerangan,
				"kbbahan" => $kbbahan,
				"kbjeniskonstruksi" => $kbjeniskonstruksi,
				"kbluas" => $kbluas,
				"kbluasbangunan" => $kbluasbangunan,
				"kbnoreginduktanah" => $kbnoreginduktanah,
				"kbstatustanah" => $kbstatustanah,
				"kbjenisaset" => 'A',
				"kbbidangbarang" => 'C',
				"jpid" => $jpid
			));

			if ($isCreated > 0) {
				$r->status = 1;
			}
		} else {
			$o = $this->kib_pengadaan_model->read(array("kbid" => $kbid));
			$o = array_merge($o, array(
				"kbstatusguna" => $kbstatusguna,
				"kbkodesubsubkel" => $kbkodesubsubkel,
				"kbkoderekneraca" => $kbkoderekneraca,
				"kbnamabarang" => $kbnamabarang,
				"kbalamat" => $kbalamat,
				"kbtipe" => $kbtipe,
				"kbjumlah" => $kbjumlah,
				"kbsatuan" => $kbsatuan,
				"kbhargasatuan" => $kbhargasatuan,
				"kbhargatotal" => $kbhargatotal,
				"kbpajak" => $kbpajak,
				"kbhargatotalpajak" => $kbhargatotalpajak,
				"kbrencanaalokasi" => $kbrencanaalokasi,
				"kbketerangan" => $kbketerangan,
				"kbbahan" => $kbbahan,
				"kbjeniskonstruksi" => $kbjeniskonstruksi,
				"kbluas" => $kbluas,
				"kbluasbangunan" => $kbluasbangunan,
				"kbnoreginduktanah" => $kbnoreginduktanah,
				"kbstatustanah" => $kbstatustanah,
				"kbjenisaset" => 'A',
				"kbbidangbarang" => 'C',
				"jpid" => $jpid
			));

			$isUpdated = $this->kib_pengadaan_model->update($o);
			if ($isUpdated > 0) {
				$r->status = 1;
			}
		}

		echo json_encode($r);
	}

	public function submit_rincian_1e() {
		$r = new stdClass;
		$r->status = 0;

		$this->load->model("kib_pengadaan_model");

		$kbstatusguna = $this->input->post("kbstatusguna", TRUE);
		$kbkodesubsubkel = $this->input->post("kbkodesubsubkel", TRUE);
		$kbkoderekneraca = $this->input->post("kbkoderekneraca", TRUE);
		$kbnamabarang = $this->input->post("kbnamabarang", TRUE);
		$kbalamat = $this->input->post("kbalamat", TRUE);
		$kbtipe = $this->input->post("kbtipe", TRUE);
		$kbjumlah = $this->input->post("kbjumlah", TRUE);
		$kbsatuan = $this->input->post("kbsatuan", TRUE);
		$kbhargasatuan = $this->input->post("kbhargasatuan", TRUE);
		$kbhargatotal = $this->input->post("kbhargatotal", TRUE);
		$kbpajak = $this->input->post("kbpajak", TRUE);
		$kbhargatotalpajak = $this->input->post("kbhargatotalpajak", TRUE);
		$kbrencanaalokasi = $this->input->post("kbrencanaalokasi", TRUE);
		$kbketerangan = $this->input->post("kbketerangan", TRUE);
		$kbbahan = $this->input->post("kbbahan", TRUE);
		$kbjeniskonstruksi = $this->input->post("kbjeniskonstruksi", TRUE);
		$kbnamaruas = $this->input->post("kbnamaruas", TRUE);
		$kbpangkalruas = $this->input->post("kbpangkalruas", TRUE);
		$kbujungruas = $this->input->post("kbujungruas", TRUE);
		$kbpanjang = $this->input->post("kbpanjang", TRUE);
		$kblebar = $this->input->post("kblebar", TRUE);
		$kbluasbangunan = $this->input->post("kbluasbangunan", TRUE);
		$jpid = $this->input->post("jpid", TRUE);
		$kbid = $this->input->post("kbid", TRUE);

		if($kbid === FALSE) {
			$isCreated = $this->kib_pengadaan_model->create(array(
				"kbstatusguna" => $kbstatusguna,
				"kbkodesubsubkel" => $kbkodesubsubkel,
				"kbkoderekneraca" => $kbkoderekneraca,
				"kbnamabarang" => $kbnamabarang,
				"kbalamat" => $kbalamat,
				"kbtipe" => $kbtipe,
				"kbjumlah" => $kbjumlah,
				"kbsatuan" => $kbsatuan,
				"kbhargasatuan" => $kbhargasatuan,
				"kbhargatotal" => $kbhargatotal,
				"kbpajak" => $kbpajak,
				"kbhargatotalpajak" => $kbhargatotalpajak,
				"kbrencanaalokasi" => $kbrencanaalokasi,
				"kbketerangan" => $kbketerangan,
				"kbbahan" => $kbbahan,
				"kbjeniskonstruksi" => $kbjeniskonstruksi,
				"kbnamaruas" => $kbnamaruas,
				"kbpangkalruas" => $kbpangkalruas,
				"kbujungruas" => $kbujungruas,
				"kbpanjang" => $kbpanjang,
				"kblebar" => $kblebar,
				"kbluasbangunan" => $kbluasbangunan,
				"kbjenisaset" => 'A',
				"kbbidangbarang" => 'D',
				"jpid" => $jpid
			));

			if ($isCreated > 0) {
				$r->status = 1;
			}
		} else {
			$o = $this->kib_pengadaan_model->read(array("kbid" => $kbid));
			$o = array_merge($o, array(
				"kbstatusguna" => $kbstatusguna,
				"kbkodesubsubkel" => $kbkodesubsubkel,
				"kbkoderekneraca" => $kbkoderekneraca,
				"kbnamabarang" => $kbnamabarang,
				"kbalamat" => $kbalamat,
				"kbtipe" => $kbtipe,
				"kbjumlah" => $kbjumlah,
				"kbsatuan" => $kbsatuan,
				"kbhargasatuan" => $kbhargasatuan,
				"kbhargatotal" => $kbhargatotal,
				"kbpajak" => $kbpajak,
				"kbhargatotalpajak" => $kbhargatotalpajak,
				"kbrencanaalokasi" => $kbrencanaalokasi,
				"kbketerangan" => $kbketerangan,
				"kbbahan" => $kbbahan,
				"kbjeniskonstruksi" => $kbjeniskonstruksi,
				"kbnamaruas" => $kbnamaruas,
				"kbpangkalruas" => $kbpangkalruas,
				"kbujungruas" => $kbujungruas,
				"kbpanjang" => $kbpanjang,
				"kblebar" => $kblebar,
				"kbluasbangunan" => $kbluasbangunan,
				"kbjenisaset" => 'A',
				"kbbidangbarang" => 'D',
				"jpid" => $jpid
			));

			$isUpdated = $this->kib_pengadaan_model->update($o);
			if ($isUpdated > 0) {
				$r->status = 1;
			}
		}

		echo json_encode($r);
	}

	public function submit_rincian_1f() {
		$r = new stdClass;
		$r->status = 0;

		$this->load->model("kib_pengadaan_model");

		$kbstatusguna = $this->input->post("kbstatusguna", TRUE);
		$kbkodesubsubkel = $this->input->post("kbkodesubsubkel", TRUE);
		$kbkoderekneraca = $this->input->post("kbkoderekneraca", TRUE);
		$kbkoderuangan = $this->input->post("kbkoderuangan", TRUE);
		$kbnamabarang = $this->input->post("kbnamabarang", TRUE);
		$kbalamat = $this->input->post("kbalamat", TRUE);
		$kbtipe = $this->input->post("kbtipe", TRUE);
		$kbjumlah = $this->input->post("kbjumlah", TRUE);
		$kbsatuan = $this->input->post("kbsatuan", TRUE);
		$kbhargasatuan = $this->input->post("kbhargasatuan", TRUE);
		$kbhargatotal = $this->input->post("kbhargatotal", TRUE);
		$kbpajak = $this->input->post("kbpajak", TRUE);
		$kbhargatotalpajak = $this->input->post("kbhargatotalpajak", TRUE);
		$kbrencanaalokasi = $this->input->post("kbrencanaalokasi", TRUE);
		$kbketerangan = $this->input->post("kbketerangan", TRUE);
		$jpid = $this->input->post("jpid", TRUE);
		$kbid = $this->input->post("kbid", TRUE);

		if($kbid === FALSE) {
			$isCreated = $this->kib_pengadaan_model->create(array(
				"kbstatusguna" => $kbstatusguna,
				"kbkodesubsubkel" => $kbkodesubsubkel,
				"kbkoderekneraca" => $kbkoderekneraca,
				"kbkoderuangan" => $kbkoderuangan,
				"kbnamabarang" => $kbnamabarang,
				"kbalamat" => $kbalamat,
				"kbtipe" => $kbtipe,
				"kbjumlah" => $kbjumlah,
				"kbsatuan" => $kbsatuan,
				"kbhargasatuan" => $kbhargasatuan,
				"kbhargatotal" => $kbhargatotal,
				"kbpajak" => $kbpajak,
				"kbhargatotalpajak" => $kbhargatotalpajak,
				"kbrencanaalokasi" => $kbrencanaalokasi,
				"kbketerangan" => $kbketerangan,
				"kbjenisaset" => 'A',
				"kbbidangbarang" => 'E',
				"jpid" => $jpid
			));

			if ($isCreated > 0) {
				$r->status = 1;
			}
		} else {
			$o = $this->kib_pengadaan_model->read(array("kbid" => $kbid));
			$o = array_merge($o, array(
				"kbstatusguna" => $kbstatusguna,
				"kbkodesubsubkel" => $kbkodesubsubkel,
				"kbkoderekneraca" => $kbkoderekneraca,
				"kbkoderuangan" => $kbkoderuangan,
				"kbnamabarang" => $kbnamabarang,
				"kbalamat" => $kbalamat,
				"kbtipe" => $kbtipe,
				"kbjumlah" => $kbjumlah,
				"kbsatuan" => $kbsatuan,
				"kbhargasatuan" => $kbhargasatuan,
				"kbhargatotal" => $kbhargatotal,
				"kbpajak" => $kbpajak,
				"kbhargatotalpajak" => $kbhargatotalpajak,
				"kbrencanaalokasi" => $kbrencanaalokasi,
				"kbketerangan" => $kbketerangan,
				"kbjenisaset" => 'A',
				"kbbidangbarang" => 'E',
				"jpid" => $jpid
			));

			$isUpdated = $this->kib_pengadaan_model->update($o);
			if ($isUpdated > 0) {
				$r->status = 1;
			}
		}

		echo json_encode($r);
	}

	public function submit_rincian_1g() {
		$r = new stdClass;
		$r->status = 0;

		$this->load->model("kib_pengadaan_model");

		$kbstatusguna = $this->input->post("kbstatusguna", TRUE);
		$kbkodesubsubkel = $this->input->post("kbkodesubsubkel", TRUE);
		$kbkoderekneraca = $this->input->post("kbkoderekneraca", TRUE);
		$kbkoderuangan = $this->input->post("kbkoderuangan", TRUE);
		$kbnamabarang = $this->input->post("kbnamabarang", TRUE);
		$kbalamat = $this->input->post("kbalamat", TRUE);
		$kbtipe = $this->input->post("kbtipe", TRUE);
		$kbjumlah = $this->input->post("kbjumlah", TRUE);
		$kbsatuan = $this->input->post("kbsatuan", TRUE);
		$kbhargasatuan = $this->input->post("kbhargasatuan", TRUE);
		$kbhargatotal = $this->input->post("kbhargatotal", TRUE);
		$kbpajak = $this->input->post("kbpajak", TRUE);
		$kbhargatotalpajak = $this->input->post("kbhargatotalpajak", TRUE);
		$kbrencanaalokasi = $this->input->post("kbrencanaalokasi", TRUE);
		$kbketerangan = $this->input->post("kbketerangan", TRUE);
		$jpid = $this->input->post("jpid", TRUE);
		$kbid = $this->input->post("kbid", TRUE);

		if($kbid === FALSE) {
			$isCreated = $this->kib_pengadaan_model->create(array(
				"kbstatusguna" => $kbstatusguna,
				"kbkodesubsubkel" => $kbkodesubsubkel,
				"kbkoderekneraca" => $kbkoderekneraca,
				"kbkoderuangan" => $kbkoderuangan,
				"kbnamabarang" => $kbnamabarang,
				"kbalamat" => $kbalamat,
				"kbtipe" => $kbtipe,								
				"kbjumlah" => $kbjumlah,
				"kbsatuan" => $kbsatuan,
				"kbhargasatuan" => $kbhargasatuan,
				"kbhargatotal" => $kbhargatotal,
				"kbpajak" => $kbpajak,
				"kbhargatotalpajak" => $kbhargatotalpajak,
				"kbrencanaalokasi" => $kbrencanaalokasi,
				"kbketerangan" => $kbketerangan,
				"kbjenisaset" => 'A',
				"kbbidangbarang" => 'E',
				"jpid" => $jpid
			));

			if ($isCreated > 0) {
				$r->status = 1;
			}
		} else {
			$o = $this->kib_pengadaan_model->read(array("kbid" => $kbid));
			$o = array_merge($o, array(
				"kbstatusguna" => $kbstatusguna,
				"kbkodesubsubkel" => $kbkodesubsubkel,
				"kbkoderekneraca" => $kbkoderekneraca,
				"kbkoderuangan" => $kbkoderuangan,
				"kbnamabarang" => $kbnamabarang,
				"kbalamat" => $kbalamat,
				"kbtipe" => $kbtipe,
				"kbjumlah" => $kbjumlah,
				"kbsatuan" => $kbsatuan,
				"kbhargasatuan" => $kbhargasatuan,
				"kbhargatotal" => $kbhargatotal,
				"kbpajak" => $kbpajak,
				"kbhargatotalpajak" => $kbhargatotalpajak,
				"kbrencanaalokasi" => $kbrencanaalokasi,
				"kbketerangan" => $kbketerangan,
				"kbjenisaset" => 'A',
				"kbbidangbarang" => 'E',
				"jpid" => $jpid
			));

			$isUpdated = $this->kib_pengadaan_model->update($o);
			if ($isUpdated > 0) {
				$r->status = 1;
			}
		}

		echo json_encode($r);
	}

	public function submit_rincian_1h() {
		$r = new stdClass;
		$r->status = 0;

		$this->load->model("kib_pengadaan_model");

		$kbstatusguna = $this->input->post("kbstatusguna", TRUE);
		$kbkodesubsubkel = $this->input->post("kbkodesubsubkel", TRUE);
		$kbkoderekneraca = $this->input->post("kbkoderekneraca", TRUE);
		$kbkoderuangan = $this->input->post("kbkoderuangan", TRUE);
		$kbnamabarang = $this->input->post("kbnamabarang", TRUE);
		$kbalamat = $this->input->post("kbalamat", TRUE);
		$kbtipe = $this->input->post("kbtipe", TRUE);
		$kbjumlah = $this->input->post("kbjumlah", TRUE);
		$kbsatuan = $this->input->post("kbsatuan", TRUE);
		$kbhargasatuan = $this->input->post("kbhargasatuan", TRUE);
		$kbhargatotal = $this->input->post("kbhargatotal", TRUE);
		$kbpajak = $this->input->post("kbpajak", TRUE);
		$kbhargatotalpajak = $this->input->post("kbhargatotalpajak", TRUE);
		$kbrencanaalokasi = $this->input->post("kbrencanaalokasi", TRUE);
		$kbketerangan = $this->input->post("kbketerangan", TRUE);
		$jpid = $this->input->post("jpid", TRUE);
		$kbid = $this->input->post("kbid", TRUE);

		if($kbid === FALSE) {
			$isCreated = $this->kib_pengadaan_model->create(array(
				"kbstatusguna" => $kbstatusguna,
				"kbkodesubsubkel" => $kbkodesubsubkel,
				"kbkoderekneraca" => $kbkoderekneraca,
				"kbkoderuangan" => $kbkoderuangan,
				"kbnamabarang" => $kbnamabarang,
				"kbalamat" => $kbalamat,
				"kbtipe" => $kbtipe,
				"kbjumlah" => $kbjumlah,
				"kbsatuan" => $kbsatuan,
				"kbhargasatuan" => $kbhargasatuan,
				"kbhargatotal" => $kbhargatotal,
				"kbpajak" => $kbpajak,
				"kbhargatotalpajak" => $kbhargatotalpajak,
				"kbrencanaalokasi" => $kbrencanaalokasi,
				"kbketerangan" => $kbketerangan,
				"kbjenisaset" => 'A',
				"kbbidangbarang" => 'E',
				"jpid" => $jpid
			));

			if ($isCreated > 0) {
				$r->status = 1;
			}
		} else {
			$o = $this->kib_pengadaan_model->read(array("kbid" => $kbid));
			$o = array_merge($o, array(
				"kbstatusguna" => $kbstatusguna,
				"kbkodesubsubkel" => $kbkodesubsubkel,
				"kbkoderekneraca" => $kbkoderekneraca,
				"kbkoderuangan" => $kbkoderuangan,
				"kbnamabarang" => $kbnamabarang,
				"kbalamat" => $kbalamat,
				"kbtipe" => $kbtipe,
				"kbjumlah" => $kbjumlah,
				"kbsatuan" => $kbsatuan,
				"kbhargasatuan" => $kbhargasatuan,
				"kbhargatotal" => $kbhargatotal,
				"kbpajak" => $kbpajak,
				"kbhargatotalpajak" => $kbhargatotalpajak,
				"kbrencanaalokasi" => $kbrencanaalokasi,
				"kbketerangan" => $kbketerangan,
				"kbjenisaset" => 'A',
				"kbbidangbarang" => 'E',
				"jpid" => $jpid
			));

			$isUpdated = $this->kib_pengadaan_model->update($o);
			if ($isUpdated > 0) {
				$r->status = 1;
			}
		}

		echo json_encode($r);
	}

	public function submit_rincian_1i() {
		$r = new stdClass;
		$r->status = 0;

		$this->load->model("kib_pengadaan_model");

		$kbstatusguna = $this->input->post("kbstatusguna", TRUE);
		$kbkodesubsubkel = $this->input->post("kbkodesubsubkel", TRUE);
		$kbkoderekneraca = $this->input->post("kbkoderekneraca", TRUE);
		$kbkoderuangan = $this->input->post("kbkoderuangan", TRUE);
		$kbnamabarang = $this->input->post("kbnamabarang", TRUE);
		$kbalamat = $this->input->post("kbalamat", TRUE);
		$kbtipe = $this->input->post("kbtipe", TRUE);
		$kbjumlah = $this->input->post("kbjumlah", TRUE);
		$kbsatuan = $this->input->post("kbsatuan", TRUE);
		$kbhargasatuan = $this->input->post("kbhargasatuan", TRUE);
		$kbhargatotal = $this->input->post("kbhargatotal", TRUE);
		$kbpajak = $this->input->post("kbpajak", TRUE);
		$kbhargatotalpajak = $this->input->post("kbhargatotalpajak", TRUE);
		$kbrencanaalokasi = $this->input->post("kbrencanaalokasi", TRUE);
		$kbketerangan = $this->input->post("kbketerangan", TRUE);
		$kbnoseri = $this->input->post("kbnoseri", TRUE);
		$jpid = $this->input->post("jpid", TRUE);
		$kbid = $this->input->post("kbid", TRUE);

		if($kbid === FALSE) {
			$isCreated = $this->kib_pengadaan_model->create(array(
				"kbstatusguna" => $kbstatusguna,
				"kbkodesubsubkel" => $kbkodesubsubkel,
				"kbkoderekneraca" => $kbkoderekneraca,
				"kbkoderuangan" => $kbkoderuangan,
				"kbnamabarang" => $kbnamabarang,
				"kbalamat" => $kbalamat,
				"kbtipe" => $kbtipe,
				"kbjumlah" => $kbjumlah,
				"kbsatuan" => $kbsatuan,
				"kbhargasatuan" => $kbhargasatuan,
				"kbhargatotal" => $kbhargatotal,
				"kbpajak" => $kbpajak,
				"kbhargatotalpajak" => $kbhargatotalpajak,
				"kbrencanaalokasi" => $kbrencanaalokasi,
				"kbketerangan" => $kbketerangan,
				"kbnoseri" => $kbnoseri,
				"kbjenisaset" => 'A',
				"kbbidangbarang" => 'F',
				"jpid" => $jpid
			));

			if ($isCreated > 0) {
				$r->status = 1;
			}
		} else {
			$o = $this->kib_pengadaan_model->read(array("kbid" => $kbid));
			$o = array_merge($o, array(
				"kbstatusguna" => $kbstatusguna,
				"kbkodesubsubkel" => $kbkodesubsubkel,
				"kbkoderekneraca" => $kbkoderekneraca,
				"kbkoderuangan" => $kbkoderuangan,
				"kbnamabarang" => $kbnamabarang,
				"kbalamat" => $kbalamat,
				"kbtipe" => $kbtipe,
				"kbjumlah" => $kbjumlah,
				"kbsatuan" => $kbsatuan,
				"kbhargasatuan" => $kbhargasatuan,
				"kbhargatotal" => $kbhargatotal,
				"kbpajak" => $kbpajak,
				"kbhargatotalpajak" => $kbhargatotalpajak,
				"kbrencanaalokasi" => $kbrencanaalokasi,
				"kbketerangan" => $kbketerangan,
				"kbnoseri" => $kbnoseri,
				"kbjenisaset" => 'A',
				"kbbidangbarang" => 'F',
				"jpid" => $jpid
			));

			$isUpdated = $this->kib_pengadaan_model->update($o);
			if ($isUpdated > 0) {
				$r->status = 1;
			}
		}

		echo json_encode($r);
	}

	public function submit_rincian_1j() {
		$r = new stdClass;
		$r->status = 0;

		$this->load->model("kib_pengadaan_model");

		$kbstatusguna = $this->input->post("kbstatusguna", TRUE);
		$kbkodesubsubkel = $this->input->post("kbkodesubsubkel", TRUE);
		$kbkoderekneraca = $this->input->post("kbkoderekneraca", TRUE);
		$kbnamabarang = $this->input->post("kbnamabarang", TRUE);
		$kbalamat = $this->input->post("kbalamat", TRUE);
		$kbtipe = $this->input->post("kbtipe", TRUE);
		$kbjumlah = $this->input->post("kbjumlah", TRUE);
		$kbsatuan = $this->input->post("kbsatuan", TRUE);
		$kbhargasatuan = $this->input->post("kbhargasatuan", TRUE);
		$kbhargatotal = $this->input->post("kbhargatotal", TRUE);
		$kbpajak = $this->input->post("kbpajak", TRUE);
		$kbhargatotalpajak = $this->input->post("kbhargatotalpajak", TRUE);
		$kbrencanaalokasi = $this->input->post("kbrencanaalokasi", TRUE);
		$kbketerangan = $this->input->post("kbketerangan", TRUE);
		$jpid = $this->input->post("jpid", TRUE);
		$kbid = $this->input->post("kbid", TRUE);

		if($kbid === FALSE) {
			$isCreated = $this->kib_pengadaan_model->create(array(
				"kbstatusguna" => $kbstatusguna,
				"kbkodesubsubkel" => $kbkodesubsubkel,
				"kbkoderekneraca" => $kbkoderekneraca,
				"kbnamabarang" => $kbnamabarang,
				"kbalamat" => $kbalamat,
				"kbtipe" => $kbtipe,
				"kbjumlah" => $kbjumlah,
				"kbsatuan" => $kbsatuan,
				"kbhargasatuan" => $kbhargasatuan,
				"kbhargatotal" => $kbhargatotal,
				"kbpajak" => $kbpajak,
				"kbhargatotalpajak" => $kbhargatotalpajak,
				"kbrencanaalokasi" => $kbrencanaalokasi,
				"kbketerangan" => $kbketerangan,
				"kbjenisaset" => 'R',
				"kbbidangbarang" => '0',
				"jpid" => $jpid
			));

			if ($isCreated > 0) {
				$r->status = 1;
			}
		} else {
			$o = $this->kib_pengadaan_model->read(array("kbid" => $kbid));
			$o = array_merge($o, array(
				"kbstatusguna" => $kbstatusguna,
				"kbkodesubsubkel" => $kbkodesubsubkel,
				"kbkoderekneraca" => $kbkoderekneraca,
				"kbnamabarang" => $kbnamabarang,
				"kbalamat" => $kbalamat,
				"kbtipe" => $kbtipe,
				"kbjumlah" => $kbjumlah,
				"kbsatuan" => $kbsatuan,
				"kbhargasatuan" => $kbhargasatuan,
				"kbhargatotal" => $kbhargatotal,
				"kbpajak" => $kbpajak,
				"kbhargatotalpajak" => $kbhargatotalpajak,
				"kbrencanaalokasi" => $kbrencanaalokasi,
				"kbketerangan" => $kbketerangan,
				"kbjenisaset" => 'R',
				"kbbidangbarang" => '0',
				"jpid" => $jpid
			));

			$isUpdated = $this->kib_pengadaan_model->update($o);
			if ($isUpdated > 0) {
				$r->status = 1;
			}
		}

		echo json_encode($r);
	}

	public function submit_rincian_3a() {
		$r = new stdClass;
		$r->status = 0;

		$this->load->model("kib_pengadaan_model");

		$kbstatusguna = $this->input->post("kbstatusguna", TRUE);
		$kbkodesubsubkel = $this->input->post("kbkodesubsubkel", TRUE);
		$kbkoderekneraca = $this->input->post("kbkoderekneraca", TRUE);
		$kbnamabarang = $this->input->post("kbnamabarang", TRUE);
		$kbalamat = $this->input->post("kbalamat", TRUE);
		$kbtipe = $this->input->post("kbtipe", TRUE);
		$kbjumlah = $this->input->post("kbjumlah", TRUE);
		$kbsatuan = $this->input->post("kbsatuan", TRUE);
		$kbnilaitotalkontrak = $this->input->post("kbnilaitotalkontrak", TRUE);
		$kbhargasatuan = $this->input->post("kbhargasatuan", TRUE);
		$kbhargatotal = $this->input->post("kbhargatotal", TRUE);
		$kbpajak = $this->input->post("kbpajak", TRUE);
		$kbhargatotalpajak = $this->input->post("kbhargatotalpajak", TRUE);
		$kbrencanaalokasi = $this->input->post("kbrencanaalokasi", TRUE);
		$kbketerangan = $this->input->post("kbketerangan", TRUE);
		$kbbahan = $this->input->post("kbbahan", TRUE);
		$kbjeniskonstruksi = $this->input->post("kbjeniskonstruksi", TRUE);
		$kbluas = $this->input->post("kbluas", TRUE);
		$kbluasbangunan = $this->input->post("kbluasbangunan", TRUE);
		$kbnoreginduktanah = $this->input->post("kbnoreginduktanah", TRUE);
		$kbstatustanah = $this->input->post("kbstatustanah", TRUE);
		$jpid = $this->input->post("jpid", TRUE);
		$kbid = $this->input->post("kbid", TRUE);

		if($kbid === FALSE) {
			$isCreated = $this->kib_pengadaan_model->create(array(
				"kbstatusguna" => $kbstatusguna,
				"kbkodesubsubkel" => $kbkodesubsubkel,
				"kbkoderekneraca" => $kbkoderekneraca,
				"kbnamabarang" => $kbnamabarang,
				"kbalamat" => $kbalamat,
				"kbtipe" => $kbtipe,
				"kbjumlah" => $kbjumlah,
				"kbsatuan" => $kbsatuan,
				"kbnilaitotalkontrak" => $kbnilaitotalkontrak,
				"kbhargasatuan" => $kbhargasatuan,
				"kbhargatotal" => $kbhargatotal,
				"kbpajak" => $kbpajak,
				"kbhargatotalpajak" => $kbhargatotalpajak,
				"kbrencanaalokasi" => $kbrencanaalokasi,
				"kbketerangan" => $kbketerangan,
				"kbbahan" => $kbbahan,
				"kbjeniskonstruksi" => $kbjeniskonstruksi,
				"kbluas" => $kbluas,
				"kbluasbangunan" => $kbluasbangunan,
				"kbnoreginduktanah" => $kbnoreginduktanah,
				"kbstatustanah" => $kbstatustanah,
				"kbjenisaset" => 'A',
				"kbbidangbarang" => 'C',
				"jpid" => $jpid
			));

			if ($isCreated > 0) {
				$r->status = 1;
			}
		} else {
			$o = $this->kib_pengadaan_model->read(array("kbid" => $kbid));
			$o = array_merge($o, array(
				"kbstatusguna" => $kbstatusguna,
				"kbkodesubsubkel" => $kbkodesubsubkel,
				"kbkoderekneraca" => $kbkoderekneraca,
				"kbnamabarang" => $kbnamabarang,
				"kbalamat" => $kbalamat,
				"kbjumlah" => $kbjumlah,
				"kbtipe" => $kbtipe,
				"kbsatuan" => $kbsatuan,
				"kbnilaitotalkontrak" => $kbnilaitotalkontrak,
				"kbhargasatuan" => $kbhargasatuan,
				"kbhargatotal" => $kbhargatotal,
				"kbpajak" => $kbpajak,
				"kbhargatotalpajak" => $kbhargatotalpajak,
				"kbrencanaalokasi" => $kbrencanaalokasi,
				"kbketerangan" => $kbketerangan,
				"kbbahan" => $kbbahan,
				"kbjeniskonstruksi" => $kbjeniskonstruksi,
				"kbluas" => $kbluas,
				"kbluasbangunan" => $kbluasbangunan,
				"kbnoreginduktanah" => $kbnoreginduktanah,
				"kbstatustanah" => $kbstatustanah,
				"kbjenisaset" => 'A',
				"kbbidangbarang" => 'C',
				"jpid" => $jpid
			));

			$isUpdated = $this->kib_pengadaan_model->update($o);
			if ($isUpdated > 0) {
				$r->status = 1;
			}
		}

		echo json_encode($r);
	}

	public function submit_rincian_3b() {
		$r = new stdClass;
		$r->status = 0;

		$this->load->model("kib_pengadaan_model");

		$kbstatusguna = $this->input->post("kbstatusguna", TRUE);
		$kbkodesubsubkel = $this->input->post("kbkodesubsubkel", TRUE);
		$kbkoderekneraca = $this->input->post("kbkoderekneraca", TRUE);
		$kbnamabarang = $this->input->post("kbnamabarang", TRUE);
		$kbalamat = $this->input->post("kbalamat", TRUE);
		$kbtipe = $this->input->post("kbtipe", TRUE);
		$kbjumlah = $this->input->post("kbjumlah", TRUE);
		$kbsatuan = $this->input->post("kbsatuan", TRUE);
		$kbnilaitotalkontrak = $this->input->post("kbnilaitotalkontrak", TRUE);
		$kbhargasatuan = $this->input->post("kbhargasatuan", TRUE);
		$kbhargatotal = $this->input->post("kbhargatotal", TRUE);
		$kbpajak = $this->input->post("kbpajak", TRUE);
		$kbhargatotalpajak = $this->input->post("kbhargatotalpajak", TRUE);
		$kbrencanaalokasi = $this->input->post("kbrencanaalokasi", TRUE);
		$kbketerangan = $this->input->post("kbketerangan", TRUE);
		$kbbahan = $this->input->post("kbbahan", TRUE);
		$kbjeniskonstruksi = $this->input->post("kbjeniskonstruksi", TRUE);
		$kbnamaruas = $this->input->post("kbnamaruas", TRUE);
		$kbpangkalruas = $this->input->post("kbpangkalruas", TRUE);
		$kbujungruas = $this->input->post("kbujungruas", TRUE);
		$kbpanjang = $this->input->post("kbpanjang", TRUE);
		$kblebar = $this->input->post("kblebar", TRUE);
		$kbluas = $this->input->post("kbluas", TRUE);
		$jpid = $this->input->post("jpid", TRUE);
		$kbid = $this->input->post("kbid", TRUE);

		if($kbid === FALSE) {
			$isCreated = $this->kib_pengadaan_model->create(array(
				"kbstatusguna" => $kbstatusguna,
				"kbkodesubsubkel" => $kbkodesubsubkel,
				"kbkoderekneraca" => $kbkoderekneraca,
				"kbnamabarang" => $kbnamabarang,
				"kbalamat" => $kbalamat,
				"kbtipe" => $kbtipe,
				"kbjumlah" => $kbjumlah,
				"kbsatuan" => $kbsatuan,
				"kbnilaitotalkontrak" => $kbnilaitotalkontrak,
				"kbhargasatuan" => $kbhargasatuan,
				"kbhargatotal" => $kbhargatotal,
				"kbpajak" => $kbpajak,
				"kbhargatotalpajak" => $kbhargatotalpajak,
				"kbrencanaalokasi" => $kbrencanaalokasi,
				"kbketerangan" => $kbketerangan,
				"kbbahan" => $kbbahan,
				"kbjeniskonstruksi" => $kbjeniskonstruksi,
				"kbnamaruas" => $kbnamaruas,
				"kbpangkalruas" => $kbpangkalruas,
				"kbujungruas" => $kbujungruas,
				"kbpanjang" => $kbpanjang,
				"kblebar" => $kblebar,
				"kbluas" => $kbluas,
				"kbjenisaset" => 'A',
				"kbbidangbarang" => 'D',
				"jpid" => $jpid
			));

			if ($isCreated > 0) {
				$r->status = 1;
			}
		} else {
			$o = $this->kib_pengadaan_model->read(array("kbid" => $kbid));
			$o = array_merge($o, array(
				"kbstatusguna" => $kbstatusguna,
				"kbkodesubsubkel" => $kbkodesubsubkel,
				"kbkoderekneraca" => $kbkoderekneraca,
				"kbnamabarang" => $kbnamabarang,
				"kbalamat" => $kbalamat,
				"kbtipe" => $kbtipe,
				"kbjumlah" => $kbjumlah,
				"kbsatuan" => $kbsatuan,
				"kbnilaitotalkontrak" => $kbnilaitotalkontrak,
				"kbhargasatuan" => $kbhargasatuan,
				"kbhargatotal" => $kbhargatotal,
				"kbpajak" => $kbpajak,
				"kbhargatotalpajak" => $kbhargatotalpajak,
				"kbrencanaalokasi" => $kbrencanaalokasi,
				"kbketerangan" => $kbketerangan,
				"kbbahan" => $kbbahan,
				"kbjeniskonstruksi" => $kbjeniskonstruksi,
				"kbnamaruas" => $kbnamaruas,
				"kbpangkalruas" => $kbpangkalruas,
				"kbujungruas" => $kbujungruas,
				"kbpanjang" => $kbpanjang,
				"kblebar" => $kblebar,
				"kbluas" => $kbluas,
				"kbjenisaset" => 'A',
				"kbbidangbarang" => 'D',
				"jpid" => $jpid
			));

			$isUpdated = $this->kib_pengadaan_model->update($o);
			if ($isUpdated > 0) {
				$r->status = 1;
			}
		}

		echo json_encode($r);
	}

	public function submit_rincian_3c() {
		$r = new stdClass;
		$r->status = 0;

		$this->load->model("kib_pengadaan_model");

		$kbstatusguna = $this->input->post("kbstatusguna", TRUE);
		$kbkodesubsubkel = $this->input->post("kbkodesubsubkel", TRUE);
		$kbkoderekneraca = $this->input->post("kbkoderekneraca", TRUE);
		$kbnamabarang = $this->input->post("kbnamabarang", TRUE);
		$kbalamat = $this->input->post("kbalamat", TRUE);
		$kbtipe = $this->input->post("kbtipe", TRUE);
		$kbjumlah = $this->input->post("kbjumlah", TRUE);
		$kbsatuan = $this->input->post("kbsatuan", TRUE);
		$kbnilaitotalkontrak = $this->input->post("kbnilaitotalkontrak", TRUE);
		$kbhargasatuan = $this->input->post("kbhargasatuan", TRUE);
		$kbhargatotal = $this->input->post("kbhargatotal", TRUE);
		$kbpajak = $this->input->post("kbpajak", TRUE);
		$kbhargatotalpajak = $this->input->post("kbhargatotalpajak", TRUE);
		$kbrencanaalokasi = $this->input->post("kbrencanaalokasi", TRUE);
		$kbketerangan = $this->input->post("kbketerangan", TRUE);
		$jpid = $this->input->post("jpid", TRUE);
		$kbid = $this->input->post("kbid", TRUE);

		if($kbid === FALSE) {
			$isCreated = $this->kib_pengadaan_model->create(array(
				"kbstatusguna" => $kbstatusguna,
				"kbkodesubsubkel" => $kbkodesubsubkel,
				"kbkoderekneraca" => $kbkoderekneraca,
				"kbnamabarang" => $kbnamabarang,
				"kbalamat" => $kbalamat,
				"kbtipe" => $kbtipe,
				"kbjumlah" => $kbjumlah,
				"kbsatuan" => $kbsatuan,
				"kbnilaitotalkontrak" => $kbnilaitotalkontrak,
				"kbhargasatuan" => $kbhargasatuan,
				"kbhargatotal" => $kbhargatotal,
				"kbpajak" => $kbpajak,
				"kbhargatotalpajak" => $kbhargatotalpajak,
				"kbrencanaalokasi" => $kbrencanaalokasi,
				"kbketerangan" => $kbketerangan,
				"kbjenisaset" => 'R',
				"kbbidangbarang" => '0',
				"jpid" => $jpid
			));

			if ($isCreated > 0) {
				$r->status = 1;
			}
		} else {
			$o = $this->kib_pengadaan_model->read(array("kbid" => $kbid));
			$o = array_merge($o, array(
				"kbstatusguna" => $kbstatusguna,
				"kbkodesubsubkel" => $kbkodesubsubkel,
				"kbkoderekneraca" => $kbkoderekneraca,
				"kbnamabarang" => $kbnamabarang,
				"kbalamat" => $kbalamat,
				"kbtipe" => $kbtipe,
				"kbjumlah" => $kbjumlah,
				"kbsatuan" => $kbsatuan,
				"kbnilaitotalkontrak" => $kbnilaitotalkontrak,
				"kbhargasatuan" => $kbhargasatuan,
				"kbhargatotal" => $kbhargatotal,
				"kbpajak" => $kbpajak,
				"kbhargatotalpajak" => $kbhargatotalpajak,
				"kbrencanaalokasi" => $kbrencanaalokasi,
				"kbketerangan" => $kbketerangan,
				"kbjenisaset" => 'R',
				"kbbidangbarang" => '0',
				"jpid" => $jpid
			));

			$isUpdated = $this->kib_pengadaan_model->update($o);
			if ($isUpdated > 0) {
				$r->status = 1;
			}
		}

		echo json_encode($r);
	}


	public function submit_rincian_2a() {
		$r = new stdClass;
		$r->status = 0;

		$this->load->model("kib_pengadaan_model");

		$kbnamabarang = $this->input->post("kbnamabarang", TRUE);
		$kbjumlah = $this->input->post("kbjumlah", TRUE);
		$kbsatuan = $this->input->post("kbsatuan", TRUE);
		$kbhargasatuan = $this->input->post("kbhargasatuan", TRUE);
		$kbhargatotal = $this->input->post("kbhargatotal", TRUE);
		$kbpajak = $this->input->post("kbpajak", TRUE);
		$kbhargatotalpajak = $this->input->post("kbhargatotalpajak", TRUE);
		$kbketerangan = $this->input->post("kbketerangan", TRUE);
		$jpid = $this->input->post("jpid", TRUE);
		$kbid = $this->input->post("kbid", TRUE);

		if($kbid === FALSE) {
			$isCreated = $this->kib_pengadaan_model->create(array(
				"kbnamabarang" => $kbnamabarang,
				"kbjumlah" => $kbjumlah,
				"kbsatuan" => $kbsatuan,
				"kbhargasatuan" => $kbhargasatuan,
				"kbhargatotal" => $kbhargatotal,
				"kbpajak" => $kbpajak,
				"kbhargatotalpajak" => $kbhargatotalpajak,
				"kbketerangan" => $kbketerangan,
				"kbjenisaset" => 'J',
				"jpid" => $jpid
			));

			if ($isCreated > 0) {
				$r->status = 1;
			}
		} else {
			$o = $this->kib_pengadaan_model->read(array("kbid" => $kbid));
			$o = array_merge($o, array(
				"kbnamabarang" => $kbnamabarang,
				"kbjumlah" => $kbjumlah,
				"kbsatuan" => $kbsatuan,
				"kbhargasatuan" => $kbhargasatuan,
				"kbhargatotal" => $kbhargatotal,
				"kbpajak" => $kbpajak,
				"kbhargatotalpajak" => $kbhargatotalpajak,
				"kbketerangan" => $kbketerangan,
				"kbjenisaset" => 'J',
				"jpid" => $jpid
			));

			$isUpdated = $this->kib_pengadaan_model->update($o);
			if ($isUpdated > 0) {
				$r->status = 1;
			}
		}

		echo json_encode($r);
	}

	public function submit_rincian_2b() {
		$r = new stdClass;
		$r->status = 0;

		$this->load->model("kib_pengadaan_model");

		$kbnamabarang = $this->input->post("kbnamabarang", TRUE);
		$kbalamat = $this->input->post("kbalamat", TRUE);
		$kbtipe = $this->input->post("kbtipe", TRUE);
		$kbjumlah = $this->input->post("kbjumlah", TRUE);
		$kbsatuan = $this->input->post("kbsatuan", TRUE);
		$kbhargasatuan = $this->input->post("kbhargasatuan", TRUE);
		$kbhargatotal = $this->input->post("kbhargatotal", TRUE);
		$kbpajak = $this->input->post("kbpajak", TRUE);
		$kbhargatotalpajak = $this->input->post("kbhargatotalpajak", TRUE);
		$kbketerangan = $this->input->post("kbketerangan", TRUE);
		$jpid = $this->input->post("jpid", TRUE);
		$kbid = $this->input->post("kbid", TRUE);

		if($kbid === FALSE) {
			$isCreated = $this->kib_pengadaan_model->create(array(
				"kbnamabarang" => $kbnamabarang,
				"kbalamat" => $kbalamat,
				"kbtipe" => $kbtipe,
				"kbjumlah" => $kbjumlah,
				"kbsatuan" => $kbsatuan,
				"kbhargasatuan" => $kbhargasatuan,
				"kbhargatotal" => $kbhargatotal,
				"kbpajak" => $kbpajak,
				"kbhargatotalpajak" => $kbhargatotalpajak,
				"kbketerangan" => $kbketerangan,
				"kbjenisaset" => 'H',
				"jpid" => $jpid
			));

			if ($isCreated > 0) {
				$r->status = 1;
			}
		} else {
			$o = $this->kib_pengadaan_model->read(array("kbid" => $kbid));
			$o = array_merge($o, array(
				"kbnamabarang" => $kbnamabarang,
				"kbalamat" => $kbalamat,
				"kbtipe" => $kbtipe,
				"kbjumlah" => $kbjumlah,
				"kbsatuan" => $kbsatuan,
				"kbhargasatuan" => $kbhargasatuan,
				"kbhargatotal" => $kbhargatotal,
				"kbpajak" => $kbpajak,
				"kbhargatotalpajak" => $kbhargatotalpajak,
				"kbketerangan" => $kbketerangan,
				"kbjenisaset" => 'H',
				"jpid" => $jpid
			));

			$isUpdated = $this->kib_pengadaan_model->update($o);
			if ($isUpdated > 0) {
				$r->status = 1;
			}
		}

		echo json_encode($r);
	}

	public function submit_rincian_2c() {
		$r = new stdClass;
		$r->status = 0;

		$this->load->model("kib_pengadaan_model");

		$kbnamabarang = $this->input->post("kbnamabarang", TRUE);
		$kbalamat = $this->input->post("kbalamat", TRUE);
		$kbtipe = $this->input->post("kbtipe", TRUE);
		$kbjumlah = $this->input->post("kbjumlah", TRUE);
		$kbsatuan = $this->input->post("kbsatuan", TRUE);
		$kbhargasatuan = $this->input->post("kbhargasatuan", TRUE);
		$kbhargatotal = $this->input->post("kbhargatotal", TRUE);
		$kbpajak = $this->input->post("kbpajak", TRUE);
		$kbhargatotalpajak = $this->input->post("kbhargatotalpajak", TRUE);
		$kbketerangan = $this->input->post("kbketerangan", TRUE);
		$jpid = $this->input->post("jpid", TRUE);
		$kbid = $this->input->post("kbid", TRUE);

		if($kbid === FALSE) {
			$isCreated = $this->kib_pengadaan_model->create(array(
				"kbnamabarang" => $kbnamabarang,
				"kbalamat" => $kbalamat,
				"kbtipe" => $kbtipe,
				"kbjumlah" => $kbjumlah,
				"kbsatuan" => $kbsatuan,
				"kbhargasatuan" => $kbhargasatuan,
				"kbhargatotal" => $kbhargatotal,
				"kbpajak" => $kbpajak,
				"kbhargatotalpajak" => $kbhargatotalpajak,
				"kbketerangan" => $kbketerangan,
				"kbjenisaset" => 'B',
				"jpid" => $jpid
			));

			if ($isCreated > 0) {
				$r->status = 1;
			}
		} else {
			$o = $this->kib_pengadaan_model->read(array("kbid" => $kbid));
			$o = array_merge($o, array(
				"kbnamabarang" => $kbnamabarang,
				"kbalamat" => $kbalamat,
				"kbtipe" => $kbtipe,
				"kbjumlah" => $kbjumlah,
				"kbsatuan" => $kbsatuan,
				"kbhargasatuan" => $kbhargasatuan,
				"kbhargatotal" => $kbhargatotal,
				"kbpajak" => $kbpajak,
				"kbhargatotalpajak" => $kbhargatotalpajak,
				"kbketerangan" => $kbketerangan,
				"kbjenisaset" => 'B',
				"jpid" => $jpid
			));

			$isUpdated = $this->kib_pengadaan_model->update($o);
			if ($isUpdated > 0) {
				$r->status = 1;
			}
		}

		echo json_encode($r);
	}


	public function get_rincian_1($jpid) {
		$this->load->model("kib_pengadaan_model");

		$r = new stdClass;
		$r->data = $this->kib_pengadaan_model->find_all_aset_tetap($jpid);

		echo json_encode($r);
	}

	public function get_rincian_2($jpid) {
		$this->load->model("kib_pengadaan_model");

		$r = new stdClass;
		$r->data = $this->kib_pengadaan_model->find_all_penambahan_nilai($jpid);

		echo json_encode($r);
	}

	public function get_rincian_3($jpid) {
		$this->load->model("kib_pengadaan_model");

		$r = new stdClass;
		$r->data = $this->kib_pengadaan_model->find_all_kdp($jpid);

		echo json_encode($r);
	}

	public function get_rincian_4($jpid) {
		$this->load->model("kib_pengadaan_model");

		$r = new stdClass;
		$r->data = $this->kib_pengadaan_model->find_all_jasa($jpid);

		echo json_encode($r);
	}

	public function get_rincian_5($jpid) {
		$this->load->model("kib_pengadaan_model");

		$r = new stdClass;
		$r->data = $this->kib_pengadaan_model->find_all_pakai_habis($jpid);

		echo json_encode($r);
	}

	public function get_rincian_6($jpid) {
		$this->load->model("kib_pengadaan_model");

		$r = new stdClass;
		$r->data = $this->kib_pengadaan_model->find_all_bantuan($jpid);

		echo json_encode($r);
	}

	public function get_pembayaran($jpid) {
		$this->load->model("pembayaran_model");

		$r = new stdClass;
		$r->data = $this->pembayaran_model->get_list(array("jpid" => $jpid), 0, 0, array("pbnospm" => "asc"));

		echo json_encode($r);
	}


	public function get_detail_by_id() {
		$this->load->model("kib_pengadaan_model");

		$kbid = $this->input->get("kbid", TRUE);

		$r = new stdClass;
		$r->data = $this->kib_pengadaan_model->read(array("kbid" => $kbid));

		echo json_encode($r);	
	}

	public function delete_by_kbid() {
		$this->load->model("kib_pengadaan_model");

		$kbid = $this->input->post("kbid", TRUE);


		$r = new stdClass;
		$r->success = 0;


		$o = $this->kib_pengadaan_model->read(array("kbid" => $kbid));
		if (!empty($o)) {
			if ($this->kib_pengadaan_model->delete($o) > 0) {
				$r->success = 1;
			}
		}

		echo json_encode($r);
	}

	public function pembayaran_spp() {
		$no_spk_sp_dokumen = $this->input->get("no_spk_sp_dokumen", TRUE);
		$this->load->model("simda/pembayaran_model", "pembayaran_model");

		$r = new stdClass;
		$r->status = 0;

		$r->data = $this->pembayaran_model->find_spp_by_no_kontrak($no_spk_sp_dokumen);

		$r->status = 1;

		echo json_encode($r);
	}

	public function pembayaran_spm() {
		$no_spp = $this->input->get("no_spp", TRUE);
		$this->load->model("simda/pembayaran_model", "pembayaran_model");

		$r = new stdClass;
		$r->status = 0;
		$r->data = $this->pembayaran_model->find_spm_by_no_spp($no_spp);
		$r->no_spp = $no_spp;

		$r->status = 1;

		echo json_encode($r);
	}

	public function pembayaran_sp2d() {
		$no_spm = $this->input->get("no_spm", TRUE);
		$this->load->model("simda/pembayaran_model", "pembayaran_model");

		$r = new stdClass;
		$r->status = 0;

		$r->data = $this->pembayaran_model->find_sp2d_by_no_spm($no_spm);

		$r->status = 1;

		echo json_encode($r);
	}

	public function pembayaran_spp_uraian(){
		$no_spk_sp_dokumen = $this->input->get("no_spk_sp_dokumen", TRUE);
		$this->load->model("simda/pembayaran_model", "pembayaran_model");

		$r = new stdClass;
		$r->status = 0;

		$r->data = $this->pembayaran_model->find_spp_on_uraian_spm($no_spk_sp_dokumen);

		$r->status = 1;

		echo json_encode($r);	
	}

	public function pembayaran_spm_input(){
		$no_spm_input = $this->input->get("no_spm_input", TRUE);
		$this->load->model("simda/pembayaran_model", "pembayaran_model");

		$r = new stdClass;
		$r->status = 0;

		$r->data = $this->pembayaran_model->find_spm_by_input($no_spm_input);

		$r->status = 1;

		echo json_encode($r);	
	}

	public function pembayaran_spm_rinc() {
		$no_spm = $this->input->get("no_spm", TRUE);
		$this->load->model("simda/pembayaran_model", "pembayaran_model");

		$r = new stdClass;
		$r->status = 0;

		$r->data = $this->pembayaran_model->get_nilai_spm($no_spm);

		$r->status = 1;

		echo json_encode($r);
	}

	public function submit_pembayaran(){
		$r = new stdClass;
		$r->status = 0;

		$this->load->model("pembayaran_model");

		$pbid = $this->input->post("pbid", TRUE);
		$pbthnspm = $this->input->post("pbthnspm", TRUE);
		$pbkoderek13 = $this->input->post("pbkoderek13", TRUE);
		$pburaianspm = $this->input->post("pburaianspm", TRUE);
		$pbnosp2d = $this->input->post("pbnosp2d", TRUE);
		$pbtglsp2d = $this->input->post("pbtglsp2d", TRUE);
		$pbtermin = $this->input->post("pbtermin", TRUE);
		$pbnospm = $this->input->post("pbnospm", TRUE);
		$pbtglspm = $this->input->post("pbtglspm", TRUE);
		$pbnospp = $this->input->post("pbnospp", TRUE);
		$pbtglspp = $this->input->post("pbtglspp", TRUE);
		$pbketerangan = $this->input->post("pbketerangan", TRUE);
		$pbnilaispm = $this->input->post("pbnilaispm", TRUE);
		$jpid = $this->input->post("jpid", TRUE);

		if($pbid === FALSE) {
			$isCreated = $this->pembayaran_model->create(array(
				"pbthnspm" => $pbthnspm,
				"pbkoderek13" => $pbkoderek13,
				"pburaianspm" => $pburaianspm,
				"pbnosp2d" => $pbnosp2d,
				"pbtglsp2d" => $pbtglsp2d != "" ? $pbtglsp2d : null,
				"pbtermin" => $pbtermin,
				"pbnospm" => $pbnospm,
				"pbtglspm" => $pbtglspm != "" ? $pbtglspm : null,
				"pbnospp" => $pbnospp,
				"pbtglspp" => $pbtglspp != "" ? $pbtglspp : null,
				"pbketerangan" => $pbketerangan,
				"pbnilaispm" => $pbnilaispm,
				"jpid" => $jpid
			));

			if ($isCreated > 0) {
				$r->status = 1;
			}
		} else {
			$o = $this->pembayaran_model->read(array("pbid" => $pbid));
			$o = array_merge($o, array(
				"pbthnspm" => $pbthnspm,
				"pbkoderek13" => $pbkoderek13,
				"pburaianspm" => $pburaianspm,
				"pbnosp2d" => $pbnosp2d,
				"pbtglsp2d" => $pbtglsp2d != "" ? $pbtglsp2d : null,
				"pbtermin" => $pbtermin,
				"pbnospm" => $pbnospm,
				"pbtglspm" => $pbtglspm != "" ? $pbtglspm : null,
				"pbnospp" => $pbnospp,
				"pbtglspp" => $pbtglspp != "" ? $pbtglspp : null,
				"pbketerangan" => $pbketerangan,
				"pbnilaispm" => $pbnilaispm,
				"jpid" => $jpid
			));

			$isUpdated = $this->pembayaran_model->update($o);
			if ($isUpdated > 0) {
				$r->status = 1;
			}
		}

		echo json_encode($r);
	}

	public function post($jpid) {
		$this->load->model("simbada/posting_jurnal_pengadaan_model", "posting_jurnal_pengadaan_model");
		$this->load->model("pembayaran_model");
		$this->load->model("jurnal_pengadaan_model");
		$this->load->model("kib_pengadaan_model");
		$this->load->model("simbada/kamus_spk_model", "kamus_spk_model");
		$this->load->model("simbada/kib_model", "kib_model");

		$jp = $this->jurnal_pengadaan_model->read(array("jpid" => $jpid));
		$spk = $this->kamus_spk_model->read($jp['id_kontrak']);

		$terkunci = 0;
		$kode_jurnal = 101;
		$instansi_yg_menyerahkan = $spk['REKANAN'];

		$tahun_spj = substr($jp['jptgl'], 0, 4);
		$no_jurnal = $this->posting_jurnal_pengadaan_model->get_max_no_jurnal($jp["jpnomorlokasi"], explode("-", $jp["jptgl"])[0]);
		$nomor = ((int)$no_jurnal["MAX"])+1;
		$no_jurnal = str_pad($nomor, 5, '0', STR_PAD_LEFT);

		$no_key = $jp['jpnomorlokasi'].$no_jurnal.$tahun_spj;

		//menu posting jurnal pengadaan ke simbada 
		$this->posting_jurnal_pengadaan_model->post_jurnal_pengadaan(
			$terkunci,
			$jp['jpnomorlokasi'], 
			$jp['jpid'], 
			$jp['jpnobaterima'], 
			$jp['jptglbaterima'], 
			$jp['jpnobaperiksa'], 
			$jp['jptglbaperiksa'], 
			$no_jurnal, 
			$jp['jptgl'], 
			$jp['id_kontrak'], 
			$no_key, 
			$tahun_spj,
			$kode_jurnal,
			$instansi_yg_menyerahkan,
			$nomor
		);

		//menu posting KIB ke simbada
		$kib = $this->kib_pengadaan_model->find_all_aset($jpid);
		$id_transaksi = $this->kib_model->get_max_id_traksaksi();
		$id_transaksi['MAX'] = $id_transaksi['MAX'] + 1;
		$pos_entri = "193F46."; // dummy diambil dari tabel setup aplikasi

		//menu posting jasa, pakai habis, dan bantuan
		$pos_entri_nonat = '193F46'; // dummy diambil dari tabel setup aplikasi

		for ($i = 0; $i < count($kib); $i++) {
			$kbkoderekneraca = $kib[$i]['kbkoderekneraca'];
			$kbjenisaset = $kib[$i]['kbjenisaset'];
			$kbbidangbarang = $kib[$i]['kbbidangbarang'];

			$data = array(
				"no_key" => $no_key,
				"no_ba_penerimaan" => $no_jurnal,
				"nomor_lokasi" => $jp['jpnomorlokasi'],
				"id_traksaksi" => $id_transaksi['MAX'],
				"pajak" => $kib[$i]['kbpajak'],
				"tahun_spj" => $tahun_spj,

				"no_register" => $no_key,
				"kode_sub_sub_kelompok" => $kib[$i]['kbkodesubsubkel'],
				"kode_sub_kel_at" => $kib[$i]['kbkoderekneraca'],
				"nama_barang" => $kib[$i]['kbnamabarang'],
				"merk_alamat" => $kib[$i]['kbalamat'],
				"jumlah_barang" => $kib[$i]['kbjumlah'],
				"satuan" => $kib[$i]['kbsatuan'],
				"harga_satuan" => $kib[$i]['kbhargasatuan'],
				"harga_total" => $kib[$i]['kbhargatotal'],
				"harga_total_plus_pajak" => $kib[$i]['kbhargatotalpajak'],
				"rencana_alokasi" => $kib[$i]['kbrencanaalokasi'],
				"penggunaan" => $kib[$i]['kbpenggunaan'],
				"keterangan" => $kib[$i]['kbketerangan'],
				"status_tanah" => $kib[$i]['kbstatustanah'],
				"no_sertifikat" => $kib[$i]['kbnosertifikat'],
				"tgl_sertifkat" => $kib[$i]['kbtglsertifikat'],
				"atas_nama_sertifikat" => $kib[$i]['kbansertifikat'],
				"panjang_tanah" => $kib[$i]['kbpanjang'],
				"lebar_tanah" => $kib[$i]['kblebar'],
				"luas_tanah" => $kib[$i]['kbluas'],

				"tipe" => $kib[$i]['kbtipe'],
				"ukuran" => $kib[$i]['kbukuran'],
				"warna" => $kib[$i]['kbwarna'],
				"bahan" => $kib[$i]['kbbahan'],

				"no_bpkb" => $kib[$i]['kbnobpkb'],
				"nopol" => $kib[$i]['kbnopolisi'],
				"no_rangka_seri" => $kib[$i]['kbnorangka'],
				"no_mesin" => $kib[$i]['kbnomesin'],
				"warna" => $kib[$i]['kbwarna'],
				"cc" => $kib[$i]['kbkapasitasslinder'],
				"no_stnk" => $kib[$i]['kbnostnk'],
				"tgl_stnk" => $kib[$i]['kbtglstnk'],
				"bahan_bakar" => $kib[$i]['kbbahan'],
				"tahun_pembuatan" => $kib[$i]['kbthnbuat'],
				"tahun_perakitan" => $kib[$i]['kbthnrakit'],

				"konstruksi" => $kib[$i]['kbjeniskonstruksi'],
				"luas_lantai" => $kib[$i]['kbluas'],
				"luas_bangunan" => $kib[$i]['kbluasbangunan'],
				"no_regs_induk_tanah" => $kib[$i]['kbnoreginduktanah'],
				"status_tanah" => $kib[$i]['kbstatustanah'],

				"nama_ruas" => $kib[$i]['kbnamaruas'],
				"pangkal" => $kib[$i]['kbpangkalruas'],
				"ujung" => $kib[$i]['kbujungruas'],
				"panjang" => $kib[$i]['kbpanjang'],
				"lebar" => $kib[$i]['kblebar'],

				"nilai_lunas" => $kib[$i]["kbnilaitotalkontrak"]
			);

			if(0 === strpos($kbkoderekneraca, '1.3.1') && $kbjenisaset === 'A') {
				echo "posting A<br/>";
				$this->posting_jurnal_pengadaan_model->post_kib_1a($pos_entri, $data);
			} else if(0 === strpos($kbkoderekneraca, '1.3.2') && $kbjenisaset === 'A') {
				$tmp = explode(".", $kbkoderekneraca);
				if(count($tmp) > 3){
					$c = (int) $tmp[3];
					if($c >= 1 && $c <= 6){
						echo "posting C<br/>";
						$this->posting_jurnal_pengadaan_model->post_kib_1c($pos_entri, $data);
					} else{
						echo "posting B<br/>";
						$this->posting_jurnal_pengadaan_model->post_kib_1b($pos_entri, $data);
					}
				}
			} else if(0 === strpos($kbkoderekneraca, '1.3.3') && $kbjenisaset === 'A') {
				echo "posting D<br/>";
				$this->posting_jurnal_pengadaan_model->post_kib_1d($pos_entri, $data);
			} else if(0 === strpos($kbkoderekneraca, '1.3.4') && $kbjenisaset === 'A') {
				echo "posting E<br/>";
				$this->posting_jurnal_pengadaan_model->post_kib_1e($pos_entri, $data);
			} else if(0 === strpos($kbkoderekneraca, '1.3.5.01') && $kbjenisaset === 'A') {
				echo "posting F<br/>";
				$this->posting_jurnal_pengadaan_model->post_kib_1f($pos_entri, $data);
			} else if(0 === strpos($kbkoderekneraca, '1.3.5.02') && $kbjenisaset === 'A') {
				echo "posting G<br/>";
				$this->posting_jurnal_pengadaan_model->post_kib_1g($pos_entri, $data);
			} else if(0 === strpos($kbkoderekneraca, '1.3.5.03') && $kbjenisaset === 'A') {
				echo "posting H<br/>";
				$this->posting_jurnal_pengadaan_model->post_kib_1h($pos_entri, $data);
			} else if(0 === strpos($kbkoderekneraca, '1.5.4') && $kbjenisaset === 'A') {
				echo "posting I<br/>";
				$this->posting_jurnal_pengadaan_model->post_kib_1i($pos_entri, $data);
			} else if(0 === strpos($kbkoderekneraca, '1.3') && $kbjenisaset === 'R') {
				
				if(0 === strpos($kbkoderekneraca, '1.3.1')){
					$bidang_barang = 'A';
				} else if(0 === strpos($kbkoderekneraca, '1.3.2')){
					$bidang_barang = 'B';
				} else if(0 === strpos($kbkoderekneraca, '1.3.3')){
					$bidang_barang = 'C';
				} else if(0 === strpos($kbkoderekneraca, '1.3.4')){
					$bidang_barang = 'D';
				} else if(0 === strpos($kbkoderekneraca, '1.3.5')){
					$bidang_barang = 'E';
				} 

				if(0 === strpos($kbkoderekneraca, '1.3.6.01.01') && $kbbidangbarang === '0'){
					echo "posting 3c<br/>";
					$this->posting_jurnal_pengadaan_model->post_kib_3c($pos_entri, $data);
				} else{
					echo "posting J<br/>";
					$this->posting_jurnal_pengadaan_model->post_kib_1j($pos_entri, $data, $bidang_barang);
				}
			} else if($kbjenisaset === 'J') {
				$id_transaksi_nonat = $this->posting_jurnal_pengadaan_model->get_max_id_transaksi();
				$id_transaksi_non_aset_tetap = $id_transaksi_nonat["MAX"] + 1;

				echo "posting 2a<br/>";
				$this->posting_jurnal_pengadaan_model->post_kib_2a($pos_entri_nonat, $data, $id_transaksi_non_aset_tetap);
			} else if($kbjenisaset === 'H') {
				$id_transaksi_nonat = $this->posting_jurnal_pengadaan_model->get_max_id_transaksi();
				$id_transaksi_non_aset_tetap = $id_transaksi_nonat["MAX"] + 1;
				//$id_aset_nonat = $pos_entri_nonat . ((int)$id_transaksi_non_aset_tetap) + 1);

				echo "posting 2b<br/>";
				$this->posting_jurnal_pengadaan_model->post_kib_2b($pos_entri_nonat, $data, $id_transaksi_non_aset_tetap);
			} else if($kbjenisaset === 'B') {
				$id_transaksi_nonat = $this->posting_jurnal_pengadaan_model->get_max_id_transaksi();
				$id_transaksi_non_aset_tetap = $id_transaksi_nonat["MAX"] + 1;
				//$id_aset_nonat = $pos_entri_nonat . ((int)$id_transaksi_non_aset_tetap + 1);

				echo "posting 2c<br/>";
				$this->posting_jurnal_pengadaan_model->post_kib_2c($pos_entri_nonat, $data, $id_transaksi_non_aset_tetap);
			} else if(0 === strpos($kbkoderekneraca, '1.3.6.01.01') && $kbbidangbarang === 'C'){
				echo "posting 3a<br/>";
				$this->posting_jurnal_pengadaan_model->post_kib_3a($pos_entri, $data);
			} else if(0 === strpos($kbkoderekneraca, '1.3.6.01.01') && $kbbidangbarang === 'D'){
				echo "posting 3b<br/>";
				$this->posting_jurnal_pengadaan_model->post_kib_3b($pos_entri, $data);
			} 
		}

		//menu posting pembayaran
		$pembayaran = $this->pembayaran_model->get_list(array("jpid" => $jpid));
		$nomor_sub_unit = substr($jp['jpnomorlokasi'], 0, 14);

		for ($i = 0; $i < count($pembayaran); $i++){
			$kode_rek_belanja = $pembayaran[$i]['pbkoderek13'];
			$termin_ke = $pembayaran[$i]['pbtermin'];
			$uraian_belanja = $pembayaran[$i]['pburaianspm'];
			$no_spm_spmu = $pembayaran[$i]['pbnospm'];
			$tgl_spm_spmu = $pembayaran[$i]['pbtglspm'];
			$nilai_spm_spmu = $pembayaran[$i]['pbnilaispm'];
			$no_sp2d = $pembayaran[$i]['pbnosp2d'];
			$tgl_sp2d = $pembayaran[$i]['pbtglsp2d'];
			$no_spp = $pembayaran[$i]['pbnospp'];
			$tgl_spp = $pembayaran[$i]['pbtglspp'];
			$id_spm = $tahun_spj.$nomor_sub_unit.$kode_rek_belanja.$no_spm_spmu.$no_sp2d;
			$pbketerangan = $pembayaran[$i]['pbketerangan'];
			
			$data = array(
				"nomor_sub_unit" => $nomor_sub_unit,
				"no_jurnal" => $no_jurnal,
				"kode_rek_belanja" => $kode_rek_belanja,
				"termin_ke" => $termin_ke,
				"uraian_belanja" => $uraian_belanja,
				"no_spm_spmu" => $no_spm_spmu,
				"tgl_spm_spmu" => $tgl_spm_spmu,
				"nilai_spm_spmu" => $nilai_spm_spmu,
				"tahun_spj" => $tahun_spj,
				"no_sp2d" => $no_sp2d,
				"tgl_sp2d" => $tgl_sp2d,
				"no_spp" => $no_spp,
				"tgl_spp" => $tgl_spp,
				"keterangan" => "",
				"no_key" => $no_key,
				"id_spm" => $id_spm
			);

			$this->posting_jurnal_pengadaan_model->post_pembayaran($data);
		}

		redirect("pengadaan/index");
	}

	public function get_uraian_belanja(){
		$this->load->model("simda/pembayaran_model", "pembayaran_model");
		$r1 = $this->input->get("kd_rek_1", TRUE);
		$r2 = $this->input->get("kd_rek_2", TRUE);
		$r3 = $this->input->get("kd_rek_3", TRUE);
		$r4 = $this->input->get("kd_rek_4", TRUE);
		$r5 = $this->input->get("kd_rek_5", TRUE);

		$r = new stdClass;
		$r->status = 0;

		$r->data = $this->pembayaran_model->get_uraian_belanja($r1, $r2, $r3, $r4, $r5);

		$r->status = 1;

		echo json_encode($r);
	}

	public function submit_penambahan_nilai() {
		$r = new stdClass;
		$r->status = 0;

		$this->load->model("kib_tambah_nilai_model");
		$this->load->model("simbada/kib_model", "kib_model");

		$tnasetindukid = $this->input->post("tnasetindukid", TRUE);
		$tnjeniskib = $this->input->post("tnjeniskib", TRUE);
		$tnnomorlokasi = $this->input->post("tnnomorlokasi", TRUE);
		$tntgl = $this->input->post("tntgl", TRUE);
		$kbkodesubsubkel = $this->input->post("kbkodesubsubkel", TRUE);
		$kbkoderekneraca = $this->input->post("kbkoderekneraca", TRUE);
		$kbnamabarang = $this->input->post("kbnamabarang", TRUE);
		$kbalamat = $this->input->post("kbalamat", TRUE);
		$kbtipe = $this->input->post("kbtipe", TRUE);
		$kbjumlah = $this->input->post("kbjumlah", TRUE);
		$kbsatuan = $this->input->post("kbsatuan", TRUE);
		$kbhargasatuan = $this->input->post("kbhargasatuan", TRUE);
		$kbhargatotal = $this->input->post("kbhargatotal", TRUE);
		$kbpajak = $this->input->post("kbpajak", TRUE);
		$kbhargatotalpajak = $this->input->post("kbhargatotalpajak", TRUE);
		$kbrencanaalokasi = $this->input->post("kbrencanaalokasi", TRUE);
		$kbketerangan = $this->input->post("kbketerangan", TRUE);
		$jpid = $this->input->post("jpid", TRUE);
		$kbid = $this->input->post("kbid", TRUE);

		$kib = $this->kib_model->read($tnasetindukid);
		$persen_tambah_nilai = floatval($kbhargatotalpajak)/floatval($kib["HARGA_TOTAL_PLUS_PAJAK"])*100;

		if ($tntgl !== FALSE && $tntgl !== '') {
			$tntgl = date_create_from_format('d-m-Y', $tntgl);
			$tntgl = date_format($tntgl, 'Y-m-d');
		} else {
			$tntgl = NULL;
		}
		
		if ($kib["BIDANG_BARANG"] == 'C') {
			if ($persen_tambah_nilai > 0 && $persen_tambah_nilai < 30) {
				$id_detail_rule_tn = 1;
			} else if ($persen_tambah_nilai > 30 && $persen_tambah_nilai < 45) {
				$id_detail_rule_tn = 2;
			} else if ($persen_tambah_nilai > 45 && $persen_tambah_nilai < 65) {
				$id_detail_rule_tn = 3;
			} else if ($persen_tambah_nilai > 65) {
				$id_detail_rule_tn = 4;
			}
		}

		if($kbid === FALSE) {
			$isCreated = $this->kib_tambah_nilai_model->create(array(
				"tnasetindukid" => $tnasetindukid,
				"tnjeniskib" => $tnjeniskib,
				"tnnomorlokasi" => $tnnomorlokasi,
				"tntgl" => $tntgl,
				"kbkodesubsubkel" => $kbkodesubsubkel,
				"kbkoderekneraca" => $kbkoderekneraca,
				"kbnamabarang" => $kbnamabarang,
				"kbalamat" => $kbalamat,
				"kbtipe" => $kbtipe,
				"kbjumlah" => $kbjumlah,
				"kbsatuan" => $kbsatuan,
				"kbhargasatuan" => $kbhargasatuan,
				"kbhargatotal" => $kbhargatotal,
				"kbpajak" => $kbpajak,
				"kbhargatotalpajak" => $kbhargatotalpajak,
				"kbrencanaalokasi" => $kbrencanaalokasi,
				"kbketerangan" => $kbketerangan,
				"kbid" => $kbid,
				"jpid" => $jpid,
				"id_detail_rule_tn" => $id_detail_rule_tn
			));

			if ($isCreated > 0) {
				$r->status = 1;
			}
		} else {
			$o = $this->kib_tambah_nilai_model->read(array("tnasetindukid" => $tnasetindukid));
			$o = array_merge($o, array(
				"tnasetindukid" => $tnasetindukid,
				"tnjeniskib" => $tnjeniskib,
				"tnnomorlokasi" => $tnnomorlokasi,
				"tntgl" => $tntgl,
				"kbkodesubsubkel" => $kbkodesubsubkel,
				"kbkoderekneraca" => $kbkoderekneraca,
				"kbnamabarang" => $kbnamabarang,
				"kbalamat" => $kbalamat,
				"kbtipe" => $kbtipe,
				"kbjumlah" => $kbjumlah,
				"kbsatuan" => $kbsatuan,
				"kbhargasatuan" => $kbhargasatuan,
				"kbhargatotal" => $kbhargatotal,
				"kbpajak" => $kbpajak,
				"kbhargatotalpajak" => $kbhargatotalpajak,
				"kbrencanaalokasi" => $kbrencanaalokasi,
				"kbketerangan" => $kbketerangan,
				"kbid" => $kbid,
				"jpid" => $jpid,
				"id_detail_rule_tn" => $id_detail_rule_tn
			));

			$isUpdated = $this->kib_tambah_nilai_model->update($o);
			if ($isUpdated > 0) {
				$r->status = 1;
			}
		}

		echo json_encode($r);
	}

}