<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Spk extends CI_Controller {

	private $per_page = 10;

	function __construct() {
		parent::__construct();
	}

	private function setup_pagination($config = array()) {
		$this->load->model("simbada/kamus_spk_model", "kamus_spk_model");
		$this->load->library('pagination');


		$filter = $this->input->get("filter", TRUE);
		$nomor_lokasi = $this->input->get("nomor_lokasi");
		$tahun = (int)$this->input->get("tahun", TRUE);

		$config['base_url'] = site_url("spk/index?filter=" . $filter . "&nomor_lokasi=" . $nomor_lokasi . "&tahun=" . $tahun);
		if (!array_key_exists("total_rows", $config)) {
			$config['total_rows'] = $this->kamus_spk_model->count($tahun, $nomor_lokasi, $filter);
		}
		$config['per_page'] = $this->per_page;

		$config['uri_segment'] = 4;
		$config['display_pages'] = TRUE;
		$config['page_query_string'] = TRUE;

		$config['first_link'] = '&laquo; First';
		$config['first_tag_open'] = '<li class="prev page">';
		$config['first_tag_close'] = '</li>';

		$config['last_link'] = 'Last &raquo;';
		$config['last_tag_open'] = '<li class="next page">';
		$config['last_tag_close'] = '</li>';

		$config['next_link'] = 'Next &rarr;';
		$config['next_tag_open'] = '<li class="next page">';
		$config['next_tag_close'] = '</li>';

		$config['prev_link'] = '&larr; Previous';
		$config['prev_tag_open'] = '<li class="prev page">';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><a href="">';
		$config['cur_tag_close'] = '</a></li>';

		$config['num_tag_open'] = '<li class="page">';
		$config['num_tag_close'] = '</li>';

		$config['full_tag_open'] = '<div style="text-align:center;"><ul class="pagination">';

		$config['full_tag_close'] = '</ul></div>';

		$this->pagination->initialize($config); 

		return $this->pagination->create_links();
	}

	function index() {
		$nomor_lokasi = $this->input->get("nomor_lokasi", TRUE);
		$tahun = (int)$this->input->get("tahun", TRUE);
		$filter = $this->input->get("filter", TRUE);
		$filter = $filter === FALSE ? "" : $filter;
		$offset = (int)$this->input->get("per_page", TRUE);


		$this->load->model("simbada/kamus_spk_model", "kamus_spk_model");


		$cfg = array();
		$cfg["total_rows"] = $this->kamus_spk_model->count($tahun, $nomor_lokasi, $filter);


		$this->twig->display("spk/daftar.html", array(
			"title" => "Daftar SPK",
			"daftar" => $this->kamus_spk_model->get_list($tahun, $nomor_lokasi, $filter, $this->per_page, $offset),
			"offset" => $offset,
			"pagination" => $this->setup_pagination($cfg),
			"filter" => $filter,
			"nomor_lokasi" => $nomor_lokasi,
			"tahun" => $tahun
		));
	}

	public function pencarian() {
		$this->load->model("simbada/kamus_spk_model", "kamus_spk_model");


		$r = new stdClass;
		$r->count = 0;


		$nomor_lokasi = $this->input->get("nomor_lokasi");
		$tahun = (int)$this->input->get("tahun", TRUE);
		$filter = $this->input->get("filter");
		$filter = $filter === FALSE ? "" : $filter;
		$offset = (int)$this->input->get("per_page", TRUE);


		$cfg = array();
		$cfg["total_rows"] = $this->kamus_spk_model->count($tahun, $nomor_lokasi, $filter);


		$r->data = $this->kamus_spk_model->get_list($tahun, $nomor_lokasi, $filter, $this->per_page, $offset);
		$r->count = $this->kamus_spk_model->count($tahun, $nomor_lokasi, $filter);
		$r->pagination = $this->setup_pagination($cfg);

		echo json_encode($r);
	}

	public function form($id=FALSE) {
		$this->load->model("simbada/kamus_spk_model", "kamus_spk_model");
		$this->load->model("simbada/kamus_kegiatan_model", "kamus_kegiatan_model");
		$this->load->model("simda/kegiatan_model", "kegiatan_model");

		if ($id !== FALSE) {
			$id_kontrak = base64UrlDecode($id);

			$spk = $this->kamus_spk_model->read($id_kontrak);

			if (!empty($spk)) {
				//$spk["ID_KEGIATAN"] = str_replace($spk["NOMOR_SUB_UNIT"], "", $spk["ID_KEGIATAN"]);

				if (!is_null($spk["TGL_SPK_SP_DOKUMEN"])) {
					$tgl_spk_sp_dokumen = date_create_from_format('Y-m-d', $spk["TGL_SPK_SP_DOKUMEN"]);
					$tgl_spk_sp_dokumen = date_format($tgl_spk_sp_dokumen, 'd-m-Y');

					$spk["TGL_SPK_SP_DOKUMEN"] = $tgl_spk_sp_dokumen;
				}

				if (!is_null($spk["TGL_ADD"])) {
					$tgl_add = date_create_from_format('Y-m-d', $spk["TGL_ADD"]);
					$tgl_add = date_format($tgl_add, 'd-m-Y');

					$spk["TGL_ADD"] = $tgl_add;
				}
			} else {
				$this->session->set_flashdata('validation_errors', "<b>SPK/SP/Kontrak tidak ditemukan!</b>");
				redirect("spk/form");
			}
		} else {
			$spk = $this->session->flashdata('spk');
			//var_dump($spk);
		}

		//var_dump($spk);

		$this->twig->display("spk/form.html", array(
			"title" => "Form SPK",
			"validation_errors" => $this->session->flashdata('validation_errors'),
			"data" => $spk,
			"id" => $id
		));
	}

	public function submit($id = FALSE) {
		$this->load->model("simbada/kamus_spk_model", "kamus_spk_model");
		$this->load->model("simbada/kamus_kegiatan_model", "kamus_kegiatan_model");
		$this->load->model("simda/kegiatan_model", "kegiatan_model");

		$nomor_sub_unit = $this->input->post("nomor_sub_unit", TRUE);
		$kode_kegiatan = $this->input->post("kode_kegiatan", TRUE);
		$no_spk_sp_dokumen = $this->input->post("no_spk_sp_dokumen", TRUE);
		$tgl_spk_sp_dokumen = $this->input->post("tgl_spk_sp_dokumen", TRUE);
		$deskripsi_spk_dokumen = $this->input->post("deskripsi_spk_dokumen", TRUE);
		$rekanan = $this->input->post("rekanan", TRUE);
		$alamat_rekanan = $this->input->post("alamat_rekanan", TRUE);
		$nilai_spk = str_replace(".", "", $this->input->post("nilai_spk", TRUE));
		$termin = $this->input->post("termin", TRUE);
		$no_add = $this->input->post("no_add", TRUE);
		$tgl_add = $this->input->post("tgl_add", TRUE);
		$uraian_add = $this->input->post("uraian_add", TRUE);
		$nilai_add = str_replace(".", "", $this->input->post("nilai_add", TRUE));
		$jml_termin_add = $this->input->post("jml_termin_add", TRUE);
		$tahun_spj = $this->input->post("tahun_spj", TRUE);

		$id_kegiatan = $nomor_sub_unit . $kode_kegiatan . $tahun_spj;
		//$exist_kegiatan = $this->kamus_kegiatan_model->read($id_kegiatan);
		
		$data = array(
			"NOMOR_SUB_UNIT" => $nomor_sub_unit,
			"TAHUN_SPJ" => $tahun_spj,
			"NO_SPK_SP_DOKUMEN" => $no_spk_sp_dokumen,
			"TGL_SPK_SP_DOKUMEN" => $tgl_spk_sp_dokumen,
			"DESKRIPSI_SPK_DOKUMEN" => $deskripsi_spk_dokumen,
			"REKANAN" => $rekanan,
			"ALAMAT_REKANAN" => $alamat_rekanan,
			"NILAI_SPK" => $nilai_spk,
			"TERMIN" => $termin,
			"NO_ADD" => $no_add,
			"TGL_ADD" => $tgl_add,
			"URAIAN_ADD" => $uraian_add,
			"NILAI_ADD" => $nilai_add,
			"JML_TERMIN_ADD" => $jml_termin_add,
			"ID_KEGIATAN" => $id_kegiatan
		);

		$Kd_Urusan = (int)$kode_kegiatan[5];
		$Kd_Bidang = (int)substr($kode_kegiatan,7,2);
		$Kd_Unit = (int)substr($kode_kegiatan,10,2);
		$Kd_Prog = (int)substr($kode_kegiatan,13,2);
		$Kd_Keg = (int)substr($kode_kegiatan,16,2);

		//$nama_kegiatan = $this->kegiatan_model->find_kegiatan_name($tahun_spj, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Prog, $Kd_Keg);

		// $data_kegiatan = array(
		// 	"KODE_KEGIATAN" => $kode_kegiatan,
		// 	"NAMA_KEGIATAN" => $nama_kegiatan['Ket_Kegiatan'],
		// 	"NOMOR_SUB_UNIT" => $nomor_sub_unit,
		// 	"TAHUN" => $tahun_spj,
		// 	"ID_KEGIATAN" => $id_kegiatan
		// );

		$kegiatanHasCreated = TRUE;

		// if(empty($exist_kegiatan)) {
		// 	$kegiatanHasCreated = $this->kamus_kegiatan_model->create($data_kegiatan);
		// }

		if ($tgl_spk_sp_dokumen !== FALSE && $tgl_spk_sp_dokumen !== '') {
			$tgl_spk_sp_dokumen = date_create_from_format('d-m-Y', $tgl_spk_sp_dokumen);
			$tahun = date_format($tgl_spk_sp_dokumen, 'Y');
			$tgl_spk_sp_dokumen = date_format($tgl_spk_sp_dokumen, 'Y-m-d');
		} else {
			$tgl_spk_sp_dokumen = NULL;
		}
		
		if ($tgl_add !== FALSE && $tgl_add !== '') {
			$tgl_add = date_create_from_format('d-m-Y', $tgl_add);
			$tahun_add = date_format($tgl_add, 'Y');
			$tgl_add = date_format($tgl_add, 'Y-m-d');
		} else {
			$tahun_add = '0000';
			$tgl_add = NULL;
		}

		$addendum = (($no_add !== "" || $tgl_add !== null || $uraian_add !== "" || $nilai_add !== "0" || $tahun_add !== "0000" || $jml_termin_add !== "")) ? 1 : 0;

		//if ($id === FALSE && $kegiatanHasCreated === TRUE)
		if ($id === FALSE) {
			$id_kontrak = $no_spk_sp_dokumen . $tahun_spj . $nomor_sub_unit;

			$exist = $this->kamus_spk_model->read($id_kontrak);

			if (empty($exist)) {
				$isCreated = $this->kamus_spk_model->create(
					$id_kontrak,
					$nomor_sub_unit, 
					$id_kegiatan, 
					$no_spk_sp_dokumen, 
					$tgl_spk_sp_dokumen, 
					$deskripsi_spk_dokumen, 
					$nilai_spk === FALSE || $nilai_spk === '' ? 0 : $nilai_spk, 
					$tahun_spj, 
					$rekanan, 
					$alamat_rekanan, 
					NULL, 
					$termin, 
					$addendum, 
					$no_add, 
					$tgl_add, 
					$uraian_add, 
					$nilai_add === FALSE || $nilai_add === '' ? 0 : $nilai_add, 
					$tahun_add, 
					$jml_termin_add
				);

				if ($isCreated) {
					$id = base64UrlEncode($id_kontrak);
				} else {
					$this->session->set_flashdata('spk', $data);
					$this->session->set_flashdata('validation_errors', "<b>Data tidak dapat disimpan!</b>");
				}
			} else {
				$this->session->set_flashdata('spk', $data);
				$this->session->set_flashdata('validation_errors', "<b>No.SPK/SP/Kontrak telah digunakan!</b>");
			}
		} else {
			$id_kontrak = base64UrlDecode($id);

			$isUpdated = $this->kamus_spk_model->update(
				$id_kontrak,
				$nomor_sub_unit, 
				$id_kegiatan, 
				$no_spk_sp_dokumen, 
				$tgl_spk_sp_dokumen, 
				$deskripsi_spk_dokumen, 
				$nilai_spk === FALSE || $nilai_spk === '' ? 0 : $nilai_spk, 
				$tahun_spj, 
				$rekanan, 
				$alamat_rekanan, 
				NULL, 
				$termin, 
				$addendum, 
				$no_add, 
				$tgl_add, 
				$uraian_add, 
				$nilai_add === FALSE || $nilai_add === '' ? 0 : $nilai_add, 
				$tahun_add, 
				$jml_termin_add
			);

			if (!$isUpdated) {
				$this->session->set_flashdata('spk', $data);
				$this->session->set_flashdata('validation_errors', "<b>Data tidak dapat disimpan!</b>");
			}
		}
		
		redirect("spk/form/" . $id);
	}

	public function hapus($id = FALSE) {
		$this->load->model("simbada/kamus_spk_model", "kamus_spk_model");
		if ($id !== FALSE) {
			$this->kamus_spk_model->delete(base64UrlDecode($id));
		}

		redirect("spk/index");
	}

	public function find_range_by_term() {
		$this->load->model("simbada/kamus_spk_model", "kamus_spk_model");
		$this->load->model("simbada/kamus_lokasi_model", "kamus_lokasi_model");

		$term = $this->input->post("term", TRUE);
		$tahun = $this->input->post("tahun", TRUE);
		$nomor_lokasi = $this->input->post("nomor_lokasi", TRUE);

		$nomor_sub_unit = $this->kamus_lokasi_model->find_by_nomor_lokasi($nomor_lokasi);
		$nomor_sub_unit = empty($nomor_sub_unit) ? "" : $nomor_sub_unit["NOMOR_SUB_UNIT"];

		$r = new stdClass;

		$r->data = $this->kamus_spk_model->get_list($tahun, $nomor_sub_unit, $term, 30);

		echo json_encode($r);
	}

	public function get_list_kegiatan() {
		$this->load->model("simbada/kamus_kegiatan_model", "kamus_kegiatan_model");

		$nomor_sub_unit = $this->input->post("nomor_sub_unit", TRUE);
		$tahun_spj = $this->input->post("tahun_spj", TRUE);

		$r = new stdClass;
		$r->data = $this->kamus_kegiatan_model->find_all_by_skpd_and_tahun($nomor_sub_unit, $tahun_spj);

		echo json_encode($r);
	}

	public function get_list_kegiatan_by_nomor_lokasi() {
		$this->load->model("simbada/kamus_kegiatan_model", "kamus_kegiatan_model");
		$this->load->model("simbada/kamus_lokasi_model", "kamus_lokasi_model");


		$nomor_lokasi = $this->input->post("nomor_lokasi", TRUE);
		$tahun_spj = $this->input->post("tahun_spj", TRUE);


		$nomor_sub_unit = $this->kamus_lokasi_model->find_by_nomor_lokasi($nomor_lokasi);
		$nomor_sub_unit = empty($nomor_sub_unit) ? "" : $nomor_sub_unit["NOMOR_SUB_UNIT"];


		$r = new stdClass;
		$r->data = $this->kamus_kegiatan_model->find_all_by_skpd_and_tahun($nomor_sub_unit, $tahun_spj);

		echo json_encode($r);
	}

	public function set_spk($no_spk, $tgl_spk, $deskripsi, $rekanan, $nilai_spk, $id_kegiatan)
	{
		return $this->db->query("
			INSERT INTO KAMUS_SPK(NO_SPK_SP_DOKUMEN, TGL_SPK_SP_DOKUMEN, DESKRIPSI_SPK_DOKUMEN, REKANAN, NILAI_SPK, ID_KEGIATAN)
			VALUES($no_spk, $tgl_spk, $deskripsi, $rekanan, $nilai_spk, $id_kegiatan)
			RETURNING NO_SPK_SP_DOKUMEN, DESKRIPSI_SPK_DOKUMEN
		");
	}

	public function get_list_kegiatan_simda() {
		$this->load->model("simda/kegiatan_model", "kegiatan_model");
		//$this->load->model("simbada/kamus_lokasi_model", "kamus_lokasi_model");
		$this->load->model("sub_unit_model");

		$tahun_spj = $this->input->post("tahun_spj", TRUE);
		$nomor_sub_unit = $this->input->post("nomor_sub_unit", TRUE);
		$bidang_kegiatan = $this->input->post("bidang_kegiatan", TRUE);
		
		$Kd_Urusan = $this->sub_unit_model->get_kd_urusan($nomor_sub_unit);
		$Kd_Bidang = $this->sub_unit_model->get_kd_bidang($nomor_sub_unit);
		$Kd_Unit = $this->sub_unit_model->get_kd_unit($nomor_sub_unit);
		$Kd_Sub = $this->sub_unit_model->get_kd_sub_unit($nomor_sub_unit);

		//var_dump($Kd_Urusan["kdurusan"], $Kd_Bidang["kdbidang"], $Kd_Unit["kdunit"], $Kd_Sub["kdsubunit"]);

		$r = new stdClass;
//		$r->data = $this->kegiatan_model->find_ta_kegiatan_by_sub_unit($Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Sub, $tahun_spj);
		$r->data = array();
		$i = 0;

		foreach ($this->kegiatan_model->find_ta_kegiatan_by_sub_unit($tahun_spj, $Kd_Urusan["kdurusan"], $Kd_Bidang["kdbidang"], $Kd_Unit["kdunit"], $Kd_Sub["kdsubunit"]) as $value) {	
			$value["Kd_Bidang"] = str_pad($value["Kd_Bidang"], 2,"0", STR_PAD_LEFT);
			$value["Kd_Unit"] = str_pad($value["Kd_Unit"], 2, "0", STR_PAD_LEFT);
			$value["Kd_Prog"] = str_pad($value["Kd_Prog"], 2, "0", STR_PAD_LEFT);
			$value["Kd_Keg"] = str_pad($value["Kd_Keg"], 2, "0", STR_PAD_LEFT);
			$r->data[$i++] =
				array(
					"ID" => $bidang_kegiatan . "." . $value["Kd_Urusan"] . "." . $value["Kd_Bidang"] . "." . $value["Kd_Unit"] . "." . $value["Kd_Prog"] . "." . $value["Kd_Keg"], 
					"KETERANGAN" => $value["Ket_Kegiatan"],
					"TAHUN" => $value["Tahun"],
					"LOKASI" => $value["Lokasi"]
				);
		}
		
		echo json_encode($r);
	}

	public function get_kegiatan_simda() {
		$this->load->model("simda/kegiatan_model", "kegiatan_model");
		$this->load->model("sub_unit_model");

		$id_kegiatan = $this->input->post("id_kegiatan", TRUE);
		$bidang_kegiatan = $this->input->post("bidang_kegiatan", TRUE);

		$Tahun = substr($id_kegiatan, 32, 4);
		$Kd_Urusan = (int)substr($id_kegiatan, 19, 1);
		$Kd_Bidang = (int)substr($id_kegiatan, 21, 2);
		$Kd_Unit = (int)substr($id_kegiatan, 24, 2);
		$Kd_Prog = (int)substr($id_kegiatan, 27, 2);
		$Kd_Keg = (int)substr($id_kegiatan, 30, 2);

		$r = new stdClass;
		$r->data = array();
		$i = 0;

		//var_dump($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Prog, $Kd_Keg);
		$kegiatan = $this->kegiatan_model->get_kegiatan($Tahun, $Kd_Urusan, $Kd_Bidang, $Kd_Unit, $Kd_Prog, $Kd_Keg);
		
		if (!empty($kegiatan)) {
			$kegiatan["Kd_Bidang"] = str_pad($kegiatan["Kd_Bidang"], 2,"0", STR_PAD_LEFT);
			$kegiatan["Kd_Unit"] = str_pad($kegiatan["Kd_Unit"], 2, "0", STR_PAD_LEFT);
			$kegiatan["Kd_Prog"] = str_pad($kegiatan["Kd_Prog"], 2, "0", STR_PAD_LEFT);
			$kegiatan["Kd_Keg"] = str_pad($kegiatan["Kd_Keg"], 2, "0", STR_PAD_LEFT);
			$r->data[$i++] =
				array(
				"ID" => $bidang_kegiatan . "." . $kegiatan["Kd_Urusan"] . "." . $kegiatan["Kd_Bidang"] . "." . $kegiatan["Kd_Unit"] . "." . $kegiatan["Kd_Prog"] . "." . $kegiatan["Kd_Keg"], 
				"KETERANGAN" => $kegiatan["Ket_Kegiatan"],
				"TAHUN" => $kegiatan["Tahun"],
				"LOKASI" => $kegiatan["Lokasi"]
			);
		} else {
			$r->data = "Kegiatan Tidak Ditemukan";
		}
		
		echo json_encode($r);
	}
}