<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (! function_exists('array_column')) {
    function array_column(array $input, $columnKey, $indexKey = null) {
        $array = array();
        foreach ($input as $value) {
            if ( ! isset($value[$columnKey])) {
                trigger_error("Key \"$columnKey\" does not exist in array");
                return false;
            }
            if (is_null($indexKey)) {
                $array[] = $value[$columnKey];
            }
            else {
                if ( ! isset($value[$indexKey])) {
                    trigger_error("Key \"$indexKey\" does not exist in array");
                    return false;
                }
                if ( ! is_scalar($value[$indexKey])) {
                    trigger_error("Key \"$indexKey\" does not contain scalar value");
                    return false;
                }
                $array[$value[$indexKey]] = $value[$columnKey];
            }
        }
        return $array;
    }
}

class Persediaan extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
	}

	// -----------------------------------------------------
	// Helper
	// -----------------------------------------------------

	private function terbilang($x)
	{
		$abil = array("", "satu", "dua", "tiga", "empat", "lima", "enam",
			"tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
		if ($x < 12)
			return " " . $abil[$x];
		elseif ($x < 20)
			return $this->terbilang($x - 10) . " belas";
		elseif ($x < 100)
			return $this->terbilang($x / 10) . " puluh" . $this->terbilang($x % 10);
		elseif ($x < 200)
			return " seratus" . $this->terbilang($x - 100);
		elseif ($x < 1000)
			return $this->terbilang($x / 100) . " ratus" . $this->terbilang($x % 100);
		elseif ($x < 2000)
			return " seribu" . $this->terbilang($x - 1000);
		elseif ($x < 1000000)
			return $this->terbilang($x / 1000) . " ribu" . $this->terbilang($x % 1000);
		elseif ($x < 1000000000)
			return $this->terbilang($x / 1000000) . " juta" . $this->terbilang($x % 1000000);
		elseif ($x < 1000000000000)
			return $this->terbilang($x / 1000000000) . " miliar" . $this->terbilang($x % 1000000000);
		elseif ($x < 1000000000000000)
			return $this->terbilang($x / 1000000000000) . " triliun" . $this->terbilang($x % 1000000000000);
	}

	// -----------------------------------------------------
	// Visual
	// -----------------------------------------------------

	public function index(){
		$data = array();
        
        $this->load->model('user_skpd_model');
        $this->load->model('user_organisasi_model');
        
        $session = $this->session->all_userdata();
        
        $uid = $this->session->userdata('uid') === FALSE ? '' : $this->session->userdata('uid');
        
        $oid = $this->user_skpd_model->get_skpd_userid($uid)['oid'];
        $nomor_unit = $this->user_organisasi_model->get_skpd_organisasi($oid)['onomorunit'];

		$data['title'] = "Daftar Kartu Barang";
		$data['skpd'] = $nomor_unit;

		$this->twig->display("simgo/persediaan/daftar_kartu_barang.html", $data);
	}
	
	public function pengadaan(){
		$data = array();
		
		$data['title'] = "Pengadaan Barang";

		$this->twig->display("simgo/persediaan/pengadaan_barang.html", $data);
	}	

	public function barang_masuk($juno = ""){
		$data = array();
        
        $this->load->model('user_skpd_model');
        $this->load->model('user_organisasi_model');
        
        $session = $this->session->all_userdata();
        
        $uid = $this->session->userdata('uid') === FALSE ? '' : $this->session->userdata('uid');
        $oid = $this->user_skpd_model->get_skpd_userid($uid)['oid'];
        $nomor_unit = $this->user_organisasi_model->get_skpd_organisasi($oid)['onomorunit'];

		$data['title'] = "Barang Masuk";
		$data['today'] = date("Y-m-d");
		$data['skpd'] = $nomor_unit;
        $data['juno'] = base64UrlDecode($juno);
        

		$this->twig->display("simgo/persediaan/barang_masuk.html", $data);
	}

	public function barang_keluar($juno = ""){
		$data = array();
        
        $this->load->model('user_skpd_model');
        $this->load->model('user_organisasi_model');
        
        $session = $this->session->all_userdata();
        
        $uid = $this->session->userdata('uid') === FALSE ? '' : $this->session->userdata('uid');
        $oid = $this->user_skpd_model->get_skpd_userid($uid)['oid'];
        $nomor_unit = $this->user_organisasi_model->get_skpd_organisasi($oid)['onomorunit'];

		$data['title'] = "Barang Keluar";
		$data['today'] = date("Y-m-d");
		$data['skpd'] = $nomor_unit;
        $data['juno'] = base64UrlDecode($juno);

		$this->twig->display("simgo/persediaan/barang_keluar.html", $data);
	}
    
    public function stok_opname($juno = ""){
        $data = array();
        
        $this->load->model('user_skpd_model');
        $this->load->model('user_organisasi_model');
		$this->load->model("simgo/persediaan/jurnal_model", "jurnal_model");
        
        $session = $this->session->all_userdata();
        
        $uid = $this->session->userdata('uid') === FALSE ? '' : $this->session->userdata('uid');
        $oid = $this->user_skpd_model->get_skpd_userid($uid)['oid'];
        $nomor_unit = $this->user_organisasi_model->get_skpd_organisasi($oid)['onomorunit'];
        
        $data['title'] = "Stok Opname";
        $data['today'] = date("Y-m-d");
		$data['skpd'] = $nomor_unit;
        $data['juno'] = base64UrlDecode($juno);
        
        $r = $this->jurnal_model->get_kartu_barang_by_nomor_unit($nomor_unit);
        $data['data'] = $r;
        
        // var_dump($r);die;
        
		$this->twig->display("simgo/persediaan/berita_acara_stok_opname.html", $data);
    }

	// -----------------------------------------------------
	// Laporan-Laporan
	// -----------------------------------------------------

	public function buku_penerimaan(){
		$data = array();
        
        $this->load->model('user_skpd_model');
        $this->load->model('user_organisasi_model');
        
        $session = $this->session->all_userdata();
        
        $uid = $this->session->userdata('uid') === FALSE ? '' : $this->session->userdata('uid');
        $oid = $this->user_skpd_model->get_skpd_userid($uid)['oid'];
        $nomor_unit = $this->user_organisasi_model->get_skpd_organisasi($oid)['onomorunit'];

		$data['title'] = "Buku Penerimaan Barang";
		$data['today'] = date("m-Y");
		$data['skpd'] = $nomor_unit;

		$this->twig->display("simgo/persediaan/laporan/buku_penerimaan_barang.html", $data);
	}

	public function buku_barang_pakai_habis(){
		$data = array();
        
        $this->load->model('user_skpd_model');
        $this->load->model('user_organisasi_model');
        
        $session = $this->session->all_userdata();
        
        $uid = $this->session->userdata('uid') === FALSE ? '' : $this->session->userdata('uid');
        $oid = $this->user_skpd_model->get_skpd_userid($uid)['oid'];
        $nomor_unit = $this->user_organisasi_model->get_skpd_organisasi($oid)['onomorunit'];

		$data['title'] = "Buku Barang Pakai Habis";
		$data['today'] = date("m-Y");
		$data['skpd'] = $nomor_unit;

		$this->twig->display("simgo/persediaan/laporan/buku_barang_pakai_habis.html", $data);
	}

	public function katalog_bukti_pengambilan(){
		$data = array();
        
        $this->load->model('user_skpd_model');
        $this->load->model('user_organisasi_model');
        
        $session = $this->session->all_userdata();
        
        $uid = $this->session->userdata('uid') === FALSE ? '' : $this->session->userdata('uid');
        $oid = $this->user_skpd_model->get_skpd_userid($uid)['oid'];
        $nomor_unit = $this->user_organisasi_model->get_skpd_organisasi($oid)['onomorunit'];

		$data['title'] = "Daftar Bukti Pengambilan Barang";
		$data['today'] = date("m-Y");
		$data['skpd'] = $nomor_unit;

		$this->twig->display("simgo/persediaan/laporan/katalog_bukti_pengambilan_barang.html", $data);
	}

	public function bukti_pengambilan($juno=""){
		$data = array();
        
        $this->load->model('user_skpd_model');
        $this->load->model('user_organisasi_model');
		
		$this->load->model('simgo/persediaan/jurnal_model', 'jurnal_model');
        
        $session = $this->session->all_userdata();
        
        $uid = $this->session->userdata('uid') === FALSE ? '' : $this->session->userdata('uid');
        $oid = $this->user_skpd_model->get_skpd_userid($uid)['oid'];
        $nomor_unit = $this->user_organisasi_model->get_skpd_organisasi($oid)['onomorunit'];
		
		$jurnal = $this->jurnal_model->get_list(array("juno" => base64UrlDecode($juno)));
		
		$jutgl = date("m-Y");
		
		if(!empty($jurnal))
		{
			$jutgl = explode("-", $jurnal[0]['jutgl']);
			$jutgl = $jutgl[1]."-".$jutgl[0];
		}

		$data['title'] = "Bukti Pengambilan Barang";
		$data['today'] = date("m-Y");
		$data['skpd'] = $nomor_unit;
		
		$data['juno'] = base64UrlDecode($juno);
        
        $r = $this->data_barang_keluar_terformat(array(
            "tanggal" => $jutgl,
            "skpd" => $nomor_unit,
            "juno" => base64UrlDecode($juno),
        ));
        
        $data['table_data'] = $r;
        
        // var_dump($r);

		$this->twig->display("simgo/persediaan/laporan/bukti_pengambilan_barang.html", $data);
	}

	public function buku_pengeluaran(){
		$data = array();
        
        $this->load->model('user_skpd_model');
        $this->load->model('user_organisasi_model');
        
        $session = $this->session->all_userdata();
        
        $uid = $this->session->userdata('uid') === FALSE ? '' : $this->session->userdata('uid');
        $oid = $this->user_skpd_model->get_skpd_userid($uid)['oid'];
        $nomor_unit = $this->user_organisasi_model->get_skpd_organisasi($oid)['onomorunit'];

		$data['title'] = "Buku Pengeluaran Barang";
		$data['today'] = date("m-Y");
		$data['skpd'] = $nomor_unit;

		$this->twig->display("simgo/persediaan/laporan/buku_pengeluaran_barang.html", $data);
	}

	public function laporan_semester(){
		$data = array();
        
		$this->load->model("skpd_user_model");
        $this->load->model("simgo/persediaan/jurnal_model", "jurnal_model");
        
        $this->load->model('user_skpd_model');
        $this->load->model('user_organisasi_model');
        
        $session = $this->session->all_userdata();
        
        $uid = $this->session->userdata('uid') === FALSE ? '' : $this->session->userdata('uid');
        
        $oid = $this->user_skpd_model->get_skpd_userid($uid)['oid'];
        $nomor_unit = $this->user_organisasi_model->get_skpd_organisasi($oid)['onomorunit'];
        
		$skpds = $this->skpd_user_model->get_skpd_user($uid);
        $tahuns = $this->jurnal_model->get_tahun_ada_transaksi();
        // var_dump($skpds);die;

		$data['title'] = "Laporan Semester";
		$data['today'] = date("m-Y");
		$data['skpd'] = $nomor_unit;
        $data['tahun'] = $tahuns;
        // var_dump($tahuns);die;

		$this->twig->display("simgo/persediaan/laporan/laporan_semester.html", $data);
	}

	public function kartu_barang($kode_barang){
		$data = array();
		$kbid = base64UrlDecode($kode_barang);
        
        $this->load->model('user_skpd_model');
        $this->load->model('user_organisasi_model');
        
        $session = $this->session->all_userdata();
        
        $uid = $this->session->userdata('uid') === FALSE ? '' : $this->session->userdata('uid');
        $oid = $this->user_skpd_model->get_skpd_userid($uid)['oid'];
        $nomor_unit = $this->user_organisasi_model->get_skpd_organisasi($oid)['onomorunit'];

		$data['title'] = "Kartu Barang";
		$data['today'] = date("m-Y");
		$data['skpd'] = $nomor_unit;
		$data['kbid'] = $kbid;

		$this->twig->display("simgo/persediaan/laporan/kartu_barang.html", $data);
	}
	
	public function katalog_stok_opname(){
		$data = array();
        
        $this->load->model('user_skpd_model');
        $this->load->model('user_organisasi_model');
        
        $session = $this->session->all_userdata();
        
        $uid = $this->session->userdata('uid') === FALSE ? '' : $this->session->userdata('uid');
        $oid = $this->user_skpd_model->get_skpd_userid($uid)['oid'];
        $nomor_unit = $this->user_organisasi_model->get_skpd_organisasi($oid)['onomorunit'];

		$data['title'] = "Daftar Stok Opname";
		$data['today'] = date("m-Y");
		$data['skpd'] = $nomor_unit;

		$this->twig->display("simgo/persediaan/katalog_stok_opname.html", $data);
	}
    
    // -----------------------------------------------------
    // Pengaturan
    // -----------------------------------------------------
    
    public function pengaturan_kamus_barang(){
		$data = array();
        
        $this->load->model('user_skpd_model');
        $this->load->model('user_organisasi_model');
        
        $session = $this->session->all_userdata();
        
        $uid = $this->session->userdata('uid') === FALSE ? '' : $this->session->userdata('uid');
        $oid = $this->user_skpd_model->get_skpd_userid($uid)['oid'];
        $nomor_unit = $this->user_organisasi_model->get_skpd_organisasi($oid)['onomorunit'];

		$data['title'] = "Pengaturan Kamus Barang";
		$data['today'] = date("d-m-Y");
		$data['skpd'] = $nomor_unit;

		$this->twig->display("simgo/persediaan/pengaturan/pengaturan_kamus_barang.html", $data);
    }
    
    public function populasi_kamus_barang(){
        $post = $this->input->post();
		$this->load->model("simgo/persediaan/barang_model", "barang_model");
        
        $limit = $post["limit"];
        $offset = ($post["page"]-1)*$limit;
        $filter = $post["filter"];
        
        $term = array(
            "kbkode LIKE '%" . $filter . "%'".
            " OR ".
            "kbkategori LIKE '%" . $filter . "%'".
            " OR ".
            "kbnama LIKE '%" . $filter . "%'".
            " OR ".
            "kbspesifikasi LIKE '%" . $filter . "%'"
        );
        
        $result["total_count"] = $this->barang_model->count($term);
        
        $result["data"] = $this->barang_model->get_list($term,$limit,$offset,array(
            "kbkategori" => "ASC",
            "kbkode" => "ASC"
        ));
        
        echo json_encode($result);
    }
    
    public function simpan_kamus_barang(){
        $post = $this->input->post();
		$this->load->model("simgo/persediaan/barang_model", "barang_model");
        
        $barang = $post["data"];
        
        if($this->barang_model->count(array("kbid" => $barang["kbid"])) > 0) {
            // update
            $this->barang_model->update($barang);
        }
        else {
            // insert
            $this->barang_model->create($barang);
        }
        echo json_encode($barang);
    }
    
    public function barang_check(){
        $post = $this->input->post();
		$this->load->model("simgo/persediaan/barang_model", "barang_model");
        
        $result = $this->barang_model->get_list(array(
            "kbkode" => $post['kbkode'],
        ));
        
        echo json_encode($result);
    }
    
    public function pengaturan_gudang(){
		$data = array();
        
        $this->load->model('user_skpd_model');
        $this->load->model('user_organisasi_model');
        
        $session = $this->session->all_userdata();
        
        $uid = $this->session->userdata('uid') === FALSE ? '' : $this->session->userdata('uid');
        $oid = $this->user_skpd_model->get_skpd_userid($uid)['oid'];
        $nomor_unit = $this->user_organisasi_model->get_skpd_organisasi($oid)['onomorunit'];

		$data['title'] = "Pengaturan Gudang";
		$data['today'] = date("d-m-Y");
		$data['skpd'] = $nomor_unit;

		$this->twig->display("simgo/persediaan/pengaturan/pengaturan_gudang.html", $data);
    }
    
    public function populasi_gudang(){
        $post = $this->input->post();
        $this->load->model("simgo/persediaan/gudang_model", "gudang_model");
        
        $result = $this->gudang_model->get_gudang_detail();
        
        echo json_encode($result);
    }
    
    public function simpan_data_gudang(){
        $post = $this->input->post();
        $this->load->model("simgo/persediaan/gudang_model", "gudang_model");
        
        $gudang = $post["data"];
        
        if($this->gudang_model->count(array("guno" => $gudang["guno"])))
        {
            $this->gudang_model->simpan_gudang($gudang["guno"], $gudang["gualamat"], $gudang["nomor_unit"]);
        }
        else
        {
            $this->gudang_model->tambah_gudang($gudang["guno"], $gudang["gualamat"], $gudang["nomor_unit"]);
        }
    }
    
    public function gudang_check(){
        $post = $this->input->post();
		$this->load->model("simgo/persediaan/gudang_model", "gudang_model");
        
        $result = $this->gudang_model->get_gudang($post["guno"]);
        
        echo json_encode($result);
    }
    
    public function pengaturan_pengguna(){
		$data = array();
        
        $this->load->model('user_skpd_model');
        $this->load->model('user_organisasi_model');
        
        $session = $this->session->all_userdata();
        
        $uid = $this->session->userdata('uid') === FALSE ? '' : $this->session->userdata('uid');
        $oid = $this->user_skpd_model->get_skpd_userid($uid)['oid'];
        $nomor_unit = $this->user_organisasi_model->get_skpd_organisasi($oid)['onomorunit'];

		$data['title'] = "Pengaturan Pengguna";
		$data['today'] = date("d-m-Y");
		$data['skpd'] = $nomor_unit;

		$this->twig->display("simgo/persediaan/pengaturan/pengaturan_pengguna.html", $data);
    }
    
    public function populasi_pengguna(){
        
    }

	// -----------------------------------------------------
	// AJAX Handler(s)
	// -----------------------------------------------------
    
    public function get_list_skpd(){
        $this->load->model("skpd_model");
        $result = $this->skpd_model->get_list();
        
        $r = new stdClass();
        
        $r->results = array();
        
        foreach ($result as $value) {
            array_push($r->results, array(
                "value" => $value["nomor_unit"],
                "id" => $value["nomor_unit"],
                "text" => $value["nama_unit"]
            ));
        }
        
        echo json_encode($r);
    }

	public function get_list_pakai_habis(){
		$this->load->model("simbada/kib_model", "kib_model");
		$term = $this->input->post("term");

		$r = new stdClass;
		$r->results = array();
		$i = 0;

		$pakai_habis = $this->kib_model->get_pakai_habis($term);

		if (!empty($pakai_habis)) {
			foreach ($pakai_habis as $key => $value) {
				array_push($r->results, array("id" => $value["NO_KEY"], "text" => $value["NO_BUKTI_PEROLEHAN"], "value" => $value["NO_BUKTI_PEROLEHAN"], "nomor" => $value["NO_BUKTI_PEROLEHAN"]));
			}
		} else {
			array_push($r->results, array("id" => "01", "text" => "SPK tidak ditemukan", "value" => "SPK tidak ditemukan", "nomor" => "SPK tidak ditemukan"));
		}
		
		echo json_encode($r);
	}

	public function get_vendor() {
		$this->load->model("simbada/kib_model", "kib_model");
		$spk = $this->input->post("spk");

		$r = new stdClass;

		$vendor = $this->kib_model->get_vendor_data($spk);
		$r->data = $vendor;

		echo json_encode($r);
	}

	public function get_list_barang_pakai_habis() {
		$this->load->model("simbada/kib_model", "kib_model");
		$spk = $this->input->post("spk");

		$r = new stdClass;

		$data = $this->kib_model->get_pakai_habis_data($spk);
		$r->data = $data;
		echo json_encode($r);
	}

	public function get_daftar_gudang(){
		$this->load->model("simgo/persediaan/gudang_model", "gudang_model");
		$post = $this->input->post();
		$data = array();

		if(!empty($post["gudang"])){
			$data = $this->gudang_model->search_gudangs_by_skpdid($post["nomor_unit"], $post["gudang"]);
		}
		else
		{
			$data = $this->gudang_model->search_gudangs_by_skpdid($post["nomor_unit"], "");
		}
		
		$r = new stdClass();
		$r->results = array();

		foreach ($data as $value) {
			array_push($r->results, array("value" => $value['guno'], "id" => $value['guno'], "text" => $value['gualamat']));
		}
		echo json_encode($r);
	}
    
    public function get_stok_barang(){
		$this->load->model("simgo/persediaan/barang_model", "barang_model");
		$post = $this->input->post();
		$data = array();
        
        $data = $this->barang_model->get_available_item(
            $post["skpd"],
            date("Y-m-d", strtotime($post["tanggal_jurnal"])),
            // $post["gudang"],
            array("kamus_barang.kbid" => $post["kbid"])
        );
        $stok = 0;
        if(!empty($data))
        {
            $stok = $data[0]["stok"];
        }
        echo json_encode($stok);
    }
    
    public function get_daftar_barang_ada_stok(){
		$this->load->model("simgo/persediaan/barang_model", "barang_model");
		$post = $this->input->post();
		$data = array();
        
        $count;
        $limit = $post["limit"];
        $offset = ($post["page"]-1) * $limit;
        
        
        // if(!empty($post["skpd"]))
        // {
            if(!empty($post["barang"])){
                $count = $this->barang_model->count_available_item(
                    $post["skpd"],
                    date("Y-m-d", strtotime($post["tanggal_jurnal"])),
                    // $post["gudang"],
                    array(
                        "( kamus_barang.kbnama LIKE '%" . $post["barang"] . "%'" .
                        " OR " .
                        "kamus_barang.kbkode LIKE '%" . $post["barang"] . "%'" .
                        " OR "  .
                        "kamus_barang.kbspesifikasi LIKE '%" . $post["barang"] . "%' )",
                        
                    )
                );
                
                $data = $this->barang_model->get_available_item(
                    $post["skpd"],
                    date("Y-m-d", strtotime($post["tanggal_jurnal"])),
                    // $post["gudang"],
                    array(
                        "( kamus_barang.kbnama LIKE '%" . $post["barang"] . "%'" .
                        " OR " .
                        "kamus_barang.kbkode LIKE '%" . $post["barang"] . "%'" .
                        " OR "  .
                        "kamus_barang.kbspesifikasi LIKE '%" . $post["barang"] . "%' )",
                        
                    ),
                    $limit, $offset
                );
            }
            else
            {
                $count = $this->barang_model->count_available_item(
                    $post["skpd"],
                    date("Y-m-d", strtotime($post["tanggal_jurnal"])),
                    // $post["gudang"],
                    array()
                );
                
                $data = $this->barang_model->get_available_item(
                    $post["skpd"],
                    date("Y-m-d", strtotime($post["tanggal_jurnal"])),
                    // $post["gudang"],
                    array(),
                    $limit, $offset
                );
            }
        // }
        // else
        // {
        //     $data = array();
        // }
		
		$r = new stdClass();
		$r->results = array();
        
        $r->page = $post["page"];
        // var_dump($post);die;
        $r->total_count = $count;

		foreach ($data as $value) {
            if($value['stok'] != 0)
			array_push($r->results, array(
                "kode" => $value['kbkode'],
                "id" => $value['kbid'],
                "text" => $value['kbnama'],
                "description" => $value['kbspesifikasi'],
                "satuan" => $value['kbunit'],
                "stok" => $value['stok']
            ));
		}
		echo json_encode($r);
	}

	public function get_daftar_barang(){
		$this->load->model("simgo/persediaan/barang_model", "barang_model");
		$post = $this->input->post();
		$data = array();
        
        $count;
        $limit = $post["limit"];
        $offset = ($post["page"]-1) * $limit;
        
        // if($post["page"]>1){var_dump($post);die;}

		if(!empty($post["barang"])){
            $count = $this->barang_model->count(array(
				"kbnama LIKE '%" . $post["barang"] . "%'" . 
                " OR " .
				"kbkode LIKE '%" . $post["barang"] . "%'" . 
                " OR " .
				"kbspesifikasi LIKE '%" . $post["barang"] . "%'",
				));
                
			$data = $this->barang_model->get_list(array(
				"kbnama LIKE '%" . $post["barang"] . "%'" . 
                " OR " .
				"kbkode LIKE '%" . $post["barang"] . "%'" . 
                " OR " .
				"kbspesifikasi LIKE '%" . $post["barang"] . "%'",
				),$limit, $offset);
		}
		else
		{
			$count = $this->barang_model->count(array(
				"kbnama LIKE '%%'",
				));
                
			$data = $this->barang_model->get_list(array(
				"kbnama LIKE '%%'",
				),$limit, $offset);
		}
		
		$r = new stdClass();
		$r->results = array();
        
        $r->page = $post["page"];
        $r->total_count = $count;

		foreach ($data as $value) {
			array_push($r->results, array(
                "kode" => $value['kbkode'],
                "id" => $value['kbid'],
                "text" => $value['kbnama'],
                "description" => $value['kbspesifikasi'],
                "satuan" => $value['kbunit']));
		}
		echo json_encode($r);
	}

	public function juno_check(){
		$post = $this->input->post(); //var_dump($post);die;

		$this->load->model("simgo/persediaan/jurnal_model", "jurnal_model");
		$this->load->model("simgo/persediaan/jurnal_detail_model", "jurnal_detail_model");
		$this->load->model("simgo/persediaan/barang_model", "barang_model");

		$r['exist'] = $this->jurnal_model->count(array(
            "juno" => $post['juno'],
            "nomor_unit" => $post['nomor_unit'],
            "tipe" => $post['jenis']));
        
        //($this->jurnal_model->count(array("juno" => $post['juno'])) > 0);
		
		if($r['exist']){
			$r['data'] = $this->jurnal_model->get_list(array("juno" => $post['juno'], "deleted IS NULL"));
			$r['data_table'] = $this->jurnal_detail_model->get_list(array("juno" => $post['juno'], "nomor_unit" => $post['nomor_unit'], "deleted IS NULL"));
			
			foreach ($r['data_table'] as $key => $value) {
				$r['data_table'][$key]['namabarang'] = $this->barang_model->get_list(array("kbid" => $value['kbid']));
				if(!empty($r['data_table'][$key]['namabarang'])){
					$r['data_table'][$key]['kbkode'] = $r['data_table'][$key]['namabarang'][0]['kbkode'];
					$r['data_table'][$key]['namabarang'] = $r['data_table'][$key]['namabarang'][0]['kbnama'];
				}
			}
		}

		echo json_encode($r);
	}

	public function get_populasi_kartu_barang(){
		$post = $this->input->post();

		$this->load->model("simgo/persediaan/jurnal_model", "jurnal_model");
        
        $term = "";
        if(array_key_exists("filter", $post))
        {
            $term = $post['filter'];
        }
        
        $limit = $post["limit"];
        $offset = ($post["page"]-1) * $limit;
        $r["total_count"] = $this->jurnal_model->count_kartu_barang_by_nomor_unit($post['nomor_unit'], $term);
		$r["data"] = $this->jurnal_model->get_kartu_barang_by_nomor_unit($post['nomor_unit'], $term, $limit, $offset);

		echo json_encode($r);
	}

	public function get_populasi_kartu_barang_pada_tanggal(){
		$post = $this->input->post();

		$this->load->model("simgo/persediaan/jurnal_model", "jurnal_model");
        
        $tanggal = $post['tanggal'];
        
		$r["data"] = $this->jurnal_model->get_kartu_barang_by_nomor_unit_and_date($post['nomor_unit'], $tanggal);
        // var_dump($tanggal);

		echo json_encode($r);
	}
	
	public function get_populasi_stok_opname(){
		$this->load->model("simgo/persediaan/jurnal_model", "jurnal_model");
		
		$post = $this->input->post();
		
        $bulan = explode("-", $post["tanggal"]);
        $tahun = $bulan[1];
        $bulan = $bulan[0];
		
		$r = $this->jurnal_model->get_list(array(
			"nomor_unit" => $post['skpd'],
			"tipe" => "5",
            "MONTH(jutgl)" => $bulan,
            "YEAR(jutgl)" => $tahun,
            "deleted IS NULL"
		));
		
		echo json_encode($r);
	}

    public function print_stok_opname($namaunit, $skpd, $tanggal, $juno, $pengurusbarang, $nippengurusbarang, $atasanlangsung, $nipatasanlangsung) {
        
        $namaunit = base64UrlDecode($namaunit);
        $skpd = base64UrlDecode($skpd);
        $tanggal = base64UrlDecode($tanggal);
        $juno = base64UrlDecode($juno);
        $pengurusbarang = base64UrlDecode($pengurusbarang);
        $nippengurusbarang = base64UrlDecode($nippengurusbarang);
        $atasanlangsung = base64UrlDecode($atasanlangsung);
        $nipatasanlangsung = base64UrlDecode($nipatasanlangsung);
        
        
        $this->load->library("jasper");

        $this->load->model("simgo/persediaan/barang_model", "barang_model");
        $this->load->model("simgo/persediaan/jurnal_model", "jurnal_model");
        $this->load->model("simgo/persediaan/jurnal_detail_model", "jurnal_detail_model");

        $tgl = $tanggal;
        $tgl = explode("-", $tgl);
        $bulan = $tgl[0];
        $tahun = $tgl[1];
        $avail = [];
        $data = [];

        $r["exist"] = $this->jurnal_model->count(array(
            "juno" => $juno,
            "nomor_unit" => $skpd,
            "tipe" => "5"));
        
        if($r["exist"]){
            $r["data"] = $this->jurnal_model->get_list(array("juno" => $juno, "deleted IS NULL"));
            $r["data_table"] = $this->jurnal_detail_model->get_list(array("juno" => $juno, "nomor_unit" => $skpd, "deleted IS NULL"));
            
            foreach ($r["data_table"] as $key => $value) {
                $r["data_table"][$key]["namabarang"] = $this->barang_model->get_list(array("kbid" => $value['kbid']));
                if(!empty($r["data_table"][$key]["namabarang"])){
                    $r["data_table"][$key]["kbkode"] = $r["data_table"][$key]["namabarang"][0]["kbkode"];
                    $r["data_table"][$key]["namabarang"] = $r["data_table"][$key]["namabarang"][0]["kbnama"];
                }

                $avail[$value['kbid']] = array(
                    "judid" => $value['judid'],
                    "kbid" => $value['kbid'],
                    "kbunit" => $value['judunit'],
                    "selisih" => $value['judqty'],
                    "keterangan" => $value['keterangan']
                );
            }
        }

        $data_buku = $this->jurnal_model->get_kartu_barang_by_nomor_unit_and_date($skpd, $tanggal);

        if (!file_exists("reports/persediaan/stok_opname.jasper")) {
            $this->jasper->compile("reports/persediaan/stok_opname.jrxml")->execute();
        }


    
        //mime
        header('Content-Type: application/pdf');
        //nama file
        header('Content-Disposition: inline;filename="' . preg_replace("/[^A-Za-z0-9 ]/", '', 'kartu_barang') . '.pdf"');
        //tanpa cache
        header('Cache-Control: max-age=0');

        if (PHP_OS === "Linux") {
            setlocale(LC_TIME, "id_ID");
        } else {
            setlocale(LC_TIME, "id");
        }

        $tmpfile = tempnam(sys_get_temp_dir(), 'tmprpt') . ".csv";
        
        // generate csv
        // table head
        $list = array("no;nama_barang;spesifikasi_barang;satuan_barang;jumlah_barang_administratif;jumlah_barang_periksa;selisih;keterangan");
        $selisih = 0;
        $index = 0;
        
        // table data
        if($data_buku) {
            foreach ($data_buku as $key => $value) {
                if ( gettype($avail[$value['kbid']]) != "undefined") {
                    $item = array(
                        "judid"         => $avail[$value["kbid"]]["judid"],
                        "kbid"          => intval($value["kbid"]),
                        "kbnama"        => $value["kbnama"],
                        "kbspesifikasi" => $value["kbspesifikasi"],
                        "kbunit"        => $value["kbunit"],
                        "kbjuml"        => floatval($value["kbjuml"]),
                        "stok"          => floatval(floatval($value["kbjuml"]) + floatval($avail[$value["kbid"]]["selisih"])),
                        "selisih"       => floatval($avail[$value["kbid"]]["selisih"]),
                        "keterangan"    => $avail[$value["kbid"]]["keterangan"]
                    );

                    array_push($data, $item);
                }



                $row = "";
                $row .= $index + 1 . ";" .
                $data[$key]["kbnama"] . ";" .
                $data[$key]["kbspesifikasi"] . ";" .
                $data[$key]["kbunit"] . ";" .
                $data[$key]["kbjuml"] . ";" .
                $data[$key]["stok"] . ";" .
                $data[$key]["selisih"] . ";" .
                $data[$key]["keterangan"];

                array_push($list, $row);
                $index++;
            }

        // var_dump($list);
        // die;
        }

        $file = fopen($tmpfile,"w");
        foreach ($list as $line) {
            fputcsv($file, explode(';', $line), "|");
        }

        fclose($file);

        $this->jasper->process(
            "reports/persediaan/stok_opname.jasper", 
            array(
                "nomor" => $juno,
                "tanggal" => $tanggal,
                "namaunit" => $namaunit,
                "pengurusbarang" => $pengurusbarang,
                "nippengurusbarang" => $nippengurusbarang,
                "atasanlangsung" => $atasanlangsung,
                "nipatasanlangsung" => $nipatasanlangsung
            ),
            array(
                "type" => "csv",
                "source" => $tmpfile,
                "columns" => "value,id,text,satuan",
                "delimiter" => "|"
            ),
            array(
                "pdf"
            )
        )->execute()->output();

        unlink($tmpfile);
    }

	
	public function print_kartu_persediaan(){
		$this->load->library("jasper");
        $this->load->model("simgo/persediaan/barang_model", "barang_model");
        $this->load->model("simgo/persediaan/jurnal_model", "jurnal_model");
        $this->load->model("simgo/persediaan/jurnal_detail_model", "jurnal_detail_model");
        
        $post = array("skpd" => "13.02.06.01");

        $data_buku = $this->jurnal_model->get_kartu_barang_by_nomor_unit($post['skpd']);

		if (!file_exists("reports/persediaan/kartu_persediaan_barang.jasper")) {
			$this->jasper->compile("reports/persediaan/kartu_persediaan_barang.jrxml")->execute();
		}

		//mime
		header('Content-Type: application/pdf');
		//nama file
		header('Content-Disposition: inline;filename="' . preg_replace("/[^A-Za-z0-9 ]/", '', 'kartu_persediaan_barang') . '.pdf"');
		//tanpa cache
		header('Cache-Control: max-age=0');

		if (PHP_OS === "Linux") {
			setlocale(LC_TIME, "id_ID");
		} else {
			setlocale(LC_TIME, "id");
		}

		$tmpfile = tempnam(sys_get_temp_dir(), 'tmprpt') . ".csv";
		
		// generate csv
        // table head
		$list = array("nomor;nama_barang;stok");
        
        // table data
        foreach ($data_buku as $key => $value) {
            $row = "";
        	
            $row .= $data_buku[$key]['kbkode'] . ";" .
		            $data_buku[$key]['kbnama'] . ";" .
		            $data_buku[$key]['kbjuml'];

            array_push($list, $row);
        }

		$file = fopen($tmpfile,"w");
		foreach ($list as $line) {
			fputcsv($file, explode(';', $line), "|");
		}

		fclose($file);

		$this->jasper->process(
			"reports/persediaan/kartu_persediaan_barang.jasper", 
			array(),
			array(
				"type" => "csv",
				"source" => $tmpfile,
				"columns" => "value,id,text,satuan",
				"delimiter" => "|"
			),
			array(
				"pdf"
			)
		)->execute()->output();

		unlink($tmpfile);
    }


    // -----------------------
    // kartu barang
    // -----------------------
	public function get_populasi_detail_kartu_barang(){
		$post = $this->input->post();

		$this->load->model("simgo/persediaan/jurnal_model", "jurnal_model");
        $tgl = $post["tanggal"];
        $tgl = explode("-", $tgl);
        $bulan = $tgl[0];
        $tahun = $tgl[1];
        
        // var_dump($tgl);die;
        
		$r = $this->jurnal_model->get_detail_kartu_barang_by_nomor_unit($post['skpd'], $post['kbid'], $tahun, $bulan);

		echo json_encode($r);
	}

	public function print_kartu_barang($namaunit, $skpd, $kbid, $tanggal, $pengurusbarang, $nippengurusbarang, $atasanlangsung, $nipatasanlangsung) {
        
        $namaunit = base64UrlDecode($namaunit);
        $skpd = base64UrlDecode($skpd);
        $kbid = base64UrlDecode($kbid);
        $tanggal = base64UrlDecode($tanggal);
        $pengurusbarang = base64UrlDecode($pengurusbarang);
        $nippengurusbarang = base64UrlDecode($nippengurusbarang);
        $atasanlangsung = base64UrlDecode($atasanlangsung);
        $nipatasanlangsung = base64UrlDecode($nipatasanlangsung);
        
		$this->load->library("jasper");
        $this->load->model("simgo/persediaan/barang_model", "barang_model");
        $this->load->model("simgo/persediaan/jurnal_model", "jurnal_model");
        $this->load->model("simgo/persediaan/jurnal_detail_model", "jurnal_detail_model");
        
        $post = array("skpd" => $skpd, "kbid" => $kbid);
        $tgl = $tanggal;
        $tgl = explode("-", $tgl);
        $bulan = $tgl[0];
        $tahun = $tgl[1];

        $data_buku = $this->jurnal_model->get_detail_kartu_barang_by_nomor_unit($post['skpd'], $post['kbid'], $tahun, $bulan);

		if (!file_exists("reports/persediaan/kartu_barang.jasper")) {
			$this->jasper->compile("reports/persediaan/kartu_barang.jrxml")->execute();
    	}
    
		//mime
		header('Content-Type: application/pdf');
		//nama file
		header('Content-Disposition: inline;filename="' . preg_replace("/[^A-Za-z0-9 ]/", '', 'kartu_barang') . '.pdf"');
		//tanpa cache
		header('Cache-Control: max-age=0');

		if (PHP_OS === "Linux") {
			setlocale(LC_TIME, "id_ID");
		} else {
			setlocale(LC_TIME, "id");
		}

		$tmpfile = tempnam(sys_get_temp_dir(), 'tmprpt') . ".csv";
		
		// generate csv
        // table head
		$list = array("skpd;nama_barang;satuan_barang;spesifikasi_barang;nomor;tanggal;masuk;keluar;sisa;keterangan");
        $sisa = 0;
        $index = 1;

        if(!empty($data_buku)) {
            $kbnama = $data_buku[0]['kbnama'];
            $kbsatuan = $data_buku[0]['kbunit'];
            $kbspesifikasi = $data_buku[0]['kbspesifikasi'];
        } else {
            $kbnama = "";
            $kbsatuan = "";
            $kbspesifikasi = "";
        }
        
        // table data
        foreach ($data_buku as $key => $value) {
            $sisa = $sisa + floatval($data_buku[$key]['judqty']);
            $row = ";;;";

            $row .= ";";
        	if ($data_buku[$key]['judqty'] > 0) {
            	$row .= $index . ";" .
            	$data_buku[$key]['jutgl'] . ";" .
                $data_buku[$key]['judqty'] . ";" .
                "0" . ";" .
                $sisa . ";" .
                $data_buku[$key]['keterangan'];
            } else if ($data_buku[$key]['judqty'] < 0) {
            	$row .= $index . ";" .
            	$data_buku[$key]['jutgl'] . ";" .
            	"0" . ";" .
                $data_buku[$key]['judqty'] . ";" .
                $sisa . ";" .
                $data_buku[$key]['keterangan'];
            } 

            array_push($list, $row);
            $index++;
        }

		$file = fopen($tmpfile,"w");
		foreach ($list as $line) {
			fputcsv($file, explode(';', $line), "|");
		}

		fclose($file);

		$this->jasper->process(
			"reports/persediaan/kartu_barang.jasper", 
			array(
				"namaunit" => $namaunit,
				"pengurusbarang" => $pengurusbarang,
				"nippengurusbarang" => $nippengurusbarang,
				"atasanlangsung" => $atasanlangsung,
				"nipatasanlangsung" => $nipatasanlangsung,
                "kbnama" => $kbnama, 
                "kbsatuan" => $kbsatuan,
                "kbspesifikasi" => $kbspesifikasi
			),
			array(
				"type" => "csv",
				"source" => $tmpfile,
				"columns" => "value,id,text,satuan",
				"delimiter" => "|"
			),
			array(
				"pdf"
			)
		)->execute()->output();

		unlink($tmpfile);
    }
    
    // -----------------------
    // buku pengambilan barang
    // -----------------------
    
    // untuk buku pengambilan dan pengeluaran barang
    private function data_barang_keluar_terformat($post){
        $this->load->model("simgo/persediaan/barang_model", "barang_model");
        $this->load->model("simgo/persediaan/jurnal_model", "jurnal_model");
        $this->load->model("simgo/persediaan/jurnal_detail_model", "jurnal_detail_model");
        
        $bulan = explode("-", $post["tanggal"]);
        $tahun = $bulan[1];
        $bulan = $bulan[0];
        
        $term = array(
            "nomor_unit" => $post["skpd"],
            "MONTH(jutgl)" => $bulan,
            "YEAR(jutgl)" => $tahun,
            "tipe" => "1",
            "deleted IS NULL"
        );
        
        if(array_key_exists("juno", $post)){
            $juno = array("juno" => $post["juno"]);
            $term = array_merge($term, $juno);
            // var_dump($term);die;
        }
        
        $r=$this->jurnal_model->get_list($term,0,0,array("jutgl" => "ASC", "juno" => "ASC"));
        
        // data structure ::
        // $fifo_pool["kbkode"]["flow_keluar_atau_masuk"]["jurnal_detail_list"]
        $fifo_pool = array();
        // $jurnal_keluar[index]
        $jurnal_keluar = array();
        
        foreach ($r as $key => $value) {
            
            $value["detail"] = array();
            array_push($jurnal_keluar, $value);
            
            $detail = $this->jurnal_detail_model->get_list(array(
                "juno" => $value["juno"],
                "nomor_unit" => $post["skpd"],
                "deleted IS NULL",
            ));
            
            // spesifikasi nama barang dan masukkan ke dalam fifo_pool
            foreach ($detail as $detail_key => $detail_value) {
                $detail[$detail_key]['namabarang'] = $this->barang_model->get_list(array(
                    "kbid" => $detail[$detail_key]['kbid']
                ));
                
                if(!empty($detail[$detail_key]['namabarang'])){
                    $detail[$detail_key]['spesifikasi'] = $detail[$detail_key]['namabarang'][0]['kbspesifikasi'];
                    $detail[$detail_key]['kbkode'] = $detail_value['kbkode'] = $detail[$detail_key]['namabarang'][0]['kbkode'];
                    $detail[$detail_key]['namabarang'] = $detail[$detail_key]['namabarang'][0]['kbnama'];
                }
                
                $detail[$detail_key]["qty_huruf"] = $this->terbilang(abs(floatval($detail[$detail_key]['judqty'])));
                
                // put jurnal_detail in fifo_pool
                    $kbkode = $detail_value['kbkode'];
                    $flow   = ($detail_value['judqty'] > 0)?1:-1;
                    
                    if(empty($fifo_pool[$kbkode])){
                        $fifo_pool[$kbkode] = array();
                    }
                    if(empty($fifo_pool[$kbkode][$flow])){
                        $fifo_pool[$kbkode][$flow] = array();
                    }
                    
                    if($flow < 0)
                    {
                        array_push($fifo_pool[$kbkode][$flow], $detail[$detail_key]);
                    }
                // ------------------------------
            }
        }
        
        // disini memecah pengeluaran yang mengeluarkan barang dari dua jurnal penerimaan berbeda
        foreach ($fifo_pool as $pool_key => $pool_value) {
            // populasikan fifo_pool -> barang masuk
            if(empty($fifo_pool[$pool_key]["1"])){
                $exist = $this->jurnal_detail_model->get_exist_goods_procurement($post["skpd"], $pool_key, $tahun, $bulan);
                // $fifo_pool[$pool_key]["1"] = $exist;
                $current = $this->jurnal_detail_model->get_this_month_procurement($post["skpd"], $pool_key, $tahun, $bulan);
                $fifo_pool[$pool_key]["1"] = array_merge($exist,$current);
            }
            
            // "ambil" barang yang dikeluarkan dari stok per pengadaan
            $result = array();
            $avail  = $fifo_pool[$pool_key]["1"];
            
            if(array_key_exists("-1", $pool_value)){
                // satu-per-satu
                foreach ($pool_value["-1"] as $single_key => $single) {
                    $out_remain= -floatval($single['judqty']);
                    
                    // ambil barang dari pengadaan yang bisa habis untuk pengeluaran ini
                    while(!empty($avail) && $avail[0]['judqty'] <= $out_remain)
                    {
                        $t = $single;
                        $out_remain -= $avail[0]["judqty"];
                        $t['judqty'] = -$avail[0]["judqty"];
                        $t['qty_huruf'] = $this->terbilang(abs(floatval($t['judqty'])));
                        $t['judhargasat'] = $avail[0]['judhargasat'];
                        array_push($result, $t);
                        array_shift($avail);
                    }
                    
                    // sisa pengeluaran yang belum diambil dari stok pengadaan diambil dari stok teratas, yang ini ada sisanya
                    if(!empty($avail) && $out_remain > 0){
                        $t = $single;
                        $t['judqty'] = -$out_remain;
                        $t['qty_huruf'] = $this->terbilang(abs(floatval($t['judqty'])));
                        $t['judhargasat'] = $avail[0]['judhargasat'];
                        array_push($result, $t);
                        $avail[0]['judqty'] -= $out_remain;
                    }
                }
            }
            
            // masukkan detail data yang sudah dipecah ke jurnal barang keluar yang telah disiapkan
            foreach ($result as $result_key => $result_val) {
                $jurnal_key = array_search($result_val['juno'], array_column($jurnal_keluar, 'juno'));
                array_push($jurnal_keluar[$jurnal_key]['detail'], $result_val);
            }
        }
        
        return $jurnal_keluar;
    }
    
    public function get_populasi_bukti_pengambilan_barang(){
        $post = $this->input->post();
        
        $r = $this->data_barang_keluar_terformat($post);
        
        echo json_encode($r);
    }
    
    public function print_bukti_pengambilan_barang($namaunit, $skpd, $juno, $tanggal, $pengurusbarang, $nippengurusbarang, $penerima, $nippenerima, $atasanlangsung, $nipatasanlangsung) {
		$this->load->library("jasper");

        $namaunit = base64UrlDecode($namaunit);
        $skpd = base64UrlDecode($skpd);
        $juno = base64UrlDecode($juno);
        $tanggal = base64UrlDecode($tanggal);
        $pengurusbarang = base64UrlDecode($pengurusbarang);
        $nippengurusbarang = base64UrlDecode($nippengurusbarang);
        $penerima = base64UrlDecode($penerima);
        $nippenerima = base64UrlDecode($nippenerima);
        $atasanlangsung = base64UrlDecode($atasanlangsung);
        $nipatasanlangsung = base64UrlDecode($nipatasanlangsung);
        
        $real_tanggal = $tanggal;
        $real_tanggal = explode("-", $real_tanggal);
        $real_tanggal = $real_tanggal[2] . "-" . $real_tanggal[1] . "-" . $real_tanggal[0];
        
        $this->load->model("simgo/persediaan/jurnal_model", "jurnal_model");
        $jurnal = $this->jurnal_model->get_list(array("juno" => $juno));
        
        if(!empty($jurnal)){
            $tanggal = $jurnal[0]["jutgl"];
            $tanggal = explode("-", $tanggal);
            $tanggal = $tanggal[1]."-".$tanggal[0];
        }
        
        $post = array("skpd" => $skpd, "tanggal" => $tanggal, "juno" => $juno);
        
        $data_buku = $this->data_barang_keluar_terformat($post);

		if (!file_exists("reports/persediaan/bukti_pengambilan_barang.jasper")) {
			$this->jasper->compile("reports/persediaan/bukti_pengambilan_barang.jrxml")->execute();
		}

		//mime
		header('Content-Type: application/pdf');
		//nama file
		header('Content-Disposition: inline;filename="' . preg_replace("/[^A-Za-z0-9 ]/", '', 'bukti_pengambilan_barang') . '.pdf"');
		//tanpa cache
		header('Cache-Control: max-age=0');

		if (PHP_OS === "Linux") {
			setlocale(LC_TIME, "id_ID");
		} else {
			setlocale(LC_TIME, "id");
		}

		$tmpfile = tempnam(sys_get_temp_dir(), 'tmprpt') . ".csv";
        
		// generate csv
        // table head
		$list = array("no_sppb;tgl_sppb;nama;satuan;jumlah_barang;jumlah_harga");
        
        // table data
        $data = [];
        foreach ($data_buku as $jukey => $jurnal) {
            foreach ($jurnal['detail'] as $judkey => $jurnal_detail) {
                $row = "";
                if($judkey == 0)
                    $row .= $jurnal['judasarno'] . ";" . $jurnal['judasartgl'] . ";";
                else
                    $row .= ";;";
                    $row .= $jurnal_detail['namabarang'] . " " . $jurnal_detail['spesifikasi'] . ";" .
                    $jurnal_detail['judunit'] . ";" .
                    -$jurnal_detail['judqty'] . ";" .
                    "Rp" . number_format(-($jurnal_detail['judqty']*$jurnal_detail['judhargasat']), 2, ",", ".");
                array_push($list,$row);
            }
        }

		$file = fopen($tmpfile,"w");

		foreach ($list as $line) {
			fputcsv($file, explode(';', $line), "|");
		}

		fclose($file);


		$this->jasper->process(
			"reports/persediaan/bukti_pengambilan_barang.jasper", 
			array(
				"namaunit" => $namaunit,
				"juno" => $juno,
                "tanggal" => $real_tanggal,
				"pengurusbarang" => $pengurusbarang,
				"nippengurusbarang" => $nippengurusbarang,
                "penerima" => $penerima,
                "nippenerima" => $nippenerima,
				"atasanlangsung" => $atasanlangsung,
				"nipatasanlangsung" => $nipatasanlangsung
			),
			array(
				"type" => "csv",
				"source" => $tmpfile,
				"columns" => "value,id,text,satuan",
				"delimiter" => "|"
			),
			array(
				"pdf"
			)
		)->execute()->output();

		unlink($tmpfile);
    }

    public function print_sppb($namaunit, $alamatunit, $skpd, $juno, $tanggal_jurnal, $jenis, $nomor_sppb, $dari, $kepada, $alamat_kepada, $dasar_no, $dasar_tgl, $tanggal, $pengurusbarang, $nippengurusbarang) {
        
        $namaunit = base64UrlDecode($namaunit);
        $alamatunit = base64UrlDecode($alamatunit);
        $skpd = base64UrlDecode($skpd);
        $juno = base64UrlDecode($juno);
        $tanggal_jurnal = base64UrlDecode($tanggal_jurnal);
        $jenis = base64UrlDecode($jenis);
        $nomor_sppb = base64UrlDecode($nomor_sppb);
        $dari = base64UrlDecode($dari);
        $kepada = base64UrlDecode($kepada);
        $alamat_kepada = base64UrlDecode($alamat_kepada);
        $dasar_no = base64UrlDecode($dasar_no);
        $dasar_tgl = base64UrlDecode($dasar_tgl);
        $tanggal = base64UrlDecode($tanggal);
        $pengurusbarang = base64UrlDecode($pengurusbarang);
        $nippengurusbarang = base64UrlDecode($nippengurusbarang);
        $untuk = "";
        
        $this->load->library("jasper");
        $this->load->model("simgo/persediaan/jurnal_model", "jurnal_model");
        $this->load->model("simgo/persediaan/jurnal_detail_model", "jurnal_detail_model");
        $this->load->model("simgo/persediaan/barang_model", "barang_model");

        $exist = $this->jurnal_model->count(array(
            "juno" => $juno,
            "nomor_unit" => $skpd,
            "tipe" => $jenis));
        
        if($exist){
            $data = $this->jurnal_model->get_list(array("juno" => $juno, "deleted IS NULL"));
            $data_buku = $this->jurnal_detail_model->get_list(array("juno" => $juno, "nomor_unit" => $skpd, "deleted IS NULL"));
            $untuk = $data[0]["untuk"];
        }
        
        $tanggal_jurnal = explode("-", $tanggal_jurnal);
        $tanggal_jurnal = $tanggal_jurnal[1]."-".$tanggal_jurnal[0];

        $post = array("skpd" => $skpd, "tanggal" => $tanggal_jurnal, "juno" => $juno);
        
        $data_keluar = $this->data_barang_keluar_terformat($post);

        // var_dump($tanggal_jurnal);die;

        if (!file_exists("reports/persediaan/sppb.jasper")) {
            $this->jasper->compile("reports/persediaan/sppb.jrxml")->execute();
        }

        //mime
        header('Content-Type: application/pdf');
        //nama file
        header('Content-Disposition: inline;filename="' . preg_replace("/[^A-Za-z0-9 ]/", '', 'sppb') . '.pdf"');
        //tanpa cache
        header('Cache-Control: max-age=0');

        if (PHP_OS === "Linux") {
            setlocale(LC_TIME, "id_ID");
        } else {
            setlocale(LC_TIME, "id");
        }

        $tmpfile = tempnam(sys_get_temp_dir(), 'tmprpt') . ".csv";

        // generate csv
        // table head
        $list = array("nomor_index;nama_barang;banyaknya;harga_satuan;jumlah;keterangan");
        $index = 0;
        
        //table data
        foreach ($data_buku as $key => $value) {
            $data_buku[$key]['namabarang'] = $this->barang_model->get_list(array("kbid" => $value['kbid']));
            if(!empty($data_buku[$key]['namabarang'])){
                $data_buku[$key]['kbkode'] = $data_buku[$key]['namabarang'][0]['kbkode'];
                $data_buku[$key]['namabarang'] = $data_buku[$key]['namabarang'][0]['kbnama'];
            }

            $row = "";
            $row .= $index + 1 . ";" .
            $data_buku[$key]['namabarang'] . ";" .
            $data_buku[$key]['judqty'] . ";" .
            $data_keluar[0]["detail"][0]["judhargasat"] . ";" .
            floatval($data_buku[$key]['judqty'])*floatval($data_keluar[0]["detail"][0]["judhargasat"]) . ";" .
            $data_buku[$key]['keterangan'];

            array_push($list,$row);
        }

        $file = fopen($tmpfile,"w");

        foreach ($list as $line) {
            fputcsv($file, explode(';', $line), "|");
        }

        fclose($file);

        $this->jasper->process(
            "reports/persediaan/sppb.jasper", 
            array(
                "namaunit" => $namaunit, 
                "alamatunit" => $alamatunit,
                "pengurusbarang" => $pengurusbarang, 
                "nippengurusbarang" => $nippengurusbarang,  
                "nomor_sppb" => $nomor_sppb,
                "dari" => $dari,
                "kepada" => $kepada,
                "alamat_kepada" => $alamat_kepada,
                "untuk" => $untuk,
                "dasar_no" => $dasar_no,
                "dasar_tgl" => $dasar_tgl,
                "tanggal" => $tanggal
                ),
            array(
                "type" => "csv",
                "source" => $tmpfile,
                "columns" => "value,id,text,satuan",
                "delimiter" => "|"
            ),
            array(
                "pdf"
            )
        )->execute()->output();

        unlink($tmpfile);
    }
    
    // -----------------------
    // buku pengeluaran barang
    // -----------------------
        
    public function get_populasi_buku_pengeluaran_barang(){
        $post = $this->input->post();
        
        $r = $this->data_barang_keluar_terformat($post);
        
        echo json_encode($r);
    }
    
    public function print_buku_pengeluaran_barang($namaunit, $skpd, $bulan, $pengurusbarang, $nippengurusbarang, $atasanlangsung, $nipatasanlangsung) {
		
        $namaunit = base64UrlDecode($namaunit);
        $skpd = base64UrlDecode($skpd);
        $bulan = base64UrlDecode($bulan);
        $pengurusbarang = base64UrlDecode($pengurusbarang);
        $nippengurusbarang = base64UrlDecode($nippengurusbarang);
        $atasanlangsung = base64UrlDecode($atasanlangsung);
        $nipatasanlangsung = base64UrlDecode($nipatasanlangsung);
        
        $this->load->library("jasper");
        
        $post = array("skpd" => $skpd, "tanggal" => $bulan);
        
        $data_buku = $this->data_barang_keluar_terformat($post);

		if (!file_exists("reports/persediaan/buku_pengeluaran_barang.jasper")) {
			$this->jasper->compile("reports/persediaan/buku_pengeluaran_barang.jrxml")->execute();
		}

		//mime
		header('Content-Type: application/pdf');
		//nama file
		header('Content-Disposition: inline;filename="' . preg_replace("/[^A-Za-z0-9 ]/", '', 'buku_pengeluaran_barang') . '.pdf"');
		//tanpa cache
		header('Cache-Control: max-age=0');

		if (PHP_OS === "Linux") {
			setlocale(LC_TIME, "id_ID");
		} else {
			setlocale(LC_TIME, "id");
		}

		$tmpfile = tempnam(sys_get_temp_dir(), 'tmprpt') . ".csv";
        
		// generate csv
        // table head
		$list = array("no_sppb;tgl_sppb;nama;spesifikasi;untuk;satuan;jumlah_barang;harga_satuan;jumlah_harga;keterangan");
        
        // table data
        foreach ($data_buku as $jukey => $jurnal) {
            foreach ($jurnal['detail'] as $judkey => $jurnal_detail) {
                $row = "";
                if($judkey == 0)
                    $row .= $jurnal['juno'] . ";" . $jurnal['jutgl'] . ";";
                else
                    $row .= ";;";
                    $row .= $jurnal_detail['namabarang'] . ";" .
                    $jurnal_detail['spesifikasi'] . ";" .
                    (($judkey == 0) ? $jurnal['untuk'] : "") . ";" .
                    $jurnal_detail['judunit'] . ";" .
                    -$jurnal_detail['judqty'] . ";" .
                    "Rp" . number_format($jurnal_detail['judhargasat'],2,",",".") . ";" .
                    "Rp" . number_format(-($jurnal_detail['judqty']*$jurnal_detail['judhargasat']), 2, ",", ".") . ";" .
                    (($judkey == 0) ? $jurnal['keterangan'] : "");
                array_push($list,$row);
            }
        }

		$file = fopen($tmpfile,"w");

		foreach ($list as $line) {
			fputcsv($file, explode(';', $line), "|");
		}

		fclose($file);


		$this->jasper->process(
			"reports/persediaan/buku_pengeluaran_barang.jasper", 
			array(
				"namaunit" => $namaunit, 
				"pengurusbarang" => $pengurusbarang, 
				"nippengurusbarang" => $nippengurusbarang, 
				"atasanlangsung" => $atasanlangsung, 
				"nipatasanlangsung" => $nipatasanlangsung),
			array(
				"type" => "csv",
				"source" => $tmpfile,
				"columns" => "value,id,text,satuan",
				"delimiter" => "|"
			),
			array(
				"pdf"
			)
		)->execute()->output();

		unlink($tmpfile);
    }
    
    // ----------------------
    // buku penerimaan barang
    // ----------------------
    
    public function get_populasi_buku_penerimaan_barang(){
        $post = $this->input->post();
        
        $this->load->model("simgo/persediaan/barang_model", "barang_model");
        $this->load->model("simgo/persediaan/jurnal_model", "jurnal_model");
        $this->load->model("simgo/persediaan/jurnal_detail_model", "jurnal_detail_model");
        
        $bulan = explode("-", $post["tanggal"]);
        $tahun = $bulan[1];
        $bulan = $bulan[0];
        
        $r=$this->jurnal_model->get_list(array(
            "nomor_unit" => $post["skpd"],
            "MONTH(jutgl)" => $bulan,
            "YEAR(jutgl)" => $tahun,
            "tipe" => "0",
            "deleted IS NULL"
        ),0,0,array("jutgl" => "ASC"));
        
        foreach ($r as $key => $value) {
            $detail = $this->jurnal_detail_model->get_list(array(
                "juno" => $value["juno"],
                "nomor_unit" => $post["skpd"],
                "deleted IS NULL"
            ));
            
            foreach ($detail as $detail_key => $detail_value) {
                $detail[$detail_key]['namabarang'] = $this->barang_model->get_list(array(
                    "kbid" => $detail[$detail_key]['kbid']
                ));
                
                if(!empty($detail[$detail_key]['namabarang'])){
                    $detail[$detail_key]['spesifikasi'] = $detail[$detail_key]['namabarang'][0]['kbspesifikasi'];
                    $detail[$detail_key]['namabarang'] = $detail[$detail_key]['namabarang'][0]['kbnama'];
                }
                
                $detail[$detail_key]["totalhargadalamhuruf"] = $this->terbilang(floatval($detail[$detail_key]['judqty']));
            }
            
            usort($detail, function ($a, $b) {
                return strcmp($a["namabarang"], $b["namabarang"]);
            });
            
            $r[$key]["detail"] = $detail;
        }
        
        echo json_encode($r);
    }
    
    public function print_buku_penerimaan_barang($namaunit, $skpd, $bulan, $pengurusbarang, $nippengurusbarang, $atasanlangsung, $nipatasanlangsung){
		$this->load->library("jasper");
        $this->load->model("simgo/persediaan/barang_model", "barang_model");
        $this->load->model("simgo/persediaan/jurnal_model", "jurnal_model");
        $this->load->model("simgo/persediaan/jurnal_detail_model", "jurnal_detail_model");

        $namaunit = base64UrlDecode($namaunit);
        $skpd = base64UrlDecode($skpd);
        $bulan = base64UrlDecode($bulan);
        $pengurusbarang = base64UrlDecode($pengurusbarang);
        $nippengurusbarang = base64UrlDecode($nippengurusbarang);
        $atasanlangsung = base64UrlDecode($atasanlangsung);
        $nipatasanlangsung = base64UrlDecode($nipatasanlangsung);
        
		$post = array("skpd" => $skpd, "tanggal" => $bulan);
		
        $tahun = explode( '-', $bulan)[1];
        $bulan2 = explode('-', $bulan)[0];
       
        $data_buku = $this->jurnal_model->get_list(array(
            "nomor_unit" => $post["skpd"],
            "MONTH(jutgl)" => $bulan2,
            "YEAR(jutgl)" => $tahun,
            "tipe" => "0",
            "deleted IS NULL"
        ),0,0,array("jutgl" => "ASC"));

		if (!file_exists("reports/persediaan/buku_penerimaan_barang.jasper")) {
			$this->jasper->compile("reports/persediaan/buku_penerimaan_barang.jrxml")->execute();
		}

		//mime
		header('Content-Type: application/pdf');
		//nama file
		header('Content-Disposition: inline;filename="' . preg_replace("/[^A-Za-z0-9 ]/", '', 'buku_penerimaan_barang') . '.pdf"');
		//tanpa cache
		header('Cache-Control: max-age=0');

		if (PHP_OS === "Linux") {
			setlocale(LC_TIME, "id_ID");
		} else {
			setlocale(LC_TIME, "id");
		}

		$tmpfile = tempnam(sys_get_temp_dir(), 'tmprpt') . ".csv";
		
		// generate csv
        // table head
		$list = array("nomor;tanggal;dari;nomor_faktur;tanggal_faktur;nama_barang;jumlah_barang;harga_satuan;jumlah_harga;nomor_ba;tanggal_ba;keterangan");
		$index = 0;
        
        // table data
        foreach ($data_buku as $key => $value) {
            $detail = $this->jurnal_detail_model->get_list(array(
                "juno" => $value["juno"],
                "nomor_unit" => $post["skpd"],
                "deleted IS NULL"
            ));


            foreach ($detail as $detail_key => $detail_value) {
                $detail[$detail_key]['namabarang'] = $this->barang_model->get_list(array(
                    "kbid" => $detail[$detail_key]['kbid']
                ));

                if(!empty($detail[$detail_key]['namabarang'])){
                    $detail[$detail_key]['spesifikasi'] = $detail[$detail_key]['namabarang'][0]['kbspesifikasi'];
                    $detail[$detail_key]['namabarang'] = $detail[$detail_key]['namabarang'][0]['kbnama'];
                }
                
                $detail[$detail_key]["totalhargadalamhuruf"] = $this->terbilang(floatval($detail[$detail_key]['judqty']));
                
                $row = "";
                
            	$row .= ++$index . ";" .
            	$data_buku[$key]['jutgl'] . ";" .
            	$data_buku[$key]['spnama'] . ";" .
            	$data_buku[$key]['judokumenno'] . ";" .
            	$data_buku[$key]['judokumentgl'] . ";" .
            	$detail[$detail_key]['namabarang'] . ";" .
                $detail[$detail_key]['judqty'] . ";" .
                "Rp" . number_format($detail[$detail_key]['judhargasat'],2,",",".") . ";" .
                "Rp" . number_format(($detail[$detail_key]['judqty']*$detail[$detail_key]['judhargasat']), 2, ",", ".") . ";" .
                $data_buku[$key]['jubuktino'] . ";" .
                $data_buku[$key]['jubuktitgl'] . ";" .
                $detail[$detail_key]['keterangan'];

                array_push($list, $row);                
            }
        }

		$file = fopen($tmpfile,"w");
		foreach ($list as $line) {
			fputcsv($file, explode(';', $line), "|");
		}

		fclose($file);

		$this->jasper->process(
			"reports/persediaan/buku_penerimaan_barang.jasper", 
			array(
				"namaunit" => $namaunit,
				"pengurusbarang" => $pengurusbarang,
				"nippengurusbarang" => $nippengurusbarang,
				"atasanlangsung" => $atasanlangsung,
				"nipatasanlangsung" => $nipatasanlangsung
				),
			array(
				"type" => "csv",
				"source" => $tmpfile,
				"columns" => "value,id,text,satuan",
				"delimiter" => "|"
			),
			array(
				"pdf"
			)
		)->execute()->output();

		unlink($tmpfile);
    }
    
    // ----------------
    // buku pakai habis
    // ----------------
    
    public function get_populasi_buku_pakai_habis(){
        $post = $this->input->post();
        
        $this->load->model("simgo/persediaan/barang_model", "barang_model");
        $this->load->model("simgo/persediaan/jurnal_model", "jurnal_model");
        $this->load->model("simgo/persediaan/jurnal_detail_model", "jurnal_detail_model");
        
        $bulan = explode("-",$post["tanggal"]);
        $tahun = $bulan[1];
        $bulan = $bulan[0];
        
        $r=$this->jurnal_model->get_list(array(
            "nomor_unit" => $post["skpd"],
            "MONTH(jutgl)" => $bulan,
            "YEAR(jutgl)" => $tahun,
			"tipe != '5'",
            "deleted IS NULL"
        ),0,0,array("jutgl" => "ASC", "tipe" => "ASC"));
        
        foreach ($r as $key => $value) {
            $detail = $this->jurnal_detail_model->get_list(array(
                "juno" => $value["juno"],
                "nomor_unit" => $post["skpd"],
                "deleted IS NULL"
            ));
            
            foreach ($detail as $detail_key => $detail_value) {
                $detail[$detail_key]['namabarang'] = $this->barang_model->get_list(array(
                    "kbid" => $detail[$detail_key]['kbid']
                ));
                
                if(!empty($detail[$detail_key]['namabarang'])){
                    $detail[$detail_key]['spesifikasi'] = $detail[$detail_key]['namabarang'][0]['kbspesifikasi'];
                    $detail[$detail_key]['namabarang'] = $detail[$detail_key]['namabarang'][0]['kbnama'];
                }
                
                $detail[$detail_key]["totalhargadalamhuruf"] = $this->terbilang(floatval(abs($detail[$detail_key]['judqty'])));
            }
            
            usort($detail, function ($a, $b) {
                return strcmp($a["namabarang"], $b["namabarang"]);
            });
            
            $r[$key]["detail"] = $detail;
        }
        
        echo json_encode($r);
    }
    
    public function print_buku_pakai_habis($namaunit, $skpd, $bulan, $pengurusbarang, $nippengurusbarang, $atasanlangsung, $nipatasanlangsung) {

		$this->load->library("jasper");
        $this->load->model("simgo/persediaan/barang_model", "barang_model");
        $this->load->model("simgo/persediaan/jurnal_model", "jurnal_model");
        $this->load->model("simgo/persediaan/jurnal_detail_model", "jurnal_detail_model");

        $namaunit = base64UrlDecode($namaunit);
        $skpd = base64UrlDecode($skpd);
        $bulan = base64UrlDecode($bulan);
        $pengurusbarang = base64UrlDecode($pengurusbarang);
        $nippengurusbarang = base64UrlDecode($nippengurusbarang);
        $atasanlangsung = base64UrlDecode($atasanlangsung);
        $nipatasanlangsung = base64UrlDecode($nipatasanlangsung);
        
        $post = array("skpd" => $skpd, "tanggal" => $bulan);
        
        // $data_buku = $this->data_barang_keluar_terformat($post);

		// if (!file_exists("reports/persediaan/buku_pengeluaran_barang.jasper")) {
		// 	$this->jasper->compile("reports/persediaan/buku_pengeluaran_barang.jrxml")->execute();
		// }
        
        //$post = array("skpd" => "13.02.06.01");
        $tahun = explode( '-', $bulan)[1];
        $bulan2 = explode('-', $bulan)[0];

        $data_buku = $this->jurnal_model->get_list(array(
            "nomor_unit" => $post["skpd"],
            "MONTH(jutgl)" => $bulan2,
            "YEAR(jutgl)" => $tahun,
            "deleted IS NULL",
			"tipe != '5'"
        ),0,0,array("jutgl" => "ASC"));

		if (!file_exists("reports/persediaan/buku_barang_pakai_habis.jasper")) {
			$this->jasper->compile("reports/persediaan/buku_barang_pakai_habis.jrxml")->execute();
		}

		//mime
		header('Content-Type: application/pdf');
		//nama file
		header('Content-Disposition: inline;filename="' . preg_replace("/[^A-Za-z0-9 ]/", '', 'buku_penerimaan_barang') . '.pdf"');
		//tanpa cache
		header('Cache-Control: max-age=0');

		if (PHP_OS === "Linux") {
			setlocale(LC_TIME, "id_ID");
		} else {
			setlocale(LC_TIME, "id");
		}

		$tmpfile = tempnam(sys_get_temp_dir(), 'tmprpt') . ".csv";
		
		// generate csv
        // table head
		$list = array("nomor;nama_barang;merk;thn_buat;tgl_diterima;jumlah_barang;harga_satuan;nomor_ba;tgl_ba;tgl_dikeluarkan;kepada;jumlah_diserahkan;no_surat_penyerahan;keterangan");
        $index = 1;
        // $row = ";;;;;;;;;;;;;";

        // table data
        foreach ($data_buku as $key => $value) {
            $detail = $this->jurnal_detail_model->get_list(array(
                "juno" => $value["juno"],
                "nomor_unit" => $post["skpd"],
                "deleted IS NULL"
            ));
            
            foreach ($detail as $detail_key => $detail_value) {
                $detail[$detail_key]['namabarang'] = $this->barang_model->get_list(array(
                    "kbid" => $detail[$detail_key]['kbid']
                ));
				
                if(!empty($detail[$detail_key]['namabarang'])){
                    $detail[$detail_key]['spesifikasi'] = $detail[$detail_key]['namabarang'][0]['kbspesifikasi'];
                    $detail[$detail_key]['namabarang'] = $detail[$detail_key]['namabarang'][0]['kbnama'];
                }
                
                $detail[$detail_key]["totalhargadalamhuruf"] = $this->terbilang(floatval(abs($detail[$detail_key]['judqty'])));

                $row = "";

                if ($data_buku[$key]['tipe'] == "0") {
                	if($detail_key == 0) {
                		$row .= $index . ";";
                		$index++;
                	} else
                		$row .= ";";
		            	$row .= $detail[$detail_key]['namabarang'] . ";" .
		            	$detail[$detail_key]['spesifikasi'] . ";" .
		                $detail[$detail_key]['judtahunbuat'] . ";" .
		                $data_buku[$key]['jutgl'] . ";" .
		                $detail[$detail_key]['judqty'] . ";" .
		                $detail[$detail_key]['judhargasat'] . ";" .
		                $data_buku[$key]['judasarno'] . ";" .
		                $data_buku[$key]['judasartgl'] . ";" .
		                "" . ";" .
		                "" . ";" .
		                "" . ";" .
		                "" . ";" .
		                $detail[$detail_key]['keterangan'] . ";";
	            } else if ($data_buku[$key]['tipe'] == "1") {
	            	if($detail_key == 0) {
	                    $row .= $index . ";";
	            		$index++;
	            	} else
	            		$row .= ";";
		            	$row .= $detail[$detail_key]['namabarang'] . ";" .
		            	$detail[$detail_key]['spesifikasi'] . ";" .
		                $detail[$detail_key]['judtahunbuat'] . ";" .
		                "" . ";" .
		                "" . ";" .
		                "" . ";" .
		                "" . ";" .
		                "" . ";" .
		                $data_buku[$key]['judasartgl'] . ";" .
		                $data_buku[$key]['sppenanggungjawab'] . ";" .
		                $detail[$detail_key]['judqty'] . ";" .
		                $data_buku[$key]['judasarno'] . ";" .
		                $detail[$detail_key]['keterangan'] . ";";
	            }
                
                array_push($list, $row);              
            }
        }

		$file = fopen($tmpfile,"w");
		foreach ($list as $line) {
			fputcsv($file, explode(';', $line), "|");
		}

		fclose($file);

		$this->jasper->process(
			"reports/persediaan/buku_barang_pakai_habis.jasper", 
			array(
				"namaunit" => $namaunit, 
				"pengurusbarang" => $pengurusbarang, 
				"nippengurusbarang" => $nippengurusbarang, 
				"atasanlangsung" => $atasanlangsung, 
				"nipatasanlangsung" => $nipatasanlangsung),
			array(
				"type" => "csv",
				"source" => $tmpfile,
				"columns" => "value,id,text,satuan",
				"delimiter" => "|"
			),
			array(
				"pdf"
			)
		)->execute()->output();

		unlink($tmpfile);
    }
    
    // ----------------
    // laporan semester
    // ----------------
    
    private function data_laporan_semester($post){
        $this->load->model("simgo/persediaan/barang_model", "barang_model");
        $this->load->model("simgo/persediaan/jurnal_model", "jurnal_model");
        $this->load->model("simgo/persediaan/jurnal_detail_model", "jurnal_detail_model");
        
        $semester = ($post['semester']=='1')?" between '1' and '6'" : " between '7' and '12'";
        $tahun = $post["tahun"];
        
        $r=$this->jurnal_model->get_list(array(
            "nomor_unit" => $post["skpd"],
            "MONTH(jutgl)".$semester,
            "YEAR(jutgl)" => $tahun,
            "deleted IS NULL"
        ),0,0,array("jutgl" => "ASC", "juno" => "ASC"));
        
        // data structure ::
        // $fifo_pool["kbkode"]["flow_keluar_atau_masuk"]["jurnal_detail_list"]
        $fifo_pool = array();
        // $jurnal[index]
        $jurnal = array();
        
        foreach ($r as $key => $value) {
            
            $value["detail"] = array();
            array_push($jurnal, $value);
            
            $detail = $this->jurnal_detail_model->get_list(array(
                "juno" => $value["juno"],
                "nomor_unit" => $post["skpd"],
                "deleted IS NULL",
            ));
            
            // spesifikasi nama barang dan masukkan ke dalam fifo_pool
            foreach ($detail as $detail_key => $detail_value) {
                $detail[$detail_key]['namabarang'] = $this->barang_model->get_list(array(
                    "kbid" => $detail[$detail_key]['kbid']
                ));
                
                if(!empty($detail[$detail_key]['namabarang'])){
                    $detail[$detail_key]['spesifikasi'] = $detail[$detail_key]['namabarang'][0]['kbspesifikasi'];
                    $detail[$detail_key]['kbkode'] = $detail_value['kbkode'] = $detail[$detail_key]['namabarang'][0]['kbkode'];
                    $detail[$detail_key]['namabarang'] = $detail[$detail_key]['namabarang'][0]['kbnama'];
                }
                
                $detail[$detail_key]["qty_huruf"] = $this->terbilang(abs(floatval($detail[$detail_key]['judqty'])));
                
                // put jurnal_detail in fifo_pool
                    $kbkode = $detail_value['kbkode'];
                    $flow   = ($detail_value['judqty'] > 0)?1:-1;
                    
                    if(empty($fifo_pool[$kbkode])){
                        $fifo_pool[$kbkode] = array();
                    }
                    if(empty($fifo_pool[$kbkode][$flow])){
                        $fifo_pool[$kbkode][$flow] = array();
                    }
                    
                    array_push($fifo_pool[$kbkode][$flow], $detail[$detail_key]);
                // ------------------------------
            }
            
            // masukkan "barang masuk" dalam data jurnal yang dipakai untuk report
            if($r[$key]['tipe'] == '0')
            {
                $r[$key]['detail'] = $detail;
                array_push($jurnal, $r[$key]);
            }
        }
        
        // disini memecah pengeluaran yang mengeluarkan barang dari dua jurnal penerimaan berbeda
        foreach ($fifo_pool as $pool_key => $pool_value) {
            // populasikan fifo_pool -> barang masuk
            if(empty($fifo_pool[$pool_key]["1"])){
                $exist = $this->jurnal_detail_model->get_exist_goods_procurement($post["skpd"], $pool_key, $tahun);
                $fifo_pool[$pool_key]["1"] = $exist;
                // $current = $this->jurnal_detail_model->get_this_month_procurement($post["skpd"], $pool_key, $tahun, $bulan);
                // $fifo_pool[$pool_key]["1"] = array_merge($exist,$current);
            }
            
            // "ambil" barang yang dikeluarkan dari stok per pengadaan
            $result = array();
            $avail  = $fifo_pool[$pool_key]["1"];
            
            if(array_key_exists("-1", $pool_value)){
                // satu-per-satu
                foreach ($pool_value["-1"] as $single_key => $single) {
                    $out_remain= -floatval($single['judqty']);
                    
                    // ambil barang dari pengadaan yang bisa habis untuk pengeluaran ini
                    while(!empty($avail) && $avail[0]['judqty'] <= $out_remain)
                    {
                        $t = $single;
                        $out_remain -= $avail[0]["judqty"];
                        $t['judqty'] = -$avail[0]["judqty"];
                        $t['qty_huruf'] = $this->terbilang(abs(floatval($t['judqty'])));
                        $t['judhargasat'] = $avail[0]['judhargasat'];
                        array_push($result, $t);
                        array_shift($avail);
                    }
                    
                    // sisa pengeluaran yang belum diambil dari stok pengadaan diambil dari stok teratas, yang ini ada sisanya
                    if(!empty($avail) && $out_remain > 0){
                        $t = $single;
                        $t['judqty'] = -$out_remain;
                        $t['qty_huruf'] = $this->terbilang(abs(floatval($t['judqty'])));
                        $t['judhargasat'] = $avail[0]['judhargasat'];
                        array_push($result, $t);
                        $avail[0]['judqty'] -= $out_remain;
                    }
                }
            }
            
            // echo json_encode($result);
            
            // masukkan detail data yang sudah dipecah ke jurnal barang keluar yang telah disiapkan
            foreach ($result as $result_key => $result_val) {
                $jurnal_key = array_search($result_val['juno'], array_column($jurnal, 'juno'));
                array_push($jurnal[$jurnal_key]['detail'], $result_val);
            }
        }
        
        // echo json_encode($jurnal);die;
        
        return $jurnal;
    }
    
    public function get_populasi_laporan_semester(){
        $post = $this->input->post();
        
        $r = $this->data_laporan_semester($post);
        
        echo json_encode($r);
    }

    public function print_laporan_semester($namaunit, $skpd, $semester, $tahun, $pengurusbarang, $nippengurusbarang, $atasanlangsung, $nipatasanlangsung) {
		$this->load->library("jasper");
        
        $post = $this->input->post();

        $namaunit = base64UrlDecode($namaunit);
        $skpd = base64UrlDecode($skpd);
        $semester = base64UrlDecode($semester);
        $tahun = base64UrlDecode($tahun);
        $pengurusbarang = base64UrlDecode($pengurusbarang);
        $nippengurusbarang = base64UrlDecode($nippengurusbarang);
        $atasanlangsung = base64UrlDecode($atasanlangsung);
        $nipatasanlangsung = base64UrlDecode($nipatasanlangsung);
        
        $post = array(
        	"skpd" => $skpd,
        	"semester" => $semester,
        	"tahun" => $tahun 
        	);
        
        $data_buku = $this->data_laporan_semester($post);

		if (!file_exists("reports/persediaan/laporan_semester.jasper")) {
			$this->jasper->compile("reports/persediaan/laporan_semester.jrxml")->execute();
		}

		//mime
		header('Content-Type: application/pdf');
		//nama file
		header('Content-Disposition: inline;filename="' . preg_replace("/[^A-Za-z0-9 ]/", '', 'laporan_semester') . '.pdf"');
		//tanpa cache
		header('Cache-Control: max-age=0');

		if (PHP_OS === "Linux") {
			setlocale(LC_TIME, "id_ID");
		} else {
			setlocale(LC_TIME, "id");
		}

		$tmpfile = tempnam(sys_get_temp_dir(), 'tmprpt') . ".csv";
        
		// generate csv
        // table head
		$list = array("nomor;tanggal_terima;dari;nomor_faktur;tanggal_faktur;jenis_surat;nomor_dasar_penerimaan;jumlah_penerimaan;nama_barang_masuk;harga_satuan_masuk;nomor_penerimaan;tanggal_penerimaan;keterangan_terima;nomor_2;tanggal_keluar;nomor_permintaan;tanggal_permintaan;untuk;jumlah_permintaan;nama_barang_keluar;harga_satuan_keluar;jumlah_harga;tanggal_penyerahan;keterangan_keluar");
        
        $penerimaan = array();
        $jurnal_penerimaan_count = 0;
        $pengeluaran = array();
        $jurnal_pengeluaran_count = 0;

        // table data
        foreach ($data_buku as $jukey => $jurnal) {
        	$nomor = $jukey + 1;
            foreach ($jurnal['detail'] as $judkey => $jurnal_detail) {
                if($jurnal['tipe'] == 0){
                	$row = "";
                	if($judkey == 0){
                		$row .= ++$jurnal_penerimaan_count . ";" .
                		$jurnal['jutgl'] . ";" .
                		$jurnal['spnama'] . ";" .
                		$jurnal['judokumenno'] . ";" .
                		$jurnal['judokumentgl'] . ";" .
                		"" . ";" .
                		$jurnal['judasarno'] . ";";
                	} else {
                		$row .= ";;;;;;;";
                	}
                	$row .= $jurnal_detail['judqty'] . " " . strtolower($jurnal_detail['judunit']) . ";" .
                	$jurnal_detail['namabarang'] . ";" .
                	$jurnal_detail['judhargasat'] . ";";
                	if($judkey == 0){
                		$row .= $jurnal['juno'] . ";" .
                		$jurnal['jutgl'] . ";" .
                		$jurnal['keterangan'] . ";";
                	} else {
                		$row .= ";;;";
                	}
                	array_push($penerimaan,$row);
                } else if($jurnal['tipe'] == 1){
                	$row = "";
                	if($judkey == 0){
                		$row .= ++$jurnal_pengeluaran_count . ";" .
                		$jurnal['jutgl'] . ";" .
                		$jurnal['judasarno'] . ";" .
                		$jurnal['judasartgl'] . ";" .
                		$jurnal['spnama'] . ";";
                	} else {
                		$row .= ";;;;;";
                	}
                	$row .= $jurnal_detail['judqty'] . " " . strtolower($jurnal_detail['judunit']) . ";" .
                	$jurnal_detail['namabarang'] . ";" .
                	$jurnal_detail['judhargasat'] . ";" .
                	floatval($jurnal_detail['judhargasat'])*floatval($jurnal_detail['judqty']) . ";";
                	if($judkey == 0){
                		$row .= $jurnal['jutgl'] . ";" .
                		$jurnal['keterangan'] . ";";
                	} else {
                		$row .= ";;";
                	}
                	array_push($pengeluaran,$row);
                }
            }
        }

        //formatting data for printing
        $loopCount = 0;
        if(count($penerimaan) > count($pengeluaran)){
        	$loopCount = count($penerimaan);
        } else{
        	$loopCount = count($pengeluaran);
        }

        for ($i=0; $i<$loopCount; $i++) {
            $table_row = "";
            if(count($penerimaan) > $i) {
                $table_row .= $penerimaan[$i];
            }
            else {
                for ($j = 0; $j < 13; $j++)
                {
                    $table_row .= ";";
                }
            }

            if(count($pengeluaran) > $i) {
                $table_row .= $pengeluaran[$i];
            }
            else {
                for ($j = 0; $j < 11; $j++)
                {
                    $table_row .= ";";
                }
            }
            array_push($list, $table_row);
        }

		$file = fopen($tmpfile,"w");

		foreach ($list as $line) {
			fputcsv($file, explode(';', $line), "|");
		}

		fclose($file);

		//penyesuaian semester untuk penampilan di Jasper
		if ($semester == 1) {
			$semester = "Pertama";
		} else {
			$semester = "Kedua";
		}

		$this->jasper->process(
			"reports/persediaan/laporan_semester.jasper", 
			array(
				"namaunit" => $namaunit,
				"semester" => $semester,
				"tahun" => $tahun, 
				"pengurusbarang" => $pengurusbarang,
				"nippengurusbarang" => $nippengurusbarang,
				"atasanlangsung" => $atasanlangsung,
				"nipatasanlangsung" => $nipatasanlangsung
				),
			array(
				"type" => "csv",
				"source" => $tmpfile,
				"columns" => "value,id,text,satuan",
				"delimiter" => "|"
			),
			array(
				"pdf"
			)
		)->execute()->output();

		unlink($tmpfile);
    }

    // ------------------
    // save item in / out
    // ------------------

	public function simpan_barang_masuk(){
		$post = $this->input->post();

		$this->load->model("simgo/persediaan/jurnal_model", "jurnal_model");
		$this->load->model("simgo/persediaan/jurnal_detail_model", "jurnal_detail_model");

		$jurnal = array(
			"juno" 			=> $post['ju']['nomorJurnal'],
			"nomor_unit" 	=> $post['ju']['skpd'],
            "spnama"        => $post['ju']['vendor'],
			"jutgl" 		=> date("Y-m-d", strtotime($post['ju']['tanggalJurnal'])),
            "judasarno"     => $post['ju']['dasar'],
            "judasartgl"    => date("Y-m-d", strtotime($post['ju']['tanggalDasar'])),
			"jubuktino" 	=> $post['ju']['bukti'],
			"jubuktitgl" 	=> date("Y-m-d", strtotime($post['ju']['tanggalBukti'])),
			"judokumenno" 	=> $post['ju']['dokumen'],
			"judokumentgl" 	=> date("Y-m-d", strtotime($post['ju']['tanggalDokumen'])),
			"tipe" 		    => '0',
			"keterangan" 	=> $post['ju']['keterangan'],
		);

		if($this->jurnal_model->count(array("juno" => $jurnal["juno"], "nomor_unit" => $jurnal["nomor_unit"])) > 0)
		{
			$jurnal["modified"] = date("Y-m-d H:i:s");
			$jurnal["modifiedby"] = $this->session->userdata("uid");

			$this->jurnal_model->update($jurnal);
		}
		else
		{
			$jurnal["created"] = date("Y-m-d H:i:s");
			$jurnal["createdby"] = $this->session->userdata("uid");
            
			$this->jurnal_model->create($jurnal);
		}

		$jurnal_detail_index = array();
		if(!empty($post['jud']))
		{
			// delete all nonexisting
			$index = array();
			foreach ($post['jud'] as $key => $value) {
				array_push($index, $value['judid']);
			}

			$exist = $this->jurnal_detail_model->get_list(array("juno"=>$post['ju']['nomorJurnal'], "deleted is NULL"));

			$delete=array();

			foreach ($exist as $existValue) {

				$flag = false;

				foreach ($index as $indexValue) {
					if ($indexValue == $existValue['judid']) {
						$flag = true;
						break;
					}
				}
				if ($flag == false) {
					array_push($delete, $existValue['judid']);
				}
			}
			foreach ($delete as $value) {
				$update = array(
					"judid" 		=> $value,
					"juno" 			=> $post['ju']['nomorJurnal'],
                    "nomor_unit"    => $post['ju']['skpd'],
					"deleted" 		=> date("Y-m-d H:i:s"),
					"deleted_by" 	=> $this->session->userdata("uid")
				);
				$this->jurnal_detail_model->update($update);
			}

			$i;

			foreach ($post['jud'] as $key => $value) {
				$jurnal_detail = array(
					// "guno" 			=> $value['gudang'],
					"kbid"  		=> $value['idBarang'],
					"juno" 			=> $post['ju']['nomorJurnal'],
                    "nomor_unit"    => $post['ju']['skpd'],
					"judqty" 		=> $value['jumlahBarang'],
					"judhargasat" 	=> $value['hargaSatuan'],
					"judunit" 		=> $value['satuan'],
					"judnilai" 		=> $value['hargaTotal'],
					"judtahunbuat" 	=> str_pad($value['tahunBuat'],4,"0",STR_PAD_LEFT)."-01-01",
					"judkadaluarsa" => str_pad($value['kadaluarsa'],4,"0",STR_PAD_LEFT)."-01-01",
					"keterangan" 	=> $value['keterangan'],
				);

				if($this->jurnal_detail_model->count(array("judid" => $value["judid"])) >= 1)
				{
					$i = $jurnal_detail["judid"] = $value["judid"];
					$this->jurnal_detail_model->update($jurnal_detail);
				}
				else
				{
					$i = $this->jurnal_detail_model->create($jurnal_detail);
				}

				array_push($jurnal_detail_index, $i);
			}
		}

		echo json_encode($jurnal_detail_index);
	}

	public function simpan_barang_keluar(){
		$post = $this->input->post();

		$this->load->model("simgo/persediaan/jurnal_model", "jurnal_model");
		$this->load->model("simgo/persediaan/jurnal_detail_model", "jurnal_detail_model");
		
        $jurnal_detail_index = array();

		$jurnal = array(
			"juno" 				=> $post['ju']['nomorJurnal'],
			"nomor_unit" 		=> $post['ju']['skpd'],
			"spnama" 			=> $post['ju']['bidang'],
			"sppenanggungjawab"	=> $post['ju']['penanggungJawab'],
			"jutgl" 			=> date("Y-m-d", strtotime($post['ju']['tanggalJurnal'])),
			"judasarno" 		=> $post['ju']['dasarHukum'],
			"judasartgl" 		=> date("Y-m-d", strtotime($post['ju']['tanggalDasarHukum'])),
			"judokumenno" 		=> $post['ju']['dokumenPenyerta'],
			"judokumentgl" 		=> date("Y-m-d", strtotime($post['ju']['tanggalDokumenPenyerta'])),
			"juestimasitgl" 	=> date("Y-m-d", strtotime($post['ju']['tanggalEstimasi'])),
			"tipe" 			    => '1',
            "untuk"             => $post['ju']['untuk'],
			"keterangan" 		=> $post['ju']['keterangan'],
        );

		if($this->jurnal_model->count(array("juno" => $jurnal["juno"], "nomor_unit" => $jurnal["nomor_unit"])) > 0)
		{
			$jurnal["modified"] = date("Y-m-d H:i:s");
			$jurnal["modifiedby"] = $this->session->userdata("uid");

			$this->jurnal_model->update($jurnal);
		}
		else
		{
			$jurnal["created"] = date("Y-m-d H:i:s");
			$jurnal["createdby"] = $this->session->userdata("uid");
            
			$this->jurnal_model->create($jurnal);
		}

		if(!empty($post['jud']))
		{
			// delete all nonexisting
			$index = array();
			foreach ($post['jud'] as $key => $value) {
				array_push($index, $value['judid']);
			}

			$exist = $this->jurnal_detail_model->get_list(array("juno"=>$post['ju']['nomorJurnal'], "deleted is NULL"));

			$delete=array();

			foreach ($exist as $existValue) {

				$flag = false;

				foreach ($index as $indexValue) {
					if ($indexValue == $existValue['judid']) {
						$flag = true;
						break;
					}
				}
				if ($flag == false) {
					array_push($delete, $existValue['judid']);
				}
			}

			foreach ($delete as $value) {
				$update = array(
					"judid" 		=> $value,
					"juno" 			=> $post['ju']['nomorJurnal'],
                    "nomor_unit"    => $post['ju']['skpd'],
					"deleted" 		=> date("Y-m-d H:i:s"),
					"deleted_by" 	=> $this->session->userdata("uid")
					);
				$this->jurnal_detail_model->update($update);
			}

			$i;

			foreach ($post['jud'] as $key => $value) {
				$jurnal_detail = array(
					// "guno" 			=> $value['gudang'],
					"kbid"		    => $value['idBarang'],
					"juno" 			=> $post['ju']['nomorJurnal'],
                    "nomor_unit"    => $post['ju']['skpd'],
					"judqty" 		=> $value['jumlahBarang'],
					"judunit" 		=> $value['satuan'],
					"keterangan" 	=> $value['keterangan'],
					);

				if($this->jurnal_detail_model->count(array("judid" => $value["judid"])) >= 1)
				{
					$i = $jurnal_detail["judid"] = $value["judid"];
					$this->jurnal_detail_model->update($jurnal_detail);
				}
				else
				{
					$i = $this->jurnal_detail_model->create($jurnal_detail);
				}

				array_push($jurnal_detail_index, $jurnal_detail);
			}
		}

		echo json_encode($jurnal_detail_index);
	}
    
    public function simpan_stok_opname(){
        $post = $this->input->post();

		$this->load->model("simgo/persediaan/jurnal_model", "jurnal_model");
		$this->load->model("simgo/persediaan/jurnal_detail_model", "jurnal_detail_model");
		
        $jurnal_detail_index = array();
        
        // var_dump($post);die;

		$jurnal = array(
			"juno" 			=> $post['ju']['nomorJurnal'],
			"nomor_unit" 	=> $post['ju']['skpd'],
			"jutgl" 		=> date("Y-m-d", strtotime($post['ju']['tanggalJurnal'])),
			"tipe" 		    => '5',
			"keterangan" 	=> $post['ju']['keterangan'],
        );

		if($this->jurnal_model->count(array("juno" => $jurnal["juno"], "nomor_unit" => $jurnal["nomor_unit"])) > 0)
		{
			$jurnal["modified"] = date("Y-m-d H:i:s");
			$jurnal["modifiedby"] = $this->session->userdata("uid");

			$this->jurnal_model->update($jurnal);
		}
		else
		{
			$jurnal["created"] = date("Y-m-d H:i:s");
			$jurnal["createdby"] = $this->session->userdata("uid");

			$this->jurnal_model->create($jurnal);
		}

		if(!empty($post['jud']))
		{
			// delete all nonexisting
			$index = array();
			foreach ($post['jud'] as $key => $value) {
				array_push($index, $value['judid']);
			}

			$exist = $this->jurnal_detail_model->get_list(array("juno"=>$post['ju']['nomorJurnal'], "deleted is NULL"));

			$delete=array();

			foreach ($exist as $existValue) {

				$flag = false;

				foreach ($index as $indexValue) {
					if ($indexValue == $existValue['judid']) {
						$flag = true;
						break;
					}
				}
				if ($flag == false) {
					array_push($delete, $existValue['judid']);
				}
			}

			foreach ($delete as $value) {
				$update = array(
					"judid" 		=> $value,
					"juno" 			=> $post['ju']['nomorJurnal'],
                    "nomor_unit"    => $post['ju']['skpd'],
					"deleted" 		=> date("Y-m-d H:i:s"),
					"deleted_by" 	=> $this->session->userdata("uid")
					);
				$this->jurnal_detail_model->update($update);
			}

			$i;

			foreach ($post['jud'] as $key => $value) {
				$jurnal_detail = array(
					// "guno" 			=> $value['gudang'],
					"kbid"		    => $value['kbid'],
					"juno" 			=> $post['ju']['nomorJurnal'],
                    "nomor_unit"    => $post['ju']['skpd'],
					"judqty" 		=> $value['selisih'],
					"judunit" 		=> $value['kbunit'],
					"keterangan" 	=> $value['keterangan'],
					);

				if($this->jurnal_detail_model->count(array("judid" => $value["judid"])) >= 1)
				{
					$i = $jurnal_detail["judid"] = $value["judid"];
					$this->jurnal_detail_model->update($jurnal_detail);
				}
				else
				{
					$i = $this->jurnal_detail_model->create($jurnal_detail);
				}

				array_push($jurnal_detail_index, $jurnal_detail);
			}
        }

		echo json_encode($jurnal_detail_index);
    }
    
    // -------------------------------------
    // Update Nomor Jurnal dan Delete Jurnal
    // -------------------------------------
    
    public function update_juno(){
        $this->load->model("simgo/persediaan/jurnal_model", "jurnal_model");
        
        $post = $this->input->post();
        
        // tidak boleh mengubah nomor jurnal ke jurnal yang telah ada
        $newno = $post["nomorbaru"];
        $junow = $post["juno"];
        $skpd  = $post["skpd"];
        
        if($this->jurnal_model->count(array("juno" => $newno, "nomor_unit" => $skpd)) > 0){
            echo json_encode("already exist");
            return;
        }
        
        $this->jurnal_model->update_juno($junow, $newno, $skpd);
        echo json_encode("updated");
    }
    
    public function delete_jurnal(){
        $this->load->model("simgo/persediaan/jurnal_model", "jurnal_model");
        
        $post = $this->input->post();
        
        $juno = $post["juno"];
        $skpd = $post["skpd"];
        
        $jurnal = array(
			"juno" 			=> $juno,
            "nomor_unit"    => $skpd,
			"deleted"       => date("Y-m-d H:i:s"),
			"deletedby"     => $this->session->userdata("uid"),
        );
        
        $r = $this->jurnal_model->update($jurnal);
        
        echo json_encode($r);
    }
}