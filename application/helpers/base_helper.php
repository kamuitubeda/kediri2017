<?php

/**
 * appbase_helper.php
 * Encoding: UTF-8
 * Created on Mar 19, 2012
 * @author Christian Wijaya (christianwijaya.net@gmail.com)
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Cek nilai variable nullable atau tidak.
 * @param type $value
 * @return boolean 
 */
function isExist($value) {
    switch ($value) {
        // Add whatever your definition of null is
        // This is just an example
        //-----------------------------
        case 'unknown': // continue
        case 'undefined': // continue
        //-----------------------------
        case 'null': // continue
        case 'NULL': // continue
        case NULL:
            return false;
    }
    // return false by default
    return true;
}

/**
 * Check array associative atau tidak.
 * @param type $array
 * @return boolean 
 */
function array_is_associative($array) {
    if (is_array($array) && !empty($array)) {
        for ($iterator = count($array) - 1; $iterator; $iterator--)
            if (!array_key_exists($iterator, $array)) {
                return true;
            }
        return !array_key_exists(0, $array);
    }
    return false;
}

/**
 * Escape variables.
 * @param type $values
 * @param type $quote 1 or 0
 * @return mixed 
 */
function escape($values, $quote = 1) {
    $values = safe_html($values);
    if (is_array($values)) {
        if (array_is_associative($values)) {
            foreach ($values as $k => $v) {
                if (!$quote)
                    $tmp_val[$k] = mysql_real_escape_string($v);
                else
                    $tmp_val[$k] = "'" . mysql_real_escape_string($v) . "'";
            }
            $values = $tmp_val;
        } else {
            for ($j = 0; $j < sizeof($values); $j++) {
                if (!$quote)
                    $values[$j] = mysql_real_escape_string($values[$j]);
                else
                    $values[$j] = "'" . mysql_real_escape_string($values[$j]) . "'";
            }
        }
    } else {
        if (isExist($values)) {
            if (!$quote)
                $values = mysql_real_escape_string($values);
            else
                $values = "'" . mysql_real_escape_string($values) . "'";
        } else {
            $values = 'NULL';
        }
    }
    return $values;
}

/**
 * Konversi nilai menjadi format html entities.
 * @param type $values
 * @return type mixed
 */
function safe_html($values) {
    if (is_array($values)) {
        if (array_is_associative($values)) {
            foreach ($values as $k => $v)
                $tmp_val[$k] = htmlspecialchars($v);
            $values = $tmp_val;
        } else {
            for ($j = 0; $j < sizeof($values); $j++)
                $values[$j] = htmlspecialchars($values[$j]);
        }
    } else
        $values = htmlspecialchars($values);

    return $values;
}

/**
 * Membangun kondisi SQL statement dari JSON input.
 * @param type $s
 * @return string 
 */
function construct_where($s) {
    $qwery = "";
    $qopers = array(
        'eq' => " = ",
        'ne' => " <> ",
        'lt' => " < ",
        'le' => " <= ",
        'gt' => " > ",
        'ge' => " >= ",
        'bw' => " LIKE ",
        'bn' => " NOT LIKE ",
        'in' => " IN ",
        'ni' => " NOT IN ",
        'ew' => " LIKE ",
        'en' => " NOT LIKE ",
        'cn' => " LIKE ",
        'nc' => " NOT LIKE ");
    if ($s) {
        $jsona = json_decode($s, true);
        if (is_array($jsona)) {
            $gopr = $jsona['groupOp'];
            $rules = $jsona['rules'];
            $i = 0;
            $rule = array();
            foreach ($rules as $rule) {
                $field = $rule['field'];
                $op = $rule['op'];
                $v = $rule['data'];

                // bug: jika input (string)0, akan diinterpretasikan menjadi false, maka pencarian akan terabaikan [SOLVE]
                if (($v || $v == '0') && $op) {
                    $i++;
                    // ToSql in this case is absolutley needed
                    $v = toSql($field, $op, $v);
                    if ($i == 1)
                        $qwery = '';
                    else
                        $qwery .= ' ' . $gopr . ' ';
                    switch ($op) {
                        // in need other thing
                        case 'in' :
                        case 'ni' :
                            $qwery .= escape($field, false) . $qopers[$op] . ' (' . $v . ')';
                            break;
                        default:
                            $qwery .= escape($field, false) . $qopers[$op] . $v;
                    }
                }
            }
        }
    }
    return $qwery;
}

/**
 * Membangun kondisi SQL statement berdasarkan field, operator dan nlai field tersebut.
 * @param type $field
 * @param type $oper
 * @param type $val
 * @return string
 */
function toSql($field, $oper, $val) {
    // we need here more advanced checking using the type of the field - i.e. integer, string, float
    switch ($field) {
        default :
            //mysql_real_escape_string is better
            if ($oper == 'bw' || $oper == 'bn')
                return "'" . escape($val, false) . "%'";
            else if ($oper == 'ew' || $oper == 'en')
                return "'%" . escape($val, false) . "'";
            else if ($oper == 'cn' || $oper == 'nc')
                return "'%" . escape($val, false) . "%'";
            else
                return "'" . escape($val, false) . "'";
    }
}

/**
 * Strip karakter slash.
 * @param type $value
 * @return mixed 
 */
function strip($value) {
    if (get_magic_quotes_gpc() != 0) {
        if (is_array($value)) {
            if (array_is_associative($value)) {
                foreach ($value as $k => $v)
                    $tmp_val[$k] = stripslashes($v);
                $value = $tmp_val;
            } else {
                for ($j = 0; $j < sizeof($value); $j++)
                    $value[$j] = stripslashes($value[$j]);
            }
        } else
            $value = stripslashes($value);
    }
    return $value;
}

/**
 * Ubah nilai null memjadi '-'
 * @param type $value
 * @return string 
 */
function display($value) {
    return isset($value) ? $value : "-";
}

/**
 * base64 encode url
 * @param type $data
 * @return type 
 */
function base64UrlEncode($data) {
    return strtr(rtrim(base64_encode($data), '='), '+/', '-_');
}

/**
 * base64 decode url
 * @param type $base64
 * @return type 
 */
function base64UrlDecode($base64) {
    return base64_decode(strtr($base64, '-_', '+/'));
}

function show_validations() {
    $ci = &get_instance();
    $str = "";
    if ($ci->session->flashdata('validation_errors')) {
        $str .= '<div class="validation">';
        $str .= $ci->session->flashdata('validation_errors');
        $str .= '</div>';
    }
    
    return $str;
}

function show_warning() {
    $ci = &get_instance();
    $str = "";
    if ($ci->session->flashdata('warning')) {
        $str .= '<div class="warning">';
        $str .= $ci->session->flashdata('warning');
        $str .= '</div>';
    }
    
    return $str;
}

function show_info() {
    $ci = &get_instance();
    $str = "";
    if ($ci->session->flashdata('info')) {
        $str .= '<div class="info">';
        $str .= $ci->session->flashdata('info');
        $str .= '</div>';
    }
    
    return $str;
}

function form_message() {
    echo show_validations();
    echo show_warning();
    echo show_info();
}

function form_data($field) {
    $ci = &get_instance();
    $post = $ci->session->flashdata('post');
    if (!is_array($post))
        return '';
    return array_key_exists($field, $post) ? $post[$field] : '';
}

/**
 *  function _push_file($path, $name)
 * This function pushes a file out to a user for download.
 * @param    STRING    $path    The full absolute path to the file to be pushed.
 * @param    STRING    $name    The file name of the file to be pushed.
 * @author   Matthew Craig 
 * @copyright 2010 Matthew Craig.
 */
function _push_file($path, $name)
{
  // make sure it's a file before doing anything!
    if(is_file($path))
    {
        // required for IE
        if(ini_get('zlib.output_compression')) { ini_set('zlib.output_compression', 'Off'); }

        // get the file mime type using the file extension
        //$this->load->helper('file');
        $ci = &get_instance();
        $ci->load->helper('file');

        /**
         * This uses a pre-built list of mime types compiled by Codeigniter found at
         * /system/application/config/mimes.php 
         * Codeigniter says this is prone to errors and should not be dependant upon
         * However it has worked for me so far. 
         * You can also add more mime types as needed.
         */
        $mime = get_mime_by_extension($path);

        // Build the headers to push out the file properly.
        header('Pragma: public');     // required
        header('Expires: 0');         // no cache
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Last-Modified: '.gmdate ('D, d M Y H:i:s', filemtime ($path)).' GMT');
        header('Cache-Control: private',false);
        header('Content-Type: '.$mime);  // Add the mime type from Code igniter.
        header('Content-Disposition: attachment; filename="'.basename($name).'"');  // Add the file name
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: '.filesize($path)); // provide file size
        header('Connection: close');
        readfile($path); // push it out
        exit();
    }
}

?>
