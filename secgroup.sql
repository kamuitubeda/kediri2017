-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 20, 2018 at 05:05 AM
-- Server version: 5.6.11
-- PHP Version: 5.5.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `kediri_2017`
--

-- --------------------------------------------------------

--
-- Table structure for table `secgroup`
--

CREATE TABLE IF NOT EXISTS `secgroup` (
  `gid` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID group',
  `oid` int(11) NOT NULL COMMENT 'ID organisasi',
  `created` datetime DEFAULT NULL,
  `createdby` varchar(50) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modifiedby` varchar(50) DEFAULT NULL,
  `gname` varchar(50) NOT NULL COMMENT 'Nama grup',
  `genable` decimal(1,0) DEFAULT '1' COMMENT 'Apakah aktif?',
  PRIMARY KEY (`gid`),
  KEY `fk_secman_8` (`oid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `secgroup`
--

INSERT INTO `secgroup` (`gid`, `oid`, `created`, `createdby`, `modified`, `modifiedby`, `gname`, `genable`) VALUES
(3, 244, '2012-08-20 01:49:08', '0', '2017-01-05 12:49:04', 'admin', 'System Administrator', '1'),
(7, 244, '2012-08-29 04:32:22', 'admin', '2017-01-05 12:48:56', 'admin', 'Pengguna', '1'),
(8, 244, '2014-10-21 11:53:33', 'admin', '2017-01-05 12:48:47', 'admin', 'Operator', '1'),
(9, 244, '2018-02-20 10:36:16', 'admin', '2018-02-20 10:36:16', 'admin', 'BPK', '1');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
