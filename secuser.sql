-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 20, 2018 at 05:05 AM
-- Server version: 5.6.11
-- PHP Version: 5.5.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `kediri_2017`
--

-- --------------------------------------------------------

--
-- Table structure for table `secuser`
--

CREATE TABLE IF NOT EXISTS `secuser` (
  `uid` varchar(50) NOT NULL COMMENT 'User ID pengguna',
  `oid` int(11) NOT NULL COMMENT 'ID organisasi',
  `created` datetime DEFAULT NULL,
  `createdby` varchar(50) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `modifiedby` varchar(50) DEFAULT NULL,
  `uname` varchar(200) DEFAULT NULL COMMENT 'Nama Pengguna',
  `upass` varchar(32) NOT NULL COMMENT '- Password pengguna\n            - Password pengguna diencode MD5',
  `uemail` varchar(200) DEFAULT NULL COMMENT 'Email pengguna',
  `uban` decimal(1,0) DEFAULT '0' COMMENT 'Apakah pengguna di ban?',
  `uenable` decimal(1,0) DEFAULT '1' COMMENT 'Apakah aktif?',
  PRIMARY KEY (`uid`),
  KEY `fk_secman_7` (`oid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `secuser`
--

INSERT INTO `secuser` (`uid`, `oid`, `created`, `createdby`, `modified`, `modifiedby`, `uname`, `upass`, `uemail`, `uban`, `uenable`) VALUES
('admin', 244, '2012-08-21 06:20:35', '0', '2017-01-05 12:49:26', 'admin', 'Administrator', '21232f297a57a5a743894a0e4a801fc3', 'christianwijaya.net@gmail.com', '0', '1'),
('adminaset', 208, '2018-01-23 10:48:26', 'admin', '2018-01-23 10:48:26', 'admin', 'Admin Aset', '6f6e7a4fc0cdfe897d3addc49e0b67e3', 'adminaset@adminaset.com', '0', '1'),
('adminjhon', 208, '2018-01-23 10:48:51', 'admin', '2018-01-23 10:48:51', 'admin', 'Admin Jhon', 'd9db423f9ca1a2d1cf9ca4b206c8a389', 'adminjhon@adminjhon.com', '0', '1'),
('adminkus', 208, '2018-01-23 10:47:45', 'admin', '2018-01-23 10:47:45', 'admin', 'Admin Kus', '6e4dd1e7ce1c82e9cbc68d250304d6a9', 'adminkus@adminkus.com', '0', '1'),
('arsip', 194, '2017-12-20 09:18:00', 'admin', '2017-12-20 09:18:00', 'admin', 'Kantor Arsip dan Perpustakaan', 'eeb2ce9f5869915c541e58ef1519e5b9', 'arsip@arsip.com', '0', '1'),
('badas', 242, '2017-12-19 08:10:47', 'admin', '2017-12-19 08:10:47', 'admin', 'Pengurus Barang Kecamatan Badas', '45e7c9553cfbafe23bf44a5d7bbbdb11', 'badas@badas.com', '0', '1'),
('bakesbang', 214, '2017-12-20 09:14:03', 'admin', '2017-12-20 09:14:03', 'admin', 'Badan Kesatuan Bangsa Politik dan Perlindungan Masyarakat', 'bca919db17ac2119ecb8a275a723e38c', 'bakesbang@bakesbang.com', '0', '1'),
('banyakan', 238, '2017-12-19 08:08:48', 'admin', '2017-12-19 08:08:48', 'admin', 'Pengurus Barang Kecamatan Banyakan', '7fdd1e013b14c1027737f095199ad60e', 'banyakan@banyakan.com', '0', '1'),
('bapemas', 196, '2017-12-20 09:19:10', 'admin', '2017-12-20 09:19:10', 'admin', 'Badan Pemberdayaan Masyarakat dan Pemerintah Desa', '6b76a5d912f0043979ce628341b6e956', 'bapemas@bapemas.com', '0', '1'),
('bappeda', 210, '2017-12-20 08:22:27', 'admin', '2017-12-20 08:22:27', 'admin', 'Badan Perencanaan Pembangunan Daerah (BAPPEDA)', '5daf21d5a174841f7368b6172d107462', 'bappeda@bappeda.com', '0', '1'),
('bkd', 216, '2017-12-20 09:14:28', 'admin', '2017-12-20 09:14:28', 'admin', 'Badan Kepegawaian Daerah', 'be33c03dd14596fb9eb64c676f25f466', 'bkd@bkd.com', '0', '1'),
('bkp3', 203, '2017-12-20 09:20:24', 'admin', '2017-12-20 09:20:24', 'admin', 'Badan Ketahanan Pangan dan Pelaksana Penyuluhan (BKP3)', 'd57eace73839db0e9738b2a4468dee5f', 'bkp3@bkp3.com', '0', '1'),
('bpbd', 247, '2017-12-20 09:25:51', 'admin', '2017-12-20 09:25:51', 'admin', 'Badan Penanggulangan Bencana Daerah (BPBD)', 'f4b87ee0226ef0b10ec0d78a8b2282ed', 'bpbd@bpbd.com', '0', '1'),
('bpk1', 244, '2018-02-20 10:51:05', 'admin', '2018-02-20 10:51:05', 'admin', 'BPK 1', '8ec929a304417ac2abbe8169692e0840', 'bpk1@bpk1.com', '0', '1'),
('bpk2', 244, '2018-02-20 10:39:39', 'admin', '2018-02-20 10:39:39', 'admin', 'BPK 2', '41992ad0cd72939ccd243ca0a63b0bf7', 'bpk2@bpk2.com', '0', '1'),
('bpk3', 244, '2018-02-20 10:52:15', 'admin', '2018-02-20 10:52:15', 'admin', 'BPK 3', 'ecab39174d3750a80969d5920f979db7', 'bpk3@bpk3.com', '0', '1'),
('bpkad', 208, '2017-12-20 09:22:50', 'admin', '2017-12-20 09:22:50', 'admin', 'BADAN PENGELOLAAN KEUANGAN DAN ASET DAERAH (BPKAD)', '7957093083bb9408f77ffa031e9b230a', 'bpkad@bpkad.com', '0', '1'),
('bppkb', 197, '2017-12-20 09:18:28', 'admin', '2017-12-20 09:18:28', 'admin', 'Badan Pemberdayaan Perempuan dan Keluarga Berencana', '629c0bb4d007ed9f979bc18d6dca8614', 'bppkb@bppkb.com', '0', '1'),
('bptsp', 246, '2017-12-20 09:24:31', 'admin', '2017-12-20 09:24:31', 'admin', 'Badan Penanaman Modal Dan Pelayanan Perijinan Terpadu Satu Pintu', '9a7f6ef6082e473154a508183f2262f8', 'bptsp@btpsp.com', '0', '1'),
('dinkes', 191, '2017-12-20 07:56:45', 'admin', '2017-12-20 07:56:45', 'admin', 'Dinas Kesehatan', '1a93e5cb6c9998b2c5e413e8489ada9d', 'dinkes@dinkes.com', '0', '1'),
('dinkop', 204, '2017-12-20 08:11:31', 'admin', '2017-12-20 08:11:31', 'admin', 'Dinas Koperasi Perindustrian dan Perdagangan', '9556dff3e24ab8d811a49a6ab16be747', 'dinkop@dinkop.com', '0', '1'),
('dinsos', 195, '2017-12-20 08:02:41', 'admin', '2017-12-20 08:02:41', 'admin', 'Dinas Sosial', '003f8e5a78e78e1fb84fc233d4bf882a', 'dinsos@dinsos.com', '0', '1'),
('disbudpar', 213, '2017-12-20 09:13:33', 'admin', '2017-12-20 09:13:33', 'admin', 'Dinas Kebudayaan dan Pariwisata', '5b9864469daf7426f31e0e26e46901fb', 'disbudpar@disbudpar.com', '0', '1'),
('disdukcapil', 198, '2017-12-20 08:03:41', 'admin', '2017-12-20 08:03:41', 'admin', 'Dinas Kependudukan dan Catatan Sipil', '9ab759dcba03123acbd21bbc5788daba', 'disdukcapil@disdukcapil.com', '0', '1'),
('dishub', 189, '2017-12-20 07:56:08', 'admin', '2017-12-20 07:56:08', 'admin', 'Dinas Perhubungan', 'a0f25e5de2638d818bbd1e1d98b6deb3', 'dishub@dishub.com', '0', '1'),
('dishut', 211, '2017-12-20 08:23:00', 'admin', '2017-12-20 08:23:00', 'admin', 'Dinas Kehutanan dan Perkebunan', '91218c21e30bc83a3bb35cf03dbf40a5', 'dishut@dishut.com', '0', '1'),
('diskominfo', 190, '2017-12-20 09:15:21', 'admin', '2017-12-20 09:15:21', 'admin', 'Dinas Komunikasi dan Informatika', '8a3749f9746551aac6aaaf5b39625c7b', 'diskominfo@diskominfo.com', '0', '1'),
('disnakan', 201, '2017-12-20 09:15:57', 'admin', '2017-12-20 09:15:57', 'admin', 'Dinas Peternakan dan Perikanan', 'fb071c23db12df2e14ce3ae2ab406754', 'disnakan@disnakan.com', '0', '1'),
('disnakertrans', 199, '2017-12-20 09:16:52', 'admin', '2017-12-20 09:16:52', 'admin', 'Dinas Tenaga Kerja dan Transmigrasi', '72ed774fa1293e91877eafa099b2461f', 'disnakertrans@disnakertrans.com', '0', '1'),
('dispenda', 207, '2017-12-20 09:22:25', 'admin', '2017-12-20 09:22:25', 'admin', 'Dinas Pendapatan Daerah', 'a2cce0093eb00210421fa87b98df3ba3', 'dispenda@dispenda.com', '0', '1'),
('disperta', 200, '2017-12-20 08:04:32', 'admin', '2017-12-20 08:04:32', 'admin', 'Dinas Pertanian', 'a798fd78f317e96bc4c1e2788c076729', 'disperta@disperta.com', '0', '1'),
('dispora', 193, '2017-12-20 08:00:19', 'admin', '2017-12-20 08:00:19', 'admin', 'Dinas Pendidikan, Pemuda, dan Olahraga', 'f07cca5d75de11fb177ceb30be24895f', 'dispora@dispora.com', '0', '1'),
('dkp', 188, '2017-12-20 09:14:53', 'admin', '2017-12-20 09:14:53', 'admin', 'Dinas Kebersihan dan Pertamanan', '8fe12875fa6859138c8274bbfd3dba34', 'dkp@dkp.com', '0', '1'),
('dppe', 202, '2017-12-20 09:16:21', 'admin', '2017-12-20 09:16:21', 'admin', 'Dinas Pengairan Pertambangan dan Energi (DPPE)', '18e2deba427a913bd9a853957627d354', 'dppe@dppe.com', '0', '1'),
('dppka', 245, '2017-12-20 08:21:14', 'admin', '2017-12-20 08:21:14', 'admin', 'Dinas Pendapatan Pengelolaan Keuangan dan Aset Daerah', '6636e20585e4198b08049a501406bdfa', 'dppka@dppka.com', '0', '1'),
('dpu', 187, '2017-12-20 07:55:40', 'admin', '2017-12-20 07:55:40', 'admin', 'Dinas Pekerjaan Umum', '2391c70baa6b9d7b36d114f49118540f', 'dpu@dpu.com', '0', '1'),
('gampengrejo', 228, '2017-12-19 08:04:19', 'admin', '2017-12-19 08:04:19', 'admin', 'Pengurus Barang Kecamatan Gampengrejo', '006180f7c8a9ec9163dc694c14ec6ceb', 'gampengrejo@gampengrejo.com', '0', '1'),
('grogol', 229, '2017-12-19 08:04:50', 'admin', '2017-12-19 08:04:50', 'admin', 'Pengurus Barang Kecamatan Grogol', '3047fbf8518623d85e3624eb333b584e', 'grogol@grogol.com', '0', '1'),
('gurah', 226, '2017-12-19 08:03:23', 'admin', '2017-12-19 08:03:23', 'admin', 'Pengurus Barang Kecamatan Gurah', '47c2ff8b0295945526fbd71dc5f8fa38', 'gurah@gurah.com', '0', '1'),
('inspektorat', 209, '2017-12-20 08:21:58', 'admin', '2017-12-20 08:21:58', 'admin', 'Inspektorat', '9795e862af22ecb7ebc171e1dc46c068', 'inspektorat@inspektorat.com', '0', '1'),
('kandangan', 235, '2017-12-19 08:07:28', 'admin', '2017-12-19 08:07:28', 'admin', 'Pengurus Barang Kecamatan Kandangan', '6d757420e2cb60c9cdc63d22a206c984', 'kandangan@kandangan.com', '0', '1'),
('kandat', 221, '2017-12-19 08:00:58', 'admin', '2017-12-19 08:00:58', 'admin', 'Pengurus Barang Kecamatan Kandat', 'd3a6fb9175427eb06d3ecb33ad56690f', 'kandat@kandat.com', '0', '1'),
('kayenkidul', 240, '2017-12-19 08:10:03', 'admin', '2017-12-19 08:10:03', 'admin', 'Pengurus Barang Kecamatan Kayen Kidul', '32148d98c019858774cb0491368b0c67', 'kayenkidul@kayenkidul.com', '0', '1'),
('kepung', 234, '2017-12-19 08:07:06', 'admin', '2017-12-19 08:07:06', 'admin', 'Pengurus Barang Kecamatan Kepung', '66de3166f7b60f167cf79afaa6984b2d', 'kepung@kepung.com', '0', '1'),
('klh', 212, '2017-12-20 09:21:16', 'admin', '2017-12-20 09:21:16', 'admin', 'Kantor Lingkungan Hidup', '966f021e7e26f1ff55621b5a8128f25c', 'klh@klh.com', '0', '1'),
('kpm', 205, '2017-12-20 09:19:32', 'admin', '2017-12-20 09:19:32', 'admin', 'Kantor Penanaman Modal', '3201da92c0267f933a62844d97d9684b', 'kpm@kpm.com', '0', '1'),
('kppt', 206, '2017-12-20 09:20:53', 'admin', '2017-12-20 09:20:53', 'admin', 'Kantor Pelayanan Perijinan Terpadu (KPPT)', 'a747e3471e58e4c5a6f77dea3ac03847', 'kppt@kppt.com', '0', '1'),
('kras', 219, '2017-12-19 08:00:00', 'admin', '2017-12-19 08:00:00', 'admin', 'Pengurus Barang Kecamatan Kras', 'f46c2f5d018a298f9c185655e50fdb9c', 'kras@kras.com', '0', '1'),
('kunjang', 237, '2017-12-19 08:08:26', 'admin', '2017-12-19 08:08:26', 'admin', 'Pengurus Barang Kecamatan Kunjang', '4d3a360b7c826cd5f4ae20359301a65e', 'kunjang@kunjang.com', '0', '1'),
('lurahpare', 243, '2017-12-20 09:21:39', 'admin', '2017-12-20 09:21:39', 'admin', 'Kantor Kelurahan Pare', 'ea0f9c26391162a282baf0524e593741', 'lurahpare@lurahpare.com', '0', '1'),
('mojo', 218, '2017-12-19 07:59:37', 'admin', '2017-12-19 07:59:37', 'admin', 'Pengurus Barang Kecamatan Mojo', '775436dbf3645b8481d03d94bc5cd890', 'mojo@mojo.com', '0', '1'),
('ngadiluwih', 220, '2017-12-19 08:00:32', 'admin', '2017-12-19 08:00:32', 'admin', 'Pengurus Barang Kecamatan Ngadiluwih', '39d542e029ac560e8ce9cbf0b26ba05e', 'ngadiluwih@ngadiluwih.com', '0', '1'),
('ngancar', 223, '2017-12-19 08:01:51', 'admin', '2017-12-19 08:01:51', 'admin', 'Pengurus Barang Kecamatan Ngancar', 'e2f3c61cad0b165ae3a785305c80782b', 'ngancar@ngancar.com', '0', '1'),
('ngasem', 241, '2017-12-19 08:10:25', 'admin', '2017-12-19 08:10:25', 'admin', 'Pengurus Barang Kecamatan Ngasem', '0e0b09071ede670b281b769c3baad2d7', 'ngasem@ngasem.com', '0', '1'),
('pagu', 227, '2017-12-19 08:03:44', 'admin', '2017-12-19 08:03:44', 'admin', 'Pengurus Barang Kecamatan Pagu', '36c448718664b544845ca388d8b98b34', 'pagu@pagu.com', '0', '1'),
('papar', 230, '2017-12-19 08:05:14', 'admin', '2017-12-19 08:05:14', 'admin', 'Pengurus Barang Kecamatan Papar', '80ca33e3be0e31e258808b682c5b60bc', 'papar@papar.com', '0', '1'),
('pare', 233, '2017-12-19 08:06:39', 'admin', '2017-12-19 08:06:39', 'admin', 'Pengurus Barang Kecamatan Pare', '48f3fe0dd5a29debec895996ff2118c5', 'pare@pare.com', '0', '1'),
('plemahan', 232, '2017-12-19 08:06:19', 'admin', '2017-12-19 08:06:19', 'admin', 'Pengurus Barang Kecamatan Plemahan', '30c391247fd5717352854269035b8675', 'plemahan@plemahan.com', '0', '1'),
('plosoklaten', 225, '2017-12-19 08:02:53', 'admin', '2017-12-19 08:02:53', 'admin', 'Pengurus Barang Kecamatan Plosoklaten', '7f57ded5e0d5177d250a797005047ab7', 'plosoklaten@plosoklaten.com', '0', '1'),
('puncu', 224, '2017-12-19 08:02:22', 'admin', '2017-12-19 08:02:22', 'admin', 'Pengurus Barang Kecamatan Puncu', '17d0ef707aca6875876b5e8607a7b74c', 'puncu@puncu.com', '0', '1'),
('purwoasri', 231, '2017-12-19 08:05:52', 'admin', '2017-12-19 08:05:52', 'admin', 'Pengurus Barang Kecamatan Purwoasri', 'c92dfd6be80e4f84ff9031416db6190d', 'purwoasri@purwoasri.com', '0', '1'),
('ringinrejo', 239, '2017-12-19 08:09:23', 'admin', '2017-12-19 08:09:23', 'admin', 'Pengurus Barang Kecamatan Ringinrejo', '16dd8851535d4dc27671eee925fd0ba2', 'ringinrejo@ringinrejo.com', '0', '1'),
('rsud', 192, '2017-12-20 07:57:29', 'admin', '2017-12-20 07:57:29', 'admin', 'Rumah Sakit Umum Daerah', 'b3e1cf10e5bef6592f9f43067bb14a5c', 'rsud@rsud.com', '0', '1'),
('satpol', 215, '2017-12-20 09:17:22', 'admin', '2017-12-20 09:17:22', 'admin', 'Kantor Satuan Polisi Pamong Praja (Satpol PP)', '6d8ef64b832cc544a31e71a5e2ee6e05', 'satpol@satpol.com', '0', '1'),
('sekda', 185, '2017-12-20 08:02:04', 'admin', '2017-12-20 08:02:04', 'admin', 'Sekretariat Daerah', '998c432cf3c3e63c5227d3eb01b32d96', 'sekda@sekda.com', '0', '1'),
('sekwan', 178, '2017-12-20 09:18:48', 'admin', '2017-12-20 09:18:48', 'admin', 'Sekretariat DPRD', 'b5e675150a4a039abd89c409c1c0ed56', 'sekwan@sekwan.com', '0', '1'),
('semen', 217, '2017-12-19 07:59:05', 'admin', '2017-12-19 07:59:05', 'admin', 'Pengurus Barang Kecamatan Semen', '31ad0be6fe4568f1e44d3b17d10748f9', 'semen@semen.com', '0', '1'),
('tarokan', 236, '2017-12-19 08:08:03', 'admin', '2017-12-19 08:08:03', 'admin', 'Pengurus Barang Kecamatan Tarokan', 'bce3f542c0cd8f082ba4027aff7356dd', 'tarokan@tarokan.com', '0', '1'),
('wates', 222, '2017-12-19 08:01:24', 'admin', '2017-12-19 08:01:24', 'admin', 'Pengurus Barang Kecamatan Wates', '37e39289b6ccf57bbe171bc03b9487cc', 'wates@wates.com', '0', '1');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
